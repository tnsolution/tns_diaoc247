﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.CRM
{
    public class Customer_ItemMobi
    {
        public int CustomerKey { get; set; } = 0;
        public int StatusKey { get; set; } = 0;
        public int HasTrade { get; set; } = 0;
        public string CustomerName { get; set; } = "";
        public string Phone1 { get; set; } = "";
        public string Phone2 { get; set; } = "";
        public string Email2 { get; set; } = "";
        public string Email1 { get; set; } = "";
        public int CategoryKey { get; set; } = 0;
        public string Status { get; set; } = "";
        public string ProjectName { get; set; } = "";
        public string CategoryName { get; set; } = "";
        public string Room { get; set; } = "";
    }
}
