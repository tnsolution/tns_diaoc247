﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.CRM
{
    public class ItemConsent
    {
        string _RecordKey = "0";
        string _CustomerKey = "0";
        string _Note = "";
        string _Price = "0";      
        string _Room = "0";
        string _Aera = "0";
        string _Project = "";
        string _ProjectKey = "";
        string _Category = "0";
        string _CategoryKey = "0";
        string _Furniture = "0";
        string _FurnitureKey = "0";
        string _CreatedDate = "";
        string _Want = "";
        public string Furniture
        {
            get
            {
                return _Furniture;
            }

            set
            {
                _Furniture = value;
            }
        }

        public string Category
        {
            get
            {
                return _Category;
            }

            set
            {
                _Category = value;
            }
        }

        public string Aera
        {
            get
            {
                return _Aera;
            }

            set
            {
                _Aera = value;
            }
        }

        public string Room
        {
            get
            {
                return _Room;
            }

            set
            {
                _Room = value;
            }
        }

        public string Project
        {
            get
            {
                return _Project;
            }

            set
            {
                _Project = value;
            }
        }

        public string Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }

        public string Note
        {
            get
            {
                return _Note;
            }

            set
            {
                _Note = value;
            }
        }

        public string CustomerKey
        {
            get
            {
                return _CustomerKey;
            }

            set
            {
                _CustomerKey = value;
            }
        }

        public string RecordKey
        {
            get
            {
                return _RecordKey;
            }

            set
            {
                _RecordKey = value;
            }
        }

        public string CreatedDate
        {
            get
            {
                return _CreatedDate;
            }

            set
            {
                _CreatedDate = value;
            }
        }

        public string ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }

        public string CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string FurnitureKey
        {
            get
            {
                return _FurnitureKey;
            }

            set
            {
                _FurnitureKey = value;
            }
        }

        public string Want
        {
            get
            {
                return _Want;
            }

            set
            {
                _Want = value;
            }
        }
    }
}
