﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.CRM
{
    public class ItemCRMSearch
    {
        string _ListKey = "";
        string _Category = "";
        string _Project = "";
        string _CategoryAsset = "";
        string _Name = "";
        string _Phone = "";
        string _Status = "";
        string _TradeType = "";
        string _Department = "0";
        string _Employee = "0";
        string _FromDate = "";
        string _ToDate = "";
        string _Inside = "";
        string _Bed = "";       

        #region MyRegion

        public string Category
        {
            get
            {
                return _Category;
            }

            set
            {
                _Category = value;
            }
        }

        public string Project
        {
            get
            {
                return _Project;
            }

            set
            {
                _Project = value;
            }
        }

        public string CategoryAsset
        {
            get
            {
                return _CategoryAsset;
            }

            set
            {
                _CategoryAsset = value;
            }
        }

        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }

        public string Phone
        {
            get
            {
                return _Phone;
            }

            set
            {
                _Phone = value;
            }
        }

        public string Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public string TradeType
        {
            get
            {
                return _TradeType;
            }

            set
            {
                _TradeType = value;
            }
        }

        public string Department
        {
            get
            {
                return _Department;
            }

            set
            {
                _Department = value;
            }
        }

        public string Employee
        {
            get
            {
                return _Employee;
            }

            set
            {
                _Employee = value;
            }
        }

        public string ListKey
        {
            get
            {
                return _ListKey;
            }

            set
            {
                _ListKey = value;
            }
        }

        public string FromDate { get => _FromDate; set => _FromDate = value; }
        public string ToDate { get => _ToDate; set => _ToDate = value; }
        public string Inside { get => _Inside; set => _Inside = value; }
        public string Bed { get => _Bed; set => _Bed = value; }
        #endregion
    }
}
