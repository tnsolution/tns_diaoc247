﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.CRM
{
    public class ItemTrade
    {
        string _TradeKey = "0";
        string _CustomerKey = "0";
        string _AssetKey = "0";
        string _AssetID = "";
        string _Owner = "";
        string _ContractDate = "";
        string _ContractExpireDate = "";
        string _Category = "";
        string _ProjectName = "";
        string _InCome = "0";
        string _Area = "0";

        public string AssetKey
        {
            get
            {
                return _AssetKey;
            }

            set
            {
                _AssetKey = value;
            }
        }

        public string AssetID
        {
            get
            {
                return _AssetID;
            }

            set
            {
                _AssetID = value;
            }
        }

        public string Owner
        {
            get
            {
                return _Owner;
            }

            set
            {
                _Owner = value;
            }
        }

        public string ContractExpireDate
        {
            get
            {
                return _ContractExpireDate;
            }

            set
            {
                _ContractExpireDate = value;
            }
        }

        public string Category
        {
            get
            {
                return _Category;
            }

            set
            {
                _Category = value;
            }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string CustomerKey
        {
            get
            {
                return _CustomerKey;
            }

            set
            {
                _CustomerKey = value;
            }
        }

        public string InCome
        {
            get
            {
                return _InCome;
            }

            set
            {
                _InCome = value;
            }
        }

        public string Area
        {
            get
            {
                return _Area;
            }

            set
            {
                _Area = value;
            }
        }

        public string TradeKey
        {
            get
            {
                return _TradeKey;
            }

            set
            {
                _TradeKey = value;
            }
        }

        public string ContractDate
        {
            get
            {
                return _ContractDate;
            }

            set
            {
                _ContractDate = value;
            }
        }
    }
}
