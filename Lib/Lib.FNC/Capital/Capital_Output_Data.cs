﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.FNC
{
    public class Capital_Output_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.CapitalDate, A.[Description], A.FromEmployee, A.ToEmployee, A.ApprovedBy,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.FromEmployee) AS FromName,
dbo.FNC_GetNameEmployee(A.ToEmployee) AS ToName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Capital_Output A";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Month, int Year)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.CapitalDate, A.[Description], A.FromEmployee, A.ToEmployee, A.ApprovedBy,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.FromEmployee) AS FromName,
dbo.FNC_GetNameEmployee(A.ToEmployee) AS ToName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Capital_Output A";

            if (Month != 0 && Year != 0)
                zSQL += " WHERE MONTH(A.CapitalDate) = @Month AND Year(A.CapitalDate) = @Year";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Month, int Year, int Approve)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.CapitalDate, A.[Description], A.FromEmployee, A.ToEmployee, A.ApprovedBy,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.FromEmployee) AS FromName,
dbo.FNC_GetNameEmployee(A.ToEmployee) AS ToName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Capital_Output A";

            if (Month != 0 && Year != 0)
                zSQL += " WHERE IsApproved = 1 AND MONTH(A.CapitalDate) = @Month AND Year(A.CapitalDate) = @Year";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.CapitalDate, A.[Description], A.FromEmployee, A.ToEmployee, A.ApprovedBy,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.FromEmployee) AS FromName,
dbo.FNC_GetNameEmployee(A.ToEmployee) AS ToName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Capital_Output A
WHERE CapitalDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static double GetPreviousMoney(int Month, int Year)
        {
            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSql.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return zSql.GetObject(@"
DECLARE @YearParamater INT;
DECLARE @MonthParamater INT
IF(@Month - 1 = 0)
    BEGIN 
        SET @YearParamater = @Year - 1 
        SET @MonthParamater = 12
    END
ELSE
    BEGIN 
        SET @YearParamater = @Year 
        SET @MonthParamater = @Month-1
    END
SELECT ISNULL(SUM(A.Amount),0) AS PreviousMonth 
FROM FNC_Capital_CloseMonth A 
WHERE A.CloseDate IS NOT NULL 
AND MONTH(A.CloseDate) = @MonthParamater
AND Year(A.CloseDate) = @YearParamater").ToDouble();
        }

        public static double SumMoney(int Month, int Year)
        {
            SqlContext zSql = new SqlContext();
            return zSql.GetObject("SELECT SUM(A.Amount) FROM FNC_Capital_Output A WHERE A.IsApproved = 1 AND MONTH(A.CapitalDate) = " + Month + " AND YEAR(CapitalDate)=" + Year).ToDouble();
            //return zSql.GetObject("SELECT SUM(A.Amount) FROM FNC_Receipt_Detail A JOIN FNC_Capital_Output B ON A.FromCapital = B.AutoKey WHERE A.IsApproved = 1 AND MONTH(A.ReceiptDate) = " + Month + " AND YEAR(ReceiptDate)=" + Year).ToDouble();
        }
    }
}
