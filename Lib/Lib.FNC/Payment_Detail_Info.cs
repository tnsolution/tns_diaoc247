﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class Payment_Detail_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _PaymentKey = 0;
        private int _PaymentBy = 0;
        private int _PaymentTo = 0;
        private int _CategoryKey = 0;
        private DateTime _PaymentDate;
        private string _Contents = "";
        private string _Description = "";
        private double _Amount = 0;
        private double _Money = 0;
        private int _IsHasVAT = 0;
        private int _VAT = 0;
        private int _IsApproved = 0;
        private int _IsApproved2 = 0;
        private int _ApprovedBy = 0;
        private int _Approved2By = 0;
        private string _ApproveNote = "";
        private string _ApproveNote2 = "";
        private DateTime _ApprovedDate;
        private DateTime _Approved2Date;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _BranchKey = 0;
        private int _CustomerKey = 0;
        private string _CreatedName = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int PaymentKey
        {
            get { return _PaymentKey; }
            set { _PaymentKey = value; }
        }
        public int PaymentBy
        {
            get { return _PaymentBy; }
            set { _PaymentBy = value; }
        }
        public int PaymentTo
        {
            get { return _PaymentTo; }
            set { _PaymentTo = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public DateTime PaymentDate
        {
            get { return _PaymentDate; }
            set { _PaymentDate = value; }
        }
        public string Contents
        {
            get { return _Contents; }
            set { _Contents = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public int VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public int IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }
        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        public DateTime ApprovedDate
        {
            get { return _ApprovedDate; }
            set { _ApprovedDate = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int IsHasVAT
        {
            get
            {
                return _IsHasVAT;
            }

            set
            {
                _IsHasVAT = value;
            }
        }

        public double Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }

        public string ApproveNote
        {
            get
            {
                return _ApproveNote;
            }

            set
            {
                _ApproveNote = value;
            }
        }

        public int IsApproved2
        {
            get
            {
                return _IsApproved2;
            }

            set
            {
                _IsApproved2 = value;
            }
        }

        public int Approved2By
        {
            get
            {
                return _Approved2By;
            }

            set
            {
                _Approved2By = value;
            }
        }

        public DateTime Approved2Date
        {
            get
            {
                return _Approved2Date;
            }

            set
            {
                _Approved2Date = value;
            }
        }

        public string ApproveNote2
        {
            get
            {
                return _ApproveNote2;
            }

            set
            {
                _ApproveNote2 = value;
            }
        }

        public DateTime ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }

            set
            {
                _ModifiedDate = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Payment_Detail_Info()
        {
        }
        public Payment_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Payment_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ApproveNote = zReader["ApproveNote"].ToString();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["PaymentKey"] != DBNull.Value)
                        _PaymentKey = int.Parse(zReader["PaymentKey"].ToString());
                    if (zReader["PaymentBy"] != DBNull.Value)
                        _PaymentBy = int.Parse(zReader["PaymentBy"].ToString());
                    if (zReader["PaymentTo"] != DBNull.Value)
                        _PaymentTo = int.Parse(zReader["PaymentTo"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["PaymentDate"] != DBNull.Value)
                        _PaymentDate = (DateTime)zReader["PaymentDate"];
                    _Contents = zReader["Contents"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["IsHasVAT"] != DBNull.Value)
                        _IsHasVAT = int.Parse(zReader["IsHasVAT"].ToString());
                    if (zReader["VAT"] != DBNull.Value)
                        _VAT = int.Parse(zReader["VAT"].ToString());
                    if (zReader["IsApproved"] != DBNull.Value)
                        _IsApproved = int.Parse(zReader["IsApproved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Payment_Detail ("
        + " PaymentKey ,PaymentBy ,PaymentTo ,CategoryKey ,PaymentDate ,Contents ,Description ,Money,Amount, IsHasVAT ,VAT ,IsApproved ,ApprovedBy ,ApprovedDate ,EmployeeKey ,DepartmentKey ,BranchKey ,CustomerKey ,CreatedName ,CreatedDate ,CreatedBy, ModifiedDate, ModifiedBy, ModifiedName ) "
         + " VALUES ( "
         + "@PaymentKey ,@PaymentBy ,@PaymentTo ,@CategoryKey ,@PaymentDate ,@Contents ,@Description ,@Money,@Amount, @IsHasVAT ,@VAT ,@IsApproved ,@ApprovedBy ,@ApprovedDate ,@EmployeeKey ,@DepartmentKey ,@BranchKey ,@CustomerKey ,@CreatedName ,GETDATE() ,@CreatedBy, GETDATE(), @ModifiedBy, @ModifiedName ) "
         + " SELECT AutoKey FROM FNC_Payment_Detail WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = _PaymentKey;
                zCommand.Parameters.Add("@PaymentBy", SqlDbType.Int).Value = _PaymentBy;
                zCommand.Parameters.Add("@PaymentTo", SqlDbType.Int).Value = _PaymentTo;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                if (_PaymentDate.Year == 0001)
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = _PaymentDate;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@VAT", SqlDbType.Int).Value = _VAT;

                zCommand.Parameters.Add("@IsHasVAT", SqlDbType.Int).Value = _IsHasVAT;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;

                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Payment_Detail SET "
                        + " PaymentKey = @PaymentKey,"
                        + " PaymentBy = @PaymentBy,"
                        + " PaymentTo = @PaymentTo,"
                        + " CategoryKey = @CategoryKey,"
                        + " PaymentDate = @PaymentDate,"
                        + " Contents = @Contents,"
                        + " Description = @Description,"
                        + " Amount = @Amount,"
                        + " VAT = @VAT, IsHasVAT = @IsHasVAT, Money=@Money, ApproveNote = @ApproveNote,"
                        + " IsApproved = @IsApproved,"
                        + " ApprovedBy = @ApprovedBy,"
                        + " ApprovedDate = @ApprovedDate,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " CustomerKey = @CustomerKey,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ApproveNote", SqlDbType.NVarChar).Value = _ApproveNote;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = _PaymentKey;
                zCommand.Parameters.Add("@PaymentBy", SqlDbType.Int).Value = _PaymentBy;
                zCommand.Parameters.Add("@PaymentTo", SqlDbType.Int).Value = _PaymentTo;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                if (_PaymentDate.Year == 0001)
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = _PaymentDate;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;

                zCommand.Parameters.Add("@VAT", SqlDbType.Int).Value = _VAT;
                zCommand.Parameters.Add("@IsHasVAT", SqlDbType.Int).Value = _IsHasVAT;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;

                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Payment_Detail SET 
IsApproved = @IsApproved,
ApprovedBy = @ApprovedBy, ApproveNote = @ApproveNote,
ApprovedDate = GETDATE() , ModifiedDate = GETDATE(), 
ModifiedBy = @ModifiedBy, ModifiedName =@ModifiedName
WHERE AutoKey = @AutoKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ApproveNote", SqlDbType.NVarChar).Value = _ApproveNote;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetStatus2()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Payment_Detail SET 
IsApproved2 = @IsApproved2,
Approved2By = @Approved2By, ApproveNote2 = @ApproveNote2,
Approved2Date = GETDATE() , ModifiedDate = GETDATE(), 
ModifiedBy = @ModifiedBy, ModifiedName =@ModifiedName
WHERE AutoKey = @AutoKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ApproveNote2", SqlDbType.NVarChar).Value = _ApproveNote2;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@IsApproved2", SqlDbType.Int).Value = IsApproved2;
                zCommand.Parameters.Add("@Approved2By", SqlDbType.Int).Value = Approved2By;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Payment_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
