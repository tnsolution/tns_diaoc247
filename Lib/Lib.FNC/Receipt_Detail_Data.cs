﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class Receipt_Detail_Data
    {
        public static DataTable List(int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.ReceiptDate, A.Description, A.ReceiptBy, A.ReceiptFrom, A.ApprovedBy, A.DepartmentKey,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ReceiptBy) AS ReceiptByName,
dbo.FNC_GetNameEmployee(A.ReceiptFrom) AS ReceiptFromName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Receipt_Detail A WHERE A.CategoryKey = @Category ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Month, int Year, int Department, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.ReceiptDate, A.Description, A.ReceiptBy, A.ReceiptFrom, A.ApprovedBy, A.DepartmentKey,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ReceiptBy) AS ReceiptByName,
dbo.FNC_GetNameEmployee(A.ReceiptFrom) AS ReceiptFromName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Receipt_Detail A 
WHERE A.CategoryKey = @Category";

            if (Month != 0)
                zSQL += " AND MONTH(A.ReceiptDate) = @Month AND Year(A.ReceiptDate) = @Year";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Month, int Year, string Department, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.ReceiptDate, A.Description, A.ReceiptBy, A.ReceiptFrom, A.ApprovedBy, A.DepartmentKey,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ReceiptBy) AS ReceiptByName,
dbo.FNC_GetNameEmployee(A.ReceiptFrom) AS ReceiptFromName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Receipt_Detail A 
WHERE A.CategoryKey = @Category";

            if (Month != 0)
                zSQL += " AND MONTH(A.ReceiptDate) = @Month AND Year(A.ReceiptDate) = @Year";
            if (Department != string.Empty)
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;               
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Month, int Year, int Department, int Category, int IsApprove)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.ReceiptDate, A.Description, A.ReceiptBy, A.ReceiptFrom, A.ApprovedBy, A.DepartmentKey,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ReceiptBy) AS ReceiptByName,
dbo.FNC_GetNameEmployee(A.ReceiptFrom) AS ReceiptFromName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Receipt_Detail A 
WHERE A.CategoryKey = @Category AND A.IsApproved = @IsApprove";

            if (Month != 0)
                zSQL += " AND MONTH(A.ReceiptDate) = @Month AND Year(A.ReceiptDate) = @Year";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@IsApprove", SqlDbType.Int).Value = IsApprove;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(DateTime FromDate, DateTime ToDate, int Department, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.ReceiptDate, A.Description, A.ReceiptBy, A.ReceiptFrom, A.ApprovedBy, A.DepartmentKey,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ReceiptBy) AS ReceiptByName,
dbo.FNC_GetNameEmployee(A.ReceiptFrom) AS ReceiptFromName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Receipt_Detail A 
WHERE A.CategoryKey = @Category ";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.ReceiptDate BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(DateTime FromDate, DateTime ToDate, string Department, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.ReceiptDate, A.Description, A.ReceiptBy, A.ReceiptFrom, A.ApprovedBy, A.DepartmentKey,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ReceiptBy) AS ReceiptByName,
dbo.FNC_GetNameEmployee(A.ReceiptFrom) AS ReceiptFromName,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
A.IsApproved, A.Amount, A.Contents
FROM FNC_Receipt_Detail A 
WHERE A.CategoryKey = @Category ";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.ReceiptDate BETWEEN @FromDate AND @ToDate";
            if (Department != string.Empty)
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                    ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }                
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static double Sum(int Month, int Year, int Department, int Category)
        {
            double Sum = 0;
            string zSQL = @"
SELECT SUM(A.Amount)
FROM FNC_Receipt_Detail A 
WHERE A.CategoryKey = @Category";

            if (Month != 0)
                zSQL += " AND MONTH(A.ReceiptDate) = @Month AND Year(A.ReceiptDate) = @Year";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                double.TryParse(zCommand.ExecuteScalar().ToString(), out Sum);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return Sum;
        }
    }
}
