﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.FNC
{
    public class ItemTicket_Payment
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _Title = "";
        private DateTime _TimePayment;
        private string _Description = "";
        private string _Amount = "";
        private DateTime _CreatedDate;
        private DateTime _ModifiedDate;
        private string _ModifiedName = "";
        private string _CreatedName = "";
        private int _TicketKey = 0;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public DateTime TimePayment
        {
            get { return _TimePayment; }
            set { _TimePayment = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public int TicketKey
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
