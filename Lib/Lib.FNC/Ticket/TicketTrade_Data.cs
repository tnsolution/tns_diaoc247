﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.FNC
{
    public class TicketTrade_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*, dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName FROM FNC_TicketTrade A";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable GetPayment(int TicketKey)
        {
         
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM FNC_TicketPayment WHERE TicketKey =@TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        
    }
        public static DataTable GetCustomer(int TicketKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT B.CustomerKey, B.CustomerName, B.Phone1, B.Phone2, 
B.Address1, B.Address2, B.Email1, B.Email2, A.IsOwner,
B.CardPlace, B.CardID, B.CardDate, B.Birthday,
CASE A.IsOwner 
    WHEN 1 THEN N'(Chủ nhà)' 
    WHEN 2 THEN N'(Khách thuê)' 
    WHEN 3 THEN N'(Khách mua)' 
END AS [Owner]
FROM FNC_TicketTrade_Customer A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.TicketKey = @Key";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = TicketKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
