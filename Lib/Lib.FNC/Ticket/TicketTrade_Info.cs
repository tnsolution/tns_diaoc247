﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.FNC
{
    public class TicketTrade_Info
    {
        #region [ Field Name ]
        private int _TicketKey = 0;
        private DateTime _TicketDate;
        private string _TicketID = "";
        private int _TicketCategory = 0;
        private int _EmployeeKey = 0;
        private string _EmployeeName = "";
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";
        private int _ManagerKey = 0;
        private string _ManagerName = "";
        private int _ProjectKey = 0;
        private string _Address = "";
        private int _AssetKey = 0;
        private int _AssetCategory = 0;
        private string _AssetType = "";
        private float _Area;
        private double _Amount = 0;
        private double _PreOrderAmount = 0;
        private DateTime _DateOrder;
        private string _ReferenceName = "";
        private int _ReferencePercent = 0;
        private string _Description = "";
        private int _Approved = 0;
        private int _ApproveBy = 0;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private double _TongGiaTriChuyenNhuong = 0;
        private double _ThanhToanNhanSoHong = 0;
        private double _BenBThanhToanBenA = 0;
        private double _GiaCongChung = 0;
        private DateTime _NgayCongChung;
        private string _ThoaThuanChuyenNhuong = "";
        private double _PhiMoiGioiBenC = 0;
        private string _PhiMoiGioiBenCBangChu = "";
        private double _SoTienThue = 0;
        private string _SoTienThueBangChu = "";
        private string _SoTienThueGom = "";
        private DateTime _NgayCocThue;
        private double _TienCocThue = 0;
        private DateTime _NgayThanhToanThue;
        private string _SoTienThanhToanThue = "";
        private string _ThueTheo = "";
        private DateTime _ChuyenTienTuNgay;
        private DateTime _ChuyenTienDenNgay;
        private string _ThoiGianThue = "";
        private DateTime _ThueTuNgay;
        private DateTime _ThueDenNgay;
        private DateTime _NgayKyHopDong;
        private DateTime _NgayBanGiao;
        private DateTime _NgayTinhTien;
        private string _ThoaThuanChoThue = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int TicketKey
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
        public DateTime TicketDate
        {
            get { return _TicketDate; }
            set { _TicketDate = value; }
        }
        public string TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }
        public int TicketCategory
        {
            get { return _TicketCategory; }
            set { _TicketCategory = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int ManagerKey
        {
            get { return _ManagerKey; }
            set { _ManagerKey = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public int AssetCategory
        {
            get { return _AssetCategory; }
            set { _AssetCategory = value; }
        }
        public string AssetType
        {
            get { return _AssetType; }
            set { _AssetType = value; }
        }
        public float Area
        {
            get { return _Area; }
            set { _Area = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public double PreOrderAmount
        {
            get { return _PreOrderAmount; }
            set { _PreOrderAmount = value; }
        }
        public DateTime DateOrder
        {
            get { return _DateOrder; }
            set { _DateOrder = value; }
        }
        public string ReferenceName
        {
            get { return _ReferenceName; }
            set { _ReferenceName = value; }
        }
        public int ReferencePercent
        {
            get { return _ReferencePercent; }
            set { _ReferencePercent = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Approved
        {
            get { return _Approved; }
            set { _Approved = value; }
        }
        public int ApproveBy
        {
            get { return _ApproveBy; }
            set { _ApproveBy = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public double TongGiaTriChuyenNhuong
        {
            get { return _TongGiaTriChuyenNhuong; }
            set { _TongGiaTriChuyenNhuong = value; }
        }
        public double ThanhToanNhanSoHong
        {
            get { return _ThanhToanNhanSoHong; }
            set { _ThanhToanNhanSoHong = value; }
        }
        public double BenBThanhToanBenA
        {
            get { return _BenBThanhToanBenA; }
            set { _BenBThanhToanBenA = value; }
        }
        public double GiaCongChung
        {
            get { return _GiaCongChung; }
            set { _GiaCongChung = value; }
        }
        public DateTime NgayCongChung
        {
            get { return _NgayCongChung; }
            set { _NgayCongChung = value; }
        }
        public string ThoaThuanChuyenNhuong
        {
            get { return _ThoaThuanChuyenNhuong; }
            set { _ThoaThuanChuyenNhuong = value; }
        }
        public double PhiMoiGioiBenC
        {
            get { return _PhiMoiGioiBenC; }
            set { _PhiMoiGioiBenC = value; }
        }
        public string PhiMoiGioiBenCBangChu
        {
            get { return _PhiMoiGioiBenCBangChu; }
            set { _PhiMoiGioiBenCBangChu = value; }
        }
        public double SoTienThue
        {
            get { return _SoTienThue; }
            set { _SoTienThue = value; }
        }
        public string SoTienThueBangChu
        {
            get { return _SoTienThueBangChu; }
            set { _SoTienThueBangChu = value; }
        }
        public string SoTienThueGom
        {
            get { return _SoTienThueGom; }
            set { _SoTienThueGom = value; }
        }
        public DateTime NgayCocThue
        {
            get { return _NgayCocThue; }
            set { _NgayCocThue = value; }
        }
        public double TienCocThue
        {
            get { return _TienCocThue; }
            set { _TienCocThue = value; }
        }
        public DateTime NgayThanhToanThue
        {
            get { return _NgayThanhToanThue; }
            set { _NgayThanhToanThue = value; }
        }
        public string SoTienThanhToanThue
        {
            get { return _SoTienThanhToanThue; }
            set { _SoTienThanhToanThue = value; }
        }
        public string ThueTheo
        {
            get { return _ThueTheo; }
            set { _ThueTheo = value; }
        }
        public DateTime ChuyenTienTuNgay
        {
            get { return _ChuyenTienTuNgay; }
            set { _ChuyenTienTuNgay = value; }
        }
        public DateTime ChuyenTienDenNgay
        {
            get { return _ChuyenTienDenNgay; }
            set { _ChuyenTienDenNgay = value; }
        }
        public string ThoiGianThue
        {
            get { return _ThoiGianThue; }
            set { _ThoiGianThue = value; }
        }
        public DateTime ThueTuNgay
        {
            get { return _ThueTuNgay; }
            set { _ThueTuNgay = value; }
        }
        public DateTime ThueDenNgay
        {
            get { return _ThueDenNgay; }
            set { _ThueDenNgay = value; }
        }
        public DateTime NgayKyHopDong
        {
            get { return _NgayKyHopDong; }
            set { _NgayKyHopDong = value; }
        }
        public DateTime NgayBanGiao
        {
            get { return _NgayBanGiao; }
            set { _NgayBanGiao = value; }
        }
        public DateTime NgayTinhTien
        {
            get { return _NgayTinhTien; }
            set { _NgayTinhTien = value; }
        }
        public string ThoaThuanChoThue
        {
            get { return _ThoaThuanChoThue; }
            set { _ThoaThuanChoThue = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }

            set
            {
                _DepartmentName = value;
            }
        }

        public string ManagerName
        {
            get
            {
                return _ManagerName;
            }

            set
            {
                _ManagerName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public TicketTrade_Info()
        {
        }
        public TicketTrade_Info(int TicketKey)
        {
            string zSQL = @"SELECT *, 
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName, 
dbo.FNC_GetDepartmentName(DepartmentKey) AS DepartmentName,
dbo.FNC_GetNameEmployee(ManagerKey) AS ManagerName
FROM FNC_TicketTrade WHERE TicketKey = @TicketKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _DepartmentName = zReader["DepartmentName"].ToString();
                    _ManagerName = zReader["ManagerName"].ToString();

                    if (zReader["TicketKey"] != DBNull.Value)
                        _TicketKey = int.Parse(zReader["TicketKey"].ToString());
                    if (zReader["TicketDate"] != DBNull.Value)
                        _TicketDate = (DateTime)zReader["TicketDate"];
                    _TicketID = zReader["TicketID"].ToString();
                    if (zReader["TicketCategory"] != DBNull.Value)
                        _TicketCategory = int.Parse(zReader["TicketCategory"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    _Address = zReader["Address"].ToString();
                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    if (zReader["AssetCategory"] != DBNull.Value)
                        _AssetCategory = int.Parse(zReader["AssetCategory"].ToString());
                    _AssetType = zReader["AssetType"].ToString();
                    if (zReader["Area"] != DBNull.Value)
                        _Area = float.Parse(zReader["Area"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["PreOrderAmount"] != DBNull.Value)
                        _PreOrderAmount = double.Parse(zReader["PreOrderAmount"].ToString());
                    if (zReader["DateOrder"] != DBNull.Value)
                        _DateOrder = (DateTime)zReader["DateOrder"];

                    _ReferenceName = zReader["ReferenceName"].ToString();
                    if (zReader["ReferencePercent"] != DBNull.Value)
                        _ReferencePercent = int.Parse(zReader["ReferencePercent"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["Approved"] != DBNull.Value)
                        _Approved = int.Parse(zReader["Approved"].ToString());
                    if (zReader["ApproveBy"] != DBNull.Value)
                        _ApproveBy = int.Parse(zReader["ApproveBy"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["TongGiaTriChuyenNhuong"] != DBNull.Value)
                        _TongGiaTriChuyenNhuong = double.Parse(zReader["TongGiaTriChuyenNhuong"].ToString());
                    if (zReader["ThanhToanNhanSoHong"] != DBNull.Value)
                        _ThanhToanNhanSoHong = double.Parse(zReader["ThanhToanNhanSoHong"].ToString());
                    if (zReader["BenBThanhToanBenA"] != DBNull.Value)
                        _BenBThanhToanBenA = double.Parse(zReader["BenBThanhToanBenA"].ToString());
                    if (zReader["GiaCongChung"] != DBNull.Value)
                        _GiaCongChung = double.Parse(zReader["GiaCongChung"].ToString());
                    if (zReader["NgayCongChung"] != DBNull.Value)
                        _NgayCongChung = (DateTime)zReader["NgayCongChung"];
                    _ThoaThuanChuyenNhuong = zReader["ThoaThuanChuyenNhuong"].ToString();
                    if (zReader["PhiMoiGioiBenC"] != DBNull.Value)
                        _PhiMoiGioiBenC = double.Parse(zReader["PhiMoiGioiBenC"].ToString());
                    _PhiMoiGioiBenCBangChu = zReader["PhiMoiGioiBenCBangChu"].ToString();
                    if (zReader["SoTienThue"] != DBNull.Value)
                        _SoTienThue = double.Parse(zReader["SoTienThue"].ToString());
                    _SoTienThueBangChu = zReader["SoTienThueBangChu"].ToString();
                    _SoTienThueGom = zReader["SoTienThueGom"].ToString();
                    if (zReader["NgayCocThue"] != DBNull.Value)
                        _NgayCocThue = (DateTime)zReader["NgayCocThue"];
                    if (zReader["TienCocThue"] != DBNull.Value)
                        _TienCocThue = double.Parse(zReader["TienCocThue"].ToString());
                    if (zReader["NgayThanhToanThue"] != DBNull.Value)
                        _NgayThanhToanThue = (DateTime)zReader["NgayThanhToanThue"];
                    _SoTienThanhToanThue = zReader["SoTienThanhToanThue"].ToString();
                    _ThueTheo = zReader["ThueTheo"].ToString();
                    if (zReader["ChuyenTienTuNgay"] != DBNull.Value)
                        _ChuyenTienTuNgay = (DateTime)zReader["ChuyenTienTuNgay"];
                    if (zReader["ChuyenTienDenNgay"] != DBNull.Value)
                        _ChuyenTienDenNgay = (DateTime)zReader["ChuyenTienDenNgay"];
                    _ThoiGianThue = zReader["ThoiGianThue"].ToString();
                    if (zReader["ThueTuNgay"] != DBNull.Value)
                        _ThueTuNgay = (DateTime)zReader["ThueTuNgay"];
                    if (zReader["ThueDenNgay"] != DBNull.Value)
                        _ThueDenNgay = (DateTime)zReader["ThueDenNgay"];
                    if (zReader["NgayKyHopDong"] != DBNull.Value)
                        _NgayKyHopDong = (DateTime)zReader["NgayKyHopDong"];
                    if (zReader["NgayBanGiao"] != DBNull.Value)
                        _NgayBanGiao = (DateTime)zReader["NgayBanGiao"];
                    if (zReader["NgayTinhTien"] != DBNull.Value)
                        _NgayTinhTien = (DateTime)zReader["NgayTinhTien"];
                    _ThoaThuanChoThue = zReader["ThoaThuanChoThue"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_TicketTrade ("
         + " TicketDate ,TicketID ,TicketCategory ,EmployeeKey ,DepartmentKey ,ManagerKey ,ProjectKey ,Address ,AssetKey ,AssetCategory ,AssetType ,Area ,Amount ,PreOrderAmount ,DateOrder ,ReferenceName ,ReferencePercent ,Description ,Approved ,ApproveBy ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ,TongGiaTriChuyenNhuong ,ThanhToanNhanSoHong ,BenBThanhToanBenA ,GiaCongChung ,NgayCongChung ,ThoaThuanChuyenNhuong ,PhiMoiGioiBenC ,PhiMoiGioiBenCBangChu ,SoTienThue ,SoTienThueBangChu ,SoTienThueGom ,NgayCocThue ,TienCocThue ,NgayThanhToanThue ,SoTienThanhToanThue ,ThueTheo ,ChuyenTienTuNgay ,ChuyenTienDenNgay ,ThoiGianThue ,ThueTuNgay ,ThueDenNgay ,NgayKyHopDong ,NgayBanGiao ,NgayTinhTien ,ThoaThuanChoThue ) "
         + " VALUES ( "
         + "@TicketDate ,@TicketID ,@TicketCategory ,@EmployeeKey ,@DepartmentKey ,@ManagerKey ,@ProjectKey ,@Address ,@AssetKey ,@AssetCategory ,@AssetType ,@Area ,@Amount ,@PreOrderAmount ,@DateOrder ,@ReferenceName ,@ReferencePercent ,@Description ,@Approved ,@ApproveBy ,GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName ,@TongGiaTriChuyenNhuong ,@ThanhToanNhanSoHong ,@BenBThanhToanBenA ,@GiaCongChung ,@NgayCongChung ,@ThoaThuanChuyenNhuong ,@PhiMoiGioiBenC ,@PhiMoiGioiBenCBangChu ,@SoTienThue ,@SoTienThueBangChu ,@SoTienThueGom ,@NgayCocThue ,@TienCocThue ,@NgayThanhToanThue ,@SoTienThanhToanThue ,@ThueTheo ,@ChuyenTienTuNgay ,@ChuyenTienDenNgay ,@ThoiGianThue ,@ThueTuNgay ,@ThueDenNgay ,@NgayKyHopDong ,@NgayBanGiao ,@NgayTinhTien ,@ThoaThuanChoThue ) "
         + " SELECT TicketKey FROM FNC_TicketTrade WHERE TicketKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                if (_TicketDate.Year == 0001)
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = _TicketDate;
                zCommand.Parameters.Add("@TicketID", SqlDbType.NVarChar).Value = _TicketID;
                zCommand.Parameters.Add("@TicketCategory", SqlDbType.Int).Value = _TicketCategory;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = _AssetCategory;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@PreOrderAmount", SqlDbType.Money).Value = _PreOrderAmount;
                if (_DateOrder.Year == 0001)
                    zCommand.Parameters.Add("@DateOrder", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateOrder", SqlDbType.DateTime).Value = _DateOrder;
                zCommand.Parameters.Add("@ReferenceName", SqlDbType.NVarChar).Value = _ReferenceName;
                zCommand.Parameters.Add("@ReferencePercent", SqlDbType.Int).Value = _ReferencePercent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApproveBy", SqlDbType.Int).Value = _ApproveBy;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@TongGiaTriChuyenNhuong", SqlDbType.Money).Value = _TongGiaTriChuyenNhuong;
                zCommand.Parameters.Add("@ThanhToanNhanSoHong", SqlDbType.Money).Value = _ThanhToanNhanSoHong;
                zCommand.Parameters.Add("@BenBThanhToanBenA", SqlDbType.Money).Value = _BenBThanhToanBenA;
                zCommand.Parameters.Add("@GiaCongChung", SqlDbType.Money).Value = _GiaCongChung;
                if (_NgayCongChung.Year == 0001)
                    zCommand.Parameters.Add("@NgayCongChung", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayCongChung", SqlDbType.DateTime).Value = _NgayCongChung;
                zCommand.Parameters.Add("@ThoaThuanChuyenNhuong", SqlDbType.NVarChar).Value = _ThoaThuanChuyenNhuong;
                zCommand.Parameters.Add("@PhiMoiGioiBenC", SqlDbType.Money).Value = _PhiMoiGioiBenC;
                zCommand.Parameters.Add("@PhiMoiGioiBenCBangChu", SqlDbType.NVarChar).Value = _PhiMoiGioiBenCBangChu;
                zCommand.Parameters.Add("@SoTienThue", SqlDbType.Money).Value = _SoTienThue;
                zCommand.Parameters.Add("@SoTienThueBangChu", SqlDbType.NVarChar).Value = _SoTienThueBangChu;
                zCommand.Parameters.Add("@SoTienThueGom", SqlDbType.NVarChar).Value = _SoTienThueGom;
                if (_NgayCocThue.Year == 0001)
                    zCommand.Parameters.Add("@NgayCocThue", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayCocThue", SqlDbType.DateTime).Value = _NgayCocThue;
                zCommand.Parameters.Add("@TienCocThue", SqlDbType.Money).Value = _TienCocThue;
                if (_NgayThanhToanThue.Year == 0001)
                    zCommand.Parameters.Add("@NgayThanhToanThue", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayThanhToanThue", SqlDbType.DateTime).Value = _NgayThanhToanThue;
                zCommand.Parameters.Add("@SoTienThanhToanThue", SqlDbType.NVarChar).Value = _SoTienThanhToanThue;
                zCommand.Parameters.Add("@ThueTheo", SqlDbType.NVarChar).Value = _ThueTheo;
                if (_ChuyenTienTuNgay.Year == 0001)
                    zCommand.Parameters.Add("@ChuyenTienTuNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ChuyenTienTuNgay", SqlDbType.DateTime).Value = _ChuyenTienTuNgay;
                if (_ChuyenTienDenNgay.Year == 0001)
                    zCommand.Parameters.Add("@ChuyenTienDenNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ChuyenTienDenNgay", SqlDbType.DateTime).Value = _ChuyenTienDenNgay;
                zCommand.Parameters.Add("@ThoiGianThue", SqlDbType.NVarChar).Value = _ThoiGianThue;
                if (_ThueTuNgay.Year == 0001)
                    zCommand.Parameters.Add("@ThueTuNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ThueTuNgay", SqlDbType.DateTime).Value = _ThueTuNgay;
                if (_ThueDenNgay.Year == 0001)
                    zCommand.Parameters.Add("@ThueDenNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ThueDenNgay", SqlDbType.DateTime).Value = _ThueDenNgay;
                if (_NgayKyHopDong.Year == 0001)
                    zCommand.Parameters.Add("@NgayKyHopDong", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayKyHopDong", SqlDbType.DateTime).Value = _NgayKyHopDong;
                if (_NgayBanGiao.Year == 0001)
                    zCommand.Parameters.Add("@NgayBanGiao", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayBanGiao", SqlDbType.DateTime).Value = _NgayBanGiao;
                if (_NgayTinhTien.Year == 0001)
                    zCommand.Parameters.Add("@NgayTinhTien", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayTinhTien", SqlDbType.DateTime).Value = _NgayTinhTien;
                zCommand.Parameters.Add("@ThoaThuanChoThue", SqlDbType.NVarChar).Value = _ThoaThuanChoThue;
                _TicketKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_TicketTrade SET "
                        + " TicketDate = @TicketDate,"
                        + " TicketID = @TicketID,"
                        + " TicketCategory = @TicketCategory,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " ManagerKey = @ManagerKey,"
                        + " ProjectKey = @ProjectKey,"
                        + " Address = @Address,"
                        + " AssetKey = @AssetKey,"
                        + " AssetCategory = @AssetCategory,"
                        + " AssetType = @AssetType,"
                        + " Area = @Area,"
                        + " Amount = @Amount,"
                        + " PreOrderAmount = @PreOrderAmount,"
                        + " DateOrder = @DateOrder,"
                        + " ReferenceName = @ReferenceName,"
                        + " ReferencePercent = @ReferencePercent,"
                        + " Description = @Description,"
                        + " Approved = @Approved,"
                        + " ApproveBy = @ApproveBy,"

                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " TongGiaTriChuyenNhuong = @TongGiaTriChuyenNhuong,"
                        + " ThanhToanNhanSoHong = @ThanhToanNhanSoHong,"
                        + " BenBThanhToanBenA = @BenBThanhToanBenA,"
                        + " GiaCongChung = @GiaCongChung,"
                        + " NgayCongChung = @NgayCongChung,"
                        + " ThoaThuanChuyenNhuong = @ThoaThuanChuyenNhuong,"
                        + " PhiMoiGioiBenC = @PhiMoiGioiBenC,"
                        + " PhiMoiGioiBenCBangChu = @PhiMoiGioiBenCBangChu,"
                        + " SoTienThue = @SoTienThue,"
                        + " SoTienThueBangChu = @SoTienThueBangChu,"
                        + " SoTienThueGom = @SoTienThueGom,"
                        + " NgayCocThue = @NgayCocThue,"
                        + " TienCocThue = @TienCocThue,"
                        + " NgayThanhToanThue = @NgayThanhToanThue,"
                        + " SoTienThanhToanThue = @SoTienThanhToanThue,"
                        + " ThueTheo = @ThueTheo,"
                        + " ChuyenTienTuNgay = @ChuyenTienTuNgay,"
                        + " ChuyenTienDenNgay = @ChuyenTienDenNgay,"
                        + " ThoiGianThue = @ThoiGianThue,"
                        + " ThueTuNgay = @ThueTuNgay,"
                        + " ThueDenNgay = @ThueDenNgay,"
                        + " NgayKyHopDong = @NgayKyHopDong,"
                        + " NgayBanGiao = @NgayBanGiao,"
                        + " NgayTinhTien = @NgayTinhTien,"
                        + " ThoaThuanChoThue = @ThoaThuanChoThue"
                       + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                if (_TicketDate.Year == 0001)
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = _TicketDate;
                zCommand.Parameters.Add("@TicketID", SqlDbType.NVarChar).Value = _TicketID;
                zCommand.Parameters.Add("@TicketCategory", SqlDbType.Int).Value = _TicketCategory;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = _AssetCategory;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@PreOrderAmount", SqlDbType.Money).Value = _PreOrderAmount;
                if (_DateOrder.Year == 0001)
                    zCommand.Parameters.Add("@DateOrder", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateOrder", SqlDbType.DateTime).Value = _DateOrder;
                zCommand.Parameters.Add("@ReferenceName", SqlDbType.NVarChar).Value = _ReferenceName;
                zCommand.Parameters.Add("@ReferencePercent", SqlDbType.Int).Value = _ReferencePercent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApproveBy", SqlDbType.Int).Value = _ApproveBy;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@TongGiaTriChuyenNhuong", SqlDbType.Money).Value = _TongGiaTriChuyenNhuong;
                zCommand.Parameters.Add("@ThanhToanNhanSoHong", SqlDbType.Money).Value = _ThanhToanNhanSoHong;
                zCommand.Parameters.Add("@BenBThanhToanBenA", SqlDbType.Money).Value = _BenBThanhToanBenA;
                zCommand.Parameters.Add("@GiaCongChung", SqlDbType.Money).Value = _GiaCongChung;
                if (_NgayCongChung.Year == 0001)
                    zCommand.Parameters.Add("@NgayCongChung", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayCongChung", SqlDbType.DateTime).Value = _NgayCongChung;
                zCommand.Parameters.Add("@ThoaThuanChuyenNhuong", SqlDbType.NVarChar).Value = _ThoaThuanChuyenNhuong;
                zCommand.Parameters.Add("@PhiMoiGioiBenC", SqlDbType.Money).Value = _PhiMoiGioiBenC;
                zCommand.Parameters.Add("@PhiMoiGioiBenCBangChu", SqlDbType.NVarChar).Value = _PhiMoiGioiBenCBangChu;
                zCommand.Parameters.Add("@SoTienThue", SqlDbType.Money).Value = _SoTienThue;
                zCommand.Parameters.Add("@SoTienThueBangChu", SqlDbType.NVarChar).Value = _SoTienThueBangChu;
                zCommand.Parameters.Add("@SoTienThueGom", SqlDbType.NVarChar).Value = _SoTienThueGom;
                if (_NgayCocThue.Year == 0001)
                    zCommand.Parameters.Add("@NgayCocThue", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayCocThue", SqlDbType.DateTime).Value = _NgayCocThue;
                zCommand.Parameters.Add("@TienCocThue", SqlDbType.Money).Value = _TienCocThue;
                if (_NgayThanhToanThue.Year == 0001)
                    zCommand.Parameters.Add("@NgayThanhToanThue", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayThanhToanThue", SqlDbType.DateTime).Value = _NgayThanhToanThue;
                zCommand.Parameters.Add("@SoTienThanhToanThue", SqlDbType.NVarChar).Value = _SoTienThanhToanThue;
                zCommand.Parameters.Add("@ThueTheo", SqlDbType.NVarChar).Value = _ThueTheo;
                if (_ChuyenTienTuNgay.Year == 0001)
                    zCommand.Parameters.Add("@ChuyenTienTuNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ChuyenTienTuNgay", SqlDbType.DateTime).Value = _ChuyenTienTuNgay;
                if (_ChuyenTienDenNgay.Year == 0001)
                    zCommand.Parameters.Add("@ChuyenTienDenNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ChuyenTienDenNgay", SqlDbType.DateTime).Value = _ChuyenTienDenNgay;
                zCommand.Parameters.Add("@ThoiGianThue", SqlDbType.NVarChar).Value = _ThoiGianThue;
                if (_ThueTuNgay.Year == 0001)
                    zCommand.Parameters.Add("@ThueTuNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ThueTuNgay", SqlDbType.DateTime).Value = _ThueTuNgay;
                if (_ThueDenNgay.Year == 0001)
                    zCommand.Parameters.Add("@ThueDenNgay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ThueDenNgay", SqlDbType.DateTime).Value = _ThueDenNgay;
                if (_NgayKyHopDong.Year == 0001)
                    zCommand.Parameters.Add("@NgayKyHopDong", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayKyHopDong", SqlDbType.DateTime).Value = _NgayKyHopDong;
                if (_NgayBanGiao.Year == 0001)
                    zCommand.Parameters.Add("@NgayBanGiao", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayBanGiao", SqlDbType.DateTime).Value = _NgayBanGiao;
                if (_NgayTinhTien.Year == 0001)
                    zCommand.Parameters.Add("@NgayTinhTien", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayTinhTien", SqlDbType.DateTime).Value = _NgayTinhTien;
                zCommand.Parameters.Add("@ThoaThuanChoThue", SqlDbType.NVarChar).Value = _ThoaThuanChoThue;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_TicketKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_TicketTrade WHERE TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string UpdateProduct()
        {
            string zSQL = "UPDATE FNC_TicketTrade SET "
                        + " ProjectKey = @ProjectKey,"
                        + " AssetKey = @AssetKey,"
                        + " Area = @Area,"
                        + " AssetCategory = @AssetCategory,"
                        + " AssetType = @AssetType"
                        + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.NVarChar).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@Area", SqlDbType.NVarChar).Value = _Area;
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.NVarChar).Value = _AssetCategory;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
