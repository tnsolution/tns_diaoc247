﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Departments_Data
    {
        public static DataTable ViewType(int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            if (Type == 1)
            {
                zSQL = "SELECT  * FROM HRM_Departments ORDER BY RANK";
            }
            else
            {
                zSQL = "SELECT  * FROM HRM_Specified";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string ViewSpec(int Spec)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            zSQL = "SELECT  * FROM HRM_Departments WHERE SpecKey=" + Spec + " ORDER BY RANK";
            string list = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();


                foreach (DataRow r in zTable.Rows)
                {
                    list += r["DepartmentKey"].ToString() + ", ";
                }
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return list.Remove(list.LastIndexOf(","));
        }

        public static string ListView(int EmployeeKey)
        {
            string zSQL = "SELECT Department FROM HRM_Department_Admin WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Departments ORDER BY RANK";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Departments A WHERE 1= 1 ";
            if (Department != 0)
            {
                zSQL += " AND DepartmentKey = " + Department;
            }

            zSQL += " ORDER BY RANK ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Month, int Year, int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, 
dbo.FNC_GetPreviousMonth(A.DepartmentKey, @Month, @Year) PreviousMonth,
dbo.FNC_GetPaymentMonth(A.DepartmentKey, @Month, @Year) PaymentMonth,
dbo.FNC_GetReceiptMonth(A.DepartmentKey, @Month, @Year) ReceiptMonth
FROM HRM_Departments A WHERE 1= 1 ";
            if (Department != 0)
            {
                zSQL += " AND DepartmentKey = " + Department;
            }

            zSQL += " ORDER BY [rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Month, int Year, string Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, 
dbo.FNC_GetPreviousMonth(A.DepartmentKey, @Month, @Year) PreviousMonth,
dbo.FNC_GetPaymentMonth(A.DepartmentKey, @Month, @Year) PaymentMonth,
dbo.FNC_GetReceiptMonth(A.DepartmentKey, @Month, @Year) ReceiptMonth
FROM HRM_Departments A WHERE 1= 1 ";
            if (Department != string.Empty)
            {
                zSQL += " AND DepartmentKey IN (" + Department + ")";
            }

            zSQL += " ORDER BY [rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Type">1 Department, 2 Group</param>
        /// <returns></returns>
        public static DataTable Department_Category(int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            if (Type == 1)
            {
                zSQL = "SELECT  * FROM HRM_Departments WHERE Rank = 1";
            }
            else
            {
                zSQL = "SELECT  * FROM HRM_Departments WHERE Rank = 2";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
