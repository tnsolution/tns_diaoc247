﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.HRM
{
    public class Employees_Data
    {
        public static int GetExistCloseMonth(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateCalculator;
            return sql.GetObject(@"
SELECT ISNULL(AutoKey,0) 
FROM HRM_CloseYear
WHERE EmployeeKey = @EmployeeKey
AND CalculateYear = YEAR(@Date)").ToInt();
        }

        public static int GetOffYear(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateCalculator;
            return sql.GetObject(@"
SELECT ISNULL(CurrentYear,0) 
FROM HRM_CloseYear
WHERE EmployeeKey = @EmployeeKey
AND CalculateYear = YEAR(@Date)").ToInt();
        }
        public static float OffNoMoney(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateCalculator;
            return float.Parse(sql.GetObject(@"
SELECT ISNULL(SUM(NumberDay),0) 
FROM HRM_TicketOff
WHERE EmployeeKey = @EmployeeKey
AND Confirmed = 1
AND CategoryKey = 1
AND YEAR(FromDate) = YEAR(@Date)").ToString());
        }

        public static float OffChecked(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateCalculator;
            return float.Parse(sql.GetObject(@"
SELECT COUNT(*)
FROM HRM_TicketOff
WHERE EmployeeKey = @EmployeeKey
AND CategoryKey = 1
AND MONTH(FromDate) = MONTH(@Date)
AND YEAR(FromDate)= YEAR(@Date)").ToString());
        }

        public static float OffNoMoney_Salary(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateCalculator;
            return float.Parse(sql.GetObject(@"
SELECT ISNULL(SUM(NumberDay),0) 
FROM HRM_TicketOff
WHERE EmployeeKey = @EmployeeKey
AND Confirmed = 1
AND CategoryKey = 1
AND MONTH(FromDate) = MONTH(@Date)
AND YEAR(FromDate)= YEAR(@Date)").ToString());
        }

        public static float OffLeft(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@DateCalculator", SqlDbType.DateTime).Value = DateCalculator;
            return float.Parse(sql.GetObject(@"SELECT dbo.FNC_DayOff(@EmployeeKey,@DateCalculator)").ToString());
        }

        public static float OffWithMoney(int EmployeeKey, DateTime DateCalculator)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            sql.CMD.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateCalculator;
            return float.Parse(sql.GetObject(@"SELECT ISNULL(SUM(NumberDay),0) 
FROM HRM_TicketOff
WHERE EmployeeKey = @EmployeeKey
AND Confirmed = 1
AND CategoryKey = 2
AND YEAR(FromDate) = YEAR(@Date)").ToString());
        }

        public static DataTable GetAllManager(int EmployeeKey)
        {
            SqlContext sql = new SqlContext();
            return sql.GetData(@"
IF OBJECT_ID('Users" + EmployeeKey + @"') IS NOT NULL 
DROP TABLE Users" + EmployeeKey + @"
IF OBJECT_ID('tempdb..#temp" + EmployeeKey + @"') IS NOT NULL 
DROP TABLE #temp" + EmployeeKey + @"
SELECT EmployeeKey, LastName + ' ' + FirstName AS EmployeeName, ManagerKey, UnitLevel, DepartmentKey, PositionKey INTO Users" + EmployeeKey + @"
FROM HRM_Employees;
WITH name_tree AS 
(
   SELECT EmployeeName, EmployeeKey, ManagerKey, UnitLevel, DepartmentKey, PositionKey
   FROM Users" + EmployeeKey + @"
   WHERE EmployeeKey = " + EmployeeKey + @"
   UNION ALL
   SELECT C.EmployeeName, C.EmployeeKey, C.ManagerKey, C.UnitLevel, C.DepartmentKey, C.PositionKey
   FROM Users" + EmployeeKey + @" c
   JOIN name_tree P on C.EmployeeKey = P.ManagerKey
   AND C.EmployeeKey <> C.ManagerKey 
) 
SELECT *
INTO #TEMP" + EmployeeKey + @"
FROM name_tree
OPTION (MAXRECURSION 0)
SELECT * FROM #TEMP" + EmployeeKey + @"");
        }

        public static int GetLevel(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT UnitLevel FROM HRM_Employees WHERE EmployeeKey =" + Employee).ToInt();
        }
        public static int GetDepartment(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT DepartmentKey FROM HRM_Employees WHERE EmployeeKey =" + Employee).ToInt();
        }
        public static int GetPositionKey(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT B.PositionKey FROM HRM_Employees A LEFT JOIN HRM_Positions B ON A.PositionKey=B.PositionKey WHERE EmployeeKey =" + Employee).ToInt();
        }
        public static string GetPosition(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT B.Position FROM HRM_Employees A LEFT JOIN HRM_Positions B ON A.PositionKey=B.PositionKey WHERE EmployeeKey =" + Employee).ToString();
        }
        public static int GetManager(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT ManagerKey FROM HRM_Employees WHERE EmployeeKey =" + Employee).ToInt();
        }
        public static string GetManagerName(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey =" + Employee).ToString();
        }

        public static DataTable Staff(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.EmployeeKey, A.Birthday, A.DateStart, A.DateEnd,
A.LastName + ' ' + A.FirstName EmployeeName, A.Gender, A.IsWorking,
C.Position, A.Phone1, A.Email1, A.Class, B.DepartmentName, 
dbo.FNC_GetNameEmployee(A.ManagerKey) ManagerName,
A.ImageThumb
FROM HRM_Employees A 
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
LEFT JOIN HRM_Positions C ON A.PositionKey = C.PositionKey
WHERE ISWORKING = 2";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey ";
            zSQL += " ORDER BY C.[Rank]";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Staff_Working(string Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A 
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
LEFT JOIN HRM_Positions C ON A.PositionKey = C.PositionKey
WHERE IsWorking=2 AND C.Major = 'Sales'";
            if (Department != string.Empty)
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            zSQL += " ORDER BY A.DepartmentKey, B.[Rank] ASC, A.UnitLevel ASC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListWorking(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName,  B.DepartmentName FROM HRM_Employees A LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey WHERE IsWorking=2 AND A.DepartmentKey=" + Department;

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListWorking(string Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName,  B.DepartmentName FROM HRM_Employees A LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey WHERE IsWorking=2 AND A.DepartmentKey IN (" + Department + ")";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A
LEFT JOIN dbo.HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
ORDER BY IsWorking, A.DepartmentKey DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey
WHERE A.DepartmentKey = @DepartmentKey
ORDER BY B.Rank, LastName, IsWorking";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Department, int Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A
LEFT JOIN dbo.HRM_Departments B ON A.DepartmentKey = B.DepartmentKey
WHERE IsWorking=2";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY LastName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable StaffSupport()
        {
            SqlContext zSQL = new SqlContext();
            return zSQL.GetData(@"
SELECT EmployeeKey, DepartmentKey, LastName + ' ' + FirstName AS EmployeeName, ManagerKey, PositionKey
FROM HRM_Employees A
WHERE A.DepartmentKey = 13 AND IsWorking = 2");
        }

        public static DataTable ListWorking()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A 
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
LEFT JOIN HRM_Positions C ON A.PositionKey = C.PositionKey
WHERE IsWorking=2 --AND C.Major = 'Sales'
ORDER BY B.[Rank] ASC, A.UnitLevel ASC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
