﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Employees_Info
    {
        #region [ Field Name ]
        private int _EmployeeKey = 0;

        private string _DepartmentRole = "";
        private double _BasicSalary = 0;
        private double _ExtraFee = 0;
        private double _SupportFee = 0;
        private float _Insurance = 0;

        private double _BasicSalary1 = 0;
        private double _ExtraFee1 = 0;
        private double _SupportFee1 = 0;
        private float _Insurance1 = 0;

        private int _PositionKey = 0;
        private string _Position = "";
        private int _SpecKey = 0;
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";

        private string _UnitLevel = "";
        private string _UnitDescription = "";

        private int _ManagerKey = 0;
        private string _ManagerName = "";
        private string _LastName = "";
        private string _FirstName = "";
        private int _Gender = 0;
        private DateTime _Birthday;
        private string _IDCard = "";
        private DateTime _IDDate;
        private string _IDPlace = "";
        private string _Class = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Phone3 = "";
        private string _Phone4 = "";
        private string _Email1 = "";
        private string _Email2 = "";
        private string _Families = "";
        private string _TaxNumber = "";
        private DateTime _DateStart;
        private DateTime _DateEnd;
        private int _IsWorking = 0;
        private string _Address1 = "";
        private string _Address2 = "";
        private string _CarNumber = "";
        private string _Banks = "";
        private string _LoginID = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private string _ImageThumb = "";
        private string _ImageLarge = "";

        private int _Method = 0;
        private int _Trial = 0;
        private DateTime _DateOffAllow;
        private bool _CheckSaleryTable;
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string UnitLevel
        {
            get { return _UnitLevel; }
            set { _UnitLevel = value; }
        }
        public int ManagerKey
        {
            get { return _ManagerKey; }
            set { _ManagerKey = value; }
        }
        public string ManagerName
        {
            get { return _ManagerName; }
            set { _ManagerName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public DateTime Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        public string IDCard
        {
            get { return _IDCard; }
            set { _IDCard = value; }
        }
        public DateTime IDDate
        {
            get { return _IDDate; }
            set { _IDDate = value; }
        }
        public string IDPlace
        {
            get { return _IDPlace; }
            set { _IDPlace = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }
        public string Phone3
        {
            get { return _Phone3; }
            set { _Phone3 = value; }
        }
        public string Phone4
        {
            get { return _Phone4; }
            set { _Phone4 = value; }
        }
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }
        public string Email2
        {
            get { return _Email2; }
            set { _Email2 = value; }
        }
        public string Families
        {
            get { return _Families; }
            set { _Families = value; }
        }
        public string TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }
        public DateTime DateStart
        {
            get { return _DateStart; }
            set { _DateStart = value; }
        }
        public DateTime DateEnd
        {
            get { return _DateEnd; }
            set { _DateEnd = value; }
        }
        public int IsWorking
        {
            get { return _IsWorking; }
            set { _IsWorking = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public string CarNumber
        {
            get { return _CarNumber; }
            set { _CarNumber = value; }
        }
        public string Banks
        {
            get { return _Banks; }
            set { _Banks = value; }
        }
        public string LoginID
        {
            get { return _LoginID; }
            set { _LoginID = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }

            set
            {
                _DepartmentName = value;
            }
        }

        public string Position
        {
            get
            {
                return _Position;
            }

            set
            {
                _Position = value;
            }
        }

        public string UnitDescription
        {
            get
            {
                return _UnitDescription;
            }

            set
            {
                _UnitDescription = value;
            }
        }

        public string ImageThumb
        {
            get
            {
                return _ImageThumb;
            }

            set
            {
                _ImageThumb = value;
            }
        }

        public string ImageLarge
        {
            get
            {
                return _ImageLarge;
            }

            set
            {
                _ImageLarge = value;
            }
        }

        public double BasicSalary
        {
            get
            {
                return _BasicSalary;
            }

            set
            {
                _BasicSalary = value;
            }
        }

        public int Method
        {
            get
            {
                return _Method;
            }

            set
            {
                _Method = value;
            }
        }

        public int Trial
        {
            get
            {
                return _Trial;
            }

            set
            {
                _Trial = value;
            }
        }

        public DateTime DateOffAllow
        {
            get
            {
                return _DateOffAllow;
            }

            set
            {
                _DateOffAllow = value;
            }
        }

        public double SupportFee
        {
            get
            {
                return _SupportFee;
            }

            set
            {
                _SupportFee = value;
            }
        }

        public float Insurance
        {
            get
            {
                return _Insurance;
            }

            set
            {
                _Insurance = value;
            }
        }

        public double ExtraFee { get => _ExtraFee; set => _ExtraFee = value; }
        public double BasicSalary1 { get => _BasicSalary1; set => _BasicSalary1 = value; }
        public double ExtraFee1 { get => _ExtraFee1; set => _ExtraFee1 = value; }
        public double SupportFee1 { get => _SupportFee1; set => _SupportFee1 = value; }
        public float Insurance1 { get => _Insurance1; set => _Insurance1 = value; }
        public int SpecKey { get => _SpecKey; set => _SpecKey = value; }
        public string DepartmentRole { get => _DepartmentRole; set => _DepartmentRole = value; }
        public bool CheckSaleryTable {
            get { return _CheckSaleryTable; }
            set { _CheckSaleryTable = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Employees_Info()
        {
        }
        public Employees_Info(int EmployeeKey)
        {
            string zSQL = @"
SELECT A.EmployeeKey, A.DepartmentKey, A.PositionKey, A.ManagerKey,
A.LastName, A.FirstName, A.ImageLarge, A.ImageThumb , A.IsWorking, 
A.Gender, A.Birthday, A.IDCard, A.IDDate, A.IDPlace, A.DateStart, A.DateEnd,
A.Class, A.UnitLevel, A.Phone1, A.Email1, A.CarNumber, A.Address1,A.CheckSaleryTable,
A.CreatedDate, A.ModifiedDate, A.CreatedBy, A.ModifiedBy, A.CreatedName, A.ModifiedName,
B.DepartmentName, C.Position, D.Unit + ' | ' +D.Description AS UnitDescription,
dbo.FNC_GetNameEmployee(A.ManagerKey) AS ManagerName, 
Trial, BasicSalary, Insurance, SupportFee, ExtraFee, Method, DateOffAllow,
BasicSalary1, Insurance1, SupportFee1, ExtraFee1, A.SpecKey, A.DepartmentRole
FROM HRM_Employees A
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
LEFT JOIN HRM_Positions C ON A.PositionKey = C.PositionKey
LEFT JOIN SYS_Unit D ON A.UnitLevel = D.AutoKey
WHERE EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _DepartmentRole = zReader["DepartmentRole"].ToString();
                    if (zReader["SpecKey"] != DBNull.Value)
                        _SpecKey = int.Parse(zReader["SpecKey"].ToString());

                    if (zReader["ExtraFee1"] != DBNull.Value)
                        _ExtraFee1 = double.Parse(zReader["ExtraFee1"].ToString());
                    if (zReader["SupportFee1"] != DBNull.Value)
                        _SupportFee1 = double.Parse(zReader["SupportFee1"].ToString());
                    if (zReader["Insurance1"] != DBNull.Value)
                        _Insurance1 = float.Parse(zReader["Insurance1"].ToString());
                    if (zReader["BasicSalary1"] != DBNull.Value)
                        _BasicSalary1 = double.Parse(zReader["BasicSalary1"].ToString());

                    if (zReader["ExtraFee"] != DBNull.Value)
                        _ExtraFee = double.Parse(zReader["ExtraFee"].ToString());
                    if (zReader["SupportFee"] != DBNull.Value)
                        _SupportFee = double.Parse(zReader["SupportFee"].ToString());
                    if (zReader["Insurance"] != DBNull.Value)
                        _Insurance = float.Parse(zReader["Insurance"].ToString());
                    if (zReader["BasicSalary"] != DBNull.Value)
                        _BasicSalary = double.Parse(zReader["BasicSalary"].ToString());

                    if (zReader["DateOffAllow"] != DBNull.Value)
                        _DateOffAllow = (DateTime)zReader["DateOffAllow"];

                    if (zReader["Trial"] != DBNull.Value)
                        _Trial = int.Parse(zReader["Trial"].ToString());
                    if (zReader["Method"] != DBNull.Value)
                        _Method = int.Parse(zReader["Method"].ToString());
                    if (zReader["BasicSalary"] != DBNull.Value)
                        _BasicSalary = double.Parse(zReader["BasicSalary"].ToString());

                    _ImageThumb = zReader["ImageThumb"].ToString();
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    _ManagerName = zReader["ManagerName"].ToString();
                    _Position = zReader["Position"].ToString();
                    _LastName = zReader["LastName"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    if (zReader["Gender"] != DBNull.Value)
                        _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["Birthday"] != DBNull.Value)
                        _Birthday = (DateTime)zReader["Birthday"];
                    _IDCard = zReader["IDCard"].ToString();
                    if (zReader["IDDate"] != DBNull.Value)
                        _IDDate = (DateTime)zReader["IDDate"];
                    _IDPlace = zReader["IDPlace"].ToString();
                    _ImageLarge = zReader["ImageLarge"].ToString();
                    _UnitLevel = zReader["UnitLevel"].ToString();
                    _UnitDescription = zReader["UnitDescription"].ToString();
                    _Class = zReader["Class"].ToString();
                    _Phone1 = zReader["Phone1"].ToString();
                    _Email1 = zReader["Email1"].ToString();
                    if (zReader["DateStart"] != DBNull.Value)
                        _DateStart = (DateTime)zReader["DateStart"];
                    if (zReader["DateEnd"] != DBNull.Value)
                        _DateEnd = (DateTime)zReader["DateEnd"];
                    if (zReader["IsWorking"] != DBNull.Value)
                        _IsWorking = int.Parse(zReader["IsWorking"].ToString());
                    _Address1 = zReader["Address1"].ToString();
                    _CarNumber = zReader["CarNumber"].ToString();
                    _CheckSaleryTable = (bool)zReader["CheckSaleryTable"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        /// <summary>
        /// cap nhat tinh tien luong
        /// </summary>
        /// <returns></returns>
        /// 
        public string UpdateDepartment()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                        + " DepartmentRole = @DepartmentRole,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentRole", SqlDbType.NVarChar).Value = _DepartmentRole;                
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateMethod()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                        + " Method = @Method,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Method", SqlDbType.Int).Value = _Method;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO HRM_Employees (
DepartmentKey, SpecKey ,PositionKey,UnitLevel ,Position , DepartmentRole,
ManagerKey ,ManagerName ,LastName ,FirstName ,
Gender ,Birthday ,IDCard ,IDDate ,IDPlace ,Class , DateOffAllow,
Phone1 ,Phone2 ,Phone3 ,Phone4 ,Email1 ,Email2 ,Families ,
DateStart ,DateEnd ,IsWorking ,Address1 ,Address2 ,CarNumber ,Banks ,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName,
BasicSalary,Insurance,SupportFee, ExtraFee,
BasicSalary1,Insurance1,SupportFee1, ExtraFee1,CheckSaleryTable)
VALUES (
@DepartmentKey, @SpecKey ,@PositionKey,@UnitLevel,@Position ,@DepartmentRole,
@ManagerKey ,@ManagerName ,@LastName ,@FirstName ,@Gender ,@Birthday ,
@IDCard ,@IDDate ,@IDPlace ,@Class, @DateOffAllow ,@Phone1 ,@Phone2 ,@Phone3 ,@Phone4 ,@Email1 ,@Email2 ,@Families ,
@DateStart ,@DateEnd ,@IsWorking ,@Address1 ,@Address2 ,@CarNumber ,@Banks ,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName,
@BasicSalary,@Insurance,@SupportFee,@ExtraFee,
@BasicSalary1,@Insurance1,@SupportFee1,@ExtraFee1,@CheckSaleryTable)
SELECT EmployeeKey FROM HRM_Employees WHERE EmployeeKey = SCOPE_IDENTITY()";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentRole", SqlDbType.NVarChar).Value = DepartmentRole;
                zCommand.Parameters.Add("@SpecKey", SqlDbType.Int).Value = _SpecKey;

                zCommand.Parameters.Add("@Insurance", SqlDbType.Float).Value = _Insurance;
                zCommand.Parameters.Add("@ExtraFee", SqlDbType.Money).Value = _ExtraFee;
                zCommand.Parameters.Add("@SupportFee", SqlDbType.Money).Value = _SupportFee;
                zCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = _BasicSalary;

                zCommand.Parameters.Add("@Insurance1", SqlDbType.Float).Value = _Insurance1;
                zCommand.Parameters.Add("@ExtraFee1", SqlDbType.Money).Value = _ExtraFee1;
                zCommand.Parameters.Add("@SupportFee1", SqlDbType.Money).Value = _SupportFee1;
                zCommand.Parameters.Add("@BasicSalary1", SqlDbType.Money).Value = _BasicSalary1;

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@UnitLevel", SqlDbType.NVarChar).Value = _UnitLevel;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = _Position;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@ManagerName", SqlDbType.NVarChar).Value = _ManagerName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;

                if (_DateOffAllow.Year == 0001)
                    zCommand.Parameters.Add("@DateOffAllow", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateOffAllow", SqlDbType.DateTime).Value = _DateOffAllow;

                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = _IDCard;
                if (_IDDate.Year == 0001)
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = _IDDate;
                zCommand.Parameters.Add("@IDPlace", SqlDbType.NVarChar).Value = _IDPlace;
                zCommand.Parameters.Add("@Class", SqlDbType.NVarChar).Value = _Class;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Phone3", SqlDbType.NVarChar).Value = _Phone3;
                zCommand.Parameters.Add("@Phone4", SqlDbType.NVarChar).Value = _Phone4;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
                zCommand.Parameters.Add("@Families", SqlDbType.NVarChar).Value = _Families;
                if (_DateStart.Year == 0001)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = _DateStart;
                if (_DateEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = _DateEnd;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Int).Value = _IsWorking;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@CheckSaleryTable", SqlDbType.Bit).Value = _CheckSaleryTable;
                zCommand.Parameters.Add("@CarNumber", SqlDbType.NVarChar).Value = _CarNumber;
                zCommand.Parameters.Add("@Banks", SqlDbType.NVarChar).Value = _Banks;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                _EmployeeKey = int.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                        + " DepartmentKey = @DepartmentKey, SpecKey = @SpecKey,DepartmentRole=@DepartmentRole,"
                        + " PositionKey = @PositionKey,"
                        + " UnitLevel = @UnitLevel,"
                        + " ManagerKey = @ManagerKey,"
                        + " LastName = @LastName,"
                        + " FirstName = @FirstName,"
                        + " Gender = @Gender,"
                        + " Birthday = @Birthday,"
                        + " IDCard = @IDCard,"
                        + " IDDate = @IDDate,"
                        + " IDPlace = @IDPlace,"
                        + " Class = @Class,"
                        + " Phone1 = @Phone1,"
                        + " Email1 = @Email1,"
                        + " DateStart = @DateStart,"
                        + " DateEnd = @DateEnd,"
                        + " IsWorking = @IsWorking,"
                        + " Address1 = @Address1,"
                        + " Address2 = @Address2,"
                        + " CarNumber = @CarNumber, DateOffAllow = @DateOffAllow,"
                        + " BasicSalary =@BasicSalary, Insurance=@Insurance, SupportFee=@SupportFee, ExtraFee=@ExtraFee,"
                        + " BasicSalary1 =@BasicSalary1, Insurance1=@Insurance1, SupportFee1=@SupportFee1, ExtraFee1=@ExtraFee1,"
                        + "CheckSaleryTable = @CheckSaleryTable,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentRole", SqlDbType.NVarChar).Value = _DepartmentRole;
                zCommand.Parameters.Add("@SpecKey", SqlDbType.Int).Value = _SpecKey;

                zCommand.Parameters.Add("@ExtraFee", SqlDbType.Money).Value = _ExtraFee;
                zCommand.Parameters.Add("@Insurance", SqlDbType.Float).Value = _Insurance;
                zCommand.Parameters.Add("@SupportFee", SqlDbType.Money).Value = SupportFee;
                zCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = _BasicSalary;

                zCommand.Parameters.Add("@ExtraFee1", SqlDbType.Money).Value = _ExtraFee1;
                zCommand.Parameters.Add("@Insurance1", SqlDbType.Float).Value = _Insurance1;
                zCommand.Parameters.Add("@SupportFee1", SqlDbType.Money).Value = SupportFee1;
                zCommand.Parameters.Add("@BasicSalary1", SqlDbType.Money).Value = _BasicSalary1;

                if (_DateOffAllow.Year == 0001)
                    zCommand.Parameters.Add("@DateOffAllow", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateOffAllow", SqlDbType.DateTime).Value = _DateOffAllow;

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@UnitLevel", SqlDbType.NVarChar).Value = _UnitLevel;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = _IDCard;
                if (_IDDate.Year == 0001)
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = _IDDate;
                zCommand.Parameters.Add("@IDPlace", SqlDbType.NVarChar).Value = _IDPlace;
                zCommand.Parameters.Add("@Class", SqlDbType.NVarChar).Value = _Class;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                if (_DateStart.Year == 0001)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = _DateStart;
                if (_DateEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = _DateEnd;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Int).Value = _IsWorking;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@CarNumber", SqlDbType.NVarChar).Value = _CarNumber;
                zCommand.Parameters.Add("@CheckSaleryTable", SqlDbType.Bit).Value = _CheckSaleryTable;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_EmployeeKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Employees WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetActivated()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                        + " IsWorking = @IsWorking,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Int).Value = _IsWorking;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateImage()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                                   + " ImageThumb = @ImageThumb,"
                                   + " ImageLarge = @ImageLarge,"
                                   + " ModifiedDate = GETDATE(),"
                                   + " ModifiedBy = @ModifiedBy,"
                                   + " ModifiedName = @ModifiedName"
                                   + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@ImageThumb", SqlDbType.NVarChar).Value = _ImageThumb;
                zCommand.Parameters.Add("@ImageLarge", SqlDbType.NVarChar).Value = _ImageLarge;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
