﻿using Lib.Config;
using System.Data;

namespace Lib.HRM
{
    public class Helper
    {
        public static DataTable GroupList()
        {
            SqlContext Sql = new SqlContext();
            DataTable zTable = Sql.GetData("SELECT * FROM HRM_Specified");
            return zTable;
        }
        public static DataTable DepartmentList(int Spec)
        {
            string SQL = "SELECT* FROM HRM_Departments WHERE SpecKey IN(" + Spec + ")";
            if (Spec == 0)
                SQL = "SELECT* FROM HRM_Departments";
            SqlContext Sql = new SqlContext();
            DataTable zTable = Sql.GetData(SQL);
            return zTable;
        }
        public static DataTable DepartmentList(string Group)
        {
            string SQL = "SELECT* FROM HRM_Departments WHERE DepartmentKey IN(" + Group + ")";
            if (Group == string.Empty)
                SQL = "SELECT* FROM HRM_Departments";
            SqlContext Sql = new SqlContext();
            DataTable zTable = Sql.GetData(SQL);
            return zTable;
        }
    }
}
