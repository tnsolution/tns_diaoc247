﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.HRM
{
    public class ItemOffSearch
    {
        string _Department = "0";
        string _Employee = "0";
        string _FromDate = "0";
        string _ToDate = "0";
        string _Status = "0";

        public string Department { get => _Department; set => _Department = value; }
        public string Employee { get => _Employee; set => _Employee = value; }
        public string FromDate { get => _FromDate; set => _FromDate = value; }
        public string ToDate { get => _ToDate; set => _ToDate = value; }
        public string Status { get => _Status; set => _Status = value; }
    }
}
