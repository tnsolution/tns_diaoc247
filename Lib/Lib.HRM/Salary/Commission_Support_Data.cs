﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Commission_Support_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
CASE Method 
	WHEN 1 THEN N'Tính % doanh số'
	WHEN 2 THEN N'Từng giao dịch'
END AS Method,
CASE TradeType
    WHEN 0 THEN N'Tất cả'
	WHEN 1 THEN N'Trực tiếp'
	WHEN 2 THEN N'Liên kết'
END AS Source,
A.[Money], A.Name, A.[Description], A.Number, 
A.ModifiedBy, A.ModifiedDate, A.AutoKey,
dbo.FNC_GetPostition(A.PositionKey) AS Position,
CASE A.TradeCategory
    WHEN 0 THEN N'Tất cả'
    ELSE dbo.FNC_SysCategoryName(A.TradeCategory) END AS Category
FROM HRM_Commission_Support A WHERE Active = 1";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM HRM_Commission_Support ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Default(int Category, int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM HRM_Commission_Support 
WHERE TradeCategory IN (0, " + Category + ") AND TradeType IN (0," + Type + ") AND Active=1";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //public static DataTable Commision(int TradeKey)
        //{

        //}
    }
}
