﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Commission_Support_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ID = "";
        private int _PositionKey = 0;
        private int _TradeCategory = 0;
        private double _Money = 0;
        private float _Number = 0;
        private int _TradeType = 0;
        private int _Method = 0;
        private string _Name = "";
        private string _Description = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _Message = "";
        private int _Active = 0;
        #endregion

        #region [ Constructor Get Information ]
        public Commission_Support_Info()
        {
        }
        public Commission_Support_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Commission_Support WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    if (zReader["TradeCategory"] != DBNull.Value)
                        _TradeCategory = int.Parse(zReader["TradeCategory"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Number"] != DBNull.Value)
                        _Number = float.Parse(zReader["Number"].ToString());
                    if (zReader["TradeType"] != DBNull.Value)
                        _TradeType = int.Parse(zReader["TradeType"].ToString());
                    if (zReader["Method"] != DBNull.Value)
                        _Method = int.Parse(zReader["Method"].ToString());
                    _Description = zReader["Description"].ToString();
                    _Name = zReader["Name"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }     
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public int TradeCategory
        {
            get { return _TradeCategory; }
            set { _TradeCategory = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public int TradeType
        {
            get { return _TradeType; }
            set { _TradeType = value; }
        }
        public int Method
        {
            get { return _Method; }
            set { _Method = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }

        public float Number
        {
            get
            {
                return _Number;
            }

            set
            {
                _Number = value;
            }
        }

        public int Active { get => _Active; set => _Active = value; }
        public string ID { get => _ID; set => _ID = value; }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Commission_Support ("
        + "ID, PositionKey ,TradeCategory ,Money, Number ,TradeType ,Method, Name ,Description ,ModifiedDate ,ModifiedBy, Active) "
         + " VALUES ( "
         + "@ID, @PositionKey ,@TradeCategory ,@Money, @Number,@TradeType ,@Method, @Name ,@Description ,GETDATE() ,@ModifiedBy, @Active ) "
         + " SELECT AutoKey FROM HRM_Commission_Support WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = _ID;
                zCommand.Parameters.Add("@Active", SqlDbType.Int).Value = _Active;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@TradeCategory", SqlDbType.Int).Value = _TradeCategory;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@TradeType", SqlDbType.Int).Value = _TradeType;
                zCommand.Parameters.Add("@Method", SqlDbType.Int).Value = _Method;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;                
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE HRM_Commission_Support SET ID=@ID,"
                        + " PositionKey = @PositionKey,"
                        + " TradeCategory = @TradeCategory,"
                        + " Money = @Money, Number = @Number,"
                        + " TradeType = @TradeType,"
                        + " Method = @Method, Name = @Name, Active =@Active,"
                        + " Description = @Description,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = _ID;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Active", SqlDbType.Int).Value = _Active;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@TradeCategory", SqlDbType.Int).Value = _TradeCategory;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
                zCommand.Parameters.Add("@TradeType", SqlDbType.Int).Value = _TradeType;
                zCommand.Parameters.Add("@Method", SqlDbType.Int).Value = _Method;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Commission_Support SET Active = 0, ModifiedDate = GETDATE(), ModifiedBy = @ModifiedBy WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
