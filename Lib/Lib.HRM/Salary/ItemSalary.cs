﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Salary
{
    public class ItemSalary
    {
        public int AutoKey;
        public int EmployeeKey;
        public int DepartmentKey;
        public string FromDate;
        public string ToDate;
        public string Description;
        public string TableDetail;//bang chi tiet
        public string Param1;
        public string Param2;
        public string Param3;
        public string Param4;
        public string Param5;
        public string Param51;
        public string Param6;
        public string Param7;
        public string Param8;
        public string Param9;
        public string Param10;
        public string Param11;
        public string Param12;
        public string Param13;
        public string Param14;
        public string Note1;
        public string Note2;
        public string Note3;
        public string Message = "";
    }
}
