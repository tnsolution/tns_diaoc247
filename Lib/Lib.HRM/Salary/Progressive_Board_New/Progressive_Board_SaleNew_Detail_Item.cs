﻿namespace Lib.HRM
{
    public class Progressive_Board_SaleNew_Detail_Item
    {
        public string ParentKey { get; set; } = "";
        public string AutoKey { get; set; } = "";
        public string ProjectKey { get; set; } = "";
        public string FromMoney { get; set; } = "";
        public string ToMoney { get; set; } = "";
        public string Rate { get; set; } = "";
        public string Rank { get; set; } = "";
        public string Description { get; set; } = "";
        public string Category { get; set; } = "";
        public string ModifiedDate { get; set; } = "";
        public string ModifiedBy { get; set; } = "";
    }
}
