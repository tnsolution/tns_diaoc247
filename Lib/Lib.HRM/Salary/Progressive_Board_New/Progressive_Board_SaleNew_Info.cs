﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Progressive_Board_SaleNew_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ProjectKey = 0;
        private string _ProjectName = "";
        private string _MainTable = "";
        private string _ShortTable = "";
        private int _Method1 = 0;
        private string _Method1Name = "";
        private int _Method2 = 0;
        private string _Method2Name = "";
        private int _Active = 0;
        private DateTime _ActiveDate;
        private string _Description = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public string ProjectName
        {
            get { return _ProjectName; }
            set { _ProjectName = value; }
        }
        public string MainTable
        {
            get { return _MainTable; }
            set { _MainTable = value; }
        }
        public string ShortTable
        {
            get { return _ShortTable; }
            set { _ShortTable = value; }
        }
        public int Method1
        {
            get { return _Method1; }
            set { _Method1 = value; }
        }
        public string Method1Name
        {
            get { return _Method1Name; }
            set { _Method1Name = value; }
        }
        public int Method2
        {
            get { return _Method2; }
            set { _Method2 = value; }
        }
        public string Method2Name
        {
            get { return _Method2Name; }
            set { _Method2Name = value; }
        }
        public int Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public DateTime ActiveDate
        {
            get { return _ActiveDate; }
            set { _ActiveDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Progressive_Board_SaleNew_Info()
        {
        }
        public Progressive_Board_SaleNew_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Progressive_Board_SaleNew WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    _ProjectName = zReader["ProjectName"].ToString();
                    _MainTable = zReader["MainTable"].ToString();
                    _ShortTable = zReader["ShortTable"].ToString();
                    if (zReader["Method1"] != DBNull.Value)
                        _Method1 = int.Parse(zReader["Method1"].ToString());
                    _Method1Name = zReader["Method1Name"].ToString();
                    if (zReader["Method2"] != DBNull.Value)
                        _Method2 = int.Parse(zReader["Method2"].ToString());
                    _Method2Name = zReader["Method2Name"].ToString();
                    if (zReader["Active"] != DBNull.Value)
                        _Active = int.Parse(zReader["Active"].ToString());
                    if (zReader["ActiveDate"] != DBNull.Value)
                        _ActiveDate = (DateTime)zReader["ActiveDate"];
                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Progressive_Board_SaleNew ("
        + " ProjectKey ,ProjectName ,MainTable ,ShortTable ,Method1 ,Method1Name ,Method2 ,Method2Name ,Active ,ActiveDate ,Description ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@ProjectKey ,@ProjectName ,@MainTable ,@ShortTable ,@Method1 ,@Method1Name ,@Method2 ,@Method2Name ,@Active ,@ActiveDate ,@Description ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) "
         + " SELECT AutoKey FROM HRM_Progressive_Board_SaleNew WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = _ProjectName;
                zCommand.Parameters.Add("@MainTable", SqlDbType.NVarChar).Value = _MainTable;
                zCommand.Parameters.Add("@ShortTable", SqlDbType.NVarChar).Value = _ShortTable;
                zCommand.Parameters.Add("@Method1", SqlDbType.Int).Value = _Method1;
                zCommand.Parameters.Add("@Method1Name", SqlDbType.NVarChar).Value = _Method1Name;
                zCommand.Parameters.Add("@Method2", SqlDbType.Int).Value = _Method2;
                zCommand.Parameters.Add("@Method2Name", SqlDbType.NVarChar).Value = _Method2Name;
                zCommand.Parameters.Add("@Active", SqlDbType.Int).Value = _Active;
                if (_ActiveDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ActiveDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ActiveDate", SqlDbType.DateTime).Value = _ActiveDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE HRM_Progressive_Board_SaleNew SET "
                        + " ProjectKey = @ProjectKey,"
                        + " ProjectName = @ProjectName,"
                        + " MainTable = @MainTable,"
                        + " ShortTable = @ShortTable,"
                        + " Method1 = @Method1,"
                        + " Method1Name = @Method1Name,"
                        + " Method2 = @Method2,"
                        + " Method2Name = @Method2Name,"
                        + " Active = @Active,"
                        + " ActiveDate = @ActiveDate,"
                        + " Description = @Description,"                       
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = _ProjectName;
                zCommand.Parameters.Add("@MainTable", SqlDbType.NVarChar).Value = _MainTable;
                zCommand.Parameters.Add("@ShortTable", SqlDbType.NVarChar).Value = _ShortTable;
                zCommand.Parameters.Add("@Method1", SqlDbType.Int).Value = _Method1;
                zCommand.Parameters.Add("@Method1Name", SqlDbType.NVarChar).Value = _Method1Name;
                zCommand.Parameters.Add("@Method2", SqlDbType.Int).Value = _Method2;
                zCommand.Parameters.Add("@Method2Name", SqlDbType.NVarChar).Value = _Method2Name;
                zCommand.Parameters.Add("@Active", SqlDbType.Int).Value = _Active;
                if (_ActiveDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ActiveDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ActiveDate", SqlDbType.DateTime).Value = _ActiveDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM HRM_Progressive_Board_SaleNew WHERE AutoKey = @AutoKey 
DELETE FROM HRM_Progressive_Board_SaleNew_Detail WHERE ParentKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
