﻿namespace Lib.HRM
{
    public class Progressive_Board_SaleNew_Item
    {
        public string AutoKey { get; set; } = "";
        public string ProjectKey { get; set; } = "";
        public string MainTable { get; set; } = "";
        public string ShortTable { get; set; } = "";
        public string Method1 { get; set; } = "";
        public string Method1Name { get; set; } = "";
        public string Method2 { get; set; } = "";
        public string Method2Name { get; set; } = "";
        public string CreatedDate { get; set; } = "";
        public string CreatedBy { get; set; } = "";
        public string CreatedName { get; set; } = "";
        public string ModifiedDate { get; set; } = "";
        public string ModifiedBy { get; set; } = "";
        public string ModifiedName { get; set; } = "";
    }
}
