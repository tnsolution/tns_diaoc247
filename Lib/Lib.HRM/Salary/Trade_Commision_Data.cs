﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace TN_WinApp.FNC_Transaction_Commision
{
public class Transaction_Commision_Data
{
public static DataTable List()
{
DataTable zTable = new DataTable();
string zSQL = "SELECT  * FROM FNC_Transaction_Commision " ;
string zConnectionString = ConnectDataBase.ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close();
}
catch (Exception ex)
{
string zstrMessage = ex.ToString();
}
return zTable;
}
public static DataTable List(int Amount)
{
DataTable zTable = new DataTable();
string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM FNC_Transaction_Commision ";
string zConnectionString = ConnectDataBase.ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close();
}
catch (Exception ex)
{
string zstrMessage = ex.ToString();
}
return zTable;
}
}
}
