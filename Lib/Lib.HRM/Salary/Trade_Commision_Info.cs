﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace TN_WinApp.FNC_Transaction_Commision
{
public class Transaction_Commision_Info
{
 
#region [ Field Name ]
private int _AutoKey = 0;
private int _TradeKey = 0;
private int _CommisionKey = 0;
private int _EmployeeKey = 0;
private double _Money = 0;
private float _Number;
private int _TradeType = 0;
private int _TradeCategory = 0;
private int _Method = 0;
private string _Description = "";
private DateTime _CreatedDate ;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedDate ;
private string _ModifiedBy = "";
private string _ModifiedName = "";
private string _Message = "";
#endregion
 
#region [ Constructor Get Information ]
public Transaction_Commision_Info()
{
}
public Transaction_Commision_Info(int AutoKey)
{
string zSQL = "SELECT * FROM FNC_Transaction_Commision WHERE AutoKey = @AutoKey"; 
string zConnectionString = ConnectDataBase.ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["AutoKey"] != DBNull.Value)
_AutoKey = int.Parse(zReader["AutoKey"].ToString());
if (zReader["TradeKey"] != DBNull.Value)
_TradeKey = int.Parse(zReader["TradeKey"].ToString());
if (zReader["CommisionKey"] != DBNull.Value)
_CommisionKey = int.Parse(zReader["CommisionKey"].ToString());
if (zReader["EmployeeKey"] != DBNull.Value)
_EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
if (zReader["Money"] != DBNull.Value)
_Money = double.Parse(zReader["Money"].ToString());
if (zReader["Number"] != DBNull.Value)
_Number = float.Parse(zReader["Number"].ToString());
if (zReader["TradeType"] != DBNull.Value)
_TradeType = int.Parse(zReader["TradeType"].ToString());
if (zReader["TradeCategory"] != DBNull.Value)
_TradeCategory = int.Parse(zReader["TradeCategory"].ToString());
if (zReader["Method"] != DBNull.Value)
_Method = int.Parse(zReader["Method"].ToString());
_Description = zReader["Description"].ToString();
if (zReader["CreatedDate"] != DBNull.Value)
_CreatedDate = (DateTime)zReader["CreatedDate"];
_CreatedBy = zReader["CreatedBy"].ToString();
_CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedDate"] != DBNull.Value)
_ModifiedDate = (DateTime)zReader["ModifiedDate"];
_ModifiedBy = zReader["ModifiedBy"].ToString();
_ModifiedName = zReader["ModifiedName"].ToString();
 }zReader.Close();zCommand.Dispose();}catch (Exception Err){_Message = Err.ToString();}finally{zConnect.Close();}}
public Transaction_Commision_Info( )
{
string zSQL = "SELECT * FROM FNC_Transaction_Commision WHERE  = @"; 
string zConnectionString = ConnectDataBase.ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@", ).Value = ;
SqlDataReader zReader = zCommand.ExecuteReader();
if (zReader.HasRows)
{
zReader.Read();
if (zReader["AutoKey"] != DBNull.Value)
_AutoKey = int.Parse(zReader["AutoKey"].ToString());
if (zReader["TradeKey"] != DBNull.Value)
_TradeKey = int.Parse(zReader["TradeKey"].ToString());
if (zReader["CommisionKey"] != DBNull.Value)
_CommisionKey = int.Parse(zReader["CommisionKey"].ToString());
if (zReader["EmployeeKey"] != DBNull.Value)
_EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
if (zReader["Money"] != DBNull.Value)
_Money = double.Parse(zReader["Money"].ToString());
if (zReader["Number"] != DBNull.Value)
_Number = float.Parse(zReader["Number"].ToString());
if (zReader["TradeType"] != DBNull.Value)
_TradeType = int.Parse(zReader["TradeType"].ToString());
if (zReader["TradeCategory"] != DBNull.Value)
_TradeCategory = int.Parse(zReader["TradeCategory"].ToString());
if (zReader["Method"] != DBNull.Value)
_Method = int.Parse(zReader["Method"].ToString());
_Description = zReader["Description"].ToString();
if (zReader["CreatedDate"] != DBNull.Value)
_CreatedDate = (DateTime)zReader["CreatedDate"];
_CreatedBy = zReader["CreatedBy"].ToString();
_CreatedName = zReader["CreatedName"].ToString();
if (zReader["ModifiedDate"] != DBNull.Value)
_ModifiedDate = (DateTime)zReader["ModifiedDate"];
_ModifiedBy = zReader["ModifiedBy"].ToString();
_ModifiedName = zReader["ModifiedName"].ToString();
 }zReader.Close();zCommand.Dispose();}catch (Exception Err){_Message = Err.ToString();}finally{zConnect.Close();}}
#endregion
 
#region [ Properties ]
public int AutoKey
{
get { return _AutoKey; }
set { _AutoKey = value; }
}
public int TradeKey
{
get { return _TradeKey; }
set { _TradeKey = value; }
}
public int CommisionKey
{
get { return _CommisionKey; }
set { _CommisionKey = value; }
}
public int EmployeeKey
{
get { return _EmployeeKey; }
set { _EmployeeKey = value; }
}
public double Money
{
get { return _Money; }
set { _Money = value; }
}
public float Number
{
get { return _Number; }
set { _Number = value; }
}
public int TradeType
{
get { return _TradeType; }
set { _TradeType = value; }
}
public int TradeCategory
{
get { return _TradeCategory; }
set { _TradeCategory = value; }
}
public int Method
{
get { return _Method; }
set { _Method = value; }
}
public string Description
{
get { return _Description; }
set { _Description = value; }
}
public DateTime CreatedDate
{
get { return _CreatedDate; }
set { _CreatedDate = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedDate
{
get { return _ModifiedDate; }
set { _ModifiedDate = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
public string Message 
{
get { return _Message; }
set { _Message = value; }
}
#endregion
 
#region [ Constructor Update Information ]
 
public string Create()
{
//---------- String SQL Access Database ---------------
    string zSQL = "INSERT INTO FNC_Transaction_Commision (" 
+ " TradeKey ,CommisionKey ,EmployeeKey ,Money ,Number ,TradeType ,TradeCategory ,Method ,Description ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
 + " VALUES ( "
 + "@TradeKey ,@CommisionKey ,@EmployeeKey ,@Money ,@Number ,@TradeType ,@TradeCategory ,@Method ,@Description ,@CreatedDate ,@CreatedBy ,@CreatedName ,@ModifiedDate ,@ModifiedBy ,@ModifiedName ) "
 + " SELECT AutoKey FROM FNC_Transaction_Commision WHERE AutoKey = SCOPE_IDENTITY() ";
string zResult = ""; 
string zConnectionString = ConnectDataBase.ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
zCommand.Parameters.Add("@TradeKey", SqlDbType.Int).Value = _TradeKey;
zCommand.Parameters.Add("@CommisionKey", SqlDbType.Int).Value = _CommisionKey;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
zCommand.Parameters.Add("@TradeType", SqlDbType.Int).Value = _TradeType;
zCommand.Parameters.Add("@TradeCategory", SqlDbType.Int).Value = _TradeCategory;
zCommand.Parameters.Add("@Method", SqlDbType.Int).Value = _Method;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
if (_CreatedDate.Year == 0001) 
zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
if (_ModifiedDate.Year == 0001) 
zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
_AutoKey =  int.Parse(zCommand.ExecuteScalar().ToString());
zCommand.Dispose();
}
catch (Exception Err)
{
 _Message = Err.ToString();
}
 finally
 {
     zConnect.Close();
  }
  return zResult;
  }

 
public string Update() 
{ 
string zSQL = "UPDATE FNC_Transaction_Commision SET " 
            + " TradeKey = @TradeKey,"
            + " CommisionKey = @CommisionKey,"
            + " EmployeeKey = @EmployeeKey,"
            + " Money = @Money,"
            + " Number = @Number,"
            + " TradeType = @TradeType,"
            + " TradeCategory = @TradeCategory,"
            + " Method = @Method,"
            + " Description = @Description,"
            + " CreatedDate = @CreatedDate,"
            + " CreatedBy = @CreatedBy,"
            + " CreatedName = @CreatedName,"
            + " ModifiedDate = @ModifiedDate,"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
           + " WHERE AutoKey = @AutoKey";
string zResult = ""; 
string zConnectionString = ConnectDataBase.ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.CommandType = CommandType.Text;
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
zCommand.Parameters.Add("@TradeKey", SqlDbType.Int).Value = _TradeKey;
zCommand.Parameters.Add("@CommisionKey", SqlDbType.Int).Value = _CommisionKey;
zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
zCommand.Parameters.Add("@Number", SqlDbType.Float).Value = _Number;
zCommand.Parameters.Add("@TradeType", SqlDbType.Int).Value = _TradeType;
zCommand.Parameters.Add("@TradeCategory", SqlDbType.Int).Value = _TradeCategory;
zCommand.Parameters.Add("@Method", SqlDbType.Int).Value = _Method;
zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
if (_CreatedDate.Year == 0001) 
zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
if (_ModifiedDate.Year == 0001) 
zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
else
zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
}
catch (Exception Err)
{
 _Message = Err.ToString();
}
 finally
 {
     zConnect.Close();
  }
  return zResult;
  }

 
public string Save()
{
string zResult;
if (_AutoKey == 0)
zResult = Create();
else
zResult = Update();
return zResult;
}
public string Delete()
{
string zResult = "";
//---------- String SQL Access Database ---------------
string zSQL = "DELETE FROM FNC_Transaction_Commision WHERE AutoKey = @AutoKey";
string zConnectionString = ConnectDataBase.ConnectionString;
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
try
{
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
zResult = zCommand.ExecuteNonQuery().ToString();
zCommand.Dispose();
}
catch (Exception Err)
{
_Message = Err.ToString();
}
finally
{
zConnect.Close();
}
return zResult;
}
#endregion
}
}
