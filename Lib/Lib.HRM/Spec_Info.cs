﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.HRM
{
    public class Spec_Info
    {
        #region [ Field Name ]
        private int _SpecKey = 0;
        private string _SpecName = "";      
        private string _Description = "";      
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _SpecKey; }
            set { _SpecKey = value; }
        }
       
        public string SpecName
        {
            get { return _SpecName; }
            set { _SpecName = value; }
        }
   
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
    
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Spec_Info()
        {
        }
        public Spec_Info(int SpecKey)
        {
            string zSQL = "SELECT * FROM HRM_Specified WHERE SpecKey = @SpecKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SpecKey", SqlDbType.Int).Value = SpecKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["SpecKey"] != DBNull.Value)
                        _SpecKey = int.Parse(zReader["SpecKey"].ToString());
                    _SpecName = zReader["SpecName"].ToString();                   
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Specified ("
        + " SpecName, Description  ) "
         + " VALUES ( "
         + "@SpecName, @Description) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
               
                zCommand.Parameters.Add("@SpecName", SqlDbType.NVarChar).Value = _SpecName;              
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
             
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Specified SET "                      
                        + " Description = @Description,"
                        + " SpecName = @SpecName"                     
                       + " WHERE SpecKey = @SpecKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SpecKey", SqlDbType.Int).Value = _SpecKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@SpecName", SqlDbType.NVarChar).Value = _SpecName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_SpecKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Specified WHERE SpecKey = @SpecKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SpecKey", SqlDbType.Int).Value = _SpecKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
                
    }
}
