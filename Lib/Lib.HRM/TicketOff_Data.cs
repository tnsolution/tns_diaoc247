﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class TicketOff_Data
    {
        public static int CoPhepNam(int MaChucVu)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT COUNT(*) FROM HRM_Positions WHERE " + MaChucVu + " NOT IN (5,6)").ToInt();
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_TicketOff ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Department, int Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, 
ApproveBy + ' ' +  CASE Approve
WHEN 1 THEN N'Đã duyệt'
WHEN 2 THEN N'Không duyệt'
WHEN 0 THEN N'Chưa xử lý'
END AS [Status],
dbo.FNC_GetStepTicket(TicketKey) AS [Current],
dbo.FNC_GetStepTicketStatus(TicketKey) AS [CurrentStatus],
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS DepartmentName
FROM HRM_TicketOff WHERE 1=1";
            if (Department != 0)
            {
                zSQL += " AND DepartmentKey = @Department";
            }

            if (Employee != 0)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }

            zSQL += " ORDER BY TicketKey DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, 
ApproveBy + ' ' +  CASE Approve
WHEN 1 THEN N'Đã duyệt'
WHEN 2 THEN N'Không duyệt'
WHEN 0 THEN N'Chưa xử lý'
END AS [Status],
dbo.FNC_GetStepTicket(TicketKey) AS [Current],
dbo.FNC_GetStepTicketStatus(TicketKey) AS [CurrentStatus],
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS DepartmentName
FROM HRM_TicketOff WHERE 1=1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND (FromDate >= @FromDate AND ToDate <= @ToDate)";
            }

            if (Department != 0)
            {
                zSQL += " AND DepartmentKey = @Department";
            }

            if (Employee != 0)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }

            zSQL += " ORDER BY TicketKey DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Department, int Employee, DateTime FromDate, DateTime ToDate, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, 
ApproveBy + ' ' +  CASE Approve
WHEN 1 THEN N'Đã duyệt'
WHEN 2 THEN N'Không duyệt'
WHEN 0 THEN N'Chưa xử lý'
END AS [Status],
dbo.FNC_GetStepTicket(TicketKey) AS [Current],
dbo.FNC_GetStepTicketStatus(TicketKey) AS [CurrentStatus],
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS DepartmentName
FROM HRM_TicketOff WHERE 1=1";
            switch (Status)
            {
                case 0:
                    zSQL += " AND Approve = 0";
                    break;
                case 1:
                    zSQL += " AND Approve = 1";
                    break;
                case 2:
                    zSQL += " AND Approve = 2";
                    break;
                case 3:
                    zSQL += " AND Approve <> 0";
                    break;
                default:
                    break;
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND (FromDate >= @FromDate AND ToDate <= @ToDate)";
            }

            if (Department != 0)
            {
                zSQL += " AND DepartmentKey = @Department";
            }

            if (Employee != 0)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }

            zSQL += " ORDER BY TicketKey DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(string Department, int Employee, DateTime FromDate, DateTime ToDate, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, 
ApproveBy + ' ' +  CASE Approve
WHEN 1 THEN N'Đã duyệt'
WHEN 2 THEN N'Không duyệt'
WHEN 0 THEN N'Chưa xử lý'
END AS [Status],
dbo.FNC_GetStepTicket(TicketKey) AS [Current],
dbo.FNC_GetStepTicketStatus(TicketKey) AS [CurrentStatus],
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS DepartmentName
FROM HRM_TicketOff WHERE 1=1";
            switch (Status)
            {
                case 0:
                    zSQL += " AND Approve = 0";
                    break;
                case 1:
                    zSQL += " AND Approve = 1";
                    break;
                case 2:
                    zSQL += " AND Approve = 2";
                    break;
                case 3:
                    zSQL += " AND Approve <> 0";
                    break;
                default:
                    break;
            }
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND (FromDate >= @FromDate AND ToDate <= @ToDate)";
            }

            if (Department != string.Empty)
            {
                zSQL += " AND DepartmentKey IN(" + Department + ")";
            }

            if (Employee != 0)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }

            zSQL += " ORDER BY TicketKey DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(string Department, int Employee, DateTime FromDate, DateTime ToDate, int Status, int Confirm)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *, 
ApproveBy + ' ' +  CASE Approve
WHEN 1 THEN N'Đã duyệt'
WHEN 2 THEN N'Không duyệt'
WHEN 0 THEN N'Chưa xử lý'
END AS [Status],
dbo.FNC_GetStepTicket(TicketKey) AS [Current],
dbo.FNC_GetStepTicketStatus(TicketKey) AS [CurrentStatus],
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS DepartmentName
FROM HRM_TicketOff WHERE 1=1";
            switch (Status)
            {
                case 0:
                    zSQL += " AND Approve = 0";
                    break;
                case 1:
                    zSQL += " AND Approve = 1";
                    break;
                case 2:
                    zSQL += " AND Approve = 2";
                    break;
                case 3:
                    zSQL += " AND Approve <> 0";
                    break;
                default:
                    break;
            }
            if (Confirm >= 0)
            {
                zSQL += " AND Confirmed=" + Confirm;
            }

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                zSQL += " AND FromDate BETWEEN @FromDate AND @ToDate";
                zSQL += " OR ToDate BETWEEN @FromDate AND @ToDate";
            }

            if (Department != string.Empty && Department != "0")
            {
                zSQL += " AND DepartmentKey IN(" + Department + ")";
            }

            if (Employee != 0)
            {
                zSQL += " AND EmployeeKey = @Employee";
            }

            zSQL += " ORDER BY TicketKey DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                    ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
