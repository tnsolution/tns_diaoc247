﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class TicketOff_Info
    {
        #region [ Field Name ]
        private int _ConfirmBy = 0;
        private int _Confirmed = 0;
        private string _OffLeft = "";
        private DateTime _ConfirmDate;
        private int _TicketKey = 0;
        private string _Contents = "";
        private int _CategoryKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private DateTime _TicketDate;
        private float _NumberDay;
        private int _Approve = 0;
        private string _ApproveBy = "";
        private string _Description = "";
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _IsView = 0;
        private string _Priority = "";
        private DateTime _CreatedDateTime;
        private string _CreatedBy = "";
        private DateTime _ModifiedDateTime;
        private string _ModifiedBy = "";
        private string _Message = "";
        private int _ManagerKey = 0;
        private string _EmployeeName = "";
        private string _DepartmentName = "";
        private string _ManagerName = "";
        private string _Step = "";
        #endregion
        #region [ Properties ]
        public int TicketKey
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
        public string Contents
        {
            get { return _Contents; }
            set { _Contents = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public float NumberDay
        {
            get { return _NumberDay; }
            set { _NumberDay = value; }
        }
        public int Approve
        {
            get { return _Approve; }
            set { _Approve = value; }
        }
        public string ApproveBy
        {
            get { return _ApproveBy; }
            set { _ApproveBy = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int IsView
        {
            get { return _IsView; }
            set { _IsView = value; }
        }
        public string Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }
        public DateTime CreatedDateTime
        {
            get { return _CreatedDateTime; }
            set { _CreatedDateTime = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime ModifiedDateTime
        {
            get { return _ModifiedDateTime; }
            set { _ModifiedDateTime = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int ManagerKey
        {
            get
            {
                return _ManagerKey;
            }

            set
            {
                _ManagerKey = value;
            }
        }

        public string ManagerName
        {
            get
            {
                return _ManagerName;
            }

            set
            {
                _ManagerName = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }

            set
            {
                _DepartmentName = value;
            }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public DateTime TicketDate
        {
            get
            {
                return _TicketDate;
            }

            set
            {
                _TicketDate = value;
            }
        }

        public string Step
        {
            get
            {
                return _Step;
            }

            set
            {
                _Step = value;
            }
        }

        public DateTime ConfirmDate
        {
            get
            {
                return _ConfirmDate;
            }

            set
            {
                _ConfirmDate = value;
            }
        }

        public int Confirmed
        {
            get
            {
                return _Confirmed;
            }

            set
            {
                _Confirmed = value;
            }
        }

        public int ConfirmBy
        {
            get
            {
                return _ConfirmBy;
            }

            set
            {
                _ConfirmBy = value;
            }
        }

        public string OffLeft { get => _OffLeft; set => _OffLeft = value; }
        #endregion
        #region [ Constructor Get Information ]
        public TicketOff_Info()
        {
        }
        public TicketOff_Info(int TicketKey)
        {
            string zSQL = @"SELECT *, 
dbo.FNC_GetNameEmployee(EmployeeKey) EmployeeName,
dbo.FNC_GetNameEmployee(ManagerKey) ManagerName,
dbo.FNC_GetDepartmentName(DepartmentKey) DepartmentName
FROM HRM_TicketOff WHERE TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _OffLeft = zReader["OffLeft"].ToString();
                    if (zReader["Confirmed"] != DBNull.Value)
                        _Confirmed = int.Parse(zReader["Confirmed"].ToString());
                    if (zReader["ConfirmBy"] != DBNull.Value)
                        _ConfirmBy = int.Parse(zReader["ConfirmBy"].ToString());
                    if (zReader["ConfirmDate"] != DBNull.Value)
                        _ConfirmDate =(DateTime)zReader["ConfirmDate"];

                    EmployeeName = zReader["EmployeeName"].ToString();
                    ManagerName = zReader["ManagerName"].ToString();
                    DepartmentName = zReader["DepartmentName"].ToString();
                    _Step = zReader["Step"].ToString();
                    if (zReader["TicketKey"] != DBNull.Value)
                        _TicketKey = int.Parse(zReader["TicketKey"].ToString());
                    _Contents = zReader["Contents"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    if (zReader["NumberDay"] != DBNull.Value)
                        _NumberDay = float.Parse(zReader["NumberDay"].ToString());
                    if (zReader["Approve"] != DBNull.Value)
                        _Approve = int.Parse(zReader["Approve"].ToString());
                    _ApproveBy = zReader["ApproveBy"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["IsView"] != DBNull.Value)
                        _IsView = int.Parse(zReader["IsView"].ToString());
                    _Priority = zReader["Priority"].ToString();
                    if (zReader["TicketDate"] != DBNull.Value)
                        _TicketDate = (DateTime)zReader["TicketDate"];
                    if (zReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)zReader["CreatedDateTime"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)zReader["ModifiedDateTime"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_TicketOff ("
         + @" Contents ,CategoryKey ,FromDate ,ToDate,TicketDate ,NumberDay ,Approve ,ApproveBy ,Description ,
         EmployeeKey, ManagerKey ,DepartmentKey ,IsView ,Priority ,ConfirmBy, ConfirmDate, Confirmed, OffLeft,
         CreatedDateTime ,CreatedBy ,ModifiedDateTime ,ModifiedBy, Step ) "
         + " VALUES ( "
         + @"@Contents ,@CategoryKey ,@FromDate ,@ToDate,@TicketDate ,@NumberDay ,@Approve ,@ApproveBy ,@Description ,
         @EmployeeKey, @ManagerKey ,@DepartmentKey ,@IsView ,@Priority ,@ConfirmBy,@ConfirmDate,@Confirmed, @OffLeft,
         GETDATE() ,@CreatedBy ,GETDATE() ,@ModifiedBy,@Step ) "
         + " SELECT TicketKey FROM HRM_TicketOff WHERE TicketKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@ConfirmBy", SqlDbType.Int).Value = _ConfirmBy;
                zCommand.Parameters.Add("@Confirmed", SqlDbType.Int).Value = _Confirmed;

                zCommand.Parameters.Add("@OffLeft", SqlDbType.NVarChar).Value = _OffLeft;

                if (_ConfirmDate.Year == 0001)
                    zCommand.Parameters.Add("@ConfirmDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ConfirmDate", SqlDbType.DateTime).Value = _FromDate;

                if (_FromDate.Year == 0001)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (TicketDate.Year == 0001)
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = _TicketDate;
                if (_ToDate.Year == 0001)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;

                zCommand.Parameters.Add("@NumberDay", SqlDbType.Float).Value = _NumberDay;
                zCommand.Parameters.Add("@Approve", SqlDbType.Int).Value = _Approve;
                zCommand.Parameters.Add("@ApproveBy", SqlDbType.NVarChar).Value = _ApproveBy;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = ManagerKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@IsView", SqlDbType.Int).Value = _IsView;
                zCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = _Priority;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@Step", SqlDbType.NVarChar).Value = _Step;
                _TicketKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateSend()
        {
            string zSQL = "UPDATE HRM_TicketOff SET "
                         + "IsView=@IsView"
                         + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@IsView", SqlDbType.Int).Value = _IsView;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateConfirm()
        {
            string zSQL = "UPDATE HRM_TicketOff SET "
                         + "Confirmed=1, ConfirmBy =@ConfirmBy, ConfirmDate=GETDATE()"
                         + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ConfirmBy", SqlDbType.Int).Value = ConfirmBy;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_TicketOff SET "
                        + " Contents = @Contents,"
                        + " CategoryKey = @CategoryKey, "
                        + " FromDate = @FromDate, TicketDate = @TicketDate,"
                        + " ToDate = @ToDate,"
                        + " NumberDay = @NumberDay,"
                        + " Approve = @Approve,"
                        + " ApproveBy = @ApproveBy,"
                        + " Description = @Description,"
                        + " EmployeeKey = @EmployeeKey, ManagerKey = @ManagerKey, Step = @Step,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " IsView = @IsView,"
                        + " Priority = @Priority,"
                        + " ModifiedDateTime = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                if (_FromDate.Year == 0001)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (TicketDate.Year == 0001)
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = _TicketDate;
                if (_ToDate.Year == 0001)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@NumberDay", SqlDbType.Float).Value = _NumberDay;
                zCommand.Parameters.Add("@Approve", SqlDbType.Int).Value = _Approve;
                zCommand.Parameters.Add("@ApproveBy", SqlDbType.NVarChar).Value = _ApproveBy;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@IsView", SqlDbType.Int).Value = _IsView;
                zCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = _Priority;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@Step", SqlDbType.NVarChar).Value = _Step;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateApprove()
        {
            string zSQL = "UPDATE HRM_TicketOff SET "
                        + "Approve=@Approve, ApproveBy=@ApproveBy, ApproveDate =GETDATE()"
                        + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Approve", SqlDbType.Int).Value = _Approve;
                zCommand.Parameters.Add("@ApproveBy", SqlDbType.NVarChar).Value = _ApproveBy;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_TicketKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_TicketOff WHERE TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
