﻿using System;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;

namespace Lib.JOB
{
    public class Projects_Data
    {
        //  --------List--------
        public static DataTable List()
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ProjectKey, A.ProjectID, A.ProjectName, A.ProjectContent, B.CustomerName
FROM JOB_Projects A
LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey";

            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand command = new SqlCommand(zSQL, zConnection);
                command.CommandType = CommandType.Text;


                SqlDataAdapter zDa = new SqlDataAdapter(command);
                zDa.Fill(zTable);
                command.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// Lấy các dự án của 1 khách hàng
        /// </summary>
        /// <param name="CustomerKey">Key khách hàng</param>
        /// <returns></returns>
        public static DataTable GetAll(int CustomerKey)
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @" 
SELECT A.ProjectKey, A.ProjectID, A.ProjectName, A.ProjectContent, 
B.CustomerName, A.StartDate, A.DueDate, C.StatusName
FROM JOB_Projects A 
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey
WHERE CustomerKey = @CustomerKey";
            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCommand = new SqlCommand(zSQL, zConnection);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;

                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// Lấy dự án theo năm và tình trạng có phân trang
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="Year"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        public static DataTable GetAll(string Status, string Year, int PageSize, int PageNumber)
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (@PageSize*(@PageNumber-1)) as Q, A.ProjectKey, A.ProjectID, 
A.ProjectName, A.ProjectContent, B.CustomerName, A.StatusKey, C.StatusName
FROM JOB_Projects A
LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey";

            if (Status.Length > 0)
                zSQL += " WHERE A.StatusKey IN (" + Status + ")";
            if (Year != string.Empty)
                zSQL += " AND (Year(A.StartDate) = @Year OR YEAR(A.DueDate) = @Year)";
            zSQL += @" ORDER BY A.CreatedDateTime DESC";
            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCommand = new SqlCommand(zSQL, zConnection);
                zCommand.Parameters.Add("@Year", SqlDbType.NVarChar).Value = Year;
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                zCommand.CommandType = CommandType.Text;
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(PageSize * PageNumber - PageSize, PageSize, zTable);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// Lấy các dự án đã được tham gia
        /// </summary>
        /// <param name="EmployeeKey"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public static DataTable GetAll(int EmployeeKey, string Status)
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @" 
SELECT A.ProjectKey, A.ProjectID, A.ProjectName, A.ProjectContent, B.CustomerName
FROM JOB_Projects A
LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey
LEFT JOIN JOB_Project_Permission D ON D.ProjectKey = A.ProjectKey
WHERE D.EmployeeKey = @EmployeeKey";

            if (Status.Length > 0)
                zSQL += " AND A.StatusKey IN (" + Status + ")";
            zSQL += " ORDER BY B.CreatedDateTime DESC";

            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCommand = new SqlCommand(zSQL, zConnection);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="Search"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public static DataTable Search(string Status, string Search, string Year)
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT '' as Q, A.ProjectKey, A.ProjectID, 
A.ProjectName, A.ProjectContent, B.CustomerName, A.StatusKey, C.StatusName
FROM JOB_Projects A
LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey";

            if (Status.Length > 0)
                zSQL += " WHERE A.StatusKey IN (" + Status + ")";
            if (Search != string.Empty)
                zSQL += " AND dbo.RemoveVN(A.ProjectName) LIKE @Search";
            if (Year != string.Empty)
                zSQL += " AND (Year(A.StartDate) = @Year OR YEAR(A.DueDate) = @Year)";
            zSQL += @" ORDER BY A.ProjectID DESC, A.CreatedDateTime DESC";
            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCommand = new SqlCommand(zSQL, zConnection);
                zCommand.Parameters.Add("@Year", SqlDbType.NVarChar).Value = Year;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.CommandType = CommandType.Text;
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }

        //  --------Report--------
        /// <summary>
        /// Báo cáo tình hình dự án
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="Year"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        public static DataTable ReportAll(string Status, string Year, int PageSize, int PageNumber)
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT (@PageSize*(@PageNumber-1)) as Q, A.ProjectKey, A.ProjectID, 
A.ProjectName, A.ProjectContent, A.StatusKey, C.StatusName,
A.Priority, A.StartDate, A.DueDate,
A.Complete, dbo.CalTime(A.ProjectKey) TimeLeft,
B.CustomerName
FROM JOB_Projects A
LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey";
            if (Status.Length > 0)
                zSQL += " WHERE A.StatusKey IN (" + Status + ")";
            if (Year != string.Empty)
                zSQL += " AND (Year(A.StartDate) = @Year OR YEAR(A.DueDate) = @Year)";
            zSQL += @" ORDER BY A.CreatedDateTime DESC";
            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCommand = new SqlCommand(zSQL, zConnection);
                zCommand.Parameters.Add("@Year", SqlDbType.NVarChar).Value = Year;
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                zCommand.Parameters.Add("@PageNumber", SqlDbType.Int).Value = PageNumber;
                zCommand.CommandType = CommandType.Text;
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(PageSize * PageNumber - PageSize, PageSize, zTable);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }



        public static int Count(string StatusKey, int Year)
        {
            int zResult = 0;

            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT Count(*) FROM JOB_Projects WHERE StatusKey IN (" + StatusKey + ")";

            if (Year != 0)
                zSQL += " AND Year(StartDate) = @Year OR YEAR(DueDate) = @Year";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;

                string strAmount = zCommand.ExecuteScalar().ToString();
                int.TryParse(zCommand.ExecuteScalar().ToString(), out zResult);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                string _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static int Count(string Status, string Search, string Year)
        {
            int zResult = 0;

            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT Count(*) FROM JOB_Projects A";
            if (Status.Length > 0)
                zSQL += " WHERE StatusKey IN (" + Status + ")";
            if (Search != string.Empty)
                zSQL += " AND dbo.RemoveVN(A.ProjectName) LIKE @Search";
            if (Year != string.Empty)
                zSQL += " AND Year(A.StartDate) = @Year OR YEAR(A.DueDate) = @Year";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StatusKey", SqlDbType.NVarChar).Value = Status;
                zCommand.Parameters.Add("@Year", SqlDbType.NVarChar).Value = Year;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                string strAmount = zCommand.ExecuteScalar().ToString();
                int.TryParse(zCommand.ExecuteScalar().ToString(), out zResult);
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                string _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static int CheckExistProject(int ProjectKey)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(*) FROM PRO_Tasks WHERE ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;

            string strAmount = zCommand.ExecuteScalar().ToString();
            int.TryParse(zCommand.ExecuteScalar().ToString(), out zResult);
            zCommand.Dispose();

            return zResult;
        }
        public static string GetProjectName(int ProjectKey)
        {
            string zResult = "";
            string zSQL = "SELECT ProjectName FROM JOB_Projects WHERE ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.CommandType = CommandType.Text;
            zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;

            string strAmount = zCommand.ExecuteScalar().ToString();
            zResult = zCommand.ExecuteScalar().ToString();
            zCommand.Dispose();

            return zResult;
        }


        public static DataTable Project_Employee_List(int Key)
        {
            string Result = "";
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B.FirstName + ' ' +B.LastName AS EmployeeName, C.LevelName
FROM dbo.JOB_Project_Permission A
LEFT JOIN dbo.EMPL_Employees B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN dbo.EMPL_Level C ON B.LevelKey = C.LevelKey
WHERE A.ProjectKey = @Key";
            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCMD = new SqlCommand(zSQL, zConnection);
                zCMD.CommandType = CommandType.Text;
                zCMD.Parameters.Add("@Key", SqlDbType.Int).Value = Key;
                SqlDataAdapter zDa = new SqlDataAdapter(zCMD);
                zDa.Fill(zTable);
                zCMD.Dispose();
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
            }
            return zTable;
        }
    }
}