﻿using System;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;

namespace Lib.JOB
{
    public class Projects_Info
    {
        private int _ProjectKey = 0;
        private string _ProjectID = "";
        private string _ProjectName = "";
        private string _ProjectContent = "";

        private int _CustomerKey = 0;
        private string _CustomerID = "";
        private string _CustomerName = "";

        private DateTime _StartDate;
        private DateTime _DueDate;

        private int _StatusKey = 0;
        private int _Complete = 0;
        private double _ProjectPrice = 0;
        private string _Priority = "Normal";

        private string[] _UserPermission;

        private string _Message = "";
        private string _CreatedBy = "";
        private DateTime _CreatedDateTime;
        private string _ModifiedBy = "";
        private DateTime _ModifiedDateTime;

        #region [ Properties ]

        public int Key
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public string ID
        {
            get { return _ProjectID; }
            set { _ProjectID = value; }
        }
        public string Name
        {
            get { return _ProjectName; }
            set { _ProjectName = value; }
        }
        public string ProjectContent
        {
            get { return _ProjectContent; }
            set { _ProjectContent = value; }
        }

        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public double ProjectPrice
        {
            get { return _ProjectPrice; }
            set { _ProjectPrice = value; }
        }
        public int Complete
        {
            get { return _Complete; }
            set { _Complete = value; }
        }
        public string Priority
        {
            set { _Priority = value; }
            get { return _Priority; }
        }
        public string[] UserPermission
        {
            set { _UserPermission = value; }
            get
            {
                GetPermission();
                return _UserPermission;
            }
        }
        public string CreatedBy
        {
            set { _CreatedBy = value; }
            get { return _CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { _CreatedDateTime = value; }
            get { return _CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { _ModifiedBy = value; }
            get { return _ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { _ModifiedDateTime = value; }
            get { return _ModifiedDateTime; }
        }

        public string Message
        {
            set { _Message = value; }
            get { return _Message; }
        }


        #endregion

        #region [ Constructor Get Information ]
        public Projects_Info()
        {

        }
        public Projects_Info(int ProjectKey)
        {
            string zSQL = " SELECT A.*,B.CustomerID,B.CustomerName FROM TASK_Projects  A LEFT JOIN CUST_Customers B ON A.CustomerKey = B.CustomerKey  WHERE A.ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    _ProjectKey = int.Parse(nReader["ProjectKey"].ToString());
                    _ProjectID = nReader["ProjectID"].ToString();
                    _ProjectName = nReader["ProjectName"].ToString().Trim();
                    _ProjectContent = nReader["ProjectContent"].ToString().Trim();

                    _CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    _CustomerID = nReader["CustomerID"].ToString();
                    _CustomerName = nReader["CustomerName"].ToString();

                    if (nReader["StartDate"] != DBNull.Value)
                        _StartDate = (DateTime)nReader["StartDate"];
                    if (nReader["DueDate"] != DBNull.Value)
                        _DueDate = (DateTime)nReader["DueDate"];

                    _StatusKey = int.Parse(nReader["StatusKey"].ToString());
                    _Complete = int.Parse(nReader["Complete"].ToString());
                    _Priority = nReader["Priority"].ToString();
                    _ProjectPrice = double.Parse(nReader["ProjectPrice"].ToString());

                    _CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    _ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
        public Projects_Info(string ProjectID)
        {
            string zSQL = " SELECT A.*,B.CustomerID,B.CustomerName FROM TASK_Projects  A"
                        + " LEFT JOIN PRO_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " WHERE A.ProjectID = @ProjectID";


            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = ProjectID;

                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    _ProjectKey = int.Parse(nReader["ProjectID"].ToString());
                    _ProjectID = nReader["ProjectID"].ToString();
                    _ProjectName = nReader["ProjectName"].ToString().Trim();
                    _ProjectContent = nReader["ProjectContent"].ToString().Trim();

                    _CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    _CustomerID = nReader["CustomerID"].ToString();
                    _CustomerName = nReader["CustomerName"].ToString();

                    if (nReader["StartDate"] != DBNull.Value)
                        _StartDate = (DateTime)nReader["StartDate"];
                    if (nReader["DueDate"] != DBNull.Value)
                        _DueDate = (DateTime)nReader["DueDate"];

                    _StatusKey = int.Parse(nReader["StatusKey"].ToString());
                    _Complete = int.Parse(nReader["Complete"].ToString());
                    _Priority = nReader["Priority"].ToString();

                    _ProjectPrice = double.Parse(nReader["ProjectPrice"].ToString());

                    _CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    _ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE TASK_Projects SET "
                        + " ProjectID = @ProjectID, "
                        + " ProjectName = @ProjectName,"
                        + " ProjectContent = @ProjectContent ,"

                        + " CustomerKey = @CustomerKey, "
                        + " StartDate = @StartDate, "
                        + " DueDate = @DueDate, "

                        + " StatusKey = @StatusKey ,"
                        + " ProjectPrice = @ProjectPrice ,"
                        + " Complete = @Complete, "
                        + " Priority = @Priority, "

                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "

                        + " WHERE ProjectKey = @ProjectKey";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = _ProjectID;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = _ProjectName;
                zCommand.Parameters.Add("@ProjectContent", SqlDbType.NVarChar).Value = _ProjectContent;

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                if (_StartDate != DateTime.MinValue)
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = _StartDate;
                else
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = System.DBNull.Value;
                if (_DueDate != DateTime.MinValue)
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = _DueDate;
                else
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@ProjectPrice", SqlDbType.Money).Value = _ProjectPrice;
                zCommand.Parameters.Add("@Complete", SqlDbType.Int).Value = _Complete;
                zCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = _Priority;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TASK_Projects(ProjectID, ProjectName, ProjectContent, CustomerKey, StartDate, DueDate, "
                        + " StatusKey, ProjectPrice,Complete, Priority,CreatedBy, CreatedDateTime,ModifiedBy,ModifiedDateTime)"
                        + " VALUES( @ProjectID,@ProjectName, @ProjectContent, @CustomerKey, @StartDate, @DueDate, "
                        + " @StatusKey, @ProjectPrice, @Complete,@Priority, @CreatedBy, getdate(),@ModifiedBy,getdate())"
                        + "SELECT ProjectKey FROM TASK_Projects WHERE ProjectKey = SCOPE_IDENTITY()";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = _ProjectID;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = _ProjectName;
                zCommand.Parameters.Add("@ProjectContent", SqlDbType.NVarChar).Value = _ProjectContent;

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                if (_StartDate != DateTime.MinValue)
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = _StartDate;
                else
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = System.DBNull.Value;
                if (_DueDate != DateTime.MinValue)
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = _DueDate;
                else
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;

                zCommand.Parameters.Add("@ProjectPrice", SqlDbType.Money).Value = _ProjectPrice;

                zCommand.Parameters.Add("@Complete", SqlDbType.Int).Value = _Complete;
                zCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = _Priority;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                _ProjectKey = Convert.ToInt32(zCommand.ExecuteScalar());
                //zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ProjectKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Projects WHERE ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion

        public string UpdatePermission()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "[dbo].[ProjectPermission]";
            string nListEmployee = "";
            for (int i = 0; i < _UserPermission.Length; i++)
            {
                nListEmployee += _UserPermission[i] + "|";
            }

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;

                zCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = _ProjectID;
                zCommand.Parameters.Add("@ListEmployee", SqlDbType.NVarChar).Value = nListEmployee;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        private void GetPermission()
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM TASK_Project_Permission WHERE ProjectKey = @ProjectKey ";
            try
            {
                SqlConnection zConnection = new SqlConnection(ConnectDataBase.ConnectionString);
                SqlCommand zCommand = new SqlCommand(zSQL, zConnection);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;

                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            _UserPermission = new string[zTable.Rows.Count];
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                _UserPermission[i] = nRow["EmployeeKey"].ToString();
                i++;
            }
        }
    }
}

