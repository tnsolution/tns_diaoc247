﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.JOB
{
    public class Tasks_Data
    {
        public static DataTable List_All()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  A.*, B.CategoryName, C.StatusName ,C.Icon, D.EmployeeName
FROM JOB_Tasks A 
LEFT JOIN JOB_Task_Category B ON A.CategoryKey = B.CategoryKey
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey
LEFT JOIN HRM_Employees D ON D.EmployeeKey = A.EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_2017()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  A.*, B.CategoryName, C.StatusName ,C.Icon, D.EmployeeName
FROM JOB_Tasks A 
LEFT JOIN JOB_Task_Category B ON A.CategoryKey = B.CategoryKey
LEFT JOIN JOB_Status C ON A.StatusKey = C.StatusKey
LEFT JOIN HRM_Employees D ON D.EmployeeKey = A.EmployeeKey
WHERE YEAR(A.CreatedDateTime) = 2017";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM JOB_Tasks ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
