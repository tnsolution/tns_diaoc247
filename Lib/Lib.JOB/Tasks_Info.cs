﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.JOB
{
    public class Tasks_Info
    {
        #region [ Field Name ]
        private int _TaskKey = 0;
        private string _TaskName = "";
        private string _TaskContent = "";
        private DateTime _StartDate;
        private DateTime _DueDate;
        private int _StatusKey = 0;
        private string _Priority = "";
        private string _Complete = "";
        private DateTime _ReminderDate;
        private int _FromEmployee = 0;
        private int _ToEmployee = 0;
        private int _CategoryKey = 0;
        private int _CustomerKey = 0;
        private int _ProjectKey = 0;
        private int _EmployeeKey = 0;
        private string _FileAttack = "";
        private string _Redo = "";
        private string _CreatedBy = "";
        private DateTime _CreatedDateTime;
        private string _ModifiedBy = "";
        private DateTime _ModifiedDateTime;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int TaskKey
        {
            get { return _TaskKey; }
            set { _TaskKey = value; }
        }
        public string TaskName
        {
            get { return _TaskName; }
            set { _TaskName = value; }
        }
        public string TaskContent
        {
            get { return _TaskContent; }
            set { _TaskContent = value; }
        }
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public string Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }
        public string Complete
        {
            get { return _Complete; }
            set { _Complete = value; }
        }
        public DateTime ReminderDate
        {
            get { return _ReminderDate; }
            set { _ReminderDate = value; }
        }
        public int FromEmployee
        {
            get { return _FromEmployee; }
            set { _FromEmployee = value; }
        }
        public int ToEmployee
        {
            get { return _ToEmployee; }
            set { _ToEmployee = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string FileAttack
        {
            get { return _FileAttack; }
            set { _FileAttack = value; }
        }
        public string Redo
        {
            get { return _Redo; }
            set { _Redo = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime CreatedDateTime
        {
            get { return _CreatedDateTime; }
            set { _CreatedDateTime = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public DateTime ModifiedDateTime
        {
            get { return _ModifiedDateTime; }
            set { _ModifiedDateTime = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Tasks_Info()
        {
        }
        public Tasks_Info(int TaskKey)
        {
            string zSQL = "SELECT * FROM JOB_Tasks WHERE TaskKey = @TaskKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = TaskKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["TaskKey"] != DBNull.Value)
                        _TaskKey = int.Parse(zReader["TaskKey"].ToString());
                    _TaskName = zReader["TaskName"].ToString();
                    _TaskContent = zReader["TaskContent"].ToString();
                    if (zReader["StartDate"] != DBNull.Value)
                        _StartDate = (DateTime)zReader["StartDate"];
                    if (zReader["DueDate"] != DBNull.Value)
                        _DueDate = (DateTime)zReader["DueDate"];
                    if (zReader["StatusKey"] != DBNull.Value)
                        _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    _Priority = zReader["Priority"].ToString();
                    _Complete = zReader["Complete"].ToString();
                    if (zReader["ReminderDate"] != DBNull.Value)
                        _ReminderDate = (DateTime)zReader["ReminderDate"];
                    if (zReader["FromEmployee"] != DBNull.Value)
                        _FromEmployee = int.Parse(zReader["FromEmployee"].ToString());
                    if (zReader["ToEmployee"] != DBNull.Value)
                        _ToEmployee = int.Parse(zReader["ToEmployee"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _FileAttack = zReader["FileAttack"].ToString();
                    _Redo = zReader["Redo"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)zReader["CreatedDateTime"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)zReader["ModifiedDateTime"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }        
        #endregion
        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO JOB_Tasks ("
        + " TaskName ,TaskContent ,StartDate ,DueDate ,StatusKey ,Priority ,Complete ,ReminderDate ,FromEmployee ,ToEmployee ,CategoryKey ,CustomerKey ,ProjectKey ,EmployeeKey ,FileAttack ,Redo ,CreatedBy ,CreatedDateTime ,ModifiedBy ,ModifiedDateTime ) "
         + " VALUES ( "
         + "@TaskName ,@TaskContent ,@StartDate ,@DueDate ,@StatusKey ,@Priority ,@Complete ,@ReminderDate ,@FromEmployee ,@ToEmployee ,@CategoryKey ,@CustomerKey ,@ProjectKey ,@EmployeeKey ,@FileAttack ,@Redo ,@CreatedBy ,@CreatedDateTime ,@ModifiedBy ,@ModifiedDateTime ) "
         + " SELECT TaskKey FROM JOB_Tasks WHERE TaskKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = _TaskKey;
                zCommand.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = _TaskName;
                zCommand.Parameters.Add("@TaskContent", SqlDbType.NVarChar).Value = _TaskContent;
                if (_StartDate.Year == 0001)
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = _StartDate;
                if (_DueDate.Year == 0001)
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = _DueDate;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = _Priority;
                zCommand.Parameters.Add("@Complete", SqlDbType.NVarChar).Value = _Complete;
                if (_ReminderDate.Year == 0001)
                    zCommand.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = _ReminderDate;
                zCommand.Parameters.Add("@FromEmployee", SqlDbType.Int).Value = _FromEmployee;
                zCommand.Parameters.Add("@ToEmployee", SqlDbType.Int).Value = _ToEmployee;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = _FileAttack;
                zCommand.Parameters.Add("@Redo", SqlDbType.NVarChar).Value = _Redo;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                if (_CreatedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = _CreatedDateTime;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                if (_ModifiedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = _ModifiedDateTime;
                _TaskKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE JOB_Tasks SET "
                        + " TaskName = @TaskName,"
                        + " TaskContent = @TaskContent,"
                        + " StartDate = @StartDate,"
                        + " DueDate = @DueDate,"
                        + " StatusKey = @StatusKey,"
                        + " Priority = @Priority,"
                        + " Complete = @Complete,"
                        + " ReminderDate = @ReminderDate,"
                        + " FromEmployee = @FromEmployee,"
                        + " ToEmployee = @ToEmployee,"
                        + " CategoryKey = @CategoryKey,"
                        + " CustomerKey = @CustomerKey,"
                        + " ProjectKey = @ProjectKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " FileAttack = @FileAttack,"
                        + " Redo = @Redo,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedDateTime = @CreatedDateTime,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedDateTime = @ModifiedDateTime"
                       + " WHERE TaskKey = @TaskKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = _TaskKey;
                zCommand.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = _TaskName;
                zCommand.Parameters.Add("@TaskContent", SqlDbType.NVarChar).Value = _TaskContent;
                if (_StartDate.Year == 0001)
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = _StartDate;
                if (_DueDate.Year == 0001)
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = _DueDate;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = _Priority;
                zCommand.Parameters.Add("@Complete", SqlDbType.NVarChar).Value = _Complete;
                if (_ReminderDate.Year == 0001)
                    zCommand.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = _ReminderDate;
                zCommand.Parameters.Add("@FromEmployee", SqlDbType.Int).Value = _FromEmployee;
                zCommand.Parameters.Add("@ToEmployee", SqlDbType.Int).Value = _ToEmployee;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = _FileAttack;
                zCommand.Parameters.Add("@Redo", SqlDbType.NVarChar).Value = _Redo;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                if (_CreatedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = _CreatedDateTime;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                if (_ModifiedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = _ModifiedDateTime;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_TaskKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM JOB_Tasks WHERE TaskKey = @TaskKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = _TaskKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
