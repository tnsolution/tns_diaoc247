﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.KPI
{
    public class KPI_Assets_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ParentKey = 0;
        private int _AssetKey = 0;
        private string _AssetID = "";
        private string _AssetType = "";
        private int _ProjectKey = 0;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _CategoryKey = 0;
        private int _CategoryPlan = 0;
        private DateTime _CreatedDate;
        private string _Description = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public KPI_Assets_Info()
        {
        }
        public KPI_Assets_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM KPI_Assets WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ParentKey"] != DBNull.Value)
                        _ParentKey = int.Parse(zReader["ParentKey"].ToString());
                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public KPI_Assets_Info(int AssetKey, int EmployeeKey, DateTime CreatedDate)
        {
            string zSQL = "SELECT * FROM KPI_Assets WHERE AssetKey = @AssetKey AND EmployeeKey = @EmployeeKey AND (CreatedDate BETWEEN @FromDate AND @ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                DateTime FromDate = new DateTime(CreatedDate.Year, CreatedDate.Month, CreatedDate.Day, 0, 0, 0);
                DateTime ToDate = new DateTime(CreatedDate.Year, CreatedDate.Month, CreatedDate.Day, 23, 59, 59);

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ParentKey"] != DBNull.Value)
                        _ParentKey = int.Parse(zReader["ParentKey"].ToString());
                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int DepartmentKey { get => _DepartmentKey; set => _DepartmentKey = value; }
        public int CategoryPlan { get => _CategoryPlan; set => _CategoryPlan = value; }
        public string AssetType { get => _AssetType; set => _AssetType = value; }
        public int ProjectKey { get => _ProjectKey; set => _ProjectKey = value; }
        public string AssetID { get => _AssetID; set => _AssetID = value; }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO KPI_Assets ("
        + " ParentKey ,AssetKey, AssetID, AssetType, ProjectKey ,EmployeeKey, DepartmentKey ,CategoryKey, CategoryPlan ,CreatedDate ,Description ) "
         + " VALUES ( "
         + "@ParentKey ,@AssetKey, @AssetID, @AssetType, @ProjectKey, @EmployeeKey, @DepartmentKey ,@CategoryKey, @CategoryPlan ,GETDATE() ,@Description ) "
         + " SELECT AutoKey FROM KPI_Assets WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = _ParentKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryPlan", SqlDbType.Int).Value = _CategoryPlan;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE KPI_Assets SET "
                        + " ParentKey = @ParentKey, AssetID = @AssetID,"
                        + " AssetKey = @AssetKey, AssetType = @AssetType, ProjectKey = @ProjectKey,"
                        + " EmployeeKey = @EmployeeKey, DepartmentKey = @DepartmentKey,"
                        + " CategoryKey = @CategoryKey, CategoryPlan=@CategoryPlan,"
                        + " CreatedDate = GETDATE(),"
                        + " Description = @Description"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = _ParentKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryPlan", SqlDbType.Int).Value = _CategoryPlan;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM KPI_Assets WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
