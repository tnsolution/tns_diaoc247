﻿using Lib.Config;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.KPI
{
    public class Helper_Data
    {
        public static DataTable List(string Param)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT AutoKey, Product AS [Name], ProductGroup
FROM SYS_Categories 
WHERE [TYPE] = 34 AND [VALUE] = 1 AND AutoKey IN (" + Param + ") ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //public static string KPI_Employee(DateTime FromDate, DateTime ToDate, int Employee)
        //{
        //    double[] Array_Require_Depart = new double[50];
        //    double[] Array_Doing_Depart = new double[50];
        //    double[] Array_Item_Rate = new double[50];

        //    int n = 0;

        //    SqlContext Sql = new SqlContext();
        //    DataTable zHeader = Helper_Data.List(_Muctieu);
        //    foreach (DataRow rValue in zHeader.Rows)
        //    {
        //        Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //        Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

        //        DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
        //                                                       + " dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
        //                                                       + " dbo.KPI_RequirePercentV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate)");

        //        Array_Require_Depart[n] = zRow.Rows[0][0].ToDouble();
        //        Array_Doing_Depart[n] = zRow.Rows[0][1].ToDouble();
        //        Array_Item_Rate[n] = zRow.Rows[0][2].ToDouble();
        //        n++;
        //    }

        //    string[] temp = KPI_Employee(Array_Require_Depart, Array_Doing_Depart, Array_Item_Rate, zHeader.Rows.Count);
        //    Sql.CloseConnect();

        //    return temp[0];
        //}
        //public static string KPI_Company(DateTime FromDate, DateTime ToDate, string Department)
        //{
        //    double[] Array_Require_Depart = new double[50];
        //    double[] Array_Doing_Depart = new double[50];

        //    double[] Array_Require_Company = new double[50];
        //    double[] Array_Doing_Company = new double[50];

        //    double[] Array_Require_Employee = new double[50];
        //    double[] Array_Doing_Employee = new double[50];
        //    double[] Array_Item_Rate = new double[50];

        //    SqlContext Sql = new SqlContext();
        //    DataTable zData = Employees_Data.Staff_Working(Department);
        //    DataTable zHeader = Helper_Data.List(_Muctieu);

        //    foreach (DataRow r in zData.Rows)
        //    {
        //        int Employee = r["EmployeeKey"].ToInt();
        //        int n = 0;
        //        foreach (DataRow rValue in zHeader.Rows)
        //        {
        //            Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //            Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //            DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
        //                                                                  + " dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
        //                                                                  + " dbo.KPI_RequirePercentV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate)");

        //            Array_Require_Company[n] = Array_Require_Company[n] + zRow.Rows[0][0].ToDouble();
        //            Array_Doing_Company[n] = Array_Doing_Company[n] + zRow.Rows[0][1].ToDouble();
        //            Array_Item_Rate[n] = zRow.Rows[0][2].ToDouble();
        //            n++;
        //        }
        //    }

        //    string[] temp = KPI_Group(Array_Require_Company, Array_Doing_Company, zHeader.Rows.Count);

        //    Sql.CloseConnect();

        //    return temp[0];
        //}

        //static string[] KPI_Group(double[] ItemRequire, double[] ItemDoing, int Length)
        //{
        //    float total = 0;
        //    for (int i = 0; i < Length; i++)
        //    {
        //        double YC = ItemRequire[i];
        //        double TH = ItemDoing[i];
        //        double TL = 0;
        //        if (YC != 0)
        //            TL = (TH / YC) * 100;
        //        else
        //            TL = TH * 100;
        //        if (TL >= 70)
        //            total += 1;
        //    }

        //    double KQ = (total / Length) * 100;
        //    string[] Result = new string[2];
        //    Result[0] = KQ.ToString("n0");
        //    Result[1] = KQ.ToString("n0") + "%";
        //    return Result;
        //}
        //static string[] KPI_Employee(double[] ItemRequire, double[] ItemDoing, double[] ItemRate, int Length)
        //{
        //    double TLKPI = 0;
        //    for (int i = 0; i < Length; i++)
        //    {
        //        double YC = ItemRequire[i];
        //        double TH = ItemDoing[i];
        //        double TL = 0;
        //        double TS = ItemRate[i]; ;//trong so

        //        if (YC != 0)
        //            TL = (TH / YC) * 100;
        //        else
        //            TL = TH * 100;
        //        if (TL >= 100)
        //        {
        //            if (YC != 0)
        //                TLKPI += (TH / YC) * TS;
        //            else
        //                TLKPI = TH * TS;
        //        }
        //    }

        //    //double KQ = (total / Length) * 100;
        //    string[] Result = new string[2];
        //    Result[0] = TLKPI.ToString("n0");
        //    Result[1] = TLKPI.ToString("n0") + "%";

        //    return Result;
        //}


        public static string KPI_Employee_V3(DateTime FromDate, DateTime ToDate, int Employee, string Muctieu)
        {
            double[] Array_Require_Employee = new double[50];
            double[] Array_Doing_Employee = new double[50];
            double[] Array_Rate_Doing_Employee = new double[50];
            double[] Array_Rate_Require_Employee = new double[50];
            double KQ = 0;
            int n = 0;
            Muctieu = Muctieu.Remove(Muctieu.LastIndexOf(','));
            SqlContext Sql = new SqlContext();
            DataTable zHeader = List(Muctieu);
            foreach (DataRow rValue in zHeader.Rows)
            {
                #region Go SQL GetData
                Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                      + " dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
                                                                      + " dbo.KPI_RequirePercentV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                      + " dbo.KPI_RequireDefaultPercentV3(" + rValue["AutoKey"].ToString() + ")");
                #endregion

                #region Calculator
                double YC = zRow.Rows[0][0].ToDouble();
                double TH = zRow.Rows[0][1].ToDouble();
                double TS = zRow.Rows[0][2].ToDouble();
                double TL = 0;
                if (YC != 0)
                    TL = (TH / YC) * TS;

                Array_Require_Employee[n] = YC;
                Array_Doing_Employee[n] = TH;
                Array_Rate_Doing_Employee[n] = TL;
                Array_Rate_Require_Employee[n] = TS;
                n++;
                #endregion

                KQ += TL;
            }

            Sql.CloseConnect();
            return KQ.ToString("n2");
        }
        public static string KPI_Company_V3(DateTime FromDate, DateTime ToDate, string Department, string Muctieu)
        {
            SqlContext Sql = new SqlContext();
            double KQ = 0;
            Muctieu = Muctieu.Remove(Muctieu.LastIndexOf(','));
            DataTable zData = Employees_Data.Staff_Working(Department);
            DataTable zHeader = List(Muctieu);

            #region [Data]
            if (zData.Rows.Count > 0)
            {
                int Flag = zData.Rows[0]["DepartmentKey"].ToInt();

                double[] Array_Require_Company = new double[50];
                double[] Array_Doing_Company = new double[50];
                double[] Array_Rate_Company = new double[50];

                foreach (DataRow r in zData.Rows)
                {
                    int zEmployee = r["EmployeeKey"].ToInt();
                    int n = 0;
                    foreach (DataRow rValue in zHeader.Rows)
                    {
                        #region Go SQL GetData
                        Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                        Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                        DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + zEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                              + " dbo.KPI_DoingV3(" + zEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
                                                                              + " dbo.KPI_RequirePercentV3(" + zEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                              + " dbo.KPI_RequireDefaultPercentV3(" + rValue["AutoKey"].ToString() + ")");
                        #endregion

                        #region Calculator
                        double YC = zRow.Rows[0][0].ToDouble();
                        double TH = zRow.Rows[0][1].ToDouble();
                        double TS = zRow.Rows[0][2].ToDouble();
                        double TL = 0;
                        if (YC != 0)
                            TL = (TH / YC) * TS;

                        Array_Require_Company[n] = Array_Require_Company[n] + YC;
                        Array_Doing_Company[n] = Array_Doing_Company[n] + TH;
                        Array_Rate_Company[n] = zRow.Rows[0][3].ToDouble();

                        n++;
                        #endregion                                              
                    }
                }
               
                for (int n = 0; n < zHeader.Rows.Count; n++)
                {
                    double YC = Array_Require_Company[n].ToDouble();
                    double TH = Array_Doing_Company[n].ToDouble();
                    double KPI = Array_Rate_Company[n].ToDouble();
                    double TL = 0;
                    if (YC != 0)
                        TL = (TH / YC) * KPI;

                    KQ += TL.ToDouble();
                }
            }
            #endregion       
            Sql.CloseConnect();
            return KQ.ToString("n2");
        }
    }
}
