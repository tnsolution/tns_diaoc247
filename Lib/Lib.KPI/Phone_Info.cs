﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.KPI
{
    public class KPI_Phone_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _DetailKey = 0;
        private int _NoteKey = 0;
        private int _InfoKey = 0;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _Status = 0;
        private string _Description = "";
        private DateTime _CreatedDate;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int DetailKey
        {
            get { return _DetailKey; }
            set { _DetailKey = value; }
        }
        public int NoteKey
        {
            get { return _NoteKey; }
            set { _NoteKey = value; }
        }
        public int InfoKey
        {
            get { return _InfoKey; }
            set { _InfoKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public KPI_Phone_Info()
        {
        }
        public KPI_Phone_Info(int InfoKey, int EmployeeKey, DateTime CreatedDate)
        {
            string zSQL = "SELECT * FROM KPI_Phone WHERE InfoKey = @InfoKey AND EmployeeKey = @EmployeeKey AND (CreatedDate BETWEEN @FromDate AND @ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                DateTime FromDate = new DateTime(CreatedDate.Year, CreatedDate.Month, CreatedDate.Day, 0, 0, 0);
                DateTime ToDate = new DateTime(CreatedDate.Year, CreatedDate.Month, CreatedDate.Day, 23, 59, 59);

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = InfoKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["InfoKey"] != DBNull.Value)
                        _InfoKey = int.Parse(zReader["InfoKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["Status"] != DBNull.Value)
                        _Status = int.Parse(zReader["Status"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO KPI_Phone ("
        + " DetailKey ,NoteKey ,InfoKey ,EmployeeKey ,DepartmentKey ,Status ,Description ,CreatedDate ) "
         + " VALUES ( "
         + "@DetailKey ,@NoteKey ,@InfoKey ,@EmployeeKey ,@DepartmentKey ,@Status ,@Description ,GETDATE() ) "
         + " SELECT AutoKey FROM KPI_Phone WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = _DetailKey;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _InfoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;            
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE KPI_Phone SET "
                        + " DetailKey = @DetailKey,"
                        + " NoteKey = @NoteKey,"
                        + " InfoKey = @InfoKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " Status = @Status,"
                        + " Description = @Description,"
                        + " CreatedDate = GETDATE()"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = _DetailKey;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _InfoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM KPI_Phone WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
