﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;

namespace Lib.SAL
{
    public class Document_Data
    {
        public static List<ItemDocument> List(int OjectKey, string TableName)
        {
            DataTable zTable = new DataTable();
            List<ItemDocument> zList = new List<ItemDocument>();
            string zSQL = @"
SELECT A.*
FROM PUL_Document A
WHERE A.ProjectKey = @ProjectKey AND A.TableName = N'FNC_Ticket'
ORDER BY A.ModifiedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = OjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                zList = zTable.DataTableToList<ItemDocument>();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zList;
        }

        public static List<ItemDocument> List(int CategoryKey, int OjectKey)
        {
            DataTable zTable = new DataTable();
            List<ItemDocument> zList = new List<ItemDocument>();
            string zSQL = @"
SELECT A.*, 
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_SysCategoryName(A.CategoryKey) CategoryName 
FROM PUL_Document A
WHERE A.ProjectKey = @ProjectKey AND A.CategoryKey = @CategoryKey 
ORDER BY A.ModifiedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = OjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                zList = zTable.DataTableToList<ItemDocument>();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zList;
        }

        public static List<ItemDocument> List(int CategoryKey, int OjectKey, string Table)
        {
            DataTable zTable = new DataTable();
            List<ItemDocument> zList = new List<ItemDocument>();
            string zSQL = @"
SELECT A.*, 
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_SysCategoryName(A.CategoryKey) CategoryName 
FROM PUL_Document A
WHERE A.ProjectKey = @ProjectKey AND A.CategoryKey = @CategoryKey  AND A.Tablename =@TableName
ORDER BY A.ModifiedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = OjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = Table;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                zList = zTable.DataTableToList<ItemDocument>();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zList;
        }
    }
}
