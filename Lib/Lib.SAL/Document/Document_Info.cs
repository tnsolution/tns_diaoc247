﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Document_Info
    {

        #region [ Field Name ]
        private int _FileKey = 0;
        private string _TableName = "";
        private int _ProjectKey = 0;
        private int _CategoryKey = 0;
        private string _Description = "";
        private string _ImageUrl = "";
        private string _ImagePath = "";
        private string _ImageThumb = "";
        private string _ImageName = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Document_Info()
        {
        }
        public Document_Info(int FileKey)
        {
            string zSQL = "SELECT * FROM PUL_Document WHERE FileKey = @FileKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = FileKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["FileKey"] != DBNull.Value)
                        _FileKey = int.Parse(zReader["FileKey"].ToString());
                    _ImageUrl = zReader["ImageUrl"].ToString();
                    _TableName = zReader["TableName"].ToString();
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _Description = zReader["Description"].ToString();
                    _ImagePath = zReader["ImagePath"].ToString();
                    _ImageThumb = zReader["ImageThumb"].ToString();
                    _ImageName = zReader["ImageName"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int FileKey
        {
            get { return _FileKey; }
            set { _FileKey = value; }
        }
        public string TableName
        {
            get { return _TableName; }
            set { _TableName = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }
        public string ImageThumb
        {
            get { return _ImageThumb; }
            set { _ImageThumb = value; }
        }
        public string ImageName
        {
            get { return _ImageName; }
            set { _ImageName = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string ImageUrl { get { return _ImageUrl; } set { _ImageUrl = value; } }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PUL_Document ("
        + " TableName ,ProjectKey ,CategoryKey ,Description ,ImagePath ,ImageThumb,ImageUrl ,ImageName ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@TableName ,@ProjectKey ,@CategoryKey ,@Description ,@ImagePath,@ImageUrl ,@ImageThumb ,@ImageName ,@CreatedDate ,@CreatedBy ,@CreatedName ,@ModifiedDate ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT FileKey FROM PUL_Document WHERE FileKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = _TableName;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@ImagePath", SqlDbType.NVarChar).Value = _ImagePath;
                zCommand.Parameters.Add("@ImageThumb", SqlDbType.NVarChar).Value = _ImageThumb;
                zCommand.Parameters.Add("@ImageUrl", SqlDbType.NVarChar).Value = _ImageUrl;
                zCommand.Parameters.Add("@ImageName", SqlDbType.NVarChar).Value = _ImageName;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _FileKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PUL_Document SET "
                        + " TableName = @TableName,"
                        + " ProjectKey = @ProjectKey,"
                        + " CategoryKey = @CategoryKey,"
                        + " Description = @Description,"
                        + " ImagePath = @ImagePath,"
                        + " ImageThumb = @ImageThumb,"
                        + " ImageName = @ImageName, ImageUrl=@ImageUrl,"
                        + " CreatedDate = @CreatedDate,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedName = @CreatedName,"
                        + " ModifiedDate = @ModifiedDate,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE FileKey = @FileKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ImageUrl", SqlDbType.NVarChar).Value = _ImageUrl;
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zCommand.Parameters.Add("@TableName", SqlDbType.NVarChar).Value = _TableName;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@ImagePath", SqlDbType.NVarChar).Value = _ImagePath;
                zCommand.Parameters.Add("@ImageThumb", SqlDbType.NVarChar).Value = _ImageThumb;
                zCommand.Parameters.Add("@ImageName", SqlDbType.NVarChar).Value = _ImageName;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_FileKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Document WHERE FileKey = @FileKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FileKey", SqlDbType.Int).Value = _FileKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string AutoInsert(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------            
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
