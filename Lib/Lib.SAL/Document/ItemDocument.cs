﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ItemDocument
    {
        private string _FileKey = "0";
        private string _TableName = "";
        private string _ProjectKey = "0";
        private string _ProjectName = "";
        private string _CategoryKey = "0";
        private string _CategoryName = "";
        private string _Description = "";
        private string _ImagePath = "";
        private string _ImageThumb = "";
        private string _ImageUrl = "";
        private string _ImageName = "";
        private string _CreatedDate = "";
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private string _ModifiedDate = "";
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";

        #region [ Properties ]
        public string FileKey
        {
            get { return _FileKey; }
            set { _FileKey = value; }
        }
        public string TableName
        {
            get { return _TableName; }
            set { _TableName = value; }
        }
        public string ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public string CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }
        public string ImageThumb
        {
            get { return _ImageThumb; }
            set { _ImageThumb = value; }
        }
        public string ImageName
        {
            get { return _ImageName; }
            set { _ImageName = value; }
        }
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public string ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string ImageUrl
        {
            get
            {
                return _ImageUrl;
            }

            set
            {
                _ImageUrl = value;
            }
        }
        #endregion
    }
}
