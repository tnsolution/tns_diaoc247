﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class Owner_Data
    {
        public static DataTable List(int AssetKey, string Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";

            switch (Type)
            {
                case "ResaleApartment":
                    zSQL = "SELECT OwnerKey, AssetKey, ProductKey, CustomerName, Phone AS Phone1, Phone2, Email AS Email1, Address AS Address1, Address2, CreatedDate FROM PUL_Resale_Apartment_Owners WHERE AssetKey = @AssetKey ORDER BY CreatedDate DESC";
                    break;

                case "ResaleHouse":
                    zSQL = "SELECT OwnerKey, AssetKey, ProductKey, CustomerName, Phone AS Phone1, Phone2, Email AS Email1, Address AS Address1, Address2, CreatedDate FROM PUL_Resale_House_Owners WHERE AssetKey = @AssetKey ORDER BY CreatedDate DESC";
                    break;

                case "ResaleGround":
                    zSQL = "SELECT OwnerKey, AssetKey, ProductKey, CustomerName, Phone AS Phone1, Phone2, Email AS Email1, Address AS Address1, Address2, CreatedDate FROM PUL_Resale_Ground_Owners WHERE AssetKey = @AssetKey ORDER BY CreatedDate DESC";
                    break;

                default:
                    break;
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable List(string Type, int AssetKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";

            switch (Type)
            {
                case "ResaleApartment":
                    zSQL = "SELECT OwnerKey, AssetKey, ProductKey, CustomerName, Phone AS Phone1, Phone2, Email AS Email1, Address AS Address1, Address2, CreatedDate FROM PUL_Resale_Apartment_Owners WHERE AssetKey = @AssetKey ORDER BY CreatedDate DESC";
                    break;

                case "ResaleHouse":
                    zSQL = "SELECT OwnerKey, AssetKey, ProductKey, CustomerName, Phone AS Phone1, Phone2, Email AS Email1, Address AS Address1, Address2, CreatedDate FROM PUL_Resale_House_Owners WHERE AssetKey = @AssetKey ORDER BY CreatedDate DESC";
                    break;

                case "ResaleGround":
                    zSQL = "SELECT OwnerKey, AssetKey, ProductKey, CustomerName, Phone AS Phone1, Phone2, Email AS Email1, Address AS Address1, Address2, CreatedDate FROM PUL_Resale_Ground_Owners WHERE AssetKey = @AssetKey ORDER BY CreatedDate DESC";
                    break;

                default:
                    break;
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Owner_House(int AssetKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PUL_Resale_House_Owners WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Owner_Ground(int AssetKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PUL_Resale_Ground_Owners WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Owner_Apartment(int AssetKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PUL_Resale_Apartment_Owners WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
