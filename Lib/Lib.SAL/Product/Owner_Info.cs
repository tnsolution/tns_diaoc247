﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class Owner_Info
    {
        #region [ Field Name ]
        private string _Type = "";
        private int _AssetKey = 0;
        private int _OwnerKey = 0;
        private int _ProductKey = 0;
        private string _ProductID = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _CustomerName = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Email = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Note = "";
        private string _Message = "";

        #endregion
        #region [ Properties ]

        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public int OwnerKey
        {
            get { return _OwnerKey; }
            set { _OwnerKey = value; }
        }
        public int ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }

        public string Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public string Note
        {
            get
            {
                return _Note;
            }

            set
            {
                _Note = value;
            }
        }

        public string Phone2
        {
            get
            {
                return _Phone2;
            }

            set
            {
                _Phone2 = value;
            }
        }

        public string Address2
        {
            get
            {
                return _Address2;
            }

            set
            {
                _Address2 = value;
            }
        }
        #endregion

        public Owner_Info() { }
        public Owner_Info(string Type, int OwnerKey)
        {
            if (Type == "ResaleApartment")
            {
                #region Building
                string zSQL = "SELECT * FROM PUL_Resale_Apartment_Owners WHERE OwnerKey = @OwnerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = OwnerKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        if (zReader["OwnerKey"] != DBNull.Value)
                            _OwnerKey = int.Parse(zReader["OwnerKey"].ToString());
                        if (zReader["AssetKey"] != DBNull.Value)
                            _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                        _CustomerName = zReader["CustomerName"].ToString();
                        _Phone1 = zReader["Phone"].ToString();
                        _Phone2 = zReader["Phone2"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Address1 = zReader["Address"].ToString();
                        _Address2 = zReader["Address2"].ToString();
                        _Note = zReader["Note"].ToString();
                        _Type = Type;
                    }
                    zReader.Close(); zCommand.Dispose();
                }
                catch (Exception Err) { _Message = Err.ToString(); }
                finally { zConnect.Close(); }
                #endregion
            }
            if (Type == "")
            {
                #region Ground
                string zSQL = "SELECT * FROM PUL_Resale_Ground_Owners WHERE OwnerKey = @OwnerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = OwnerKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        if (zReader["OwnerKey"] != DBNull.Value)
                            _OwnerKey = int.Parse(zReader["OwnerKey"].ToString());
                        if (zReader["AssetKey"] != DBNull.Value)
                            _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                        _CustomerName = zReader["CustomerName"].ToString();
                        _Phone1 = zReader["Phone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Address1 = zReader["Address"].ToString();
                        _Note = zReader["Note"].ToString();
                        _Type = Type;
                    }
                    zReader.Close(); zCommand.Dispose();
                }
                catch (Exception Err) { _Message = Err.ToString(); }
                finally { zConnect.Close(); }
                #endregion
            }
            if (Type == "ResaleHouse")
            {
                #region House
                string zSQL = "SELECT * FROM PUL_Resale_House_Owners WHERE OwnerKey = @OwnerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = OwnerKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        if (zReader["OwnerKey"] != DBNull.Value)
                            _OwnerKey = int.Parse(zReader["OwnerKey"].ToString());
                        if (zReader["AssetKey"] != DBNull.Value)
                            _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                        _CustomerName = zReader["CustomerName"].ToString();
                        _Phone1 = zReader["Phone"].ToString();
                        _Phone2 = zReader["Phone2"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Address1 = zReader["Address"].ToString();
                        _Address2 = zReader["Address2"].ToString();
                        _Note = zReader["Note"].ToString();
                        _Type = Type;
                    }
                    zReader.Close(); zCommand.Dispose();
                }
                catch (Exception Err) { _Message = Err.ToString(); }
                finally { zConnect.Close(); }
                #endregion
            }
        }

        public string Create(string Type)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            if (Type == "ResaleApartment")
            {
                zSQL = "INSERT INTO PUL_Resale_Apartment_Owners ("
                + "AssetKey, ProductKey ,ProductID ,FirstName ,LastName, CustomerName ,Phone, Phone2 ,Email ,Address, Address2, CreatedDate ) "
                + " VALUES ( "
                + "@AssetKey,@ProductKey ,@ProductID ,@FirstName ,@LastName, @CustomerName ,@Phone, @Phone2 ,@Email ,@Address, @Address2, GETDATE() ) "
                + " SELECT OwnerKey FROM PUL_Resale_Apartment_Owners WHERE OwnerKey = SCOPE_IDENTITY() ";
            }
            //if (Type == "")
            //{
            //    zSQL = "INSERT INTO PUL_Resale_Ground_Owners ("
            //    + "AssetKey, ProductKey ,ProductID ,FirstName ,LastName ,Phone, Phone2 ,Email ,Address, Address2, CreatedDate ) "
            //    + " VALUES ( "
            //    + "@AssetKey, @ProductKey ,@ProductID ,@FirstName ,@LastName ,@Phone, @Phone2 ,@Email ,@Address, @Address2, GETDATE() ) "
            //    + " SELECT OwnerKey FROM PUL_Resale_Ground_Owners WHERE OwnerKey = SCOPE_IDENTITY() ";
            //}
            if (Type == "ResaleHouse")
            {
                zSQL = "INSERT INTO PUL_Resale_House_Owners ("
                + "AssetKey, ProductKey ,ProductID ,FirstName ,LastName, CustomerName, Phone ,Phone2 ,Email ,Address, Address2, CreatedDate ) "
                + " VALUES ( "
                + "@AssetKey, @ProductKey ,@ProductID ,@FirstName ,@LastName, @CustomerName ,@Phone, @Phone2 ,@Email ,@Address, @Address, GETDATE()) "
                + " SELECT OwnerKey FROM PUL_Resale_House_Owners WHERE OwnerKey = SCOPE_IDENTITY() ";
            }

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = _OwnerKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = _ProductID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                _OwnerKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update(string Type)
        {
            string zSQL = "";
            if (Type == "ResaleApartment")
            {
                zSQL = "UPDATE PUL_Resale_Apartment_Owners SET AssetKey = @AssetKey,"
                    + " ProductKey = @ProductKey,"
                    + " ProductID = @ProductID,"
                    + " FirstName = @FirstName,"
                    + " LastName = @LastName, CustomerName = @CustomerName,"
                    + " Phone = @Phone, Phone2 = @Phone2,"
                    + " Email = @Email,"
                    + " Address = @Address, Address2 = @Address2, CreatedDate = GETDATE()"
                   + " WHERE OwnerKey = @OwnerKey";
            }
            if (Type == "")
            {
                zSQL = "UPDATE PUL_Resale_Ground_Owners SET AssetKey = @AssetKey,"
                    + " ProductKey = @ProductKey,"
                    + " ProductID = @ProductID,"
                    + " FirstName = @FirstName,"
                    + " LastName = @LastName, CustomerName = @CustomerName,"
                    + " Phone = @Phone, Phone2 = @Phone2,"
                    + " Email = @Email,"
                    + " Address = @Address , Address2 = @Address2, CreatedDate = GETDATE()"
                   + " WHERE OwnerKey = @OwnerKey";
            }
            if (Type == "ResaleHouse")
            {
                zSQL = "UPDATE PUL_Resale_House_Owners SET AssetKey = @AssetKey,"
                    + " ProductKey = @ProductKey,"
                    + " ProductID = @ProductID,"
                    + " FirstName = @FirstName,"
                    + " LastName = @LastName, CustomerName = @CustomerName,"
                    + " Phone = @Phone, Phone2 = @Phone2,"
                    + " Email = @Email,"
                    + " Address = @Address , Address2 = @Address2, CreatedDate = GETDATE()"
                   + " WHERE OwnerKey = @OwnerKey";
            }
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = _OwnerKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = _ProductID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string Type)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            if (Type == "ResaleApartment")
                zSQL = "DELETE FROM PUL_Resale_Apartment_Owners WHERE OwnerKey = @OwnerKey";
            if (Type == "")
                zSQL = "DELETE FROM PUL_Resale_Ground_Owners WHERE OwnerKey = @OwnerKey";
            if (Type == "ResaleHouse")
                zSQL = "DELETE FROM PUL_Resale_House WHERE OwnerKey = @OwnerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = _OwnerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string Type, int AssetKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            if (Type == "ResaleApartment")
                zSQL = "DELETE FROM PUL_Resale_Apartment_Owners WHERE AssetKey = @AssetKey";
            if (Type == "")
                zSQL = "DELETE FROM PUL_Resale_Ground_Owners WHERE AssetKey = @AssetKey";
            if (Type == "ResaleHouse")
                zSQL = "DELETE FROM PUL_Resale_House WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
