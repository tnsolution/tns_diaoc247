﻿namespace Lib.SAL
{
    public class Product_ItemMobile
    {
        public int AssetKey { get; set; } = 0;
        public string AssetType { get; set; } = "";
        public int CategoryKey { get; set; } = 0;
        public string CategoryName { get; set; } = "";
        public int ProjectKey { get; set; } = 0;
        public string ProjectName { get; set; } = "";
        public string AssetID { get; set; } = "";
        public string ID1 { get; set; } = "";
        public string ID2 { get; set; } = "";
        public string ID3 { get; set; } = "";
        public string Purpose { get; set; } = "";
        public string Room { get; set; } = "0";
        public int StatusKey { get; set; } = 0;
        public string Status { get; set; } = "";
        public float Area { get; set; } = 0;
        public string Address { get; set; } = "";
        public int Hot { get; set; } = 0;
        public double Price_VND { get; set; } = 0;
        public double PriceRent_VND { get; set; } = 0;
        public string EmployeeName { get; set; } = "";
        public string ImagePath { get; set; } = "";
        public string CustomerName { get; set; } = "";
        public string CustomerPhone { get; set; } = "";
        public string EmployeePhone { get; set; } = "";
        public int EmployeeKey
        {
            get;
            set;
        } = 0;
    }
}
