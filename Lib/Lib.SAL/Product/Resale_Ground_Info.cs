﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Resale_Ground_Info
    {
        #region [ Field Name ]
        private int _WebPublic = 0;
        private string _Title = "";
        private int _ProjectKey = 0;
        private string _ProjectName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";

        private string _ID1 = "";
        private string _ID2 = "";
        private string _ID3 = "";

        private string _AssetSalesStatus = "";
        private int _AssetSalesStatusKey = 0;
        private string _AssetLegal = "";
        private int _AssetLegalKey = 0;

        private float _Area = 0;
        private int _AssetKey = 0;
        private string _AssetID = "";
        private string _GroundName = "";

        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private string _Street = "";
        private int _WardKey = 0;
        private string _Ward = "";
        private int _DistrictKey = 0;
        private string _District = "";
        private int _ProvinceKey = 0;
        private string _Province = "";
        private string _Address = "";
        private string _PurposeSearch = "";
        private string _PurposeShow = "";
        private string _Type = "";
        private string _LotCode = "";
        private string _MainDoor = "";
        private float _Highway;
        private string _Structure = "";
        private int _NumberRoom = 0;
        private string _Details = "";
        private double _PriceRent = 0;
        private double _PricePurchase = 0;
        private double _Price = 0;
        private int _IsPublic = 0;
        private string _ContractBooking = "";
        private string _ContractPayment = "";
        private string _CertificatePager = "";
        private string _Description = "";
        private DateTime _DateContractEnd;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private int _IsDraft = 0;
        private string _Web = "";
        #endregion
        #region [ Properties ]
        public DateTime DateContractEnd
        {
            get { return _DateContractEnd; }
            set { _DateContractEnd = value; }
        }
        public float Area
        {
            get { return _Area; }
            set { _Area = value; }
        }

        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public string AssetID
        {
            get { return _AssetID; }
            set { _AssetID = value; }
        }
        public string GroundName
        {
            get { return _GroundName; }
            set { _GroundName = value; }
        }

        public int AssetLegalKey
        {
            get
            {
                return _AssetLegalKey;
            }

            set
            {
                _AssetLegalKey = value;
            }
        }
        public string AssetLegal
        {
            get
            {
                return _AssetLegal;
            }

            set
            {
                _AssetLegal = value;
            }
        }
        public int AssetSalesStatusKey
        {
            get
            {
                return _AssetSalesStatusKey;
            }

            set
            {
                _AssetSalesStatusKey = value;
            }
        }
        public string AssetSalesStatus
        {
            get
            {
                return _AssetSalesStatus;
            }

            set
            {
                _AssetSalesStatus = value;
            }
        }

        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }
        public int WardKey
        {
            get { return _WardKey; }
            set { _WardKey = value; }
        }
        public string Ward
        {
            get { return _Ward; }
            set { _Ward = value; }
        }
        public int DistrictKey
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public string District
        {
            get { return _District; }
            set { _District = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public string Province
        {
            get { return _Province; }
            set { _Province = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string PurposeSearch
        {
            get { return _PurposeSearch; }
            set { _PurposeSearch = value; }
        }
        public string PurposeShow
        {
            get { return _PurposeShow; }
            set { _PurposeShow = value; }
        }
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string LotCode
        {
            get { return _LotCode; }
            set { _LotCode = value; }
        }
        public string MainDoor
        {
            get { return _MainDoor; }
            set { _MainDoor = value; }
        }
        public float Highway
        {
            get { return _Highway; }
            set { _Highway = value; }
        }
        public string Structure
        {
            get { return _Structure; }
            set { _Structure = value; }
        }
        public int NumberRoom
        {
            get { return _NumberRoom; }
            set { _NumberRoom = value; }
        }
        public string Details
        {
            get { return _Details; }
            set { _Details = value; }
        }
        public double PriceRent
        {
            get { return _PriceRent; }
            set { _PriceRent = value; }
        }
        public double PricePurchase
        {
            get { return _PricePurchase; }
            set { _PricePurchase = value; }
        }
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public int IsPublic
        {
            get { return _IsPublic; }
            set { _IsPublic = value; }
        }
        public string ContractBooking
        {
            get { return _ContractBooking; }
            set { _ContractBooking = value; }
        }
        public string ContractPayment
        {
            get { return _ContractPayment; }
            set { _ContractPayment = value; }
        }
        public string CertificatePager
        {
            get { return _CertificatePager; }
            set { _CertificatePager = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int IsDraft
        {
            get
            {
                return _IsDraft;
            }

            set
            {
                _IsDraft = value;
            }
        }

        public int ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string ID1
        {
            get
            {
                return _ID1;
            }

            set
            {
                _ID1 = value;
            }
        }

        public string ID2
        {
            get
            {
                return _ID2;
            }

            set
            {
                _ID2 = value;
            }
        }

        public string ID3
        {
            get
            {
                return _ID3;
            }

            set
            {
                _ID3 = value;
            }
        }

        public string Web
        {
            get
            {
                return _Web;
            }

            set
            {
                _Web = value;
            }
        }

        public int DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public int WebPublic
        {
            get
            {
                return _WebPublic;
            }

            set
            {
                _WebPublic = value;
            }
        }

        public string Title
        {
            get
            {
                return _Title;
            }

            set
            {
                _Title = value;
            }
        }

        #endregion
        #region [ Constructor Get Information ]
        public Resale_Ground_Info()
        {
        }
        public Resale_Ground_Info(int AssetKey)
        {
            string zSQL = @"
SELECT A.*, 
G.CategoryName,
B.ProjectName,
G.CategoryName,
D.Product AS AssetSalesStatus,
F.Product AS AssetLegalStatus
FROM PUL_Resale_Ground A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN dbo.SYS_Categories D ON A.GroundStatus = D.AutoKey
LEFT JOIN dbo.SYS_Categories F ON A.Legal = F.AutoKey
LEFT JOIN dbo.PUL_Category G ON G.CategoryKey = A.CategoryKey
WHERE A.AssetKey = @AssetKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Web = zReader["Web"].ToString();
                    if (zReader["WebPublic"] != DBNull.Value)
                        _WebPublic = int.Parse(zReader["WebPublic"].ToString());
                    _ProjectName = zReader["ProjectName"].ToString();
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();

                    _ID1 = zReader["ID1"].ToString();
                    _ID2 = zReader["ID2"].ToString();
                    _ID3 = zReader["ID3"].ToString();

                    _AssetSalesStatus = zReader["AssetSalesStatus"].ToString();
                    if (zReader["GroundStatus"] != DBNull.Value)
                        _AssetSalesStatusKey = int.Parse(zReader["GroundStatus"].ToString());
                    _AssetLegal = zReader["AssetLegalStatus"].ToString();
                    if (zReader["Legal"] != DBNull.Value)
                        _AssetLegalKey = int.Parse(zReader["Legal"].ToString());

                    _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["DateContractEnd"] != DBNull.Value)
                        _DateContractEnd = (DateTime)zReader["DateContractEnd"];

                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());

                    if (zReader["Area"] != DBNull.Value)
                        _Area = float.Parse(zReader["Area"].ToString());
                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    _AssetID = zReader["AssetID"].ToString();
                    _GroundName = zReader["GroundName"].ToString();
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _Street = zReader["Street"].ToString();
                    if (zReader["WardKey"] != DBNull.Value)
                        _WardKey = int.Parse(zReader["WardKey"].ToString());
                    _Ward = zReader["Ward"].ToString();
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    _District = zReader["District"].ToString();
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    _Province = zReader["Province"].ToString();
                    _Address = zReader["Address"].ToString();
                    _PurposeSearch = zReader["PurposeSearch"].ToString();
                    _PurposeShow = zReader["PurposeShow"].ToString();
                    _Type = zReader["Type"].ToString();
                    _LotCode = zReader["LotCode"].ToString();
                    _MainDoor = zReader["MainDoor"].ToString();
                    if (zReader["Highway"] != DBNull.Value)
                        _Highway = float.Parse(zReader["Highway"].ToString());
                    _Structure = zReader["Structure"].ToString();
                    if (zReader["NumberRoom"] != DBNull.Value)
                        _NumberRoom = int.Parse(zReader["NumberRoom"].ToString());
                    _Details = zReader["Details"].ToString();
                    if (zReader["PriceRent"] != DBNull.Value)
                        _PriceRent = double.Parse(zReader["PriceRent"].ToString());
                    if (zReader["PricePurchase"] != DBNull.Value)
                        _PricePurchase = double.Parse(zReader["PricePurchase"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["IsPublic"] != DBNull.Value)
                        _IsPublic = int.Parse(zReader["IsPublic"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"
INSERT INTO PUL_Resale_Ground (ProjectKey,CategoryKey, ID1, ID2, ID3, DepartmentKey,
AssetID ,GroundName ,GroundCategory ,GroundStatus ,ProductKey ,ProductCategory ,ProductID ,Legal ,
FeatureCategory ,EmployeeKey ,EmployeeID ,EmployeeName ,Street ,WardKey ,Ward ,DistrictKey ,
District ,ProvinceKey ,Province ,Address ,PurposeSearch ,PurposeShow ,Type ,LotCode ,MainDoor ,Highway ,Structure ,
NumberRoom ,Details ,PriceRent ,PricePurchase ,Price ,IsPublic ,Area ,Description ,DateContractEnd,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName,IsDraft,Web ) 
VALUES (@ProjectKey, @CategoryKey, @ID1, @ID2, @ID3, @DepartmentKey,
@AssetID ,@GroundName ,@GroundCategory ,@GroundStatus ,@ProductKey ,@ProductCategory ,@ProductID ,@Legal ,
@FeatureCategory ,@EmployeeKey ,@EmployeeID ,@EmployeeName ,@Street ,@WardKey ,@Ward ,@DistrictKey ,
@District ,@ProvinceKey ,@Province ,@Address ,@PurposeSearch ,@PurposeShow ,@Type ,@LotCode ,@MainDoor ,@Highway ,@Structure ,
@NumberRoom ,@Details ,@PriceRent ,@PricePurchase ,@Price ,@IsPublic ,@Area ,@Description ,@DateContractEnd,
@CreatedDate ,@CreatedBy ,@CreatedName ,@ModifiedDate ,@ModifiedBy ,@ModifiedName,@IsDraft,@Web, @Title, @WebPublic )
SELECT AssetKey FROM PUL_Resale_Ground WHERE AssetKey = SCOPE_IDENTITY() ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = WebPublic;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Title;

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _Web;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ID1;
                zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ID2;
                zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ID3;

                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = IsDraft;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.Char).Value = _AssetID;
                zCommand.Parameters.Add("@GroundName", SqlDbType.NVarChar).Value = _GroundName;
                zCommand.Parameters.Add("@GroundCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@GroundStatus", SqlDbType.Int).Value = _AssetSalesStatusKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = 0;
                zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _AssetLegalKey;
                zCommand.Parameters.Add("@FeatureCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = _Ward;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = _District;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _PurposeSearch;
                zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _PurposeShow;
                zCommand.Parameters.Add("@Type", SqlDbType.NVarChar).Value = _Type;
                zCommand.Parameters.Add("@LotCode", SqlDbType.NVarChar).Value = _LotCode;
                zCommand.Parameters.Add("@MainDoor", SqlDbType.NVarChar).Value = _MainDoor;
                zCommand.Parameters.Add("@Highway", SqlDbType.Float).Value = _Highway;
                zCommand.Parameters.Add("@Structure", SqlDbType.NVarChar).Value = _Structure;
                zCommand.Parameters.Add("@NumberRoom", SqlDbType.Int).Value = _NumberRoom;
                zCommand.Parameters.Add("@Details", SqlDbType.NVarChar).Value = _Details;
                zCommand.Parameters.Add("@PriceRent", SqlDbType.Money).Value = _PriceRent;
                zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = _PricePurchase;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;

                if (_DateContractEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = _DateContractEnd;

                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _AssetKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PUL_Resale_Ground SET ProjectKey = @ProjectKey, CategoryKey = @CategoryKey, ID1=@ID1, ID2=@ID2, ID3=@ID3, DepartmentKey =@DepartmentKey,"
                        + " AssetID = @AssetID,"
                        + " GroundName = @GroundName,"
                        + " GroundCategory = @GroundCategory,"
                        + " GroundStatus = @GroundStatus,"
                        + " ProductKey = @ProductKey,"
                        + " ProductCategory = @ProductCategory,"
                        + " ProductID = @ProductID,"
                        + " Legal = @Legal,"
                        + " FeatureCategory = @FeatureCategory,"
                        + " Street = @Street,"
                        + " WardKey = @WardKey,"
                        + " Ward = @Ward,"
                        + " DistrictKey = @DistrictKey,"
                        + " District = @District,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " Province = @Province,"
                        + " Address = @Address,"
                        + " PurposeSearch = @PurposeSearch,"
                        + " PurposeShow = @PurposeShow,"
                        + " Type = @Type,"
                        + " LotCode = @LotCode,"
                        + " MainDoor = @MainDoor,"
                        + " Highway = @Highway,"
                        + " Structure = @Structure,"
                        + " NumberRoom = @NumberRoom,"
                        + " Details = @Details,"
                        + " PriceRent = @PriceRent,"
                        + " PricePurchase = @PricePurchase,"
                        + " Price = @Price,"
                        + " IsPublic = @IsPublic,"
                        + " Area = @Area,"
                        + " Description = @Description, DateContractEnd = @DateContractEnd,"
                        + " ModifiedDate = @ModifiedDate,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName, IsDraft = @IsDraft, Web =@Web, @Title, @WebPublic"
                       + " WHERE AssetKey = @AssetKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = WebPublic;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = Title;

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _Web;

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ID1;
                zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ID2;
                zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ID3;

                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = IsDraft;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.Char).Value = _AssetID;
                zCommand.Parameters.Add("@GroundName", SqlDbType.NVarChar).Value = _GroundName;
                zCommand.Parameters.Add("@GroundCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@GroundStatus", SqlDbType.Int).Value = _AssetSalesStatusKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = 0;
                zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _AssetLegalKey;
                zCommand.Parameters.Add("@FeatureCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = _Ward;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = _District;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _PurposeSearch;
                zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _PurposeShow;
                zCommand.Parameters.Add("@Type", SqlDbType.NVarChar).Value = _Type;
                zCommand.Parameters.Add("@LotCode", SqlDbType.NVarChar).Value = _LotCode;
                zCommand.Parameters.Add("@MainDoor", SqlDbType.NVarChar).Value = _MainDoor;
                zCommand.Parameters.Add("@Highway", SqlDbType.Float).Value = _Highway;
                zCommand.Parameters.Add("@Structure", SqlDbType.NVarChar).Value = _Structure;
                zCommand.Parameters.Add("@NumberRoom", SqlDbType.Int).Value = _NumberRoom;
                zCommand.Parameters.Add("@Details", SqlDbType.NVarChar).Value = _Details;
                zCommand.Parameters.Add("@PriceRent", SqlDbType.Money).Value = _PriceRent;
                zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = _PricePurchase;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_DateContractEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = _DateContractEnd;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AssetKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Resale_Ground WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Share()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE PUL_Resale_Ground SET 
IsPublic = @IsPublic, 
ModifiedName = @ModifiedName,
ModifiedDate = @ModifiedDate,
ModifiedBy = @ModifiedBy
WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;

                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Status()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE PUL_Resale_Ground SET 
GroundStatus = @GroundStatus,
ModifiedName = @ModifiedName, 
ModifiedDate = @ModifiedDate,
ModifiedBy = @ModifiedBy, 
WHERE AssetKey = @AssetKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@GroundStatus", SqlDbType.Int).Value = _AssetSalesStatusKey;

                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string PostWeb()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PUL_Resale_Ground SET WebPublic = @WebPublic, Web = @Web, Title = @Title WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _WebPublic;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _Web;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
