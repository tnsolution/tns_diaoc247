﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class Project_Category_Data
    {
        public static DataTable List(int ProjectKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.CategoryName 
FROM PUL_Project_Category A 
LEFT JOIN PUL_Category B ON A.CategoryKey = B.CategoryKey 
WHERE ProjectKey = @ProjectKey ORDER BY CategoryName ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
