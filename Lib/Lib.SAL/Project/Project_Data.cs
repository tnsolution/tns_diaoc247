﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class Project_Data
    {
        public static DataTable CountWant(int ProjectKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT COUNT(A.AssetKey) AS [Num], A.PurposeShow AS [Name] FROM PUL_Resale_Apartment A WHERE ProjectKey = @ProjectKey GROUP BY A.PurposeShow";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable CountCategory(int ProjectKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT COUNT(CategoryKey) AS [Num], dbo.FNC_AssetCategoryName(A.CategoryKey) [Name] FROM PUL_Resale_Apartment A WHERE ProjectKey = @ProjectKey GROUP BY A.CategoryKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable CountStatus(int ProjectKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT COUNT(A.ApartmentStatus) AS [Num], A.ApartmentStatus AS [Status], dbo.FNC_SysCategoryName(A.ApartmentStatus) [Name] FROM PUL_Resale_Apartment A WHERE ProjectKey = @ProjectKey GROUP BY A.ApartmentStatus";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static List<ItemAsset> List_Asset(int ProjectKey)
        {
            DataTable zTable = new DataTable();
            List<ItemAsset> zList = new List<ItemAsset>();
            string zSQL = @"
SELECT 'ResaleApartment' AS AssetType, A.AssetKey, A.AssetID, A.NumberBed AS Room, A.WallArea AS AreaWall, A.Price_VND, A.ProjectKey, 
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) as [Status],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName
FROM PUL_Resale_Apartment A 
WHERE A.ProjectKey = @Key AND A.IsResale = 1 
ORDER BY A.ApartmentStatus DESC, A.AssetID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = ProjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                zList = zTable.DataTableToList<ItemAsset>();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }


            return zList;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM PUL_Project ORDER BY ProjectName ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(string Project, int ProvinceKey, int DistrictKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM PUL_Project A WHERE ProjectKey IN (" + Project + ") ";
            if (ProvinceKey != 0)
                zSQL += " AND A.ProvinceKey = @ProvinceKey ";
            if (DistrictKey != 0)
                zSQL += " AND A.DistrictKey = @DistrictKey ";
            zSQL += " ORDER BY ProjectName ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int ProvinceKey, int DistrictKey, int Category, int Bed)
        {
            DataTable zTable = new DataTable();
            string SubQuery1 = @"
SELECT 1 AS Style , A.ProjectKey, '' AS AssetCategory, '' CategoryName,
	N'Dự án ' + A.ProjectName AS Name, A.Investor, A.Address [Address], '0' Area, '0' Money
	FROM PUL_Project A
    WHERE 1 = 1";

            if (ProvinceKey != 0)
                SubQuery1 += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                SubQuery1 += " AND A.DistrictKey = @DistrictKey";

            string SubQuery2 = @"
SELECT 2 AS Style, B.ProjectKey, B.CategoryKey AS AssetCategory, C.CategoryName, 	
	B.Name AS Name, '' ,'', B.Area, B.Money
	FROM PUL_Project_Assets B 		
	LEFT JOIN PUL_Project D ON D.ProjectKey = B.ProjectKey 
	LEFT JOIN PUL_Category C ON C.CategoryKey = B.CategoryKey
    WHERE 1 = 1";

            if (Category != 0)
                SubQuery2 += " AND B.CategoryKey = @CategoryKey";
            if (Bed != 0)
                SubQuery2 += " AND B.Bed = @Bed";

            string zSQL = @"
SELECT * FROM( " + SubQuery1 + " UNION ALL " + SubQuery2 + ")X ORDER BY X.ProjectKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Bed", SqlDbType.Int).Value = Bed;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int ProvinceKey, int DistrictKey, int Category, int Bed, double FromPrice, double ToPrice)
        {
            DataTable zTable = new DataTable();
            string SubQuery1 = @"
SELECT 1 AS Style , A.ProjectKey, '' AS AssetCategory, '' CategoryName,
	N'Dự án ' + A.ProjectName AS Name, A.Investor, A.Address [Address], '0' Area, '0' Money
	FROM PUL_Project A
    WHERE 1 = 1";

            if (ProvinceKey != 0)
                SubQuery1 += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                SubQuery1 += " AND A.DistrictKey = @DistrictKey";

            string SubQuery2 = @"
SELECT 2 AS Style, B.ProjectKey, B.CategoryKey AS AssetCategory, C.CategoryName, 	
	B.Name AS Name, '' ,'', B.Area, B.Money
	FROM PUL_Project_Assets B 		
	LEFT JOIN PUL_Project D ON D.ProjectKey = B.ProjectKey 
	LEFT JOIN PUL_Category C ON C.CategoryKey = B.CategoryKey
    WHERE 1 = 1";

            if (FromPrice > 0 && ToPrice > 0)
                SubQuery2 += " AND B.Money BETWEEN " + FromPrice + " AND " + ToPrice;
            if (FromPrice > 0 && ToPrice == 0)
                SubQuery2 += " AND B.Money <= " + ToPrice;
            if (FromPrice == 0 && ToPrice > 0)
                SubQuery2 += " AND B.Money >= " + ToPrice;

            if (Category != 0)
                SubQuery2 += " AND B.CategoryKey = @CategoryKey";
            if (Bed != 0)
                SubQuery2 += " AND B.Bed = @Bed";

            string zSQL = @"
SELECT * FROM( " + SubQuery1 + " UNION " + SubQuery2 + ")X ORDER BY X.ProjectKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Bed", SqlDbType.Int).Value = Bed;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int ProvinceKey, int DistrictKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM PUL_Project A WHERE 1 = 1";

            if (ProvinceKey != 0)
                zSQL += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                zSQL += " AND A.DistrictKey = @DistrictKey";

            zSQL += @" ORDER BY ProjectName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Category_Product(int Key)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"proc_Get_Product_Category";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = Key;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Transfer(int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AssetKey,
A.AssetID, B.ProjectName AS Name_Project, C.CategoryName, B.Address, 
A.PurposeShow, A.WallArea AS Area, A.Price, D.Product Name_Status, A.EmployeeName, A.Price, A.PriceRent
FROM PUL_Resale_Apartment A 
LEFT JOIN PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN PUL_Category C ON C.CategoryKey = A.ApartmentCategory
LEFT JOIN SYS_Categories D ON D.AutoKey = A.ApartmentStatus
";

            if (EmployeeKey != 0)
                zSQL += " WHERE A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //===================

        public static DataTable Search_Aparment(int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.AssetKey,A.ApartmentName AS AssetID, A.CreatedName,
A.PurposeShow,
A.PriceRent,A.Price,
A.Area,A.Address,
C.Product AS Name_Project, 
D.Product AS Name_Category,
E.Product AS Name_Status,
B.Product AS TYPE,
F.LastName + ' ' + F.FirstName AS EmployeeName
FROM PUL_Resale_Apartment A 
LEFT JOIN SYS_Categories B ON A.ProductCategory = B.AutoKey 
LEFT JOIN SYS_Categories C ON A.ProductKey = C.AutoKey 
LEFT JOIN SYS_Categories D ON A.ApartmentCategory = D.AutoKey 
LEFT JOIN SYS_Categories E ON A.ApartmentStatus = E.AutoKey 
LEFT JOIN HRM_Employees F ON A.EmployeeKey = F.EmployeeKey
WHERE B.Type = 3
";

            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY E.Product ASC, A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Aparment(
            int Category, int ProjectKey, int ProvinceKey, int DistrictKey, int AssetCategory, int EmployeeKey,
            string Status, string Purpose, string Product, string ApartmentName,
            float FromArea, float ToArea,
            double FromPricePurchase, double ToPricePurchase,
            double FromPriceRent, double ToPriceRent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.AssetKey, A.ApartmentName AS AssetID, A.CreatedName,
A.PurposeShow,
A.PriceRent,A.Price,
A.Area,A.Address,
C.Product AS Name_Project, 
D.Product AS Name_Category,
E.Product AS Name_Status,
B.Product AS TYPE
FROM PUL_Resale_Apartment A 
LEFT JOIN SYS_Categories B ON A.ProductCategory = B.AutoKey 
LEFT JOIN SYS_Categories C ON A.ProductKey = C.AutoKey 
LEFT JOIN SYS_Categories D ON A.ApartmentCategory = D.AutoKey 
LEFT JOIN SYS_Categories E ON A.ApartmentStatus = E.AutoKey 
WHERE B.Type = 3 AND A.ProductCategory = @CategoryKey 
";
            if (ProjectKey != 0)
                zSQL += " AND A.ProductKey = @ProjectKey";
            if (ProvinceKey != 0)
                zSQL += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                zSQL += " AND A.DistrictKey = @DistrictKey";
            if (AssetCategory != 0)
                zSQL += " AND A.ApartmentCategory = @ApartmentCategory";
            if (ApartmentName != string.Empty)
                zSQL += " AND A.ApartmentName LIKE @Name";

            if (Status != string.Empty)
                zSQL += " AND ApartmentStatus IN (" + Status.Remove(Status.LastIndexOf(','), 1) + ")";
            if (Purpose != string.Empty)
                zSQL += " AND PurposeSearch LIKE '%" + Purpose + "%'";
            if (Product == "1")
                zSQL += " AND EmployeeKey = " + EmployeeKey;

            if (FromArea > 0 && ToArea > 0)
                zSQL += " AND A.Area BETWEEN " + FromArea + " AND " + ToArea;
            if (FromArea > 0 && ToArea == 0)
                zSQL += " AND A.Area <= " + FromArea;
            if (FromArea == 0 && ToArea > 0)
                zSQL += " AND A.Area >= " + ToArea;

            if (FromPricePurchase > 0 && ToPricePurchase > 0)
                zSQL += " AND A.PricePurchase BETWEEN " + FromPricePurchase + " AND " + ToPricePurchase;
            if (FromPricePurchase > 0 && ToPricePurchase == 0)
                zSQL += " AND A.PricePurchase <= " + FromPricePurchase;
            if (FromPricePurchase == 0 && ToPricePurchase > 0)
                zSQL += " AND A.PricePurchase >= " + ToPricePurchase;

            if (FromPriceRent > 0 && ToPriceRent > 0)
                zSQL += " AND A.PriceRent BETWEEN " + FromPriceRent + " AND " + ToPriceRent;
            if (FromPriceRent > 0 && ToPriceRent == 0)
                zSQL += " AND A.PriceRent <= " + FromPriceRent;
            if (FromPriceRent == 0 && ToPriceRent > 0)
                zSQL += " AND A.PriceRent >= " + ToPriceRent;

            zSQL += " ORDER BY E.Product ASC, AssetID, A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                zCommand.Parameters.Add("@ApartmentCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + ApartmentName + "%";

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_House(
          int Category, int ProjectKey, int ProvinceKey, int DistrictKey, int AssetCategory, int EmployeeKey,
          string Status, string Purpose, string Product, string Name,
          float FromArea, float ToArea,
          double FromPricePurchase, double ToPricePurchase,
          double FromPriceRent, double ToPriceRent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT 
A.AssetKey, A.CreatedName,
A.HouseName AS AssetID, 
A.Address,
A.PurposeShow,
A.Area,
A.PriceRent,
A.Price,
B.Product AS Name_Project,
C.Product AS Name_Status,
D.Product AS Name_Legal,
E.Product AS Name_Category
FROM PUL_Resale_House A
LEFT JOIN dbo.SYS_Categories B ON A.ProductKey = B.AutoKey
LEFT JOIN dbo.SYS_Categories C ON A.HouseStatus = C.AutoKey
LEFT JOIN dbo.SYS_Categories D ON A.Legal = D.AutoKey
LEFT JOIN dbo.SYS_Categories E ON A.FeatureCategory = E.AutoKey
WHERE A.HouseCategory = @Category";

            if (ProjectKey != 0)
                zSQL += " AND A.ProductKey = @ProjectKey";
            if (ProvinceKey != 0)
                zSQL += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                zSQL += " AND A.DistrictKey = @DistrictKey";
            if (Status != string.Empty)
                zSQL += " AND A.HouseStatus IN (" + Status.Remove(Status.LastIndexOf(','), 1) + ")";
            if (Purpose != string.Empty)
                zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
            if (Product == "1")
                zSQL += " AND A.EmployeeKey = " + EmployeeKey;
            if (FromArea != 0 && ToArea > 0)
                zSQL += " AND A.Area BETWEEN @FromArea AND @ToArea";
            if (FromArea != 0 && ToArea == 0)
                zSQL += " AND A.Area <= @FromArea";
            if (FromArea == 0 && ToArea > 0)
                zSQL += " AND A.Area >= @ToArea";
            if (Name != string.Empty)
                zSQL += " AND A.HouseName LIKE @Name";
            if (FromPricePurchase != 0 && ToPricePurchase > 0)
                zSQL += " AND A.PricePurchase BETWEEN @FromPricePurchase AND @ToPricePurchase";
            if (FromPricePurchase != 0 && ToPricePurchase == 0)
                zSQL += " AND A.PricePurchase <= @PricePurchase";
            if (FromPricePurchase == 0 && ToPricePurchase != 0)
                zSQL += " AND A.PricePurchase >= @ToPricePurchase";

            if (FromPriceRent != 0 && ToPriceRent != 0)
                zSQL += " AND A.PriceRent BETWEEN @FromPriceRent AND @ToPriceRent";
            if (FromPriceRent != 0 && ToPriceRent == 0)
                zSQL += " AND A.PriceRent <= @FromPriceRent";
            if (FromPriceRent == 0 && ToPriceRent != 0)
                zSQL += " AND A.PriceRent >= @ToPriceRent";

            zSQL += " ORDER BY E.Product ASC, AssetID, A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@FromArea", SqlDbType.Float).Value = FromArea;
                zCommand.Parameters.Add("@ToArea", SqlDbType.Float).Value = ToArea;
                zCommand.Parameters.Add("@ToPriceRent", SqlDbType.Money).Value = ToPriceRent;
                zCommand.Parameters.Add("@FromPriceRent", SqlDbType.Money).Value = FromPriceRent;
                zCommand.Parameters.Add("@FromPricePurchase", SqlDbType.Money).Value = FromPricePurchase;
                zCommand.Parameters.Add("@ToPricePurchase", SqlDbType.Money).Value = ToPricePurchase;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Ground(
         int Category, int ProjectKey, int ProvinceKey, int DistrictKey, int AssetCategory, int EmployeeKey,
         string Status, string Purpose, string Product, string Name,
         float FromArea, float ToArea,
         double FromPricePurchase, double ToPricePurchase,
         double FromPriceRent, double ToPriceRent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT 
A.AssetKey, A.CreatedName,
A.LotCode AS AssetID,
A.Address,
A.PurposeShow,
A.Area,
A.PriceRent,
A.Price,
B.Product AS Name_Project,
C.Product AS Name_Status,
D.Product AS Name_Legal,
E.Product AS Name_Category
FROM PUL_Resale_Ground A
LEFT JOIN dbo.SYS_Categories B ON A.ProductKey = B.AutoKey
LEFT JOIN dbo.SYS_Categories C ON A.GroundStatus = C.AutoKey
LEFT JOIN dbo.SYS_Categories D ON A.Legal = D.AutoKey
LEFT JOIN dbo.SYS_Categories E ON A.FeatureCategory = E.AutoKey
WHERE 1=1";

            if (ProjectKey != 0)
                zSQL += " AND A.ProjectKey = @ProjectKey";
            if (ProvinceKey != 0)
                zSQL += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                zSQL += " AND A.DistrictKey = @DistrictKey";
            if (Status != string.Empty)
                zSQL += " AND A.GroundStatus IN (" + Status.Remove(Status.LastIndexOf(','), 1) + ")";
            if (Purpose != string.Empty)
                zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Remove(Purpose.LastIndexOf(','), 1) + "%'";
            if (Product == "1")
                zSQL += " AND A.EmployeeKey = " + EmployeeKey;
            if (FromArea != 0 && ToArea > 0)
                zSQL += " AND A.Area BETWEEN @FromArea AND @ToArea";
            if (FromArea != 0 && ToArea == 0)
                zSQL += " AND A.Area <= @FromArea";
            if (FromArea == 0 && ToArea > 0)
                zSQL += " AND A.Area >= @ToArea";
            if (Name != string.Empty)
                zSQL += " AND A.LotCode LIKE @Name";
            if (FromPricePurchase != 0 && ToPricePurchase > 0)
                zSQL += " AND A.PricePurchase BETWEEN @FromPricePurchase AND @ToPricePurchase";
            if (FromPricePurchase != 0 && ToPricePurchase == 0)
                zSQL += " AND A.PricePurchase <= @PricePurchase";
            if (FromPricePurchase == 0 && ToPricePurchase != 0)
                zSQL += " AND A.PricePurchase >= @ToPricePurchase";

            if (FromPriceRent != 0 && ToPriceRent != 0)
                zSQL += " AND A.PriceRent BETWEEN @FromPriceRent AND @ToPriceRent";
            if (FromPriceRent != 0 && ToPriceRent == 0)
                zSQL += " AND A.PriceRent <= @FromPriceRent";
            if (FromPriceRent == 0 && ToPriceRent != 0)
                zSQL += " AND A.PriceRent >= @ToPriceRent";

            zSQL += " ORDER BY AssetID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@FromArea", SqlDbType.Float).Value = FromArea;
                zCommand.Parameters.Add("@ToArea", SqlDbType.Float).Value = ToArea;
                zCommand.Parameters.Add("@ToPriceRent", SqlDbType.Money).Value = ToPriceRent;
                zCommand.Parameters.Add("@FromPriceRent", SqlDbType.Money).Value = FromPriceRent;
                zCommand.Parameters.Add("@FromPricePurchase", SqlDbType.Money).Value = FromPricePurchase;
                zCommand.Parameters.Add("@ToPricePurchase", SqlDbType.Money).Value = ToPricePurchase;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable Search_Project_Building_Asset(
          int ProjectCategory, int ProjectKey,
          int ProvinceKey, int DistrictKey, int AssetCategory,
          double FromMoney, double ToMoney)
        {
            DataTable zTable = new DataTable();
            string zWhereProject = "";
            string zWhereAsset = "";

            if (ProjectKey != 0)
            {
                zWhereProject += " AND A.ProjectKey = @ProjectKey";
                zWhereAsset += " AND D.ProjectKey = @ProjectKey";
            }

            if (DistrictKey != 0)
            {
                zWhereProject += " AND A.DistrictKey = @DistrictKey";
                zWhereAsset += " AND D.DistrictKey = @DistrictKey";
            }

            if (ProvinceKey != 0)
            {
                zWhereProject += " AND A.ProvinceKey = @ProvinceKey";
                zWhereAsset += " AND D.ProvinceKey = @ProvinceKey";
            }

            if (AssetCategory != 0)
            {
                zWhereAsset += " AND B.CategoryKey = @AssetCategoryKey";
            }

            if (FromMoney != 0 && ToMoney != 0)
            {
                zWhereAsset += " AND B.Money BETWEEN @FromMoney AND @ToMoney";
            }

            string zSQL = @"
SELECT * FROM 
(
	SELECT 1 AS Style , A.BuildingKey AS [KEY], '' AS AssetCategory, 
	A.CategoryKey AS ProjectCategory, 
	A.ProjectKey, N'Dự án ' + A.ProjectName AS Name, A.Investor, N'Quận ' + C.Name + ', ' + B.Name AS Address, '0' Area, '0' CategoryName , '0' Money
	FROM dbo.PUL_Buildings A 
	LEFT JOIN SYS_Province B ON A.ProvinceKey = B.ProvinceKey
	LEFT JOIN SYS_District C ON A.DistrictKey = C.DistrictKey
    WHERE A.CategoryKey = @CategoryKey
	" + zWhereProject + @"

	UNION ALL
	
	SELECT 2 AS Style, B.BuildingKey AS [KEY], B.CategoryKey AS AssetCategory, 
	D.CategoryKey AS ProjectCategory, 
	D.ProjectKey, N'Căn ' + B.Name AS Name, '' ,'', B.Area , C.Product, B.Money
	FROM PUL_Building_Assets B 
	LEFT JOIN dbo.SYS_Categories C ON C.AutoKey = B.CategoryKey
	LEFT JOIN dbo.PUL_Buildings D ON D.BuildingKey = B.BuildingKey 
    WHERE D.CategoryKey = @CategoryKey
	" + zWhereAsset + @"
) X
ORDER BY X.[KEY]";


            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = ProjectCategory;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                zCommand.Parameters.Add("@AssetCategoryKey", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@FromMoney", SqlDbType.Money).Value = FromMoney;
                zCommand.Parameters.Add("@ToMoney", SqlDbType.Money).Value = ToMoney;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Building(int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, A.BuildingKey AS [KEY],
N'Quận ' + D.Name + ', ' + C.Name AS 'Address1' 
FROM PUL_Buildings A 
LEFT JOIN SYS_Categories B ON A.CategoryKey = B.AutoKey 
LEFT JOIN dbo.SYS_Province C ON A.ProvinceKey = C.ProvinceKey
LEFT JOIN dbo.SYS_District D ON A.DistrictKey = D.DistrictKey
WHERE A.CategoryKey = @CategoryKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Ground()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, A.GroundKey AS [KEY], A.Disctrict + ','+a.Province AS Address1 FROM PUL_Grounds A";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Ground(int ProjectKey, int ProvinceKey, int DistrictKey, double FromMoney, double ToMoney, float FromArea, float ToArea)
        {
            DataTable zTable = new DataTable();
            string zWhereProject = "";
            string zWhereAsset = "";

            if (ProjectKey != 0)
            {
                zWhereProject += " AND A.ProjectKey = @ProjectKey";
                zWhereAsset += " AND D.ProjectKey = @ProjectKey";
            }

            if (DistrictKey != 0)
            {
                zWhereProject += " AND A.DistrictKey = @DistrictKey";
                zWhereAsset += " AND D.DistrictKey = @DistrictKey";
            }

            if (ProvinceKey != 0)
            {
                zWhereProject += " AND A.ProvinceKey = @ProvinceKey";
                zWhereAsset += " AND D.ProvinceKey = @ProvinceKey";
            }

            if (FromMoney != 0 && ToMoney != 0)
                zWhereAsset += " AND B.Money BETWEEN @FromMoney AND @ToMoney";
            if (FromMoney != 0 && ToMoney == 0)
                zWhereAsset += " AND B.Money <= @FromMoney";
            if (FromMoney == 0 && ToMoney != 0)
                zWhereAsset += " AND B.Money >= @ToMoney";

            if (FromArea != 0 && ToArea != 0)
                zWhereAsset += " AND B.Area BETWEEN @FromArea AND @ToArea";
            if (FromArea != 0 && ToArea == 0)
                zWhereAsset += " AND B.Area <= @FromArea";
            if (FromArea == 0 && ToArea != 0)
                zWhereAsset += " AND B.Area >= @ToArea";

            string zSQL = @"SELECT * FROM 
(
	SELECT 1 AS Style , A.GroundKey AS [KEY], '' AS AssetCategory, 
	A.CategoryKey AS ProjectCategory, 
	A.ProjectKey, N'Dự án ' + A.ProjectName AS Name, A.Investor, N'Quận ' + C.Name + ', ' + B.Name AS Address, '0' Area, '0' CategoryName , '0' Money
	FROM dbo.PUL_Grounds A 
	LEFT JOIN SYS_Province B ON A.ProvinceKey = B.ProvinceKey
	LEFT JOIN SYS_District C ON A.DistrictKey = C.DistrictKey
    WHERE A.CategoryKey = 0
	" + zWhereProject + @"

	UNION ALL
	
	SELECT 2 AS Style, B.GroundKey AS [KEY], B.CategoryKey AS AssetCategory, 
	D.CategoryKey AS ProjectCategory, 
	D.ProjectKey, N'Lô ' + B.Name AS Name, '' ,'', B.Area , C.Product, B.Money
	FROM PUL_Ground_Assets B 
	LEFT JOIN dbo.SYS_Categories C ON C.AutoKey = B.CategoryKey
	LEFT JOIN dbo.PUL_Grounds D ON D.GroundKey = B.GroundKey 
    WHERE D.CategoryKey = 0
	" + zWhereAsset + @"
) X
ORDER BY X.GroundKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                zCommand.Parameters.Add("@FromArea", SqlDbType.Float).Value = FromArea;
                zCommand.Parameters.Add("@ToArea", SqlDbType.Float).Value = ToArea;
                zCommand.Parameters.Add("@FromMoney", SqlDbType.Money).Value = FromMoney;
                zCommand.Parameters.Add("@ToMoney", SqlDbType.Money).Value = ToMoney;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Adjacent()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *,A.AdjacentKey AS [KEY],  A.District +','+A.Province AS Address1 FROM PUL_Adjacents A";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Adjacent(int ProjectKey, int ProvinceKey, int DistrictKey, double FromMoney, double ToMoney, float FromArea, float ToArea)
        {
            DataTable zTable = new DataTable();
            string zWhereProject = "";
            string zWhereAsset = "";

            if (ProjectKey != 0)
            {
                zWhereProject += " AND A.ProjectKey = @ProjectKey";
                zWhereAsset += " AND D.ProjectKey = @ProjectKey";
            }

            if (DistrictKey != 0)
            {
                zWhereProject += " AND A.DistrictKey = @DistrictKey";
                zWhereAsset += " AND D.DistrictKey = @DistrictKey";
            }

            if (ProvinceKey != 0)
            {
                zWhereProject += " AND A.ProvinceKey = @ProvinceKey";
                zWhereAsset += " AND D.ProvinceKey = @ProvinceKey";
            }

            if (FromMoney != 0 && ToMoney != 0)
                zWhereAsset += " AND B.Money BETWEEN @FromMoney AND @ToMoney";
            if (FromMoney != 0 && ToMoney == 0)
                zWhereAsset += " AND B.Money <= @FromMoney";
            if (FromMoney == 0 && ToMoney != 0)
                zWhereAsset += " AND B.Money >= @ToMoney";

            if (FromArea != 0 && ToArea != 0)
                zWhereAsset += " AND B.Area BETWEEN @FromArea AND @ToArea";
            if (FromArea != 0 && ToArea == 0)
                zWhereAsset += " AND B.Area <= @FromArea";
            if (FromArea == 0 && ToArea != 0)
                zWhereAsset += " AND B.Area >= @ToArea";

            string zSQL = @"SELECT * FROM 
(
	SELECT 1 AS Style , A.AdjacentKey AS [KEY], '' AS AssetCategory, 
	A.CategoryKey AS ProjectCategory, 
	A.ProjectKey, N'Dự án ' + A.ProjectName AS Name, A.Investor, N'Quận ' + C.Name + ', ' + B.Name AS Address, '0' Area, '0' CategoryName , '0' Money
	FROM dbo.PUL_Adjacents A 
	LEFT JOIN SYS_Province B ON A.ProvinceKey = B.ProvinceKey
	LEFT JOIN SYS_District C ON A.DistrictKey = C.DistrictKey
    WHERE A.CategoryKey = 0
	" + zWhereProject + @"

	UNION ALL
	
	SELECT 2 AS Style, B.AdjacentKey AS [KEY], B.CategoryKey AS AssetCategory, 
	D.CategoryKey AS ProjectCategory, 
	D.ProjectKey, B.Name, '' AS Investor,'' AS Address, B.Area , C.Product, B.Money
	FROM PUL_Adjacent_Assets B 
	LEFT JOIN dbo.SYS_Categories C ON C.AutoKey = B.CategoryKey
	LEFT JOIN dbo.PUL_Adjacents D ON D.AdjacentKey = B.AdjacentKey 
    WHERE D.CategoryKey = 0
	" + zWhereAsset + @"
) X
ORDER BY X.AdjacentKey";


            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                zCommand.Parameters.Add("@FromArea", SqlDbType.Float).Value = FromArea;
                zCommand.Parameters.Add("@ToArea", SqlDbType.Float).Value = ToArea;
                zCommand.Parameters.Add("@FromMoney", SqlDbType.Money).Value = FromMoney;
                zCommand.Parameters.Add("@ToMoney", SqlDbType.Money).Value = ToMoney;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Office()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OfficeKey AS [KEY], A.Investor, A.District + ',' + a.Province AS Address1, B.Product AS ProjectName, A.AverageMoneyUSD, A.AverageMoney
FROM PUL_Offices A 
LEFT JOIN dbo.SYS_Categories B ON A.ProjectKey = B.AUTOKEY";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search_Project_Office(int ProjectKey, int ProvinceKey, int DistrictKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.OfficeKey AS [KEY], A.Investor, A.District + ',' + a.Province AS Address1, B.Product AS ProjectName, A.AverageMoneyUSD, A.AverageMoney 
FROM PUL_Offices A 
LEFT JOIN dbo.SYS_Categories B ON A.ProjectKey = B.AUTOKEY";

            if (ProjectKey != 0)
                zSQL += " WHERE A.ProjectKey = @ProjectKey";
            else
                zSQL += " WHERE A.ProjectKey >0";

            if (ProvinceKey != 0)
                zSQL += " AND A.ProvinceKey = @ProvinceKey";
            if (DistrictKey != 0)
                zSQL += " AND A.DistrictKey = @DistrictKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
