﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class ScheduleDetail_Info
    {
        #region [ Field Name ]
        private int _ID = 0;
        private string _Title = "";
        private int _TimeKey = 0;
        private string _WorkTime = "";
        private string _WorkContent = "";
        private int _DepartmentKey = 0;
        private int _EmployeeKey = 0;
        private string _Subject = "";
        private string _Description = "";
        private string _Start = "";
        private string _End = "";
        private bool _AllDay;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public int TimeKey
        {
            get { return _TimeKey; }
            set { _TimeKey = value; }
        }
        public string WorkTime
        {
            get { return _WorkTime; }
            set { _WorkTime = value; }
        }
        public string WorkContent
        {
            get { return _WorkContent; }
            set { _WorkContent = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Start
        {
            get { return _Start; }
            set { _Start = value; }
        }
        public string End
        {
            get { return _End; }
            set { _End = value; }
        }
        public bool AllDay
        {
            get { return _AllDay; }
            set { _AllDay = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public ScheduleDetail_Info()
        {
        }
        public ScheduleDetail_Info(int ID)
        {
            string zSQL = "SELECT * FROM HRM_ScheduleDetail WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ID"] != DBNull.Value)
                        _ID = int.Parse(zReader["ID"].ToString());
                    _Title = zReader["Title"].ToString();
                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    _WorkTime = zReader["WorkTime"].ToString();
                    _WorkContent = zReader["WorkContent"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _Subject = zReader["Subject"].ToString();
                    _Description = zReader["Description"].ToString();
                    _Start = zReader["Start"].ToString();
                    _End = zReader["End"].ToString();
                    if (zReader["AllDay"] != DBNull.Value)
                        _AllDay = (bool)zReader["AllDay"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ScheduleDetail ("
        + " Title ,TimeKey ,WorkTime ,WorkContent ,DepartmentKey ,EmployeeKey ,Subject ,[Description] ,[Start] ,[End] ,AllDay ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@Title ,@TimeKey ,@WorkTime ,@WorkContent ,@DepartmentKey ,@EmployeeKey ,@Subject ,@Description ,@Start ,@End ,@AllDay ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT ID FROM HRM_ScheduleDetail WHERE ID = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@WorkTime", SqlDbType.NVarChar).Value = _WorkTime;
                zCommand.Parameters.Add("@WorkContent", SqlDbType.NVarChar).Value = _WorkContent;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = _Subject;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Start", SqlDbType.NVarChar).Value = _Start;
                zCommand.Parameters.Add("@End", SqlDbType.NVarChar).Value = _End;
                zCommand.Parameters.Add("@AllDay", SqlDbType.Bit).Value = _AllDay;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _ID = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_ScheduleDetail SET "
                        + " Title = @Title,"
                        + " TimeKey = @TimeKey,"
                        + " WorkTime = @WorkTime,"
                        + " WorkContent = @WorkContent,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " Subject = @Subject,"
                        + " Description = @Description,"
                        + " Start = @Start,"
                        + " [End] = @End,"
                        + " AllDay = @AllDay,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE ID = @ID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@WorkTime", SqlDbType.NVarChar).Value = _WorkTime;
                zCommand.Parameters.Add("@WorkContent", SqlDbType.NVarChar).Value = _WorkContent;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = _Subject;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Start", SqlDbType.NVarChar).Value = _Start;
                zCommand.Parameters.Add("@End", SqlDbType.NVarChar).Value = _End;
                zCommand.Parameters.Add("@AllDay", SqlDbType.Bit).Value = _AllDay;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ID == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_ScheduleDetail WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
