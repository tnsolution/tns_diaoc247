﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class Helper
    {
        public static DataTable Get_Default_Employee_Support(int ManagerKey)
        {
            SqlContext zSql = new SqlContext();
            return zSql.GetData(@"
SELECT EmployeeKey, DepartmentKey, LastName + ' ' + FirstName AS EmployeeName  
FROM HRM_Employees A
WHERE A.DepartmentKey = 13 AND IsWorking = 2
UNION 
SELECT EmployeeKey, DepartmentKey, LastName + ' ' + FirstName AS EmployeeName  
FROM HRM_Employees A WHERE EmployeeKey = " + ManagerKey);
        }
    }
}
