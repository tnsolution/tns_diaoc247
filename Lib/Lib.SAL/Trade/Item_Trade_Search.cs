﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class Item_Trade_Search
    {
        public int Department { get; set; } = 0;
        public int Employee { get; set; } = 0;
        public string FromDate { get; set; } = "0";
        public string ToDate { get; set; } = "0";
        public int Project { get; set; } = 0;
        public int CategoryTrade { get; set; } = 0;
        public int TradeType { get; set; } = 0;
        public string AssetID { get; set; } = "";
    }
}
