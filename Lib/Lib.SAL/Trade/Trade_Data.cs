﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class Trade_Data
    {
        public static int SumMonth(int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            zSql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

            return zSql.GetObject(@"SELECT SUM(A.HireMonth)
FROM dbo.FNC_Transaction A
WHERE (A.IsFinish = 1)
AND A.EmployeeKey = @EmployeeKey
AND A.DateApprove BETWEEN @FromDate AND @ToDate").ToInt();
        }

        public static DataTable GetSupportCommission(int TradeKey)
        {
            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@TradeKey", SqlDbType.Int).Value = TradeKey;
            return zSql.GetData(@"
SELECT A.AutoKey, A.CommisionKey, A.Money, A.TradeKey, A.EmployeeKey, A.Number, 
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
A.Description, B.ID, B.TradeCategory, B.Money, B.Number,
B.TradeType, B.Method, B.Name, B.Description
FROM FNC_Transaction_Commision A
LEFT JOIN HRM_Commission_Support B ON A.CommisionKey=B.AutoKey
WHERE A.TradeKey = @TradeKey");
        }
        public static DataTable PreInitSupport(int TradeType, int TradeCategory, int ManagerKey)
        {
            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = ManagerKey;
            zSql.CMD.Parameters.Add("@TradeType", SqlDbType.Int).Value = TradeType;
            zSql.CMD.Parameters.Add("@TradeCategory", SqlDbType.Int).Value = TradeCategory;
            return zSql.GetData(@"
SELECT EmployeeKey, DepartmentKey, 
LastName + ' ' + FirstName AS EmployeeName, 
B.[MONEY], B.Number, B.Name
FROM HRM_Employees A, HRM_Commission_Support B
WHERE A.DepartmentKey = 13 
AND IsWorking = 2 
AND B.TradeCategory = 0
UNION ALL
SELECT EmployeeKey, DepartmentKey, 
LastName + ' ' + FirstName AS EmployeeName, 
B.[MONEY], B.Number, B.Name
FROM HRM_Employees A, HRM_Commission_Support B
WHERE EmployeeKey = @ManagerKey 
AND B.TradeCategory = @TradeCategory 
AND B.TradeType =@TradeType");
        }

        public static DataTable GetCustomer(int TradeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT B.CustomerKey, B.CustomerName, B.Phone1, B.Phone2, 
B.Address1, B.Address2, B.Email1, B.Email2, A.IsOwner,
B.CardPlace, B.CardID, B.CardDate, B.Birthday,
CASE A.IsOwner 
    WHEN 1 THEN N'(Chủ nhà)' 
    WHEN 2 THEN N'(Khách thuê)' 
    WHEN 3 THEN N'(Khách mua)' 
END AS [Owner]
FROM FNC_Transaction_Customer A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.TransactionKey = @Key";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = TradeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Get(int DepartmentKey, int EmployeeKey, int TradeType, DateTime FromDate, DateTime ToDate, int Project, int Category)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, A.AssetID, A.Income, A.DateContract,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A 
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate";
            if (Project != 0)
                zSQL += " AND A.ProjectKey = @Project";
            if (Category != 0)
                zSQL += " AND A.TransactionCategory = @Category";
            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Project", SqlDbType.Int).Value = Project;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetShortTerm(int DepartmentKey, int EmployeeKey, int TradeType, DateTime FromDate, DateTime ToDate, int Project, int Category, string AssetID)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, A.AssetID, A.Income, A.DateContract, A.HireMonth,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A 
WHERE ShortTerm=1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            if (AssetID != string.Empty)
                zSQL += " AND A.AssetID = @AssetID";
            if (Project != 0)
                zSQL += " AND A.ProjectKey = @Project";
            if (Category != 0)
                zSQL += " AND A.TransactionCategory = @Category";
            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = AssetID;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Project", SqlDbType.Int).Value = Project;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get(int DepartmentKey, int EmployeeKey, int TradeType, DateTime FromDate, DateTime ToDate, int Project, int Category, string AssetID)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, 
A.AssetID, A.Income, A.DateContract, IsApproved,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address], 
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A 
WHERE 1=1";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {
                if (TradeType == 3)
                    zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
                else
                    zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }
            if (AssetID != string.Empty)
                zSQL += " AND A.AssetID = @AssetID";
            if (Project != 0)
                zSQL += " AND A.ProjectKey = @Project";
            if (Category != 0)
                zSQL += " AND A.TransactionCategory = @Category";
            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = AssetID;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Project", SqlDbType.Int).Value = Project;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetShortTerm(int DepartmentKey, int EmployeeKey, int TradeType, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, A.AssetID, A.Income, A.DateContract, A.HireMonth,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetManagerNameByDepartment(A.DepartmentKey) AS ManagerName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A 
WHERE A.ShortTerm = 1 AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Get(DateTime FromDate, DateTime ToDate, string Department, int Employee, int TradeType)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, 
A.AssetID, A.Income, A.DateContract, IsApproved,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetManagerNameByDepartment(A.DepartmentKey) AS ManagerName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A 
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate";

            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";

            if (Department != string.Empty)
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Get(int DepartmentKey, int EmployeeKey, int TradeType, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, 
A.AssetID, A.Income, A.DateContract, IsApproved,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetManagerNameByDepartment(A.DepartmentKey) AS ManagerName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A 
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate";

            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get(int DepartmentKey, int EmployeeKey, int TradeType, int TradeCategory, int Project, int AssetCategory, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, A.AssetID, A.Income, A.DateContract,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A WHERE 1=1";

            if (TradeType == 1)
                zSQL += " AND A.IsApproved = 0";
            if (TradeType == 2)
                zSQL += " AND A.IsFinish = 0";
            if (TradeType == 3)
                zSQL += " AND A.IsApproved = 1 AND A.IsFinish = 1";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Chart(DateTime FromDate, DateTime ToDate, string Department, int Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT Month(FinishDate) as MM, SUM(Income) [Money], COUNT(Income) Number
FROM FNC_Transaction 
WHERE IsApproved = 1 AND IsFinish = 1 
AND (FinishDate BETWEEN @FromDate AND @ToDate)";

            if (Employee != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey";
            if (Department != string.Empty)
                zSQL += " AND DepartmentKey IN (" + Department + ")";

            zSQL += " GROUP BY Month(FinishDate)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChartList(DateTime FromDate, DateTime ToDate, string Department, int Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, A.AssetID, A.Income, A.DateContract,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A WHERE A.IsApproved = 1 AND A.IsFinish = 1 AND (A.FinishDate BETWEEN @FromDate AND @ToDate)";
            if (Department != string.Empty)
                zSQL += " AND A.DepartmentKey IN (" + Department + ")";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;                
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable Chart(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT Month(FinishDate) as MM, SUM(Income) [Money], COUNT(Income) Number
FROM FNC_Transaction 
WHERE IsApproved = 1 AND IsFinish = 1 
AND (FinishDate BETWEEN @FromDate AND @ToDate)";

            if (Employee != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND DepartmentKey = @DepartmentKey";

            zSQL += " GROUP BY Month(FinishDate)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Chart_Mobile(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT Month(FinishDate) as MM, SUM(Income) [Money], COUNT(Income) Number,
dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName
FROM FNC_Transaction 
WHERE IsApproved = 1 AND IsFinish = 1 
AND (FinishDate BETWEEN @FromDate AND @ToDate)";

            if (Employee != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND DepartmentKey = @DepartmentKey";

            zSQL += " GROUP BY Month(FinishDate), EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChartList(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionDate, A.TransactionID, A.TransactionKey, A.AssetID, A.Income, A.DateContract,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategory,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategory
FROM FNC_Transaction A WHERE A.IsApproved = 1 AND A.IsFinish = 1 AND (A.FinishDate BETWEEN @FromDate AND @ToDate)";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY A.CreatedDate DESC";

            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int CheckExists(int TicketKey)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
            int result = sql.GetObject("SELECT A.IsDraft FROM FNC_Transaction A WHERE IsDraft = @TicketKey").ToInt();
            return result;
        }

        public static DataTable GetSupport(int Trade)
        {
            SqlContext zContext = new SqlContext();
            return zContext.GetData(@"
SELECT A.SupportFee,
dbo.FNC_SysCategoryName(A.SupportFee) AS CategoryName,  
dbo.FNC_GetNameEmployee(A.SupportBy) AS EmployeeName,
A.Money, A.Type 
FROM HRM_Support_Trade A
WHERE TradeKey = " + Trade);
        }
    }
}
