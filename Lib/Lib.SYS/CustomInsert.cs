﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SYS
{
    public class CustomInsert
    {
        public static ItemReturn Exe(string Query)
        {
            ItemReturn zResult = new ItemReturn();
            //---------- String SQL Access Database ---------------        
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(Query, zConnect);
                zResult.Result= zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                zResult.Message = ex.Message;
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
