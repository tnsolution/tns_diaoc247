﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Lib.SYS
{
    public class MyCryptography
    {
        public static string HashPass(string nPass)
        {
            HashAlgorithm Hash = HashAlgorithm.Create("SHA1");
            byte[] pwordData = Encoding.Default.GetBytes(nPass);
            byte[] bHash = Hash.ComputeHash(pwordData);
            return Convert.ToBase64String(bHash);
        }

        public static Boolean VerifyHash(string NewPass, string OldPass)
        {
            string HashNewPass = HashPass(NewPass);
            return (OldPass == HashNewPass);
        }      
    }
}
