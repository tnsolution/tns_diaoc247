﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SYS.Report
{
    public class RptHelper
    {
        public static double PreviousMonth(int Month, int Year, string Department)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;           
            string sql = @"
DECLARE @YearParamater INT;
DECLARE @MonthParamater INT
IF(@Month - 1 = 0)
    BEGIN 
        SET @YearParamater = @Year - 1 
        SET @MonthParamater = 12
    END
ELSE
    BEGIN 
        SET @YearParamater = @Year 
        SET @MonthParamater = @Month-1
    END
SELECT ISNULL(SUM(Amount),0) 
FROM FNC_CloseMonth 
WHERE CategoryKey = 1 
AND CloseFinish = 1 
AND MONTH(CloseDate) = @MonthParamater 
AND YEAR(CloseDate) = @YearParamater ";
            if (Department != string.Empty)
                sql += "AND DepartmentKey IN (" + Department + ")";
            return Convert.ToDouble(zSqlContext.GetObject(sql));
        }

        public static double PreviousMonth(int Month, int Year, int Department)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            string sql = @"
DECLARE @YearParamater INT;
DECLARE @MonthParamater INT
IF(@Month - 1 = 0)
    BEGIN 
        SET @YearParamater = @Year - 1 
        SET @MonthParamater = 12
    END
ELSE
    BEGIN 
        SET @YearParamater = @Year 
        SET @MonthParamater = @Month-1
    END
SELECT ISNULL(SUM(Amount),0) 
FROM FNC_CloseMonth 
WHERE CategoryKey = 1 
AND CloseFinish = 1 
AND MONTH(CloseDate) = @MonthParamater 
AND YEAR(CloseDate) = @YearParamater ";
            if (Department != 0)
                sql += "AND DepartmentKey = @Department";
            return Convert.ToDouble(zSqlContext.GetObject(sql));
        }
        public static double PreviousMonth(int Month, int Year)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToDouble(zSqlContext.GetObject(@"
DECLARE @YearParamater INT;
DECLARE @MonthParamater INT
IF(@Month - 1 = 0)
    BEGIN 
        SET @YearParamater = @Year - 1 
        SET @MonthParamater = 12
    END
ELSE
    BEGIN 
        SET @YearParamater = @Year 
        SET @MonthParamater = @Month - 1
    END
SELECT ISNULL(SUM(Amount),0) 
FROM FNC_CloseMonth 
WHERE CategoryKey = 1 
AND CloseFinish = 1 
AND MONTH(CloseDate) = @MonthParamater 
AND YEAR(CloseDate) = @YearParamater"));
        }
        public static double PreviousMonth_Play(int Month, int Year, string Department)
        {
            string SQL = @"
DECLARE @YearParamater INT;
DECLARE @MonthParamater INT
IF(@Month - 1 = 0)
    BEGIN 
        SET @YearParamater = @Year - 1 
        SET @MonthParamater = 12
    END
ELSE
    BEGIN 
        SET @YearParamater = @Year 
        SET @MonthParamater = @Month-1
    END
SELECT ISNULL(SUM(Amount),0) 
FROM FNC_CloseMonth WHERE CategoryKey = 2 
AND CloseFinish = 1 
AND MONTH(CloseDate) = @MonthParamater 
AND YEAR(CloseDate) = @YearParamater";
            if (Department != string.Empty)
            {
                SQL += " AND DepartmentKey IN (" + Department + ")";
            }

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToDouble(zSqlContext.GetObject(SQL));

        }
        public static double TotalReceipt(int Month, int Year, int Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND MONTH(ReceiptDate) = @Month AND YEAR(ReceiptDate)=@Year AND IsApproved=1";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }

        public static double TotalReceipt(int Month, int Year, string Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND MONTH(ReceiptDate) = @Month AND YEAR(ReceiptDate)=@Year AND IsApproved=1";
            if (Department != string.Empty)
                Sql += " AND DepartmentKey IN (" + Department + ")";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }

        public static double TotalPayment(int Month, int Year, string Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND MONTH(PaymentDate) = @Month AND YEAR(PaymentDate)=@Year AND IsApproved=1 AND IsApproved2=1";
            if (Department != string.Empty)
                Sql += " AND DepartmentKey IN (" + Department + ")";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }

        public static double TotalPayment(int Month, int Year, int Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND MONTH(PaymentDate) = @Month AND YEAR(PaymentDate)=@Year AND IsApproved=1 AND IsApproved2=1";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }

        public static double TotalBalance(int Month, int Year)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToDouble(zSqlContext.GetObject("SELECT ISNULL(dbo.FNC_GetFunds(@Month, @Year),0)"));
        }
        public static double TotalBalanceCapital(DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSqlContext.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            return zSqlContext.GetObject(@"
SELECT ISNULL(SUM(A.Amount),0) AS PreviousMonth 
FROM FNC_Capital_CloseMonth A 
WHERE A.CloseDate BETWEEN @FromDate AND @ToDate").ToDouble();
        }
    }
}
