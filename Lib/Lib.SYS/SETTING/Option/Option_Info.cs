﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SYS
{
    public class Option_Info
    {

        #region [ Field Name ]
        private int _OptionKey = 0;
        private string _OptionName = "";
        private string _OptionSummarize = "";
        private int _CategoryKey = 0;
        private int _Status = 0;
        private int _Lever = 0;
        private int _Value = 0;
        private int _Parent = 0;
        private string _CreatedBy = "";
        private string _CreatedDate;
        private string _ModifiedBy = "";
        private string _ModifiedDate;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int OptionKey
        {
            get { return _OptionKey; }
            set { _OptionKey = value; }
        }
        public string OptionName
        {
            get { return _OptionName; }
            set { _OptionName = value; }
        }
        public string OptionSummarize
        {
            get { return _OptionSummarize; }
            set { _OptionSummarize = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public int Lever
        {
            get { return _Lever; }
            set { _Lever = value; }
        }
        public int Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Option_Info()
        {
        }
        public Option_Info(int OptionKey)
        {
            string zSQL = "SELECT * FROM SYS_Option WHERE OptionKey = @OptionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OptionKey", SqlDbType.Int).Value = OptionKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["OptionKey"] != DBNull.Value)
                        _OptionKey = int.Parse(zReader["OptionKey"].ToString());
                    _OptionName = zReader["OptionName"].ToString();
                    _OptionSummarize = zReader["OptionSummarize"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["Status"] != DBNull.Value)
                        _Status = int.Parse(zReader["Status"].ToString());
                    if (zReader["Lever"] != DBNull.Value)
                        _Lever = int.Parse(zReader["Lever"].ToString());
                    if (zReader["Value"] != DBNull.Value)
                        _Value = int.Parse(zReader["Value"].ToString());
                    if (zReader["Parent"] != DBNull.Value)
                        _Parent = int.Parse(zReader["Parent"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    //_CreatedDate = Convert.ToDateTime((DateTime)zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    //_ModifiedDate = Convert.ToDateTime((DateTime)zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Option ("
        + " OptionName ,OptionSummarize ,CategoryKey ,Status ,Lever, Value ,Parent ,CreatedBy ,CreatedDate ,ModifiedBy ,ModifiedDate ) "
         + " VALUES ( "
         + "@OptionName ,@OptionSummarize ,@CategoryKey ,@Status ,@Lever, @Value ,@Parent ,@CreatedBy ,GETDATE(),@ModifiedBy ,GETDATE()) "
         + " SELECT OptionKey FROM SYS_Option WHERE OptionKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OptionKey", SqlDbType.Int).Value = _OptionKey;
                zCommand.Parameters.Add("@OptionName", SqlDbType.NVarChar).Value = _OptionName;
                zCommand.Parameters.Add("@OptionSummarize", SqlDbType.NVarChar).Value = _OptionSummarize;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@Lever", SqlDbType.Int).Value = _Lever;
                zCommand.Parameters.Add("@Value", SqlDbType.Int).Value = _Value;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE SYS_Option SET "
                        + " OptionName = @OptionName,"
                        + " OptionSummarize = @OptionSummarize,"
                        + " CategoryKey = @CategoryKey,"
                        + " Status = @Status,"
                        + " Lever = @Lever,"
                        + " Value = @Value,"
                        + " Parent = @Parent,"
                        + " CreatedBy = @CreatedBy,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedDate = GETDATE()"
                       + " WHERE OptionKey = @OptionKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OptionKey", SqlDbType.Int).Value = _OptionKey;
                zCommand.Parameters.Add("@OptionName", SqlDbType.NVarChar).Value = _OptionName;
                zCommand.Parameters.Add("@OptionSummarize", SqlDbType.NVarChar).Value = _OptionSummarize;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@Lever", SqlDbType.Int).Value = _Lever;
                zCommand.Parameters.Add("@Value", SqlDbType.Int).Value = _Value;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
              
            
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
               
  
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_OptionKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Option WHERE OptionKey = @OptionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OptionKey", SqlDbType.Int).Value = _OptionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
