﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.SYS
{
    public class ItemUser
    {
        public string Message = "";
        public string MessageCode = "";
        public string UserKey = "";
        public string UserName = "";
        public string UnitLevel = "0";
        public string ManagerKey = "0";
        public string EmployeeName = "";
        public string EmployeeKey = "0";
        public string DepartmentKey = "0";
        public string DepartmentName = "";
        public string DepartmentRole = "";
        public string ImageThumb = "";
        public string SpecKey = "0";
    }
}
