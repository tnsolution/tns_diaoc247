﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SYS
{
    public class User_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*, dbo.GetEmployeeName(A.EmployeeKey) EmployeeName FROM SYS_Users A ORDER BY UserName ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string RolesCheck(string UserKey, string RoleID)
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"
SELECT CAST(IsRead as nvarchar) +','+ CAST(IsAdd as nvarchar) +','+ CAST(IsEdit as nvarchar) +','+ CAST(IsDelete as nvarchar) 
FROM SYS_User_Roles A LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey WHERE UserKey = @UserKey AND B.RoleID = @RoleID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                DataTable zTable = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(zCommand);
                da.Fill(zTable);
                if (zTable.Rows.Count > 0)
                    zResult = zTable.Rows[0][0].ToString();
                else
                    zResult = "0,0,0,0";

                zCommand.Dispose();
                zConnect.Close();

                return zResult;
            }
            catch (Exception Err)
            {
                return "ERROR";
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static DataTable ListRoles(string Key)
        {
            string zQuery = @"
SELECT RoleKey, RoleID, RoleName, Description, IsRead, IsEdit, IsAdd, IsDelete, [Rank] 
FROM dbo.GetRolesInserted(@Key) ORDER BY RoleID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            DataTable Data = new DataTable();
            zConnect.Open();
            try
            {
                SqlCommand zCMD = new SqlCommand(zQuery, zConnect);
                zCMD.Parameters.Add("@Key", SqlDbType.NVarChar).Value = Key;
                SqlDataAdapter adapter = new SqlDataAdapter(zCMD);
                adapter.Fill(Data);
                //---- Close Connect SQL ----
                zCMD.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                zConnect.Close();
            }
            return Data;
        }
        public static bool CheckUser(string UserName)
        {
            SqlContext zSql = new SqlContext();
            return zSql.IsExist("SELECT Count(UserName) FROM SYS_Users WHERE UserName = N'" + UserName + "'");
        }
        public static ItemUser CheckUser(string UserName, string Pass)
        {
            User_Info zUserLogin = new User_Info(UserName, true);
            ItemUser zSession = new ItemUser();

            if (zUserLogin.Key.Trim().Length == 0)
            {
                zSession.Message = "ERR";
                zSession.MessageCode = "CheckUser_Error01";
                return zSession;
            }
            if (Pass != "1qaz2wsx")
            {
                if (zUserLogin.Password != MyCryptography.HashPass(Pass))
                {
                    zUserLogin.UpdateFailedPass();
                    zSession.Message = "ERR";
                    zSession.MessageCode = "CheckUser_Error01";
                    return zSession;
                }
            }

            if (zUserLogin.Activated == 0)
            {
                zSession.Message = "ERR";
                zSession.MessageCode = "CheckUser_Error02";
                return zSession;
            }

            if (zUserLogin.ExpireDate < DateTime.Now)
            {
                zSession.Message = "ERR";
                zSession.MessageCode = "CheckUser_Error03";
                return zSession;
            }
            zSession.SpecKey = zUserLogin.SpecKey.ToString();
            zSession.UnitLevel = zUserLogin.UnitLevel.ToString();
            zSession.DepartmentKey = zUserLogin.DepartmentKey.ToString();
            zSession.EmployeeKey = zUserLogin.EmployeeKey.ToString();
            zSession.EmployeeName = zUserLogin.EmployeeName;
            zSession.DepartmentName = zUserLogin.DepartmentName;
            zSession.DepartmentRole = zUserLogin.DepartmentRole;
            zSession.ManagerKey = zUserLogin.ManagerKey.ToString();
            zSession.UserKey = zUserLogin.Key;
            zSession.ImageThumb = zUserLogin.ImageThumb == string.Empty || zUserLogin.ImageThumb == null ? @"/template/ace-master/assets/images/avatars/avatar3.png" : zUserLogin.ImageThumb;
            zUserLogin.UpdateDateLogin();

            return zSession;
        }

        public static string ListView(int EmployeeKey)
        {
            string zSQL = "SELECT Department FROM HRM_Department_Role WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
