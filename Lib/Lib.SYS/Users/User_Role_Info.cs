﻿using System;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;

namespace Lib.SYS
{
    public class User_Roles_Info
    {
        private string _AutoID = "";
        private string _UserKey = "";
        private string _UserName = "";

        private string _RoleKey = "";
        private string _RoleID = "";
        private string _RoleName = "";

        private int _IsRead;
        private int _IsAdd;
        private int _IsEdit;
        private int _IsDelete;

        private string _Message = "";

        #region [Properties ]

        public string UserKey
        {
            set { _UserKey = value; }
            get { return _UserKey; }
        }
        public string UserName
        {
            set { _UserName = value; }
            get { return _UserName; }
        }     
        public string ID
        {
            set
            {
                _RoleID = value;
            }
            get { return _RoleID; }
        }
        public string Name
        {
            set { _RoleName = value; }
            get { return _RoleName; }
        }

        public int Read
        {
            set { _IsRead = value; }
            get { return _IsRead; }
        }
        public int Add
        {
            set { _IsAdd = value; }
            get { return _IsAdd; }
        }

        public int Edit
        {
            set { _IsEdit = value; }
            get { return _IsEdit; }
        }
        public int Del
        {
            set { _IsDelete = value; }
            get { return _IsDelete; }
        }


        public string Message
        {
            set { _Message = value; }
            get { return _Message; }
        }

        public string RoleKey
        {
            get
            {
                return _RoleKey;
            }

            set
            {
                _RoleKey = value;
            }
        }

        public string AutoID
        {
            get
            {
                return _AutoID;
            }

            set
            {
                _AutoID = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]

        public User_Roles_Info()
        {
        }

        public User_Roles_Info(string UserKey, string RoleKey)
        {
            string zSQL = " SELECT A.*,B.RoleID,B.RoleName,C.UserName FROM SYS_User_Roles A"
                        + " LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey "
                        + " LEFT JOIN SYS_Users C ON A.UserKey = C.UserKey "
                        + " WHERE A.UserKey = @UserKey AND A.RoleKey = @RoleKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _AutoID = nReader["ID"].ToString();
                    _UserKey = nReader["UserKey"].ToString();
                    _UserName = nReader["UserName"].ToString();

                    _RoleKey = nReader["RoleKey"].ToString();
                    _RoleID = nReader["RoleID"].ToString();
                    _RoleName = nReader["RoleName"].ToString();

                    _IsRead = (int)nReader["IsRead"];
                    _IsAdd = (int)nReader["IsAdd"];
                    _IsEdit = (int)nReader["IsEdit"];
                    _IsDelete = (int)nReader["IsDelete"];
                }
                nReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }

        public void Check_Role(string RoleID)
        {
            string zSQL = " SELECT * FROM SYS_User_Roles A"
                        + " INNER JOIN SYS_Role B ON A.RoleKey = B.RoleKey "
                        + " WHERE A.UserKey = @UserKey AND B.RoleID = @RoleID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);

            try
            {
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _UserKey = nReader["UserKey"].ToString();
                    RoleKey = nReader["RoleKey"].ToString();
                    _RoleID = nReader["RoleID"].ToString();
                    _RoleName = nReader["RoleName"].ToString();

                    _IsRead = (int)nReader["IsRead"];
                    _IsAdd = (int)nReader["IsAdd"];
                    _IsEdit = (int)nReader["IsEdit"];
                    _IsDelete = (int)nReader["IsDelete"];
                }
                else
                {
                    _IsRead = 0;
                    _IsAdd = 0;
                    _IsEdit = 0;
                    _IsDelete = 0;
                }
                nReader.Close();
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
        #endregion

        #region [ Constructor Update Information ]

        public string Update()
        {
            string zResult = ""; ;

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User_Roles SET "

                        + " IsRead = @IsRead,"
                        + " IsAdd = @IsAdd,"
                        + " IsEdit = @IsEdit,"
                        + " IsDelete = @IsDelete"

                        + " WHERE UserKey = @UserKey AND RoleKey = @RoleKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;

                zCommand.Parameters.Add("@IsRead", SqlDbType.Int).Value = _IsRead;
                zCommand.Parameters.Add("@IsAdd", SqlDbType.Int).Value = _IsAdd;
                zCommand.Parameters.Add("@IsEdit", SqlDbType.Int).Value = _IsEdit;
                zCommand.Parameters.Add("@IsDelete", SqlDbType.Int).Value = _IsDelete;

                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User_Roles("
                        + " UserKey, RoleKey, IsRead,IsAdd, IsEdit,IsDelete)"
                        + " VALUES(@UserKey, @RoleKey, @IsRead,@IsAdd, @IsEdit,@IsDelete)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;

                zCommand.Parameters.Add("@IsRead", SqlDbType.Int).Value = _IsRead;
                zCommand.Parameters.Add("@IsAdd", SqlDbType.Int).Value = _IsAdd;
                zCommand.Parameters.Add("@IsEdit", SqlDbType.Int).Value = _IsEdit;
                zCommand.Parameters.Add("@IsDelete", SqlDbType.Int).Value = _IsDelete;


                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User_Roles WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteAll()
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User_Roles WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }    
        #endregion
    }
}
