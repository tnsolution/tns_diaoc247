﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Excel_Detail_Info
    {
        #region [ Field Name ]
        private string _ObjectKey = "0"; // Key liên kết với san pham
        private string _ObjectTable = "";
        private int _DetailKey = 0; // note table
        private int _Key = 0;
        private int _ExcelKey = 0;
        private DateTime _InfoDate;
        private string _SirName = "";
        private string _LastName = "";
        private string _FirstName = "";
        private string _FullName = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Email1 = "";
        private string _Email2 = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _ProductID = "";
        private string _Product = "";
        private string _Asset = "";
        private string _Description = "";

        private string _ID1 = "";
        private string _ID2 = "";
        private string _ID3 = "";
        private string _AreaWall = "";
        private string _AreaWater = "";
        private string _Price = "";
        private string _AssetCategory = "";
        private string _Room = "";
        private string _DirectionView = "";
        private string _DirectionDoor = "";

        private string _AssetType = "";
        private string _AssetTable = "";

        private string _Status = "";
        private int _StatusKey = 0;
        private string _StatusProcess = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";

        private string _EmployeePhone = "";
        private string _EmployeeName = "";
        private int _EmployeeKey = 0;
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _Key; }
            set { _Key = value; }
        }
        public int ExcelKey
        {
            get { return _ExcelKey; }
            set { _ExcelKey = value; }
        }
        public DateTime InfoDate
        {
            get { return _InfoDate; }
            set { _InfoDate = value; }
        }
        public string SirName
        {
            get { return _SirName; }
            set { _SirName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }
        public string Email2
        {
            get { return _Email2; }
            set { _Email2 = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string Product
        {
            get { return _Product; }
            set { _Product = value; }
        }
        public string Asset
        {
            get { return _Asset; }
            set { _Asset = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public int DetailKey
        {
            get
            {
                return _DetailKey;
            }

            set
            {
                _DetailKey = value;
            }
        }

        public string StatusProcess
        {
            get
            {
                return _StatusProcess;
            }

            set
            {
                _StatusProcess = value;
            }
        }

        public int StatusKey
        {
            get
            {
                return _StatusKey;
            }

            set
            {
                _StatusKey = value;
            }
        }

        public string FullName
        {
            get
            {
                return _FullName;
            }

            set
            {
                _FullName = value;
            }
        }

        public string AreaWater
        {
            get
            {
                return _AreaWater;
            }

            set
            {
                _AreaWater = value;
            }
        }

        public string AreaWall
        {
            get
            {
                return _AreaWall;
            }

            set
            {
                _AreaWall = value;
            }
        }

        public string ID3
        {
            get
            {
                return _ID3;
            }

            set
            {
                _ID3 = value;
            }
        }

        public string ID2
        {
            get
            {
                return _ID2;
            }

            set
            {
                _ID2 = value;
            }
        }

        public string ID1
        {
            get
            {
                return _ID1;
            }

            set
            {
                _ID1 = value;
            }
        }

        public string Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }

        public string AssetCategory
        {
            get
            {
                return _AssetCategory;
            }

            set
            {
                _AssetCategory = value;
            }
        }

        public string Room
        {
            get
            {
                return _Room;
            }

            set
            {
                _Room = value;
            }
        }

        public string DirectionView
        {
            get
            {
                return _DirectionView;
            }

            set
            {
                _DirectionView = value;
            }
        }

        public string DirectionDoor
        {
            get
            {
                return _DirectionDoor;
            }

            set
            {
                _DirectionDoor = value;
            }
        }

        public string ObjectKey
        {
            get
            {
                return _ObjectKey;
            }

            set
            {
                _ObjectKey = value;
            }
        }

        public string ObjectTable
        {
            get
            {
                return _ObjectTable;
            }

            set
            {
                _ObjectTable = value;
            }
        }

        public string AssetType
        {
            get
            {
                return _AssetType;
            }

            set
            {
                _AssetType = value;
            }
        }

        public string AssetTable
        {
            get
            {
                return _AssetTable;
            }

            set
            {
                _AssetTable = value;
            }
        }

        public int EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }

        public string EmployeePhone
        {
            get
            {
                return _EmployeePhone;
            }

            set
            {
                _EmployeePhone = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Excel_Detail_Info()
        {
        }
        public Excel_Detail_Info(int InfoKey)
        {
            string zSQL = "SELECT * FROM TASK_Excel_Detail WHERE InfoKey = @InfoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = InfoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["InfoKey"] != DBNull.Value)
                        _Key = int.Parse(zReader["InfoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["InfoDate"] != DBNull.Value)
                        _InfoDate = (DateTime)zReader["InfoDate"];
                    _SirName = zReader["SirName"].ToString();
                    _LastName = zReader["LastName"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    _FullName = zReader["FullName"].ToString();
                    _Phone1 = zReader["Phone1"].ToString();
                    _Phone2 = zReader["Phone2"].ToString();
                    _Email1 = zReader["Email1"].ToString();
                    _Email2 = zReader["Email2"].ToString();
                    _Address1 = zReader["Address1"].ToString();
                    _Address2 = zReader["Address2"].ToString();
                    _ProductID = zReader["ProductID"].ToString();
                    _Product = zReader["Product"].ToString();
                    _Asset = zReader["Asset"].ToString();
                    _AssetType = zReader["AssetType"].ToString();
                    _AssetTable = zReader["AssetTable"].ToString();
                    _Description = zReader["Description"].ToString();
                    _Status = zReader["Status"].ToString();
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="InfoKey"></param>
        /// <param name="Type">1 Thông tin gửi, 2 chưa gửi</param>
        public Excel_Detail_Info(int InfoKey, int Type)
        {
            string zSQL = "";
            if (Type == 1)
            {
                zSQL = @"  
SELECT A.DetailKey, B.InfoKey, B.SirName, B.LastName, B.FirstName, B.FullName, B.AssetType, B.AssetTable,
B.ID1, B.ID2, B.ID3, B.AreaWall, B.AreaWater, B.Price, B.AssetCategory, B.Room, B.DirectionView, B.DirectionDoor,
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, B.Product, B.Asset, B.Description,
dbo.FNC_GetStatusInfoData(A.StatusInfo) AS [Status], A.StatusInfo AS StatusKey, C.SendTo AS EmployeeKey,
dbo.FNC_GetNameEmployee(C.SendTo) AS EmployeeName, B.AssetType, B.AssetTable, B.ProductID,
dbo.FNC_GetEmployeePhone(C.SendTo) AS EmployeePhone,
B.CreatedDate, B.CreatedBy, B.CreatedName, B.ModifiedDate, B.ModifiedBy, B.ModifiedName, A.ObjectKey, A.ObjectTable
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey 
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey
WHERE A.DetailKey = @InfoKey";
            }
            else
            {
                zSQL = @"  
SELECT 0 DetailKey,A.InfoKey, A.SirName, A.LastName, A.FirstName, A.FullName, A.AssetType, A.AssetTable,
A.ID1, A.ID2, A.ID3, A.AreaWall, A.AreaWater, A.Price, A.AssetCategory, A.Room, A.DirectionView, A.DirectionDoor,
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, A.Product, A.Asset, A.Description, A.AssetType, A.AssetTable, '' ProductID,
CASE A.Status WHEN 0 THEN N'Thông tin này chưa gửi đi' END AS [Status], -1 AS StatusKey, 
'' EmployeeKey, '' AS EmployeeName, '' AS AssetType, '' AS AssetTable, '' AS EmployeePhone,
A.CreatedDate, A.CreatedBy, A.CreatedName, A.ModifiedDate, A.ModifiedBy, A.ModifiedName, 0 AS ObjectKey, '' AS ObjectTable
FROM TASK_Excel_Detail A
WHERE InfoKey = @InfoKey";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = InfoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["DetailKey"] != DBNull.Value)
                        _DetailKey = int.Parse(zReader["DetailKey"].ToString());
                    if (zReader["InfoKey"] != DBNull.Value)
                        _Key = int.Parse(zReader["InfoKey"].ToString());
                    _SirName = zReader["SirName"].ToString();
                    _LastName = zReader["LastName"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    _FullName = zReader["FullName"].ToString();
                    _Phone1 = zReader["Phone1"].ToString();
                    _Phone2 = zReader["Phone2"].ToString();
                    _Email1 = zReader["Email1"].ToString();
                    _Email2 = zReader["Email2"].ToString();
                    _Address1 = zReader["Address1"].ToString();
                    _Address2 = zReader["Address2"].ToString();
                    _EmployeePhone = zReader["EmployeePhone"].ToString();
                    _ID1 = zReader["ID1"].ToString();
                    _ID2 = zReader["ID2"].ToString();
                    _ID3 = zReader["ID3"].ToString();
                    _ProductID = zReader["ProductID"].ToString();
                    _AreaWall = zReader["AreaWall"].ToString();
                    _AreaWater = zReader["AreaWater"].ToString();
                    _AssetCategory = zReader["AssetCategory"].ToString();
                    _Room = zReader["Room"].ToString();
                    _DirectionView = zReader["DirectionView"].ToString();
                    _DirectionDoor = zReader["DirectionDoor"].ToString();
                    _ObjectKey = zReader["ObjectKey"].ToString();
                    _ObjectTable = zReader["ObjectTable"].ToString();
                    _AssetType = zReader["AssetType"].ToString();
                    _AssetTable = zReader["AssetTable"].ToString();
                    _Product = zReader["Product"].ToString();
                    _Asset = zReader["Asset"].ToString();
                    _Description = zReader["Description"].ToString();
                    _Status = zReader["Status"].ToString();
                    _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        //public string Create()
        //{
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "INSERT INTO TASK_Excel_Detail ("
        //    + " ExcelKey,SirName ,LastName ,FirstName, FullName ,Phone1 ,Phone2 ,Email1 ,Email2 ,Address1 ,Address2 ,ProductID ,Product ,Asset ,Description ,Status ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
        //    + " VALUES ( "
        //    + "@ExcelKey,@SirName ,@LastName ,@FirstName, @FullName ,@Phone1 ,@Phone2 ,@Email1 ,@Email2 ,@Address1 ,@Address2 ,@ProductID ,@Product ,@Asset ,@Description ,@Status ,GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName ) "
        //    + " SELECT InfoKey FROM TASK_Excel_Detail WHERE InfoKey = SCOPE_IDENTITY() ";
        //    string zResult = "";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _Key;
        //        zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
        //        zCommand.Parameters.Add("@SirName", SqlDbType.NVarChar).Value = _SirName;
        //        zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
        //        zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
        //        zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName;
        //        zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
        //        zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
        //        zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
        //        zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
        //        zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
        //        zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
        //        zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
        //        zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = _Product;
        //        zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = _Asset;
        //        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
        //        zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
        //        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
        //        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
        //        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
        //        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
        //        _Key = int.Parse(zCommand.ExecuteScalar().ToString());
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Err)
        //    {
        //        _Message = Err.ToString();
        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        //public string Update()
        //{
        //    string zSQL = "UPDATE TASK_Excel_Detail SET "
        //                + " SirName = @SirName,"
        //                + " LastName = @LastName,"
        //                + " FirstName = @FirstName, FullName = @FullName,"
        //                + " Phone1 = @Phone1,"
        //                + " Phone2 = @Phone2,"
        //                + " Email1 = @Email1,"
        //                + " Email2 = @Email2,"
        //                + " Address1 = @Address1,"
        //                + " Address2 = @Address2,"
        //                + " Product = @Product,"
        //                + " Asset = @Asset,"
        //                + " Description = @Description,"
        //                + " Status = @Status,"
        //                + " ModifiedDate = GETDATE(),"
        //                + " ModifiedBy = @ModifiedBy,"
        //                + " ModifiedName = @ModifiedName"
        //               + " WHERE InfoKey = @InfoKey";
        //    string zResult = "";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _Key;
        //        zCommand.Parameters.Add("@SirName", SqlDbType.NVarChar).Value = _SirName;
        //        zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
        //        zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
        //        zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName;
        //        zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
        //        zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
        //        zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
        //        zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
        //        zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
        //        zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
        //        zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = _ProductID;
        //        zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = _Product;
        //        zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = _Asset;
        //        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
        //        zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
        //        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
        //        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
        //        zResult = zCommand.ExecuteNonQuery().ToString();
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Err)
        //    {
        //        _Message = Err.ToString();
        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}

        //public string Save()
        //{
        //    string zResult;
        //    if (_Key == 0)
        //        zResult = Create();
        //    else
        //        zResult = Update();
        //    return zResult;
        //}
        //public string Delete()
        //{
        //    string zResult = "";
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "DELETE FROM TASK_Excel_Detail WHERE InfoKey = @InfoKey";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _Key;
        //        zResult = zCommand.ExecuteNonQuery().ToString();
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Err)
        //    {
        //        _Message = Err.ToString();
        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}
        public string UpdateStatus()
        {
            string zSQL = "UPDATE TASK_Excel_Detail SET "
                        + " Status = @Status,"
                        + " ModifiedDate = GETATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE InfoKey = @InfoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _Key;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
