﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Infomation_Employees_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _InfoKey = 0;
        private int _EmployeeKey = 0;
        private string _EmployeeName = "";
        private DateTime _DateTransfer;
        private DateTime _DateRespone;
        private int _StatusKey = 0;
        private string _Status = "";
        private string _Note = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Infomation_Employees_Info()
        {
        }
        public Infomation_Employees_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM TASK_Employees WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["InfoKey"] != DBNull.Value)
                        _InfoKey = int.Parse(zReader["InfoKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["DateTransfer"] != DBNull.Value)
                        _DateTransfer = (DateTime)zReader["DateTransfer"];
                    if (zReader["DateRespone"] != DBNull.Value)
                        _DateRespone = (DateTime)zReader["DateRespone"];
                    if (zReader["StatusKey"] != DBNull.Value)
                        _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    _Status = zReader["Status"].ToString();
                    _Note = zReader["Note"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Infomation_Employees_Info(int InfoKey, int EmployeeKey)
        {
            string zSQL = "SELECT * FROM TASK_Employees WHERE InfoKey = @InfoKey AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = InfoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["InfoKey"] != DBNull.Value)
                        _InfoKey = int.Parse(zReader["InfoKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["DateTransfer"] != DBNull.Value)
                        _DateTransfer = (DateTime)zReader["DateTransfer"];
                    if (zReader["DateRespone"] != DBNull.Value)
                        _DateRespone = (DateTime)zReader["DateRespone"];
                    if (zReader["StatusKey"] != DBNull.Value)
                        _StatusKey = int.Parse(zReader["StatusKey"].ToString());
                    _Status = zReader["Status"].ToString();
                    _Note = zReader["Note"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int InfoKey
        {
            get { return _InfoKey; }
            set { _InfoKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public DateTime DateTransfer
        {
            get { return _DateTransfer; }
            set { _DateTransfer = value; }
        }
        public DateTime DateRespone
        {
            get { return _DateRespone; }
            set { _DateRespone = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TASK_Employees ("
        + " InfoKey ,EmployeeKey ,EmployeeName ,DateTransfer ,DateRespone ,StatusKey ,Status ,Note ) "
         + " VALUES ( "
         + "@InfoKey ,@EmployeeKey ,@EmployeeName ,@DateTransfer ,@DateRespone ,@StatusKey ,@Status ,@Note ) "
         + " SELECT AutoKey FROM TASK_Employees WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _InfoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                if (_DateTransfer.Year == 0001)
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = _DateTransfer;
                if (_DateRespone.Year == 0001)
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = _DateRespone;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = _Status;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE TASK_Employees SET "
                        + " InfoKey = @InfoKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " DateTransfer = @DateTransfer,"
                        + " DateRespone = @DateRespone,"
                        + " StatusKey = @StatusKey,"
                        + " Status = @Status,"
                        + " Note = @Note"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _InfoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                if (_DateTransfer.Year == 0001)
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateTransfer", SqlDbType.DateTime).Value = _DateTransfer;
                if (_DateRespone.Year == 0001)
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = _DateRespone;
                zCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = _StatusKey;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = _Status;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Employees WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
