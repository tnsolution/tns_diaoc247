﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.TASK
{
    public class ItemSearch_Info
    {
        private int _Status = 0;
        private int _Type = 0;
        private int _Employee = 0;
        private string _CustomerName = "";
        private string _ProjectName = "";
        private string _ProjectKey = "";

        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public int Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public int Employee
        {
            get
            {
                return _Employee;
            }

            set
            {
                _Employee = value;
            }
        }

        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }
    }
}
