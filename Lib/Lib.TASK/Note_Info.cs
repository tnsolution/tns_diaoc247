﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Note_Info
    {
        #region [ Field Name ]
        private int _NoteKey = 0;
        private DateTime _NoteDate;
        private int _NoteStatus = 0;
        private int _SendFrom = 0;
        private int _SendTo = 0;
        private string _Description = "";
        private float _Amount;
        private string _Product = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int NoteKey
        {
            get { return _NoteKey; }
            set { _NoteKey = value; }
        }
        public DateTime NoteDate
        {
            get { return _NoteDate; }
            set { _NoteDate = value; }
        }
        public int NoteStatus
        {
            get { return _NoteStatus; }
            set { _NoteStatus = value; }
        }
        public int SendFrom
        {
            get { return _SendFrom; }
            set { _SendFrom = value; }
        }
        public int SendTo
        {
            get { return _SendTo; }
            set { _SendTo = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public float Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public string Product
        {
            get { return _Product; }
            set { _Product = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Note_Info()
        {
        }
        public Note_Info(int NoteKey)
        {
            string zSQL = "SELECT * FROM TASK_Note WHERE NoteKey = @NoteKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = NoteKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["NoteKey"] != DBNull.Value)
                        _NoteKey = int.Parse(zReader["NoteKey"].ToString());
                    if (zReader["NoteDate"] != DBNull.Value)
                        _NoteDate = (DateTime)zReader["NoteDate"];
                    if (zReader["NoteStatus"] != DBNull.Value)
                        _NoteStatus = int.Parse(zReader["NoteStatus"].ToString());
                    if (zReader["SendFrom"] != DBNull.Value)
                        _SendFrom = int.Parse(zReader["SendFrom"].ToString());
                    if (zReader["SendTo"] != DBNull.Value)
                        _SendTo = int.Parse(zReader["SendTo"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = float.Parse(zReader["Amount"].ToString());
                    _Product = zReader["Product"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TASK_Note ("
        + " NoteDate ,NoteStatus ,SendFrom ,SendTo ,Description ,Amount ,Product ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@NoteDate ,@NoteStatus ,@SendFrom ,@SendTo ,@Description ,@Amount ,@Product ,@CreatedDate ,@CreatedBy ,@CreatedName ,@ModifiedDate ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT NoteKey FROM TASK_Note WHERE NoteKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                if (_NoteDate.Year == 0001)
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = _NoteDate;
                zCommand.Parameters.Add("@NoteStatus", SqlDbType.Int).Value = _NoteStatus;
                zCommand.Parameters.Add("@SendFrom", SqlDbType.Int).Value = _SendFrom;
                zCommand.Parameters.Add("@SendTo", SqlDbType.Int).Value = _SendTo;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = _Amount;
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = _Product;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _NoteKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE TASK_Note SET "
                        + " NoteDate = @NoteDate,"
                        + " NoteStatus = @NoteStatus,"
                        + " SendFrom = @SendFrom,"
                        + " SendTo = @SendTo,"
                        + " Description = @Description,"
                        + " Amount = @Amount,"
                        + " Product = @Product,"
                        + " CreatedDate = @CreatedDate,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedName = @CreatedName,"
                        + " ModifiedDate = @ModifiedDate,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE NoteKey = @NoteKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                if (_NoteDate.Year == 0001)
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = _NoteDate;
                zCommand.Parameters.Add("@NoteStatus", SqlDbType.Int).Value = _NoteStatus;
                zCommand.Parameters.Add("@SendFrom", SqlDbType.Int).Value = _SendFrom;
                zCommand.Parameters.Add("@SendTo", SqlDbType.Int).Value = _SendTo;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Float).Value = _Amount;
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = _Product;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_NoteKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Note WHERE NoteKey = @NoteKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
