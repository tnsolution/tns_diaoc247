﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Report_Info
    {
        #region [ Field Name ]
        private int _ReportKey = 0;
        private int _EmployeeKey = 0;
        private string _EmployeeName = "";
        private int _ManagerKey = 0;
        private int _DepartmentKey = 0;
        private DateTime _ReportDate;
        private string _Description = "";
        private string _UnitID = "";
        private string _Title = "";
        private string _Note = "";
        private int _IsDraft = 0;

        private string _Start = "";
        private string _End = "";
        private bool _AllDay = true;

        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _ReportKey; }
            set { _ReportKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int ManagerKey
        {
            get { return _ManagerKey; }
            set { _ManagerKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public DateTime ReportDate
        {
            get { return _ReportDate; }
            set { _ReportDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string UnitID
        {
            get { return _UnitID; }
            set { _UnitID = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public int IsDraft
        {
            get { return _IsDraft; }
            set { _IsDraft = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string Start
        {
            get
            {
                return _Start;
            }

            set
            {
                _Start = value;
            }
        }

        public string End
        {
            get
            {
                return _End;
            }

            set
            {
                _End = value;
            }
        }

        public bool AllDay
        {
            get
            {
                return _AllDay;
            }

            set
            {
                _AllDay = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Report_Info()
        {
        }
        public Report_Info(int ReportKey)
        {
            string zSQL = "SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM TASK_Report WHERE ReportKey = @ReportKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = ReportKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _Start = zReader["Start"].ToString();
                    _End = zReader["End"].ToString();
                    if (zReader["AllDay"] != DBNull.Value)
                        _AllDay = Convert.ToBoolean(zReader["AllDay"]);

                    if (zReader["ReportKey"] != DBNull.Value)
                        _ReportKey = int.Parse(zReader["ReportKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["ReportDate"] != DBNull.Value)
                        _ReportDate = (DateTime)zReader["ReportDate"];
                    _Description = zReader["Description"].ToString();
                    _UnitID = zReader["UnitID"].ToString();
                    _Title = zReader["Title"].ToString();
                    _Note = zReader["Note"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }            
        public Report_Info(int EmployeeKey, DateTime ReportDate)
        {
            int Day = ReportDate.Day;
            int Month = ReportDate.Month;
            int Year = ReportDate.Year;

            string zSQL = "SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM TASK_Report WHERE DAY(ReportDate) = @Day AND MONTH(ReportDate) = @Month AND YEAR(ReportDate) = @Year AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Day", SqlDbType.Int).Value = Day;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["ReportKey"] != DBNull.Value)
                        _ReportKey = int.Parse(zReader["ReportKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["ReportDate"] != DBNull.Value)
                        _ReportDate = (DateTime)zReader["ReportDate"];
                    _Description = zReader["Description"].ToString();
                    _UnitID = zReader["UnitID"].ToString();
                    _Title = zReader["Title"].ToString();
                    _Note = zReader["Note"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Report_Info(int EmployeeKey, bool IsTop)
        {
            string zSQL = "SELECT TOP 1 *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM TASK_Report WHERE EmployeeKey = @EmployeeKey ORDER BY ReportDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["ReportKey"] != DBNull.Value)
                        _ReportKey = int.Parse(zReader["ReportKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["ReportDate"] != DBNull.Value)
                        _ReportDate = (DateTime)zReader["ReportDate"];
                    _Description = zReader["Description"].ToString();
                    _UnitID = zReader["UnitID"].ToString();
                    _Title = zReader["Title"].ToString();
                    _Note = zReader["Note"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TASK_Report ("
        + " EmployeeKey ,ManagerKey ,DepartmentKey ,ReportDate ,Description, [Start], [End], AllDay ,UnitID ,Title ,Note ,IsDraft ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@EmployeeKey ,@ManagerKey ,@DepartmentKey ,@ReportDate ,@Description, @Start, @End, @AllDay ,@UnitID ,@Title ,@Note ,@IsDraft ,GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT ReportKey FROM TASK_Report WHERE ReportKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Start", SqlDbType.NVarChar).Value = _Start;
                zCommand.Parameters.Add("@End", SqlDbType.NVarChar).Value = _End;
                zCommand.Parameters.Add("@AllDay", SqlDbType.Bit).Value = _AllDay;

                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = _ReportKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                if (_ReportDate.Year == 0001)
                    zCommand.Parameters.Add("@ReportDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReportDate", SqlDbType.DateTime).Value = _ReportDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@UnitID", SqlDbType.NVarChar).Value = _UnitID;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _ReportKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE TASK_Report SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " ManagerKey = @ManagerKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " ReportDate = @ReportDate,"
                        + " Description = @Description, [Start] = @Start, [End] = @End, AllDay = @AllDay,"
                        + " UnitID = @UnitID,"
                        + " Title = @Title,"
                        + " Note = @Note,"
                        + " IsDraft = @IsDraft,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE ReportKey = @ReportKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Start", SqlDbType.NVarChar).Value = _Start;
                zCommand.Parameters.Add("@End", SqlDbType.NVarChar).Value = _End;
                zCommand.Parameters.Add("@AllDay", SqlDbType.Bit).Value = _AllDay;

                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = _ReportKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                if (_ReportDate.Year == 0001)
                    zCommand.Parameters.Add("@ReportDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReportDate", SqlDbType.DateTime).Value = _ReportDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@UnitID", SqlDbType.NVarChar).Value = _UnitID;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ReportKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Report WHERE ReportKey = @ReportKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = _ReportKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
