﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Report_Rec_Info
    {
        #region [ Field Name ]
        private int _ID = 0;
        private int _ReportKey = 0;
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _Result = 0;
        private string _Description = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public int ReportKey
        {
            get { return _ReportKey; }
            set { _ReportKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int Result
        {
            get { return _Result; }
            set { _Result = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Report_Rec_Info()
        {
        }
        public Report_Rec_Info(int ID)
        {
            string zSQL = "SELECT * FROM TASK_Report_Detail WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ID"] != DBNull.Value)
                        _ID = int.Parse(zReader["ID"].ToString());
                    if (zReader["ReportKey"] != DBNull.Value)
                        _ReportKey = int.Parse(zReader["ReportKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["Result"] != DBNull.Value)
                        _Result = int.Parse(zReader["Result"].ToString());
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TASK_Report_Detail ("
        + " ReportKey ,CategoryKey ,CategoryName ,Result ,Description ) "
         + " VALUES ( "
         + "@ReportKey ,@CategoryKey ,@CategoryName ,@Result ,@Description ) "
         + " SELECT ID FROM TASK_Report_Detail WHERE ID = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = _ReportKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@Result", SqlDbType.Int).Value = _Result;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                _ID = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE TASK_Report_Detail SET "
                        + " ReportKey = @ReportKey,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " Result = @Result,"
                        + " Description = @Description"
                       + " WHERE ID = @ID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = _ReportKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@Result", SqlDbType.Int).Value = _Result;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ID == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Report_Detail WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(int Key)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Report_Detail WHERE ReportKey = @ReportKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ReportKey", SqlDbType.Int).Value = Key;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
