﻿using System;
using System.Data;
using System.Data.SqlClient;
using TNConfig;

namespace Lib.Misc
{
    public class Project_Info
    {
        #region [ Field Name ]
        private int _ProjectKey = 0;
        private string _ProjectID = "";
        private string _ProjectName = "";
        private string _Address = "";
        private string _Investor = "";
        private int _InvestorKey = 0;
        private int _WardKey = 0;
        private int _DistrictKey = 0;
        private int _ProvinceKey = 0;
        private int _EmployeeKey = 0;
        private int _Type = 0;
        private string _Street = "";
        private string _Ward = "";
        private string _District = "";
        private string _Province = "";
        private int _Rank = 0;
        private double _BasicPrice = 0;
        private string _Note = "";
        private string _Utilities = "";
        private string _Contents = "";
        private int _IsDraft = 0;
        private int _Properties = 0;
        private string _Picture = "";
        private string _Lat = "";
        private string _Lng = "";
        private string _LatLng = "";
        private string _MainTable = "";
        private string _ShortTable = "";
        private int _IsPrimary = 0;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public string ProjectID
        {
            get { return _ProjectID; }
            set { _ProjectID = value; }
        }
        public string ProjectName
        {
            get { return _ProjectName; }
            set { _ProjectName = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string Investor
        {
            get { return _Investor; }
            set { _Investor = value; }
        }
        public int InvestorKey
        {
            get { return _InvestorKey; }
            set { _InvestorKey = value; }
        }
        public int WardKey
        {
            get { return _WardKey; }
            set { _WardKey = value; }
        }
        public int DistrictKey
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }
        public string Ward
        {
            get { return _Ward; }
            set { _Ward = value; }
        }
        public string District
        {
            get { return _District; }
            set { _District = value; }
        }
        public string Province
        {
            get { return _Province; }
            set { _Province = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public double BasicPrice
        {
            get { return _BasicPrice; }
            set { _BasicPrice = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string Utilities
        {
            get { return _Utilities; }
            set { _Utilities = value; }
        }
        public string Contents
        {
            get { return _Contents; }
            set { _Contents = value; }
        }
        public int IsDraft
        {
            get { return _IsDraft; }
            set { _IsDraft = value; }
        }
        public int Properties
        {
            get { return _Properties; }
            set { _Properties = value; }
        }
        public string Picture
        {
            get { return _Picture; }
            set { _Picture = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Lat
        {
            get
            {
                return _Lat;
            }

            set
            {
                _Lat = value;
            }
        }

        public string Lng
        {
            get
            {
                return _Lng;
            }

            set
            {
                _Lng = value;
            }
        }

        public string LatLng
        {
            get
            {
                return _LatLng;
            }

            set
            {
                _LatLng = value;
            }
        }

        public int IsPrimary
        {
            get
            {
                return _IsPrimary;
            }

            set
            {
                _IsPrimary = value;
            }
        }

        public string MainTable
        {
            get
            {
                return _MainTable;
            }

            set
            {
                _MainTable = value;
            }
        }

        public string ShortTable
        {
            get
            {
                return _ShortTable;
            }

            set
            {
                _ShortTable = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Project_Info()
        {
        }
        public Project_Info(int ProjectKey)
        {
            string zSQL = "SELECT * FROM PUL_Project WHERE ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    _ProjectID = zReader["ProjectID"].ToString();
                    _ProjectName = zReader["ProjectName"].ToString();
                    _Address = zReader["Address"].ToString();
                    _Investor = zReader["Investor"].ToString();
                    if (zReader["InvestorKey"] != DBNull.Value)
                        _InvestorKey = int.Parse(zReader["InvestorKey"].ToString());
                    if (zReader["WardKey"] != DBNull.Value)
                        _WardKey = int.Parse(zReader["WardKey"].ToString());
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    _Street = zReader["Street"].ToString();
                    _Ward = zReader["Ward"].ToString();
                    _District = zReader["District"].ToString();
                    _Province = zReader["Province"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["BasicPrice"] != DBNull.Value)
                        _BasicPrice = double.Parse(zReader["BasicPrice"].ToString());
                    _Note = zReader["Note"].ToString();
                    _Utilities = zReader["Utilities"].ToString();
                    _Contents = zReader["Contents"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["Properties"] != DBNull.Value)
                        _Properties = int.Parse(zReader["Properties"].ToString());
                    _Picture = zReader["Picture"].ToString();
                    _Lat = zReader["Lat"].ToString();
                    _Lng = zReader["Lng"].ToString();
                    if (zReader["IsPrimary"] != DBNull.Value)
                        _IsPrimary = int.Parse(zReader["IsPrimary"].ToString());
                    _LatLng = zReader["LatLng"].ToString();
                    _MainTable = zReader["MainTable"].ToString();
                    _ShortTable = zReader["ShortTable"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        /// <summary>
        /// tim mã theo tên dự án
        /// </summary>
        /// <param name="ProjectName"></param>
        public Project_Info(string ProjectName)
        {
            string zSQL = "SELECT ProjectKey, ProjectName FROM PUL_Project WHERE ProjectName LIKE @ProjectName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + ProjectName + "%";
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ProjectKey = zReader["ProjectKey"].ToInt();
                    _ProjectName = zReader["ProjectName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"
INSERT INTO PUL_Project (
ProjectID ,ProjectName ,Address ,Investor ,InvestorKey ,WardKey ,DistrictKey ,ProvinceKey ,
EmployeeKey ,Type ,Street ,Ward ,District ,Province ,Rank ,BasicPrice ,Note ,Utilities ,
Contents ,IsDraft ,Properties ,Picture , Lat, Lng, LatLng, IsPrimary, MainTable, ShortTable,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) 
VALUES ( 
@ProjectID ,@ProjectName ,@Address ,@Investor ,@InvestorKey ,@WardKey ,@DistrictKey ,@ProvinceKey ,
@EmployeeKey ,@Type ,@Street ,@Ward ,@District ,@Province ,@Rank ,@BasicPrice ,@Note ,@Utilities ,
@Contents ,@IsDraft ,@Properties ,@Picture ,@Lat,@Lng,@LatLng, @IsPrimary, @MainTable, @ShortTable,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName) 
SELECT ProjectKey FROM PUL_Project WHERE ProjectKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = _ProjectID;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = _ProjectName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@Investor", SqlDbType.NVarChar).Value = _Investor;
                zCommand.Parameters.Add("@InvestorKey", SqlDbType.Int).Value = _InvestorKey;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = _Ward;
                zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = _District;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@BasicPrice", SqlDbType.Money).Value = _BasicPrice;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@Utilities", SqlDbType.NVarChar).Value = _Utilities;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@Properties", SqlDbType.Int).Value = _Properties;
                zCommand.Parameters.Add("@Picture", SqlDbType.NVarChar).Value = _Picture;              
                zCommand.Parameters.Add("@Lat", SqlDbType.NVarChar).Value = _Lat;
                zCommand.Parameters.Add("@Lng", SqlDbType.NVarChar).Value = _Lng;
                zCommand.Parameters.Add("@LatLng", SqlDbType.NVarChar).Value = _LatLng;
                zCommand.Parameters.Add("@IsPrimary", SqlDbType.NVarChar).Value = _IsPrimary;
                zCommand.Parameters.Add("@MainTable", SqlDbType.NVarChar).Value = _MainTable;
                zCommand.Parameters.Add("@ShortTable", SqlDbType.NVarChar).Value = _ShortTable;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                _ProjectKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PUL_Project SET "
                        + " ProjectID = @ProjectID,"
                        + " ProjectName = @ProjectName,"
                        + " Address = @Address,"
                        + " Investor = @Investor,"
                        + " InvestorKey = @InvestorKey,"
                        + " WardKey = @WardKey,"
                        + " DistrictKey = @DistrictKey,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " Type = @Type,"
                        + " Street = @Street,"
                        + " Ward = @Ward,"
                        + " District = @District,"
                        + " Province = @Province,"
                        + " Rank = @Rank,"
                        + " BasicPrice = @BasicPrice,"
                        + " Note = @Note,"
                        + " Utilities = @Utilities,"
                        + " Contents = @Contents,"
                        + " IsDraft = @IsDraft,"
                        + " Properties = @Properties,"
                        + " Picture = @Picture,"
                        + " Lat = @Lat,"
                        + " Lng = @Lng,"
                        + " LatLng = @LatLng, IsPrimary = @IsPrimary, MainTable = @MainTable, ShortTable = @ShortTable,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ProjectKey = @ProjectKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = _ProjectID;
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = _ProjectName;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@Investor", SqlDbType.NVarChar).Value = _Investor;
                zCommand.Parameters.Add("@InvestorKey", SqlDbType.Int).Value = _InvestorKey;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = _Ward;
                zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = _District;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@BasicPrice", SqlDbType.Money).Value = _BasicPrice;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@Utilities", SqlDbType.NVarChar).Value = _Utilities;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@Properties", SqlDbType.Int).Value = _Properties;
                zCommand.Parameters.Add("@Picture", SqlDbType.NVarChar).Value = _Picture;
                zCommand.Parameters.Add("@Lat", SqlDbType.NVarChar).Value = _Lat;
                zCommand.Parameters.Add("@Lng", SqlDbType.NVarChar).Value = _Lng;
                zCommand.Parameters.Add("@LatLng", SqlDbType.NVarChar).Value = _LatLng;
                zCommand.Parameters.Add("@IsPrimary", SqlDbType.NVarChar).Value = _IsPrimary;
                zCommand.Parameters.Add("@MainTable", SqlDbType.NVarChar).Value = _MainTable;
                zCommand.Parameters.Add("@ShortTable", SqlDbType.NVarChar).Value = _ShortTable;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateLatLng()
        {
            string zSQL = "UPDATE PUL_Project SET "
                        + " Lat = @Lat,"
                        + " Lng = @Lng,"
                        + " LatLng = @LatLng,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE ProjectKey = @ProjectKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Lat", SqlDbType.NVarChar).Value = _Lat;
                zCommand.Parameters.Add("@Lng", SqlDbType.NVarChar).Value = _Lng;
                zCommand.Parameters.Add("@LatLng", SqlDbType.NVarChar).Value = _LatLng;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ProjectKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Project WHERE ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteRelate()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM PUL_Document A WHERE A.ProjectKey = @ProjectKey
DELETE FROM PUL_Project_Assets A WHERE A.ProjectKey = @ProjectKey
DELETE FROM PUL_Project_Category A WHERE A.ProjectKey = @ProjectKey
DELETE FROM PUL_SharePermition A WHERE A.AssetKey = @ProjectKey AND ObjectTable = 'Project'";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
