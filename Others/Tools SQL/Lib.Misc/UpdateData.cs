﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TNConfig;
using System.Data.SqlClient;
using System.Data;

namespace Lib.Misc
{
    public class UpdateData
    {
        public static string InsertUpdate(string Table, string ColumnUpdate, string ValueUpdate, string ColumnWhere, string ValueWhere)
        {
            string zSQL = "UPDATE " + Table + " SET " + ColumnUpdate + " = " + "@" + ColumnUpdate + " WHERE " + ColumnWhere + " = " + "@" + ColumnWhere + " ";
            string zResult = "";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(zConnectionString);
            nConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, nConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@" + ColumnWhere, SqlDbType.NVarChar).Value = ValueWhere;
                zCommand.Parameters.Add("@" + ColumnUpdate, SqlDbType.NVarChar).Value = ValueUpdate;

                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return zResult;
        }
        public static string InsertUpdate(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = SQL;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                return Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
