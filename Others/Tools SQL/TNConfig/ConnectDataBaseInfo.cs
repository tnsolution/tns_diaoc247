﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace TNConfig
{
    public class ConnectDataBaseInfo
    {
        private string _DataSource = @"192.168.1.245";
        private string _DataBase = "HRM";
        private string _User = "quanly";
        private string _Password = "1";

        private string _AttachDbFilename = Path.GetFullPath(@"DataBase\TN_Solution.MDF");        
        private string _ConnectionString = "";

        private bool _IsConnectLocal = false;

        public ConnectDataBaseInfo()
        {

        }
        public string DataSource
        {
            get
            {
                return _DataSource;
            }
            set
            {
                _DataSource = value;
            }
        }
        public string DataBase
        {
            get
            {
                return _DataBase;
            }
            set
            {
                _DataBase = value;
            }
        }
        public string UserName
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }
        public string AttachDbFilename
        {
            get
            {
                return _AttachDbFilename;
            }
            set
            {
                _AttachDbFilename = value;
            }
        }
        public bool IsConnectLocal
        {
            get
            {
                return _IsConnectLocal;
            }
            set
            {
                _IsConnectLocal = value;
            }
        }
        public string ConnectionString
        {
            get
            {
                if (!_IsConnectLocal)
                {
                    _ConnectionString = "Data Source = " + _DataSource + ";DataBase= " + _DataBase + ";User=" + _User + ";Password= " + _Password;
                }
                else
                {
                    _ConnectionString = "Data Source = " + _DataSource + ";AttachDbFilename= " + _AttachDbFilename + ";User Instance =true;Integrated Security= SSPI";
                }
                return _ConnectionString;
            }
            set
            {
                _ConnectionString = value;
            }
        }
    }
}
