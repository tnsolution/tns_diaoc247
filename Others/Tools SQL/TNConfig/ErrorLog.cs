﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TNSolution
{
    public class ErrorLog
    {
        /// <summary>
        /// Ghi lỗi ra txt
        /// </summary>
        /// <param name="exp">lỗi</param>
        public static void CreateLogFiles(string exp)
        {
            if (System.IO.File.Exists(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log"))
            {
                using (FileStream fs = new FileStream(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log", FileMode.Append))
                {
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(exp + " : " + DateTime.Now.ToString() + Environment.NewLine);
                    sw.Close();
                }
            }
            else
            {
                using (FileStream fs = new FileStream(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log", FileMode.OpenOrCreate))
                {
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(exp + " : " + DateTime.Now.ToString() + Environment.NewLine);
                    sw.Close();
                }
            }
        }
    }
}
