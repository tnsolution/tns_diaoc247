﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace TNConfig
{
   public  class LogicManagement
    {
       private string m_StrConnectionString = "";
       private SqlConnection m_cnn;
       private SqlCommand m_cmd;
       private SqlDataAdapter m_app;
       public DataTable GetTBlByProcedure(string strNameproc, string[] arrParameters, string[] arrValues )
       {
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strNameproc, m_cnn);

               m_cmd.CommandType = CommandType.StoredProcedure;
               for (int i = 0; i < arrParameters.Length; i++)
               {
                   SqlParameter prarss = new SqlParameter(arrParameters[i], arrValues[i]);
                    m_cmd.Parameters.Add(prarss);

               }
                   m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill( tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public DataTable GetTBlByProcedure(string strNameproc, string[] arrParameters, string[] arrValues, int PageSize, int PageNumber)
       {
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strNameproc, m_cnn);

               m_cmd.CommandType = CommandType.StoredProcedure;
               for (int i = 0; i < arrParameters.Length; i++)
               {
                   SqlParameter prarss = new SqlParameter(arrParameters[i], arrValues[i]);
                    m_cmd.Parameters.Add(prarss);

               }
                   m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(PageSize * PageNumber - PageSize, PageSize , tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public DataTable GetTBtBysqlCMD(string strSql, string[] arrParameters, string[] arrValues)
       {
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strSql, m_cnn);

               m_cmd.CommandType = CommandType.Text;
               for (int i = 0; i < arrParameters.Length; i++)
               {
                   SqlParameter prarss = new SqlParameter(arrParameters[i], arrValues[i]);
                   m_cmd.Parameters.Add(prarss);

               }
               m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }

       public DataTable GetTBtBysqlCMD(string strSql, string[] arrParameters, string[] arrValues, int PageSize, int PageNumber )
       {
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strSql, m_cnn);

               m_cmd.CommandType = CommandType.Text;
               for (int i = 0; i < arrParameters.Length; i++)
               {
                   SqlParameter prarss = new SqlParameter(arrParameters[i], arrValues[i]);
                   m_cmd.Parameters.Add(prarss);

               }
               m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(PageSize * PageNumber - PageSize, PageSize, tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu!   Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public DataTable GetTBtBysqlCMD(string strSql)
       {
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strSql, m_cnn);
               m_cmd.CommandType = CommandType.Text;                
               m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public DataTable GetTBtBysqlCMD(string strSql, int nPageSize, int nPageNumber) // Có phân trang 
       {
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strSql, m_cnn);
               m_cmd.CommandType = CommandType.Text;
               m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(nPageSize * nPageNumber - nPageSize, nPageSize, tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public DataTable GetTBLByProcName(string strNameProc)
       {

           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {
              
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strNameProc, m_cnn);                
               m_cmd.CommandType = CommandType.StoredProcedure;
               m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public DataTable GetTBLByProcName(string strNameProc, int nPageSize, int nPageNumber)
       {

           m_StrConnectionString = ConnectDataBase.ConnectionString;
           DataTable tbl = new DataTable();
           try
           {

               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strNameProc, m_cnn);
               m_cmd.CommandType = CommandType.StoredProcedure;
               m_app = new SqlDataAdapter(m_cmd);
               m_app.Fill(nPageSize * nPageNumber - nPageSize, nPageSize, tbl);

           }
           catch (Exception ex)
           {
               throw new Exception("Không thể lấy được bảng dữ liệu! Vui lòng kiểm tra kết nối cơ sở dữ liệu! Chi tiết xem  " + ex.Message);
           }
           finally
           {

               m_app.Dispose();
               m_cmd.Dispose();
               m_cnn.Close();
           }
           return tbl;
       }
       public string UpdateByProcedure(string strNameproc, string[] arrParameters, string[] arrValues)
       {

           string result = "";
             m_StrConnectionString = ConnectDataBase.ConnectionString;
             m_cnn = new SqlConnection(m_StrConnectionString);
             m_cnn.Open();
           try
           {
              
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strNameproc, m_cnn);                
               m_cmd.CommandType = CommandType.StoredProcedure;
               for (int i = 0; i < arrParameters.Length; i++)
               {
                   SqlParameter prarss = new SqlParameter(arrParameters[i], arrValues[i]);
                   m_cmd.Parameters.Add(prarss);

               }
               result = m_cmd.ExecuteNonQuery().ToString() ;
               m_cmd.Dispose();
           }
           catch(Exception ex)
           {

               throw new Exception("Không thể cập nhật dữ liệu => " + ex.Message);
           }
           finally
           {
               m_cnn.Close();
           }
           return result;
       }
        public string        UpdateByProcedureSacalar (string strNameproc, string[] arrParameters, string[] arrValues)
       {

           string result = "";
             m_StrConnectionString = ConnectDataBase.ConnectionString;
             m_cnn = new SqlConnection(m_StrConnectionString);
             m_cnn.Open();
           try
           {
              
               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(strNameproc, m_cnn);                
               m_cmd.CommandType = CommandType.StoredProcedure;
               for (int i = 0; i < arrParameters.Length; i++)
               {
                   SqlParameter prarss = new SqlParameter(arrParameters[i], arrValues[i]);
                   m_cmd.Parameters.Add(prarss);

               }
               result = m_cmd.ExecuteScalar().ToString() ;
               m_cmd.Dispose();
           }
           catch(Exception ex)
           {

               throw new Exception("Không thể cập nhật dữ liệu => " + ex.Message);
           }
           finally
           {
               m_cnn.Close();
           }
           return result;
       }
       public string QlqCommentText(string Sql)
       {

           string result = "";
           m_StrConnectionString = ConnectDataBase.ConnectionString;
           m_cnn = new SqlConnection(m_StrConnectionString);
           m_cnn.Open();
           try
           {

               m_cnn = new SqlConnection(m_StrConnectionString);
               m_cnn.Open();
               m_cmd = new SqlCommand(Sql, m_cnn);
               m_cmd.CommandType = CommandType.Text;               
               result = m_cmd.ExecuteNonQuery().ToString();
               m_cmd.Dispose();
           }
           catch (Exception ex)
           {

               throw new Exception("Lỗi cập nhật dữ liệu!" + ex.Message);
           }
           finally
           {
               m_cnn.Close();
           }
           return result;
       }
    }
}
