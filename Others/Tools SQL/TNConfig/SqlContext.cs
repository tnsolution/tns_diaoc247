﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TNConfig;

namespace TNConfig
{
    public class SqlContext
    {
        private static string _ConStr = ConnectDataBase.ConnectionString;

        SqlConnection _con = null;
        SqlCommand _CMD = new SqlCommand();

        /// <summary>
        /// Get Command SQL
        /// </summary>
        public SqlCommand CMD
        {
            get { return _CMD; }
            set { _CMD = value; }
        }

        public SqlContext()
        {
            _con = new SqlConnection();
            _con.ConnectionString = _ConStr;
            _con.Open();
        }

        SqlCommand GetCommand()
        {
            CMD.CommandType = CommandType.Text;
            CMD.Connection = _con;
            return CMD;
        }

        /// <summary>
        /// using: new dbConnect().getData('select * from table');
        /// </summary>
        /// <param name="sql">sql query</param>
        /// <returns></returns>
        public DataTable GetData(string sql)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = GetCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
            cmd.Parameters.Clear();

            CloseConnect();
            return dt;
        }

        /// <summary>
        /// using: new dbConnect().getData('procedure name');
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="isStoredProcedure"></param>
        /// <returns></returns>
        public DataTable GetData(string sql, bool isStoredProcedure)
        {
            DataTable dt = new DataTable();

            SqlCommand cmd = GetCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sql;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
            cmd.Parameters.Clear();

            CloseConnect();
            return dt;
        }

        /// <summary>
        /// Check exist: new dbConnect().IsExist('select * from table'); 
        /// </summary>
        /// <param name="sql">sql query</param>
        /// <returns>true/false</returns>
        public bool IsExist(string sql)
        {
            int result = 0;

            SqlCommand cmd = GetCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            result = int.Parse(cmd.ExecuteScalar().ToString());
            cmd.Parameters.Clear();

            CloseConnect();
            if (result > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Get object: new dbConnect().GetObject('select * from table'); 
        /// </summary>
        /// <param name="sql">sql query</param>
        /// <returns>string or ""</returns>
        public object GetObject(string sql)
        {
            object Ob = new object();

            SqlCommand cmd = GetCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            Ob = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

            CloseConnect();
            if (Ob != null)
                return Ob;
            else
                return "";
        }

        public void CloseConnect()
        {
            _con.Close();
        }
    }
}
