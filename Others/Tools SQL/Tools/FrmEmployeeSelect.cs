﻿using Lib.Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tools
{
    public partial class FrmEmployeeSelect : Form
    {
        public List<string> Employee { get; set; }
        public FrmEmployeeSelect()
        {
            InitializeComponent();
            InitListView(LVData);
        }

        private void FrmEmployeeSelect_Load(object sender, EventArgs e)
        {
            DataTable zTable = Database_Data.GetData("SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME FROM HRM_Employees");
            LoadDataToListView(zTable);
        }

        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "Số TT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ tên";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        void LoadDataToListView(DataTable dt)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LVData.Items.Clear();
            int n = dt.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = dt.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = "";
                lvi.Tag = nRow["EmployeeKey"]; // Set the tag to                             

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NAME"].ToString();
                lvi.SubItems.Add(lvsi);

                LVData.Items.Add(lvi);
            }
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            Employee = new List<string>();
            foreach (ListViewItem item in LVData.Items)
            {
                if (item.Checked)
                    Employee.Add(item.Tag.ToString());
            }

            this.Close();
        }
    }
}
