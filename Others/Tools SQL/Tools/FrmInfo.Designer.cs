﻿namespace Tools
{
    partial class FrmInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnViewData = new System.Windows.Forms.Button();
            this.cboProject = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LVData2 = new System.Windows.Forms.ListView();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnViewData);
            this.panel1.Controls.Add(this.cboProject);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 52);
            this.panel1.TabIndex = 2;
            // 
            // btnViewData
            // 
            this.btnViewData.Location = new System.Drawing.Point(281, 12);
            this.btnViewData.Name = "btnViewData";
            this.btnViewData.Size = new System.Drawing.Size(75, 30);
            this.btnViewData.TabIndex = 2;
            this.btnViewData.Text = "Xem";
            this.btnViewData.UseVisualStyleBackColor = true;
            // 
            // cboProject
            // 
            this.cboProject.FormattingEnabled = true;
            this.cboProject.Location = new System.Drawing.Point(59, 17);
            this.cboProject.Name = "cboProject";
            this.cboProject.Size = new System.Drawing.Size(216, 22);
            this.cboProject.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dự án";
            // 
            // LVData2
            // 
            this.LVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData2.FullRowSelect = true;
            this.LVData2.GridLines = true;
            this.LVData2.Location = new System.Drawing.Point(0, 52);
            this.LVData2.MultiSelect = false;
            this.LVData2.Name = "LVData2";
            this.LVData2.ShowGroups = false;
            this.LVData2.Size = new System.Drawing.Size(784, 509);
            this.LVData2.TabIndex = 6;
            this.LVData2.UseCompatibleStateImageBehavior = false;
            this.LVData2.View = System.Windows.Forms.View.Details;
            // 
            // FrmInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.LVData2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.Name = "FrmInfo";
            this.Text = ".";
            this.Load += new System.EventHandler(this.FrmInfo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnViewData;
        private System.Windows.Forms.ComboBox cboProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView LVData2;
    }
}