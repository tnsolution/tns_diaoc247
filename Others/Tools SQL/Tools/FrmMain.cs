﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNConfig;

namespace Tools
{
    public partial class FrmMain : Form
    {
    
        public FrmMain()
        {
            InitializeComponent();

            lblStatus.Text = "Kiểm tra kết nối CRM";

            RWConfig nConfig = new RWConfig();
            ConnectDataBaseInfo nConnectInfo = new ConnectDataBaseInfo();
            if (nConfig.ReadConfig())
            {
                nConnectInfo = nConfig.ConnectInfo;
                ConnectDataBase nConnect = new ConnectDataBase(nConnectInfo.ConnectionString);
                if (nConnect.Message.Length > 0)
                {
                    lblStatus.Text = "Chưa kết nối CRM";                 
                }
                else
                {
                    lblStatus.Text = "Đã kết nối CRM";              
                }
            }
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {

        }
        private void MN_Connect_Click(object sender, EventArgs e)
        {
            FrmConnectServer frm = new FrmConnectServer();
            frm.ShowDialog();
            ConnectDataBase.ConnectionString = frm._ConnectDataBaseInfo.ConnectionString;

            if (frm._IsConnect)
                lblStatus.Text = "Đã kết nối CRM";
            else
                lblStatus.Text = "Chưa kết nối CRM";
        }
        private void MN_Export_Click(object sender, EventArgs e)
        {
            FrmProductExport frm = new FrmProductExport();
            frm.MdiParent = this;
            frm.Show();
        }
        private void MN_Import_Click(object sender, EventArgs e)
        {
            FrmProductImport frm = new FrmProductImport();
            frm.MdiParent = this;
            frm.Show();
            
        }
        private void MN_CheckData_Click(object sender, EventArgs e)
        {
            FrmProductCheck frm = new FrmProductCheck();
            frm.MdiParent = this;
            frm.Show();
        }
        private void MN_Cut_Click(object sender, EventArgs e)
        {
            FrmProductCut frm = new FrmProductCut();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MN_Log_Click(object sender, EventArgs e)
        {
            FrmProductLog frm = new FrmProductLog();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
