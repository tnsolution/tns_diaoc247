﻿using Lib.Misc;
using System;
using System.Data;
using System.Windows.Forms;
using TNLibrary.SYS;

namespace Tools
{
    public partial class FrmProductCheck : Form
    {
        public FrmProductCheck()
        {
            InitializeComponent();
            LoadDataToToolbox.ComboBoxData(cboProject, "SELECT ProjectKey, ProjectName FROM PUL_Project", false);
        }
        private void FrmProductCheck_Load(object sender, EventArgs e)
        {
            InitListView(LVData1);
        }
        private void btnViewData_Click(object sender, EventArgs e)
        {
            PopulateLV(LVData1, Database_Data.GetAssetList(Convert.ToInt32(cboProject.SelectedValue)));
        }

        private void LVData1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ID = "";
            int No = 0;
            if (LVData1.SelectedItems.Count > 0)
            {
                ID = LVData1.SelectedItems[0].SubItems[1].Text;
                No = int.Parse(LVData1.SelectedItems[0].SubItems[2].Text);
                LVData2.Clear();

                #region [SETUP LV]
                ListView LV = LVData2;
                ColumnHeader colHead;
                colHead = new ColumnHeader();
                colHead.Text = "STT";
                colHead.Width = 50;
                colHead.TextAlign = HorizontalAlignment.Center;
                LV.Columns.Add(colHead);

                colHead = new ColumnHeader();
                colHead.Text = "Thông tin";
                colHead.Width = 150;
                colHead.TextAlign = HorizontalAlignment.Left;
                LV.Columns.Add(colHead);

                for (int i = 1; i <= No; i++)
                {
                    colHead = new ColumnHeader();
                    colHead.Text = "Giá trị số: " + i;
                    colHead.Width = 150;
                    colHead.TextAlign = HorizontalAlignment.Left;
                    LV.Columns.Add(colHead);
                }
                #endregion                              

                PopulateLV_Detail(ID);
            }
        }
        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã căn";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số lượng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        void PopulateLV(ListView LV, DataTable InTable)
        {
            LV.Items.Clear();
            int i = 1;
            foreach (DataRow nRow in InTable.Rows)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Text = (i++).ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[0].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[1].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
        }
        void PopulateLV_Detail(string ID)
        {
            DataTable zTable = Database_Data.GetAssetDuplicate(ID);
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    DataTable tableRow = Database_Data.GetAsset(Convert.ToInt32(r["AssetKey"]));
                    int i = 0;
                    foreach (DataColumn c in tableRow.Columns)
                    {
                        ListViewItem lvi;
                        ListViewItem.ListViewSubItem lvsi;

                        lvi = new ListViewItem();
                        lvi.Text = "";

                        lvsi = new ListViewItem.ListViewSubItem();
                        lvsi.Text = c.ColumnName;
                        lvi.SubItems.Add(lvsi);

                        lvsi = new ListViewItem.ListViewSubItem();
                        lvsi.Text = tableRow.Rows[i][c.ColumnName].ToString();

                        LVData2.Items.Add(lvi);
                    }
                }
            }
        }
    }
}
