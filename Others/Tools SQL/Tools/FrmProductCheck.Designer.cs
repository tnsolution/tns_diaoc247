﻿namespace Tools
{
    partial class FrmProductCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnViewData = new System.Windows.Forms.Button();
            this.cboProject = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LVData1 = new System.Windows.Forms.ListView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.LVData2 = new System.Windows.Forms.ListView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnViewData);
            this.panel1.Controls.Add(this.cboProject);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(950, 52);
            this.panel1.TabIndex = 1;
            // 
            // btnViewData
            // 
            this.btnViewData.Location = new System.Drawing.Point(281, 12);
            this.btnViewData.Name = "btnViewData";
            this.btnViewData.Size = new System.Drawing.Size(75, 30);
            this.btnViewData.TabIndex = 2;
            this.btnViewData.Text = "Xem";
            this.btnViewData.UseVisualStyleBackColor = true;
            this.btnViewData.Click += new System.EventHandler(this.btnViewData_Click);
            // 
            // cboProject
            // 
            this.cboProject.FormattingEnabled = true;
            this.cboProject.Location = new System.Drawing.Point(59, 17);
            this.cboProject.Name = "cboProject";
            this.cboProject.Size = new System.Drawing.Size(216, 22);
            this.cboProject.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dự án";
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(672, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Thông tin trùng";
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(274, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sản phẩm";
            // 
            // LVData1
            // 
            this.LVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData1.FullRowSelect = true;
            this.LVData1.GridLines = true;
            this.LVData1.Location = new System.Drawing.Point(0, 16);
            this.LVData1.MultiSelect = false;
            this.LVData1.Name = "LVData1";
            this.LVData1.ShowGroups = false;
            this.LVData1.Size = new System.Drawing.Size(274, 520);
            this.LVData1.TabIndex = 2;
            this.LVData1.UseCompatibleStateImageBehavior = false;
            this.LVData1.View = System.Windows.Forms.View.Details;
            this.LVData1.SelectedIndexChanged += new System.EventHandler(this.LVData1_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 52);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.LVData1);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.LVData2);
            this.splitContainer2.Panel2.Controls.Add(this.label3);
            this.splitContainer2.Size = new System.Drawing.Size(950, 536);
            this.splitContainer2.SplitterDistance = 274;
            this.splitContainer2.TabIndex = 5;
            // 
            // LVData2
            // 
            this.LVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData2.FullRowSelect = true;
            this.LVData2.GridLines = true;
            this.LVData2.Location = new System.Drawing.Point(0, 16);
            this.LVData2.MultiSelect = false;
            this.LVData2.Name = "LVData2";
            this.LVData2.ShowGroups = false;
            this.LVData2.Size = new System.Drawing.Size(672, 520);
            this.LVData2.TabIndex = 5;
            this.LVData2.UseCompatibleStateImageBehavior = false;
            this.LVData2.View = System.Windows.Forms.View.Details;
            // 
            // FrmProductCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 588);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.Name = "FrmProductCheck";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmProductCheck_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnViewData;
        private System.Windows.Forms.ComboBox cboProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView LVData1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView LVData2;
    }
}