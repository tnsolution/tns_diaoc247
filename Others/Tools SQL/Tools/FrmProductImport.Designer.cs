﻿namespace Tools
{
    partial class FrmProductImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdateData = new System.Windows.Forms.Button();
            this.btnCheckData = new System.Windows.Forms.Button();
            this.btnExportData = new System.Windows.Forms.Button();
            this.btnViewData = new System.Windows.Forms.Button();
            this.cboProject = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LVData1 = new System.Windows.Forms.ListView();
            this.LVData2 = new System.Windows.Forms.ListView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnUpdateData);
            this.panel1.Controls.Add(this.btnCheckData);
            this.panel1.Controls.Add(this.btnExportData);
            this.panel1.Controls.Add(this.btnViewData);
            this.panel1.Controls.Add(this.cboProject);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(967, 90);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(204, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Lưu ý, file excel chỉ đọc sheet số 1";
            // 
            // btnUpdateData
            // 
            this.btnUpdateData.Location = new System.Drawing.Point(848, 17);
            this.btnUpdateData.Name = "btnUpdateData";
            this.btnUpdateData.Size = new System.Drawing.Size(111, 30);
            this.btnUpdateData.TabIndex = 4;
            this.btnUpdateData.Text = "Cập nhật Data";
            this.btnUpdateData.UseVisualStyleBackColor = true;
            this.btnUpdateData.Click += new System.EventHandler(this.btnUpdateData_Click);
            // 
            // btnCheckData
            // 
            this.btnCheckData.Location = new System.Drawing.Point(483, 17);
            this.btnCheckData.Name = "btnCheckData";
            this.btnCheckData.Size = new System.Drawing.Size(111, 30);
            this.btnCheckData.TabIndex = 3;
            this.btnCheckData.Text = "Kiểm tra Data";
            this.btnCheckData.UseVisualStyleBackColor = true;
            this.btnCheckData.Visible = false;
            // 
            // btnExportData
            // 
            this.btnExportData.Location = new System.Drawing.Point(366, 17);
            this.btnExportData.Name = "btnExportData";
            this.btnExportData.Size = new System.Drawing.Size(111, 30);
            this.btnExportData.TabIndex = 2;
            this.btnExportData.Text = "Import Excel";
            this.btnExportData.UseVisualStyleBackColor = true;
            this.btnExportData.Click += new System.EventHandler(this.btnImportData_Click);
            // 
            // btnViewData
            // 
            this.btnViewData.Location = new System.Drawing.Point(285, 17);
            this.btnViewData.Name = "btnViewData";
            this.btnViewData.Size = new System.Drawing.Size(75, 30);
            this.btnViewData.TabIndex = 2;
            this.btnViewData.Text = "Xem";
            this.btnViewData.UseVisualStyleBackColor = true;
            this.btnViewData.Click += new System.EventHandler(this.btnViewData_Click);
            // 
            // cboProject
            // 
            this.cboProject.FormattingEnabled = true;
            this.cboProject.Location = new System.Drawing.Point(63, 22);
            this.cboProject.Name = "cboProject";
            this.cboProject.Size = new System.Drawing.Size(216, 22);
            this.cboProject.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dự án";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 90);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LVData1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LVData2);
            this.splitContainer1.Size = new System.Drawing.Size(967, 435);
            this.splitContainer1.SplitterDistance = 478;
            this.splitContainer1.TabIndex = 2;
            // 
            // LVData1
            // 
            this.LVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData1.FullRowSelect = true;
            this.LVData1.GridLines = true;
            this.LVData1.Location = new System.Drawing.Point(0, 0);
            this.LVData1.MultiSelect = false;
            this.LVData1.Name = "LVData1";
            this.LVData1.ShowGroups = false;
            this.LVData1.Size = new System.Drawing.Size(478, 435);
            this.LVData1.TabIndex = 2;
            this.LVData1.UseCompatibleStateImageBehavior = false;
            this.LVData1.View = System.Windows.Forms.View.Details;
            // 
            // LVData2
            // 
            this.LVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData2.FullRowSelect = true;
            this.LVData2.GridLines = true;
            this.LVData2.Location = new System.Drawing.Point(0, 0);
            this.LVData2.MultiSelect = false;
            this.LVData2.Name = "LVData2";
            this.LVData2.ShowGroups = false;
            this.LVData2.Size = new System.Drawing.Size(485, 435);
            this.LVData2.TabIndex = 2;
            this.LVData2.UseCompatibleStateImageBehavior = false;
            this.LVData2.View = System.Windows.Forms.View.Details;
            // 
            // FrmProductImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 525);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmProductImport";
            this.ShowIcon = false;
            this.Text = "...";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmProductImport_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExportData;
        private System.Windows.Forms.Button btnViewData;
        private System.Windows.Forms.ComboBox cboProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnUpdateData;
        private System.Windows.Forms.ListView LVData1;
        private System.Windows.Forms.ListView LVData2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCheckData;
    }
}