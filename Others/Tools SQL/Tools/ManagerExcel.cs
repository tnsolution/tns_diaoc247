﻿using Lib.Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TNConfig;

namespace Tools
{
    public partial class ManagerExcel : Form
    {
        #region [ Variables ]       
        bool IsConnected = false;

        string mFileNameImport = "";
        DataTable _dtExcel;

        private Thread mThreadExcel;
        private int mIndexExcel = 0;

        private Thread mThreadSQL;
        private int mIndexSQL = 0;

        delegate void SetItemExcelCallback(DataRow RowExcel);
        #endregion

        public ManagerExcel()
        {
            InitializeComponent();
            InitListView(LVData);

            lblStatus.Text = "Kiểm tra kết nối CRM";

            RWConfig nConfig = new RWConfig();
            ConnectDataBaseInfo nConnectInfo = new ConnectDataBaseInfo();
            if (nConfig.ReadConfig())
            {
                nConnectInfo = nConfig.ConnectInfo;
                ConnectDataBase nConnect = new ConnectDataBase(nConnectInfo.ConnectionString);
                if (nConnect.Message.Length > 0)
                {
                    lblStatus.Text = "Chưa kết nối CRM";
                    IsConnected = false;
                }
                else
                {
                    lblStatus.Text = "Đã kết nối CRM";
                    IsConnected = true;
                }
            }
        }

        private void ManagerExcel_Load(object sender, EventArgs e)
        {

        }

        private void MN_Connect_Click(object sender, EventArgs e)
        {
            FrmConnectServer frm = new FrmConnectServer();
            frm.ShowDialog();
            ConnectDataBase.ConnectionString = frm._ConnectDataBaseInfo.ConnectionString;

            if (frm._IsConnect)
                lblStatus.Text = "Đã kết nối CRM";
            else
                lblStatus.Text = "Chưa kết nối CRM";
        }

        private void MN_ImportExcel_Click(object sender, EventArgs e)
        {
            GetExcelSheet();
        }

        private void MN_CheckData_Click(object sender, EventArgs e)
        {

        }

        private void cboSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            OpenExcelSheet();
        }

        void GetExcelSheet()
        {
            cboSheet.Items.Clear();
            cboSheet.Text = "";

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Microsoft Excel 2007 (*.xlsx)|*.XLSX|All files (*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                mIndexSQL = 0;
                LVData.Items.Clear();
                mIndexExcel = 0;

                mFileNameImport = dlg.FileName;
                lblExcel.Text = "Tập tin Excel: " + mFileNameImport;

                if (mFileNameImport.Trim().Length > 0)
                {
                    int totalSheet = 0; //No of sheets on excel file  
                    using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + mFileNameImport + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                    {
                        objConn.Open();
                        OleDbCommand cmd = new OleDbCommand();
                        OleDbDataAdapter oleda = new OleDbDataAdapter();
                        DataSet ds = new DataSet();
                        DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = string.Empty;
                        if (dt != null)
                        {
                            var tempDataTable = (from dataRow in dt.AsEnumerable()
                                                 where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                                 select dataRow).CopyToDataTable();
                            dt = tempDataTable;
                            totalSheet = dt.Rows.Count;
                            for (int i = 0; i < totalSheet; i++)
                            {
                                cboSheet.Items.Add(dt.Rows[i]["TABLE_NAME"].ToString());
                            }
                            //sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                        }
                    }
                    Cursor.Current = Cursors.Default;
                }
            }
        }
        void OpenExcelSheet()
        {
            if (cboSheet.Text.Trim().Length > 0)
            {
                try
                {
                    LVData.Items.Clear();

                    Cursor.Current = Cursors.WaitCursor;
                    _dtExcel = new DataTable();

                    DataTable dtResult = null;
                    using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + mFileNameImport + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                    {
                        objConn.Open();
                        OleDbCommand cmd = new OleDbCommand();
                        OleDbDataAdapter oleda = new OleDbDataAdapter();
                        DataSet ds = new DataSet();
                        DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        cmd.Connection = objConn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT * FROM [" + cboSheet.Text + "]";
                        oleda = new OleDbDataAdapter(cmd);
                        oleda.Fill(ds, "excelData");
                        dtResult = ds.Tables["excelData"];
                        objConn.Close();

                        _dtExcel = dtResult;

                        mThreadExcel = new Thread(new ThreadStart(LoadDataFromTableExcelToListview));
                        mThreadExcel.Start();

                        //return dtResult; //Returning Dattable  
                    }

                    Cursor.Current = Cursors.Default;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Lỗi liên hệ hổ trợ!", MessageBoxButtons.OK);
                }
            }
        }

        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Danh xưng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "SĐT1";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "SĐT2";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Email1";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Email2";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "ĐC liên hệ";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "ĐC thường trú";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Dự án";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã căn";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        #region [ Excel to List ]
        void LoadDataFromTableExcelToListview()
        {
            for (int i = 1; i < _dtExcel.Rows.Count; i++)
            {
                UpdateExcelToLisView(_dtExcel.Rows[i]);
            }
            mThreadExcel.Abort();
        }
        void UpdateExcelToLisView(DataRow RowExcel)
        {
            if (this.LVData.InvokeRequired)
            {
                SetItemExcelCallback d = new SetItemExcelCallback(UpdateExcelToLisView);
                this.Invoke(d, new object[] { RowExcel });
            }
            else
            {
                string AssetID = RowExcel[11].ToString().Trim();
                string Project = RowExcel[10].ToString().Trim();

                int Result = Database_Data.Exsist(AssetID, Project);

                UpdateItemExcel(RowExcel, Result);
                LVData.EnsureVisible(LVData.Items.Count - 1);
            }
        }
        void UpdateItemExcel(DataRow nRow, int IsDuplicate)
        {
            mIndexExcel++;

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Name = mIndexSQL.ToString();

            lvi.Text = mIndexExcel.ToString();

            if (IsDuplicate == 1)
            {
                lvi.BackColor = Color.Maroon;
                lvi.ForeColor = Color.White;
                lvi.Tag = -1;
            }
            else
            {
                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;
                lvi.Tag = mIndexExcel;
            }
            lvi.ImageIndex = 0;

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[1].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[2].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[3].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[4].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[5].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[6].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[7].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[8].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[9].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[10].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[11].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[12].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            LVData.Items.Add(lvi);
        }
        #endregion

        private void chkDuplicate_CheckedChanged(object sender, EventArgs e)
        {
            if (LVData.Items.Count > 0)
            {
                if (chkDuplicate.Checked)
                {
                    for (int i = 0; i < LVData.Items.Count; i++)
                    {
                        if (int.Parse(LVData.Items[i].Tag.ToString()) == -1)
                        {
                            LVData.Items[i].Checked = true;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < LVData.Items.Count; i++)
                    {
                        if (int.Parse(LVData.Items[i].Tag.ToString()) == -1)
                        {
                            LVData.Items[i].Checked = false;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Chưa có data");
            }
        }

        private void btnClearData_Click(object sender, EventArgs e)
        {
            if (LVData.Items.Count > 0)
            {
                if (MessageBox.Show("Bạn muốn xóa data trùng ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    foreach (ListViewItem item in LVData.Items)
                    {
                        if (item.Checked)
                        {
                            LVData.Items.Remove(item);
                        }
                    }
                    //for (int i = 0; i < LVData.Items.Count; i++)
                    //{
                    //    if (LVData.Items[i].Checked)
                    //    {
                    //        LVData.Items[i].Remove();
                    //    }
                    //}
                }
            }
            else
            {
                MessageBox.Show("Chưa có data");
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (LVData.Items.Count > 0)
            {
                if (MessageBox.Show("Bạn đã chắc chắn thông tin để tải lên CRM ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Excel_Info zExcelFile = new Excel_Info();
                    zExcelFile.Date = DateTime.Now;
                    zExcelFile.FileAttack = "";
                    zExcelFile.CreatedDate = DateTime.Now;
                    zExcelFile.ModifiedDate = DateTime.Now;
                    zExcelFile.ModifiedBy = "99";
                    zExcelFile.CreatedBy = "99";
                    zExcelFile.ModifiedName = "System Administrator";
                    zExcelFile.CreatedName = "System Administrator";
                    zExcelFile.Create();

                    StringBuilder SQL = new StringBuilder();

                    foreach (ListViewItem item in LVData.Items)
                    {
                        SQL.AppendLine(@" 
INSERT INTO TASK_Excel_Detail (ExcelKey, InfoDate ,SirName ,FirstName ,LastName ,Phone1 ,Phone2 ,Email1 ,Email2 ,Address1 ,Address2, ProductID ,Product ,Asset, Status) 
VALUES ('" + zExcelFile.AutoKey + "','" + DateTime.Now + "', N'" +
    item.SubItems[1].ToString().Trim() + "', N'" + item.SubItems[2].ToString().Trim() + "', N'" +
    item.SubItems[3].ToString().Trim() + "', N'" + item.SubItems[4].ToString().Trim() + "', N'" +
    item.SubItems[5].ToString().Trim() + "', N'" + item.SubItems[6].ToString().Trim() + "', N'" +
    item.SubItems[7].ToString().Trim() + "',N'" + item.SubItems[8].ToString().Trim() + "', N'" +
    item.SubItems[9].ToString().Trim() + "',N'" + item.SubItems[10].ToString().Trim() + "',N'" +
    item.SubItems[11].ToString().Trim() + "',N'" + item.SubItems[12].ToString().Trim() + "', '0') ");

                        LVData.Items.Remove(item);
                    }
                    string Message = UpdateData.InsertUpdate(SQL.ToString());

                    if (Message != string.Empty)
                        MessageBox.Show("Lỗi tải tập tin !" + Message);
                    else
                        MessageBox.Show("Đã hoàn thành");
                }
            }
            else
            {
                MessageBox.Show("Chưa có data");
            }
        }

        private void MN_DelDuplicate_Click(object sender, EventArgs e)
        {

        }

        private void MN_ImportData_Click(object sender, EventArgs e)
        {

        }
    }
}
