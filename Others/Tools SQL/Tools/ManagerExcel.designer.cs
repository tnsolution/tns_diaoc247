﻿namespace Tools
{
    partial class ManagerExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LVData = new System.Windows.Forms.ListView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.connectCRMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_Connect = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_ImportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_CheckData = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_DelDuplicate = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_ImportData = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblExcel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnClearData = new System.Windows.Forms.Button();
            this.chkDuplicate = new System.Windows.Forms.CheckBox();
            this.lblAlert = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboSheet = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LVData
            // 
            this.LVData.CheckBoxes = true;
            this.LVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData.FullRowSelect = true;
            this.LVData.GridLines = true;
            this.LVData.Location = new System.Drawing.Point(0, 75);
            this.LVData.MultiSelect = false;
            this.LVData.Name = "LVData";
            this.LVData.Size = new System.Drawing.Size(1008, 632);
            this.LVData.TabIndex = 2;
            this.LVData.UseCompatibleStateImageBehavior = false;
            this.LVData.View = System.Windows.Forms.View.Details;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectCRMToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // connectCRMToolStripMenuItem
            // 
            this.connectCRMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MN_Connect,
            this.MN_ImportExcel,
            this.MN_CheckData,
            this.MN_DelDuplicate,
            this.MN_ImportData});
            this.connectCRMToolStripMenuItem.Name = "connectCRMToolStripMenuItem";
            this.connectCRMToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.connectCRMToolStripMenuItem.Text = "Menu";
            // 
            // MN_Connect
            // 
            this.MN_Connect.Name = "MN_Connect";
            this.MN_Connect.Size = new System.Drawing.Size(188, 22);
            this.MN_Connect.Text = "Kết nối dữ liệu";
            this.MN_Connect.Click += new System.EventHandler(this.MN_Connect_Click);
            // 
            // MN_ImportExcel
            // 
            this.MN_ImportExcel.Name = "MN_ImportExcel";
            this.MN_ImportExcel.Size = new System.Drawing.Size(188, 22);
            this.MN_ImportExcel.Text = "Nhập Excel";
            this.MN_ImportExcel.Click += new System.EventHandler(this.MN_ImportExcel_Click);
            // 
            // MN_CheckData
            // 
            this.MN_CheckData.Name = "MN_CheckData";
            this.MN_CheckData.Size = new System.Drawing.Size(188, 22);
            this.MN_CheckData.Text = "Kiểm tra trùng data";
            this.MN_CheckData.Visible = false;
            this.MN_CheckData.Click += new System.EventHandler(this.MN_CheckData_Click);
            // 
            // MN_DelDuplicate
            // 
            this.MN_DelDuplicate.Name = "MN_DelDuplicate";
            this.MN_DelDuplicate.Size = new System.Drawing.Size(188, 22);
            this.MN_DelDuplicate.Text = "Xóa trùng data";
            this.MN_DelDuplicate.Visible = false;
            this.MN_DelDuplicate.Click += new System.EventHandler(this.MN_DelDuplicate_Click);
            // 
            // MN_ImportData
            // 
            this.MN_ImportData.Name = "MN_ImportData";
            this.MN_ImportData.Size = new System.Drawing.Size(188, 22);
            this.MN_ImportData.Text = "Nhập data vào CRM";
            this.MN_ImportData.Visible = false;
            this.MN_ImportData.Click += new System.EventHandler(this.MN_ImportData_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lblExcel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 707);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 9.25F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(122, 17);
            this.lblStatus.Text = "Tình trạng kết nối";
            // 
            // lblExcel
            // 
            this.lblExcel.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblExcel.ForeColor = System.Drawing.Color.Black;
            this.lblExcel.Name = "lblExcel";
            this.lblExcel.Size = new System.Drawing.Size(23, 17);
            this.lblExcel.Text = "....";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnImport);
            this.panel1.Controls.Add(this.btnClearData);
            this.panel1.Controls.Add(this.chkDuplicate);
            this.panel1.Controls.Add(this.lblAlert);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cboSheet);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 51);
            this.panel1.TabIndex = 5;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(578, 10);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(144, 25);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "4. Tải data vào CRM";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnClearData
            // 
            this.btnClearData.Location = new System.Drawing.Point(445, 10);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(127, 25);
            this.btnClearData.TabIndex = 4;
            this.btnClearData.Text = "3. Xóa trùng data";
            this.btnClearData.UseVisualStyleBackColor = true;
            this.btnClearData.Click += new System.EventHandler(this.btnClearData_Click);
            // 
            // chkDuplicate
            // 
            this.chkDuplicate.AutoSize = true;
            this.chkDuplicate.Location = new System.Drawing.Point(285, 12);
            this.chkDuplicate.Name = "chkDuplicate";
            this.chkDuplicate.Size = new System.Drawing.Size(149, 20);
            this.chkDuplicate.TabIndex = 3;
            this.chkDuplicate.Text = "2. Chọn các file trùng";
            this.chkDuplicate.UseVisualStyleBackColor = true;
            this.chkDuplicate.CheckedChanged += new System.EventHandler(this.chkDuplicate_CheckedChanged);
            // 
            // lblAlert
            // 
            this.lblAlert.AutoSize = true;
            this.lblAlert.ForeColor = System.Drawing.Color.Navy;
            this.lblAlert.Location = new System.Drawing.Point(972, 14);
            this.lblAlert.Name = "lblAlert";
            this.lblAlert.Size = new System.Drawing.Size(24, 16);
            this.lblAlert.TabIndex = 2;
            this.lblAlert.Text = "....";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "1. Chọn sheet";
            // 
            // cboSheet
            // 
            this.cboSheet.FormattingEnabled = true;
            this.cboSheet.Location = new System.Drawing.Point(107, 11);
            this.cboSheet.Name = "cboSheet";
            this.cboSheet.Size = new System.Drawing.Size(162, 22);
            this.cboSheet.TabIndex = 0;
            this.cboSheet.SelectedIndexChanged += new System.EventHandler(this.cboSheet_SelectedIndexChanged);
            // 
            // ManagerExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.LVData);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ManagerExcel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý file excel";
            this.Load += new System.EventHandler(this.ManagerExcel_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView LVData;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem connectCRMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MN_Connect;
        private System.Windows.Forms.ToolStripMenuItem MN_ImportExcel;
        private System.Windows.Forms.ToolStripMenuItem MN_CheckData;
        private System.Windows.Forms.ToolStripMenuItem MN_DelDuplicate;
        private System.Windows.Forms.ToolStripMenuItem MN_ImportData;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblExcel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboSheet;
        private System.Windows.Forms.Label lblAlert;
        private System.Windows.Forms.CheckBox chkDuplicate;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnClearData;
    }
}