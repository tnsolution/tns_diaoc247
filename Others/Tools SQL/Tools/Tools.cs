﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib.Misc;
using TNConfig;

namespace ReadWrite_User
{
    public partial class Tools : Form
    {
        public Tools()
        {
            InitializeComponent();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            FrmConnectServer frm = new FrmConnectServer();
            frm.ShowDialog();
            ConnectDataBase.ConnectionString = frm._ConnectDataBaseInfo.ConnectionString;


            cboTable.DataSource = Database_Data.GetTable();
            cboTable.DisplayMember = "NAME";
            cboTable.ValueMember = "NAME";
        }

        private void FrmReadWriteUser_Load(object sender, EventArgs e)
        {

        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            GvData.DataSource = Database_Data.GetData(cboTable.Text);
        }

        private void GvData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow zRowEdit = GvData.Rows[e.RowIndex];

                string ValueWhere = zRowEdit.Cells[0].Value.ToString();
                string ColumnWhere = GvData.Columns[0].HeaderText;

                string ColumnUpdate = GvData.Columns[e.ColumnIndex].HeaderText;
                string ValueUpdate = zRowEdit.Cells[e.ColumnIndex].Value.ToString();

                UpdateData zUpdate = new UpdateData();
                zUpdate.InsertUpdate(cboTable.Text, ColumnUpdate, ValueUpdate, ColumnWhere, ValueWhere);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }            
        }
    }
}
