﻿using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ReportEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ReportKey.Value = Request["ID"];

                LoadInfo();
            }
        }
        void LoadInfo()
        {
            int Key = HID_ReportKey.Value.ToInt();
            Report_Info zInfo = new Report_Info(Key);
            txt_Date.Value = zInfo.ReportDate.ToString("dd/MM/yyyy");
            txt_Description.Value = zInfo.Note;

            if (zInfo.IsDraft == 1)
                Lit_TitlePage.Text = "Lập báo cáo.";
            else
                Lit_TitlePage.Text = "Chỉnh sửa báo cáo.";

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy") + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();

            zSb = new StringBuilder();
            zSb.AppendLine("<table id='tbldata' class='table  table-bordered table-hover'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Chỉ tiêu/ tháng</th>");
            zSb.AppendLine("        <th>Kết quả (chỉ nhập số)</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("        <tbody>");

            int i = 0;
            DataTable zChild = Report_Rec_Data.List(zInfo.Key, zInfo.ReportDate.Month, zInfo.ReportDate.Year);
            if (zChild.Rows.Count > 0)
            {
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["Result"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }
            }
            else
            {
                zChild = Plan_Rec_Data.List(zInfo.EmployeeKey, DateTime.Now.Month, DateTime.Now.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>0</td>");
                    zSb.AppendLine("    </tr>");
                }
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            Lit_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemResult InitInfo(string Date, string Description)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            DateTime ReportDate = Tools.ConvertToDate(Date);
            ItemResult zResult = new ItemResult();
            Report_Info zInfo = new Report_Info(Employee, ReportDate);
            if (zInfo.Key > 0)
            {
                zResult.Message = "Bạn đã lập báo cáo cho ngày " + Date + " rồi !.";
                zResult.Result = zInfo.Key.ToString();
                return zResult;
            }

            zInfo.ReportDate = Tools.ConvertToDate(Date);
            zInfo.Note = Description;
            zInfo.Title = "Báo cáo:" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");
            zInfo.IsDraft = 1;
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "OK";
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveReport(int Key, string Date, string Description, string Table)
        {
            ItemResult zResult = new ItemResult();
            Report_Info zInfo = new Report_Info(Key);
            zInfo.ReportDate = Tools.ConvertToDate(Date);
            zInfo.Note = Description;
            zInfo.IsDraft = 0;
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            if (zInfo.Message == string.Empty)
            {
                string Message = new Report_Rec_Info().Delete(zInfo.Key);

                List<ItemWeb> zList = new List<ItemWeb>();
                string[] row = Table.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in row)
                {
                    string[] col = item.Split(',');
                    ItemWeb itemWeb = new ItemWeb();
                    itemWeb.Value = col[0];
                    itemWeb.Text = col[1];
                    zList.Add(itemWeb);
                }

                foreach (ItemWeb item in zList)
                {
                    Report_Rec_Info zRecord = new Report_Rec_Info();
                    zRecord.ReportKey = zInfo.Key;
                    zRecord.CategoryKey = item.Value.ToInt();
                    zRecord.Result = item.Text.ToInt();
                    zRecord.Create();
                }
            }

            zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}