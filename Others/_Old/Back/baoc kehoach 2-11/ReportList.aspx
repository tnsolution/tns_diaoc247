﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ReportList.aspx.cs" Inherits="WebApp.SAL.ReportList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .profile-info-name {
            width: 250px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal runat="server" ID="LitPage_Head"></asp:Literal>
                <span class="tools pull-right">               
                    <a href="#mReportBox" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="row">
                    <ul class="list-unstyled spaced">
                        <asp:Literal ID="LitReport_Title" runat="server"></asp:Literal>
                    </ul>
                </div>
                <h4 class="header smaller lighter blue">
                    <i class="ace-icon fa fa-hand-o-right green"></i>&nbsp ....</></h4>
                <div class="row">
                    <div class="col-sm-12">
                        <asp:Literal ID="LitReport_Data" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Báo cáo đã lập</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div id="scroll">
                                <table class='table  table-bordered table-hover' id='tblData'>
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>
                                                <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                                                </asp:DropDownList></th>
                                            <th>
                                                <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                                                </asp:DropDownList></th>
                                            <th>Ngày/ tháng</th>
                                            <th>Ghi chú</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="LitReport_TableList" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mSearch" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Chọn điều kiện</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input name="form-field-radio" type="radio" class="ace input" value="0" checked />
                                <span class="lbl bigger-120">Xem ngày</span>
                            </label>
                            <small>Xem mục tiêu trong ngày đã thực hiện</small>
                        </div>
                        <div class="radio">
                            <label>
                                <input name="form-field-radio" type="radio" class="ace input" value="1" />
                                <span class="lbl bigger-120">Xem tháng</span>
                            </label>
                            <small>Tổng hợp các mục tiêu đạt được trong tháng</small>
                        </div>
                    </div>
                    <div class="form-group" id="viewDate">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_FromDate" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </div>
                    <div class="form-group" id="viewMonth">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_ToDate" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="triggerSearch">OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ReportKey" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script src="/template/tableHeadFixer.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2").select2({ width: "100%" });
            $("#tblData tbody").on("click", "tr", function () {
                var trid = $(this).closest('tr').attr('id');
                $('.se-pre-con').fadeIn('slow');
                window.location = "/SAL/ReportList.aspx?ID=" + trid;
            });
            $('#scroll').ace_scroll({
                size: 400,
            });
            $("#tblData").tableHeadFixer();
            $("[id$=DDL_Employee]").change(function () {
                myFunction(this, 2, 1);
            })
            $("[id$=DDL_Department]").change(function () {
                myFunction(this, 1, 1);
            })
        });
        function myFunction(arg, no, select) {
            var input, filter, table, tr, td, i;
            input = $("#" + arg.getAttribute("id")).val();
            if (input == 0) {
                for (i = 0; i < tr.length; i++) { tr[i].style.display = ""; }
                return false;
            }
            else {
                if (select == 1) {
                    input = $("#" + arg.getAttribute("id") + ' option:selected').text();
                }
            }
            filter = input.toUpperCase();
            table = $("#tblData");
            tr = table.find("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[no];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
</asp:Content>