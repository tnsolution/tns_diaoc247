﻿using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebApp.SAL
{
    public partial class ReportList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ReportKey.Value = Request["ID"].ToString();

                CheckRole();
                LoadInfo();
                LoadData();
            }
        }
        void LoadInfo()
        {
            StringBuilder zSb = new StringBuilder();
            Report_Info zInfo = new Report_Info(HID_ReportKey.Value.ToInt());
            if (zInfo.Key == 0)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo = new Report_Info(EmployeeKey, true);
            }

            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Nhân viên: " + zInfo.CreatedName);
            zSb.AppendLine("</li>");
            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Báo cáo ngày: " + zInfo.ReportDate.ToString("dd/MM/yyyy"));
            zSb.AppendLine("</li>");
            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Ghi chú: " + zInfo.Note);
            zSb.AppendLine("</li>");
            LitReport_Title.Text = zSb.ToString();

            DataTable zChild = Report_Rec_Data.List(zInfo.Key, zInfo.ReportDate.Month, zInfo.ReportDate.Year);
            zSb = new StringBuilder();
            zSb.AppendLine("                        <div class='row'>");
            zSb.AppendLine("                            <div class='col-xs-12'>");
            zSb.AppendLine("                                <div class='profile-user-info profile-user-info-striped'>");

            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Mục tiêu</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>");
            zSb.AppendLine("                                            <div class=col-sm-4><span>Kết quả trong ngày</span></div><div class=col-sm-7>Tổng kết quả/ tháng</div>");
            zSb.AppendLine("                                        </div>");
            zSb.AppendLine("                                    </div>");

            for (int j = 0; j < zChild.Rows.Count; j++)
            {
                DataRow rChild = zChild.Rows[j];
                zSb.AppendLine("                                <div class='profile-info-row'>");
                zSb.AppendLine("                                    <div class='profile-info-name'>" + rChild["CategoryName"].ToString() + "</div>");
                zSb.AppendLine("                                        <div class='profile-info-value'>");
                zSb.AppendLine("                                           <div class=col-sm-4><span>" + rChild["Result"].ToString() + "</span></div><div class=col-sm-7><span>" + rChild["Doing"].ToString() + "</span> / <span>" + rChild["Require"].ToString() + "</span></div>");
                zSb.AppendLine("                                        </div>");
                zSb.AppendLine("                                    </div>");
            }

            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                            </div>");
            zSb.AppendLine("                        </div>");

            LitReport_Data.Text = zSb.ToString();

            LitPage_Head.Text = "Báo cáo " + zInfo.EmployeeName + " ngày " + zInfo.ReportDate.ToString("dd/MM/yyyy");
        }
        void LoadData()
        {
            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            int i = 0;
            int Employee = DDL_Employee.SelectedValue.ToInt();
            int Department = DDL_Department.SelectedValue.ToInt();
            int Unit = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            StringBuilder zSb = new StringBuilder();
            DataTable zMaster = Report_Data.List(Department, Employee, FromDate, ToDate);

            foreach (DataRow r in zMaster.Rows)
            {
                zSb.AppendLine("            <tr class='' id=" + r["ReportKey"].ToString() + ">");
                zSb.AppendLine("                <td>" + i++ + "</td>");
                zSb.AppendLine("                <td>" + r["DepartmentName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["ReportDate"].ToDateString() + "</td>");
                zSb.AppendLine("                <td>" + r["Note"].ToString() + "</td>");
                zSb.AppendLine("            </tr>");
            }

            LitReport_TableList.Text = zSb.ToString();
        }
        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking = 2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}