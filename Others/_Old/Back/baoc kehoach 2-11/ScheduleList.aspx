﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleList.aspx.cs" Inherits="WebApp.SAL.ScheduleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal runat="server" ID="LitPage_Head"></asp:Literal>
                <span class="tools pull-right">
                    <a href="#mScheduleBox" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="row">
                    <ul class="list-unstyled spaced">
                        <asp:Literal ID="LitSchedule_Title" runat="server"></asp:Literal>
                    </ul>
                </div>
                <h4 class="header smaller lighter blue">
                    <i class="ace-icon fa fa-hand-o-right green"></i>&nbsp ....</></h4>
                <div class="row">
                    <div class="col-sm-12">
                        <asp:Literal ID="LitSchedule_Data" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Kế hoạch đã lập</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div id="scroll">
                                <table class='table  table-bordered table-hover' id='tblData'>
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>
                                                <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                                                </asp:DropDownList></th>
                                            <th>
                                                <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                                                </asp:DropDownList></th>
                                            <th>Ngày</th>
                                            <th>Ghi chú</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Literal ID="LitSchedule_TableList" runat="server"></asp:Literal>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--modal-->
    <div class="modal fade" id="mScheduleDetail" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Kế hoạch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Thời gian </label>
                        <div class="input-group bootstrap-timepicker" id="datepairExample">
                            <input type="text" id="txt_From" class="form-control col-sm-6 time start" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                            <input type="text" id="txt_To" class="form-control col-sm-6 time end" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                        <div>
                            <input name="txt_Content" type="text" id="txt_Content" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSaveRecord" style="display: none">OK</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnUpdateRecord" style="display: none">OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ScheduleKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $(".select2").select2({ width: "100%" });
            $("#tblData tbody").on("click", "tr", function () {
                var trid = $(this).closest('tr').attr('id');
                $('.se-pre-con').fadeIn('slow');
                window.location = "/SAL/ScheduleList.aspx?ID=" + trid;
            });
            $('#scroll').ace_scroll({
                size: 400,
            });
            $("#tblData").tableHeadFixer();
            $("[id$=DDL_Employee]").change(function () {
                myFunction(this, 2, 1);
            })
            $("[id$=DDL_Department]").change(function () {
                myFunction(this, 1, 1);
            })
        });
        function myFunction(arg, no, select) {
            var input, filter, table, tr, td, i;
            input = $("#" + arg.getAttribute("id")).val();
            if (input == 0) {
                for (i = 0; i < tr.length; i++) { tr[i].style.display = ""; }
                return false;
            }
            else {
                if (select == 1) {
                    input = $("#" + arg.getAttribute("id") + ' option:selected').text();
                }
            }
            filter = input.toUpperCase();
            table = $("#tblData");
            tr = table.find("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[no];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
</asp:Content>
