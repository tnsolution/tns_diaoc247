﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ScheduleList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ScheduleKey.Value = Request["ID"].ToString();

                CheckRole();
                LoadInfo();
                LoadData();
            }
        }
        void LoadInfo()
        {
            StringBuilder zSb = new StringBuilder();
            Schedule_Info zInfo = new Schedule_Info(HID_ScheduleKey.Value.ToInt());
            if (zInfo.Key == 0)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo = new Schedule_Info(EmployeeKey, true);
            }

            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Nhân viên: " + zInfo.CreatedName);
            zSb.AppendLine("</li>");
            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Ngày hiển thị kế hoạch: " + zInfo.ScheduleDate.ToString("dd/MM/yyyy"));
            zSb.AppendLine("</li>");
            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Ghi chú: " + zInfo.Description);
            zSb.AppendLine("</li>");
            LitSchedule_Title.Text = zSb.ToString();

            DataTable zChild = Schedule_Rec_Data.List(zInfo.Key);
            zSb = new StringBuilder();
            zSb.AppendLine("                        <div class='row'>");
            zSb.AppendLine("                            <div class='col-xs-12'>");
            zSb.AppendLine("                                <div class='profile-user-info profile-user-info-striped'>");

            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Thời gian</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>");
            zSb.AppendLine("                                            <span>Nội dung</span>");
            zSb.AppendLine("                                        </div>");
            zSb.AppendLine("                                    </div>");

            for (int j = 0; j < zChild.Rows.Count; j++)
            {
                DataRow rChild = zChild.Rows[j];
                zSb.AppendLine("                                <div class='profile-info-row'>");
                zSb.AppendLine("                                    <div class='profile-info-name'>" + rChild["WorkTime"].ToString() + "</div>");
                zSb.AppendLine("                                        <div class='profile-info-value'>");
                zSb.AppendLine("                                            <span>" + rChild["WorkContent"].ToString() + "</span>");
                zSb.AppendLine("                                        </div>");
                zSb.AppendLine("                                    </div>");
            }

            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                            </div>");
            zSb.AppendLine("                        </div>");

            LitSchedule_Data.Text = zSb.ToString();


            LitPage_Head.Text = "Kế hoạch " + zInfo.EmployeeName + " ngày " + zInfo.ScheduleDate.ToString("dd/MM/yyyy");
        }
        void LoadData()
        {
            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int EmployeeKey = DDL_Employee.SelectedValue.ToInt();
            int DepartmentKey = DDL_Department.SelectedValue.ToInt();

            StringBuilder zSb = new StringBuilder();
            DataTable zMaster = Schedule_Data.List(DepartmentKey, EmployeeKey, FromDate, ToDate);
            int i = 1;
            foreach (DataRow r in zMaster.Rows)
            {
                zSb.AppendLine("            <tr class='' id='" + r["TimeKey"].ToString() + "'>");
                zSb.AppendLine("                <td>" + i++ + "</td>");
                zSb.AppendLine("                <td>" + r["DepartmentName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["ScheduleDate"].ToDateString() + "</td>");
                zSb.AppendLine("                <td>" + r["Description"].ToString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            LitSchedule_TableList.Text = zSb.ToString();
        }
        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;

                    LitScript.Text = "<script>$(document).ready(function () {$('#tblData td:nth-child(2)').hide(); $('#tblData th:nth-child(2)').hide();})</script>";
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking = 2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;

                    LitScript.Text = "<script>$(document).ready(function () {$('#tblData td:nth-child(2)').hide(); $('#tblData th:nth-child(2)').hide();})</script>";
                    break;
            }
        }
    }
}