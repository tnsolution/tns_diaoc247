﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleList.aspx.cs" Inherits="WebApp.SAL.ScheduleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Kế hoạch đã lập trong ngày
                <asp:Label ID="lblDateView" runat="server" Text="..."></asp:Label>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnPrev">
                        <i class="ace-icon fa fa-arrow-left"></i>Trước
                    </button>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnNext">
                        Sau<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                    </button>
                    <a href="#mSearch" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm lọc
                    </a>
                    <a href="#mScheduleBox" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <!--modal-->
    <div class="modal fade" id="mSearch" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Chọn điều kiện</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSearch">OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_FromDate" runat="server" Value="" />
    <asp:HiddenField ID="HID_ToDate" runat="server" Value="" />
    <asp:HiddenField ID="HID_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnView" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2").select2({ width: "100%" });

            $("#btnPrev").click(function () {
                $("[id$=HID_ViewNextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#btnNext").click(function () {
                $("[id$=HID_ViewNextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $("#btnSearch").click(function () {
                $("[id$=btnView]").trigger("click");
            });

            $('.show-details-btn').on('click', function (e) {
                e.preventDefault();
                $(this).closest('tr').next().toggleClass('open');
                $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
            });

            $("#tblData tbody tr td:not(:last-child)").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "ScheduleView.aspx?ID=" + trid;
            });
        });
    </script>
</asp:Content>
