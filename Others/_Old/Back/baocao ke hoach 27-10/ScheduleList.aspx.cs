﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ScheduleList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();

                LoadData();
            }
        }

        void LoadData()
        {
            int EmployeeKey = DDL_Employee.SelectedValue.ToInt();
            int DepartmentKey = DDL_Department.SelectedValue.ToInt();

            StringBuilder zSb = new StringBuilder();
            DataTable zMaster = Schedule_Data.List(
                DepartmentKey, EmployeeKey,
                Convert.ToDateTime(HID_FromDate.Value),
                Convert.ToDateTime(HID_ToDate.Value));

            lblDateView.Text = Convert.ToDateTime(HID_ToDate.Value).ToString("dd/MM/yyyy");

            zSb.AppendLine("<table class='table  table-bordered table-hover' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>Ngày</th>");          
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            //int i = 1;
            //foreach (DataRow r in zMaster.Rows)
            //{
            //    string detail = @"<div class='action-buttons'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div>";

            //    #region Master Tr
            //    zSb.AppendLine("            <tr class='' id='" + r["TimeKey"].ToString() + "'>");
            //    zSb.AppendLine("                <td>" + i++ + "</td>");
            //    zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
            //    zSb.AppendLine("                <td>" + r["ScheduleDate"].ToDateString() + "</td>");
            //    zSb.AppendLine("                <td>" + r["Description"].ToString() + "</td>");
            //    zSb.AppendLine("                <td>" + detail + "</td>");
            //    zSb.AppendLine("            </tr>");
            //    #endregion

            //    #region [Child Tr]
            //    DataTable zChild = Schedule_Rec_Data.List(r["TimeKey"].ToInt());
            //    zSb.AppendLine("            <tr class='detail-row' id='" + r["TimeKey"].ToString() + "'>");
            //    zSb.AppendLine("                <td colspan='5'>");
            //    zSb.AppendLine("                    <div class='table-detail'>");
            //    zSb.AppendLine("                        <div class='row'>");
            //    zSb.AppendLine("                            <div class='col-xs-12 col-sm-7'>");
            //    zSb.AppendLine("                                <div class='profile-user-info profile-user-info-striped'>");

            //    for (int j = 0; j < zChild.Rows.Count; j++)
            //    {
            //        DataRow rChild = zChild.Rows[j];
            //        zSb.AppendLine("                                <div class='profile-info-row'>");
            //        zSb.AppendLine("                                    <div class='profile-info-name'>" + rChild["WorkTime"].ToString() + "</div>");
            //        zSb.AppendLine("                                        <div class='profile-info-value'>");
            //        zSb.AppendLine("                                            <span>" + rChild["WorkContent"].ToString() + "</span>");
            //        zSb.AppendLine("                                        </div>");
            //        zSb.AppendLine("                                    </div>");
            //    }

            //    zSb.AppendLine("                                </div>");
            //    zSb.AppendLine("                            </div>");
            //    zSb.AppendLine("                        </div>");
            //    zSb.AppendLine("                    </div>");
            //    zSb.AppendLine("                </td>");
            //    zSb.AppendLine("            </tr>");
            //    #endregion
            //}

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            int ViewNextPrev = HID_ViewNextPrev.Value.ToInt();
            if (ViewNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HID_FromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddDays(1);
                ToDate = ToDate.AddDays(1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();

            }

            if (ViewNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HID_FromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddDays(-1);
                ToDate = ToDate.AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }

            LoadData();
        }

        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking = 2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}