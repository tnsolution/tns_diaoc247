﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ReportList.aspx.cs" Inherits="WebApp.SAL.ReportList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/fullcalendar.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" />
    <style>
        .fc-content {
            white-space: inherit !important;
            font-size: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="page-content">
        <div class="page-header">
            <h1 id="tieudetrang">Nhập báo cáo
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="row">
                    <table class='table'>
                        <tr>
                            <td style="width: 150px"><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td>
                            <td id="nguoicapnhat"></td>
                        </tr>
                        <tr>
                            <td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td>
                            <td id="ngaycapnhat"></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="form-group">
                        <input id="txt_title" type="text" class='form-control' placeholder='Tiêu đề' value="" />
                    </div>
                </div>
                <div class="row" id="noidungbentrai">
                    <table id="tbldata" class="table  table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Chỉ tiêu</th>
                                <th>Kết quả ngày</th>
                                <th>Yêu cầu/ tháng</th>
                                <th>Đã thực hiện/ tháng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="edit-disabled" tabindex="1">0</td>
                                <td class="edit-disabled" category="288" tabindex="1">Đăng tin</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">1</td>
                                <td class="edit-disabled" category="306" tabindex="1">Gọi điện chăm sóc khách hàng</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">2</td>
                                <td class="edit-disabled" category="307" tabindex="1">Gọi điện khai thác/cập nhật SP</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">3</td>
                                <td class="edit-disabled" category="297" tabindex="1">Bài viết zalo</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">4</td>
                                <td class="edit-disabled" category="296" tabindex="1">Bài viết facebook</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">5</td>
                                <td class="edit-disabled" category="291" tabindex="1">Khách phát sinh mới</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">6</td>
                                <td class="edit-disabled" category="290" tabindex="1">Khách lên dự án</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">7</td>
                                <td class="edit-disabled" category="298" tabindex="1">Đặt cọc</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                            <tr>
                                <td class="edit-disabled" tabindex="1">8</td>
                                <td class="edit-disabled" category="295" tabindex="1">Ký hợp đồng</td>
                                <td tabindex="1">0</td>
                                <td class="edit-disabled">0</td>
                                <td class="edit-disabled">0</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class='row'>
                    <div class="form-group">
                        <textarea class='form-control' id='txt_Description' placeholder='Ghi chú' rows='4'></textarea>
                    </div>
                </div>
                <div class="row">
                    <a class="btn btn-primary" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o"></i>
                        Cập nhật
                    </a>
                </div>
            </div>
            <div class="col-xs-6">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Chọn phòng--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button" id="btntim">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <input type="hidden" id="eventStartDate" />
    <input type="hidden" id="eventEndDate" />
    <input type="hidden" id="eventId" value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/template/numeric-input-example.js"></script>
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    <script>
        var Employee = getEmployeeKey();
        var Department = getDepartmentKey();

        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();
        });

        jQuery(function ($) {
            var unit = getUnitLevel();
            if (unit <= 1) {
                $(".modal.aside").ace_aside();
            }

            $(".select2").select2({ width: "100%" });
            $("#btnSearch").click(function () {
                Employee = $('[id$=DDL_Employee]').val();
                Department = $('[id$=DDL_Department]').val();
                if (Employee == null) {
                    alert("Bạn phải chọn nhân viên");
                }
                if (Department == null) {
                    alert("Bạn phải chọn phòng");
                }
                $('#calendar').fullCalendar('destroy');
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: '',// 'month,agendaWeek,agendaDay'//
                    },
                    defaultView: 'month',
                    eventClick: updateEvent,
                    selectable: true,
                    selectHelper: true,
                    select: selectDate,
                    editable: true,
                    events: '/JsonResponse_Report.ashx?Employee=' + Employee + '&Department=' + Department,
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventRender: function (event, element) {
                        //alert(event.title);
                        element.qtip({
                            content: {
                                text: qTipText(event.start, event.end, event.description),
                                title: '<strong>' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top center',
                                at: 'bottom center'
                            },
                            style: { classes: 'qtip-shadow qtip-rounded' }
                        });
                    }
                });
            });

            $('[id$=DDL_Department]').change(function () {
                var Key = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        DepartmentKey: $(this).val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee]");
                        District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                        $.each(msg.d, function () {
                            District.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        jQuery(function ($) {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: '',//;'month,agendaWeek,agendaDay'//
                },
                defaultView: 'month',
                eventClick: updateEvent,
                selectable: true,
                selectHelper: true,
                select: selectDate,
                editable: true,
                events: '/JsonResponse_Report.ashx?Employee=' + Employee + '&Department=' + Department,
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventRender: function (event, element) {
                    //alert(event.title);
                    element.qtip({
                        content: {
                            text: qTipText(event.start, event.end, event.description),
                            title: '<strong>' + event.title + '</strong>'
                        },
                        position: {
                            my: 'top center',
                            at: 'bottom center'
                        },
                        style: { classes: 'qtip-shadow qtip-rounded' }
                    });
                }
            });
            $("#btnSave").click(function () {
                var total = $('#tbldata tbody tr').length;
                var row = "";
                var num = 0;
                $('#tbldata tbody tr').each(function () {
                    num++;
                    if (num < total)
                        row += "" + $(this).find('td:eq(1)').attr('Category') + "," + $(this).find('td:eq(2)').text() + ";";
                    else
                        row += "" + $(this).find('td:eq(1)').attr('Category') + "," + $(this).find('td:eq(2)').text();
                });
                if ($("#eventStartDate").val() == '') {
                    alert("Bạn phải chọn ngày lập báo cáo");
                    return false;
                }
                $.ajax({
                    type: 'POST',
                    url: '/SAL/ReportList.aspx/SaveReport',
                    data: JSON.stringify({
                        'Key': $("#eventId").val(),
                        'StartDate': $("#eventStartDate").val(),
                        'EndDate': $("#eventEndDate").val(),
                        'Title': $("[id$=txt_title]").val(),
                        'Description': $("[id$=txt_Description]").val(),
                        'Table': row,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                        }
                        else
                            location.reload();
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });

        var currentUpdateEvent;
        var addStartDate;
        var addEndDate;
        var globalAllDay;

        function eventDropped(event, dayDelta, minuteDelta, allDay, revertFunc) {
            if ($(this).data("qtip")) $(this).qtip("destroy");

            updateEventOnDropResize(event);
        }
        function eventResized(event, dayDelta, minuteDelta, revertFunc) {
            if ($(this).data("qtip")) $(this).qtip("destroy");

            updateEventOnDropResize(event);
        }
        function eventResized(event, dayDelta, minuteDelta, revertFunc) {
            if ($(this).data("qtip")) $(this).qtip("destroy");

            updateEventOnDropResize(event);
        }

        function updateEvent(event, element) {
            $("#eventId").val(event.id);
            $.ajax({
                type: 'POST',
                url: '/SAL/ReportList.aspx/GetRecord',
                data: JSON.stringify({
                    ReportKey: event.id,
                    ReportDate: '',
                    EmployeeKey: Employee
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (r) {
                    $("#btnSave").hide();

                    $("#noidungbentrai").empty();
                    $("#noidungbentrai").append($(r.d.Message));

                    $("#nguoicapnhat").html(r.d.Result);
                    $("#ngaycapnhat").html(r.d.Result2);

                    $("#txt_title").val(r.d.Result3);
                    $("#txt_Description").val(r.d.Result4);
                    $('#tbldata').editableTableWidget().
                        numericInputExample().
                        find('td:nth-child(2)').
                        focus(function () {
                            $(this).select();
                        });

                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Lỗi: " + xhr.responseText);
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function () {
                }
            });
            return false;
        }
        function selectDate(start, end, allDay) {
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;

            $("#eventId").val(0);
            $("#txt_title").val("Báo cáo ngày " + moment(addStartDate.toJSON()).format("DD/MM/YYYY"));
            $.ajax({
                type: 'POST',
                url: '/SAL/ReportList.aspx/GetRecord',
                data: JSON.stringify({
                    ReportKey: 0,
                    ReportDate: start,
                    EmployeeKey: Employee
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (r) {
                    $("#eventStartDate").val(addStartDate.toJSON());
                    $("#eventEndDate").val(addEndDate.toJSON());

                    $("#noidungbentrai").empty();
                    $("#noidungbentrai").append($(r.d.Message));

                    $("#nguoicapnhat").html(r.d.Result);
                    $("#ngaycapnhat").html(r.d.Result2);

                    $("#tbldata").editableTableWidget().
                        numericInputExample().
                        find('td:nth-child(2)').
                        focus(function () {
                            $(this).select();
                        });

                    if (r.d.Result5 != 0)
                        $("#btnSave").hide();
                    else
                        $("#btnSave").show();

                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Lỗi: " + xhr.responseText);
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function () {
                }
            });
        }

        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }
        function qTipText(start, end, description) {
            var text;

            //if (end !== null)
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") + "<br/><br/>" + description;
            //else
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><br/>" + description;

            return description;
        }

        function getUnitLevel() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[3].split('=');
                    return cookies[1];
                }
            }
        }
        function getEmployeeKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[2].split('=');
                    return cookies[1];
                }
            }
        }
        function getDepartmentKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[1].split('=');
                    return cookies[1];
                }
            }
        }
    </script>
</asp:Content>
