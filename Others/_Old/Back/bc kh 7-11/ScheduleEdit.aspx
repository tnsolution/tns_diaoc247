﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleEdit.aspx.cs" Inherits="WebApp.SAL.ScheduleEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/jquery.timepicker.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày hiển thị kế hoạch</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                        id="txt_Date" placeholder="Chọn ngày tháng năm" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ghi chú</label>
                            <div class="col-sm-5">
                                <input type="text" runat="server" class="form-control" id="txt_Description" placeholder="..." required />
                            </div>
                        </div>

                    </div>
                </div>
                <h4 class="header smaller lighter blue">
                    <a href="#mScheduleDetail" data-toggle="modal"><i class="ace-icon fa fa-plus green"></i>Thêm nội dung</a></h4>
                <div class="row">
                    <div class="col-sm-9">
                        <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mScheduleDetail" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Chỉnh sữa nội dụng</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Thời gian </label>
                        <div class="input-group bootstrap-timepicker" id="datepairExample">
                            <input type="text" id="txt_From" class="form-control col-sm-6 time start" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                            <input type="text" id="txt_To" class="form-control col-sm-6 time end" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                        <div>
                            <input name="txt_Content" type="text" id="txt_Content" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSaveRecord">OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_DetailRecord" runat="server" Value="0" />
    <asp:HiddenField ID="HID_ScheduleKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/template/datepair.js"></script>
    <script type="text/javascript" src="/template/jquery.datepair.js"></script>
    <script>
        jQuery(function ($) {
            $("#btnSaveRecord").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/SAL/ScheduleEdit.aspx/SaveDetailRecord',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_DetailRecord]").val(),
                        'Key': $("[id$=HID_ScheduleKey]").val(),
                        'Time': $("#txt_From").val() + "-" + $("#txt_To").val(),
                        'Content': $("#txt_Content").val()
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        if (msg.d.Message.length <= 0) {
                            populateTable();
                        }
                        else {
                            Page.showPopupMessage("Lỗi nhập dòng tin", msg.d.Message);
                        }
                    },
                    complete: function () {
                        $("[id$=HID_DetailRecord]").val(0);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSave").click(function () {
                if (validate()) {
                    $.ajax({
                        type: 'POST',
                        url: '/SAL/ScheduleEdit.aspx/SaveSchedule',
                        data: JSON.stringify({
                            'Key': $("[id$=HID_ScheduleKey]").val(),
                            'Date': $("[id$=txt_Date]").val(),
                            'Description': $("[id$=txt_Description]").val()
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        beforeSend: function () {
                        },
                        success: function (msg) {
                            if (msg.d.Message.length <= 0) {
                                Page.showNotiMessageInfo("Lưu thành công", "Kế hoạch bạn sẽ hiển thị vào ngày đã chọn");
                                window.location = "/SAL/ScheduleList.aspx";
                            }
                            else {
                                Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                            }
                        },
                        complete: function () {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $("#btnDelete").click(function () {
                if ($("[id$=HID_ScheduleKey]").val() > 0) {
                    if (confirm("Bạn có chắc xóa.")) {
                        $.ajax({
                            type: "POST",
                            url: "/SAL/ScheduleEdit.aspx/DeleteSchedule",
                            data: JSON.stringify({
                                "Key": $("[id$=HID_ScheduleKey]").val(),
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $(".se-pre-con").fadeIn("slow");
                            },
                            success: function (msg) {
                                if (msg.d.Message != "") {
                                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                                }
                                else {
                                    location.href = "/SAL/ScheduleList.aspx";
                                }
                            },
                            complete: function () {
                                $(".se-pre-con").fadeOut("slow");
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr.status);
                                console.log(xhr.responseText);
                                console.log(thrownError);
                            }
                        });
                    }
                }
            });
            $("#tbldata > tbody").on("click", "tr", function () {
                var AutoKey = $(this).closest('tr').attr('id');
                $.ajax({
                    type: 'POST',
                    url: '/SAL/ScheduleEdit.aspx/GetDetailRecord',
                    data: JSON.stringify({
                        'AutoKey': AutoKey
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        if (msg.d.Message == '') {
                            $('#mScheduleDetail').modal({
                                backdrop: true,
                                show: true
                            });
                            $("[id$=HID_DetailRecord]").val(msg.d.ID);
                            var array = msg.d.WorkTime.split('-');
                            $("#txt_From").val(array[0]);
                            $("#txt_To").val(array[1]);
                            $("#txt_Content").val(msg.d.WorkContent);
                        }
                        else {
                            Page.showPopupMessage("Lỗi lấy dòng tin", msg.d.Message);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $('#txt_From').timepicker({
                'minTime': '00:00am',
                'maxTime': '23:59pm',
                'showDuration': false
            });
            $('#txt_To').timepicker({
                'minTime': '00:00am',
                'maxTime': '23:59pm',
                'showDuration': false
            });
            $('#datepairExample').datepair({ 'defaultTimeDelta': 1800000 });       
        })

        function populateTable() {
            $.ajax({
                type: 'POST',
                url: 'ScheduleEdit.aspx/GetListRecord',
                data: JSON.stringify({
                    'Key': $("[id$=HID_ScheduleKey]").val(),
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (msg) {
                    if (msg.d.length > 0) {
                        $('#tbldata > tbody').empty();
                        for (var i = 0; i < msg.d.length; i++) {
                            $('#tbldata > tbody:last').append('<tr class="" id="' + msg.d[i].ID + '" style="cursor:pointer">'
                                    + '<td>' + $('#tbldata > tbody tr').length + '</td>'
                                    + '<td>' + msg.d[i].WorkTime + '</td>'
                                    + '<td>' + msg.d[i].WorkContent + '</td></tr>');
                        }
                    } else {
                        Page.showPopupMessage("Lỗi liên hệ Admin", "Lỗi lấy dữ liệu kế hoạch.")
                    }
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
