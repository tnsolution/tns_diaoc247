﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleList.aspx.cs" Inherits="WebApp.SAL.ScheduleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/fullcalendar.min.css" />
    <link rel="stylesheet" href="/template/jquery.timepicker.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" />
    <style>
        .fc-content {
            font-size: 14px !important;
            white-space: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="page-content">
        <div class="page-header">
            <h1>Kế hoạch
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Chọn phòng--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>  
    <div class="modal fade" id="mView" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" id="title">Kế hoạch</h4>
                </div>
                <div class="modal-body">
                    <div id="khoa">
                        <div class="form-group">
                            <label class="control-label">Thời gian </label>
                            <div class="input-group bootstrap-timepicker" id="datepairExample2">
                                <input type="text" id="txt_AddFrom" class="form-control col-sm-6 time start" placeholder="Chọn giờ phút" required="" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                                <input type="text" id="txt_AddTo" class="form-control col-sm-6 time end" placeholder="Chọn giờ phút" required="" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                            <div class="input-group">
                                <input name="txt_Content" type="text" id="txt_AddContent" class="form-control" placeholder="..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button" id="addRecord">
                                        <i class="ace-icon fa fa-plus bigger-110"></i>
                                        Thêm !
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="noidung">
                        <table id="tbldata" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th>...</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSave">OK</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="childId" value="0" />
    <input type="hidden" id="eventId" value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="/template/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/template/datepair.js"></script>
    <script type="text/javascript" src="/template/jquery.datepair.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            $("#tbldata tbody tr").on("click", "td:not(:last-child)", function () {
                var tmp = $(this).closest('tr').find("td:eq(1)").text();
                var id = $(this).closest('tr').attr("id");
                var td1 = tmp.split("-")[0];
                var td2 = tmp.split("-")[1];
                var td3 = $(this).closest('tr').find("td:eq(2)").text();

                $("#txt_AddFrom").val(td1);
                $("#txt_AddTo").val(td2);
                $("#txt_AddContent").val(td3);
                $("#childId").val(id);
                console.log(id);
            });
            $("#tbldata").on("click", "a[btn='del']", function (e) {
                var id = $(this).closest('tr').attr("id");
                if (confirm('Bạn có chắc xóa !.')) {
                    $.ajax({
                        type: "POST",
                        url: "/SAL/ScheduleList.aspx/Delete",
                        data: JSON.stringify({
                            'childKey': id,                         
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d == 'OK') {
                                $("#" + id).remove();                               
                            } else {
                                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d);
                            }
                        },
                        complete: function () {
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            })
        });

        var Employee = getEmployeeKey();
        var Department = getDepartmentKey();
        jQuery(function ($) {
            var unit = getUnitLevel();
            if (unit <= 1) {
                $(".modal.aside").ace_aside();
            }

            $(".select2").select2({ width: "100%" });
            $('#txt_AddFrom').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });
            $('#txt_AddTo').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });
            $('#txt_UpdateFrom').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });
            $('#txt_UpdateTo').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });

            $('#datepairExample1').datepair({ 'defaultTimeDelta': 1800000 });
            $('#datepairExample2').datepair({ 'defaultTimeDelta': 1800000 });

            $("#btnSearch").click(function () {
                Employee = $('[id$=DDL_Employee]').val();
                Department = $('[id$=DDL_Department]').val();
                if (Employee == null) {
                    alert("Bạn phải chọn nhân viên");
                }
                if (Department == null) {
                    alert("Bạn phải chọn phòng");
                }

                $('#calendar').fullCalendar('destroy');
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''//month,agendaWeek,agendaDay
                    },
                    defaultView: 'month',
                    eventClick: updateEvent,
                    selectable: true,
                    selectHelper: true,
                    select: selectDate,
                    editable: true,
                    events: '/JsonResponse_Schedule.ashx?Employee=' + Employee + '&Department=' + Department,               
                    eventRender: function (event, element) {
                        //alert(event.title);
                        element.qtip({
                            content: {
                                text: qTipText(event.start, event.end, event.description),
                                title: '<strong>' + event.title + '</strong>'
                            },
                            position: {
                                my: 'bottom left',
                                at: 'top right'
                            },
                            style: { classes: 'qtip-shadow qtip-rounded' }
                        });
                    }
                });
            });
            $('[id$=DDL_Department]').change(function () {
                var Key = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        DepartmentKey: $(this).val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee]");
                        District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                        $.each(msg.d, function () {
                            District.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        jQuery(function ($) {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: ''//month,agendaWeek,agendaDay
                },
                defaultView: 'month',
                eventClick: updateEvent,
                selectable: true,
                selectHelper: true,
                select: selectDate,
                editable: true,
                events: '/JsonResponse_Schedule.ashx?Employee=' + Employee + '&Department=' + Department,
                eventRender: function (event, element) {
                    //alert(event.title);
                    element.qtip({
                        content: {
                            text: qTipText(event.start, event.end, event.description),
                            title: '<strong>' + event.title + '</strong>'
                        },
                        position: {
                            my: 'bottom left',
                            at: 'top right'
                        },
                        style: { classes: 'qtip-shadow qtip-rounded' }
                    });
                }
            });
            $("#btnSave").click(function () {
                $('.se-pre-con').fadeIn('slow');
                location.reload();
            });
            $("#addRecord").click(function () {
                var start = '', end = '', allday = true;
                if (addStartDate != null)
                    start = addStartDate.toJSON();
                if (addEndDate != null)
                    end = addEndDate.toJSON();
                if (start != '' && end != '')
                    allday = isAllDay(addStartDate, addEndDate);

                $.ajax({
                    type: 'POST',
                    url: '/SAL/ScheduleList.aspx/SaveRecord',
                    data: JSON.stringify({
                        TimeKey: $("#eventId").val(),
                        childKey: $("#childId").val(),
                        FromTime: $("#txt_AddFrom").val(),
                        ToTime: $("#txt_AddTo").val(),
                        Noidung: $("#txt_AddContent").val(),
                        start: start,
                        end: end,
                        allDay: allday
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        $("#tbldata tbody").empty();
                        $("#tbldata tbody").append($(r.d.Result));
                        $("#eventId").val(r.d.Result2);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Lỗi: " + xhr.responseText);
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    }
                });
                $("#childId").val(0)
            });
        });

        var currentUpdateEvent;
        var addStartDate;
        var addEndDate;
        var globalAllDay;

        function selectDate(start, end, allDay) {
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;

            $("#eventId").val(0);
            $.ajax({
                type: 'POST',
                url: '/SAL/ScheduleList.aspx/GetRecord',
                data: JSON.stringify({
                    ReportKey: 0,
                    ReportDate: start,
                    EmployeeKey: Employee
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (r) {
                    $("#eventStartDate").val(addStartDate.toJSON());
                    $("#eventEndDate").val(addEndDate.toJSON());
                    $("#tbldata tbody").empty();
                    $("#tbldata tbody").append($(r.d.Message));
                    $("#eventId").val(r.d.Result5);
                    $(".se-pre-con").fadeOut("slow");
                    $("#title").text("Kế hoạch " + r.d.Result3);
                    if (r.d.Result == 0) {
                        $("#khoa").hide();
                        $("#btnSave").hide();
                        $('#tbldata tbody tr td:nth-child(4)').hide();
                        $('#tbldata tbody tr th:nth-child(4)').hide();
                    }
                    else {
                        $("#khoa").show();
                        $("#btnSave").show();
                        $('#tbldata tbody tr td:nth-child(4)').show();
                        $('#tbldata tbody tr th:nth-child(4)').show();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Lỗi: " + xhr.responseText);
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function () {
                }
            });
            $('#mView').modal('show');
            console.log('new[' + $("#eventId").val() + ']' + addStartDate);
        }
        function updateEvent(event, element) {
            if ($(this).data("qtip")) $(this).qtip("destroy");
            currentUpdateEvent = event;
            $("#eventId").val(event.id);
            $.ajax({
                type: 'POST',
                url: '/SAL/ScheduleList.aspx/GetRecord',
                data: JSON.stringify({
                    ReportKey: event.id,
                    ReportDate: '',
                    EmployeeKey: Employee
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (r) {
                    $("#tbldata tbody").empty();
                    $("#tbldata tbody").append($(r.d.Message));
                    $(".se-pre-con").fadeOut("slow");
                    $("#title").text("Kế hoạch " + r.d.Result3);
                    if (r.d.Result == 0) {
                        $("#khoa").hide();
                        $("#btnSave").hide();
                        $('#tbldata tbody tr td:nth-child(4)').hide();
                        $('#tbldata tbody tr th:nth-child(4)').hide();
                    }
                    else {
                        $("#khoa").show();
                        $("#btnSave").show();
                        $('#tbldata tbody tr td:nth-child(4)').show();
                        $('#tbldata tbody tr th:nth-child(4)').show();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Lỗi: " + xhr.responseText);
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function () {
                }
            });
            $('#mView').modal('show');
            console.log('xem[' + event.id + ']' + addStartDate);
            return false;
        }

        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }
        function qTipText(start, end, description) {
            var text;

            //if (end !== null)
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") + "<br/><br/>" + description;
            //else
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><br/>" + description;

            return description;
        }
        //----------------
        function getEmployeeKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[2].split('=');
                    return cookies[1];
                }
            }
        }
        function getDepartmentKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[1].split('=');
                    return cookies[1];
                }
            }
        }
    </script>
</asp:Content>
