﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ScheduleList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
            }
        }

        [WebMethod]
        public static ItemResult GetRecord(int ReportKey, string ReportDate, int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            StringBuilder zSb = new StringBuilder();

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string Name = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            DateTime zDate = DateTime.Now;
            Schedule_Info zInfo = new Schedule_Info();
            if (ReportKey != 0)
            {
                zInfo = new Schedule_Info(ReportKey);
                Name = zInfo.EmployeeName;
            }
            else
            {
                zDate = Convert.ToDateTime(ReportDate);
                zInfo = new Schedule_Info(Employee, zDate);
                Name = zInfo.EmployeeName;
            }

            if (zInfo.Key > 0)
            {
                //get data đã có
                int i = 1;
                DataTable zChild = ScheduleDetail_Data.GetSchedule(zInfo.Key);
                foreach (DataRow r in zChild.Rows)
                    zSb.AppendLine("<tr id=" + r["ID"].ToString() + "><td>" + i + "</td><td>" + r["Title"].ToString() + "</td><td>" + r["Description"].ToString() + "</td><td><a btn=del class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130'></i></a></td></tr>");

                zResult.Result2 = zInfo.ScheduleDate.ToString("dd/MM/yyyy");
            }
            else
            {
                zResult.Result2 = zDate.ToString("dd/MM/yyyy");
            }
            zSb.ToString();

            //return paramater

            if (zDate < DateTime.Now)
            {
                zResult.Result = "0";
            }
            else
            {
                zResult.Result = "1";
            }

            zResult.Result3 = Name;
            zResult.Result5 = zInfo.Key.ToString();          
            zResult.Message = zSb.ToString();

            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveRecord(int TimeKey, int childKey, string FromTime, string ToTime, string Noidung, string start, string end, bool allDay)
        {
            if (TimeKey == 0)
            {
                Schedule_Info zParent = new Schedule_Info();
                zParent.Start = start;
                zParent.End = end;
                zParent.AllDay = allDay;
                zParent.Title = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zParent.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zParent.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
                zParent.ScheduleDate = Convert.ToDateTime(start);
                zParent.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zParent.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zParent.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zParent.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zParent.Create();

                TimeKey = zParent.Key;
            }

            ScheduleDetail_Info zInfo = new ScheduleDetail_Info(childKey);
            zInfo.TimeKey = TimeKey;
            zInfo.Title = FromTime + "-" + ToTime;
            zInfo.Description = Noidung;
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            StringBuilder zSb = new StringBuilder();
            int i = 1;
            DataTable zChild = ScheduleDetail_Data.GetSchedule(TimeKey);
            foreach (DataRow r in zChild.Rows)
                zSb.AppendLine("<tr id=" + r["ID"].ToString() + "><td>" + i + "</td><td>" + r["Title"].ToString() + "</td><td>" + r["Description"].ToString() + "</td><td><a btn=del class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130'></i></a></td></tr>");

            ItemResult zre = new ItemResult();
            zre.Result = zSb.ToString();
            zre.Result2 = TimeKey.ToString();
            return zre;
        }

        [WebMethod]
        public static string Delete(int childKey)
        {
            ScheduleDetail_Info zInfo = new ScheduleDetail_Info();
            zInfo.ID = childKey;
            zInfo.Delete();
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return zInfo.Message;
        }

        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY DepartmentName", false);
                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}