﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Transfer.aspx.cs" Inherits="WebApp.INFO.Transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        .form-group {
            margin-bottom: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Giao nhận sản phẩm...
                <span class="pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <asp:Literal ID="Lit_Message" runat="server"></asp:Literal>
                <table class='table table-hover table-bordered' id='tblData'>
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Dự án</th>
                            <th>Mã sản phẩm</th>
                            <th>Loại sản phẩm</th>
                            <th>Tình trạng</th>
                            <th>Quản lý</th>
                            <th>Tình trạng thông tin</th>
                            <th>Lần cập nhật gần nhất</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mSearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Tìm kiếm, xử lý thông tin công việc</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Người gửi--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Employee2" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Người nhận--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Project" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Status" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="-1" Text="--Tình trạng--" disabled="disabled" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Đang chờ xác nhận"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đã xác nhận"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Chưa chuyển giao"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txt_Name" runat="server" CssClass="form-control" Text="" placeholder="-- Mã sản phẩm--"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSearch" runat="server" onserverclick="btnSearch_ServerClick">
                        <i class="ace-icon fa fa-search-plus"></i>
                        Tìm
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mSend" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Kiểm tra gửi thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-info">
                                <asp:Literal ID="Lit_mTitle" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Nhập số thông tin cần gửi</label>
                                <asp:TextBox ID="txt_InfoNo" runat="server" CssClass="form-control" Text="" placeholder="..." required></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Nhân viên cần gửi đến</label>
                                <asp:DropDownList ID="DDL_ToEmployee" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%" required>
                                    <asp:ListItem Value="0" Text="--Tất cả dự án--" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Ghi chú</label>
                                <asp:TextBox ID="txt_Description" runat="server" CssClass="form-control" Text="" placeholder="..."></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSend" runat="server" onserverclick="btnSend_ServerClick">
                        <i class="ace-icon fa fa-send-o"></i>
                        Gửi
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $(".select2").select2({ width: "100%" });
        });
    </script>
</asp:Content>
