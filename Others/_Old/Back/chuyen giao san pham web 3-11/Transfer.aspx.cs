﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;

namespace WebApp.INFO
{
    public partial class Transfer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_ToEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT A.ProjectKey, A.ProjectName FROM PUL_Project A", false);

                CheckRoles();
                //LoadData();
            }
        }
        void LoadData()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int No = 1;
            StringBuilder zSb = new StringBuilder();
            DataTable zList = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zList = Product_Data.ManagerProduct_Sent(0, 0);
                    break;

                case 2:
                    zList = Product_Data.ManagerProduct_Sent(DepartmentKey, 0);
                    break;

                default:
                    zList = Product_Data.ManagerProduct_Sent(DepartmentKey, EmployeeKey);
                    break;
            }
            foreach (DataRow r in zList.Rows)
            {
                string TinhTrang = "";
                int XacNhan = r["Confirmed"] == DBNull.Value ? -1 : r["Confirmed"].ToInt();
                switch (XacNhan)
                {
                    case 0:
                        TinhTrang = "Chờ xác nhận: " + r["NewStaffName"].ToString();
                        break;

                    case 1:
                        TinhTrang = "Đã xác nhận: " + r["ConfirmDate"].ToDateTimeString() + " " + r["ConfirmByName"].ToString();
                        break;
                }

                zSb.AppendLine("            <tr id='" + r["AssetKey"].ToString() + "' fid='" + r["AssetType"].ToString() + "'>");
                zSb.AppendLine("                <td>" + No++ + "</td>");
                zSb.AppendLine("                <td>" + r["ProjectName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["CategoryName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["Status"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + TinhTrang + "</td>");
                zSb.AppendLine("                <td>" + r["NumberOfDay"].ToString() + " ngày</td>");
                zSb.AppendLine("            </tr>");
            }

            if (DDL_Project.SelectedValue.ToInt() != 0)
                Lit_mTitle.Text = "Thông tin Dự án " + DDL_Project.SelectedItem.Text + " của " + DDL_Employee1.SelectedItem.Text + " có " + zList.Rows.Count + " thông tin <br/>";
            else
                Lit_mTitle.Text = "Tất cả có " + zList.Rows.Count + " thông tin <br/>";

            Literal_Table.Text = zSb.ToString();
        }
        void SearchData()
        {
            int Employee1 = DDL_Employee1.SelectedValue.ToInt();
            int Employee2 = DDL_Employee2.SelectedValue.ToInt();
            int Project = DDL_Project.SelectedValue.ToInt();
            int Status = DDL_Status.SelectedValue.ToInt();
            string Name = txt_Name.Text.Trim();
            int No = 1;
            StringBuilder zSb = new StringBuilder();
            DataTable zList = Product_Data.ManagerProduct(Project, Employee1, Employee2, Name, 0);

            foreach (DataRow r in zList.Rows)
            {
                string TinhTrang = "";
                int XacNhan = r["Confirmed"] == DBNull.Value ? -1 : r["Confirmed"].ToInt();
                switch (XacNhan)
                {
                    case 0:
                        TinhTrang = "Chờ xác nhận: " + r["NewStaffName"].ToString();
                        break;

                    case 1:
                        TinhTrang = "Đã xác nhận: " + r["ConfirmDate"].ToDateTimeString() + " " + r["ConfirmByName"].ToString();
                        break;
                }

                zSb.AppendLine("            <tr id='" + r["AssetKey"].ToString() + "' fid='" + r["AssetType"].ToString() + "'>");
                zSb.AppendLine("                <td>" + No++ + "</td>");
                zSb.AppendLine("                <td>" + r["ProjectName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["CategoryName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["Status"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + TinhTrang + "</td>");
                zSb.AppendLine("                <td>" + r["NumberOfDay"].ToString() + " ngày</td>");
                zSb.AppendLine("            </tr>");
            }

            if (DDL_Project.SelectedValue.ToInt() != 0)
                Lit_mTitle.Text = "Thông tin Dự án " + DDL_Project.SelectedItem.Text + " của " + DDL_Employee1.SelectedItem.Text + " có " + zList.Rows.Count + " thông tin <br/>";
            else
                Lit_mTitle.Text = "Tất cả có " + zList.Rows.Count + " thông tin <br/>";

            Literal_Table.Text = zSb.ToString();

            //ItemSearch_Info zISearch = new ItemSearch_Info();
            //zISearch.Employee = Employee;
            //zISearch.ProjectKey = ProjectKey.ToString();
            //Session["SearchInfo"] = zISearch;
        }
        void SendData()
        {
            Lit_Message.Text = "";
            int Employee1 = DDL_Employee1.SelectedValue.ToInt();
            int Employee2 = DDL_Employee2.SelectedValue.ToInt();
            int Project = DDL_Project.SelectedValue.ToInt();
            int Status = DDL_Status.SelectedValue.ToInt();
            int No = txt_InfoNo.Text.ToInt();
            string Name = txt_Name.Text.Trim();

            DataTable zList = Product_Data.ManagerProduct(Project, Employee1, 0, Name, No);

            string Insert = "";
            string Update = "";
            ItemResult zIResult = new ItemResult();

            foreach (DataRow r in zList.Rows)
            {
                //Update += " Update PUL_Resale_Apartment SET EmployeeKey = " + DDL_ToEmployee.SelectedValue + ", DepartmentKey = dbo.FNC_GetDepartment(" + DDL_ToEmployee.SelectedValue + ") WHERE AssetKey = " + r.AssetKey;
                Insert += @"
INSERT INTO SYS_LogTranfer 
(OldStaff, NewStaff, ObjectTable, ObjectID, Note, Confirmed, CreatedDate, Project, AssetName, AssetCategory) VALUES 
(" + r["EmployeeKey"].ToInt() + "," + DDL_ToEmployee.SelectedValue + ",N'" + r["AssetType"].ToString() + "'," + r["AssetKey"].ToInt() + ",N'Chuyen Tiep',0,GETDATE(), " + Project + ", N'" + r["AssetID"].ToString().Trim() + "',N'" + r["CategoryName"].ToString() + "')";
            }

            zIResult = CustomInsert.Exe(Insert);
            if (zIResult.Message != string.Empty)
            {
                Lit_Message.Text = "Lỗi gửi chi tiết thông tin !" + zIResult.Message;
                return;
            }
            else
            {
                Lit_Message.Text = "Thông tin đã gửi cho [" + DDL_ToEmployee.SelectedItem.Text + "] thành công, chờ người nhận xác nhận !.";
            }
            zIResult = CustomInsert.Exe(Update);
            if (zIResult.Message != string.Empty)
            {
                Lit_Message.Text = "Lỗi gửi chi tiết thông tin !'" + zIResult.Message;
            }
        }
        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            SearchData();
        }
        protected void btnSend_ServerClick(object sender, EventArgs e)
        {
            SendData();
        }
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            StringBuilder zSb = new StringBuilder();
            if (UnitLevel <= 1)
            {
                zSb.AppendLine("<a href='#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mSearch'><i class='ace-icon fa fa-search icon-only bigger-110'></i>&nbsp;Tìm lọc</a>");
                zSb.AppendLine("<a href='#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mSend'><i class='ace-icon fa fa-send'></i>&nbsp;Gửi</a>");
            }

            zSb.AppendLine("<a href='#' class='btn btn-primary btn-white' data-toggle='modal' id='confirm' ><i class='ace-icon fa fa-check'></i>&nbsp;Xác nhận</a>");
            Lit_Button.Text = zSb.ToString();
        }
    }
}