﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeEdit.aspx.cs" Inherits="WebApp.FNC.TradeEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <style>
        sup {
            vertical-align: sup;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Cập nhật giao dịch
                <asp:Literal ID="txt_Name" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a type="button" class="btn btn-white btn-default btn-bold" href="TradeList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin giao dịch
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#file">
                                <i class="blue ace-icon fa fa-folder bigger-110"></i>
                                Tập tin văn bản
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#picture">
                                <i class="blue ace-icon fa fa-image bigger-110"></i>
                                Hình ảnh
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane active">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Người lập</label>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label for="" class="col-sm-2 control-label">Ngày ký hợp đồng</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                id="txt_DateSign" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                    <label for="" class="col-sm-2 control-label" type="divExpire">Ngày hết hợp đồng <sup>*</sup></label>
                                    <div class="col-sm-2" type="divExpire">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                id="txt_Expired" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Loại giao dịch</label>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="DDL_Category" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label for="" class="col-sm-2 control-label">Ngày cọc</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                id="txt_DatePreOrder" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="space"></div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        Thông tin sản phẩm giao dịch
                                    </h4>
                                    <div class="widget-toolbar action-buttons">
                                        <a href="#mAssetFilter" class="pink" data-toggle="modal">
                                            <i class="ace-icon fa fa-search bigger-125"></i>
                                            Chọn sản phẩm
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <asp:Literal ID="Lit_TableProduct" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                            <div class="space"></div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-rss orange"></i>
                                        Thông tin chủ nhà, khách.
                                    </h4>
                                    <div class="widget-toolbar action-buttons">
                                        <a id="btnAddGuest" class="pink" href="#">
                                            <i class="ace-icon fa fa-plus bigger-125"></i>
                                            Thêm khách mới
                                        </a>
                                        <a href="#mGuestFilter" class="pink" data-toggle="modal">
                                            <i class="ace-icon fa fa-search bigger-125"></i>
                                            Chọn khách
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <asp:Literal ID="Lit_TableCustomer" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                            <div class="space"></div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-dollar orange"></i>
                                        Chi phí.                                               
                                    </h4>
                                    <div class="widget-toolbar no-border">
                                        <input name="switch-field-1" class="ace ace-switch" type="checkbox" runat="server" id="chkDevined" />
                                        <span class="lbl">&nbsp Chia/1.1</span>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Giá có VAT</label>
                                                <div class="col-sm-2">
                                                    <input type="text" runat="server" class="form-control" id="txt_AmountVAT" placeholder="Nhập số" role="numeric" data='money' calcmoney="yes" />
                                                </div>
                                                <label class="col-sm-2 control-label">Giá chưa VAT</label>
                                                <div class="col-sm-2">
                                                    <input type="text" runat="server" class="form-control" id="txt_Amount" placeholder="Tự động" role="numeric" data='money' readonly="true" />
                                                </div>
                                                <label class="col-sm-2 control-label">Phí giảm trừ</label>
                                                <div class="col-sm-2">
                                                    <input type="text" runat="server" class="form-control" id="txt_OtherFee" placeholder="Nhập số" role="numeric" data='money' calcmoney="yes" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">VAT %</label>
                                                <div class="col-sm-2">
                                                    <input type="text" runat="server" class="form-control" id="txt_VAT" placeholder="% VAT" role="numeric" calcmoney="yes" data='money' maxlength="2" />
                                                </div>
                                                <label class="col-sm-2 control-label">Phí hoa đồng</label>
                                                <div class="col-sm-2">
                                                    <input type="text" runat="server" class="form-control" id="txt_Commision" placeholder="Nhập số" role="numeric" calcmoney="yes" data='money' />
                                                </div>
                                                <label class="col-sm-2 control-label">Doanh thu</label>
                                                <div class="col-sm-2">
                                                    <input type="text" runat="server" class="form-control" id="txt_Income" placeholder="Tự động" role="numeric" style="border-color: red" data='money' readonly="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="space"></div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Ghi chú
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <textarea class="form-control limited" id="txt_Description" maxlength="500" style="margin-top: 0px; margin-bottom: 0px; height: 60px;" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="file" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-danger list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Các tập tin của dự án, khi tải tập tin vui lòng chọn mục tập tin (giới thiệu, pháp lý, ...) cần tải lên, chỉ cho phép các tập tin văn bản</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <input type="file" id="id-input-file-1" name="id-input-file-1" multiple="" />
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label class="">Diễn giải tập tin</label>
                                        <asp:TextBox ID="txt_Description1" runat="server" CssClass="form-control" placeholder="Diễn giải tập tin, ..."></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadFile">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Upload
                                    </a>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="accordion" class="accordion-style1 panel-group">
                                        <asp:Literal ID="Lit_Folder" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="picture" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Các tập tin của dự án, khi tải tập tin vui lòng chọn mục tập tin (giới thiệu, pháp lý, ...) cần tải lên, chỉ cho phép các tập tin văn bản</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-2">
                                    <input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <label class="">Diễn giải tập tin</label>
                                        <asp:TextBox ID="txt_Description2" runat="server" CssClass="form-control" placeholder="Diễn giải hình ảnh, ..."></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Upload
                                    </a>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="accordion2" class="accordion-style1 panel-group">
                                        <asp:Literal ID="Lit_ListPicture" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search -->
    <div class="modal fade" id="mAssetFilter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn nguồn sản phẩm</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-10">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <asp:DropDownList ID="DDL_AssetProject" class="select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:DropDownList ID="DDL_AssetCategory" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" id="txt_AssetID" placeholder="Mã" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <button id="btnAssetSearch" type="button" class="btn btn-sm btn-purple btn-block pull-right">Tìm</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12" id="table">
                            <asp:Literal ID="Lit_Asset" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnAssetSelect" data-dismiss="modal">
                        Chọn</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mGuestFilter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn nguồn khách hàng</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txt_Search_CustomerID" placeholder="Tìm SĐT" />
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txt_Search_CustomerName" placeholder="Tìm tên khách hàng" />
                            </div>
                        </div>
                        <div class="col-md-2 pull-right">
                            <button id="btnGuestSearch" type="button" class="btn btn-block btn-info">Tìm</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Literal ID="Lit_Guest" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <!---guest--->
    <div class="modal fade" id="mOwner">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin khách/ chủ nhà</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Họ và tên<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày sinh<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_Birthday" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">CMND<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_CardID" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày cấp<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_CardDate" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nơi cấp<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_CardPlace" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT1<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Phone1" placeholder="Nhập text" role="phone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT2</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" role="phone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ thường trú(*)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Address1" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ liên hệ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="ace" value="1" name="chkPurpose" id="chkIsOwner1" />
                                                <span class="lbl">Chủ nhà</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="radio" class="ace" value="2" name="chkPurpose" id="chkIsOwner2" />
                                                <span class="lbl">Khách thuê</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="ace" value="3" name="chkPurpose" id="chkIsOwner3" />
                                                <span class="lbl">Khách mua</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveCustomer">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <!---guest list --->
    <div class="modal fade" id="mOwnerList">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn chủ nhà</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 id="note"></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered" id="tblOwnerList">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tên khách hàng</th>
                                        <th>SĐT</th>
                                        <th>Ngày cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3">Chưa có data</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSelectCustomer" data-dismiss="modal">
                        Chọn</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_OwnerKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TradeKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerEdit" runat="server" Value="0" />
    <!--kiem tra tình trang xử lý thao tac trên form 1 sửa,2 thêm mới -->
    <asp:Button ID="btnUploadFile" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadFile_Click" />
    <asp:Button ID="btnUploadImg" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadImg_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/FNC/TradeEdit.js"></script>
</asp:Content>
