﻿using Lib.CRM;
using Lib.FNC;
using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TradeEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_TradeKey.Value = Request["ID"];

                Tools.DropDown_DDL(DDL_Category, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 29", false);
                Tools.DropDown_DDL(DDL_AssetProject, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                CheckRole();
                LoadData();
                LoadCustomer();
                LoadFolder();
                LoadPicture();
                InitSearchAsset();
                InitSearchGuest();
            }
        }
        void LoadData()
        {
            Trade_Info zInfo = new Trade_Info(HID_TradeKey.Value.ToInt());
            DDL_Category.SelectedValue = zInfo.TradeCategoryKey.ToString();
            DDL_Employee.SelectedValue = zInfo.EmployeeKey.ToString();

            if (zInfo.IsDevined == 1)
                chkDevined.Checked = true;
            else
                chkDevined.Checked = false;

            txt_DatePreOrder.Value = zInfo.DateDeposit.ToString("dd/MM/yyyy");
            txt_DateSign.Value = zInfo.DateContract.ToString("dd/MM/yyyy");
            txt_Expired.Value = zInfo.DateContractEnd.ToString("dd/MM/yyyy");
            txt_AmountVAT.Value = zInfo.AmountVAT.ToDoubleString();
            txt_Amount.Value = zInfo.AmountVAT.ToDoubleString();
            txt_VAT.Value = zInfo.VAT.ToString();
            txt_OtherFee.Value = zInfo.OtherFee.ToDoubleString();
            txt_Commision.Value = zInfo.TransactionFee.ToDoubleString();
            txt_Income.Value = zInfo.Income.ToDoubleString();
            txt_Description.Value = zInfo.Description;

            string Asset = "";
            if (zInfo.AssetKey != 0)
            {
                Asset = @"  <tr id=" + zInfo.AssetKey + " type=" + zInfo.AssetType + @">
                                                <td>" + 1 + @"</td>
                                                <td>" + zInfo.ProjectName + @"</td>
                                                <td>" + zInfo.AssetID + @"</td>
                                                <td>" + zInfo.AssetCategoryName + @"</td>
                                                <td>" + zInfo.Area + @"</td>
                                                <td>" + zInfo.AddressProject + @"</td>
                                                <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDelAsset' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>
                                            </tr>";
            }

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine(@"<table class='table' id='tblAsset'>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Dự án</th>
                                                <th>Mã sản phẩm</th>
                                                <th>Loại sản phẩm</th>
                                                <th>Diện tích m<sup>2</sup></th>
                                                <th>Địa chỉ</th>
                                                <th>...</th>
                                            </tr>
                                        </thead>
                                        <tbody>" + Asset + "</tbody></table>");

            Lit_TableProduct.Text = zSb.ToString();
            HID_TradeKey.Value = zInfo.Key.ToString();
            HID_AssetKey.Value = zInfo.AssetKey.ToString();
            HID_AssetType.Value = zInfo.AssetType;
        }
        void LoadCustomer()
        {
            DataTable zTable = Trade_Data.GetCustomer(HID_TradeKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table' id='tblCustomer'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th>#</th>");
            zSb.AppendLine("            <th>Họ tên</th>");
            zSb.AppendLine("            <th>SĐT</th>");
            zSb.AppendLine("            <th>Ngày sinh</th>");
            zSb.AppendLine("            <th>CMND</th>");
            zSb.AppendLine("            <th>Địa chỉ</th>");
            zSb.AppendLine("            <th>...</th>");
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int o = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("        <tr id='" + r["CustomerKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (o++) + "</td>");
                    zSb.AppendLine("            <td>" + r["CustomerName"].ToString() + "<br/>" + r["Owner"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Phone1"].ToString() + " <br/>" + r["Phone2"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Birthday"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>CMND: " + r["CardID"].ToString() + "<br/>Nơi cấp: " + r["CardPlace"].ToString() + " </td>");
                    zSb.AppendLine("            <td>Liên hệ:" + r["Address1"].ToString() + "<br/>Thường trú: " + r["Address2"].ToString() + "</td>");
                    zSb.AppendLine("            <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDelCustomer' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("        </tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("              <td>#</td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_TableCustomer.Text = zSb.ToString();
        }
        void LoadPicture()
        {
            List<ItemDocument> zList = Document_Data.List(130, HID_TradeKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<ul class='ace-thumbnails clearfix'>");
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<li id='" + item.FileKey + "'>");
                zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageUrl + "' />");
                zSb.AppendLine("            <div class='text'>");
                zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                zSb.AppendLine("            </div>");
                zSb.AppendLine("    </a>");
                zSb.AppendLine("    <div class='tools tools-bottom'><a href='#' btn='btnDeleteImg'><i class='ace-icon fa fa-times red'></i>Xóa</a></div>");
                zSb.AppendLine("</li>");
            }
            zSb.AppendLine(" </ul>");
            Lit_ListPicture.Text = zSb.ToString();
        }
        void LoadFolder()
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemDocument> zList = Document_Data.List(135, HID_TradeKey.Value.ToInt(), "Transaction");
            zSb.AppendLine("<table class='table table-hover' id='tblFile'>");
            zSb.AppendLine("  <thead>");
            zSb.AppendLine("     <tr>");
            zSb.AppendLine("         <th>STT</th>");
            zSb.AppendLine("         <th>Tập tin</th>");
            zSb.AppendLine("         <th>Diễn giải</th>");
            zSb.AppendLine("         <th>Ngày cập nhật</th>");
            zSb.AppendLine("         <th>Người cập nhật</th>");
            zSb.AppendLine("         <th>...</th>");
            zSb.AppendLine("     </tr>");
            zSb.AppendLine(" </thead>");
            zSb.AppendLine(" <tbody>");
            int i = 1;
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td>" + item.Description + "</td><td>" + item.ModifiedDate + "</td><td>" + item.ModifiedName + "</td><td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteFile' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td></tr>");
            }
            zSb.AppendLine(" </tbody>");
            zSb.AppendLine(" </table>");

            Lit_Folder.Text = zSb.ToString();
        }
        //-----
        void InitSearchAsset()
        {
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            List<Product_Item> zList = new List<Product_Item>();
            zList = Product_Data.Search(DDL_AssetProject.SelectedValue.ToInt(), DDL_AssetCategory.SelectedValue.ToInt(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 1, CurrentAgent, 100);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblAssetSearch'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Mã sản phẩm</th>");
            zSb.AppendLine("        <th>Loại sản phẩm</th>");
            zSb.AppendLine("        <th>Diện tích m<sup>2</sup></th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int i = 1;
            foreach (Product_Item r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.AssetKey + "' type='" + r.AssetType + "'>");
                zSb.AppendLine("                <td>" + i++ + "</td>");
                zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                zSb.AppendLine("                <td>" + r.Area + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Asset.Text = zSb.ToString();
        }
        void InitSearchGuest()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            List<ItemCustomer> zList = Customer_Data.Get(Department, Employee);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblGuestSearch'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Tên khách hàng</th>");
            zSb.AppendLine("        <th>SĐT</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            int no = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "'>");
                zSb.AppendLine("               <td>" + (no++) + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br/>" + r.Phone2 + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Guest.Text = zSb.ToString();
        }
        [WebMethod]
        public static ItemResult DeleteTrade(int TradeKey)
        {
            Trade_Info zInfo = new Trade_Info();
            zInfo.Key = TradeKey;
            zInfo.Delete();

            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult InitTrade(int TradeCategory, string DateSign, string DateExpired, string DatePre, string Description, int Employee)
        {
            Trade_Info zInfo = new Trade_Info();
            zInfo.TradeCategoryKey = TradeCategory;
            zInfo.DateContract = Tools.ConvertToDate(DateSign);
            zInfo.DateDeposit = Tools.ConvertToDate(DatePre);
            zInfo.DateContractEnd = Tools.ConvertToDate(DateExpired);
            zInfo.Description = Description;
            zInfo.TransactionDate = DateTime.Now;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(Employee);
            zInfo.ApprovedBy = Employees_Data.GetManager(Employee);
            zInfo.EmployeeKey = Employee;

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Create();

            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveTrade(int TradeKey, int TradeCategory, string DateSign, string DateExpired, string DatePre, string Description, int Employee, string AmountVAT, string Amount, string OtherFee, string VAT, string Commision, string Income, string IsDevined)
        {
            Trade_Info zInfo = new Trade_Info(TradeKey);
            zInfo.TradeCategoryKey = TradeCategory;
            zInfo.DateContract = Tools.ConvertToDate(DateSign);
            zInfo.DateDeposit = Tools.ConvertToDate(DatePre);
            zInfo.DateContractEnd = Tools.ConvertToDate(DateExpired);
            zInfo.Description = Description;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(Employee);
            zInfo.ApprovedBy = Employees_Data.GetManager(Employee);
            zInfo.EmployeeKey = Employee;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Amount = double.Parse(Amount);
            zInfo.AmountVAT = double.Parse(AmountVAT);
            zInfo.VAT = VAT.ToInt();
            zInfo.TransactionFee = double.Parse(Commision);
            zInfo.OtherFee = double.Parse(OtherFee);
            zInfo.Income = double.Parse(Income);
            zInfo.IsDevined = IsDevined.ToInt();
            zInfo.Update();

            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        //-----
        [WebMethod]
        public static ItemAsset[] AssetSearch(int Project, int Category, string AssetID)
        {
            List<ItemAsset> zList = new List<ItemAsset>();
            zList = Product_Data.Search(Project, Category, string.Empty, string.Empty, string.Empty, string.Empty, AssetID, string.Empty, string.Empty, 0, 0);
            return zList.ToArray();
        }
        [WebMethod]
        public static ItemAsset AssetSelect(int Key, int AssetKey, string Type)
        {
            Trade_Info zTrade = new Trade_Info(Key);
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            //giao dịch cho thuê update => sp lai thanh đa cho thue, nguoc lai la đã bán.
            if (zTrade.TradeCategoryKey == 227)
                zInfo.ItemAsset.StatusKey = "30";
            else
                zInfo.ItemAsset.StatusKey = "29";
            zInfo.UpdateStatus(Type);

            zTrade.AssetType = zInfo.ItemAsset.AssetType;
            zTrade.AssetKey = zInfo.ItemAsset.AssetKey.ToInt();
            zTrade.ProjectKey = zInfo.ItemAsset.ProjectKey.ToInt();
            zTrade.Address = zInfo.ItemAsset.Address;
            float area = 0;
            float.TryParse(zInfo.ItemAsset.AreaWall, out area);
            zTrade.Area = area;
            zTrade.AssetID = zInfo.ItemAsset.AssetID;
            zTrade.AssetCategoryName = zInfo.ItemAsset.CategoryName;
            zTrade.AssetCategory = zInfo.ItemAsset.CategoryKey.ToInt();
            zTrade.Key = Key;
            zTrade.UpdateProduct();

            return zInfo.ItemAsset;
        }
        [WebMethod]
        public static ItemResult AssetDelete(int Key)
        {
            Trade_Info zTrade = new Trade_Info(Key);

            //xoa sp ra khỏi giao dịch thì update lai là chưa giao dich
            Product_Info zInfo = new Product_Info(zTrade.AssetKey, zTrade.AssetType);
            zInfo.ItemAsset.StatusKey = "31";
            zInfo.UpdateStatus(zTrade.AssetType);

            zTrade.AssetType = "";
            zTrade.AssetKey = 0;
            zTrade.ProjectKey = 0;
            zTrade.Address = "";
            zTrade.Area = 0;
            zTrade.AssetID = "";
            zTrade.AssetCategoryName = "";
            zTrade.AssetCategory = 0;
            zTrade.Key = Key;
            zTrade.UpdateProduct();
            ItemResult zResult = new ItemResult();
            zResult.Message = zTrade.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetOwner(int AssetKey, string Type)
        {
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            List<ItemCustomer> zList = zTable.DataTableToList<ItemCustomer>();

            foreach (ItemCustomer item in zList)
            {
                string temp = "";
                if (item.Phone1.Contains("/"))
                    temp = item.Phone1.Split('/')[0];
                else
                    temp = item.Phone1;
                temp = temp.Trim().Replace(" ", "");
                Customer_Info zCustomer = new Customer_Info(temp);
                if (zCustomer.Key > 0)
                {
                    item.CustomerKey = zCustomer.Key.ToString();
                    item.CustomerName = zCustomer.CustomerName;
                    item.Phone1 = zCustomer.Phone1;
                    item.Phone2 = zCustomer.Phone2;
                    item.Address1 = zCustomer.Address1;
                    item.Address2 = zCustomer.Address2;
                    item.CardDate = zCustomer.CardDate.ToDateString();
                    item.CardID = zCustomer.CardID;
                    item.CardPlace = zCustomer.CardPlace;
                    item.Birthday = zCustomer.Birthday.ToDateString();
                }
            }

            return zList.ToArray();
        }
        [WebMethod]
        public static ItemCustomer OneOwner(int OwnerKey, string Type)
        {
            Owner_Info zOwn = new Owner_Info(Type, OwnerKey);
            ItemCustomer zResult = new ItemCustomer();
            Customer_Info zCustomer = new Customer_Info(zOwn.Phone1);
            if (zCustomer.Key > 0)
            {
                zResult.CustomerKey = zCustomer.Key.ToString();
                zResult.CustomerName = zCustomer.CustomerName;
                zResult.Phone1 = zCustomer.Phone1;
                zResult.Phone2 = zCustomer.Phone2;
                zResult.Address1 = zCustomer.Address1;
                zResult.Address2 = zCustomer.Address2;
                zResult.CardDate = zCustomer.CardDate.ToDateString();
                zResult.CardID = zCustomer.CardID;
                zResult.CardPlace = zCustomer.CardPlace;
                zResult.Birthday = zCustomer.Birthday.ToDateString();
            }
            else
            {
                zResult.CustomerName = zCustomer.CustomerName;
                zResult.Phone1 = zOwn.Phone1;
                zResult.Phone2 = zOwn.Phone2;
                zResult.Address1 = zOwn.Address1;
                zResult.Address2 = zOwn.Address2;
            }

            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveGuest(int TradeKey, int OwnerKey, int CustomerKey, int AssetKey, string AssetType, string CustomerName, string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int EmployeeKey, int IsOwner)
        {
            int Status = 0;// tinh trang khách 277 đã giao dich, 279 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 277;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemResult zResult = new ItemResult();

            //luu thong tin khach
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.ID = Phone.Replace(" ", "");
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone.Replace(" ", "");
            zInfo.Phone2 = Phone2.Replace(" ", "");
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.EmployeeKey = EmployeeKey.ToInt();
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.CreatedDate = DateTime.Now;
            zInfo.ModifiedDate = DateTime.Now;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();

            //luu khach giao dich
            Trade_Customer_Info zCustomerTrade = new Trade_Customer_Info(TradeKey, zInfo.Key);
            zCustomerTrade.CustomerKey = zInfo.Key;
            zCustomerTrade.Key = TradeKey;
            zCustomerTrade.IsOwner = IsOwner;
            zCustomerTrade.Save();

            //luu tinh trang quan tam
            Consents_Info zConsent = new Consents_Info(zInfo.Key, true);
            zConsent.CategoryConsent = "277"; //đã giao dịch
            zConsent.Name_Consent = "Đã giao dịch";
            if (zConsent.CustomerKey == 0)
            {
                zConsent.CustomerKey = zInfo.Key;
                zConsent.Create();
            }
            else
            {
                zConsent.Update(zInfo.Key);
            }

            //cap nhat thong tin chu nha
            Owner_Info zOwner = new Owner_Info(AssetType, OwnerKey);
            zOwner.CustomerName = CustomerName;
            zOwner.Phone1 = Phone.Replace(" ", "");
            zOwner.Phone2 = Phone2.Replace(" ", "");
            zOwner.Address1 = Address;
            zOwner.Address2 = Address2;
            zOwner.Update(AssetType);

            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetGuest(int TradeKey)
        {
            DataTable zTable = Trade_Data.GetCustomer(TradeKey);
            List<ItemCustomer> zList = zTable.DataTableToList<ItemCustomer>();
            return zList.ToArray();
        }
        [WebMethod]
        public static ItemResult GuestDelete(int TradeKey, int CustomerKey)
        {
            ItemResult zResult = new ItemResult();
            Trade_Customer_Info zInfo = new Trade_Customer_Info();

            //trả tình trạng khách khi xóa ra khỏi giao dịch
            Consents_Info zConsent = new Consents_Info(CustomerKey, true);
            zConsent.CategoryConsent = "";
            zConsent.Name_Consent = "";
            zConsent.Update(CustomerKey);

            zInfo.Delete(CustomerKey, TradeKey);
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer GetCustomer(int TradeKey, int CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            Trade_Customer_Info zTrade = new Trade_Customer_Info(TradeKey, CustomerKey);
            ItemCustomer zResult = new ItemCustomer();

            zResult.CustomerKey = zInfo.Key.ToString();
            zResult.CustomerName = zInfo.CustomerName;
            zResult.Birthday = zInfo.Birthday.ToDateString();
            zResult.Phone1 = zInfo.Phone1;
            zResult.Phone2 = zInfo.Phone2;
            zResult.Address1 = zInfo.Address1;
            zResult.Address2 = zInfo.Address2;
            zResult.CardID = zInfo.CardID;
            zResult.CardDate = zInfo.CardDate.ToDateString();
            zResult.CardPlace = zInfo.CardPlace;
            zResult.IsOwner = zTrade.IsOwner.ToString();
            zResult.Owner = zTrade.Owner;
            return zResult;
        }

        //************ cap nhat tinh trang khách hàng khi lập giao dịch, chọn hoặc thêm mới ngang, 
        //nếu là chủ nhà => cập nhật tình trang khách là ký gửi đã giao dịch, 
        //nếu là khách mua => cập nhật tình trang khách là đã giao dịch và là chủ nhà mới của căn giao dịch
        //nếu là khách thuê => cập nhật tình trang khách là đã giao dịch 
        [WebMethod]
        public static ItemResult SaveCustomer(int CustomerKey, int AssetKey, string AssetType, string CustomerName, string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int IsOwner)
        {
            int Status = 0;// tinh trang khách 277 đã giao dich, 279 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 277;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        //*************
        [WebMethod]
        public static ItemResult SaveNewCustomer(int TradeKey, int AssetKey, string AssetType, string CustomerName, string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int EmployeeKey, int IsOwner)
        {
            int Status = 0;// tinh trang khách 227 đã giao dich, 229 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 279;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info();
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            //luu khach giao dich
            Trade_Customer_Info zCustomerTrade = new Trade_Customer_Info(TradeKey, zInfo.Key);
            zCustomerTrade.CustomerKey = zInfo.Key;
            zCustomerTrade.Key = TradeKey;
            zCustomerTrade.IsOwner = IsOwner;
            zCustomerTrade.Save();

            //luu tinh trang quan tam
            //Consents_Info zConsent = new Consents_Info(zInfo.Key, true);
            //zConsent.CategoryConsent = "277"; //đã giao dịch
            //zConsent.Name_Consent = "Đã giao dịch";
            //if (zConsent.CustomerKey == 0)
            //{
            //    zConsent.CustomerKey = zInfo.Key;
            //    zConsent.Create();
            //}
            //else
            //{
            //    zConsent.Update(zInfo.Key);
            //}

            zInfo.Save();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GuestSearch(string Phone, string Name)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
            {
                Employee = 0;
                Department = 0;
            }
            List<ItemCustomer> zList = Customer_Data.Get(string.Empty, string.Empty, string.Empty, Name, Phone, string.Empty, Department, Employee);
            return zList.ToArray();
        }
        [WebMethod]
        public static ItemResult DeleteFile(int FileKey)
        {
            ItemResult zResult = new ItemResult();
            Document_Info zInfo = new Document_Info();
            zInfo.FileKey = FileKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer CheckPhone(string Phone)
        {
            Customer_Info zInfo = new Customer_Info(Phone);
            ItemCustomer zCustomer = new ItemCustomer();
            zCustomer.CustomerName = zInfo.CustomerName;
            zCustomer.Address2 = zInfo.Address2;
            zCustomer.Address1 = zInfo.Address1;
            zCustomer.Phone1 = zInfo.Phone1;
            zCustomer.Phone2 = zInfo.Phone2;
            zCustomer.Birthday = zInfo.Birthday.ToDateString();
            zCustomer.CardID = zInfo.CardID;
            zCustomer.CardPlace = zInfo.CardPlace;
            zCustomer.CardDate = zInfo.CardDate.ToDateString();
            return zCustomer;
        }
        void UploadFile()
        {
            int zCategoryKey = 135;
            int zProductKey = HID_TradeKey.Value.ToInt();
            string zSQL = "";

            string zPath = "~/Upload/File/Transaction/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileName(zInputFile.FileName);
                    string zFilePath = Server.MapPath(Path.Combine(zPath, zFileName));
                    string zFileUrl = (zPath + zFileName).Substring(1, zPath.Length + zFileName.Length - 1);

                    if (File.Exists(zFilePath))
                        File.Delete(zFilePath);
                    zInputFile.SaveAs(zFilePath);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, Description ,ImagePath, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('Transaction','" + zProductKey + "','" + zCategoryKey + "',N'" + txt_Description1.Text + "',N'" + zFilePath + "',N'" + zFileUrl + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadFolder();
                }
            }
        }
        void UploadImg()
        {
            int zCategoryKey = 130;
            int zProductKey = HID_TradeKey.Value.ToInt();
            string zSQL = "";

            string zPath = "~/Upload/File/Transaction/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                    string zFileThumb = zFileName + "_thumb." + Path.GetExtension(zInputFile.FileName);
                    string zFileLarge = Path.GetFileName(zInputFile.FileName);

                    string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                    string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                    string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                    string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                    if (File.Exists(zFilePathLarge))
                        File.Delete(zFilePathLarge);
                    zInputFile.SaveAs(zFilePathLarge);

                    Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                    System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(150, 150));
                    objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, Description, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('Transaction','" + zProductKey + "','" + zCategoryKey + "', N'" + txt_Description2.Text + "', N'" + zFileLarge + "', N'" + zFileUrlThumb + "', N'" + zFileUrlLarge + "', N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadPicture();
                }
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            UploadFile();
        }
        protected void btnUploadImg_Click(object sender, EventArgs e)
        {
            UploadImg();
        }

        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL04";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey =" + Department + " ORDER BY LastName", false);
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + "ORDER BY LastName", false);
                    break;
            }

            if (_Permitsion[2] == "1")
            {
                Lit_Button.Text = "<a class='btn btn-white btn-info btn-bold' id='btnEdit' href='#'><i class='ace-icon fa fa-save blue'></i>Cập nhật</a>";
            }
            if (_Permitsion[3] == "1")
            {
                Lit_Button.Text += " <a class='btn btn-white btn-warning btn-bold' id='btnDel' href='#'><i class='ace-icon fa fa-trash-o bigger-120 orange'></i>Xóa</a>";
            }
        }
        #endregion
    }
}