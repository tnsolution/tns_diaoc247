﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeList.aspx.cs" Inherits="WebApp.FNC.TradeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <a href="#mInitTrade" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
                <span class="pull-right action-buttons" style="display: none">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <!---->
    <div class="modal fade modal-default" id="mInitTrade" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Thêm thông tin giao dịch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Người lập giao dịch</label>
                        <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true" required>
                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Loại giao dịch</label>
                        <select id="DDL_Category" class="form-control select2">
                            <option value="0" disabled="disabled" selected="selected">--Chọn--</option>
                            <option value="227">Cho thuê</option>
                            <option value="228">Chuyển nhượng</option>
                            <option value="230">Bán mới</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ngày ký hợp đồng</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_DateSign" placeholder="Chọn ngày tháng năm" required />
                        </div>
                    </div>
                    <div class="form-group" type="divExpire">
                        <label class="control-label">Ngày hết hợp đồng</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_Expired" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ngày đặt cọc</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_DatePreOrder" placeholder="Chọn ngày tháng năm" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea rows="7" class="form-control" id="txt_Description" placeholder="Ghi chú"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnInitTrade">Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:HiddenField ID="HID_TradeType" runat="server" Value="3" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        jQuery(function ($) {


            $(".select2").select2({ width: "100%" });
            $("#DDL_Category").on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected == 227)
                    $("[type='divExpire']").show();
                else
                    $("[type='divExpire']").hide();
            });
            $("#tblData tbody").on("click", "tr", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "/FNC/TradeView.aspx?ID=" + trid;
            });
            $("#btnInitTrade").click(function () {
                if (!validate()) {
                    return false;
                }

                var tran_employee = $("[id$=DDL_Employee]").val();
                var tran_category = $("[id$=DDL_Category]").val();
                var tran_datesign = $("[id$=txt_DateSign]").val();
                var tran_expired = $("[id$=txt_Expired]").val();
                var tran_datepre = $("[id$=txt_DatePreOrder]").val();
                var tran_description = $("[id$=txt_Description]").val();
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeEdit.aspx/InitTrade',
                    data: JSON.stringify({
                        "TradeCategory": tran_category,
                        "DateSign": tran_datesign,
                        "DateExpired": tran_expired,
                        "DatePre": tran_datepre,
                        "Description": tran_description,
                        "Employee": tran_employee
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            window.location = "/FNC/TradeEdit.aspx?ID=" + r.d.Result;
                        else
                            Page.showPopupMessage("Lỗi khởi tạo thông tin", r.d.Message);
                    },
                    error: function () {
                        alert('Lỗi khởi tạo dữ liệu !');
                        $('.se-pre-con').fadeOut('slow');
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    }
                });
            });
            $("#ViewPrevious").click(function () {
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
        });

        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
