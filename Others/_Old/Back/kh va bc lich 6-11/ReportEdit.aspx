﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ReportEdit.aspx.cs" Inherits="WebApp.SAL.ReportEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày báo cáo</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                        id="txt_Date" placeholder="" readonly="true" disabled="disabled" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nội dung tóm tắt</label>
                            <div class="col-sm-5">
                                <textarea runat="server" class="form-control" id="txt_Description" placeholder="..." rows="4"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <h4 class="header smaller lighter blue">
                    <i class="ace-icon fa fa-hashtag green"></i>Chi tiết</h4>
                <div class="row">
                    <div class="col-sm-9">
                        <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ReportKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/template/numeric-input-example.js"></script>
    <script>
        $(document).ready(function () {
            $('#tbldata').editableTableWidget().numericInputExample().find('td:nth-child(2)').focus(function () { $(this).select(); });
            $("#btnSave").click(function () {
                var total = $('#tbldata tbody tr').length;
                var row = "";
                var num = 0;
                $('#tbldata tbody tr').each(function () {
                    num++;
                    if (num < total)
                        row += "" + $(this).find('td:eq(1)').attr('Category') + "," + $(this).find('td:eq(2)').text() + ";";
                    else
                        row += "" + $(this).find('td:eq(1)').attr('Category') + "," + $(this).find('td:eq(2)').text();
                });

                $.ajax({
                    type: 'POST',
                    url: '/SAL/ReportEdit.aspx/SaveReport',
                    data: JSON.stringify({
                        'Key': $("[id$=HID_ReportKey]").val(),
                        'Date': $("[id$=txt_Date]").val(),
                        'Description': $("[id$=txt_Description]").val(),
                        'Table': row,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d.Message.length <= 0) {
                            location.href = "/SAL/ReportList.aspx";
                        }
                        else {
                            Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>
</asp:Content>
