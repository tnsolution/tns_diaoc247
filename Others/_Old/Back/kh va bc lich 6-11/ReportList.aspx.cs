﻿using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ReportList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
            }
        }

        [WebMethod]
        public static ItemResult GetRecord(int ReportKey, string ReportDate, int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            StringBuilder zSb = new StringBuilder();

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string Name = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            DateTime zDate = DateTime.Now;
            Report_Info zInfo = new Report_Info();
            if (ReportKey != 0)
            {
                zInfo = new Report_Info(ReportKey);
                Name = zInfo.EmployeeName;
            }
            else
            {
                zDate = Convert.ToDateTime(ReportDate);
                zInfo = new Report_Info(Employee, zDate);
            }

            zSb.AppendLine("<table id='tbldata' class='table table-bordered table-hover'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Chỉ tiêu</th>");
            zSb.AppendLine("        <th>Kết quả ngày</th>");
            zSb.AppendLine("        <th>Yêu cầu/ tháng</th>");
            zSb.AppendLine("        <th>Đã thực hiện/ tháng</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("        <tbody>");

            if (zInfo.Key > 0)
            {
                //get data đã có
                int i = 1;
                DataTable zChild = Report_Rec_Data.List(zInfo.Key, zInfo.ReportDate.Month, zInfo.ReportDate.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["Result"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Require"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Doing"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }

                zResult.Result2 = zInfo.ReportDate.ToString("dd/MM/yyyy");
            }
            else
            {
                //get data mới
                int i = 1;
                DataTable zChild = Plan_Rec_Data.List(EmployeeKey, zDate.Month, zDate.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>0</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Require"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Doing"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }

                zResult.Result2 = zDate.ToString("dd/MM/yyyy");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            zSb.ToString();

            // return paramater
            zResult.Result = Name;
            zResult.Result3 = zInfo.Title;
            zResult.Result4 = zInfo.Description;
            zResult.Message = zSb.ToString();

            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveReport(int Key, string StartDate, string EndDate, string Title, string Description, string Table)
        {
            ItemResult zResult = new ItemResult();
            DateTime ReportDate = new DateTime();
            Report_Info zInfo = new Report_Info();
            if (Key == 0)
            {
                ReportDate = Convert.ToDateTime(StartDate);
                if (Title == string.Empty)
                    Title = "Báo cáo ngày " + ReportDate.ToString("dd/MM/yyyy");

                zInfo.ReportDate = ReportDate;
                zInfo.Start = StartDate;
                zInfo.End = EndDate;
            }
            else
            {
                zInfo = new Report_Info(Key);
                zInfo.Title = Title;
                zInfo.Description = Description;
            }


            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            if (zInfo.Message == string.Empty)
            {
                string Message = new Report_Rec_Info().Delete(zInfo.Key);

                List<ItemWeb> zList = new List<ItemWeb>();
                string[] row = Table.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in row)
                {
                    string[] col = item.Split(',');
                    ItemWeb itemWeb = new ItemWeb();
                    itemWeb.Value = col[0];
                    itemWeb.Text = col[1];
                    zList.Add(itemWeb);
                }

                foreach (ItemWeb item in zList)
                {
                    Report_Rec_Info zRecord = new Report_Rec_Info();
                    zRecord.ReportKey = zInfo.Key;
                    zRecord.CategoryKey = item.Value.ToInt();
                    zRecord.Result = item.Text.ToInt();
                    zRecord.Create();
                }
            }

            zResult.Message = zInfo.Message;
            return zResult;
        }

        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY DepartmentName", false);
                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}