﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleList.aspx.cs" Inherits="WebApp.SAL.ScheduleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/fullcalendar.min.css" />
    <link rel="stylesheet" href="/template/jquery.timepicker.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" />
    <style>
        .fc-content {
            font-size: 14px !important;
            white-space: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="page-content">
        <div class="page-header">
            <h1>Kế hoạch
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <!--modal-->
    <div class="modal fade" id="mUpdate" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Kế hoạch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Thời gian </label>
                        <div class="input-group bootstrap-timepicker" id="datepairExample1">
                            <input type="text" id="txt_UpdateFrom" class="form-control col-sm-6 time start" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                            <input type="text" id="txt_UpdateTo" class="form-control col-sm-6 time end" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                        <div>
                            <input name="txt_Content" type="text" id="txt_UpdateContent" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger pull-right" data-dismiss="modal" id="btnDeleteRecord">Delete</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnUpdateRecord">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mAddNew" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Kế hoạch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Thời gian </label>
                        <div class="input-group bootstrap-timepicker" id="datepairExample2">
                            <input type="text" id="txt_AddFrom" class="form-control col-sm-6 time start" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                            <input type="text" id="txt_AddTo" class="form-control col-sm-6 time end" placeholder="Chọn giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                        <div>
                            <input name="txt_Content" type="text" id="txt_AddContent" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSaveRecord">OK</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="eventId" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="/template/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/template/datepair.js"></script>
    <script type="text/javascript" src="/template/jquery.datepair.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });

        var Employee = getEmployeeKey();
        var Department = getDepartmentKey();
        jQuery(function ($) {
            //console.log(Employee);
            //console.log(Department);
            $(".select2").select2({ width: "100%" });
            $(".modal.aside").ace_aside();

            $('#txt_AddFrom').timepicker({
                'minTime': '00:00am',
                'maxTime': '23:59pm',
                'showDuration': false
            });
            $('#txt_AddTo').timepicker({
                'minTime': '00:00am',
                'maxTime': '23:59pm',
                'showDuration': false
            });
            $('#txt_UpdateFrom').timepicker({
                'minTime': '00:00am',
                'maxTime': '23:59pm',
                'showDuration': false
            });
            $('#txt_UpdateTo').timepicker({
                'minTime': '00:00am',
                'maxTime': '23:59pm',
                'showDuration': false
            });

            $('#datepairExample1').datepair({ 'defaultTimeDelta': 1800000 });
            $('#datepairExample2').datepair({ 'defaultTimeDelta': 1800000 });

            $("#btnSearch").click(function () {
                Employee = $('[id$=DDL_Employee]').val();
                Department = $('[id$=DDL_Department]').val();

                $('#calendar').fullCalendar('destroy');
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''//month,agendaWeek,agendaDay
                    },
                    defaultView: 'month',
                    eventClick: updateEvent,
                    selectable: true,
                    selectHelper: true,
                    select: selectDate,
                    editable: true,
                    events: '/JsonResponse_Schedule.ashx?Employee=' + Employee + '&Department=' + Department,
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventRender: function (event, element) {
                        //alert(event.title);
                        element.qtip({
                            content: {
                                text: qTipText(event.start, event.end, event.description),
                                title: '<strong>' + event.title + '</strong>'
                            },
                            position: {
                                my: 'bottom left',
                                at: 'top right'
                            },
                            style: { classes: 'qtip-shadow qtip-rounded' }
                        });
                    }
                });
            });
        });
        jQuery(function ($) {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: ''//month,agendaWeek,agendaDay
                },
                defaultView: 'month',
                eventClick: updateEvent,
                selectable: true,
                selectHelper: true,
                select: selectDate,
                editable: true,
                events: '/JsonResponse_Schedule.ashx?Employee=' + Employee + '&Department=' + Department,
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventRender: function (event, element) {
                    //alert(event.title);
                    element.qtip({
                        content: {
                            text: qTipText(event.start, event.end, event.description),
                            title: '<strong>' + event.title + '</strong>'
                        },
                        position: {
                            my: 'bottom left',
                            at: 'top right'
                        },
                        style: { classes: 'qtip-shadow qtip-rounded' }
                    });
                }
            });
            $("#btnSaveRecord").click(function () {
                var eventToAdd = {
                    title: $("#txt_AddFrom").val() + "-" + $("#txt_AddTo").val(),
                    description: $("#txt_AddContent").val(),
                    start: addStartDate.toJSON(),
                    end: addEndDate.toJSON(),
                    employeekey: Employee,
                    departmentkey: Department,
                    allDay: isAllDay(addStartDate, addEndDate)
                };

                PageMethods.addEvent(eventToAdd, addSuccess);
            });
            $("#btnUpdateRecord").click(function () {
                var eventToAdd = {
                    id: currentUpdateEvent.id,
                    title: $("#txt_UpdateFrom").val() + "-" + $("#txt_UpdateTo").val(),
                    description: $("#txt_UpdateContent").val()
                };
                PageMethods.updateEvent(eventToAdd, addSuccess);

                currentUpdateEvent.title = $("#txt_UpdateFrom").val() + "-" + $("#txt_UpdateTo").val();
                currentUpdateEvent.description = $("#txt_UpdateContent").val();
                $('#calendar').fullCalendar('updateEvent', currentUpdateEvent);
            });
            $("#btnDeleteRecord").click(function () {
                if (confirm("do you really want to delete this event?")) {
                    var id = $("#eventId").val();
                    PageMethods.deleteEvent(id, deleteSuccess);
                    $('#calendar').fullCalendar('removeEvents', id);
                }
            });
        });

        var currentUpdateEvent;
        var addStartDate;
        var addEndDate;
        var globalAllDay;

        function updateEvent(event, element) {
            //alert(event.description);
            if ($(this).data("qtip")) $(this).qtip("destroy");

            currentUpdateEvent = event;
            var temp = event.title.split('-')[0];

            $('#mUpdate').modal('show');
            $("#txt_UpdateFrom").val(event.title.split('-')[0]);
            $("#txt_UpdateTo").val(event.title.split('-')[0]);
            $("#txt_UpdateContent").val(event.description);
            $("#eventId").val(event.id);
            return false;
        }
        function updateSuccess(updateResult) {
            //alert(updateResult);
        }
        function deleteSuccess(deleteResult) {
            //alert(deleteResult);
        }
        function addSuccess(addResult) {
            // if addresult is -1, means event was not added
            //    alert("added key: " + addResult);

            if (addResult != -1) {
                $('#calendar').fullCalendar('renderEvent',
                                {
                                    title: $("#txt_AddFrom").val() + "-" + $("#txt_AddTo").val(),
                                    start: addStartDate,
                                    end: addEndDate,
                                    id: addResult,
                                    description: $("#txt_AddContent").val(),
                                    allDay: globalAllDay
                                },
                                true // make the event "stick"
                            );


                $('#calendar').fullCalendar('unselect');
            }
        }
        function UpdateTimeSuccess(updateResult) {
            //alert(updateResult);
        }
        function selectDate(start, end, allDay) {

            $('#mAddNew').modal('show');
            //$("#addEventStartDate").text("" + start.toLocaleString());
            //$("#addEventEndDate").text("" + end.toLocaleString());

            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;
            //alert(allDay);
        }
        function eventDropped(event, dayDelta, minuteDelta, allDay, revertFunc) {
            if ($(this).data("qtip")) $(this).qtip("destroy");

            updateEventOnDropResize(event);
        }
        function eventResized(event, dayDelta, minuteDelta, revertFunc) {
            if ($(this).data("qtip")) $(this).qtip("destroy");

            updateEventOnDropResize(event);
        }
        function eventResized(event, dayDelta, minuteDelta, revertFunc) {
            if ($(this).data("qtip")) $(this).qtip("destroy");

            updateEventOnDropResize(event);
        }
        function checkForSpecialChars(stringToCheck) {
            var pattern = /[^A-Za-z0-9 ]/;
            return pattern.test(stringToCheck);
        }
        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }
        function qTipText(start, end, description) {
            var text;

            //if (end !== null)
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") + "<br/><br/>" + description;
            //else
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><br/>" + description;

            return description;
        }
        //----------------
        function getEmployeeKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[2].split('=');
                    return cookies[1];
                }
            }
        }
        function getDepartmentKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[1].split('=');
                    return cookies[1];
                }
            }
        }
    </script>
</asp:Content>
