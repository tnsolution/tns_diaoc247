﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ScheduleList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
            }
        }        
        [WebMethod(true)]
        public static int addEvent(ScheduleItem Item)
        {
            int Key = ScheduleDetail_Data.GetParent(Item.Start, Item.End, Item.DepartmentKey, Item.EmployeeKey);
            if (Key == 0)
            {
                Schedule_Info zParent = new Schedule_Info();
                zParent.EmployeeKey = Item.EmployeeKey;
                zParent.DepartmentKey = Item.DepartmentKey;
                zParent.ScheduleDate = DateTime.Now;
                zParent.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zParent.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zParent.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zParent.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zParent.Create();
                Key = zParent.Key;
            }

            ScheduleDetail_Info zInfo = new ScheduleDetail_Info();
            zInfo.TimeKey = Key;
            zInfo.Title = Item.Title;
            zInfo.Description = Item.Description;
            zInfo.Start = Item.Start;
            zInfo.End = Item.End;
            zInfo.AllDay = Item.AllDay;
            zInfo.EmployeeKey = Item.EmployeeKey;
            zInfo.DepartmentKey = Item.DepartmentKey;

            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Create();

            if (zInfo.Message == string.Empty)
            {
                List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];

                if (idList != null)
                    idList.Add(zInfo.ID);

                return zInfo.ID;
            }
            else
            {
                return -1;
            }
        }
        [WebMethod(true)]
        public static int updateEvent(ScheduleItem Item)
        {
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(Item.ID))
            {
                ScheduleDetail_Info zInfo = new ScheduleDetail_Info();
                zInfo.ID = Item.ID;
                zInfo.Title = Item.Title;
                zInfo.Description = Item.Description;
                zInfo.Start = Item.Start;
                zInfo.End = Item.End;
                zInfo.AllDay = Item.AllDay;
                zInfo.EmployeeKey = Item.EmployeeKey;
                zInfo.DepartmentKey = Item.DepartmentKey;
                zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

                zInfo.Update();

                return zInfo.ID;           
            }

            return -1;
        }
        [WebMethod(true)]
        public static int deleteEvent(int id)
        {
            //idList is stored in Session by JsonResponse.ashx for security reasons
            //whenever any event is update or deleted, the event id is checked
            //whether it is present in the idList, if it is not present in the idList
            //then it may be a malicious user trying to delete someone elses events
            //thus this checking prevents misuse
            List<int> idList = (List<int>)System.Web.HttpContext.Current.Session["idList"];
            if (idList != null && idList.Contains(id))
            {
                ScheduleDetail_Info zInfo = new ScheduleDetail_Info();
                zInfo.ID = id;
                zInfo.Delete();
                return 1;
            }

            return -1;
        }
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY DepartmentName", false);
                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}