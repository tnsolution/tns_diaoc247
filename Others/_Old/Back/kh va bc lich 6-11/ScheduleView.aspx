﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleView.aspx.cs" Inherits="WebApp.SAL.ScheduleView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="row">
                    <ul class="list-unstyled spaced">
                        <asp:Literal ID="Lit_ScheduleInfo" runat="server"></asp:Literal>
                    </ul>
                </div>
                <h4 class="header smaller lighter blue">
                    <i class="ace-icon fa fa-hand-o-right green"></i>&nbsp Chi tiết</></h4>
                <div class="row">
                    <div class="col-sm-12">
                        <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ScheduleKey" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(function () {
            $("#btnEdit").click(function () {
                window.location = "ScheduleEdit.aspx?ID=" + Page.getUrlParameter("ID");
            });
        });
    </script>
</asp:Content>
