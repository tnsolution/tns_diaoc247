﻿using Lib.CRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.CRM
{
    public partial class CustomerList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
                LoadData();
            }
        }

        void LoadData()
        {
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            if (UnitLevel < 2)
            {
                Employee = 0;
                Department = 0;
            }
            List<ItemCustomer> zList;
            if (Session["SearchCRM"] != null)
            {
                ItemCRMSearch zSearch = new ItemCRMSearch();
                if (zSearch.CustomerCategory == "1")
                { zList = Customer_Data.Get(zSearch.CustomerCategory, zSearch.Project, zSearch.AssetCategory, zSearch.CustomerName, zSearch.Phone, zSearch.Status, Department, Employee); }
                else
                { zList = Customer_Data.Get_Trade(zSearch.Project, zSearch.AssetCategory, zSearch.CustomerName, zSearch.Phone, zSearch.TradeType, Department, Employee); }
            }
            else
            {
                zList = Customer_Data.Get(Department, Employee);
            }
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên khách hàng</th>");
            zSb.AppendLine("        <th>SĐT</th>");
            zSb.AppendLine("        <th>Email</th>");
            zSb.AppendLine("        <th>Dự án quan tâm</th>");
            zSb.AppendLine("        <th>Mức độ</th>");
            zSb.AppendLine("        <th>Ngày cập nhật</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int n = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "' type='" + r.CategoryKey + "'>");
                zSb.AppendLine("               <td>" + (n++).ToString() + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br>" + r.Phone2 + "</td>");
                zSb.AppendLine("               <td>" + r.Email1 + "<br>" + r.Email2 + "</td>");
                zSb.AppendLine("               <td>" + r.ProjectName + "</td>");
                zSb.AppendLine("               <td>" + r.Consent + "</td>");
                zSb.AppendLine("               <td>" + r.ModifiedDate.ToDateString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static ItemCustomer[] SearchCustomer(string Type, string Project, string Asset, string Name, string Phone, string Status, int Department, int Employee)
        {
            List<ItemCustomer> zList = Customer_Data.Get(Type, Project, Asset, Name, Phone, Status, Department, Employee);
            ItemCRMSearch zSearch = new ItemCRMSearch();
            zSearch.CustomerCategory = "1";
            zSearch.Project = Project;
            zSearch.AssetCategory = Asset;
            zSearch.CustomerName = Name;
            zSearch.Phone = Phone;
            zSearch.Status = Status;
            zSearch.Department = Department.ToString();
            zSearch.Employee = Employee.ToString();
            HttpContext.Current.Session.Add("SearchCRM", zSearch);
            return zList.ToArray();
        }

        [WebMethod(EnableSession = true)]
        public static ItemCustomer[] SearchCustomerTrade(string Project, string Asset, string Name, string Phone, string TradeType, int Department, int Employee)
        {
            List<ItemCustomer> zList = Customer_Data.Get_Trade(Project, Asset, Name, Phone, TradeType, Department, Employee);
            ItemCRMSearch zSearch = new ItemCRMSearch();
            zSearch.CustomerCategory = "2";
            zSearch.Project = Project;
            zSearch.AssetCategory = Asset;
            zSearch.CustomerName = Name;
            zSearch.Phone = Phone;
            zSearch.TradeType = TradeType;
            zSearch.Department = Department.ToString();
            zSearch.Employee = Employee.ToString();
            HttpContext.Current.Session.Add("SearchCRM", zSearch);
            return zList.ToArray();
        }

        [WebMethod]
        public static string GetTrade(int Customer)
        {
            List<ItemTrade> zList = Customer_Data.Get_Asset(Customer);
            string html = "";


            for (var j = 0; j < zList.Count; j++)
            {
                html += "<div class='col-xs-4'>";
                html += "   <div class='profile-user-info profile-user-info-striped'>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Khách là </div><div class='profile-info-value'>" + zList[j].Owner + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Mã căn/ Sản phẩm</div><div class='profile-info-value'>" + zList[j].AssetID + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Loại giao dịch</div><div class='profile-info-value'>" + zList[j].Category + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Dự án</div><div class='profile-info-value'>" + zList[j].ProjectName + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Ngày hết hạn hợp đồng</div><div class='profile-info-value'>" + zList[j].ContractExpireDate + "</div>";
                html += "       </div>";
                html += "   </div>";
                html += "</div>";
            }

            return html;
        }

        #region [Roles]
        static string[] _Permitsion; //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void CheckRole()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL03";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');

            _Permitsion = result;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    HID_Manager.Value = "1";
                    Tools.DropDown_DDL(DDL_Project, @"SELECT A.ProjectKey, A.ProjectName FROM PUL_Project A ORDER BY A.ProjectName", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    HID_Manager.Value = "1";

                    Tools.DropDown_DDL(DDL_Project, @"SELECT ProjectKey, ProjectName FROM PUL_Project A LEFT JOIN PUL_SharePermition B ON A.ProjectKey = B.AssetKey WHERE B.ObjectTable='Project' AND B.EmployeeKey = " + Department + " ORDER BY A.ProjectName", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY [RANK]", false);

                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                case 3:
                    HID_Manager.Value = "0";

                    Tools.DropDown_DDL(DDL_Project, @"SELECT ProjectKey, ProjectName FROM PUL_Project A LEFT JOIN PUL_SharePermition B ON A.ProjectKey = B.AssetKey WHERE B.ObjectTable='Project' AND B.EmployeeKey = " + Department + " ORDER BY A.ProjectName", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY [RANK]", false);

                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND DepartmentKey = " + Department, false);

                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
        #endregion
    }
}