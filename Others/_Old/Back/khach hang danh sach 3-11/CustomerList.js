﻿$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(document).ready(function () {    
    $('.modal.aside').ace_aside();
    $(".select2").select2({ width: "100%" });

    $("#tblData tbody").on("click", "td:not(.notclick)", function () {
        if ($(this).closest('tr').hasClass('detail-row open'))
            return false;
        var trid = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('Type');
        $('.se-pre-con').fadeIn('slow');
        window.location = "CustomerView.aspx?ID=" + trid + "&Type=" + type;
    });
    $('#tblData tbody').on('click', '.show-details-btn', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).closest('tr').next().hasClass("open")) {
            var id = $(this).closest('tr').attr("id");
            $.ajax({
                type: "POST",
                url: "/CRM/CustomerList.aspx/GetMoreDetail",
                data: JSON.stringify({
                    "Customer": id,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (r) {
                    $('div[customer=' + id + ']').empty();
                    $('div[customer=' + id + ']').append($(r.d));
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        $(this).closest('tr').next().toggleClass('open');
        $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
    });
    $("#giaodich").hide();
    $("#quantam").hide();
    $("#tintrang").hide();

    if ($("[id$=HID_Manager]").val() == "0") {
        $("#quanly").hide();
    }
    $("[id$=DDL_Category]").on('change', function (e) {
        var valueSelected = this.value;
        switch (valueSelected) {
            case "0":
                $("#giaodich").hide();
                $("#quantam").hide();
                $("#tintrang").hide();
                break;
            case "1":
                $("#quantam").show();
                $("#tintrang").show();
                $("#giaodich").hide();
                break;
            case "2":
            case "3":
                $("#quantam").show();
                $("#tintrang").hide();
                $("#giaodich").show();
                break;
        }
    });
    $("[id$=DDL_Project]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetCategoryAsset",
                data: JSON.stringify({
                    "ProjectKey": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                success: function (msg) {
                    var District = $("[id$=DDL_AssetCategory]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    $("[id$=DDL_Department]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetEmployees",
                data: JSON.stringify({
                    "DepartmentKey": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                success: function (msg) {
                    var District = $("[id$=DDL_Employee]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });

    $("#btnSearch").click(function (e) {
        var tradetype = "";
        var status = "";
        var category = $("[id$=DDL_Category]").val();
        var project = $("[id$=DDL_Project]").val();
        var assetcategory = $("[id$=DDL_AssetCategory]").val();

        var name = $("#txtName").val();
        var phone = $("#txtPhone").val();

        var department = $("[id$=DDL_Department]").val();
        var employee = $("[id$=DDL_Employee]").val();

        if (assetcategory == null)
            assetcategory = "";
        if (category == null)
            category = "";
        if (project == null || project == 0)
            project = "";

        $("input[name='chkStatus']:checked").each(function (i) {
            status += $(this).val() + ",";
        });
        $("input[name='chkTradeType']:checked").each(function (i) {
            tradetype += $(this).val() + ",";
        });
        $.ajax({
            type: "POST",
            url: "/CRM/CustomerList.aspx/Search",
            data: JSON.stringify({
                "Category": category,
                "Status": status,
                "Project": project,
                "CategoryAsset": assetcategory,
                "Name": name,
                "Phone": phone,
                "TradeType": tradetype,
                "Department": department,
                "Employee": employee,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $('#tblData > tbody').empty();
                $('#tblData > tbody').append($(msg.d));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnInitCustomer").click(function () {
        var customerType = $("input[name='rdoCustomerType']:checked").val();
        var customerTrade = $("input[name='rdoCustomerTrade']:checked").val();
        var customerName = $("#txtCustomerName").val();

        if (customerType == null ||
            customerTrade == null ||
            customerName == "") {
            Page.showNotiMessageInfo("...", "Bản phải nhập các thông tin cơ bản khi nhập thông tin mới !");
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/CRM/CustomerEdit.aspx/InitCustomer',
            data: JSON.stringify({
                "Type": customerType,
                "Trade": customerTrade,
                "Name": customerName,
            }),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                var trid = r.d.Result;
                var type = r.d.Result2;
                if (r.d.Message != "") {
                    if (confirm("Đã có khách này, bạn có muốn xem thông tin ?.")) {
                        window.location = "CustomerView.aspx?ID=" + trid;
                    }
                }
                else {
                    window.location = "CustomerEdit.aspx?ID=" + trid + "&Type=" + type;
                }
            },
            error: function () {
            },
            complete: function () {
            }
        });
    });
});

/*

var num = 1;
                for (var i = 0; i < msg.d.length; i++) {
                    var button = '<div class="action-buttons">';
                    button += '<a href="#" class="green bigger-140 show-details-btn" title="Show Details">';
                    button += '<i class="ace-icon fa fa-angle-double-up"></i>';
                    button += '<span class="sr-only">Details</span>';
                    button += '</a>';
                    button += '</div>';

                    var detail = '<tr class="detail-row">';
                    detail += '<td colspan="6">';
                    detail += '<div class="table-detail"><div class="row" customer=' + msg.d[i].CustomerKey + '>';
                    detail += '</div></div>';
                    detail += '</td>';
                    detail += '</tr>';

                    //var datetime = new Date(Date.parse(msg.d[i].ModifiedDate, "dd/MM/yyyy"));
                    $('#tblData > tbody:last').append('<tr class="" id="' + msg.d[i].CustomerKey + '" type="' + msg.d[i].CategoryKey + '">'
                            + '<td>' + (num++) + '</td>'
                            + '<td>' + msg.d[i].CustomerName + '</td>'
                            + '<td>' + msg.d[i].Phone1 + "<br/>" + msg.d[i].Phone2 + '</td>'
                            + '<td>' + msg.d[i].Email1 + "<br/>" + msg.d[i].Email2 + '</td>'
                            + '<td class="notclick">' + button + '</td>'                
                            + '<td>' + msg.d[i].ModifiedDate + '</td>'
                            + '</tr>' + detail);
                }


*/