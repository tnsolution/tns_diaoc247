﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductGallery.aspx.cs" Inherits="WebApp.SAL.ProductGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        .headercell {
            width: 100px;
            height: 66px;
        }

        .samplecolor {
            border: 1px solid #999;
            border-radius: 4px;
        }

        .btn {
            font-size: 12px;
        }

        .profile-users {
            display: inline-flex !important;
        }

            .profile-users .memberdiv {
                margin: -15px 4px 0px !important;
            }

                .profile-users .memberdiv .body {
                    margin: 0px !important;
                }

        .houseIcon {
            border: 1px solid #528ec2 !important;
            border-radius: 4px !important;
        }

        .last {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content no-padding-bottom">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_Title" runat="server"></asp:Literal>
                <span class="btn-group tools pull-right">
                    <a href="ProductList.aspx" class="btn btn-primary btn-white" onclick="$('.se-pre-con').fadeIn('slow')"><i class="ace-icon fa fa-external-link"></i>&nbsp;Xem danh sách</a>
                    <a href="#mProduct" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i>&nbsp;Tạo mới</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-2">
                <asp:Literal ID="Lit_Info1" runat="server"></asp:Literal>
            </div>
            <div class="col-xs-10 no-padding">
                <div id="parent">
                    <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm sản phẩm
                    </div>
                </div>
                <div class="modal-body">
                    <asp:Literal ID="Lit_Filter" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <!--chi de xem thong tin-->
    <div class="modal fade" id="mView" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" id="assetHead">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin làm việc</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="assetInfo">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--chi cap nhat san pham-->
    <div class="modal fade" id="mProduct" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Cập nhật thông tin làm việc</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tình trạng</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_StatusAsset" CssClass="select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_DateContractEnd" placeholder="dd/MM/yyyy" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><span id="giathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_PriceRentVND" placeholder="Nhập số" role="numeric" data='money' />
                                        <input type="text" class="form-control" id="txt_PriceRentUSD" placeholder="Nhập số" role="numeric" data='money' />
                                    </div>
                                    <label class="col-sm-2 control-label"><span id="giaban">Giá bán VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_PriceVND" placeholder="Nhập số" role="numeric" data='money' />
                                        <input type="text" class="form-control" id="txt_PriceUSD" placeholder="Nhập số" role="numeric" data='money' />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch1" />
                                            <label class="onoffswitch-label" for="Usdswitch1">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nội thất</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_Furnitur" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Nhu cầu</label>
                                    <div class="col-sm-3">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="ace" value="CN" name="chkPurpose" />
                                                <span class="lbl">Chuyển nhượng</span>
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="ace" value="CT" name="chkPurpose" />
                                                <span class="lbl">Cho thuê</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="txt_AssetDescription" placeholder="..." rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnProcessInfo">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <!--chi cap nhat chu nha-->
    <div class="modal fade" id="mGuest">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin liên hệ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Họ tên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT1</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT2</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Email" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ liên lạc</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveGuest" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <!--chi dung xu ly cong viec-->
    <div class="modal fade" id="mProcess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" id="PageHead">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="PageInfo">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header widget-header-blue widget-header-flat">
                                    <h5 class="widget-title lighter red" id="msgAction">....</h5>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-0">
                                        <div class="space-6"></div>
                                        <div class="row">
                                            <div id="kygui" style="display: none">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Tình trạng</label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="DDL_StatusAsset2" CssClass="select2" runat="server" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                                        <div class="col-sm-3">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" role="datepicker" class="form-control pull-right"
                                                                    id="txt_updateDateContractEnd" placeholder="dd/MM/yyyy" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><span id="updategiathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="txt_updatePriceRentVND" placeholder="Nhập số" role="numeric" data='money' />
                                                            <input type="text" class="form-control" id="txt_updatePriceRentUSD" placeholder="Nhập số" role="numeric" data='money' />
                                                        </div>
                                                        <label class="col-sm-2 control-label"><span id="updategiaban">Giá bán VNĐ</span><sup>*</sup></label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="txt_updatePriceVND" placeholder="Nhập số" role="numeric" data='money' />
                                                            <input type="text" class="form-control" id="txt_updatePriceUSD" placeholder="Nhập số" role="numeric" data='money' />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="onoffswitch">
                                                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch" />
                                                                <label class="onoffswitch-label" for="Usdswitch">
                                                                    <span class="onoffswitch-inner"></span>
                                                                    <span class="onoffswitch-switch"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Nội thất</label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="DDL_Furnitur2" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-sm-2 control-label">Nhu cầu</label>
                                                        <div class="col-sm-3">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="ace" value="CN" name="chkPurpose" />
                                                                    <span class="lbl">Chuyển nhượng</span>
                                                                </label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="ace" value="CT" name="chkPurpose" />
                                                                    <span class="lbl">Cho thuê</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Ghi chú</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control" id="txt_updateAssetDescription" placeholder="..." rows="4"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="chunha" style="display: none">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Họ và tên(*)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoName" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">SĐT1</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoPhone1" placeholder="Nhập text" role="phone" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">SĐT2</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoPhone2" placeholder="Nhập text" role="phone" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoEmail" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoAddress1" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Địa chỉ liên hệ</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoAddress2" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnProcessTask">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_InfoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_InfoType" runat="server" Value="0" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_GuestKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <script src="/SAL/ProductGallery.js"></script>
</asp:Content>
