﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

// set mặt định là PUL_Resale_Apartment , ResaleApartment

namespace WebApp.SAL
{
    public partial class ProductGallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ProjectKey.Value = Request["ID"];

                Tools.DropDown_DDL(DDL_Furnitur, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_Furnitur2, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_StatusAsset, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);
                Tools.DropDown_DDL(DDL_StatusAsset2, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);

                LoadData();
                LoadSelect();
                LoadInfo();
            }
        }
        void LoadData()
        {
            Project_Info zInfo = new Project_Info(HID_ProjectKey.Value.ToInt());
            if (zInfo.IsPrimary != 1)
            {
                Response.Write("<script>alert('Dự án bạn xem không phải dự án chính của công ty, phần mềm sẽ quay lại trang trước đó !');window.location = '/SAL/ProductList.aspx'</script>");
            }

            Lit_Title.Text = "Thông tin sản phẩm dự án " + zInfo.ProjectName;
        }
        void LoadSelect()
        {
            int Key = HID_ProjectKey.Value.ToInt();
            string Tower = Tools.Html_Select("DDL_Tower", "--Chọn tháp--", "form-control select2", false, "SELECT DISTINCT ID1 AS [KEY], ID1 FROM PUL_Resale_Apartment WHERE ProjectKey = " + Key + " AND LEN(ID1) > 0 ORDER BY ID1", false);
            string Floor = Tools.Html_Select("DDL_Floor", "--Chọn tầng--", "form-control select2", false, "SELECT DISTINCT ID2 AS [KEY], ID2 FROM PUL_Resale_Apartment WHERE ProjectKey = " + Key + " ORDER BY ID2 DESC", true);
            string Status = Tools.Html_Select("DDL_Status", "--Chọn tình trạng--", "form-control select2", false, "SELECT AutoKey, Product FROM SYS_Categories WHERE TYPE = 4", true);
            string Want = @"
<select id='DDL_Want' name='DDL_Want' class='select2'>
<option selected='selected' value='name' disabled='disabled'>--Chọn nhu cầu--</option>
<option value='0'>--Tất cả--</option>
<option value='CN/'>Chuyển nhượng</option>
<option value='CT/'>Cho thuê</option>
</select>";

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='row'>");
            zSb.AppendLine("    <div class='col-xs-12'>");
            zSb.AppendLine(Tower);
            zSb.AppendLine("<div class='hr hr2 hr-double'></div>");
            zSb.AppendLine(Floor);
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("    <div class='center'><a class='btn btn-primary btn-sm' id='btnSearch'><i class='ace-icon fa fa-eye'></i>&nbsp Lọc</a></div>");
            zSb.AppendLine("<div class='row'>");
            zSb.AppendLine("    <div class='col-xs-12'>");
            zSb.AppendLine(Status);
            zSb.AppendLine("<div class='hr hr2 hr-double'></div>");
            zSb.AppendLine(Want);
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            Lit_Filter.Text = zSb.ToString();
        }
        void LoadInfo()
        {
            int Key = HID_ProjectKey.Value.ToInt();
            DataTable zTable = Project_Data.CountStatus(Key);
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover'>");
            zSb.AppendLine("<thead><tr><th>STT</th><th>#</th><th>Tình trạng</th><th class='last'>SL</th></tr></thead>");
            zSb.AppendLine("    <tbody>");
            int i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                string CssBack = "";
                switch (r["Status"].ToString())
                {
                    case "29":
                        CssBack = "style='background: #DD5A43 !important;'";
                        break;
                    case "30":
                        CssBack = "style='background: #43dd6c !important;'";
                        break;
                    default:
                        break;
                }
                zSb.AppendLine("<tr><td>" + i++ + "</td><td><div " + CssBack + " class='samplecolor'>&nbsp</div></td><td>" + (r["Name"].ToString() == "" ? "Khác" : r["Name"].ToString()) + "</td><td>" + r["Num"].ToString() + "</td>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            zTable = Project_Data.CountCategory(Key);
            zSb.AppendLine("<table class='table table-hover'>");
            zSb.AppendLine("<thead><tr><th>STT</th><th>Loại</th><th class='last'>SL</th></tr></thead>");
            zSb.AppendLine("    <tbody>");
            i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr><td>" + i++ + "</td><td>" + (r["Name"].ToString() == "" ? "Chưa có data" : r["Name"].ToString()) + "</td><td>" + r["Num"].ToString() + "</td>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            zTable = Project_Data.CountWant(Key);
            zSb.AppendLine("<table class='table table-hover'>");
            zSb.AppendLine("<thead><tr><th>STT</th><th>Nhu cầu</th><th class='last'>SL</th></tr></thead>");
            zSb.AppendLine("    <tbody>");
            i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr><td>" + i++ + "</td><td>" + (r["Name"].ToString() == "" ? "Chưa có data" : r["Name"].ToString()) + "</td><td>" + r["Num"].ToString() + "</td>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_Info1.Text = zSb.ToString();
        }
        [WebMethod]
        public static string GenerateTable(int Key, string Tower, string Floor)
        {
            StringBuilder zSb = new StringBuilder();

            DataTable zTableCellHeader = Product_Data.GetFloor(Key, Tower, Floor);
            zSb.AppendLine("<table id='tableData' class='table table-striped'>");
            for (int i = 0; i < zTableCellHeader.Rows.Count; i++)
            {
                string floor = zTableCellHeader.Rows[i]["ID2"].ToString();
                DataTable zTableCellContent = Product_Data.GetAsset(Key, Tower, floor);

                zSb.AppendLine("<tr>");
                zSb.AppendLine("<td>");
                zSb.AppendLine(CellHeader("Tầng " + floor));
                zSb.AppendLine("</td>");
                zSb.AppendLine("<td>");
                zSb.AppendLine("       <div class='profile-users clearfix'>");
                foreach (DataRow r in zTableCellContent.Rows)
                {
                    int StatusTask = 0;
                    int DetailKey = 0;
                    DataTable zTable = Product_Data.GetData(r["AssetID"].ToString().Trim());
                    if (zTable.Rows.Count > 0)
                    {
                        StatusTask = zTable.Rows[0]["StatusKey"].ToInt();
                        DetailKey = zTable.Rows[0]["AutoKey"].ToInt();
                    }

                    zSb.AppendLine(CellContent(
                        r["AssetKey"].ToInt(),
                        r["Category"].ToString().Trim(),
                        r["AssetID"].ToString().Trim(),
                        r["Status"].ToString().Trim(),
                        r["Room"].ToString().Trim(),
                        r["Want"].ToString().Trim(),
                        StatusTask, DetailKey));
                }
                zSb.AppendLine("     </div>");
                zSb.AppendLine("</td>");
                zSb.AppendLine("</tr>");
            }
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }
        //[WebMethod]
        //public static ItemCustomer GetGuest(string Type, int AutoKey)
        //{
        //    Type = "ResaleApartment";
        //    Owner_Info zInfo = new Owner_Info(Type, AutoKey);
        //    ItemCustomer zItem = new ItemCustomer();
        //    zItem.AssetKey = zInfo.AssetKey.ToString();
        //    zItem.OwnerKey = zInfo.OwnerKey.ToString();
        //    zItem.CustomerName = zInfo.CustomerName;
        //    zItem.Phone1 = zInfo.Phone1;
        //    zItem.Phone2 = zInfo.Phone2;
        //    zItem.Email1 = zInfo.Email;
        //    zItem.Address1 = zInfo.Address1;
        //    zItem.Address2 = zInfo.Address2;
        //    zItem.Type = zInfo.Type;
        //    return zItem;
        //}

        //kiem tra user hien tai co duoc phep xem so dt cua thong tin sản phẩm dang xem
        static bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            if (UnitLevel == 0 || UnitLevel == 1 || CurrentAgent == OwnerAgent)
            {
                return true;
            }

            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }

        #region [Helper vẽ biểu tượng]
        static string CellContent(int Key, string Category, string Name, string Status, string Room, string Want, int StatusTask, int DetailKey)
        {
            string CssBack = "";
            string CssFont = "";
            string CssFaIcon = "blue";
            if (StatusTask == 6)
            {
                CssBack = "style='background: #f1e32d  !important;'";
                CssFont = "style='color:white !important;'";
                CssFaIcon = "white";
            }
            else
            {
                switch (Status)
                {
                    case "29":
                        CssBack = "style='background: #DD5A43 !important;'";
                        CssFont = "style='color:white !important;'";
                        CssFaIcon = "white";
                        break;

                    case "30":
                        CssBack = "style='background: #43dd6c !important;'";
                        CssFont = "style='color:white !important;'";
                        CssFaIcon = "white";
                        break;

                    default:
                        break;
                }
            }
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='btn btn-white btn-default itemdiv memberdiv houseIcon' " + CssBack + " detailkey='" + DetailKey + "' status='" + Status + "' id='" + Name + "' want='" + Want + "' onclick='GetAssetInfo(\"" + Name + "\"," + DetailKey + ")'>");
            zSb.AppendLine("<div class='inline pos-rel'>");
            zSb.AppendLine("<div><div class='user'><a href = '#'><i class='ace-icon fa fa-home middle bigger-220 " + CssFaIcon + @"'></i></a></div>");
            zSb.AppendLine("<div class='body'><div class='name'><a href = '#' class='user' " + CssFont + "><b>" + Name + @"</b></a></div></div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("<div " + CssFont + @">" + Category + ":" + Room + " PN</div>");
            ////zSb.AppendLine("<div class='popover right'>");
            ////zSb.AppendLine("<div class='arrow'></div>");
            ////zSb.AppendLine("<div class='popover-content'>");
            ////zSb.AppendLine("<div class='bolder'>Content Editor</div>");
            ////zSb.AppendLine("<div class='time'><i class='ace-icon fa fa-usd middle bigger-120 orange'></i><span class='red'> 3,000,000,000 (VNĐ) </span></div>");
            ////zSb.AppendLine("<div class='hr dotted hr-8'></div>");
            ////zSb.AppendLine("<div class='tools action-buttons'>");
            ////zSb.AppendLine("<a href = '#'><i class='ace-icon fa fa-pencil-square-o red2 bigger-150'></i> Cập nhật</a>");
            ////zSb.AppendLine("</div>");
            ////zSb.AppendLine("</div>");
            ////zSb.AppendLine("</div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("</div>");
            return zSb.ToString();
        }
        static string CellHeader(string Name)
        {
            string html = @"<div class='btn btn-white btn-default houseIcon headercell'>
															<div class='inline pos-rel'>
																<div class='user action-buttons'>
																	<a href='#'>
																	</a>
																</div>
																<div class='body'>
																	<div class='name'>
																		<a href='#'><b>
																			" + Name + @"</b>
																		</a>
																	</div>
																</div>															
															</div>
														</div>";
            return html;
        }
        #endregion

        #region [Kiểm tra sản phẩm khi click]
        [WebMethod]
        public static ItemResult CheckData(string AssetID)
        {
            ItemResult zResult = new ItemResult();

            //Kiểm tra chéo 2 thông tin
            //1 CV
            //2 SP

            StringBuilder zSb = new StringBuilder();
            string button = "";

            Product_Info zAsset = new Product_Info(AssetID, "ResaleApartment");
            DataTable zTable = Product_Data.GetData(AssetID);
            //Gộp chung 2 thông tin CV và SP
            ItemAsset zItem = zAsset.ItemAsset;

            if (zTable.Rows.Count > 0)
            {
                //1 - tình huống đã có thông tin công việc
                zItem.Task_Key = zTable.Rows[0]["AutoKey"].ToString();
                zItem.Task_InfoKey = zTable.Rows[0]["InfoKey"].ToString();
                zItem.Task_AssetKey = zTable.Rows[0]["AssetKey"].ToString();
                zItem.Task_TableName = zTable.Rows[0]["TableName"].ToString();
                zItem.Task_StatusKey = zTable.Rows[0]["StatusKey"].ToString();
                zItem.Task_StatusName = zTable.Rows[0]["Status"].ToString();

                if (zItem.IsResale.ToInt() == 0)
                {
                    //SP chưa ký gửi có CV
                    //hiển thị form làm việc và thông tin làm việc trước đó nếu có
                    string dropdown = Tools.Html_Dropdown_StatusTask("SELECT ID, Name, Parent FROM TASK_Categories WHERE ID NOT IN (2,6,9) ORDER BY ID");
                    button = "<h4 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i> Xử lý thông tin <div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + dropdown + "</div></h4>";
                    #region [Data xử lý]
                    Excel_Detail_Info zInfo = new Excel_Detail_Info(zItem.Task_Key.ToInt(), 1);
                    zSb.AppendLine(@"<div class='widget-box'>                  
                    <div class='widget-body'>
                        <div class='widget-main padding-0'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zInfo.Asset + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Thông tin xử lý trước đó</div>
                                    <div class='profile-info-value'>Người: " + zTable.Rows[0]["EmployeeName"].ToString() + "<br />Ngày: " + zTable.Rows[0]["DateReplied"].ToDateString() + "<br />Nội dung: " + zItem.Task_StatusName + @"</div>
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>");
                    #endregion

                    zResult.Result = "1";
                    zResult.Result2 = zSb.ToString();
                    zResult.Message = button;
                }
                else
                {
                    //SP có ký gửi có CV
                    //hiển thị thông tin chi tiết CV và thông tin cần xử lý
                    //kiểm tra tình trạng phản hồi thông tin công việc

                    Excel_Detail_Info zInfo = new Excel_Detail_Info(zItem.Task_Key.ToInt(), 1);

                    bool isExists = false;
                    ItemCustomer zGuest = null;

                    if (zItem.AssetKey.ToInt() != 0)
                    {
                        DataTable zTableGuest = Owner_Data.List("ResaleApartment", zItem.AssetKey.ToInt());
                        zGuest = new ItemCustomer();
                        if (zTableGuest.Rows.Count > 0)
                        {
                            isExists = true;
                            zGuest.CustomerName = zTableGuest.Rows[0]["CustomerName"].ToString();
                            zGuest.Phone1 = zTableGuest.Rows[0]["Phone1"].ToString();
                            zGuest.Phone2 = zTableGuest.Rows[0]["Phone2"].ToString();
                            zGuest.Address1 = zTableGuest.Rows[0]["Address1"].ToString();
                            zGuest.Address2 = zTableGuest.Rows[0]["Address2"].ToString();
                        }
                    }

                    string dropdown = Tools.Html_Dropdown_StatusTask("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent <> 0 ORDER BY ID");
                    button = "<h4 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i> Xử lý thông tin <div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + dropdown + "</div></h4>";
                    if (isExists)
                    {
                        zSb.AppendLine("<div class='row'>");
                        zSb.AppendLine("<div class='col-xs-6'>");
                        #region [SP đã có]
                        zSb.AppendLine(@"<div class='widget-box'>                   
                    <div class='widget-body'>
                        <div class='widget-header widget-header-flat'>
                            <h4 class='widget-title lighter'>Thông tin cũ</h4>
                        </div>
                        <div class='widget-main padding-4'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zItem.ProjectName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zItem.AssetID + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zGuest.CustomerName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zGuest.Phone1 + ", " + zGuest.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zGuest.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zGuest.Address2 + @"</div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>");
                        #endregion
                        zSb.AppendLine("</div>");
                        zSb.AppendLine("<div class='col-xs-6'>");
                        #region [SP xử lý]
                        zSb.AppendLine(@"<div class='widget-box'>
                    <div class='widget-body'>
                        <div class='widget-header widget-header-flat'>
                            <h4 class='widget-title lighter'>Thông tin mới</h4>
                        </div>
                        <div class='widget-main padding-4'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value' id='needcheck'>" + zInfo.Asset + @"</div>
                                </div>                             
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>");
                        #endregion
                        zSb.AppendLine("</div>");
                        zSb.AppendLine("</div>");
                    }
                    else
                    {
                        #region [SP xử lý]
                        zSb.AppendLine(@"<div class='widget-box'>                  
                    <div class='widget-body'>
                        <div class='widget-main padding-0'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zInfo.Asset + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>");
                        #endregion
                    }

                    string extrainfo = @"
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Thông tin xử lý trước đó</div>
                                    <div class='profile-info-value'>Người: " + zTable.Rows[0]["EmployeeName"].ToString() + "<br />Ngày: " + zTable.Rows[0]["DateReplied"].ToDateString() + "<br />Nội dung: " + zItem.Task_StatusName + @"</div>
                                </div> ";
                    zSb.AppendLine(extrainfo);
                    zResult.Result = "2";
                    zResult.Result2 = zSb.ToString();
                    zResult.Message = button;
                }
            }
            else
            {
                //2 - tinh huống chưa có thông tin công việc
                if (zItem.IsResale.ToInt() == 0)
                {
                    //SP chưa ký gửi, chưa CV
                    // hiển thị thông báo chưa có thông tin
                    zResult.Result = "3";
                    zResult.Result2 = "<p>[" + zItem.AssetID + "] chưa có thông tin sản phẩm này, vui lòng chọn thông tin khác !.<p>";
                }
                else
                {
                    //SP đã ký gửi, chưa CV
                    //hiển thị thông tin chi tiết
                    #region [Chỉ hiển thị để xem]
                    int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                    #region [html view usd]
                    string ViewMoney = "";
                    ViewMoney += " <div class='col-sm-12'>";
                    ViewMoney += "      <div class='onoffswitch'>";
                    ViewMoney += "          <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='Usdswitch' />";
                    ViewMoney += "          <label class='onoffswitch-label' for='Usdswitch'><span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span></label>";
                    ViewMoney += "      </div>";
                    ViewMoney += "  </div>";
                    #endregion

                    #region [Asset Info]
                    zSb = new StringBuilder();
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Mã căn</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='macan'>" + zAsset.ItemAsset.AssetID + "</div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang'>" + zAsset.ItemAsset.Status + "</div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe'>" + zAsset.ItemAsset.DateContractEnd + "</div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Giá bán <i class='ace-icon fa fa-usd blue'></i></div>");
                    zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giaban1' style='display:none'>" + zAsset.ItemAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giaban2'>" + zAsset.ItemAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Giá thuê <i class='ace-icon fa fa-usd blue'></i></div>");
                    zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giathue1' style='display:none'>" + zAsset.ItemAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giathue2'>" + zAsset.ItemAsset.PriceRent_VND + " (VND)</div></div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Giá mua <i class='ace-icon fa fa-usd blue'></i></div>");
                    zSb.AppendLine("        <div class='profile-info-value'><span class=giatien id='giamua'>" + zAsset.ItemAsset.PricePurchase + " (VND)</span></div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='dientich'>" + zAsset.ItemAsset.AreaWall + "</div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='noithat'>" + zAsset.ItemAsset.Furniture + "</div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='nhucau'>" + zAsset.ItemAsset.Purpose + "</div>");
                    zSb.AppendLine("    </div>");

                    if (UnitLevel <= 1)
                    {
                        zSb.AppendLine("    <div class='profile-info-row'>");
                        zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
                        zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
                        zSb.AppendLine("    </div>");
                    }
                    else if (zAsset.ItemAsset.EmployeeKey == HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"])
                    {
                        zSb.AppendLine("    <div class='profile-info-row'>");
                        zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
                        zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
                        zSb.AppendLine("    </div>");
                    }
                    if (!CheckViewPhone(
                        zAsset.ItemAsset.EmployeeKey.ToInt(),
                        zAsset.ItemAsset.AssetKey.ToInt(),
                        zAsset.ItemAsset.AssetType))
                    {
                        zSb.AppendLine("    <div class='profile-info-row'>");
                        zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                        zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.EmployeeName + "</div>");
                        zSb.AppendLine("    </div>");
                    }
                    else
                    {
                        zSb.AppendLine("    <div class='profile-info-row'>");
                        zSb.AppendLine("        <div class='profile-info-name'>Chủ nhà</div>");
                        zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.CustomerName + "</div>");
                        zSb.AppendLine("    </div>");
                        zSb.AppendLine("    <div class='profile-info-row'>");
                        zSb.AppendLine("        <div class='profile-info-name'>Số điện thoại</div>");
                        zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.Phone1.HtmlPhone() + "</div>");
                        zSb.AppendLine("    </div>");
                    }

                    zSb.AppendLine("</div>");
                    #endregion

                    #region [Action button]
                    button = "<h4 class='modal-title blue'> <i class='ace-icon fa fa-rss orange'></i> Thông tin làm việc <div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>";
                    //button += "<span class='btn-group pull-right'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>";
                    button += "<ul class='dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>";
                    button += "<li><a href='/SAL/ProductView.aspx?ID=" + zAsset.ItemAsset.AssetKey + "&Type=" + zAsset.ItemAsset.AssetType + "' onclick='$('.se-pre-con').fadeIn('slow');'><i class='ace-icon fa fa-external-link blue'></i> Xem chi tiết</a></li>";
                    button += "<li><a href='#' onclick='UpdateAsset(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-pencil-square-o red'></i> Cập nhật thông tin làm việc</a></li>";
                    //button += "<li><a href='#' onclick='UpdateGuest(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-pencil-square-o red'></i> Cập nhật thông tin chủ nhà</a></li>";
                    //if (UnitLevel <= 1)
                    //{
                    //    button += "<li><a href='#' onclick='UpdateAsset2(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-pencil-square-o red'></i> Cập nhật thông tin cứng</a></li>";
                    //    button += "<li><a href='#' onclick='DeleteAsset(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-trash red'></i> Xóa thông tin</a></li>";
                    //}
                    button += "<li class='divider'></li>";
                    button += "<li><a href='#' data-dismiss='modal'><i class='fa fa-times red'></i> Đóng</a></li>";
                    button += "</ul>";
                    button += "</span>";
                    #endregion

                    zResult.Result = "4";
                    zResult.Result2 = zSb.ToString();
                    zResult.Message = button;
                    #endregion
                }
            }
            return zResult;
        }

        #endregion

        //------------------------
        [WebMethod]
        public static ItemResult GetInfoAsset(int ID, int Type)
        {
            string dropdown = Tools.Html_Dropdown_StatusTask("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent <> 0 ORDER BY ID");

            ItemResult zResult = new ItemResult();
            bool isExists = false;
            Excel_Detail_Info zInfo = new Excel_Detail_Info(ID, Type);
            Product_Info zAsset = new Product_Info(zInfo.Asset, "ResaleApartment");
            ItemAsset zItem = zAsset.ItemAsset;
            ItemCustomer zGuest = null;

            if (zItem.AssetKey.ToInt() != 0)
            {
                DataTable zTableGuest = Owner_Data.List("ResaleApartment", zItem.AssetKey.ToInt());
                zGuest = new ItemCustomer();
                if (zTableGuest.Rows.Count > 0)
                {
                    isExists = true;
                    zGuest.CustomerName = zTableGuest.Rows[0]["CustomerName"].ToString();
                    zGuest.Phone1 = zTableGuest.Rows[0]["Phone1"].ToString();
                    zGuest.Phone2 = zTableGuest.Rows[0]["Phone2"].ToString();
                    zGuest.Address1 = zTableGuest.Rows[0]["Address1"].ToString();
                    zGuest.Address2 = zTableGuest.Rows[0]["Address2"].ToString();
                }
            }

            StringBuilder zSb = new StringBuilder();
            StringBuilder zSb2 = new StringBuilder();
            zSb2.AppendLine("<h4 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i> Xử lý thông tin <div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + dropdown + "</div></h4>");

            if (isExists)
            {
                zSb.AppendLine("<div class='row'>");
                zSb.AppendLine("<div class='col-xs-6'>");
                #region [SP đã có]
                zSb.AppendLine(@"<div class='widget-box'>                   
                    <div class='widget-body'>
                        <div class='widget-header widget-header-flat'>
                            <h4 class='widget-title lighter'>Thông tin cũ</h4>
                        </div>
                        <div class='widget-main padding-4'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zItem.ProjectName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zItem.AssetID + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zGuest.CustomerName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zGuest.Phone1 + ", " + zGuest.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zGuest.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zGuest.Address2 + @"</div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>");
                #endregion
                zSb.AppendLine("</div>");
                zSb.AppendLine("<div class='col-xs-6'>");
                #region [SP xử lý]
                zSb.AppendLine(@"<div class='widget-box'>
                    <div class='widget-body'>
                        <div class='widget-header widget-header-flat'>
                            <h4 class='widget-title lighter'>Thông tin mới</h4>
                        </div>
                        <div class='widget-main padding-4'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value' id='needcheck'>" + zInfo.Asset + @"</div>
                                </div>                             
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>");
                #endregion
                zSb.AppendLine("</div>");
                zSb.AppendLine("</div>");
            }
            else
            {
                #region [SP xử lý]
                zSb.AppendLine(@"<div class='widget-box'>                  
                    <div class='widget-body'>
                        <div class='widget-main padding-0'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zInfo.Asset + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>");
                #endregion
            }
            zResult.Result2 = zSb2.ToString();
            zResult.Result = zSb.ToString();
            zResult.Message = zAsset.ItemAsset.AssetKey;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer GetInfoCustomer(int ID, int Type)
        {
            Excel_Detail_Info zInfo = new Excel_Detail_Info(ID, Type);
            ItemCustomer zGuest = new ItemCustomer();
            zGuest.CustomerName = zInfo.FullName;
            zGuest.Phone1 = zInfo.Phone1;
            zGuest.Phone2 = zInfo.Phone2;
            zGuest.Email1 = zInfo.Email1;
            zGuest.Address1 = zInfo.Address1;
            zGuest.Address2 = zInfo.Address2;
            return zGuest;
        }
        [WebMethod]
        public static ItemAsset CheckAsset(int AssetKey, int Type)
        {
            Product_Info zAsset = new Product_Info(AssetKey, "ResaleApartment");
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }

        [WebMethod]
        public static ItemResult SaveProduct(
          int ID, int StatusInfo, int Type, int AssetKey,
          string AssetStatus, string AssetRemind,
          string AssetPrice_VND, string AssetPriceRent_VND,
          string AssetPrice_USD, string AssetPriceRent_USD,
          string AssetFurnitur, string AssetPurpose, string AssetDescription)
        {
            ItemResult zResult = new ItemResult();
            Excel_Detail_Info zInfo = new Excel_Detail_Info(ID, Type);
            ItemAsset zAsset = new ItemAsset();
            zAsset.IsResale = "1";

            #region [PREPARING DATA, Tên dự án, loại căn, bảng lưu data]
            string idenTable = "ResaleApartment";
            string ProjectName = zInfo.Product;
            if (zInfo.AssetCategory.ToInt() == 10 ||
                zInfo.AssetCategory.ToInt() == 11)
                idenTable = "ResaleHouse";
            //if (zInfo.AssetCategory.ToInt() == 12)
            //    idenTable = "Bảng Đất nền";

            DataTable zCategroyTable = new DataTable();
            int zCategoryKey = zInfo.AssetCategory.ToInt();
            int zProjectKey = 0;
            Project_Info zProject = new Project_Info(ProjectName);
            zProjectKey = zProject.Key;
            #endregion

            #region [Asset FixInfo]
            zAsset.ProjectKey = zProjectKey.ToString();
            zAsset.CategoryKey = zCategoryKey.ToString();
            zAsset.AssetID = zInfo.Asset;
            zAsset.ID1 = zInfo.ID1;
            zAsset.ID2 = zInfo.ID2;
            zAsset.ID3 = zInfo.ID3;
            zAsset.AreaWall = zInfo.AreaWall;
            zAsset.AreaWater = zInfo.AreaWater;
            zAsset.PricePurchase = zInfo.Price;
            zAsset.Room = zInfo.Room;
            zAsset.View = zInfo.DirectionView;
            zAsset.Door = zInfo.DirectionDoor;
            zAsset.CategoryKey = zInfo.AssetCategory;
            zAsset.AssetType = idenTable;
            zAsset.IsResale = "1";
            zAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
            zAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            #endregion

            #region [Asset SaleInfo]
            zAsset.Description = AssetDescription;
            zAsset.DateContractEnd = AssetRemind;
            zAsset.StatusKey = AssetStatus;
            zAsset.PriceRent_USD = AssetPriceRent_USD;
            zAsset.Price_USD = AssetPrice_USD;
            zAsset.PriceRent_VND = AssetPriceRent_VND;
            zAsset.Price_VND = AssetPrice_VND;
            zAsset.Furniture = AssetFurnitur;
            zAsset.PurposeKey = AssetPurpose;
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }
            #endregion

            Product_Info zProduct = new Product_Info(AssetKey, zAsset.AssetType);
            zProduct.ItemAsset = zAsset;
            zProduct.Save(zAsset.AssetType);
            if (zProduct.ItemAsset.Message != string.Empty)
            {
                zResult.Message = zProduct.ItemAsset.Message;
                zResult.Result = "Error";
                return zResult;
            }

            Owner_Info zOwner = new Owner_Info();
            zOwner.CustomerName = zInfo.FullName;
            zOwner.Phone1 = zInfo.Phone1;
            zOwner.Phone2 = zInfo.Phone2;
            zOwner.Email = zInfo.Email1;
            zOwner.Address1 = zInfo.Address1;
            zOwner.Address2 = zInfo.Address2;
            zOwner.AssetKey = zProduct.ItemAsset.AssetKey.ToInt();
            zOwner.Create(idenTable);

            if (zOwner.Message != string.Empty)
            {
                zResult.Message = "Lỗi xử lý chủ nhà";
                zResult.Result = "Error";
                return zResult;
            }

            //cap nhat nguoc lai nguon tin de lien ket san pham
            Note_Detail_Info zNote_Detail = new Note_Detail_Info();
            zNote_Detail.DetailKey = zInfo.DetailKey;
            zNote_Detail.StatusInfo = StatusInfo;
            zNote_Detail.DateRespone = DateTime.Now;
            zNote_Detail.ObjectKey = zAsset.AssetKey.ToInt();
            zNote_Detail.ObjectTable = idenTable;
            zNote_Detail.UpdateStatus();

            if (zNote_Detail.Message != string.Empty)
            {
                zResult.Message = "Lỗi xử lý nguồn tin";
                zResult.Result = "Error";
                return zResult;
            }

            zResult.Message = "OK";
            zResult.Result = zProduct.ItemAsset.AssetKey;
            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveTask(int ID, int Status)
        {
            ItemResult zResult = new ItemResult();
            Note_Detail_Info zNote = new Note_Detail_Info();
            zNote.DetailKey = ID;
            zNote.StatusInfo = Status;
            zNote.DateRespone = DateTime.Now;
            zNote.UpdateStatus();
            if (zNote.Message != string.Empty)
            {
                zResult.Message = zNote.Message;
                zResult.Result = "Error-1";
            }
            else
            {
                zResult.Message = "OK";
            }
            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveInfoCustomer(int ID, int Status, int AssetKey, string CustomerName, string Phone1, string Phone2, string Email, string Address1, string Address2)
        {
            ItemResult zResult = new ItemResult();
            Note_Detail_Info zNote = new Note_Detail_Info(ID);
            zNote.StatusInfo = Status;
            zNote.DateRespone = DateTime.Now;
            zNote.UpdateStatus();

            Owner_Info zInfo = new Owner_Info();
            zInfo.CustomerName = CustomerName;
            zInfo.Phone1 = Phone1;
            zInfo.Phone2 = Phone2;
            zInfo.Email = Email;
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.AssetKey = AssetKey;
            zInfo.Create("ResaleApartment");

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "OK";

            return zResult;
        }
        //------------------

        //Action khi chỉ xem thông tin ký gửi
        #region Hiển thị lại thông tin để cập nhật sản phẩm
        [WebMethod]
        public static ItemAsset GetAssetEdit(int AssetKey, string Type)
        {
            Product_Info zAsset = new Product_Info(AssetKey, "ResaleApartment");
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }
        #endregion

        #region [Lưu thông tin làm việc (cập nhật sản phẩm)]
        [WebMethod]
        public static ItemResult SaveProduct(
          int AssetKey, string AssetStatus, string AssetRemind,
          string AssetPrice_VND, string AssetPriceRent_VND,
          string AssetPrice_USD, string AssetPriceRent_USD,
          string AssetFurnitur, string AssetPurpose, string AssetDescription)
        {
            string Type = "ResaleApartment";
            ItemResult zResult = new ItemResult();
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            zInfo.ItemAsset.Price_USD = AssetPrice_USD;
            zInfo.ItemAsset.PriceRent_USD = AssetPriceRent_USD;
            zInfo.ItemAsset.Price_VND = AssetPrice_VND;
            zInfo.ItemAsset.PriceRent_VND = AssetPriceRent_VND;
            zInfo.ItemAsset.FurnitureKey = AssetFurnitur;
            zInfo.ItemAsset.DateContractEnd = AssetRemind;
            zInfo.ItemAsset.StatusKey = AssetStatus;
            zInfo.ItemAsset.PurposeKey = AssetPurpose;
            zInfo.ItemAsset.Description = AssetDescription.Trim();
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zInfo.ItemAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }

            zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save(Type);
            return zResult;
        }
        #endregion        
    }
}

/*
 
             [WebMethod]
        public static string GetAssetInfo(int AssetKey)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            Product_Info zAsset = new Product_Info(AssetKey, "ResaleApartment");

            #region [html view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "      <div class='onoffswitch'>";
            ViewMoney += "          <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='Usdswitch' />";
            ViewMoney += "          <label class='onoffswitch-label' for='Usdswitch'><span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span></label>";
            ViewMoney += "      </div>";
            ViewMoney += "  </div>";
            #endregion

            #region [Asset Info]
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã căn</div>");
            string button = @"<div class='profile-info-value' id='macan'>" + zAsset.ItemAsset.AssetID;
            button += "<div class='btn-group pull-right'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>";
            button += "<ul class='dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>";
            button += "<li><a href='/SAL/ProductView.aspx?ID=" + zAsset.ItemAsset.AssetKey + "&Type=" + zAsset.ItemAsset.AssetType + "' onclick='$('.se-pre-con').fadeIn('slow');'><i class='ace-icon fa fa-external-link blue'></i> Xem chi tiết</a></li>";
            button += "<li><a href='#' onclick='UpdateAsset(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-pencil-square-o red'></i> Cập nhật thông tin làm việc</a></li>";
            button += "<li><a href='#' onclick='UpdateGuest(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-pencil-square-o red'></i> Cập nhật thông tin chủ nhà</a></li>";

            if (UnitLevel <= 1)
            {
                button += "<li><a href='#' onclick='UpdateAsset2(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-pencil-square-o red'></i> Cập nhật thông tin cứng</a></li>";
                button += "<li><a href='#' onclick='DeleteAsset(" + zAsset.ItemAsset.AssetKey + ");'><i class='ace-icon fa fa-trash red'></i> Xóa thông tin</a></li>";
            }
          
            button += "</ul>";
            button += "</div>";
            button += "</div>";

            zSb.AppendLine(button);
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang'>" + zAsset.ItemAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe'>" + zAsset.ItemAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giaban1' style='display:none'>" + zAsset.ItemAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giaban2'>" + zAsset.ItemAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giathue1' style='display:none'>" + zAsset.ItemAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giathue2'>" + zAsset.ItemAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><span class=giatien id='giamua'>" + zAsset.ItemAsset.PricePurchase + " (VND)</span></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dientich'>" + zAsset.ItemAsset.AreaWall + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat'>" + zAsset.ItemAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau'>" + zAsset.ItemAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");

            if (UnitLevel <= 1)
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
                zSb.AppendLine("    </div>");
            }
            else if (zAsset.ItemAsset.EmployeeKey == HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"])
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
                zSb.AppendLine("    </div>");
            }
            if (!CheckViewPhone(
                zAsset.ItemAsset.EmployeeKey.ToInt(),
                zAsset.ItemAsset.AssetKey.ToInt(),
                zAsset.ItemAsset.AssetType))
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.EmployeeName + "</div>");
                zSb.AppendLine("    </div>");
            }
            else
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Chủ nhà</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.CustomerName + "</div>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Số điện thoại</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.Phone1.HtmlPhone() + "</div>");
                zSb.AppendLine("    </div>");
            }

            zSb.AppendLine("</div>");
            #endregion

            return zSb.ToString();
        }
        [WebMethod]
        public static ItemAsset GetAssetEdit(int AssetKey, string Type)
        {
            Product_Info zAsset = new Product_Info(AssetKey, "ResaleApartment");
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }

        [WebMethod]
        public static ItemResult SaveProduct(
          int AssetKey, string AssetStatus, string AssetRemind,
          string AssetPrice_VND, string AssetPriceRent_VND,
          string AssetPrice_USD, string AssetPriceRent_USD,
          string AssetFurnitur, string AssetPurpose, string AssetDescription)
        {
            string Type = "ResaleApartment";
            ItemResult zResult = new ItemResult();
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            zInfo.ItemAsset.Price_USD = AssetPrice_USD;
            zInfo.ItemAsset.PriceRent_USD = AssetPriceRent_USD;
            zInfo.ItemAsset.Price_VND = AssetPrice_VND;
            zInfo.ItemAsset.PriceRent_VND = AssetPriceRent_VND;
            zInfo.ItemAsset.FurnitureKey = AssetFurnitur;
            zInfo.ItemAsset.DateContractEnd = AssetRemind;
            zInfo.ItemAsset.StatusKey = AssetStatus;
            zInfo.ItemAsset.PurposeKey = AssetPurpose;
            zInfo.ItemAsset.Description = AssetDescription.Trim();
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zInfo.ItemAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }

            zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save(Type);
            return zResult;
        }
     
     */


//if (zTable.Rows.Count > 0)
//{
//    //đã có thông tin công việc
//    int Status = zTable.Rows[0]["StatusKey"].ToInt();
//    //kiểm tra phản hồi công việc
//    switch (Status)
//    {
//        case 1:     //Ký gửi
//        case 9:     //Cập nhật chủ nhà > hiển thị thông tin như form hiện tại
//            break;
//        default:    //phản hồi cuộc gọi > hiển thị thông tin cuộc gọi gần nhất + thông tin cần gọi
//            break;
//    }
//}
//else
//{
//    //chưa có thông tin công việc > hiển thị thông báo chưa có thông tin
//}