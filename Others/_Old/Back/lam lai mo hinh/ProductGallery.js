﻿var _usd = 0, _assetkey = 0;       //tinh gia usd
var _status = 0;    //tinh trang nguon tin
$(document).on('change', '[type=checkbox]', function (e) {
    // code here
    if ($(this).is(':checked')) {
        $("div[view='usd']").show();
        $("div[view='vnd']").hide();
        _usd = 1;
    }
    else {
        $("div[view='vnd']").show();
        $("div[view='usd']").hide();
        _usd = 0;
    }
});
$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(window).load(function () {
    // Animate loader off screen
    //$(".se-pre-con").fadeOut("slow");
});
$(document).ready(function () {
    $(".modal.aside").ace_aside();
    $(".footer").css("display", "none");
    $(".select2").select2({ width: "100%" });
    var height = $(window).height() - 210;
    $('#parent').height(height);
    //------------------------------------------------Lay out
    $("#btnProcessTask").click(function () {
        runAction();
    });
    $("#btnProcessInfo").click(function () {
        var Purpose = "";
        $("input[name='chkPurpose']:checked").each(function (i) {
            Purpose += $(this).val() + "/ ";
        });

        $.ajax({
            type: "POST",
            url: "/SAL/ProductGallery.aspx/SaveProduct",
            data: JSON.stringify({
                "AssetKey": _assetkey,
                "AssetStatus": $("[id$=DDL_StatusAsset]").val(),
                "AssetRemind": $("[id$=txt_DateContractEnd]").val(),
                "AssetPrice_VND": $("[id$=txt_PriceVND]").val(),
                "AssetPriceRent_VND": $("[id$=txt_PriceRentVND]").val(),
                "AssetPrice_USD": $("[id$=txt_PriceUSD]").val(),
                "AssetPriceRent_USD": $("[id$=txt_PriceRentUSD]").val(),
                "AssetFurnitur": $("[id$=DDL_Furnitur]").val(),
                "AssetPurpose": Purpose,
                "AssetDescription": $("[id$=txt_AssetDescription]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message == "OK") {
                    Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.se-pre-con').fadeOut('slow');
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnSaveGuest").click(function () {
        $.ajax({
            type: 'POST',
            url: '/SAL/ProductEdit.aspx/SaveGuest',
            data: JSON.stringify({
                'Type': $("[id$=HID_AssetType]").val(),
                'AutoKey': $('[id$=HID_GuestKey]').val(),
                'AssetKey': $("[id$=HID_AssetKey]").val(),
                'CustomerName': $('[id$=txt_CustomerName]').val(),
                'Phone': $('[id$=txt_Phone]').val(),
                'Phone2': $('[id$=txt_Phone2]').val(),
                'Email': $('[id$=txt_Email]').val(),
                'Address': $('[id$=txt_Address]').val(),
                'Address2': $('[id$=txt_Address2]').val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    $("#mGuest").modal("hide");
                    Page.showNotiMessageInfo("Thông báo !.", "Đã cập nhật thành công");
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnSearch").click(function () {
        $('.se-pre-con').fadeIn('slow');
        LoadMap();
    });
    //------------------------------------------------Action button

    $("[id$=txt_PriceRentUSD]").hide();
    $("[id$=txt_PriceUSD]").hide();

    $("[id$=Usdswitch1]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PriceRentUSD]").show();
            $("[id$=txt_PriceUSD]").show();
            $("[id$=txt_PriceRentVND]").hide();
            $("[id$=txt_PriceVND]").hide();
            $("#giathue").text("Giá thuê USD");
            $("#giaban").text("Giá bán USD");
            _usd = 1;
        }
        else {
            $("[id$=txt_PriceRentUSD]").hide();
            $("[id$=txt_PriceUSD]").hide();
            $("[id$=txt_PriceRentVND]").show();
            $("[id$=txt_PriceVND]").show();
            $("#giathue").text("Giá thuê VNĐ");
            $("#giaban").text("Giá bán VNĐ");
            _usd = 0;
        }
    });

    $("[id$=txt_PriceUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_PriceRentUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_PriceVND]").blur(function () { ConvertMoney(); });
    $("[id$=txt_PriceRentVND]").blur(function () { ConvertMoney(); });

    $("#DDL_Want").on("change", function () {
        var want = this.value;
        if (want == 0) {
            $("#tableData td:not(:first-child) div.houseIcon[want]").css({ 'opacity': 1 });
            return;
        }
        $("#tableData td:not(:first-child) div.houseIcon[want]").css({ 'opacity': 1 });
        $("#tableData td:not(:first-child) div.houseIcon:not([want='" + want + "'])").css({ 'opacity': 0.25 });
    });
    $("#DDL_Status").on("change", function () {
        var status = this.value;
        if (status == 0) {
            $("#tableData td:not(:first-child) div.houseIcon[status]").css({ 'opacity': 1 });
            return;
        }
        $("#tableData td:not(:first-child) div.houseIcon[status]").css({ 'opacity': 1 });
        $("#tableData td:not(:first-child) div.houseIcon:not([status='" + status + "'])").css({ 'opacity': 0.25 });
    });
    //------------------------------------------------Event Input

    // khoi chạy
    LoadMap();
});
//--------------------Action
function action(id, txt) {
    _status = id;
    $("#kygui").hide();
    $("#khachhang").hide();
    $("#chunha").hide();
    $("#msgAction").text("Bạn chọn xử lý: " + txt);

    //ky gui
    if (id == 1) {
        $("#kygui").show();
        $("#msgAction").text("Bạn chọn xử lý: " + txt + " vui lòng kiểm tra các thông tin liên quan !.");
    }
    //quan tam
    if (id == 2) {
        $("#khachhang").show();
        $("#msgAction").text("Bạn chọn xử lý: " + txt + " vui lòng kiểm tra các thông tin liên quan !.");
    }
    //chu nha
    if (id == 9) {
        $("#chunha").show();
        $("#msgAction").text("Bạn chọn xử lý: " + txt + " vui lòng kiểm tra các thông tin liên quan !.");
    }
}
function runAction() {
    switch (_status) {
        case 1:
            saveProduct();
            break;

        case 2:
            break;

        case 9:
            saveOwner();
            break;

        default:
            updateInfo();
            break;
    }
}
function updateInfo() {
    var id = $("[id$=HID_InfoKey]").val();
    $.ajax({
        type: 'POST',
        url: '/SAL/ProductGallery.aspx/SaveTask',
        data: JSON.stringify({
            'ID': id,
            'Status': _status,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu cập nhật thông tin thành công.");
                window.location = "/SAL/ProductGallery.aspx"
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.responseText.indexOf('network') != -1)
                Page.showNotiMessageInfo("Lỗi mạng", "Vui lòng thử lại sau ít phút !.");
            else
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function saveOwner() {
    if (_assetkey != 0) {
        var id = $("[id$=HID_InfoKey]").val();
        var type = $("[id$=HID_InfoType]").val();

        $.ajax({
            type: 'POST',
            url: '/SAL/ProductGallery.aspx/SaveInfoCustomer',
            data: JSON.stringify({
                'ID': id,
                'Status': _status,
                'AssetKey': _assetkey,
                'CustomerName': $("[id$=txt_InfoName]").val(),
                'Phone1': $("[id$=txt_InfoPhone1]").val(),
                'Phone2': $("[id$=txt_InfoPhone2]").val(),
                'Email': $("[id$=txt_InfoEmail]").val(),
                'Address1': $("[id$=txt_InfoAddress1]").val(),
                'Address2': $("[id$=txt_InfoAddress2]").val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message == "OK") {
                    Page.showNotiMessageInfo("Thông báo !", "Đã lưu thong tin chu nha thành công.");
                    window.location = "/SAL/ProductGallery.aspx"
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
    else
        Page.showNotiMessageInfo("Không thể cập nhật thông tin", "Chưa có sản phẩm này chưa có trong dữ liệu, vui lòng kiểm tra lại thông tin !.");
}
function saveProduct() {
    var id = $("[id$=HID_InfoKey]").val();
    var type = $("[id$=HID_InfoType]").val();
    var Purpose = "";
    $("input[name='chkPurpose']:checked").each(function (i) {
        Purpose += $(this).val() + "/ ";
    });

    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/SaveProduct",
        data: JSON.stringify({
            "ID": id,
            "StatusInfo": _status,
            "Type": type,
            "AssetKey": _assetkey,
            "AssetStatus": $("[id$=DDL_StatusAsset]").val(),
            "AssetRemind": $("[id$=txt_DateContractEnd]").val(),
            "AssetPrice_VND": $("[id$=txt_PriceVND]").val(),
            "AssetPriceRent_VND": $("[id$=txt_PriceRentVND]").val(),
            "AssetPrice_USD": $("[id$=txt_PriceUSD]").val(),
            "AssetPriceRent_USD": $("[id$=txt_PriceRentUSD]").val(),
            "AssetFurnitur": $("[id$=DDL_Furnitur]").val(),
            "AssetPurpose": Purpose,
            "AssetDescription": $("[id$=txt_AssetDescription]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                window.location = "/SAL/ProductGallery.aspx"
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.se-pre-con').fadeOut('slow');
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getProduct(id, type) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetInfoAsset",
        data: JSON.stringify({
            "ID": id,
            "Type": type,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            _assetkey = msg.d.Message;
            //checkProduct(msg.d.Message, type);

            $("#PageHead").empty();
            $("#PageHead").append($(msg.d.Result2));
            $("#PageInfo").empty();
            $("#PageInfo").append($(msg.d.Result));
            $("#mProcess").modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getOwner(id, type) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetInfoCustomer",
        data: JSON.stringify({
            "ID": id,
            "Type": type,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function (msg) {
            $("#txt_InfoName").val(msg.d.CustomerName);
            $("#txt_InfoPhone1").val(msg.d.Phone1);
            $("#txt_InfoPhone2").val(msg.d.Phone2);
            $("#txt_InfoEmail").val(msg.d.Email1);
            $("#txt_InfoAddress1").val(msg.d.Address1);
            $("#txt_InfoAddress2").val(msg.d.Address2);
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function checkProduct(id, type) {
    if (id != 0) {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductGallery.aspx/CheckAsset",
            data: JSON.stringify({
                "AssetKey": id,
                "Type": type,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (msg) {
                $("#msgAction").text("SP da co, vui long kiem tra cap nhat thong tin !.");

                $("[id$=DDL_StatusAsset]").val(msg.d.StatusKey);
                $("[id$=txt_DateContractEnd]").val(msg.d.DateContractEnd);
                $("[id$=txt_PriceRentVND]").val(msg.d.PriceRent_VND);
                $("[id$=txt_PriceRentUSD]").val(msg.d.PriceRent_USD);
                $("[id$=txt_PriceVND]").val(msg.d.Price_VND);
                $("[id$=txt_PriceUSD]").val(msg.d.Price_USD);
                $("[id$=DDL_Furnitur]").val(msg.d.FurnitureKey);
                var SetPurpose = msg.d.PurposeKey.split("/");
                for (var i in SetPurpose) {
                    var val = $.trim(SetPurpose[i]);
                    $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}
//-----------------------------
function GetAssetInfo(id, id2) {
    var trid = id2;
    var trtype = 1;//type nay la type cong viec da gui chua gửi
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/CheckData",
        data: JSON.stringify({
            "AssetID": id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (r) {
            console.log(r.d.Result);
            if (r.d.Result == "3") {
                Page.showNotiMessageInfo(r.d.Result2);               
            }
            else {
                if (r.d.Result == "4") {
                    $("#mView").modal({
                        backdrop: true,
                        show: true
                    });

                    $("#assetInfo").empty();
                    $("#assetInfo").append($(r.d.Result2));
                    $("#assetHead").empty();
                    $("#assetHead").append($(r.d.Message));
                }
                else {
                    $("#mProcess").modal({
                        backdrop: true,
                        show: true
                    });

                    $("#PageHead").empty();
                    $("#PageHead").append($(r.d.Result2));
                    $("#PageInfo").empty();
                    $("#PageInfo").append($(r.d.Message));

                    //mở xử lý thong tin
                    getProduct(trid, trtype);                 
                    setTimeout(function () {
                        getOwner(trid, trtype);
                    }, 1000);
                }
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function LoadMap() {
    var tower = $("#DDL_Tower").val();
    var floor = $("#DDL_Floor").val();
    if (tower == null || tower <= 0)
        tower = $("#DDL_Tower").prop('selectedIndex', 1).val();//mac dinh chon 1 thap
    if (floor == null || floor <= 0)
        floor = $("#DDL_Floor").prop('selectedIndex', 1).val();
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GenerateTable",
        data: JSON.stringify({
            "Key": $("[id$=HID_ProjectKey]").val(),
            "Tower": tower,
            "Floor": floor,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            $('#parent').empty();
            $('#parent').append($(r.d));
        },
        complete: function () {
            setTimeout(function () {
                $('.se-pre-con').fadeOut('slow');
                $("#tableData").tableHeadFixer({
                    "head": false,
                    "left": 1
                });
            }, 3000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//--------------------
function ActiveClick(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetAssetInfo",
        data: JSON.stringify({
            "AssetKey": id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (r) {
            //$("#assetInfo").empty();
            //$("#assetInfo").append($(r.d));

            $("#mView").modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function Active() {
    $(".se-pre-con").fadeIn("slow");
    var want = $("#DDL_Want").val();
    var status = $("#DDL_Status").val();

    if (want == 0) {
        $("#tableData td:not(:first-child) div.houseIcon[want]").css({ 'opacity': 1 });
        return;
    }
    else {
        $("#tableData td:not(:first-child) div.houseIcon:not([want='" + want + "'])").css({ 'opacity': 0.25 });
        $("#tableData td:not(:first-child) div.houseIcon[want='" + want + "']").css({ 'opacity': 1 });
        return;
    }
}
function UpdateAsset(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetAssetEdit",
        data: JSON.stringify({
            "AssetKey": id,
            "Type": ""
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            _assetkey = msg.d.AssetKey;
            $("[id$=txt_DateContractEnd]").val(msg.d.DateContractEnd);
            $("[id$=txt_PriceRentVND]").val(msg.d.PriceRent_VND);
            $("[id$=txt_PriceRentUSD]").val(msg.d.PriceRent_USD);
            $("[id$=txt_PriceVND]").val(msg.d.Price_VND);
            $("[id$=txt_PriceUSD]").val(msg.d.Price_USD);
            $("[id$=DDL_Furnitur]").val(msg.d.FurnitureKey).trigger("change");
            $("[id$=DDL_StatusAsset]").val(msg.d.StatusKey).trigger("change");
            var SetPurpose = msg.d.PurposeKey.split("/");
            for (var i in SetPurpose) {
                var val = $.trim(SetPurpose[i]);
                $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
            }

            $("#mProduct").modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function UpdateGuest(id) {
    //$.ajax({
    //    type: "POST",
    //    url: "/SAL/ProductGallery.aspx/GetGuest",
    //    data: JSON.stringify({
    //        'Type': "",
    //        'AutoKey': id,
    //    }),
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    beforeSend: function () {
    //    },
    //    success: function (msg) {
    //        if (msg.d.Message != "") {
    //            Page.showPopupMessage("Lỗi !", msg.d.Message);
    //        }
    //        else {
    //            $('#mGuest').modal({
    //                backdrop: true,
    //                show: true
    //            });
    //            $("[id$=HID_AssetType]").val(msg.d.Type);
    //            $("[id$=HID_GuestKey]").val(msg.d.OwnerKey);
    //            $("[id$=HID_AssetKey]").val(msg.d.AssetKey);
    //            $("[id$=txt_CustomerName]").val(msg.d.CustomerName);
    //            $("[id$=txt_Phone]").val(msg.d.Phone1);
    //            $("[id$=txt_Phone2]").val(msg.d.Phone2);
    //            $("[id$=txt_Email]").val(msg.d.Email1);
    //            $("[id$=txt_Address]").val(msg.d.Address1);
    //            $("[id$=txt_Address2]").val(msg.d.Address2);
    //        }
    //    },
    //    complete: function () {
    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {
    //        console.log(xhr.status);
    //        console.log(xhr.responseText);
    //        console.log(thrownError);
    //    }
    //});

    $('#mGuest').modal({
        backdrop: true,
        show: true
    });
}
function ConvertMoney() {
    if (_usd == 1) {
        var priceUsd = $("[id$=txt_PriceUSD]").val();
        var priceRentUsd = $("[id$=txt_PriceRentUSD]").val();
        var priceVnd = Page.RemoveComma(priceUsd) * usdRate;
        var priceRentVnd = Page.RemoveComma(priceRentUsd) * usdRate;

        $("[id$=txt_PriceRentVND]").val(Page.FormatMoney(priceRentVnd));
        $("[id$=txt_PriceVND]").val(Page.FormatMoney(priceVnd));
    }
    else {
        var priceVnd = $("[id$=txt_PriceVND]").val();
        var priceRentVnd = $("[id$=txt_PriceRentVND]").val();
        var priceUsd = Page.RemoveComma(priceVnd) / usdRate;
        var priceRentUsd = Page.RemoveComma(priceRentVnd) / usdRate;

        $("[id$=txt_PriceRentUSD]").val(Page.FormatMoney(priceRentUsd));
        $("[id$=txt_PriceUSD]").val(Page.FormatMoney(priceUsd));
    }
}
function validate() {
    $('input[required]').each(function (idx, item) {
        Page.checkError($(item));
    });
    $('select[required]').each(function (idx, item) {
        Page.checkSelect($(item));
    });
    if ($('div.error').length > 0) {
        return false;
    }
    return true;
}