﻿var error_obj = null;
var error_prefix = "Phải nhập ";
var error_message = "<div class='text-danger error'>{msg}</div>";
var usdRate = 22760;
var Page = {
    initialize: function () {

    },   
    //---------------------------------
    showNotiMessageInfo: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-info gritter-light"
        });
    },
    showNotiMessageError: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-error gritter-light"
        });
    },
    showNotiMessageInfoButton: function (title, content) {
        $.gritter.add({
            title: title,
            text: content + '<br/><a href="#" class="red">Đồng ý</a>',
            class_name: "gritter-error gritter-light"
        });
    },
    showPopupMessage: function (title, content) {
        $("#titContent").text(content);
        $("#titMessage").text(title);
        $("#mMessageBox").modal({
            backdrop: true,
            show: true
        });
    },
    getUrlParameter: function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split("&"),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split("=");

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    getCurrentDate: function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
    getNextWorkDate: function () {
        var output = "";
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate() + 1;

        if (d.getDay() == 6) {
            day = d.getDate() + 2;
            output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        }
        else {
            output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        }

        return output;
    },
    getLastDayMonth: function () {
        var date = new Date();
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var month = ((lastDay.getMonth().length + 1) === 1) ? (lastDay.getMonth() + 1) : '0' + (lastDay.getMonth() + 1);
        var day = lastDay.getDate() + "/" + month + "/" + lastDay.getFullYear();
        return day;
    },
    getFirstDayMonth: function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var month = ((firstDay.getMonth().length + 1) === 1) ? (firstDay.getMonth() + 1) : '0' + (firstDay.getMonth() + 1);
        var day = firstDay.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + "/" + month + "/" + firstDay.getFullYear();
        return output;
    },
    //---------------------------------
    CheckExt: function (file) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'png':
            case 'gif':
                return false;
            case 'zip':
            case 'rar':
                return false;
            case 'pdf':
                return false;
            case 'xlsx':
            case 'xls':
            case 'docx':
            case 'doc':
                return true;

            default:
                return false;
        }
    },
    RemoveUnicode: function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "");
        str = str.replace(/-+-/g, ""); //thay thế 2- thành 1- 
        str = str.replace(/^\-+|\-+$/g, "");

        return str;
    },
    FormatMoney: function (num) {
        var str = num.toString().replace("$", "").replace(/ /g, ''),
        parts = false,
        output = [],
        i = 1,
        formatted = null;

        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");

        return (formatted);
    },
    RemoveComma: function (value) {
        return value.replace(/,/g, '');
    },
    //----------------------
    validate: function () {
        $('input[required]').each(function (idx, item) {
            Page.checkError($(item));
        });
        $('select[required]').each(function (idx, item) {
            Page.checkSelect($(item));
        });

        if ($('div.error').length > 0) {
            return false;
        }
        return true;
    },
    checkError: function (txt) {
        if (txt.attr("role") === "datepicker") {
            txt.parent().next().remove();
        } else {
            txt.next().remove();
        }
        var val = $.trim(txt.val());
        if (val === "") {
            var title = error_prefix + $.trim(txt.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            if (txt.attr("role") === "datepicker") {
                txt.parent().parent().append($(error));
            } else {
                txt.parent().append($(error));
            }
            if (error_obj === null) {
                error_obj = txt;
            }
        }
    },
    checkSelect: function (select) {
        select.next().next().remove();
        var val = select.val();
        if (val <= 0) {
            var title = error_prefix + $.trim(select.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            select.parent().append($(error));
            if (error_obj === null) {
                error_obj = select;
            }
        }
    },
    //-----------------------
    resetValue: function (id) {
        jQuery("#" + id).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                case 'select-one':
                case 'select-multiple':
                case 'date':
                case 'number':
                case 'tel':
                case 'email':
                    jQuery(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
                    break;
            }
        });
    },
};
$(document).ready(Page.initialize);