﻿using Lib.Config;
using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ProductGallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                {
                    HID_ProjectKey.Value = Request["ID"];
                    Project_Info zInfo = new Project_Info(HID_ProjectKey.Value.ToInt());
                    HID_AssetType.Value = zInfo.ShortTable;
                    HID_MainTable.Value = zInfo.MainTable;
                }
                Tools.DropDown_DDL(DDL_Legal, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 6", false);
                Tools.DropDown_DDL(DDL_Furnitur2, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_StatusAsset2, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 4", false);

                LoadSelect();
                LoadInfo();

                Lit_Title.Text = "Lexington Residence";
            }
        }
        void LoadSelect()
        {
            int Key = HID_ProjectKey.Value.ToInt();
            string Tower = Tools.Html_Select("DDL_Tower", "--Chọn tháp--", "form-control select2", false, "SELECT DISTINCT ID1 AS [KEY], ID1 FROM PUL_Resale_Apartment WHERE ProjectKey = " + Key + " AND LEN(ID1) > 0 ORDER BY ID1", false);
            string Floor = Tools.Html_Select("DDL_Floor", "--Chọn tầng--", "form-control select2", false, "SELECT DISTINCT ID2 AS [KEY], ID2 FROM PUL_Resale_Apartment WHERE ProjectKey = " + Key + " ORDER BY ID2 DESC", true);
            string Status = Tools.Html_Select("DDL_Status", "--Chọn tình trạng--", "form-control select2", false, "SELECT AutoKey, Product FROM SYS_Categories WHERE TYPE = 4", true);
            string AssetNo = "<input type='text' class='form-control' id='txtAssetNo' maxlength='2' placeholder='Số căn'>";
            string Want = @"
<select id='DDL_Want' name='DDL_Want' class='select2'>
<option selected='selected' value='name' disabled='disabled'>--Chọn nhu cầu--</option>
<option value='0'>--Tất cả--</option>
<option value='CN/'>Chuyển nhượng</option>
<option value='CT/'>Cho thuê</option>
</select>";

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='row'>");
            zSb.AppendLine("    <div class='col-xs-12'>");
            zSb.AppendLine(Tower + Floor + AssetNo);
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("<div class='row'>");
            zSb.AppendLine("    <div class='col-xs-12'>");
            zSb.AppendLine(Status);

            zSb.AppendLine(Want);
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            Lit_Filter.Text = zSb.ToString();
        }
        void LoadInfo()
        {
            int ProjectKey = HID_ProjectKey.Value.ToInt();
            DataTable zTable = Project_Data.CountStatus(ProjectKey);
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover'>");
            zSb.AppendLine("<thead><tr><th>STT</th><th>#</th><th>Tình trạng</th><th class='last'>SL</th></tr></thead>");
            zSb.AppendLine("    <tbody>");
            int i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                string CssBack = "";
                switch (r["Status"].ToString())
                {
                    case "29":
                        CssBack = "style='background: #DD5A43 !important;'";
                        break;
                    case "30":
                        CssBack = "style='background: #43dd6c !important;'";
                        break;
                    default:
                        break;
                }
                zSb.AppendLine("<tr><td>" + i++ + "</td><td><div " + CssBack + " class='samplecolor'>&nbsp</div></td><td>" + (r["Name"].ToString() == "" ? "Khác" : r["Name"].ToString()) + "</td><td>" + r["Num"].ToString() + "</td>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            zTable = Project_Data.CountCategory(ProjectKey);
            zSb.AppendLine("<table class='table table-hover'>");
            zSb.AppendLine("<thead><tr><th>STT</th><th>Loại</th><th class='last'>SL</th></tr></thead>");
            zSb.AppendLine("    <tbody>");
            i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr><td>" + i++ + "</td><td>" + (r["Name"].ToString() == "" ? "Chưa có data" : r["Name"].ToString()) + "</td><td>" + r["Num"].ToString() + "</td>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            zTable = Project_Data.CountWant(ProjectKey);
            zSb.AppendLine("<table class='table table-hover'>");
            zSb.AppendLine("<thead><tr><th>STT</th><th>Nhu cầu</th><th class='last'>SL</th></tr></thead>");
            zSb.AppendLine("    <tbody>");
            i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr><td>" + i++ + "</td><td>" + (r["Name"].ToString() == "" ? "Chưa có data" : r["Name"].ToString()) + "</td><td>" + r["Num"].ToString() + "</td>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_Info1.Text = zSb.ToString();
        }
        [WebMethod]
        public static string GenerateTable(int ProjectKey, string MainTable, string ShortTable, string Tower, string Floor, string AssetNo)
        {
            StringBuilder zSb = new StringBuilder();

            DataTable zTableCellHeader = Product_Data.GetFloor(ProjectKey, Tower, Floor);
            zSb.AppendLine("<table id='tableData' class='table table-striped'>");
            for (int i = 0; i < zTableCellHeader.Rows.Count; i++)
            {
                string floor = zTableCellHeader.Rows[i]["ID2"].ToString();
                DataTable zTableCellContent = Product_Data.GetAsset(ProjectKey, Tower, floor, AssetNo);
                zSb.AppendLine("<tr>");
                zSb.AppendLine("<td>" + CellHeader("Tầng " + floor) + "</td>");
                zSb.AppendLine("<td>");
                zSb.AppendLine("       <div style='display:inline-flex;'>");
                foreach (DataRow r in zTableCellContent.Rows)
                {
                    int StatusTask = r["StatusKey"].ToInt();
                    int DetailKey = r["AutoKey"].ToInt();
                    bool Privacy = CheckViewPhone(r["EmployeeManager"].ToInt(), r["AssetKey"].ToInt(), ShortTable);

                    zSb.AppendLine(CellContent(
                        r["AssetKey"].ToInt(), r["IsResale"].ToInt(),
                        r["Category"].ToString().Trim(),
                        r["AssetID"].ToString().Trim(),
                        r["Status"].ToString().Trim(),//tinh trang sp, đã bán, giao dich,...
                        r["Room"].ToString().Trim(),
                        r["Want"].ToString().Trim(),//nhu cầu
                        StatusTask, DetailKey,//tình trang cv, mã cv
                        Privacy, ShortTable));
                }
                zSb.AppendLine("     </div>");
                zSb.AppendLine("</td>");
                zSb.AppendLine("</tr>");
            }
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }

        #region  [kiem tra user hien tai co duoc phep xem so dt cua thong tin sản phẩm dang xem]
        static bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            if (UnitLevel == 0 || UnitLevel == 1 || CurrentAgent == OwnerAgent)
            {
                return true;
            }

            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }
        #endregion        
        #region [Helper vẽ biểu tượng]
        static string CellContent(
            int AssetKey, int IsResale, string Category, string AssetID,
            string Status, string Room, string Want, int StatusTask,
            int DetailKey, bool Privacy, string AssetType)
        {
            string Opacity = "0.1";
            if (Privacy)
                Opacity = "1";

            string ActionJQ = "";
            string CssDiv = "";
            string CssFont = "";
            string faIconHome = "<i class='ace-icon fa fa-home middle bigger-220 blue'></i>";
            string faIconTask = "<i class='fa fa-cog fa-spin bigger-130 blue'></i>";

            if (IsResale == 0)//chưa ký gửi
            {
                faIconHome = "<i class='ace-icon fa fa-home middle bigger-220 blue'></i>";
                CssDiv = "style='margin-right:5px; !important; Opacity:" + Opacity + "''";
                CssFont = "style=''";

                if (StatusTask == 6)//có cong viec
                {
                    faIconTask = "<i class='fa fa-cog fa-spin bigger-130 blue'></i>";
                    ActionJQ = "onclick='OpenTask(" + AssetKey + "," + DetailKey + ")'";// mở công việc
                }
                else//chưa công việc
                {
                    faIconTask = "";
                    ActionJQ = "onclick='CreateInfo(" + AssetKey + ",\"" + AssetType + "\")'";//tạo mới thông tin hoàn toàn
                }
            }
            else//có ky gui
            {
                if (Status.ToInt() == 29 || Status.ToInt() == 30)// da giao dich
                {
                    faIconHome = "<i class='ace-icon fa fa-home middle bigger-220 white'></i>";
                    CssDiv = "style='margin-right:5px; !important; background: #DD5A43 !important; Opacity:" + Opacity + "'";
                    CssFont = "style='color:white !important;'";
                }
                else//chua giao dich
                {
                    faIconHome = "<i class='ace-icon fa fa-home middle bigger-220 white'></i>";
                    CssDiv = "style='margin-right:5px; !important; background: #43dd6c !important; Opacity:" + Opacity + "'";
                    CssFont = "style='color:white !important;'";
                }

                if (StatusTask == 6) //có cong viec
                {
                    faIconTask = "<i class='fa fa-cog fa-spin bigger-130 white'></i>";
                    ActionJQ = "onclick='OpenTaskAndInfo(" + AssetKey + ",\"" + AssetType + "\"," + DetailKey + ")'";
                }
                else//chưa công việc
                {
                    faIconTask = "";
                    ActionJQ = " onclick='OpenInfo(" + AssetKey + ",\"" + AssetType + "\")'";
                }
            }
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='btn btn-white btn-default no-padding ItemIcon' " + CssDiv + " statustask=" + StatusTask + " detailkey = '" + DetailKey + "' status = '" + Status + "' id = '" + AssetID + "' want = '" + Want + "' " + ActionJQ + ">");
            zSb.AppendLine("    <div><a href = '#'>" + faIconHome + "</a>&nbsp;<span>" + faIconTask + "</span></div>");
            zSb.AppendLine("    <div><a href = '#'" + CssFont + "><b>" + AssetID + @"</b></a></div>");
            zSb.AppendLine("    <div " + CssFont + @">" + Category + ":" + Room + " PN</div>");
            zSb.AppendLine("</div>");
            return zSb.ToString();
        }
        static string CellHeader(string Name)
        {
            string html = @"<div class='btn btn-white btn-default houseIcon headercell'><div style='padding-top:15px'><a href='#'><b>" + Name + @"</b></a></div></div>";
            return html;
        }
        #endregion

        //-------------------Option No Task
        [WebMethod]
        public static ItemResult OpenInfo(int AssetKey, string AssetType)
        {
            Product_Info zInfo = new Product_Info(AssetKey, AssetType);
            ItemAsset zAsset = zInfo.ItemAsset;
            DataTable zTable = new SqlContext().GetData("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent = 1 ORDER BY ID");
            StringBuilder zSb = new StringBuilder();

            #region Action button and title
            zSb.AppendLine("<ul class='dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>");
            zSb.AppendLine("<li><a href='/SAL/ProductView.aspx?ID=" + AssetKey + "&Type=" + AssetType + "' onclick='$('.se-pre-con').fadeIn('slow');'><i class='ace-icon fa fa-external-link blue'></i> Xem chi tiết</a></li>");
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<li><a href='#' onclick='ActionInfo(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'><i class='ace-icon fa fa fa-pencil-square-o orange'></i> " + r["Name"].ToString() + "</a></li>");
            }
            zSb.AppendLine("    <li class='divider'></li>");
            zSb.AppendLine("    <li><a href='#' data-dismiss='modal'><i class='fa fa-times red'></i> Đóng</a></li>");
            zSb.AppendLine("</ul>");
            string button = "<h5 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i> Thông tin chi tiết<div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + zSb.ToString() + "</div></h5>";
            #endregion

            #region [Chỉ hiển thị để xem]
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            #region [html view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "      <div class='onoffswitch'>";
            ViewMoney += "          <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='Usdswitch' />";
            ViewMoney += "          <label class='onoffswitch-label' for='Usdswitch'><span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span></label>";
            ViewMoney += "      </div>";
            ViewMoney += "  </div>";
            #endregion
            #region [Asset Info]
            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã căn</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='macan'>" + zAsset.AssetID + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang'>" + zAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe'>" + zAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giaban1' style='display:none'>" + zAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giaban2'>" + zAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giathue1' style='display:none'>" + zAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giathue2'>" + zAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giamua1' style='display:none'>" + zAsset.PricePurchase_USD + " (USD)</span></div><div view=vnd class='pull-left giatien' id='giamua2'>" + zAsset.PricePurchase_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dientich'>" + zAsset.AreaWall + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat'>" + zAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau'>" + zAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.Description + "</div>");
            zSb.AppendLine("    </div>");

            if (!CheckViewPhone(
                zAsset.EmployeeKey.ToInt(),
                zAsset.AssetKey.ToInt(),
                zAsset.AssetType))
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.EmployeeName + "</div>");
                zSb.AppendLine("    </div>");
            }
            else
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Chủ nhà</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.CustomerName + "</div>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Số điện thoại</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.Phone1.HtmlPhone() + "</div>");
                zSb.AppendLine("    </div>");
            }

            zSb.AppendLine("</div>");
            #endregion                                  
            #endregion

            ItemResult zResult = new ItemResult();
            zResult.Result = zSb.ToString();
            zResult.Result2 = button;
            zResult.Result3 = zAsset.AssetKey;
            return zResult;
        }
        [WebMethod]
        public static ItemAsset GetEditAsset(int AssetKey, string AssetType)
        {
            Product_Info zInfo = new Product_Info(AssetKey, AssetType);
            return zInfo.ItemAsset;
        }
        [WebMethod]
        public static ItemResult SaveAsset(
            int AssetKey, string AssetType,
            string AssetStatus, string AssetRemind,
            string AssetPrice_VND, string AssetPriceRent_VND,
            string AssetPrice_USD, string AssetPriceRent_USD,
            string AssetPricePurchase_VND, string AssetPricePurchase_USD,
            string AssetFurnitur, string AssetPurpose,
            string AssetDescription, string AssetLegal)
        {
            ItemResult zResult = new ItemResult();
            Product_Info zProduct = new Product_Info(AssetKey, AssetType);
            ItemAsset zAsset = zProduct.ItemAsset;
            zAsset.AssetKey = AssetKey.ToString();
            zAsset.IsResale = "1";
            if (AssetStatus == "313")//tinh trang dùng ở
                zAsset.IsResale = "0";
            zAsset.StatusKey = AssetStatus;
            zAsset.LegalKey = AssetLegal;
            #region [Asset SaleInfo]
            zAsset.Description = AssetDescription;
            zAsset.DateContractEnd = AssetRemind;
            zAsset.PriceRent_USD = AssetPriceRent_USD;
            zAsset.PriceRent_VND = AssetPriceRent_VND;
            zAsset.PricePurchase_VND = AssetPricePurchase_VND;
            zAsset.PricePurchase_USD = AssetPricePurchase_USD;
            zAsset.Price_VND = AssetPrice_VND;
            zAsset.Price_USD = AssetPrice_USD;
            zAsset.Furniture = AssetFurnitur;
            zAsset.PurposeKey = AssetPurpose;
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }
            #endregion      
            zProduct.Save(AssetType);
            if (zProduct.ItemAsset.Message != string.Empty)
            {
                zResult.Message = zProduct.ItemAsset.Message;
                zResult.Result = "Error";
                return zResult;
            }

            zResult.Message = "OK";
            zResult.Result = zProduct.ItemAsset.AssetKey;
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveGuest(
            int AutoKey, string AssetType,
            int AssetKey, string CustomerName,
            string Phone1, string Phone2, string Email1,
            string Address1, string Address2)
        {
            ItemResult zResult = new ItemResult();
            Owner_Info zCustomer = new Owner_Info(AssetType, AutoKey);
            zCustomer.AssetKey = AssetKey;
            zCustomer.CustomerName = CustomerName;
            zCustomer.Email = Email1;
            zCustomer.Phone1 = Phone1;
            zCustomer.Phone2 = Phone2;
            zCustomer.Address1 = Address1;
            zCustomer.Address2 = Address2;
            zCustomer.Create(AssetType);
            if (zCustomer.Message != string.Empty)
                zResult.Message = zCustomer.Message;
            return zResult;
        }
        //------------------- Option Task New
        [WebMethod]
        public static ItemResult OpenTask(int TaskKey)
        {
            DataTable zTable = new SqlContext().GetData("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent <> 0 ORDER BY ID");
            StringBuilder zSb = new StringBuilder();
            #region Action button and title
            zSb.AppendLine("<ul class='dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>");
            foreach (DataRow r in zTable.Rows)
            {
                if (Convert.ToInt32(r["Parent"]) != 1)
                    zSb.AppendLine("<li><a href='#' onclick='ActionTask(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'><i class='ace-icon fa fa fa-pencil-square-o orange'></i> " + r["Name"].ToString() + "</a></li>");
            }
            zSb.AppendLine("    <li class='divider'></li>");
            foreach (DataRow r in zTable.Rows)
            {
                if (Convert.ToInt32(r["Parent"]) == 1 &&
                    Convert.ToInt32(r["ID"]) != 9)
                    zSb.AppendLine("<li><a href='#' onclick='ActionTask(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'><i class='ace-icon fa fa fa-pencil-square-o orange'></i> " + r["Name"].ToString() + "</a></li>");
            }
            zSb.AppendLine("    <li class='divider'></li>");
            zSb.AppendLine("    <li><a href='#' data-dismiss='modal'><i class='fa fa-times red'></i> Đóng</a></li>");
            zSb.AppendLine("</ul>");
            string button = "<h5 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i> Xử lý thông tin<div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + zSb.ToString() + "</div></h5>";
            #endregion

            Excel_Detail_Info zInfo = new Excel_Detail_Info(TaskKey, 1);
            zSb = new StringBuilder();
            #region [SP xử lý]
            zSb.AppendLine(@"<div class='widget-box'>                  
                    <div class='widget-body'>                       
                        <div class='widget-main padding-0'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zInfo.Asset + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>");
            #endregion

            ItemResult zResult = new ItemResult();
            zResult.Result = zSb.ToString();
            zResult.Result2 = button;
            zResult.Message = zInfo.DetailKey.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer GetTaskGuest(int TaskKey)
        {
            Excel_Detail_Info zInfo = new Excel_Detail_Info(TaskKey, 1);
            ItemCustomer zGuest = new ItemCustomer();
            zGuest.CustomerName = zInfo.FullName;
            zGuest.Phone1 = zInfo.Phone1;
            zGuest.Phone2 = zInfo.Phone2;
            zGuest.Email1 = zInfo.Email1;
            zGuest.Address1 = zInfo.Address1;
            zGuest.Address2 = zInfo.Address2;
            return zGuest;
        }
        [WebMethod]
        public static ItemResult SaveTask(
            int TaskKey, int TaskStatus, int AssetKey,
            string AssetType, string AssetStatus, string AssetRemind,
            string AssetPrice_VND, string AssetPriceRent_VND,
            string AssetPrice_USD, string AssetPriceRent_USD,
            string AssetPricePurchase_VND, string AssetPricePurchase_USD,
            string AssetFurnitur, string AssetPurpose,
            string AssetDescription, string AssetLegal,
            string CustomerName,
            string Email, string Phone1, string Phone2,
            string Address1, string Address2)
        {
            ItemResult zResult = new ItemResult();

            //luu san pham           
            Product_Info zInfo = new Product_Info(AssetKey, AssetType);
            ItemAsset zAsset = zInfo.ItemAsset;
            #region [Asset SaleInfo]
            zAsset.Description = AssetDescription;
            zAsset.DateContractEnd = AssetRemind;
            zAsset.StatusKey = AssetStatus;
            zAsset.PriceRent_USD = AssetPriceRent_USD;
            zAsset.Price_USD = AssetPrice_USD;
            zAsset.PriceRent_VND = AssetPriceRent_VND;
            zAsset.Price_VND = AssetPrice_VND;
            zAsset.PricePurchase_USD = AssetPricePurchase_USD;
            zAsset.PricePurchase_VND = AssetPricePurchase_VND;
            zAsset.Furniture = AssetFurnitur;
            zAsset.PurposeKey = AssetPurpose;
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }
            #endregion         
            zInfo.ItemAsset.IsResale = "1";
            zAsset.LegalKey = AssetLegal;
            if (zInfo.ItemAsset.EmployeeKey.ToInt() == 0 &&
            zInfo.ItemAsset.DepartmentKey.ToInt() == 0 &&
            zInfo.ItemAsset.CreatedBy == string.Empty &&
            zInfo.ItemAsset.CreatedDate == string.Empty &&
            zInfo.ItemAsset.CreatedName == string.Empty)
            {
                zInfo.ItemAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zInfo.ItemAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.UpdateManagerment(AssetType);
            }

            zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Update(AssetType);
            if (zInfo.ItemAsset.Message.Trim().Length > 0)
            {
                zResult.Message = "Lỗi xử lý công việc !. " + zInfo.ItemAsset.Message;
                return zResult;
            }
            else
                zResult.Message = string.Empty;

            //luu chu nhà
            Owner_Info zOwner = new Owner_Info();
            zOwner.AssetKey = AssetKey;
            zOwner.CustomerName = CustomerName;
            zOwner.Phone1 = Phone1;
            zOwner.Phone2 = Phone2;
            zOwner.Email = Email;
            zOwner.Address1 = Address1;
            zOwner.Address2 = Address2;
            zOwner.Create(AssetType);

            if (zOwner.Message.Trim().Length > 0)
            {
                zResult.Message = "Lỗi xử lý chủ nhà !. " + zOwner.Message;
                return zResult;
            }
            else
                zResult.Message = string.Empty;

            //luu lại thông tin công việc
            Excel_Detail_Info zTask = new Excel_Detail_Info(TaskKey, 1);
            Note_Detail_Info zTaskNote = new Note_Detail_Info();
            zTaskNote.DetailKey = zTask.DetailKey;
            zTaskNote.StatusInfo = TaskStatus;
            zTaskNote.DateRespone = DateTime.Now;
            zTaskNote.ObjectKey = zAsset.AssetKey.ToInt();
            zTaskNote.ObjectTable = AssetType;
            zTaskNote.UpdateStatus();

            if (zTaskNote.Message.Trim().Length > 0)
            {
                zResult.Message = "Lỗi cập nhật công việc !. " + zTaskNote.Message;
                return zResult;
            }
            else
                zResult.Message = string.Empty;
            return zResult;
        }

        //------------------ Option Task And Info
        [WebMethod]
        public static ItemResult OpenTaskAndInfo(int AssetKey, string AssetType, int TaskKey)
        {
            StringBuilder zSb = new StringBuilder();
            Product_Info zInfoAsset = new Product_Info(AssetKey, AssetType);
            Excel_Detail_Info zInfoTask = new Excel_Detail_Info(TaskKey, 1);
            ItemAsset zAsset = zInfoAsset.ItemAsset;
            DataTable zTable = new SqlContext().GetData("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent <> 0 ORDER BY ID");
            #region Action button and title
            zSb.AppendLine("<ul class='dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>");
            foreach (DataRow r in zTable.Rows)
            {
                if (Convert.ToInt32(r["Parent"]) != 1)
                    zSb.AppendLine("<li><a href='#' onclick='ActionTask(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'><i class='ace-icon fa fa fa-pencil-square-o orange'></i> " + r["Name"].ToString() + "</a></li>");
            }
            zSb.AppendLine("    <li class='divider'></li>");
            foreach (DataRow r in zTable.Rows)
            {
                if (Convert.ToInt32(r["Parent"]) == 1 &&
                    Convert.ToInt32(r["ID"]) != 9)
                    zSb.AppendLine("<li><a href='#' onclick='ActionTask(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'><i class='ace-icon fa fa fa-pencil-square-o orange'></i> " + r["Name"].ToString() + "</a></li>");
            }
            zSb.AppendLine("    <li class='divider'></li>");
            zSb.AppendLine("    <li><a href='#' data-dismiss='modal'><i class='fa fa-times red'></i> Đóng</a></li>");
            zSb.AppendLine("</ul>");
            string button = "<h5 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i>[" + zInfoAsset.ItemAsset.AssetID.Trim() + "] có thông công việc mới cần xử lý <div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + zSb.ToString() + "</div></h5>";
            #endregion
            zSb = new StringBuilder();
            #region [Hiển thị lại thông tin cũ để xem]
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            #region [html view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "      <div class='onoffswitch'>";
            ViewMoney += "          <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='Usdswitch' />";
            ViewMoney += "          <label class='onoffswitch-label' for='Usdswitch'><span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span></label>";
            ViewMoney += "      </div>";
            ViewMoney += "  </div>";
            #endregion
            #region [Asset Info]
            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã căn</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='macan'>" + zAsset.AssetID + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang'>" + zAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe'>" + zAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giaban1' style='display:none'>" + zAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giaban2'>" + zAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giathue1' style='display:none'>" + zAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giathue2'>" + zAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giamua1' style='display:none'>" + zAsset.PricePurchase_USD + " (USD)</span></div><div view=vnd class='pull-left giatien' id='giamua2'>" + zAsset.PricePurchase_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dientich'>" + zAsset.AreaWall + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat'>" + zAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau'>" + zAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.Description + "</div>");
            zSb.AppendLine("    </div>");
            if (!CheckViewPhone(
                zAsset.EmployeeKey.ToInt(),
                zAsset.AssetKey.ToInt(),
                zAsset.AssetType))
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.EmployeeName + "</div>");
                zSb.AppendLine("    </div>");
            }
            else
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Chủ nhà</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.CustomerName + "</div>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Số điện thoại</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.Phone1.HtmlPhone() + "</div>");
                zSb.AppendLine("    </div>");
            }

            zSb.AppendLine("</div>");
            #endregion
            #endregion
            #region [Hiển thị thông tin làm việc]         
            #region [SP xử lý]
            zSb.AppendLine(@"<div class='widget-box'>      
                    <div class='widget-header widget-header-blue widget-header-flat'>
                        <h5 class='widget-title lighter red'>Thông tin mới cần cập nhật</h5>
                    </div>            
                    <div class='widget-body'>                       
                        <div class='widget-main padding-0'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfoTask.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zInfoTask.Asset + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfoTask.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfoTask.Phone1 + ", " + zInfoTask.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfoTask.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfoTask.Address2 + @"</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>");
            #endregion
            #endregion
            ItemResult zResult = new ItemResult();
            zResult.Result = zSb.ToString();
            zResult.Result2 = button;
            zResult.Result3 = zAsset.AssetKey;
            return zResult;
        }

        [WebMethod]
        public static ItemResult UpdateTask(int TaskKey, int TaskStatus)
        {
            ItemResult zResult = new ItemResult();
            Note_Detail_Info zNote = new Note_Detail_Info();
            zNote.DetailKey = TaskKey;
            zNote.StatusInfo = TaskStatus;
            zNote.DateRespone = DateTime.Now;
            zNote.UpdateStatus();
            if (zNote.Message != string.Empty)
            {
                zResult.Message = zNote.Message;
                zResult.Result = "Error-1";
            }
            else
            {
                zResult.Message = "OK";
            }
            return zResult;
        }

        //------------------- Get Nearest Task Replied
        [WebMethod]
        public static ItemResult GetNearestTask(int TaskKey)
        {
            ItemResult zResult = new ItemResult();
            Note_Detail_Info zNote = new Note_Detail_Info(TaskKey);
            zResult.Message = zNote.StatusRespone + " - " + zNote.DateRespone.ToDateTimeString();

            return zResult;
        }
    }
}