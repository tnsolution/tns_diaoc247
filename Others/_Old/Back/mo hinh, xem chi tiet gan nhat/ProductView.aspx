﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductView.aspx.cs" Inherits="WebApp.SAL.ProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
    <style>
        .profile-user-info {
            width: 100% !important;
        }

        .widget-box {
            margin: 0px !important;
        }

        .profile-info-name {
            width: 150px !important;
        }

        #scroll table th {
            padding: 0px !important;
        }

        #scroll table td {
            padding-left: 4px !important;
            padding-right: 4px !important;
        }

        #scroll table .td1 {
            width: 10%;
        }

        #scroll table .td2 {
            width: 15%;
        }

        #scroll table .td5 {
            width: 15%;
        }

        .widget-main {
            padding: 4px !important;
        }

        .col-xs-6 {
            padding-left: 10px;
            padding-right: 10px;
        }

        .indam {
            font-weight: bolder;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết sản phẩm
                <span id="tieudetrang">
                    <asp:Literal ID="txt_Asset" runat="server"></asp:Literal>
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a class="btn btn-white btn-info btn-bold" id="btnExeReminder">
                        <i class="ace-icon fa fa-adjust blue"></i>
                        Xử lý nhắc nhở
                    </a>
                    <a href="#" class="btn btn-white btn-warning btn-bold toggle-accordion" id="btnExpand">Mở rộng 
                    </a>
                </span>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:goBackWithRefresh(); $('.se-pre-con').fadeIn('slow');">
                        <i class="ace-icon fa fa-reply info"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6" id="leftbox">
                <table id="tblMain" class="table  table-bordered table-hover">
                    <tbody>
                        <tr class="action indam">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin làm việc</td>
                            <td style="width: 1%">

                                <a href="#" tabindex="-1" class="center" id="update"><i class='ace-icon fa fa-pencil-square-o orange bigger-130'></i></a>

                            </td>
                        </tr>
                        <tr class="detail-row open">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Literal ID="Lit_InfoAsset" runat="server"></asp:Literal>
                                            <asp:Literal ID="Lit_InfoOwner" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin cơ bản</td>
                            <td></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Literal ID="Lit_ExtraAsset" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Hình ảnh</td>
                            <td>

                                <a href="#mUpload" tabindex="-1" class="center" data-toggle="modal"><i class='ace-icon fa fa-plus orange bigger-130'></i></a>

                            </td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <ul class="ace-thumbnails clearfix" id="hinhanh">
                                        <asp:Literal ID="Lit_File" runat="server"></asp:Literal>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Chủ nhà</td>
                            <td>

                                <a href="#" tabindex="-1" class="center" id="AddGuest"><i class="ace-icon fa fa-plus orange bigger-130"></i></a>

                            </td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="chunha">
                                            <asp:Literal ID="Lit_Owner" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin quản lý</td>
                            <td>
                                <a href="#" tabindex="-1" class="center" id="AddShare"><i class="ace-icon fa fa-plus orange bigger-130"></i></a></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="quanly">
                                            <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-6" id="rightbox">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Các sản phẩm cùng dự án:
                            <asp:Literal ID="Lit_RightTitle" runat="server"></asp:Literal></h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div id="scroll">
                                <asp:Literal ID="Lit_RightTable" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mProcess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" id="PageHead">
                    <h5>Cập nhật thông tin làm việc</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tình trạng</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_StatusAsset" CssClass="select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_DateContractEnd" placeholder="dd/MM/yyyy" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><span id="giathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_PriceRentVND" placeholder="Nhập số" role="numeric" data='money' />
                                        <input type="text" class="form-control" id="txt_PriceRentUSD" placeholder="Nhập số" role="numeric" data='money' />
                                    </div>
                                    <label class="col-sm-2 control-label"><span id="giaban">Giá bán VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_PriceVND" placeholder="Nhập số" role="numeric" data='money' />
                                        <input type="text" class="form-control" id="txt_PriceUSD" placeholder="Nhập số" role="numeric" data='money' />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch1" />
                                            <label class="onoffswitch-label" for="Usdswitch1">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nội thất</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_Furnitur" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Nhu cầu</label>
                                    <div class="col-sm-3">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="ace" value="CN" name="chkPurpose" />
                                                <span class="lbl">Chuyển nhượng</span>
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="ace" value="CT" name="chkPurpose" />
                                                <span class="lbl">Cho thuê</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="txt_AssetDescription" placeholder="..." rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnProcess">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mGuest">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin liên hệ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Họ tên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT1</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT2</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Email" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ liên lạc</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveGuest" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mUpload">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Tải ảnh</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="text-warning list-unstyled spaced">
                                <li><i class="ace-icon fa fa-exclamation-triangle"></i>Hình ảnh sản phẩm (bạn có thể chọn nhiều tập tin)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
                        </div>
                        <div class="col-xs-4">
                            <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
                                <i class="ace-icon fa fa-plus"></i>
                                Upload
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mPost">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Đăng tin</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <asp:DropDownList ID="DDL_WebChon" CssClass="form-control select2" runat="server">
                                    <asp:ListItem Value="0" Text="Không đăng tin"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đăng tin"></asp:ListItem>
                                </asp:DropDownList>
                                <input type="text" class="form-control" id="txt_WebTieuDe" placeholder="Tiêu đề tin đăng" />
                                <textarea class="form-control" id="txt_WebNoiDung" placeholder="Nội dung tin đăng"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' id='btnSavePost' runat="server" data-dismiss='modal'>
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
    <asp:HiddenField ID="HID_RemindKey" runat="server" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" />
    <asp:HiddenField ID="HID_EmployeeKey" runat="server" />
    <asp:HiddenField ID="HID_GuestKey" runat="server" />
    <asp:Button ID="btnUploadImg" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadImg_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/SAL/ProductView.js"></script>
    <script>
        jQuery(function ($) {
            $("#btnExpand").on("click", function () {
                $(this).toggleClass("active");
                $(this).text($(this).text() == 'Mở rộng' ? 'Thu gọn' : 'Mở rộng');
                if ($(this).hasClass("active"))
                    $('#tblMain tbody tr').removeClass('open indam');
                else {
                    $('#tblMain tbody tr.action').addClass('indam');
                    $('#tblMain tbody tr').nextUntil('.action').addClass('open');
                }
            });
            $('#tblMain tr.action td:not(:last-child)').on('click', function (e) {
                e.preventDefault();
                if ($(this).closest('tr').hasClass('indam'))
                    $(this).closest('tr').removeClass('indam');
                else
                    $(this).closest('tr').addClass('indam');
                $(this).closest('tr').next().toggleClass('open');
            });
        });
    </script>
</asp:Content>
