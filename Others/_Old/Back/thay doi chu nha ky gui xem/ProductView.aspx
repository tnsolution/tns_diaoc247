﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductView.aspx.cs" Inherits="WebApp.SAL.ProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        #scroll table th {
            padding: 0px !important;
        }

        #scroll table td {
            padding-left: 4px !important;
            padding-right: 4px !important;
        }

        #scroll table .td1 {
            width: 10%;
        }

        #scroll table .td2 {
            width: 15%;
        }

        #scroll table .td5 {
            width: 15%;
        }

        .widget-main {
            padding: 4px !important;
        }

        .col-xs-6 {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết sản phẩm
                <span id="tieudetrang">
                    <asp:Literal ID="txt_Asset" runat="server"></asp:Literal>
                </span>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a href="javascript:void(0)" class="btn btn-white btn-warning btn-bold toggle-accordion" accordion-id="#accordion">
                        <i class="ace-icon fa fa-expand orange"></i>Mở rộng 
                    </a>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnExeReminder">
                        <i class="ace-icon fa fa-adjust blue"></i>
                        Xử lý nhắc nhở
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:goBackWithRefresh(); $('.se-pre-con').fadeIn('slow');">
                        <i class="ace-icon fa fa-reply info"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6" id="leftbox">
                <div id="accordion" class="accordion-style1 panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Thông tin làm việc                                         
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseOne" aria-expanded="true">
                            <div class="panel-body" id="body1">
                                <h5 class="row header smaller lighter orange no-margin-top no-padding-top col-xs-offset-0 col-xs-12">...
                                    <span class='pull-right action-buttons'><a href="#" tabindex="-1" class="center" id="update"><i class='ace-icon fa fa-pencil-square-o orange'></i>&nbsp;Cập nhật</a></span>
                                </h5>
                                <asp:Literal ID="Lit_InfoAsset" runat="server"></asp:Literal>
                                <asp:Literal ID="Lit_InfoOwner" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Thông tin cơ bản                                    
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseTwo">
                            <div class="panel-body">
                                <asp:Literal ID="Lit_ExtraAsset" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Hình ảnh
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThree">
                            <div class="panel-body">
                                <ul class="ace-thumbnails clearfix" id="hinhanh">
                                    <asp:Literal ID="Lit_File" runat="server"></asp:Literal>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Chủ nhà
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse4">
                            <div class="panel-body" id="chunha">
                                <asp:Literal ID="Lit_Owner" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Tin đăng 247
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse5">
                            <div class="panel-body">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Tiêu đề
                                        </div>
                                        <div class="profile-info-value" id="tieude" value>
                                            <asp:Literal ID="txt_WebTitle" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Nội dung tin
                                        </div>
                                        <div class="profile-info-value" id="noidung" value>
                                            <asp:Literal ID="txt_WebContent" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Xuất bản
                                        </div>
                                        <div class="profile-info-value" id="xuatban" value>
                                            <asp:Literal ID="txt_WebPublish" runat="server"></asp:Literal>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Quản lý
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse6">
                            <div class="panel-body" id="quanly">
                                <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6" id="rightbox">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Các sản phẩm cùng dự án:
                            <asp:Literal ID="Lit_RightTitle" runat="server"></asp:Literal></h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div id="scroll">
                                <asp:Literal ID="Lit_RightTable" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mProcess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" id="PageHead">
                    <h5>Cập nhật thông tin làm việc</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tình trạng</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_StatusAsset" CssClass="select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_DateContractEnd" placeholder="dd/MM/yyyy" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><span id="giathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_PriceRentVND" placeholder="Nhập số" role="numeric" data='money' />
                                        <input type="text" class="form-control" id="txt_PriceRentUSD" placeholder="Nhập số" role="numeric" data='money' />
                                    </div>
                                    <label class="col-sm-2 control-label"><span id="giaban">Giá bán VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_PriceVND" placeholder="Nhập số" role="numeric" data='money' />
                                        <input type="text" class="form-control" id="txt_PriceUSD" placeholder="Nhập số" role="numeric" data='money' />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch1" />
                                            <label class="onoffswitch-label" for="Usdswitch1">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nội thất</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_Furnitur" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Nhu cầu</label>
                                    <div class="col-sm-3">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="ace" value="CN" name="chkPurpose" />
                                                <span class="lbl">Chuyển nhượng</span>
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="ace" value="CT" name="chkPurpose" />
                                                <span class="lbl">Cho thuê</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="txt_AssetDescription" placeholder="..." rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnProcess">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
    <asp:HiddenField ID="HID_RemindKey" runat="server" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" />
    <asp:HiddenField ID="HID_EmployeeKey" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/SAL/ProductView.js"></script>
</asp:Content>
