﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ProductView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AssetKey.Value = Request["ID"];
                if (Request["Type"] != null)
                    HID_AssetType.Value = Request["Type"];

                Tools.DropDown_DDL(DDL_Furnitur, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_StatusAsset, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);

                LoadAsset();
                CheckRoles();
            }
        }
        void LoadAsset()
        {
            StringBuilder Owner = new StringBuilder();
            StringBuilder zSb = new StringBuilder();
            Product_Info zAsset = new Product_Info(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            HID_ProjectKey.Value = zAsset.ItemAsset.ProjectKey;
            HID_EmployeeKey.Value = zAsset.ItemAsset.EmployeeKey;

            LoadOwner(zAsset.ItemAsset.EmployeeKey, zAsset.ItemAsset.EmployeeName);
            LoadData(zAsset.ItemAsset);
            LoadFile(HID_AssetKey.Value.ToInt());

            txt_Asset.Text = zAsset.ItemAsset.AssetID;
            txt_WebTitle.Text = zAsset.ItemAsset.WebTitle;
            txt_WebPublish.Text = zAsset.ItemAsset.WebPublished.ToInt() == 0 ? "Chưa đăng tin" : "Đã đăng tin";
            txt_WebContent.Text = zAsset.ItemAsset.WebContent;

            #region [html view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "  <input name='switch-field-1' class='ace ace-switch' type='checkbox' id='chkPriceType' />";
            ViewMoney += "  <span class='lbl'>&nbsp USD</span>";
            ViewMoney += "  </div>";
            #endregion

            #region [Asset Info]
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang' value>" + zAsset.ItemAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe' value>" + zAsset.ItemAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán</div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giaban1' value>" + zAsset.ItemAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giaban2' value>" + zAsset.ItemAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê</div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giathue1' value>" + zAsset.ItemAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giathue2' value>" + zAsset.ItemAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua</div>");
            zSb.AppendLine("        <div class='profile-info-value'><span class=giatien id='giamua' value>" + zAsset.ItemAsset.PricePurchase + " (VND)</span></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat' value>" + zAsset.ItemAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau' value>" + zAsset.ItemAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");

            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
            zSb.AppendLine("    </div>");

            //int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            //if (UnitLevel <= 1)
            //{
            //    zSb.AppendLine("    <div class='profile-info-row'>");
            //    zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            //    zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
            //    zSb.AppendLine("    </div>");
            //}
            //else if (zAsset.ItemAsset.EmployeeKey == HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"])
            //{
            //    zSb.AppendLine("    <div class='profile-info-row'>");
            //    zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            //    zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
            //    zSb.AppendLine("    </div>");
            //}

            if (!CheckViewPhone(
                zAsset.ItemAsset.EmployeeKey.ToInt(),
                zAsset.ItemAsset.AssetKey.ToInt(),
                zAsset.ItemAsset.AssetType))
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales' value>" + zAsset.ItemAsset.EmployeeName + "</div>");
                zSb.AppendLine("    </div>");
            }
            else
            {
                Owner.AppendLine("<div class='profile-user-info profile-user-info-striped' id='chunha1'>");
                Owner.AppendLine("<div class='profile-info-row'>");
                Owner.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                Owner.AppendLine("    <div class='profile-info-value' value>" + zAsset.ItemAsset.CustomerName + "</div>");
                Owner.AppendLine("</div>");
                Owner.AppendLine("<div class='profile-info-row'>");
                Owner.AppendLine("    <div class='profile-info-name'>Số điện thoại</div>");
                Owner.AppendLine("    <div class='profile-info-value' value>" + zAsset.ItemAsset.Phone1.HtmlPhone() + zAsset.ItemAsset.Phone2.HtmlPhone() + "</div>");
                Owner.AppendLine("</div>");
                Owner.AppendLine("</div>");

            }

            zSb.AppendLine("</div>");
            Lit_InfoAsset.Text = zSb.ToString();
            Lit_InfoOwner.Text = Owner.ToString();
            #endregion

            #region [Extra Info]
            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Dự án</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='duan' value>" + zAsset.ItemAsset.ProjectName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã sản phẩm</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='masp' value>" + zAsset.ItemAsset.AssetID + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Loại sản phẩm</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='loaisp' value>" + zAsset.ItemAsset.CategoryName + "</div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Số phòng ngủ</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='sophong' value>" + zAsset.ItemAsset.Room + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dttimtuong' value>" + zAsset.ItemAsset.AreaWall + " </div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích thông thủy</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dtthongthuy' value>" + zAsset.ItemAsset.AreaWater + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Hướng view</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='huongview' value>" + zAsset.ItemAsset.View + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Hướng cửa</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='huongcua' value>" + zAsset.ItemAsset.Door + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");

            Lit_ExtraAsset.Text = zSb.ToString();
            #endregion

            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người khởi tạo</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoikhoitao' value>" + zAsset.ItemAsset.CreatedName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày khởi tạo</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaykhoitao' value>" + zAsset.ItemAsset.CreatedDate + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người cập nhật</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoicapnhat' value>" + zAsset.ItemAsset.ModifiedName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày cập nhật</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaycapnhat' value>" + zAsset.ItemAsset.ModifiedDate + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người quản lý</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoiquanly' value>" + zAsset.ItemAsset.EmployeeName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");

            Lit_Info.Text = zSb.ToString();
        }
        void LoadOwner(string EmployeeKey, string EmployeeName)
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Owner_Data.List(HID_AssetType.Value, HID_AssetKey.Value.ToInt());
            string color = "border: 1px solid #7f209c;";
            if (zTable.Rows.Count > 0)
            {
                bool IsAllow = CheckViewPhone(EmployeeKey.ToInt(), HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
                string Phone = "Bạn chưa được <b>" + EmployeeName + "</b> cho xem thông tin này";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    if (i != 0)
                        color = "";

                    DataRow r = zTable.Rows[i];
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' style='" + color + "'>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>" + r["CustomerName"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    if (IsAllow)
                        zSb.AppendLine("    <div class='profile-info-value' value>" + r["Phone1"].HtmlPhone() + r["Phone2"].HtmlPhone() + "</div>");
                    else
                        zSb.AppendLine("    <div class='profile-info-value' value>" + Phone.HtmlPhone() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Email</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>" + r["Email1"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Địa chỉ</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>Liên lạc: " + r["Address1"].ToString() + "<br/> Thường trú: " + r["Address2"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Ngày cập nhật</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>" + r["CreatedDate"].ToDateString() + "</div>");
                    zSb.AppendLine(" </div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='space-6'></div>");
                }
            }
            Lit_Owner.Text = zSb.ToString();
        }
        void LoadFile(int Key)
        {
            List<ItemDocument> zList = Document_Data.List(135, Key);
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine("<li id='" + item.FileKey + "'>");
                    zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                    zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageThumb + "' />");
                    zSb.AppendLine("            <div class='text'>");
                    zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                    zSb.AppendLine("            </div>");
                    zSb.AppendLine("    </a>");
                    zSb.AppendLine("</li>");
                }
            }
            Lit_File.Text = zSb.ToString();
        }
        void LoadData(ItemAsset Item)
        {
            string DDL_Category = Tools.Html_Select("DDL_Category", "Loại SP", "form-control input-sm", false, "SELECT A.CategoryKey, C.CategoryName FROM PUL_Project_Category A LEFT JOIN PUL_Category C ON C.CategoryKey = A.CategoryKey WHERE A.ProjectKey=" + HID_ProjectKey.Value, false);
            string DDL_Purpose = @"
<select name='DDL_Purpose' id='DDL_Purpose' class='form-control input-sm'>
	<option selected='selected' value='0' disabled='disabled'>Nhu cầu</option>
	<option value='CN'>Chuyển nhượng</option>
	<option value='CT'>Cho thuê</option>
	<option value='KT'>Kinh doanh</option>
</select>";
            string txt_Name = "<input type='text' id='txt_Name' class='form-control input-sm' placeholder='Mã căn' />";
            string txt_Bed = @"
<select name='DDL_Room' id='DDL_Room' class='form-control input-sm'>
	<option selected='selected' value='0' disabled='disabled'>PN</option>
	<option value='1'>1 PN</option>
	<option value='2'>2 PN</option>
	<option value='3'>3 PN</option>
    <option value='3'>4 PN</option>
    <option value='3'>5 PN</option>
</select>";
            string btn_Search = "<a href='#' class='btn-block btn btn-sm btn-purple' id='btnSearch'><i class='ace-icon fa fa-search'></i>Lọc</a>";

            Lit_RightTitle.Text = Item.ProjectName;

            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            List<ItemAsset> zList = new List<ItemAsset>();

            zList = Product_Data.Search(Item.ProjectKey.ToInt(), 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, 100);

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblExtraData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th colspan=2>" + txt_Name + "</th>");
            zSb.AppendLine("        <th>" + txt_Bed + "</th>");
            zSb.AppendLine("        <th>" + DDL_Category + "</th>");
            zSb.AppendLine("        <th>" + DDL_Purpose + "</th>");
            zSb.AppendLine("        <th>" + btn_Search + "</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zList.Count > 0)
            {
                int i = 1;
                foreach (ItemAsset r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td class='td2'>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td class='td2'>" + r.Room + "</td>");
                    zSb.AppendLine("                <td class='td2'>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td class='td5'>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_RightTable.Text = zSb.ToString();
        }

        //kiem tra nhac nhở san phẩm
        [WebMethod]
        public static ItemResult CheckReminder(int AssetKey)
        {
            ItemResult zResult = new ItemResult();
            int Exists = Product_Data.CheckReminder(AssetKey);
            if (Exists > 0)
            {
                zResult.Message = "Sản phẩm này đã đến hẹn, chưa được xử lý, vui lòng kiểm tra ngày liên hệ !";
                zResult.Result = Exists.ToString();
            }
            return zResult;
        }
        [WebMethod]
        public static ItemResult ExeReminder(int RemindKey)
        {
            ItemResult zResult = new ItemResult();
            Notification_Info zInfo = new Notification_Info();
            zInfo.ID = RemindKey;
            zInfo.IsRead = 1;
            zInfo.ReadDate = DateTime.Now;
            zInfo.UpdateReaded();
            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
                zResult.Result = "0";
            }
            else
            {
                zResult.Message = "Đã xử lý thành công !";
                zResult.Result = "1";
            }
            return zResult;
        }

        //get 1 thong tin khi click ben phai
        [WebMethod]
        public static ItemAsset GetAsset(int AssetKey, string Type)
        {
            string CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            if (UnitLevel <= 1)
                zInfo.ItemAsset.Edit = "1";
            else if (zInfo.ItemAsset.EmployeeKey == CurrentAgent)
                zInfo.ItemAsset.Edit = "1";

            return zInfo.ItemAsset;
        }
        [WebMethod]
        public static ItemResult GetOwner(int AssetKey, string Type, int EmployeeKey, string EmployeeName)
        {
            ItemResult Result = new ItemResult();
            StringBuilder zSb = new StringBuilder();
            StringBuilder zTop = new StringBuilder();
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            if (zTable.Rows.Count > 0)
            {
                bool IsAllow = CheckViewPhone(EmployeeKey, AssetKey, Type);
                string Phone = "Bạn chưa được <b>" + EmployeeName + "</b> cho xem thông tin này";
                string color = "border: 1px solid #7f209c;";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    if (i != 0)
                        color = "";
                    DataRow r = zTable.Rows[i];
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' style='" + color + "'>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    if (IsAllow)
                        zSb.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                    else
                        zSb.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Email</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["Email1"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Địa chỉ</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>Liên lạc: " + r["Address1"].ToString() + "<br/> Thường trú: " + r["Address2"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Ngày cập nhật</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CreatedDate"].ToDateString() + "</div>");
                    zSb.AppendLine(" </div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='space-6'></div>");

                    #region [get 1 chu nha]
                    if (i == 0)
                    {
                        zTop.AppendLine("<div class='profile-info-row'>");
                        zTop.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                        zTop.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                        zTop.AppendLine("</div>");
                        zTop.AppendLine("<div class='profile-info-row'>");
                        zTop.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                        if (IsAllow)
                            zTop.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                        else
                            zTop.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                        zTop.AppendLine("</div>");
                    }
                    #endregion
                }

                Result.Result = zSb.ToString();
                Result.Result2 = zTop.ToString();
            }

            return Result;
        }
        [WebMethod]
        public static string GetFile(int AssetKey)
        {
            List<ItemDocument> zList = Document_Data.List(135, AssetKey);
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine("<li id='" + item.FileKey + "'>");
                    zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                    zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageThumb + "' />");
                    zSb.AppendLine("            <div class='text'>");
                    zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                    zSb.AppendLine("            </div>");
                    zSb.AppendLine("    </a>");
                    zSb.AppendLine("</li>");
                }
            }
            return zSb.ToString();
        }

        //search
        [WebMethod(EnableSession = true)]
        public static string SearchAsset(int Project, string AssetID, string Room, string Category, string Purpose)
        {
            List<ItemAsset> zList = new List<ItemAsset>();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            zList = Product_Data.Search(Project, Category.ToInt(), string.Empty, string.Empty, string.Empty, Room, AssetID, string.Empty, Purpose, 0, 0);
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (ItemAsset r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }

        //kiem tra user hien tai co duoc phep xem so dt cua thong tin sản phẩm dang xem
        static bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            if (UnitLevel == 0 || UnitLevel == 1 || CurrentAgent == OwnerAgent)
            {
                return true;
            }

            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }
        //loc lay gia va nhu cau
        static string GetPrice(ItemAsset Item)
        {
            string PurposeShow = "";
            if (Item.Purpose.Contains(","))
            {
                string[] temp = Item.Purpose.Split(',');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "Chuyển nhượng":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                                break;

                            case "Cho thuê":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                                break;
                        }
                    }
                }
            }
            else
            {
                string s = Item.Purpose;
                switch (s.Trim())
                {
                    case "Chuyển nhượng":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                        break;

                    case "Cho thuê":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                        break;
                }
            }

            return PurposeShow;
        }

        [WebMethod]
        public static ItemAsset GetAssetEdit(int AssetKey, string Type)
        {
            Product_Info zAsset = new Product_Info(AssetKey, "ResaleApartment");
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }

        [WebMethod]
        public static ItemResult SaveProduct(
          int AssetKey, string AssetStatus, string AssetRemind,
          string AssetPrice_VND, string AssetPriceRent_VND,
          string AssetPrice_USD, string AssetPriceRent_USD,
          string AssetFurnitur, string AssetPurpose, string AssetDescription)
        {
            string Type = "ResaleApartment";
            ItemResult zResult = new ItemResult();
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            zInfo.ItemAsset.Price_USD = AssetPrice_USD;
            zInfo.ItemAsset.PriceRent_USD = AssetPriceRent_USD;
            zInfo.ItemAsset.Price_VND = AssetPrice_VND;
            zInfo.ItemAsset.PriceRent_VND = AssetPriceRent_VND;
            zInfo.ItemAsset.FurnitureKey = AssetFurnitur;
            zInfo.ItemAsset.DateContractEnd = AssetRemind;
            zInfo.ItemAsset.StatusKey = AssetStatus;
            zInfo.ItemAsset.PurposeKey = AssetPurpose;
            zInfo.ItemAsset.Description = AssetDescription.Trim();
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zInfo.ItemAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }

            zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save(Type);
            return zResult;
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Lit_Button.Text = @"
                        <a href='#' class='btn btn-white btn-warning btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil orange'></i>
                            Điều chỉnh
                        </a>";
                    break;

                default:
                    if (_Permitsion[2].ToInt() == 1 &&
                        CurrentEmployee == HID_EmployeeKey.Value.ToInt())
                    {
                        Lit_Button.Text = @"
                        <a href='#' class='btn btn-white btn-warning btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil orange'></i>
                            Điều chỉnh
                        </a>";
                    }
                    break;
            }
        }
        #endregion
    }
}