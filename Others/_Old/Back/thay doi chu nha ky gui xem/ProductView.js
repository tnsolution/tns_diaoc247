﻿var _usd = 0, _assetkey = 0;       //tinh gia usd
$(function () {
    $("[id$=txt_PriceRentUSD]").hide();
    $("[id$=txt_PriceUSD]").hide();
    $("[id$=Usdswitch1]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PriceRentUSD]").show();
            $("[id$=txt_PriceUSD]").show();
            $("[id$=txt_PriceRentVND]").hide();
            $("[id$=txt_PriceVND]").hide();
            $("#giathue").text("Giá thuê USD");
            $("#giaban").text("Giá bán USD");
            _usd = 1;
        }
        else {
            $("[id$=txt_PriceRentUSD]").hide();
            $("[id$=txt_PriceUSD]").hide();
            $("[id$=txt_PriceRentVND]").show();
            $("[id$=txt_PriceVND]").show();
            $("#giathue").text("Giá thuê VNĐ");
            $("#giaban").text("Giá bán VNĐ");
            _usd = 0;
        }
    });
    $("[id$=txt_PriceUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_PriceRentUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_PriceVND]").blur(function () { ConvertMoney(); });
    $("[id$=txt_PriceRentVND]").blur(function () { ConvertMoney(); });
    $("div[view='usd']").hide();
    $("div[view='vnd']").show();
    $("#chkPriceType").change(function () {
        if ($(this).is(':checked')) {
            $("div[view='usd']").show();
            $("div[view='vnd']").hide();
        }
        else {
            $("div[view='vnd']").show();
            $("div[view='usd']").hide();
        }
    });
    //------------------------------------------------------------------------------------End Action USD
    $("#btnExeReminder").hide();
    $("#btnExeReminder").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/ExeReminder",
            data: JSON.stringify({
                "RemindKey": $("[id$=HID_RemindKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Result == 1) {
                    Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                    $("#btnExeReminder").hide();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#tblExtraData tbody").on("click", "tr", function () {
        var trid = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('fid');

        //load data on click
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/GetAsset",
            data: JSON.stringify({
                "AssetKey": trid,
                "Type": type
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $("#leftbox div[value]").html("");

                var giaban1 = msg.d.Price_USD + " (USD) ";
                var giaban2 = msg.d.Price_VND + " (VNĐ)"
                var giathue1 = msg.d.PriceRent_USD + " (USD)";
                var giathue2 = msg.d.PriceRent_VND + " (VNĐ)"
                var giamua = msg.d.PricePurchase + " (VNĐ)";

                $("#tieudetrang").text(msg.d.AssetID);
                $("#tinhtrang").text(msg.d.Status);

                $("#giaban1").text(giaban1);
                $("#giathue1").text(giathue1);
                $("#giaban2").text(giaban2);
                $("#giathue2").text(giathue2);

                $("#giamua").text(giamua);
                $("#noithat").text(msg.d.Furniture);
                $("#nhucau").text(msg.d.Purpose);
                $("#ghichu").text(msg.d.Description);

                //var cString = getCookie("UserLog");
                //var val = getCookieValue(cString, "UnitLevel");
                //var emp = getCookieValue(cString, "EmployeeKey");
                //if (val <= 1) {                  
                //    $("#ghichu").text(msg.d.Description);
                //} else {
                //    if (msg.d.EmployeeKey == emp)
                //        $("#ghichu").text(msg.d.Description);
                //}

                $("#duan").text(msg.d.ProjectName);
                $("#masp").text(msg.d.AssetID);
                $("#loaisp").text(msg.d.CategoryName);
                $("#sophong").text(msg.d.Room);

                $("#ngaylienhe").text(msg.d.DateContractEnd);
                $("#dttimtuong").text(msg.d.AreaWall);
                $("#dtthongthuy").text(msg.d.AreaWater);
                $("#huongview").text(msg.d.View);
                $("#huongcua").text(msg.d.Door);

                $("#tieude").text(msg.d.WebTitle);
                $("#xuatban").text(msg.d.WebPublished);
                $("#noidung").text(msg.d.WebContent);

                $("#nguoikhoitao").text(msg.d.CreatedName);
                $("#ngaykhoitao").text(msg.d.CreatedDate);
                $("#nguoicapnhat").text(msg.d.ModifiedName);
                $("#ngaycapnhat").text(msg.d.ModifiedDate);
                $("#nguoiquanly").text(msg.d.EmployeeName);

                var employeename = msg.d.EmployeeName;
                if (employeename == null)
                    employeename = "";
                var employeekey = msg.d.EmployeeKey;
                if (employeekey == null)
                    employeekey = 0;

                setTimeout(function () {
                    //load chu nha on click
                    $.ajax({
                        type: "POST",
                        url: "/SAL/ProductView.aspx/GetOwner",
                        data: JSON.stringify({
                            "AssetKey": trid,
                            "Type": type,
                            "EmployeeKey": employeekey,
                            "EmployeeName": employeename,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                        },
                        success: function (msg) {
                            $("#chunha").empty();
                            $("#chunha").append($(msg.d.Result));
                            $("#chunha1").empty();
                            $("#chunha1").append($(msg.d.Result2));
                        },
                        complete: function () {
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }, 1000);

                if (msg.d.Edit == 0) {
                    $("#btnEdit").hide();
                }
                else {
                    $("#btnEdit").show();
                }
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.se-pre-con').fadeOut('slow');
                Page.showNotiMessageError("Lỗi,", "Vui lòng liên hệ Admin");
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        //load file on click
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/GetFile",
            data: JSON.stringify({
                "AssetKey": trid,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                $("#hinhanh").empty();
                $("#hinhanh").append($(msg.d));
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        //kiem tra ngay lien he on click
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/CheckReminder",
            data: JSON.stringify({
                "AssetKey": trid,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    $("[id$=HID_RemindKey]").val(msg.d.Result);
                    Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                    $("#btnExeReminder").show();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        $("[id$=HID_AssetKey]").val(trid);
        $("[id$=HID_AssetType]").val(type);
    });
    $("#btnSearch").click(function () {
        var Category = $("[id$=DDL_Category]").val();
        var Purpose = $("[id$=DDL_Purpose]").val();
        var Room = $("[id$=DDL_Room]").val();
        if (Category == null)
            Category = 0;
        if (Purpose == null)
            Purpose = "";
        if (Room == null)
            Room = "";

        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/SearchAsset",
            data: JSON.stringify({
                "Project": $("[id$=HID_ProjectKey]").val(),
                "AssetID": $("#txt_Name").val(),
                "Room": Room,
                "Category": Category,
                "Purpose": Purpose,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $('#tblExtraData tbody').empty();
                $('#tblExtraData tbody').append($(msg.d));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnEdit").click(function () {
        $('.se-pre-con').fadeIn('slow');
        var link = '/SAL/ProductEdit.aspx?ID=' + $("[id$=HID_AssetKey]").val() + '&Type=' + $("[id$=HID_AssetType]").val();
        window.location = link;
    });
    $("#btnProcess").click(function () {
        var Purpose = "";
        $("input[name='chkPurpose']:checked").each(function (i) {
            Purpose += $(this).val() + "/ ";
        });

        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/SaveProduct",
            data: JSON.stringify({
                "AssetKey": _assetkey,
                "AssetStatus": $("[id$=DDL_StatusAsset]").val(),
                "AssetRemind": $("[id$=txt_DateContractEnd]").val(),
                "AssetPrice_VND": $("[id$=txt_PriceVND]").val(),
                "AssetPriceRent_VND": $("[id$=txt_PriceRentVND]").val(),
                "AssetPrice_USD": $("[id$=txt_PriceUSD]").val(),
                "AssetPriceRent_USD": $("[id$=txt_PriceRentUSD]").val(),
                "AssetFurnitur": $("[id$=DDL_Furnitur]").val(),
                "AssetPurpose": Purpose,
                "AssetDescription": $("[id$=txt_AssetDescription]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message == "") {
                    Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.se-pre-con').fadeOut('slow');
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        $("#mProcess").modal("hide");
    });
    $("#update").click(function () {
        $("#mProcess").modal({
            backdrop: true,
            show: true
        });        
        var id = $("[id$=HID_AssetKey]").val();
        console.log(id);
        UpdateAsset(id);
    });
    //------------------------------------------------------------------------------------End Action button
    var x = $('#leftbox').height();
    var x = x - 75;
    $(".select2").select2({ width: "100%" });
    $('#scroll').ace_scroll({
        size: x,
    });
    $("#tblExtraData").tableHeadFixer();
    //mo rong tat ca accordion
    //$('#accordion .panel-collapse').collapse('show');
    $(".toggle-accordion").on("click", function () {
        $(this).toggleClass("active");
        $(this).text($(this).text() == 'Mở rộng' ? 'Thu gọn' : 'Mở rộng');
        if ($(this).hasClass("active"))
            $('#accordion .panel-collapse').collapse('hide');
        else
            $('#accordion .panel-collapse').collapse('show');
    });
    $(".iframe").colorbox({ iframe: true, width: "100%", height: "100%" });
    //------------------------------------------------------------------------------------End Lay Out
  
    //kiem tra khi load
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/CheckReminder",
        data: JSON.stringify({
            "AssetKey": Page.getUrlParameter("ID"),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                $("[id$=HID_RemindKey]").val(msg.d.Result);
                Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                $("#btnExeReminder").show();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
});

function UpdateAsset(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/GetAssetEdit",
        data: JSON.stringify({
            "AssetKey": id,
            "Type": ""
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            _assetkey = msg.d.AssetKey;
            $("[id$=txt_DateContractEnd]").val(msg.d.DateContractEnd);
            $("[id$=txt_PriceRentVND]").val(msg.d.PriceRent_VND);
            $("[id$=txt_PriceRentUSD]").val(msg.d.PriceRent_USD);
            $("[id$=txt_PriceVND]").val(msg.d.Price_VND);
            $("[id$=txt_PriceUSD]").val(msg.d.Price_USD);
            $("[id$=txt_AssetDescription]").val(msg.d.Description);
            $("[id$=DDL_Furnitur]").val(msg.d.FurnitureKey).trigger("change");
            $("[id$=DDL_StatusAsset]").val(msg.d.StatusKey).trigger("change");
            var SetPurpose = msg.d.PurposeKey.split("/");
            for (var i in SetPurpose) {
                var val = $.trim(SetPurpose[i]);
                $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
            }

            $("#mProcess").modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function ConvertMoney() {
    if (_usd == 1) {
        var priceUsd = $("[id$=txt_PriceUSD]").val();
        var priceRentUsd = $("[id$=txt_PriceRentUSD]").val();
        var priceVnd = Page.RemoveComma(priceUsd) * usdRate;
        var priceRentVnd = Page.RemoveComma(priceRentUsd) * usdRate;

        $("[id$=txt_PriceRentVND]").val(Page.FormatMoney(priceRentVnd));
        $("[id$=txt_PriceVND]").val(Page.FormatMoney(priceVnd));
    }
    else {
        var priceVnd = $("[id$=txt_PriceVND]").val();
        var priceRentVnd = $("[id$=txt_PriceRentVND]").val();
        var priceUsd = Page.RemoveComma(priceVnd) / usdRate;
        var priceRentUsd = Page.RemoveComma(priceRentVnd) / usdRate;

        $("[id$=txt_PriceRentUSD]").val(Page.FormatMoney(priceRentUsd));
        $("[id$=txt_PriceUSD]").val(Page.FormatMoney(priceUsd));
    }
}

//function getCookie(cname) {
//    var name = cname + "=";
//    var decodedCookie = decodeURIComponent(document.cookie);
//    var ca = decodedCookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(name) == 0) {
//            return c.substring(name.length, c.length);
//        }
//    }
//    return "";
//}
//function getCookieValue(cname, cval) {
//    var ca = cname.split('&');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(cval) == 0) {
//            return c.substring(cval.length, c.length).replace("=", "");
//        }
//    }
//}