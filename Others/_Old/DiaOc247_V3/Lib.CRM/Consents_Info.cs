﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.CRM
{
    public class Consents_Info
    {
        #region [ Field Name ]
        private int _RecordKey = 0;
        private int _CustomerKey = 0;
        private string _Note = "";
        private double _Price = 0;
        private string _CategoryConsent = "";
        private string _CategoryNeed = "";
        private string _CategoryInside = "";
        private string _CategoryAsset = "";
        private string _Project = "";
        private string _ProjectOther = "";
        private string _ConsentBedRoom = "";
        private string _ConsentAera = "0";
        private string _ConsentDirection = "";
        private string _ConsentView = "";
        private string _ConsentFloor = "";
        private string _Message = "";
        private string _Name_Consent = "";
        private string _Name_Need = "";
        private string _Name_Inside = "";
        private string _Name_Asset = "";
        private string _Name_Project = "";
        #endregion
        #region [ Properties ]
        public int RecordKey
        {
            get { return _RecordKey; }
            set { _RecordKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CategoryConsent
        {
            get { return _CategoryConsent; }
            set { _CategoryConsent = value; }
        }
        public string CategoryNeed
        {
            get { return _CategoryNeed; }
            set { _CategoryNeed = value; }
        }
        public string CategoryInside
        {
            get { return _CategoryInside; }
            set { _CategoryInside = value; }
        }
        public string CategoryAsset
        {
            get { return _CategoryAsset; }
            set { _CategoryAsset = value; }
        }
        public string Project
        {
            get { return _Project; }
            set { _Project = value; }
        }
        public string ProjectOther
        {
            get { return _ProjectOther; }
            set { _ProjectOther = value; }
        }
        public string ConsentBedRoom
        {
            get { return _ConsentBedRoom; }
            set { _ConsentBedRoom = value; }
        }
        public string ConsentAera
        {
            get { return _ConsentAera; }
            set { _ConsentAera = value; }
        }
        public string ConsentDirection
        {
            get { return _ConsentDirection; }
            set { _ConsentDirection = value; }
        }
        public string ConsentView
        {
            get { return _ConsentView; }
            set { _ConsentView = value; }
        }
        public string ConsentFloor
        {
            get { return _ConsentFloor; }
            set { _ConsentFloor = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string Name_Consent
        {
            get
            {
                return _Name_Consent;
            }

            set
            {
                _Name_Consent = value;
            }
        }

        public string Name_Need
        {
            get
            {
                return _Name_Need;
            }

            set
            {
                _Name_Need = value;
            }
        }

        public string Name_Inside
        {
            get
            {
                return _Name_Inside;
            }

            set
            {
                _Name_Inside = value;
            }
        }

        public string Name_Asset
        {
            get
            {
                return _Name_Asset;
            }

            set
            {
                _Name_Asset = value;
            }
        }

        public string Name_Project
        {
            get
            {
                return _Name_Project;
            }

            set
            {
                _Name_Project = value;
            }
        }

        public double Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }

        public string Note
        {
            get
            {
                return _Note;
            }

            set
            {
                _Note = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Consents_Info()
        {
        }
        public Consents_Info(int RecordKey)
        {
            string zSQL = "SELECT * FROM CRM_Customer_Consents WHERE RecordKey = @RecordKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RecordKey", SqlDbType.NVarChar).Value = RecordKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Note = zReader["Note"].ToString();
                    if (zReader["RecordKey"] != DBNull.Value)
                        _RecordKey = int.Parse(zReader["RecordKey"].ToString());

                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());

                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());

                    _CategoryConsent = zReader["CategoryConsent"].ToString();
                    _CategoryNeed = zReader["CategoryNeed"].ToString();
                    _CategoryInside = zReader["CategoryInside"].ToString();
                    _CategoryAsset = zReader["CategoryAsset"].ToString();
                    _Project = zReader["Project"].ToString();
                    _ProjectOther = zReader["ProjectOther"].ToString();
                    _ConsentBedRoom = zReader["ConsentBedRoom"].ToString();
                    _ConsentAera = zReader["ConsentAera"].ToString();
                    _ConsentDirection = zReader["ConsentDirection"].ToString();
                    _ConsentView = zReader["ConsentView"].ToString();
                    _ConsentFloor = zReader["ConsentFloor"].ToString();

                    _Name_Consent = zReader["Name_Consent"].ToString();
                    _Name_Need = zReader["Name_Need"].ToString();
                    _Name_Inside = zReader["Name_Inside"].ToString();
                    _Name_Asset = zReader["Name_Asset"].ToString();
                    _Name_Project = zReader["Name_Project"].ToString();
                }

                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Consents_Info(int CustomerKey, bool Option2)
        {
            string zSQL = "SELECT * FROM CRM_Customer_Consents WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Note = zReader["Note"].ToString();
                    if (zReader["RecordKey"] != DBNull.Value)
                        _RecordKey = int.Parse(zReader["RecordKey"].ToString());

                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());

                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());

                    _CategoryConsent = zReader["CategoryConsent"].ToString();
                    _CategoryNeed = zReader["CategoryNeed"].ToString();
                    _CategoryInside = zReader["CategoryInside"].ToString();
                    _CategoryAsset = zReader["CategoryAsset"].ToString();
                    _Project = zReader["Project"].ToString();
                    _ProjectOther = zReader["ProjectOther"].ToString();
                    _ConsentBedRoom = zReader["ConsentBedRoom"].ToString();
                    _ConsentAera = zReader["ConsentAera"].ToString();
                    _ConsentDirection = zReader["ConsentDirection"].ToString();
                    _ConsentView = zReader["ConsentView"].ToString();
                    _ConsentFloor = zReader["ConsentFloor"].ToString();

                    _Name_Consent = zReader["Name_Consent"].ToString();
                    _Name_Need = zReader["Name_Need"].ToString();
                    _Name_Inside = zReader["Name_Inside"].ToString();
                    _Name_Asset = zReader["Name_Asset"].ToString();
                    _Name_Project = zReader["Name_Project"].ToString();
                }

                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Customer_Consents ("
        + "Note, Price, CustomerKey ,CategoryConsent ,CategoryNeed ,CategoryInside ,CategoryAsset ,Project ,ProjectOther ,ConsentBedRoom ,ConsentAera ,ConsentDirection ,ConsentView ,ConsentFloor, Name_Consent,Name_Need,Name_Inside,Name_Asset,Name_Project,CreatedDate,ModifiedDate ) "
         + " VALUES ( "
         + "@Note, @Price, @CustomerKey ,@CategoryConsent ,@CategoryNeed ,@CategoryInside ,@CategoryAsset ,@Project ,@ProjectOther ,@ConsentBedRoom ,@ConsentAera ,@ConsentDirection ,@ConsentView ,@ConsentFloor, @Name_Consent, @Name_Need, @Name_Inside, @Name_Asset, @Name_Project, GetDate(),GetDate() ) "
         + " SELECT RecordKey FROM CRM_Customer_Consents WHERE RecordKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;

                zCommand.Parameters.Add("@RecordKey", SqlDbType.Int).Value = _RecordKey;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CategoryConsent", SqlDbType.NVarChar).Value = _CategoryConsent;
                zCommand.Parameters.Add("@CategoryNeed", SqlDbType.NVarChar).Value = _CategoryNeed;
                zCommand.Parameters.Add("@CategoryInside", SqlDbType.NVarChar).Value = _CategoryInside;
                zCommand.Parameters.Add("@CategoryAsset", SqlDbType.NVarChar).Value = _CategoryAsset;
                zCommand.Parameters.Add("@Project", SqlDbType.NVarChar).Value = _Project;
                zCommand.Parameters.Add("@ProjectOther", SqlDbType.NVarChar).Value = _ProjectOther;
                zCommand.Parameters.Add("@ConsentBedRoom", SqlDbType.NVarChar).Value = _ConsentBedRoom;
                zCommand.Parameters.Add("@ConsentAera", SqlDbType.NVarChar).Value = _ConsentAera;
                zCommand.Parameters.Add("@ConsentDirection", SqlDbType.NVarChar).Value = _ConsentDirection;
                zCommand.Parameters.Add("@ConsentView", SqlDbType.NVarChar).Value = _ConsentView;
                zCommand.Parameters.Add("@ConsentFloor", SqlDbType.NVarChar).Value = _ConsentFloor;

                zCommand.Parameters.Add("@Name_Consent", SqlDbType.NVarChar).Value = _Name_Consent;
                zCommand.Parameters.Add("@Name_Need", SqlDbType.NVarChar).Value = _Name_Need;
                zCommand.Parameters.Add("@Name_Inside", SqlDbType.NVarChar).Value = _Name_Inside;
                zCommand.Parameters.Add("@Name_Asset", SqlDbType.NVarChar).Value = _Name_Asset;
                zCommand.Parameters.Add("@Name_Project", SqlDbType.NVarChar).Value = _Name_Project;

                _RecordKey = int.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE CRM_Customer_Consents SET Note =@Note, Price = @Price, ModifiedDate = GETDATE(),"                      
                        + " CategoryConsent = @CategoryConsent,"
                        + " CategoryNeed = @CategoryNeed,"
                        + " CategoryInside = @CategoryInside,"
                        + " CategoryAsset = @CategoryAsset,"
                        + " Project = @Project,"
                        + " ProjectOther = @ProjectOther,"
                        + " ConsentBedRoom = @ConsentBedRoom,"
                        + " ConsentAera = @ConsentAera,"
                        + " ConsentDirection = @ConsentDirection,"
                        + " ConsentView = @ConsentView,"
                        + " ConsentFloor = @ConsentFloor, Name_Consent =@Name_Consent, Name_Need=@Name_Need, Name_Inside=@Name_Inside, Name_Asset=@Name_Asset, Name_Project=@Name_Project"
                       + " WHERE RecordKey = @RecordKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@RecordKey", SqlDbType.Int).Value = _RecordKey;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;             
                zCommand.Parameters.Add("@CategoryConsent", SqlDbType.NVarChar).Value = _CategoryConsent;
                zCommand.Parameters.Add("@CategoryNeed", SqlDbType.NVarChar).Value = _CategoryNeed;
                zCommand.Parameters.Add("@CategoryInside", SqlDbType.NVarChar).Value = _CategoryInside;
                zCommand.Parameters.Add("@CategoryAsset", SqlDbType.NVarChar).Value = _CategoryAsset;
                zCommand.Parameters.Add("@Project", SqlDbType.NVarChar).Value = _Project;
                zCommand.Parameters.Add("@ProjectOther", SqlDbType.NVarChar).Value = _ProjectOther;
                zCommand.Parameters.Add("@ConsentBedRoom", SqlDbType.NVarChar).Value = _ConsentBedRoom;
                zCommand.Parameters.Add("@ConsentAera", SqlDbType.NVarChar).Value = _ConsentAera;
                zCommand.Parameters.Add("@ConsentDirection", SqlDbType.NVarChar).Value = _ConsentDirection;
                zCommand.Parameters.Add("@ConsentView", SqlDbType.NVarChar).Value = _ConsentView;
                zCommand.Parameters.Add("@ConsentFloor", SqlDbType.NVarChar).Value = _ConsentFloor;

                zCommand.Parameters.Add("@Name_Consent", SqlDbType.NVarChar).Value = _Name_Consent;
                zCommand.Parameters.Add("@Name_Need", SqlDbType.NVarChar).Value = _Name_Need;
                zCommand.Parameters.Add("@Name_Inside", SqlDbType.NVarChar).Value = _Name_Inside;
                zCommand.Parameters.Add("@Name_Asset", SqlDbType.NVarChar).Value = _Name_Asset;
                zCommand.Parameters.Add("@Name_Project", SqlDbType.NVarChar).Value = _Name_Project;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_RecordKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_Customer_Consents WHERE RecordKey = @RecordKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RecordKey", SqlDbType.Int).Value = _RecordKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update(int CustomerKey)
        {
            string zSQL = "UPDATE CRM_Customer_Consents SET Price =@Price,"
                        + " CategoryConsent = @CategoryConsent,"
                        + " CategoryNeed = @CategoryNeed,"
                        + " CategoryInside = @CategoryInside,"
                        + " CategoryAsset = @CategoryAsset,"
                        + " Project = @Project,"
                        + " ProjectOther = @ProjectOther,"
                        + " ConsentBedRoom = @ConsentBedRoom,"
                        + " ConsentAera = @ConsentAera,"
                        + " ConsentDirection = @ConsentDirection,"
                        + " ConsentView = @ConsentView,"
                        + " ConsentFloor = @ConsentFloor, Name_Consent= @Name_Consent, Name_Need=@Name_Need, Name_Inside = @Name_Inside, Name_Asset =@Name_Asset, Name_Project =@Name_Project"
                       + " WHERE CustomerKey = @CustomerKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@CategoryConsent", SqlDbType.NVarChar).Value = _CategoryConsent;
                zCommand.Parameters.Add("@CategoryNeed", SqlDbType.NVarChar).Value = _CategoryNeed;
                zCommand.Parameters.Add("@CategoryInside", SqlDbType.NVarChar).Value = _CategoryInside;
                zCommand.Parameters.Add("@CategoryAsset", SqlDbType.NVarChar).Value = _CategoryAsset;
                zCommand.Parameters.Add("@Project", SqlDbType.NVarChar).Value = _Project;
                zCommand.Parameters.Add("@ProjectOther", SqlDbType.NVarChar).Value = _ProjectOther;
                zCommand.Parameters.Add("@ConsentBedRoom", SqlDbType.NVarChar).Value = _ConsentBedRoom;
                zCommand.Parameters.Add("@ConsentAera", SqlDbType.NVarChar).Value = _ConsentAera;
                zCommand.Parameters.Add("@ConsentDirection", SqlDbType.NVarChar).Value = _ConsentDirection;
                zCommand.Parameters.Add("@ConsentView", SqlDbType.NVarChar).Value = _ConsentView;
                zCommand.Parameters.Add("@ConsentFloor", SqlDbType.NVarChar).Value = _ConsentFloor;

                zCommand.Parameters.Add("@Name_Consent", SqlDbType.NVarChar).Value = _Name_Consent;
                zCommand.Parameters.Add("@Name_Need", SqlDbType.NVarChar).Value = _Name_Need;
                zCommand.Parameters.Add("@Name_Inside", SqlDbType.NVarChar).Value = _Name_Inside;
                zCommand.Parameters.Add("@Name_Asset", SqlDbType.NVarChar).Value = _Name_Asset;
                zCommand.Parameters.Add("@Name_Project", SqlDbType.NVarChar).Value = _Name_Project;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
