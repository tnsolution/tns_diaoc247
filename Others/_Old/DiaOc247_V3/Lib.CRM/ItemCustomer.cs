﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.CRM
{
    public class ItemCustomer
    {
        string _SourceKey = "0";
        string _SourceName = "";
        string _SourceNote = "";
        string _WantSearch = "";
        string _Description = "";
        string _HasTrade = "0";
        string _Type = "";
        string _StatusKey = "0";
        string _AssetKey = "0";
        string _OwnerKey = "0";
        string _CategoryKey = "0";
        string _CustomerKey = "0";
        string _CustomerID = "";
        string _CustomerName = "";
        string _Birthday = "";
        string _ProjectKey = "";
        string _ProjectName = "";
        string _ConsentKey = "";
        string _Consent = "";
        string _WantShow = "";
        string _Phone1 = "";
        string _Phone2 = "";
        string _Email1 = "";
        string _Email2 = "";
        string _Owner = "";
        string _IsOwner = "";
        string _Address1 = "";
        string _Address2 = "";
        string _Address3 = "";
        string _CardDate = "";
        string _CardPlace = "";
        string _Status = "";
        string _CardID = "";
        string _CompanyName = "";
        string _Tax = "";
        string _Room = "";
        string _CategoryName = "";
        string _CreatedDate = "";
        string _CreatedName = "";
        string _ModifiedDate = "";
        string _ModifiedName = "";
        string _Message = "";

        public string ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }

            set
            {
                _ModifiedDate = value;
            }
        }

        public string Phone2
        {
            get
            {
                return _Phone2;
            }

            set
            {
                _Phone2 = value;
            }
        }

        public string Phone1
        {
            get
            {
                return _Phone1;
            }

            set
            {
                _Phone1 = value;
            }
        }

        public string Consent
        {
            get
            {
                return _Consent;
            }

            set
            {
                _Consent = value;
            }
        }

        public string ConsentKey
        {
            get
            {
                return _ConsentKey;
            }

            set
            {
                _ConsentKey = value;
            }
        }

        public string ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }

        public string Birthday
        {
            get
            {
                return _Birthday;
            }

            set
            {
                _Birthday = value;
            }
        }

        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }

        public string CustomerID
        {
            get
            {
                return _CustomerID;
            }

            set
            {
                _CustomerID = value;
            }
        }

        public string CustomerKey
        {
            get
            {
                return _CustomerKey;
            }

            set
            {
                _CustomerKey = value;
            }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string Email1
        {
            get
            {
                return _Email1;
            }

            set
            {
                _Email1 = value;
            }
        }

        public string Email2
        {
            get
            {
                return _Email2;
            }

            set
            {
                _Email2 = value;
            }
        }

        public string CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string Owner
        {
            get
            {
                return _Owner;
            }

            set
            {
                _Owner = value;
            }
        }

        public string IsOwner
        {
            get
            {
                return _IsOwner;
            }

            set
            {
                _IsOwner = value;
            }
        }

        public string Address1
        {
            get
            {
                return _Address1;
            }

            set
            {
                _Address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return _Address2;
            }

            set
            {
                _Address2 = value;
            }
        }

        public string CardDate
        {
            get
            {
                return _CardDate;
            }

            set
            {
                _CardDate = value;
            }
        }

        public string CardPlace
        {
            get
            {
                return _CardPlace;
            }

            set
            {
                _CardPlace = value;
            }
        }

        public string CardID
        {
            get
            {
                return _CardID;
            }

            set
            {
                _CardID = value;
            }
        }

        public string AssetKey
        {
            get
            {
                return _AssetKey;
            }

            set
            {
                _AssetKey = value;
            }
        }

        public string OwnerKey
        {
            get
            {
                return _OwnerKey;
            }

            set
            {
                _OwnerKey = value;
            }
        }

        public string Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public string CreatedDate
        {
            get
            {
                return _CreatedDate;
            }

            set
            {
                _CreatedDate = value;
            }
        }

        public string Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public string HasTrade
        {
            get
            {
                return _HasTrade;
            }

            set
            {
                _HasTrade = value;
            }
        }

        public string StatusKey
        {
            get
            {
                return _StatusKey;
            }

            set
            {
                _StatusKey = value;
            }
        }

        public string WantShow
        {
            get
            {
                return _WantShow;
            }

            set
            {
                _WantShow = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public string Address3
        {
            get
            {
                return _Address3;
            }

            set
            {
                _Address3 = value;
            }
        }

        public string Tax
        {
            get
            {
                return _Tax;
            }

            set
            {
                _Tax = value;
            }
        }

        public string CompanyName
        {
            get
            {
                return _CompanyName;
            }

            set
            {
                _CompanyName = value;
            }
        }

        public string Room
        {
            get
            {
                return _Room;
            }

            set
            {
                _Room = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public string SourceKey
        {
            get
            {
                return _SourceKey;
            }

            set
            {
                _SourceKey = value;
            }
        }

        public string SourceName
        {
            get
            {
                return _SourceName;
            }

            set
            {
                _SourceName = value;
            }
        }

        public string SourceNote
        {
            get
            {
                return _SourceNote;
            }

            set
            {
                _SourceNote = value;
            }
        }

        public string WantSearch
        {
            get
            {
                return _WantSearch;
            }

            set
            {
                _WantSearch = value;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }
    }
}
