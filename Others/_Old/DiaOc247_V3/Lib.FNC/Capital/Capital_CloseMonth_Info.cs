﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class Capital_CloseMonth_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _CloseDate;
        private int _CloseBy = 0;
        private int _CloseFinish = 0;
        private double _Amount = 0;
        private DateTime _CreateDate;
        private int _CategoryKey = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Capital_CloseMonth_Info()
        {
        }
        public Capital_CloseMonth_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Capital_CloseMonth WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["CloseDate"] != DBNull.Value)
                        _CloseDate = (DateTime)zReader["CloseDate"];
                    if (zReader["CloseBy"] != DBNull.Value)
                        _CloseBy = int.Parse(zReader["CloseBy"].ToString());
                    if (zReader["CloseFinish"] != DBNull.Value)
                        _CloseFinish = int.Parse(zReader["CloseFinish"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["CreateDate"] != DBNull.Value)
                        _CreateDate = (DateTime)zReader["CreateDate"];
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Capital_CloseMonth_Info(int Month, int Year)
        {
            string zSQL = "SELECT * FROM FNC_Capital_CloseMonth WHERE Month(CloseDate) = @Month AND Year(CloseDate) = @Year";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["CloseDate"] != DBNull.Value)
                        _CloseDate = (DateTime)zReader["CloseDate"];
                    if (zReader["CloseBy"] != DBNull.Value)
                        _CloseBy = int.Parse(zReader["CloseBy"].ToString());
                    if (zReader["CloseFinish"] != DBNull.Value)
                        _CloseFinish = int.Parse(zReader["CloseFinish"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["CreateDate"] != DBNull.Value)
                        _CreateDate = (DateTime)zReader["CreateDate"];
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime CloseDate
        {
            get { return _CloseDate; }
            set { _CloseDate = value; }
        }
        public int CloseBy
        {
            get { return _CloseBy; }
            set { _CloseBy = value; }
        }
        public int CloseFinish
        {
            get { return _CloseFinish; }
            set { _CloseFinish = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Capital_CloseMonth ("
        + " CloseDate ,CloseBy ,CloseFinish ,Amount ,CreateDate ,CategoryKey ) "
         + " VALUES ( "
         + "@CloseDate ,@CloseBy ,@CloseFinish ,@Amount ,@CreateDate ,@CategoryKey ) "
         + " SELECT AutoKey FROM FNC_Capital_CloseMonth WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_CloseDate.Year == 0001)
                    zCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = _CloseDate;
                zCommand.Parameters.Add("@CloseBy", SqlDbType.Int).Value = _CloseBy;
                zCommand.Parameters.Add("@CloseFinish", SqlDbType.Int).Value = _CloseFinish;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                if (_CreateDate.Year == 0001)
                    zCommand.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = _CreateDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Capital_CloseMonth SET "
                        + " CloseDate = @CloseDate,"
                        + " CloseBy = @CloseBy,"
                        + " CloseFinish = @CloseFinish,"
                        + " Amount = @Amount,"
                        + " CreateDate = @CreateDate,"
                        + " CategoryKey = @CategoryKey"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_CloseDate.Year == 0001)
                    zCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = _CloseDate;
                zCommand.Parameters.Add("@CloseBy", SqlDbType.Int).Value = _CloseBy;
                zCommand.Parameters.Add("@CloseFinish", SqlDbType.Int).Value = _CloseFinish;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                if (_CreateDate.Year == 0001)
                    zCommand.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = _CreateDate;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Capital_CloseMonth WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
