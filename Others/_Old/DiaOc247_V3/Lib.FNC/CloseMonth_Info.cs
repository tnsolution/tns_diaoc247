﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.FNC
{
    public class CloseMonth_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _DepartmentKey = 0;
        private int _CategoryKey = 0; // 1 thu chi, 2 là mở quỹ
        private DateTime _CloseDate;
        private int _CloseBy = 0;
        private int _CloseFinish = 0;
        private double _Amount = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public CloseMonth_Info()
        {
        }
        public CloseMonth_Info(int Month, int Year, int Department, int Category)
        {
            string nSQL = "SELECT * FROM FNC_CloseMonth WHERE CategoryKey = @Category AND Month(CloseDate) = @Month AND Year(CloseDate) = @Year AND DepartmentKey =@Department";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                nCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                nCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    _DepartmentKey = int.Parse(nReader["DepartmentKey"].ToString());
                    _AutoKey = int.Parse(nReader["AutoKey"].ToString());
                    if (nReader["CloseDate"] != DBNull.Value)
                        _CloseDate = (DateTime)nReader["CloseDate"];
                    _CloseBy = int.Parse(nReader["CloseBy"].ToString());
                    _CloseFinish = int.Parse(nReader["CloseFinish"].ToString());
                    _Amount = double.Parse(nReader["Amount"].ToString());
                }
                nReader.Close(); nCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { nConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime CloseDate
        {
            get { return _CloseDate; }
            set { _CloseDate = value; }
        }
        public int CloseBy
        {
            get { return _CloseBy; }
            set { _CloseBy = value; }
        }
        public int CloseFinish
        {
            get { return _CloseFinish; }
            set { _CloseFinish = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO FNC_CloseMonth ("
        + "DepartmentKey, CloseDate ,CloseBy ,CloseFinish ,Amount, CategoryKey ) "
         + " VALUES ( "
         + "@DepartmentKey, @CloseDate ,@CloseBy ,@CloseFinish ,@Amount, @CategoryKey ) ";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                nCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                if (_CloseDate.Year == 0001)
                    nCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = _CloseDate;
                nCommand.Parameters.Add("@CloseBy", SqlDbType.Int).Value = _CloseBy;
                nCommand.Parameters.Add("@CloseFinish", SqlDbType.Int).Value = _CloseFinish;
                nCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Update()
        {
            string nSQL = "UPDATE FNC_CloseMonth SET "
                        + " CloseDate = @CloseDate, DepartmentKey = @DepartmentKey, CategoryKey = @CategoryKey,"
                        + " CloseBy = @CloseBy,"
                        + " CloseFinish = @CloseFinish,"
                        + " Amount = @Amount"
                       + " WHERE AutoKey = @AutoKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                nCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                if (_CloseDate.Year == 0001)
                    nCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@CloseDate", SqlDbType.DateTime).Value = _CloseDate;
                nCommand.Parameters.Add("@CloseBy", SqlDbType.Int).Value = _CloseBy;
                nCommand.Parameters.Add("@CloseFinish", SqlDbType.Int).Value = _CloseFinish;
                nCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult;
            if (_AutoKey == 0)
                nResult = Create();
            else
                nResult = Update();
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FNC_CloseMonth WHERE AutoKey = @AutoKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
