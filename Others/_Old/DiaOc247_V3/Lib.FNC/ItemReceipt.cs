﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.FNC
{
    public class ItemReceipt
    {
        public string AutoKey { get; set; }
        public string ReceiptFrom { get; set; }
        public string ReceiptDate { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string DepartmentKey { get; set; }
        public string EmployeeKey { get; set; }
        public string Message { get; set; }
    }
}
