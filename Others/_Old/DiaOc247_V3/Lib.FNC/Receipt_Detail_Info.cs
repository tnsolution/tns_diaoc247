﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.FNC
{
    public class Receipt_Detail_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ReceiptKey = 0;
        private int _ReceiptBy = 0;
        private int _ReceiptFrom = 0;
        private int _CategoryKey = 0;
        private DateTime _ReceiptDate;
        private string _Contents = "";
        private string _Description = "";
        private double _Amount = 0;
        private double _VAT = 0;
        private int _IsApproved = 0;
        private int _ApprovedBy = 0;
        private DateTime _ApprovedDate;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _BranchKey = 0;
        private int _CustomerKey = 0;
        private string _CreatedName = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private int _FromCapital = 0;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int ReceiptKey
        {
            get { return _ReceiptKey; }
            set { _ReceiptKey = value; }
        }
        public int ReceiptBy
        {
            get { return _ReceiptBy; }
            set { _ReceiptBy = value; }
        }
        public int ReceiptFrom
        {
            get { return _ReceiptFrom; }
            set { _ReceiptFrom = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public DateTime ReceiptDate
        {
            get { return _ReceiptDate; }
            set { _ReceiptDate = value; }
        }
        public string Contents
        {
            get { return _Contents; }
            set { _Contents = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public double VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public int IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }
        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        public DateTime ApprovedDate
        {
            get { return _ApprovedDate; }
            set { _ApprovedDate = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int FromCapital
        {
            get
            {
                return _FromCapital;
            }

            set
            {
                _FromCapital = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public DateTime ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }

            set
            {
                _ModifiedDate = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Receipt_Detail_Info()
        {
        }
        public Receipt_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Receipt_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ReceiptKey"] != DBNull.Value)
                        _ReceiptKey = int.Parse(zReader["ReceiptKey"].ToString());
                    if (zReader["ReceiptBy"] != DBNull.Value)
                        _ReceiptBy = int.Parse(zReader["ReceiptBy"].ToString());
                    if (zReader["ReceiptFrom"] != DBNull.Value)
                        _ReceiptFrom = int.Parse(zReader["ReceiptFrom"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["ReceiptDate"] != DBNull.Value)
                        _ReceiptDate = (DateTime)zReader["ReceiptDate"];
                    _Contents = zReader["Contents"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["VAT"] != DBNull.Value)
                        _VAT = double.Parse(zReader["VAT"].ToString());
                    if (zReader["IsApproved"] != DBNull.Value)
                        _IsApproved = int.Parse(zReader["IsApproved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Receipt_Detail_Info(int AutoKey, bool Capital)
        {
            string zSQL = "SELECT * FROM FNC_Receipt_Detail WHERE FromCapital = @FromCapital";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromCapital", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["FromCapital"] != DBNull.Value)
                        FromCapital = int.Parse(zReader["FromCapital"].ToString());
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ReceiptKey"] != DBNull.Value)
                        _ReceiptKey = int.Parse(zReader["ReceiptKey"].ToString());
                    if (zReader["ReceiptBy"] != DBNull.Value)
                        _ReceiptBy = int.Parse(zReader["ReceiptBy"].ToString());
                    if (zReader["ReceiptFrom"] != DBNull.Value)
                        _ReceiptFrom = int.Parse(zReader["ReceiptFrom"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["ReceiptDate"] != DBNull.Value)
                        _ReceiptDate = (DateTime)zReader["ReceiptDate"];
                    _Contents = zReader["Contents"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["VAT"] != DBNull.Value)
                        _VAT = double.Parse(zReader["VAT"].ToString());
                    if (zReader["IsApproved"] != DBNull.Value)
                        _IsApproved = int.Parse(zReader["IsApproved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Receipt_Detail ("
         + " ReceiptKey ,ReceiptBy ,ReceiptFrom ,CategoryKey ,ReceiptDate ,Contents ,Description ,Amount ,VAT ,IsApproved ,ApprovedBy ,ApprovedDate ,EmployeeKey ,DepartmentKey ,BranchKey ,CustomerKey ,CreatedName ,CreatedDate ,CreatedBy, ModifiedDate, ModifiedName, ModifiedBy, FromCapital ) "
         + " VALUES ( "
         + "@ReceiptKey ,@ReceiptBy ,@ReceiptFrom ,@CategoryKey ,@ReceiptDate ,@Contents ,@Description ,@Amount ,@VAT ,@IsApproved ,@ApprovedBy ,@ApprovedDate ,@EmployeeKey ,@DepartmentKey ,@BranchKey ,@CustomerKey ,@CreatedName ,GETDATE() ,@CreatedBy, GETDATE(), @ModifiedName, @ModifiedBy, @FromCapital ) "
         + " SELECT AutoKey FROM FNC_Receipt_Detail WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.Int).Value = _ReceiptKey;
                zCommand.Parameters.Add("@ReceiptBy", SqlDbType.Int).Value = _ReceiptBy;
                zCommand.Parameters.Add("@ReceiptFrom", SqlDbType.Int).Value = _ReceiptFrom;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                if (_ReceiptDate.Year == 0001)
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = _VAT;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@FromCapital", SqlDbType.Int).Value = _FromCapital;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Receipt_Detail SET "
                        + " ReceiptKey = @ReceiptKey,"
                        + " ReceiptBy = @ReceiptBy,"
                        + " ReceiptFrom = @ReceiptFrom,"
                        + " CategoryKey = @CategoryKey,"
                        + " ReceiptDate = @ReceiptDate,"
                        + " Contents = @Contents,"
                        + " Description = @Description,"
                        + " Amount = @Amount,"
                        + " VAT = @VAT,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " CustomerKey = @CustomerKey, ModifiedDate = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.Int).Value = _ReceiptKey;
                zCommand.Parameters.Add("@ReceiptBy", SqlDbType.Int).Value = _ReceiptBy;
                zCommand.Parameters.Add("@ReceiptFrom", SqlDbType.Int).Value = _ReceiptFrom;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                if (_ReceiptDate.Year == 0001)
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = _ReceiptDate;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = _VAT;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.Parameters.Add("@FromCapital", SqlDbType.Int).Value = _FromCapital;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Receipt_Detail SET
IsApproved = @IsApproved,
ApprovedBy = @ApprovedBy,
ApprovedDate = GETDATE(), 
ModifiedBy = @ModifiedBy, 
ModifiedName = @ModifiedName 
WHERE AutoKey = @AutoKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Receipt_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}