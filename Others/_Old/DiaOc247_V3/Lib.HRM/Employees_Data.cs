﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;

namespace Lib.HRM
{
    public class Employees_Data
    {
        public static int GetDepartment(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT DepartmentKey FROM HRM_Employees WHERE EmployeeKey =" + Employee).ToInt();
        }
        public static int GetManager(int Employee)
        {
            SqlContext sql = new SqlContext();
            return sql.GetObject("SELECT ManagerKey FROM HRM_Employees WHERE EmployeeKey =" + Employee).ToInt();
        }

        public static DataTable Staff(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.EmployeeKey, A.Birthday, A.DateStart, A.DateEnd,
A.LastName + ' ' + A.FirstName EmployeeName, A.Gender, A.IsWorking,
C.Position, A.Phone1, A.Email1, A.Class, B.DepartmentName, 
dbo.FNC_GetNameEmployee(A.ManagerKey) ManagerName,
A.ImageThumb
FROM HRM_Employees A 
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
LEFT JOIN HRM_Positions C ON A.PositionKey = C.PositionKey
WHERE ISWORKING = 2";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey ";
            zSQL += "ORDER BY C.[Rank]";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A
LEFT JOIN dbo.HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
ORDER BY IsWorking";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey
WHERE A.DepartmentKey = @DepartmentKey
ORDER BY B.Rank, LastName, IsWorking";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Department, int Employee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.LastName + ' ' + A.FirstName AS EmployeeName, B.DepartmentName 
FROM HRM_Employees A
LEFT JOIN dbo.HRM_Departments B ON A.DepartmentKey = B.DepartmentKey
WHERE IsWorking=2";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            zSQL += " ORDER BY LastName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
