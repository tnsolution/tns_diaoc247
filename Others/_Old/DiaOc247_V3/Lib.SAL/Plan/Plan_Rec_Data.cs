﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Plan_Rec_Data
    {
        public static DataTable List(int PlanKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ID, A.PlanKey, A.CategoryKey, 
A.Value, B.Product AS CategoryName, B.Rank
FROM HRM_Plan_Detail A 
LEFT JOIN SYS_Categories B ON A.CategoryKey = B.AutoKey
WHERE A.PlanKey = @PlanKey 
ORDER BY B.[Rank]";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PlanKey", SqlDbType.Int).Value = PlanKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Plan_Detail ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
      
        public static DataTable List(int EmployeeKey, int Month, int Year)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ID, A.PlanKey, A.CategoryKey, C.Product AS CategoryName, 
A.Value AS Require, 
dbo.FNC_GetDoingReport(B.EmployeeKey,A.CategoryKey,Month(B.PlanDate),Year(B.PlanDate)) AS Doing
FROM HRM_Plan_Detail A 
LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey 
WHERE B.EmployeeKey = @EmployeeKey 
AND MONTH(B.PlanDate) = @Month 
AND YEAR(B.PlanDate) = @Year 
ORDER BY C.RANK";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
