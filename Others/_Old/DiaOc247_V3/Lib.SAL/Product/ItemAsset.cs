﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ItemAsset
    {
        private string _AssetKey = "0";

        private string _ProjectKey = "0";
        private string _ProjectName = "";

        private string _CategoryKey = "0";
        private string _CategoryName = "";

        private string _AssetType = "";
        private string _AssetID = "";

        private string _ID1 = "";
        private string _ID2 = "";
        private string _ID3 = "";

        private string _Address = "";       

        private string _StatusKey = "0";
        private string _Status = "";
        private string _LegalKey = "0";
        private string _Legal = "";

        private string _PurposeKey = "";
        private string _Purpose = "";

        private string _EmployeeKey = "0";
        private string _EmployeePhone = "";
        private string _EmployeeName = "";
        private string _DepartmentKey = "0";

        private string _Highway = "";
        private string _Structure = "";
        private string _View = "";
        private string _Door = "";
        private string _Room = "";
        private string _FurnitureKey = "0";
        private string _Furniture = "";
        private string _Description = "";
        private string _DateContractEnd = "";

        private string _AreaBuild = "0";
        private string _Area = "0";
        private string _AreaWall = "0";
        private string _AreaWater = "0";
        private string _AreaGround = "0";
        private string _AreaSize = "0";

        private string _Money = "0";
        private string _Money2 = "0";
        private string _PriceRent = "0";
        private string _PricePurchase = "0";
        private string _Price = "0";

        private string _PricePurchase_USD = "0";
        private string _PricePurchase_VND = "0";
        private string _PriceRent_VND = "0";
        private string _PriceRent_USD = "0";
        private string _Price_USD = "0";
        private string _Price_VND = "0";

        private string _WebPublished = "0";
        private string _WebTitle = "";
        private string _WebContent = "";
        private string _Hot = "0";
        private string _IsResale = "0";

        //thông tin chủ nhà
        private string _AutoKey = "0";
        private string _CustomerName = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Email1 = "";

        private string _CreatedDate = "";
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private string _ModifiedDate = "";
        private string _ModifiedBy = "";
        private string _ModifiedName = "";

        private string _Message = "";
        private string _IsShare = "0"; 
        private int _Edit = 0;//xac nhan chinh sua JS //cho phep chinh sua JS

        //phần công việc
        private string _Task_Key = "0";
        private string _Task_InfoKey = "0";
        private string _Task_AssetKey = "0";
        private string _Task_TableName = "";
        private string _Task_StatusName = "";
        private string _Task_StatusKey = "0";

        public string AssetKey
        {
            get
            {
                return _AssetKey;
            }

            set
            {
                _AssetKey = value;
            }
        }
        public string ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }
        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }
        public string CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }
        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }
        public string AssetType
        {
            get
            {
                return _AssetType;
            }

            set
            {
                _AssetType = value;
            }
        }
        public string AssetID
        {
            get
            {
                return _AssetID;
            }

            set
            {
                _AssetID = value;
            }
        }
        public string ID1
        {
            get
            {
                return _ID1;
            }

            set
            {
                _ID1 = value;
            }
        }
        public string ID2
        {
            get
            {
                return _ID2;
            }

            set
            {
                _ID2 = value;
            }
        }
        public string ID3
        {
            get
            {
                return _ID3;
            }

            set
            {
                _ID3 = value;
            }
        }
        public string Address
        {
            get
            {
                return _Address;
            }

            set
            {
                _Address = value;
            }
        }
        public string StatusKey
        {
            get
            {
                return _StatusKey;
            }

            set
            {
                _StatusKey = value;
            }
        }
        public string Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }
        public string LegalKey
        {
            get
            {
                return _LegalKey;
            }

            set
            {
                _LegalKey = value;
            }
        }

        public string Legal
        {
            get
            {
                return _Legal;
            }

            set
            {
                _Legal = value;
            }
        }

        public string PurposeKey
        {
            get
            {
                return _PurposeKey;
            }

            set
            {
                _PurposeKey = value;
            }
        }

        public string Purpose
        {
            get
            {
                return _Purpose;
            }

            set
            {
                _Purpose = value;
            }
        }

        public string EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public string Highway
        {
            get
            {
                return _Highway;
            }

            set
            {
                _Highway = value;
            }
        }

        public string Structure
        {
            get
            {
                return _Structure;
            }

            set
            {
                _Structure = value;
            }
        }

        public string View
        {
            get
            {
                return _View;
            }

            set
            {
                _View = value;
            }
        }

        public string Door
        {
            get
            {
                return _Door;
            }

            set
            {
                _Door = value;
            }
        }

        public string Room
        {
            get
            {
                return _Room;
            }

            set
            {
                _Room = value;
            }
        }
        public string FurnitureKey
        {
            get
            {
                return _FurnitureKey;
            }

            set
            {
                _FurnitureKey = value;
            }
        }
        public string Furniture
        {
            get
            {
                return _Furniture;
            }

            set
            {
                _Furniture = value;
            }
        }
        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }
        public string DateContractEnd
        {
            get
            {
                return _DateContractEnd;
            }

            set
            {
                _DateContractEnd = value;
            }
        }
        public string AreaBuild
        {
            get
            {
                return _AreaBuild;
            }

            set
            {
                _AreaBuild = value;
            }
        }
        public string Area
        {
            get
            {
                return _Area;
            }

            set
            {
                _Area = value;
            }
        }
        public string AreaWall
        {
            get
            {
                return _AreaWall;
            }

            set
            {
                _AreaWall = value;
            }
        }
        public string AreaWater
        {
            get
            {
                return _AreaWater;
            }

            set
            {
                _AreaWater = value;
            }
        }
        public string PriceRent
        {
            get
            {
                return _PriceRent;
            }

            set
            {
                _PriceRent = value;
            }
        }
        public string PricePurchase
        {
            get
            {
                return _PricePurchase;
            }

            set
            {
                _PricePurchase = value;
            }
        }
        public string Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }
        public string WebPublished
        {
            get
            {
                return _WebPublished;
            }

            set
            {
                _WebPublished = value;
            }
        }
        public string WebTitle
        {
            get
            {
                return _WebTitle;
            }

            set
            {
                _WebTitle = value;
            }
        }
        public string WebContent
        {
            get
            {
                return _WebContent;
            }

            set
            {
                _WebContent = value;
            }
        }
        public string Hot
        {
            get
            {
                return _Hot;
            }

            set
            {
                _Hot = value;
            }
        }
        public string CreatedDate
        {
            get
            {
                return _CreatedDate;
            }

            set
            {
                _CreatedDate = value;
            }
        }
        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }
        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }
        public string ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }

            set
            {
                _ModifiedDate = value;
            }
        }
        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }
        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public string Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }

        public string AreaGround
        {
            get
            {
                return _AreaGround;
            }

            set
            {
                _AreaGround = value;
            }
        }

        public string AreaSize
        {
            get
            {
                return _AreaSize;
            }

            set
            {
                _AreaSize = value;
            }
        }

        public string PriceRent_USD
        {
            get
            {
                return _PriceRent_USD;
            }

            set
            {
                _PriceRent_USD = value;
            }
        }

        public string Price_USD
        {
            get
            {
                return _Price_USD;
            }

            set
            {
                _Price_USD = value;
            }
        }

        public string PriceRent_VND
        {
            get
            {
                return _PriceRent_VND;
            }

            set
            {
                _PriceRent_VND = value;
            }
        }

        public string Price_VND
        {
            get
            {
                return _Price_VND;
            }

            set
            {
                _Price_VND = value;
            }
        }

        public string IsShare
        {
            get
            {
                return _IsShare;
            }

            set
            {
                _IsShare = value;
            }
        }

        public string IsResale
        {
            get
            {
                return _IsResale;
            }

            set
            {
                _IsResale = value;
            }
        }

        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }

        public string Phone1
        {
            get
            {
                return _Phone1;
            }

            set
            {
                _Phone1 = value;
            }
        }

        public string Phone2
        {
            get
            {
                return _Phone2;
            }

            set
            {
                _Phone2 = value;
            }
        }

        public string Task_AssetKey
        {
            get
            {
                return _Task_AssetKey;
            }

            set
            {
                _Task_AssetKey = value;
            }
        }

        public string Task_TableName
        {
            get
            {
                return _Task_TableName;
            }

            set
            {
                _Task_TableName = value;
            }
        }

        public string Task_StatusName
        {
            get
            {
                return _Task_StatusName;
            }

            set
            {
                _Task_StatusName = value;
            }
        }

        public string Task_StatusKey
        {
            get
            {
                return _Task_StatusKey;
            }

            set
            {
                _Task_StatusKey = value;
            }
        }

        public string Task_Key
        {
            get
            {
                return _Task_Key;
            }

            set
            {
                _Task_Key = value;
            }
        }

        public string Task_InfoKey
        {
            get
            {
                return _Task_InfoKey;
            }

            set
            {
                _Task_InfoKey = value;
            }
        }

        public string Address1
        {
            get
            {
                return _Address1;
            }

            set
            {
                _Address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return _Address2;
            }

            set
            {
                _Address2 = value;
            }
        }

        public string AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public string Email1
        {
            get
            {
                return _Email1;
            }

            set
            {
                _Email1 = value;
            }
        }

        public string PricePurchase_VND
        {
            get
            {
                return _PricePurchase_VND;
            }

            set
            {
                _PricePurchase_VND = value;
            }
        }

        public string PricePurchase_USD
        {
            get
            {
                return _PricePurchase_USD;
            }

            set
            {
                _PricePurchase_USD = value;
            }
        }

        public string EmployeePhone
        {
            get
            {
                return _EmployeePhone;
            }

            set
            {
                _EmployeePhone = value;
            }
        }

        public int Edit
        {
            get
            {
                return _Edit;
            }

            set
            {
                _Edit = value;
            }
        }

        public string Money2
        {
            get
            {
                return _Money2;
            }

            set
            {
                _Money2 = value;
            }
        }
    }
}
