﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ItemGuest
    {
        private string _Type = "";
        private int _AssetKey = 0;
        private int _OwnerKey = 0;
        private int _CustomerKey = 0;
        private int _ProductKey = 0;
        private string _ProductID = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _CustomerName = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Email = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Note = "";
        private string _CardDate = "";
        private string _CardPlace = "";      
        private string _CardID = "";
        private string _CreatedDate = "";
        private string _Birthday = "";
        private int _IsOwner = 0;
        private string _Owner = "";
        private string _Message = "";

        #region [ Properties ]

        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public int OwnerKey
        {
            get { return _OwnerKey; }
            set { _OwnerKey = value; }
        }
        public int ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Address
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }
        public string Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }
        public string Note
        {
            get
            {
                return _Note;
            }

            set
            {
                _Note = value;
            }
        }
        public string CreatedDate
        {
            get
            {
                return _CreatedDate;
            }

            set
            {
                _CreatedDate = value;
            }
        }
        public string Phone2
        {
            get
            {
                return _Phone2;
            }

            set
            {
                _Phone2 = value;
            }
        }
        public string Address2
        {
            get
            {
                return _Address2;
            }

            set
            {
                _Address2 = value;
            }
        }
        public string CardDate
        {
            get
            {
                return _CardDate;
            }

            set
            {
                _CardDate = value;
            }
        }
        public string CardPlace
        {
            get
            {
                return _CardPlace;
            }

            set
            {
                _CardPlace = value;
            }
        }
        public string CardID
        {
            get
            {
                return _CardID;
            }

            set
            {
                _CardID = value;
            }
        }
        public int CustomerKey
        {
            get
            {
                return _CustomerKey;
            }

            set
            {
                _CustomerKey = value;
            }
        }
        public string Birthday
        {
            get
            {
                return _Birthday;
            }

            set
            {
                _Birthday = value;
            }
        }

        public int IsOwner
        {
            get
            {
                return _IsOwner;
            }

            set
            {
                _IsOwner = value;
            }
        }

        public string Owner
        {
            get
            {
                return _Owner;
            }

            set
            {
                _Owner = value;
            }
        }
        #endregion
    }
}
