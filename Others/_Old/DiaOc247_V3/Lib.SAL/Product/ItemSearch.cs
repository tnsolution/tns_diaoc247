﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class ItemSearch
    {
        private string _ProjectKey = "0";
        private string _CategoryKey = "0";
        private string _EmployeeKey = "0";
        private string _Personal = "";
        private string _Status = "";
        private string _Purpose = "";
        private string _Product = "";
        private string _Name = "";
        private string _Bed = "";
        private string _ID1 = "";
        private string _ID2 = "";
        private string _ID3 = "";

        public string EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }
        public string Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }
        public string Purpose
        {
            get
            {
                return _Purpose;
            }

            set
            {
                _Purpose = value;
            }
        }
        public string Product
        {
            get
            {
                return _Product;
            }

            set
            {
                _Product = value;
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }
        public string Bed
        {
            get
            {
                return _Bed;
            }

            set
            {
                _Bed = value;
            }
        }
        public string ID1
        {
            get
            {
                return _ID1;
            }

            set
            {
                _ID1 = value;
            }
        }
        public string ID2
        {
            get
            {
                return _ID2;
            }

            set
            {
                _ID2 = value;
            }
        }
        public string ID3
        {
            get
            {
                return _ID3;
            }

            set
            {
                _ID3 = value;
            }
        }

        public string ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }

        public string CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string Personal
        {
            get
            {
                return _Personal;
            }

            set
            {
                _Personal = value;
            }
        }
    }
}
