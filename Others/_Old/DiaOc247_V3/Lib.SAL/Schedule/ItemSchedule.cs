﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ItemSchedule
    {
        private string _Message = "";
        private string _ID = "0";
        private string _TimeKey = "0";
        private string _WorkTime = "0";
        private string _WorkContent = "0";      

        public string WorkContent
        {
            get
            {
                return _WorkContent;
            }

            set
            {
                _WorkContent = value;
            }
        }

        public string WorkTime
        {
            get
            {
                return _WorkTime;
            }

            set
            {
                _WorkTime = value;
            }
        }

        public string TimeKey
        {
            get
            {
                return _TimeKey;
            }

            set
            {
                _TimeKey = value;
            }
        }

        public string ID
        {
            get
            {
                return _ID;
            }

            set
            {
                _ID = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }
    }
}
