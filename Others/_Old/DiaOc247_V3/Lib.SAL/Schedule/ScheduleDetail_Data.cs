﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ScheduleDetail_Data
    {
        public static DataTable GetSchedule(int Key)
        {
            SqlContext zSql = new SqlContext();
            return zSql.GetData("SELECT * FROM HRM_ScheduleDetail WHERE TimeKey =" + Key);
        }

        public static List<ScheduleItem> GetSchedule(DateTime Start, DateTime End, int Department, int Employee)
        {
            string zQuery = @"
SELECT B.TimeKey AS ID, A.Title, A.DepartmentKey, A.EmployeeKey, A.[Description], A.[Start], A.[End], A.AllDay 
FROM HRM_ScheduleDetail A 
LEFT JOIN HRM_Schedule B ON A.TIMEKEY = B.TIMEKEY 
WHERE A.[Start]>=@start AND A.[End]<=@end";

            if (Employee != 0)
                zQuery += " AND A.EmployeeKey = @Employee";
            if (Department != 0)
                zQuery += " AND A.DepartmentKey = @Department";

            SqlContext zSql = new SqlContext();

            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            zSql.CMD.Parameters.Add("@start", SqlDbType.DateTime).Value = Start;
            zSql.CMD.Parameters.Add("@end", SqlDbType.DateTime).Value = End;

            return zSql.GetData(zQuery).DataTableToList<ScheduleItem>();
        }

        public static List<ScheduleItem> GetSchedule_Mobile(DateTime Start, DateTime End, int Department, int Employee)
        {
            string zQuery = @"
SELECT B.TimeKey AS ID, A.Title, A.DepartmentKey, A.EmployeeKey, A.[Description], A.[Start], A.[End], A.AllDay 
FROM HRM_ScheduleDetail A 
LEFT JOIN HRM_Schedule B ON A.TIMEKEY = B.TIMEKEY 
WHERE A.CreatedDate BETWEEN @start AND @end";

            if (Employee != 0)
                zQuery += " AND A.EmployeeKey = @Employee";
            if (Department != 0)
                zQuery += " AND A.DepartmentKey = @Department";

            SqlContext zSql = new SqlContext();

            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            zSql.CMD.Parameters.Add("@start", SqlDbType.DateTime).Value = Start;
            zSql.CMD.Parameters.Add("@end", SqlDbType.DateTime).Value = End;

            return zSql.GetData(zQuery).DataTableToList<ScheduleItem>();
        }

        public static int GetParent(string Start, string End, int Department, int Employee)
        {
            string zQuery = "SELECT TimeKey FROM HRM_ScheduleDetail WHERE [Start]>=@start AND [End]<=@end AND EmployeeKey = @Employee AND DepartmentKey = @Department";
            SqlContext zSql = new SqlContext();

            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            zSql.CMD.Parameters.Add("@start", SqlDbType.DateTime).Value = Start;
            zSql.CMD.Parameters.Add("@end", SqlDbType.DateTime).Value = End;

            return zSql.GetObject(zQuery).ToInt();
        }
    }
}
