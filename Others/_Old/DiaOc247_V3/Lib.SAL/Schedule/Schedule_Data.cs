﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class Schedule_Data
    {
        public static List<ScheduleItem> GetSchedule(DateTime Start, DateTime End, int Department, int Employee)
        {
            string zQuery = "SELECT TimeKey AS ID, Title, DepartmentKey, EmployeeKey, [Description], [Start], [End], AllDay FROM HRM_Schedule WHERE [Start]>=@start AND [End]<=@end";
            if (Employee != 0)
                zQuery += " AND EmployeeKey = @Employee";
            if (Employee != 0)
                zQuery += " AND DepartmentKey = @Department";

            SqlContext zSql = new SqlContext();

            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            zSql.CMD.Parameters.Add("@start", SqlDbType.DateTime).Value = Start;
            zSql.CMD.Parameters.Add("@end", SqlDbType.DateTime).Value = End;

            return zSql.GetData(zQuery).DataTableToList<ScheduleItem>();
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*, dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName FROM HRM_Schedule A ORDER BY ScheduleDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);                
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int DepartmentKey, int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Schedule WHERE 1=1";
            if (DepartmentKey != 0)
                zSQL += " AND DepartmentKey = @DepartmentKey";
            if(EmployeeKey !=0)
                zSQL += " AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, 
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS DepartmentName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName 
FROM HRM_Schedule A 
WHERE [Start] BETWEEN @FromDate And @ToDate";
            if (DepartmentKey != 0)
                zSQL += " AND DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey";

            zSQL += " ORDER BY ScheduleDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
