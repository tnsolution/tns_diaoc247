﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Schedule_Rec_Info
    {
        #region [ Field Name ]
        private int _ID = 0;
        private int _TimeKey = 0;
        private string _WorkTime = "";
        private string _WorkContent = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public int TimeKey
        {
            get { return _TimeKey; }
            set { _TimeKey = value; }
        }
        public string WorkTime
        {
            get { return _WorkTime; }
            set { _WorkTime = value; }
        }
        public string WorkContent
        {
            get { return _WorkContent; }
            set { _WorkContent = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Schedule_Rec_Info()
        {
        }
        public Schedule_Rec_Info(int ID)
        {
            string zSQL = "SELECT * FROM HRM_ScheduleDetail WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ID"] != DBNull.Value)
                        _ID = int.Parse(zReader["ID"].ToString());
                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    _WorkTime = zReader["WorkTime"].ToString();
                    _WorkContent = zReader["WorkContent"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_ScheduleDetail ("
        + " TimeKey ,WorkTime ,WorkContent ) "
         + " VALUES ( "
         + "@TimeKey ,@WorkTime ,@WorkContent ) "
         + " SELECT ID FROM HRM_ScheduleDetail WHERE ID = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@WorkTime", SqlDbType.NVarChar).Value = _WorkTime;
                zCommand.Parameters.Add("@WorkContent", SqlDbType.NVarChar).Value = _WorkContent;
                _ID = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_ScheduleDetail SET "
                        + " TimeKey = @TimeKey,"
                        + " WorkTime = @WorkTime,"
                        + " WorkContent = @WorkContent"
                       + " WHERE ID = @ID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@WorkTime", SqlDbType.NVarChar).Value = _WorkTime;
                zCommand.Parameters.Add("@WorkContent", SqlDbType.NVarChar).Value = _WorkContent;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ID == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_ScheduleDetail WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
