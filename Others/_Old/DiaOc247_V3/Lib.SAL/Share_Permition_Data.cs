﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Share_Permition_Data
    {
        public static DataTable List_ShareEmployee(int AssetKey, string TableName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  A.*, dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName 
FROM PUL_SharePermition A 
WHERE AssetKey = @AssetKey AND ObjectTable=@Table";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                zCommand.Parameters.Add("@Table", SqlDbType.NVarChar).Value = TableName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_ShareDepartment(int AssetKey, string TableName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT  A.*, B.DepartmentName AS EmployeeName 
FROM PUL_SharePermition A LEFT JOIN HRM_Departments B ON A.EmployeeKey = B.DepartmentKey 
WHERE AssetKey = @AssetKey AND ObjectTable=@Table";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                zCommand.Parameters.Add("@Table", SqlDbType.NVarChar).Value = TableName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
