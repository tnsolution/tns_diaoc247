﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SYS
{
    public class Categories_Data
    {
        public static DataTable ListFolder()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.AutoKey, A.Product FROM SYS_Categories A WHERE A.Type = 20 AND AutoKey <> 130 ORDER BY PRODUCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListFolder(string Key)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.AutoKey, A.Product FROM SYS_Categories A WHERE A.Type = 20 AND AutoKey IN (" + Key + ") ORDER BY PRODUCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListDefine_Target()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Categories WHERE Type = 34 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //--------------
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM SYS_Categories ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*, B.Product AS CategoryName FROM SYS_Categories A LEFT JOIN dbo.SYS_Categories B ON A.Category = B.AutoKey WHERE A.Type = @Type ORDER BY RANK, PRODUCT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static string GetName(int Type, int AutoKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT Product FROM SYS_Categories WHERE AutoKey = @AutoKey AND Type = @Type";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;

                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                return Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
