﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SYS
{
    public class HomePage
    {
        public static int Count_Require_Trade(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int Result = 0;
            string zSQL = @"
	SELECT SUM(A.Value) AS Require 
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN HRM_Employees C ON B.EmployeeKey = C.EmployeeKey
    WHERE A.CategoryKey = 298 AND (B.PlanDate BETWEEN @FromDate AND @ToDate)";
            if (Employee != 0)
                zSQL += " AND B.EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                Result = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return Result;
        }
        public static int Count_Success_Trade(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            string zSQL = @"
SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE (A.IsApproved = 1) AND A.DateApprove BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND (A.EmployeeKey = @EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Trade_NeedApproved(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"
SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE A.IsApproved = 0";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Trade_NeedCollect(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE A.IsFinish = 0";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static double Sum_Trade(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT SUM(A.Income) AS COUNT
FROM FNC_Transaction A
WHERE A.IsApproved = 1 AND A.IsFinish = 1 AND (A.FinishDate BETWEEN @FromDate AND @ToDate)";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {


                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static DataTable Get_Annou(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Announce";
            if (Department != 0)
                zSQL += " WHERE DepartmentKey IN (0,@DepartmentKey) ";
            zSQL += " ORDER BY CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_Schedule(int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.* 
FROM HRM_ScheduleDetail A 
WHERE CONVERT(DATE,[Start],120) = CONVERT(DATE,GETDATE(),120)
AND EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_ListStaff(DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT ROW_NUMBER() OVER (ORDER BY INCOME DESC) AS RowNumber , ISNULL(Income,0) AS INCOME, Name_Employee
FROM (
	SELECT A.LastName + ' ' + A.FirstName Name_Employee,  dbo.FNC_GetIncome(A.EmployeeKey,@FromDate,@ToDate) AS INCOME 
    FROM HRM_Employees A 
    WHERE A.IsWorking = 2 AND A.PositionKey IN (5,3)
) X";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
