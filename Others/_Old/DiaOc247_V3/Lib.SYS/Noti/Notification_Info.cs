﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SYS
{
    public class Notification_Info
    {

        #region [ Field Name ]
        private int _ID = 0;
        private int _EmployeeKey = 0;
        private string _Message = "";
        private int _Type = 0;
        private string _ObjectTableName = "";
        private string _ObjectTable = "";
        private int _IsRead = 0;
        private int _IsShow = 1;
        private DateTime _ReadDate;
        private DateTime _CreatedDate;
        #endregion

        #region [ Constructor Get Information ]
        public Notification_Info()
        {
        }
        public Notification_Info(int ID)
        {
            string zSQL = "SELECT * FROM SYS_Notification WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _ID = int.Parse(zReader["ID"].ToString());
                    _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _Message = zReader["Message"].ToString();
                    _Type = int.Parse(zReader["Type"].ToString());
                    _ObjectTableName = zReader["ObjectTableName"].ToString();
                    _ObjectTable = zReader["ObjectTable"].ToString();
                    _IsRead = int.Parse(zReader["IsRead"].ToString());
                    _IsShow = int.Parse(zReader["IsShow"].ToString());
                    if (zReader["ReadDate"] != DBNull.Value)
                        _ReadDate = (DateTime)zReader["ReadDate"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]

        public int IsShow
        {
            get { return _IsShow; }
            set { _IsShow = value; }
        }
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string ObjectTableName
        {
            get { return _ObjectTableName; }
            set { _ObjectTableName = value; }
        }
        public string ObjectTable
        {
            get { return _ObjectTable; }
            set { _ObjectTable = value; }
        }
        public int IsRead
        {
            get { return _IsRead; }
            set { _IsRead = value; }
        }
        public DateTime ReadDate
        {
            get { return _ReadDate; }
            set { _ReadDate = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Notification ("
        + " EmployeeKey ,Message ,Type ,ObjectTableName ,ObjectTable ,IsRead ,ReadDate ,CreatedDate, IsShow ) "
         + " VALUES ( "
         + "@EmployeeKey ,@Message ,@Type ,@ObjectTableName ,@ObjectTable ,@IsRead ,@ReadDate ,@CreatedDate, @IsShow ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Message", SqlDbType.NVarChar).Value = _Message;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@ObjectTableName", SqlDbType.NVarChar).Value = _ObjectTableName;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = _ObjectTable;
                zCommand.Parameters.Add("@IsRead", SqlDbType.Int).Value = _IsRead;
                zCommand.Parameters.Add("@IsShow", SqlDbType.Int).Value = _IsShow;
                if (_ReadDate.Year == 0001)
                    zCommand.Parameters.Add("@ReadDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReadDate", SqlDbType.DateTime).Value = _ReadDate;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Notification SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " Message = @Message,"
                        + " Type = @Type,"
                        + " ObjectTableName = @ObjectTableName,"
                        + " ObjectTable = @ObjectTable,"
                        + " IsRead = @IsRead, IsShow =@IsShow,"
                        + " ReadDate = @ReadDate,"
                        + " CreatedDate = @CreatedDate"
                       + " WHERE ID = @ID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@Message", SqlDbType.NVarChar).Value = _Message;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@ObjectTableName", SqlDbType.NVarChar).Value = _ObjectTableName;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = _ObjectTable;
                zCommand.Parameters.Add("@IsRead", SqlDbType.Int).Value = _IsRead;
                zCommand.Parameters.Add("@IsShow", SqlDbType.Int).Value = _IsShow;
                if (_ReadDate.Year == 0001)
                    zCommand.Parameters.Add("@ReadDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ReadDate", SqlDbType.DateTime).Value = _ReadDate;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ID == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Notification WHERE ID = @ID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //===================Su dung
        public string InsertAuto(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string CustomSQL(string SQL)
        {
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateReaded()
        {
            string zSQL = "UPDATE SYS_Notification SET IsRead = @IsRead, ReadDate = GETDATE() WHERE ID = @ID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ID", SqlDbType.Int).Value = _ID;
                zCommand.Parameters.Add("@IsRead", SqlDbType.Int).Value = _IsRead;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
