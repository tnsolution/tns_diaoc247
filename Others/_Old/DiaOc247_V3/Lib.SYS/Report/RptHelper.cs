﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SYS.Report
{
    public class RptHelper
    {
        public static double PreviousMonth(int Month, int Year, int Department)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            string sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_CloseMonth WHERE CategoryKey = 1 AND CloseFinish = 1 AND MONTH(CloseDate) = (@Month - 1) AND YEAR(CloseDate) = @Year ";
            if (Department != 0)
                sql += "AND DepartmentKey = @Department";
            return Convert.ToDouble(zSqlContext.GetObject(sql));
        }
        public static double PreviousMonth(int Month, int Year)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToDouble(zSqlContext.GetObject("SELECT ISNULL(SUM(Amount),0) FROM FNC_CloseMonth WHERE CategoryKey = 1 AND CloseFinish = 1 AND MONTH(CloseDate) = (@Month - 1) AND YEAR(CloseDate) = @Year"));
        }
        public static double TotalReceipt(int Month, int Year, int Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND MONTH(ReceiptDate) = @Month AND YEAR(ReceiptDate)=@Year AND IsApproved=1";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }
        public static double TotalPayment(int Month, int Year, int Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND MONTH(PaymentDate) = @Month AND YEAR(PaymentDate)=@Year AND IsApproved=1 AND IsApproved2=1";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }

        public static double TotalBalance(int Month, int Year)
        {          
            SqlContext zSqlContext = new SqlContext();            
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;         
            return Convert.ToDouble(zSqlContext.GetObject("SELECT ISNULL(dbo.FNC_GetFunds(@Month, @Year),0)"));
        }

        public static double TotalBalanceCapital(DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSqlContext.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            return zSqlContext.GetObject(@"SELECT ISNULL(SUM(A.Amount),0) AS PreviousMonth FROM FNC_Capital_CloseMonth A 
WHERE A.CloseDate BETWEEN @FromDate AND @ToDate").ToDouble();
        }
    }
}
