﻿using Lib.Config;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;

namespace Lib.SYS
{
    public class Tools
    {
        private static string _ConnectionString = ConnectDataBase.ConnectionString;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FullName"></param>
        /// <returns>[0] Lastname [1] FirstName</returns>
        public static string[] SplitName(string FullName)
        {
            string[] Result = new string[2];
            string lastName = "";
            string firstName = "";
            if (FullName.Split(' ').Length > 1)
            {

                firstName = FullName.Substring(FullName.LastIndexOf(' ') + 1);
                lastName = FullName.Substring(0, FullName.LastIndexOf(' '));
            }
            else
            {
                firstName = FullName;
            }
            Result[0] = lastName;
            Result[1] = firstName;
            return Result;
        }

        public static string MoneyRate(string CurrencyCode)
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                String xmlSourceUrl = "http://www.vietcombank.com.vn/ExchangeRates/ExrateXML.aspx";
                xmlDocument.Load(xmlSourceUrl);

                //từ đây hoàn toàn có thể thao tác dữ liệu xml bằng đối tượng xmlDocument
                //lấy ví zụ chuyển từ XmlDocument thành tập các đối tượng Generic dạng List<Exrate>
                XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Exrate");
                List<Exrate> listExrate = null;
                if (nodeList != null && nodeList.Count > 0)
                {
                    listExrate = new List<Exrate>();
                    foreach (XmlNode xmlNode in nodeList)
                    {
                        Exrate entityExrate = new Exrate();
                        entityExrate.CurrencyCode = xmlNode.Attributes["CurrencyCode"].InnerText;
                        entityExrate.CurrencyName = xmlNode.Attributes["CurrencyName"].InnerText;
                        entityExrate.Buy = float.Parse(xmlNode.Attributes["Buy"].InnerText);
                        entityExrate.Transfer = float.Parse(xmlNode.Attributes["Transfer"].InnerText);
                        entityExrate.Sell = float.Parse(xmlNode.Attributes["Sell"].InnerText);

                        listExrate.Add(entityExrate);
                    }
                }

                return listExrate.Single(s => s.CurrencyCode == CurrencyCode).Sell.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string Html_Select(string Id, string FirstSelect, string CssClass, bool SelectMany, string SQL, bool SelectAll)
        {
            StringBuilder zSb = new StringBuilder();

            if (SelectMany)
                zSb.AppendLine("<select id='" + Id + "' name='" + Id + "' class='" + CssClass + "' multiple='' >");
            else
                zSb.AppendLine("<select id='" + Id + "' name='" + Id + "' class='" + CssClass + "'>");

            zSb.AppendLine(" <option selected='selected' value='-1' disabled='disabled'>" + FirstSelect + "</option>");

            if (SelectAll)
                zSb.AppendLine("<option value='0'>Tất cả</option>");

            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);

                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<option value='" + r[0].ToString() + "'>" + r[1].ToString() + "</option>");
                }
            }
            catch (Exception ex)
            {
                zSb.AppendLine("<option value='Error'>" + ex.ToString() + "</option>");
            }
            finally
            {
                zConnect.Close();
            }

            zSb.AppendLine("</select>");
            return zSb.ToString();
        }
        public static string Html_Select(string Id, string FirstSelect, string CssClass, string SQL)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<select id='" + Id + "' name='" + Id + "' class='" + CssClass + "'>");
            zSb.AppendLine("<option selected='selected' value='-1' disabled='disabled'>" + FirstSelect + "</option>");
            zSb.AppendLine("<option value='0'></option>");

            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);

                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<option value='" + r[0].ToString() + "'>" + r[1].ToString() + "</option>");
                }
            }
            catch (Exception ex)
            {
                zSb.AppendLine("<option value='Error'>" + ex.ToString() + "</option>");
            }
            finally
            {
                zConnect.Close();
            }

            zSb.AppendLine("</select>");
            return zSb.ToString();
        }

        //CHI DANH CHO FORM TASK, FROM BIEU TUONG
        public static string Html_Dropdown_StatusTask(string SQL)
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);


                zSb.AppendLine("<ul class='dropdown-menu dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close'>");
                foreach (DataRow r in zTable.Rows)
                {
                    if (Convert.ToInt32(r["Parent"]) != 1)
                        zSb.AppendLine("<li><a href='#' onclick='action(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'>" + r["Name"].ToString() + "</a></li>");
                }
                zSb.AppendLine(" <li class='divider'></li>");
                foreach (DataRow r in zTable.Rows)
                {
                    if (Convert.ToInt32(r["Parent"]) == 1)
                        zSb.AppendLine("<li><a href='#' onclick='action(" + r["ID"].ToString() + ",\"" + r["Name"].ToString() + "\")'><i class='ace-icon fa fa-pencil-square-o blue'></i> " + r["Name"].ToString() + "</a></li>");

                }
                zSb.AppendLine("<li class='divider'></li>");
                zSb.AppendLine("<li><a href='#' data-dismiss='modal'><i class='fa fa-times red'></i> Đóng</a></li>");
                zSb.AppendLine("</ul>");
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            finally
            {
                zConnect.Close();
            }

            return zSb.ToString();
        }

        public static List<ItemWeb> GetDistricts(int ProvinceKey)
        {
            string zResult = "";
            string StringSQL = "SELECT DistrictKey, Type + ' ' + Name AS NAME FROM SYS_District WHERE ProvinceKey = @ProvinceKey ORDER BY Rank, Type";
            List<ItemWeb> zListDistricts = new List<ItemWeb>();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(StringSQL, zConnect);
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = ProvinceKey;
                SqlDataReader zReader = zCommand.ExecuteReader();

                if (zReader.HasRows)
                {
                    while (zReader.Read())
                    {
                        zListDistricts.Add(new ItemWeb
                        {
                            Value = zReader["DistrictKey"].ToString(),
                            Text = zReader["NAME"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zListDistricts;
        }
        public static List<ItemWeb> GetEmployee(int DepartmentKey)
        {
            string zResult = "";
            string StringSQL = "SELECT EmployeeKey, LastName + ' ' + FirstName AS EmployeeName FROM HRM_Employees WHERE DepartmentKey = @DepartmentKey AND IsWorking=2";
            List<ItemWeb> zListDistricts = new List<ItemWeb>();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(StringSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataReader zReader = zCommand.ExecuteReader();

                if (zReader.HasRows)
                {
                    while (zReader.Read())
                    {
                        zListDistricts.Add(new ItemWeb
                        {
                            Value = zReader["EmployeeKey"].ToString(),
                            Text = zReader["EmployeeName"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zListDistricts;
        }
        public static List<ItemWeb> GetProject(int DepartmentKey)
        {
            string zResult = "";
            string StringSQL = "SELECT A.AssetKey AS ProjectKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey = " + DepartmentKey + " ORDER BY ProjectName";
            List<ItemWeb> zListDistricts = new List<ItemWeb>();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(StringSQL, zConnect);
                SqlDataReader zReader = zCommand.ExecuteReader();

                if (zReader.HasRows)
                {
                    while (zReader.Read())
                    {
                        zListDistricts.Add(new ItemWeb
                        {
                            Value = zReader["ProjectKey"].ToString(),
                            Text = zReader["ProjectName"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zListDistricts;
        }

        public static string DropDown_DDL_Month(DropDownList DDL)
        {
            try
            {
                for (int i = 1; i <= 12; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();
                    item.Text = "Tháng " + i;
                    DDL.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return string.Empty;
        }
        public static string DropDown_DDL(DropDownList DDL, string StringSQL, bool IsView, bool CutString)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(StringSQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);

                DataRow nRow;
                if (IsView)
                {
                    nRow = zTable.NewRow();
                    nRow[0] = 0;
                    nRow[1] = "";
                    zTable.Rows.InsertAt(nRow, 0);
                }
                DataTable NewTable = new DataTable();
                NewTable.Columns.Add(zTable.Columns[0].ColumnName);
                NewTable.Columns.Add(zTable.Columns[1].ColumnName);
                foreach (DataRow r in zTable.Rows)
                {
                    DataRow rN = NewTable.NewRow();
                    rN[0] = r[0];
                    rN[1] = r[1].ToString().Substring(r[1].ToString().LastIndexOf('\\'), r[1].ToString().Length - r[1].ToString().LastIndexOf('\\'));
                    NewTable.Rows.Add(rN);
                }

                DDL.DataSource = NewTable;
                DDL.DataTextField = NewTable.Columns[1].ColumnName;
                DDL.DataValueField = NewTable.Columns[0].ColumnName;
                DDL.DataBind();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string DropDown_DDL(DropDownList DDL, string StringSQL, bool IsView)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(StringSQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);

                DataRow nRow;
                if (IsView)
                {
                    nRow = zTable.NewRow();
                    nRow[0] = 0;
                    nRow[1] = "";
                    zTable.Rows.InsertAt(nRow, 0);
                }

                DDL.DataSource = zTable;
                DDL.DataTextField = zTable.Columns[1].ColumnName;
                DDL.DataValueField = zTable.Columns[0].ColumnName;
                DDL.DataBind();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string DropDown_DDL_Proc(DropDownList DDL, string StringProc, string ParName, int ParValue, bool IsView)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(StringProc, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@" + ParName, SqlDbType.Int).Value = ParValue;
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);

                DataRow nRow;
                if (IsView)
                {
                    nRow = zTable.NewRow();
                    nRow[0] = 0;
                    nRow[1] = "";
                    zTable.Rows.InsertAt(nRow, 0);
                }

                DDL.DataSource = zTable;
                DDL.DataTextField = zTable.Columns[1].ColumnName;
                DDL.DataValueField = zTable.Columns[0].ColumnName;
                DDL.DataBind();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        /// <summary>
        /// AutoNumber
        /// </summary>
        /// <param name="In_ID"></param>
        /// <returns>01A01[01], 01A01[02], ...</returns>
        public static string AutoID(string In_ID)
        {
            var str = In_ID.Trim();
            var tempArray = str.Split('A');
            string tempId = tempArray[1];
            int id = Convert.ToInt32(tempId);
            id = id + 1;
            string autoId = "01A01" + String.Format("{0:00}", id);
            return autoId;
        }

        public static DateTime ConvertToDate(string strDate)
        {
            if (strDate.Contains("-"))
            {
                strDate = strDate.Replace("-", "/");
            }

            CultureInfo zProvider = CultureInfo.InvariantCulture;
            DateTime nDate = new DateTime();
            if (strDate.Trim().Length > 0)
            {
                if (DateTime.TryParseExact(strDate, "dd/MM/yyyy", zProvider, DateTimeStyles.None, out nDate))
                {
                    return nDate;
                }
            }
            return new DateTime(0001, 01, 01);
        }
        public static DateTime ConvertToDateTime(string strDate)
        {
            if (strDate.Contains("-"))
            {
                strDate = strDate.Replace("-", "/");
            }

            CultureInfo zProvider = CultureInfo.InvariantCulture;
            DateTime nDate = new DateTime();
            if (strDate.Trim().Length > 0)
            {
                if (DateTime.TryParseExact(strDate + " " + DateTime.Now.ToString("HH") + ":" + DateTime.Now.ToString("mm"), "dd/MM/yyyy HH:mm", zProvider, DateTimeStyles.None, out nDate))
                {
                    return nDate;
                }
            }
            return new DateTime(0001, 01, 01);
        }
        public static DateTime ConvertToDate(string strDate, string US)
        {
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy",
                                    CultureInfo.InvariantCulture);

            return date;
        }

        public static string SendMail(string MailBody, string MailAddress)
        {
            string zMailServer = "no-reply@hht.vn";
            string zMailPass = "Nn123123";
            string zMessage = "";
            try
            {
                string Content = MailBody;
                MailMessage zMail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("mail.hht.vn", 25);
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;

                zMail.SubjectEncoding = Encoding.UTF8;
                zMail.BodyEncoding = Encoding.UTF8;

                zMail.From = new MailAddress(zMailServer, "cty.hht.vn");
                zMail.To.Add(MailAddress);
                zMail.Subject = "Thông báo tự động";
                zMail.Priority = MailPriority.High;
                zMail.IsBodyHtml = true;
                zMail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                zMail.Body = Content;

                SmtpServer.Credentials = new System.Net.NetworkCredential(zMailServer, zMailPass);
                SmtpServer.Send(zMail);
            }
            catch (Exception ex)
            {
                zMessage = ex.Message;
            }
            return zMessage;
        }
        public static string SendMail_Google(string MailBody, string MailAddress, string EmailSend, string EmailPass, bool MultiEmail)
        {
            string zMessage = "";
            try
            {
                string Content = MailBody;

                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com", 587);
                SmtpServer.EnableSsl = true;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.Credentials = new System.Net.NetworkCredential(EmailSend, EmailPass);

                MailMessage zMail = new MailMessage();
                zMail.SubjectEncoding = Encoding.UTF8;
                zMail.BodyEncoding = Encoding.UTF8;
                zMail.From = new MailAddress(EmailSend, "CRM auto reminder");
                zMail.IsBodyHtml = true;
                if (!MultiEmail)
                {
                    zMail.To.Add(MailAddress);
                }
                else
                {
                    string[] temp = MailAddress.Split(';');
                    foreach (string s in temp)
                    {
                        zMail.To.Add(s);
                    }
                }

                zMail.Subject = "Thông báo tự động";
                zMail.Priority = MailPriority.High;
                zMail.IsBodyHtml = true;
                zMail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                zMail.Body = Content;

                SmtpServer.Send(zMail);
            }
            catch (Exception ex)
            {
                zMessage = ex.Message;
            }
            return zMessage;
        }


        #region [Unicode array]
        private static readonly string[] VietnameseSigns = new string[]
 {
 "aAeEoOuUiIdDyY",
 "áàạảãâấầậẩẫăắằặẳẵ",
 "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
 "éèẹẻẽêếềệểễ",
 "ÉÈẸẺẼÊẾỀỆỂỄ",
 "óòọỏõôốồộổỗơớờợởỡ",
 "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
 "úùụủũưứừựửữ",
 "ÚÙỤỦŨƯỨỪỰỬỮ",
 "íìịỉĩ",
 "ÍÌỊỈĨ",
 "đ",
 "Đ",
 "ýỳỵỷỹ",
 "ÝỲỴỶỸ"
 };
        #endregion

        public static string RemoveVietnamese(string str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str.Replace(" ", "");
        }

        public static string ListBox(ListBox LB, string SQL)
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                SqlDataAdapter zDa = new SqlDataAdapter(zCommand);
                zDa.Fill(zTable);

                LB.DataSource = zTable;
                LB.DataTextField = zTable.Columns[1].ColumnName;
                LB.DataValueField = zTable.Columns[0].ColumnName;
                LB.DataBind();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static DataTable ExcelToTable(string FilePath, string Extension, string isHDR)
        {
            try
            {
                string zConStr = "";

                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        zConStr = ConfigurationManager.ConnectionStrings["Excel03ConString"]
                                 .ConnectionString;
                        break;
                    case ".xlsx": //Excel 07
                        zConStr = ConfigurationManager.ConnectionStrings["Excel07ConString"]
                                  .ConnectionString;
                        break;
                }

                zConStr = String.Format(zConStr, FilePath, isHDR);
                OleDbConnection zCon = new OleDbConnection(zConStr);
                OleDbCommand zCMD = new OleDbCommand();
                OleDbDataAdapter zDa = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                zCMD.Connection = zCon;

                //Get the name of First Sheet
                zCon.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = zCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                zCon.Close();

                //Read Data from First Sheet
                zCon.Open();
                zCMD.CommandText = "SELECT * From [" + SheetName + "]";
                zDa.SelectCommand = zCMD;
                zDa.Fill(dt);
                zCon.Close();

                return dt;
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Error");
                DataRow r = dt.NewRow();
                r["Error"] = ex.ToString();
                dt.Rows.Add(r);
                return dt;
            }
        }

        /// <summary>
        /// Đổi kích thước hình ảnh
        /// </summary>
        /// <param name="imgToResize">Kiểu Image</param>
        /// <param name="size">Size(100, 100)</param>
        /// <returns></returns>
        public static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
        {
            //Get the image current width
            int sourceWidth = imgToResize.Width;
            //Get the image current height
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            //Calulate  width with new desired size
            nPercentW = ((float)size.Width / (float)sourceWidth);
            //Calculate height with new desired size
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;
            //New Width
            int destWidth = (int)(sourceWidth * nPercent);
            //New Height
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            // Draw image with new width and height
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            return (System.Drawing.Image)b;
        }

        public static DataTable ImportToDataTable(string FilePath, string SheetName)
        {
            DataTable dt = new DataTable();
            FileInfo fi = new FileInfo(FilePath);

            // Check if the file exists
            if (!fi.Exists)
                throw new Exception("File " + FilePath + " Does Not Exists");

            using (ExcelPackage xlPackage = new ExcelPackage(fi))
            {
                // get the first worksheet in the workbook
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[1];// [SheetName];

                // Fetch the WorkSheet size
                ExcelCellAddress startCell = worksheet.Dimension.Start;
                ExcelCellAddress endCell = worksheet.Dimension.End;

                // create all the needed DataColumn
                for (int col = startCell.Column; col <= endCell.Column; col++)
                    dt.Columns.Add(col.ToString());

                // place all the data into DataTable [ + 1 là trừ dòng đầu]
                for (int row = startCell.Row + 1; row <= endCell.Row; row++)
                {
                    DataRow dr = dt.NewRow();
                    int x = 0;
                    for (int col = startCell.Column; col <= endCell.Column; col++)
                    {
                        dr[x++] = worksheet.Cells[row, col].Value;
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
    }
}
