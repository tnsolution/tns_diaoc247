﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Data;
using System.Reflection;

namespace Lib.SYS
{
    public static class Utils
    {
        public static string HtmlPhone(this object obj)
        {
            string temp = "";
            if (obj.ToString() != string.Empty)
                temp = " <i class='ace-icon fa fa-mobile bigger-180 blue'></i> " + obj.ToString();
            return temp;
        }

        public static string ToFullLink(this object obj)
        {
            string name = obj.ToString();
            string temp = Uri.EscapeUriString(name);
            string result = "";
            String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            result = strUrl + temp;
            return result;
        }
        public static string ToThumb(this object obj)
        {
            if (obj.ToString() == string.Empty || obj.ToString().Length <= 0)
            {
                return @"\Upload\Image\NoImg.png";
            }
            else
            {
                return obj.ToString();
            }
        }
        public static string StripHtml(string Txt)
        {
            if (Txt != null)
            {
                return Regex.Replace(Txt, "<(.|\\n)*?>", string.Empty);
            }
            else
            {
                return "";
            }
        }
        public static int ToInt(this object obj)
        {
            try
            {
                return int.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static double ToDouble(this object obj)
        {
            try
            {
                return double.Parse(obj.ToString());
            }
            catch
            {
                return 0;
            }
        }
        public static string ToDateString(this object obj)
        {
            try
            {
                string s = "";
                DateTime zDate = DateTime.Parse(obj.ToString());
                if (zDate != DateTime.MinValue)
                    s = zDate.ToString("dd/MM/yyyy");
                return s;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        public static string ToDateTimeString(this object obj)
        {
            try
            {
                string s = "";
                DateTime zDate = DateTime.Parse(obj.ToString());
                if (zDate != DateTime.MinValue)
                    s = zDate.ToString("dd/MM/yyyy HH:mm");
                return s;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        public static string ToDoubleString(this object obj)
        {
            try
            {
                return double.Parse(obj.ToString()).ToString("n0");
            }
            catch
            {
                return "0";
            }
        }
        public static String ToAscii(this String unicode)
        {

            unicode = unicode.ToLower().Trim();
            unicode = Regex.Replace(unicode, "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
            unicode = Regex.Replace(unicode, "[๖ۣۜ]", "");
            unicode = Regex.Replace(unicode, "[óòỏõọôồốổỗộơớờởỡợ]", "o");
            unicode = Regex.Replace(unicode, "[éèẻẽẹêếềểễệ]", "e");
            unicode = Regex.Replace(unicode, "[íìỉĩị]", "i");
            unicode = Regex.Replace(unicode, "[úùủũụưứừửữự]", "u");
            unicode = Regex.Replace(unicode, "[ýỳỷỹỵ]", "y");
            unicode = Regex.Replace(unicode, "[đ]", "d");
            unicode = unicode.Replace(" ", "-").Replace("[()]", "");
            unicode = Regex.Replace(unicode, "[-\\s+/]+", "-");
            unicode = Regex.Replace(unicode, "\\W+", "-"); //Nếu bạn muốn thay dấu khoảng trắng thành dấu "_" hoặc dấu cách " " thì thay kí tự bạn muốn vào đấu "-"
            return unicode;
        }
        public static string ToEnglish(string s)
        {
            string sspace = s.Replace(" ", "");
            string slow = sspace.ToLower();
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = slow.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public static string ToUrlDecode(this string txt)
        {
            return HttpUtility.UrlDecode(txt);
        }
        public static string GetShortContent(string strContent, int Length)
        {
            if (strContent.Length < Length)
            {
                return strContent;
            }
            else
            {
                return (strContent.Substring(0, Length) + "...");
            }
        }

        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}