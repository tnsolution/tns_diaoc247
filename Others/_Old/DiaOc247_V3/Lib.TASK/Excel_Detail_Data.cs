﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Excel_Detail_Data
    {
        public static DataTable List(int Amount, string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM TASK_Excel_Detail WHERE Status = 0 AND Product LIKE @Name";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int Count_Task(int EmployeeKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(A.InfoKey)  FROM dbo.TASK_Note_Detail A RIGHT JOIN dbo.TASK_Note B ON A.NoteKey = B.NoteKey WHERE StatusInfo = 6";
            if (EmployeeKey != 0)
                zSQL += "AND B.SendTo = @EmployeeKey";
            // lay thong tin chua thuc hien va chua lien lac duoc
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = EmployeeKey;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Task(int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(A.InfoKey)  FROM dbo.TASK_Note_Detail A RIGHT JOIN dbo.TASK_Note B ON A.NoteKey = B.NoteKey WHERE StatusInfo = 6";
            if (EmployeeKey != 0)
                zSQL += "AND B.SendTo = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND B.CreatedDate BETWEEN @FromDate AND @ToDate";
            // lay thong tin chua thuc hien va chua lien lac duoc
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Task(int DepartmentKey, bool ViewDepartment)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @" SELECT COUNT(A.InfoKey) 
  FROM TASK_Note_Detail A 
  LEFT JOIN TASK_Note B ON A.NoteKey = B.NoteKey 
  LEFT JOIN HRM_Employees C ON C.EmployeeKey = B.SendTo
  WHERE StatusInfo = 6 AND C.DepartmentKey = @DepartmentKey";
          
            // lay thong tin chua thuc hien va chua lien lac duoc
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);                
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
             
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Task(int DepartmentKey, bool ViewDepartment, DateTime FromDate, DateTime ToDate)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @" SELECT COUNT(A.InfoKey) 
  FROM TASK_Note_Detail A 
  LEFT JOIN TASK_Note B ON A.NoteKey = B.NoteKey 
  LEFT JOIN HRM_Employees C ON C.EmployeeKey = B.SendTo
  WHERE StatusInfo = 6 AND C.DepartmentKey = @DepartmentKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND B.CreatedDate BETWEEN @FromDate AND @ToDate";
            // lay thong tin chua thuc hien va chua lien lac duoc
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Amount(string ProjectName)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(A.InfoKey)  FROM dbo.TASK_Excel_Detail A WHERE Product LIKE @Product AND Status = 0";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = "%" + ProjectName + "%";
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }        
    }
}
