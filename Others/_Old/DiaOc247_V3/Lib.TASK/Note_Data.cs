﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.TASK
{
    public class Note_Data
    {
        public static DataTable GetNearestInfo(int Employee)
        {
            return new SqlContext().GetData(@"
SELECT 'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room,
A.ApartmentStatus AS StatusKey,		
A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName, 	
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath,
B.OldStaff, B.NewStaff
FROM PUL_Resale_Apartment A RIGHT JOIN SYS_LogTranfer B ON B.ObjectID = A.AssetKey 
WHERE A.IsResale = 1 AND B.Confirmed = 0 AND B.NewStaff = " + Employee);
        }
        public static DataTable GetNearestTask(int ProjectKey, string AssetID, int InfoKey)
        {
            return new SqlContext().GetData("SELECT * FROM dbo.GetTaskEarlier(" + ProjectKey + ",N'" + AssetID + "'," + InfoKey + ")");
        }

        public static DataTable ListInfo_Done(int InfoKey)
        {
            return new SqlContext().GetData(@"
SELECT TOP 1 A.InfoKey, B.DateRespone,
dbo.FNC_GetStatusInfoData(B.[Description]) AS Respone,
dbo.FNC_GetNameEmployee(c.SendTo) AS EmployeeName
FROM TASK_Excel_Detail A 
LEFT JOIN TASK_Note_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON C.NoteKey = B.NoteKey
WHERE A.InfoKey = " + InfoKey + " ORDER BY DateRespone DESC");
        }

        //----------------------------- V3
        public static DataTable ListInfo_New(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.InfoKey AS [KEY], '0' AS [RelateKey], '2' AS [TYPE], '' [Status], '' EmployeeName,
A.FullName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product, "" ObjectKey, "" ObjectTable, A.AssetType, A.AssetTable, ProductID
FROM TASK_Excel_Detail A
WHERE A.Status = 0";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_New(string ProjectKey, string AssetName, string CustomerName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.InfoKey AS [KEY], '2' AS [TYPE], 
'0' AS [RelateKey], '' [Status], '' EmployeeName,
A.FullName AS CustomerName, A.AssetType, A.AssetTable,
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product, '' ObjectKey, '' ObjectTable, ProductID
FROM TASK_Excel_Detail A
WHERE A.Status = 0";
            if (ProjectKey != "0")
                zSQL += " AND A.ProductID = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND A.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (A.LastName LIKE @Name OR A.FirstName LIKE @Name)";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectKey;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_New(int Amount, string ProjectName, string AssetName, string CustomerName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.InfoKey AS [KEY], '2' AS [TYPE], '0' AS [RelateKey], '' [Status], '' EmployeeName,
A.FullName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product, '' ObjectKey, '' ObjectTable, ProductID
FROM TASK_Excel_Detail A
WHERE A.Status = 0";
            if (ProjectName != string.Empty)
                zSQL += " AND A.Product = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND A.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (A.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListInfo_New(int Amount, int Project, string AssetName, string CustomerName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.InfoKey AS [KEY], '2' AS [TYPE], '0' AS [RelateKey], '' [Status], '' EmployeeName,
A.FullName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product, '' ObjectKey, '' ObjectTable, ProductID
FROM TASK_Excel_Detail A
WHERE A.Status = 0";
            if (Project != 0)
                zSQL += " AND A.ProductID = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND A.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (A.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = Project;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_New(int Amount, string ProjectName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.InfoKey AS [KEY], '0' AS [RelateKey], '2' AS [TYPE], '' [Status], '' EmployeeName,
A.FullName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product, "" ObjectKey, "" ObjectTable
FROM TASK_Excel_Detail A
WHERE A.Status = 0";
            if (ProjectName != string.Empty)
                zSQL += " AND A.Product = @Product";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListInfo_Sent(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.DetailKey AS [Key], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status],
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.FullName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product, A.ObjectKey, A.ObjectTable, B.AssetType, B.AssetTable, B.ProductID
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey 
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey 
WHERE B.Status = 1 AND A.StatusInfo = 6
ORDER BY A.DateRespone DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_Sent(int Amount, int Department, int CurrentEmployee)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.DetailKey AS [Key], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status],
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.FullName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product, A.ObjectKey, A.ObjectTable, B.AssetType, B.AssetTable, B.ProductID
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey 
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey 
LEFT JOIN HRM_Employees D ON D.EmployeeKey = C.SendTo
WHERE B.Status = 1 AND A.StatusInfo <> 99";

            if (CurrentEmployee != 0)
                zSQL += " AND C.SendTo = @Employee";
            if (Department != 0)
                zSQL += " AND D.DepartmentKey = @Department";
            zSQL += " ORDER BY A.DateRespone DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = CurrentEmployee;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_Sent(string ProjectKey, string AssetName, string CustomerName, int EmployeeKey, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.DetailKey AS [KEY], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status], 
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.FullName AS CustomerName, B.AssetType, B.AssetTable,
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product, A.ObjectKey, A.ObjectTable, B.ProductID
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey WHERE B.Status = 1 AND A.StatusInfo <> 99";
            if (EmployeeKey != 0)
                zSQL += " AND C.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND A.StatusInfo = @Status";
            if (ProjectKey != "0")
                zSQL += " AND B.ProductID = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND B.Asset LIKE @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (B.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            zSQL += " ORDER BY A.DateRespone DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectKey;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = "%" + AssetName + "%";
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListInfo_Sent(int Amount, int Project, string AssetName, string CustomerName, int EmployeeKey, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP " + Amount + @" A.DetailKey AS [KEY], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status], 
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.FullName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product, A.ObjectKey, A.ObjectTable, B.ProductID
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey WHERE B.Status = 1 AND A.StatusInfo <> 99";
            if (EmployeeKey != 0)
                zSQL += " AND C.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND A.StatusInfo = @Status";
            if (Project != 0)
                zSQL += " AND B.ProductID = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND B.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (B.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            zSQL += " ORDER BY A.DateRespone DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = Project;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListInfo_Sent(int Amount, string ProjectName, string AssetName, string CustomerName, int EmployeeKey, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP " + Amount + @" A.DetailKey AS [KEY], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status], 
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.FullName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product, A.ObjectKey, A.ObjectTable
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey WHERE B.Status = 1 AND A.StatusInfo <> 99";
            if (EmployeeKey != 0)
                zSQL += " AND C.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND A.StatusInfo = @Status";
            if (ProjectName != string.Empty)
                zSQL += " AND B.Product = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND B.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (B.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            zSQL += " ORDER BY A.DateRespone DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }      
        //------------------------------        
    }
}
