﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Plan_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.EmployeeKey, A.PlanDate, dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, A.PlanKey FROM HRM_Plan A ORDER BY MONTH(A.PlanDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int DepartmentKey, int EmployeeKey, DateTime Date)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.EmployeeKey, A.PlanDate, dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, A.PlanKey FROM HRM_Plan A WHERE 1 = 1";
            if (EmployeeKey != 0)
            {
                zSQL += " AND EmployeeKey = @EmployeeKey";
            }
            if (Date != null)
            {
                zSQL += " AND MONTH(PlanDate) = @Month AND YEAR(PlanDate) = @Year";
            }
            zSQL += " ORDER BY MONTH(A.PlanDate) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Date.Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Date.Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
