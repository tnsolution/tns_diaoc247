﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.TASK
{
    public class Report
    {
        public static DataTable TableReport_Mobi(int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {        
            int Month = ToDate.Month;

            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT A.CategoryKey, A.CategoryName, '' Result, SUM(A.Value) AS Require, C.ProductID
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey 
    AND MONTH(B.PlanDate) = @Month
    GROUP BY CategoryKey, ProductID, CategoryName
	UNION ALL
	SELECT A.CategoryKey, A.CategoryName, SUM(A.Result) AS Result ,''Require, C.ProductID
	FROM TASK_Report_Detail A 
	LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey
    AND B.CreatedDate BETWEEN @FromDate AND @ToDate
	GROUP BY CategoryKey, ProductID, CategoryName
) 
SELECT CAST( ProductID AS INT) AS ID, CategoryName, SUM(Result) AS Result, SUM(Require) AS Require FROM X
GROUP BY ProductID, CategoryName
ORDER BY ID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable TableReport(int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT A.CategoryName, '' Result, A.Value AS Require 
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey WHERE B.EmployeeKey = @EmployeeKey	
	UNION ALL
	SELECT A.CategoryName, SUM(A.Result) AS Result ,''Require
	FROM TASK_Report_Detail A 
	LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey
	WHERE B.EmployeeKey = @EmployeeKey
	GROUP BY CategoryName
) 
SELECT CategoryName, SUM(Result) AS Result, SUM(Require) AS Require FROM X
GROUP BY CategoryName
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TableReport(int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            int Month = FromDate.Month;

            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT A.CategoryKey, A.CategoryName, '' Result, SUM(A.Value) AS Require, C.ProductID
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey 
    AND MONTH(B.PlanDate) = @Month
    GROUP BY CategoryKey, ProductID, CategoryName
	UNION ALL
	SELECT A.CategoryKey, A.CategoryName, SUM(A.Result) AS Result ,''Require, C.ProductID
	FROM TASK_Report_Detail A 
	LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey
    AND B.CreatedDate BETWEEN @FromDate AND @ToDate
	GROUP BY CategoryKey, ProductID, CategoryName
) 
SELECT CAST( ProductID AS INT) AS ID, CategoryName, SUM(Result) AS Result, SUM(Require) AS Require FROM X
GROUP BY ProductID, CategoryName
ORDER BY ID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TableReport(int EmployeeKey, int FromMonth, int FromYear, int ToMonth, int ToYear)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT A.CategoryName, '' Result, SUM(A.Value) AS Require
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
	WHERE B.EmployeeKey = @EmployeeKey 
    AND MONTH(B.PlanDate) BETWEEN @FromMonth AND @ToMonth
    AND YEAR(B.PlanDate) BETWEEN @FromYear AND @ToYear
    GROUP BY CategoryName
	UNION ALL
	SELECT A.CategoryName, SUM(A.Result) AS Result ,''Require
	FROM TASK_Report_Detail A 
	LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey
	WHERE B.EmployeeKey = @EmployeeKey
    AND MONTH(B.CreatedDate) BETWEEN @FromMonth AND @ToMonth
    AND MONTH(B.CreatedDate) BETWEEN @FromYear AND @ToYear
	GROUP BY CategoryName
) 
SELECT CategoryName, SUM(Result) AS Result, SUM(Require) AS Require FROM X
GROUP BY CategoryName
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromMonth", SqlDbType.Int).Value = FromMonth;
                zCommand.Parameters.Add("@FromYear", SqlDbType.Int).Value = FromYear;
                zCommand.Parameters.Add("@ToMonth", SqlDbType.Int).Value = ToMonth;
                zCommand.Parameters.Add("@ToYear", SqlDbType.Int).Value = ToYear;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int GetRequire_Trade(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            int Result = 0;
            string zSQL = @"
	SELECT SUM(A.Value) AS Require 
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN HRM_Employees C ON B.EmployeeKey = C.EmployeeKey
    WHERE A.CategoryKey = 298 AND (B.PlanDate BETWEEN @FromDate AND @ToDate)";
            if (EmployeeKey != 0)
                zSQL += " AND B.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                Result = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return Result;
        }

        public static DataTable ChartReport_Project(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT SUM(A.Income) AS INCOME, B.ProjectName
FROM FNC_Transaction A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
WHERE A.IsApproved = 1 AND IsFinish = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " GROUP BY B.ProjectName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChartReport_Department(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT SUM(A.Income) AS INCOME, C.DepartmentName
FROM FNC_Transaction A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN dbo.HRM_Departments C ON A.DepartmentKey = C.DepartmentKey
WHERE A.IsApproved = 1 AND IsFinish = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " GROUP BY C.DepartmentName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChartReport_Employee(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT SUM(A.Income) AS INCOME, C.LastName + ' ' + C.FirstName AS EmployeeName
FROM FNC_Transaction A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN dbo.HRM_Employees C ON C.EmployeeKey = A.EmployeeKey
WHERE A.IsApproved = 1 AND IsFinish = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " GROUP BY C.LastName, C.FirstName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
