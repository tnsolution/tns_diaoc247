﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Report_Rec_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM TASK_Report_Detail ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Key)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ID, A.ReportKey, A.CategoryKey, A.Result,
dbo.FNC_SysCategoryName(A.CategoryKey) AS CategoryName 
FROM TASK_Report_Detail A 
LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey 
WHERE B.ReportKey = @Key";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = Key;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Key, int Month, int Year)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ID, A.ReportKey, A.CategoryKey, A.Result, 
dbo.FNC_GetDoingReport(B.EmployeeKey, A.CategoryKey, @Month, @Year) AS Doing,
dbo.FNC_GetRequireReport(B.EmployeeKey,A.CategoryKey, @Month, @Year) AS Require,
dbo.FNC_SysCategoryName(A.CategoryKey) AS CategoryName 
FROM TASK_Report_Detail A 
LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey 
WHERE B.ReportKey = @Key";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = Key;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Key, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.ID, A.ReportKey, A.CategoryKey, A.Result, 
dbo.FNC_GetDoingReport(B.EmployeeKey, A.CategoryKey, @Month, @Year) AS Doing,
dbo.FNC_GetRequireReport(B.EmployeeKey,A.CategoryKey, @Month, @Year) AS Require,
dbo.FNC_SysCategoryName(A.CategoryKey) AS CategoryName 
FROM TASK_Report_Detail A 
LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey 
WHERE B.EmployeeKey = @Employee AND B.CreatedDate BETWEEN @Fromdate AND @Todate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Key;
                zCommand.Parameters.Add("@Fromdate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@Todate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = FromDate.Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = FromDate.Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListMobile(int Key, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.CategoryKey, SUM(A.Result) AS Result, 
dbo.FNC_GetRequireReport(B.EmployeeKey,A.CategoryKey, @Month, @Year) AS Require,
dbo.FNC_SysCategoryName(A.CategoryKey) AS CategoryName 
FROM TASK_Report_Detail A 
LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey 
WHERE B.EmployeeKey = @Employee AND B.CreatedDate BETWEEN @Fromdate AND @Todate
GROUP BY A.CategoryKey, EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Key;
                zCommand.Parameters.Add("@Fromdate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@Todate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = FromDate.Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = FromDate.Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}