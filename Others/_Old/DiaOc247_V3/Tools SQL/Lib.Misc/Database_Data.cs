﻿using System.Data;
using System.Data.SqlClient;
using TNConfig;

namespace Lib.Misc
{
    public class Database_Data
    {
        public static DataTable GetAssetList(int ProjectKey)
        {
            SqlConnection zCon = new SqlConnection();
            zCon.ConnectionString = ConnectDataBase.ConnectionString;
            zCon.Open();
            DataTable zTable = new DataTable();
            string sql = @"SELECT AssetID, COUNT(AssetID) FROM PUL_Resale_Apartment WHERE ProjectKey = " + ProjectKey + " GROUP BY AssetID HAVING COUNT(AssetID) > 1";
            SqlCommand zCMD = new SqlCommand(sql, zCon);
            zCMD.CommandType = CommandType.Text;
            SqlDataAdapter sqlda = new SqlDataAdapter(zCMD);
            sqlda.Fill(zTable);
            sqlda.Dispose();
            zCMD.Dispose();
            zCon.Close();
            return zTable;
        }
        public static DataTable GetAssetDuplicate(string Name)
        {
            SqlConnection zCon = new SqlConnection();
            zCon.ConnectionString = ConnectDataBase.ConnectionString;
            zCon.Open();
            DataTable zTable = new DataTable();
            string sql = @"SELECT AssetID, AssetKey FROM PUL_Resale_Apartment WHERE AssetID LIKE N'%" + Name + "%'";
            SqlCommand zCMD = new SqlCommand(sql, zCon);
            zCMD.CommandType = CommandType.Text;
            SqlDataAdapter sqlda = new SqlDataAdapter(zCMD);
            sqlda.Fill(zTable);
            sqlda.Dispose();
            zCMD.Dispose();
            zCon.Close();
            return zTable;
        }
        public static DataTable GetAsset(int Key)
        {
            SqlConnection zCon = new SqlConnection();
            zCon.ConnectionString = ConnectDataBase.ConnectionString;
            zCon.Open();
            DataTable zTable = new DataTable();
            string sql = @"SELECT * FROM PUL_Resale_Apartment WHERE AssetKey =" + Key;
            SqlCommand zCMD = new SqlCommand(sql, zCon);
            zCMD.CommandType = CommandType.Text;
            SqlDataAdapter sqlda = new SqlDataAdapter(zCMD);
            sqlda.Fill(zTable);
            sqlda.Dispose();
            zCMD.Dispose();
            zCon.Close();
            return zTable;
        }

        public static DataTable GetTable()
        {
            SqlConnection zCon = new SqlConnection();
            zCon.ConnectionString = ConnectDataBase.ConnectionString;
            zCon.Open();
            DataTable zTable = new DataTable();
            string sql = "SELECT NAME FROM SYS.TABLES ORDER BY NAME ASC";
            SqlCommand zCMD = new SqlCommand(sql, zCon);
            zCMD.CommandType = CommandType.Text;
            SqlDataAdapter sqlda = new SqlDataAdapter(zCMD);
            sqlda.Fill(zTable);
            sqlda.Dispose();
            zCMD.Dispose();
            zCon.Close();
            return zTable;
        }

        public static DataTable GetData(string SQL)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetData(SQL);
        }
        public static int Exsist(string AssetID, string Project)
        {
            SqlContext Sql = new SqlContext();
            bool Result = Sql.IsExist("SELECT COUNT(A.InfoKey) FROM TASK_Excel_Detail A WHERE A.Product = '" + Project + "' AND Asset = '" + AssetID + "'");
            if (Result)
                return 1;
            else
                return 0;
        }

    }
}
