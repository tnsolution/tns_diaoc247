﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TNConfig;

namespace Lib.Misc
{
    public class Product_Data
    {
        public static DataTable Get(int Project)
        {
            SqlConnection zCon = new SqlConnection();
            zCon.ConnectionString = ConnectDataBase.ConnectionString;
            zCon.Open();
            DataTable zTable = new DataTable();
            string sql = @"
SELECT A.AssetKey, A.ProjectKey, A.CategoryKey, A.AssetID, ID1,ID2,ID3,  A.NumberBed AS Room, A.WallArea AS AreaWall, 
A.WaterArea AS AreaWater,A.PricePurchase,A.DirectionDoor AS Door,A.DirectionView AS [View],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName
FROM PUL_Resale_Apartment A WHERE A.ProjectKey = " + Project;

            SqlCommand zCMD = new SqlCommand(sql, zCon);
            zCMD.CommandType = CommandType.Text;
            SqlDataAdapter sqlda = new SqlDataAdapter(zCMD);
            sqlda.Fill(zTable);
            sqlda.Dispose();
            zCMD.Dispose();
            zCon.Close();
            return zTable;
        }
    }
}
