﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;

namespace TNConfig
{
    public class Misc
    {
        string mFromatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public static string ErrorShow = "Lỗi xin liên hệ kỹ thuật. ";

        /// <summary>
        /// Ghi lỗi ra txt
        /// </summary>
        /// <param name="exp">lỗi</param>
        public static void CreateLogFiles(string exp)
        {
            if (System.IO.File.Exists(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log"))
            {
                using (FileStream fs = new FileStream(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log", FileMode.Append))
                {
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(exp + " : " + DateTime.Now.ToString() + Environment.NewLine);
                    sw.Close();
                }
            }
            else
            {
                using (FileStream fs = new FileStream(Environment.CurrentDirectory + DateTime.Now.ToString("dd-MM-yyyy") + "err.log", FileMode.OpenOrCreate))
                {
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(exp + " : " + DateTime.Now.ToString() + Environment.NewLine);
                    sw.Close();
                }
            }
        }

        public static DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1, 0, 0, 0, 0);
        }

        public static DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
            DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1, 23, 59, 59, 0);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        public string MoneyTextBox(string txt)
        {
            double Amount;
            if (!double.TryParse(txt, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            return Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }

        public string DecimalTextbox(string txt)
        {
            float text;
            if (!float.TryParse(txt, NumberStyles.Any, mFormatProviderDecimal, out text))
                text = 0;
            return text.ToString(mFromatDecimal, mFormatProviderDecimal);
        }

        #region Dùng để tìm không dấu
        char[] chrMangSoSanh = new char[] {
            'á', 'à', 'ả', 'ã', 'ạ',
            'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ',
            'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ',//16        
            'í', 'ì', 'ỉ', 'ĩ', 'ị',//21
            'ú', 'ù', 'ủ', 'ũ', 'ụ',
            'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự',//32
            'é', 'è', 'ẻ', 'ẽ', 'ẹ',
            'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ',//43
            'ó', 'ò', 'ỏ', 'õ', 'ọ',
            'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ',
            'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ',//60
            'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ',//65
            'đ'//66
        };
        char ReplaceChar(char chrIn)
        {
            char c = chrIn;
            for (int i = 0; i < chrMangSoSanh.Length; i++)
            {
                if (chrIn.ToString().ToLower() == chrMangSoSanh[i].ToString().ToLower())
                {
                    if (i <= 16)
                    {
                        c = 'a';
                    }
                    else if (i <= 21)
                    {
                        c = 'i';
                    }
                    else if (i <= 32)
                    {
                        c = 'u';
                    }
                    else if (i <= 43)
                    {
                        c = 'e';
                    }
                    else if (i <= 60)
                    {
                        c = 'o';
                    }
                    else if (i <= 65)
                    {
                        c = 'y';
                    }
                    else
                    {
                        c = 'd';
                    }
                    break;
                }
            }
            return c;
        }
        public string GetNonUnicode(string strInput)
        {
            char[] chrArr = strInput.ToCharArray();
            string s = "";
            for (int i = 0; i < chrArr.Length; i++)
            {
                s += ReplaceChar(chrArr[i]);
            }
            return s;
        }

        /// <summary>
        /// chuyen co dau sang khong dau
        /// </summary>
        /// <param name="s">chuoi dau vao</param>
        /// <returns>chuoi dau ra chuyen sang lower</returns>
        public static string ConvertToNonUnicode(string s)
        {
            string sspace = s.Replace(" ", "");
            string slow = sspace.ToLower();
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = slow.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        #endregion
    }
}
