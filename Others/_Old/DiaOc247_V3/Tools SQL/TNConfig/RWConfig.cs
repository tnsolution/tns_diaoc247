﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TNConfig
{
    public class RWConfig
    {
        private string _FileName = Path.GetTempPath() + "trivietLogistic7D46F736-6D5D-4A63-8863-351DC5E38E93.dll";
        private string _Keys = "151019752801197824012008";
        private ConnectDataBaseInfo _ConnectInfo;

        public RWConfig()
        {
            _ConnectInfo = new ConnectDataBaseInfo();
        }
        public RWConfig(string strConnect, bool IsConnectLocal)
        {
            _ConnectInfo = new ConnectDataBaseInfo();
            string[] strTemp;
            strTemp = strConnect.Split(';');
            if (IsConnectLocal)
            {
                _ConnectInfo.DataSource = strTemp[0];
                _ConnectInfo.AttachDbFilename = strTemp[1]; ;
            }
        }
        public ConnectDataBaseInfo ConnectInfo
        {
            get
            {
                return _ConnectInfo;
            }
            set
            {
                _ConnectInfo = value;
            }
        }

        public void SaveConfig()
        {
            WriteFile(HashStringConnectInfo(_ConnectInfo));
        }
        public bool ReadConfig()
        {
            bool nResult = false;
            string strConnect = "";
            if (File.Exists(_FileName))
            {
                FileStream fBin = new FileStream(_FileName, FileMode.Open);

                fBin.Position = 0;
                BinaryReader nRead = new BinaryReader(fBin);

                strConnect = nRead.ReadString();

                nRead.Close();
                fBin.Close();
                nResult = true;
                _ConnectInfo = InputInfo(UnHashStringConnectInfo(strConnect));
            }
            else
            {
                _ConnectInfo = new ConnectDataBaseInfo();
                nResult = false;
            }
            return nResult;
        }

        private void WriteFile(string strConnectInfo)
        {
            string nResult = "";
            try
            {
                FileStream fBin = new FileStream(_FileName, FileMode.OpenOrCreate);
                BinaryWriter w = new BinaryWriter(fBin);

                w.Write(strConnectInfo);
                w.Flush();
                w.Close();
                fBin.Close();
            }
            catch (Exception ex)
            {
                nResult = ex.ToString();
            }

        }
        private string HashStringConnectInfo(ConnectDataBaseInfo ConnectInfo)
        {
            string nTemp = RandomString(5000);
            string nResult = "", strConnect = "", nSize;
            Random random = new Random();
            strConnect += ConnectInfo.DataSource + ";";
            strConnect += ConnectInfo.DataBase + ";";
            strConnect += ConnectInfo.UserName + ";";
            strConnect += ConnectInfo.Password + ";";
            strConnect += ConnectInfo.AttachDbFilename + ";";
            strConnect += ConnectInfo.IsConnectLocal.ToString() + ";";

            nSize = Convert.ToString(strConnect.Length);
            nSize = nSize.PadLeft(3, '0');

            strConnect = nSize + strConnect;

            int k = 0;
            for (int i = 0; i < strConnect.Length; i++)
            {
                nResult += strConnect[i].ToString();

                int nKey = int.Parse(_Keys.Substring(k, 2));
                nResult += nTemp.Substring(random.Next(4500), nKey);

                k = k + 2;
                if (k >= _Keys.Length)
                    k = 0;
            }


            return nResult;
        }
        private string UnHashStringConnectInfo(string strConnect)
        {
            string nResult = "";
            string nLenStrConnect = "";
            int nPosition = 0, k = 0;

            nLenStrConnect += strConnect[nPosition].ToString();
            //get length
            for (int i = 0; i < 2; i++)
            {
                nPosition += int.Parse(_Keys.Substring(k, 2)) + 1;
                nLenStrConnect += strConnect[nPosition].ToString();
                k = k + 2;
            }

            for (int i = 0; i < int.Parse(nLenStrConnect); i++)
            {
                nPosition += int.Parse(_Keys.Substring(k, 2)) + 1;
                nResult += strConnect[nPosition].ToString();
                k = k + 2;
                if (k >= _Keys.Length)
                    k = 0;
            }
            return nResult;
        }
        private ConnectDataBaseInfo InputInfo(string strConnect)
        {
            ConnectDataBaseInfo nConnect = new ConnectDataBaseInfo();
            string[] strTemp;
            strTemp = strConnect.Split(';');
            nConnect.DataSource = strTemp[0];
            nConnect.DataBase = strTemp[1];
            nConnect.UserName = strTemp[2];
            nConnect.Password = strTemp[3];
            nConnect.AttachDbFilename = strTemp[4];
            nConnect.IsConnectLocal = bool.Parse(strTemp[5]);

            return nConnect;
        }

        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder("");
            string nResult = "";
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(random.Next(30, 122)));
                nResult += ch.ToString();
            }
            return nResult;
        }        
    }
}
