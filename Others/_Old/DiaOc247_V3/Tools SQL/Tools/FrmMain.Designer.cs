﻿namespace Tools
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.connectCRMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_Connect = new System.Windows.Forms.ToolStripMenuItem();
            this.sảnPhẩmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_Log = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_Import = new System.Windows.Forms.ToolStripMenuItem();
            this.MN_CheckData = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectCRMToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(799, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // connectCRMToolStripMenuItem
            // 
            this.connectCRMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MN_Connect,
            this.sảnPhẩmToolStripMenuItem,
            this.MN_Export,
            this.MN_Import,
            this.MN_CheckData});
            this.connectCRMToolStripMenuItem.Name = "connectCRMToolStripMenuItem";
            this.connectCRMToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.connectCRMToolStripMenuItem.Text = "Menu";
            // 
            // MN_Connect
            // 
            this.MN_Connect.Name = "MN_Connect";
            this.MN_Connect.Size = new System.Drawing.Size(163, 22);
            this.MN_Connect.Text = "Kết nối dữ liệu";
            this.MN_Connect.Click += new System.EventHandler(this.MN_Connect_Click);
            // 
            // sảnPhẩmToolStripMenuItem
            // 
            this.sảnPhẩmToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MN_Cut,
            this.MN_Log});
            this.sảnPhẩmToolStripMenuItem.Name = "sảnPhẩmToolStripMenuItem";
            this.sảnPhẩmToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.sảnPhẩmToolStripMenuItem.Text = "Quản lý";
            // 
            // MN_Cut
            // 
            this.MN_Cut.Name = "MN_Cut";
            this.MN_Cut.Size = new System.Drawing.Size(178, 22);
            this.MN_Cut.Text = "Chuyển sản phẩm";
            this.MN_Cut.Click += new System.EventHandler(this.MN_Cut_Click);
            // 
            // MN_Log
            // 
            this.MN_Log.Name = "MN_Log";
            this.MN_Log.Size = new System.Drawing.Size(178, 22);
            this.MN_Log.Text = "Log sản phẩm";
            this.MN_Log.Click += new System.EventHandler(this.MN_Log_Click);
            // 
            // MN_Export
            // 
            this.MN_Export.Name = "MN_Export";
            this.MN_Export.Size = new System.Drawing.Size(163, 22);
            this.MN_Export.Text = "Export Data SP";
            this.MN_Export.Click += new System.EventHandler(this.MN_Export_Click);
            // 
            // MN_Import
            // 
            this.MN_Import.Name = "MN_Import";
            this.MN_Import.Size = new System.Drawing.Size(163, 22);
            this.MN_Import.Text = "Import Data SP";
            this.MN_Import.Click += new System.EventHandler(this.MN_Import_Click);
            // 
            // MN_CheckData
            // 
            this.MN_CheckData.Name = "MN_CheckData";
            this.MN_CheckData.Size = new System.Drawing.Size(163, 22);
            this.MN_CheckData.Text = "Check Data SP";
            this.MN_CheckData.Click += new System.EventHandler(this.MN_CheckData_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 568);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(799, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 9.25F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(150, 17);
            this.lblStatus.Text = "Tình trạng kết nối SQL";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 590);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.Name = "FrmMain";
            this.Text = "...";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem connectCRMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MN_Connect;
        private System.Windows.Forms.ToolStripMenuItem MN_Export;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripMenuItem MN_Import;
        private System.Windows.Forms.ToolStripMenuItem MN_CheckData;
        private System.Windows.Forms.ToolStripMenuItem sảnPhẩmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MN_Cut;
        private System.Windows.Forms.ToolStripMenuItem MN_Log;
    }
}