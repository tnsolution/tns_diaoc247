﻿using Lib.Misc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using TNLibrary.SYS;

namespace Tools
{
    public partial class FrmProductCut : Form
    {
        string _TableName = "";
        string _TableShort = "";
        List<string> _ListEmployee = new List<string>();
        public FrmProductCut()
        {
            InitializeComponent();
            InitListView(LVData);
        }
        private void FrmProductCut_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cboProject, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", true);
            LoadDataToToolbox.ComboBoxData(cboFromEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees", true);
            LoadDataToToolbox.ComboBoxData(cboToEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees", true);
        }
        private void btnView_Click(object sender, EventArgs e)
        {
            int ProjectKey = Convert.ToInt32(cboProject.SelectedValue);
            int EmployeeKey = Convert.ToInt32(cboFromEmployee.SelectedValue);

            Project_Info zProject = new Project_Info(ProjectKey);
            _TableName = zProject.MainTable;
            _TableShort = zProject.ShortTable;

            string SQL = @"
SELECT *, A.NumberBed AS Room, A.WallArea AS AreaWall, 
A.WaterArea AS AreaWater,A.PricePurchase, 
A.DirectionDoor AS Door,A.DirectionView AS[View], 
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName, 
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName 
FROM " + _TableName + " A WHERE A.ProjectKey =" + ProjectKey + " AND EmployeeKey = " + EmployeeKey;

            DataTable zTable = Database_Data.GetData(SQL);
            LoadDataListView(zTable);
        }
        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã căn";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tháp";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tầng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số căn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diện tích tim tường";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diện tích thông thủy";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá mua";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại sản phẩm";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phòng ngủ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hướng View";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hướng cửa";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        void LoadDataListView(DataTable InTable)
        {
            LVData.Items.Clear();
            int i = 1;
            foreach (DataRow nRow in InTable.Rows)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Tag = nRow["AssetKey"];
                lvi.Text = (i++).ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AssetID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID1"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID2"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID3"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AreaWall"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AreaWater"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PricePurchase"].ToDoubleString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Room"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["View"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Door"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LVData.Items.Add(lvi);
            }
        }
        private void chkCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCheck.Checked)
            {
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    LVData.Items[i].Checked = true;
                }
            }
            else
            {
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    LVData.Items[i].Checked = false;
                }
            }
        }
        private void btnCut_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có chắc chuyển hết thông tin !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string Update = "", Insert = "";
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    if (LVData.Items[i].Checked)
                    {
                        ListViewItem item = LVData.Items[i];
                        int AssetKey = Convert.ToInt32(item.Tag);
                        string AssetID = LVData.Items[i].SubItems[1].Text.Trim();
                        string AssetCategory = LVData.Items[i].SubItems[8].Text.Trim();

                        Update += @" 
Update PUL_Resale_Apartment 
SET EmployeeKey = " + cboToEmployee.SelectedValue + ", DepartmentKey = dbo.FNC_GetDepartment(" + cboToEmployee.SelectedValue + @") 
WHERE AssetKey = " + AssetKey + " AND EmployeeKey = " + cboFromEmployee.SelectedValue;

                        Insert += @"
INSERT INTO SYS_LogTranfer 
(OldStaff, NewStaff, ObjectTable, ObjectID, Note, Confirmed, CreatedDate, Project, AssetName, AssetCategory) VALUES 
(" + cboFromEmployee.SelectedValue + "," + cboToEmployee.SelectedValue + ",N'" + _TableShort + "'," + AssetKey + ",N'Cắt thông tin',0,GETDATE(), " + cboProject.SelectedValue + ", N'" + AssetID + "',N'" + AssetCategory + "')";

                    }
                }

                string Mess = UpdateData.InsertUpdate(Update);
                if (Mess != string.Empty)
                {
                    MessageBox.Show(Mess);
                    return;
                }

                Mess = UpdateData.InsertUpdate(Insert);
                if (Mess != string.Empty)
                {
                    MessageBox.Show(Mess);
                    return;
                }
                else
                {
                    MessageBox.Show("Thành công");
                }
            }
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            FrmEmployeeSelect frm = new FrmEmployeeSelect();
            frm.ShowDialog();
            _ListEmployee = frm.Employee;
            if (_ListEmployee.Count > 0)
            {
                if (MessageBox.Show("Bạn có muốn chia sẽ thông tin !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string Insert = "";
                    foreach (ListViewItem item in LVData.Items)
                    {
                        if (item.Checked)
                        {
                            int AssetKey = Convert.ToInt32(item.Tag);
                            foreach (string s in _ListEmployee)
                            {
                                int EmployeeKey = Convert.ToInt32(s);
                                Share_Permition_Info zInfo = new Share_Permition_Info(AssetKey, EmployeeKey, _TableShort);
                                if (zInfo.AutoKey > 0)
                                    break;

                                Insert += "INSERT INTO PUL_SharePermition (AssetKey ,EmployeeKey, ObjectTable ) VALUES (" + AssetKey + "," + EmployeeKey + ", N'" + _TableShort + "' ) ";
                            }
                        }
                    }

                    string Mess = UpdateData.InsertUpdate(Insert);
                    if (Mess != string.Empty)
                    {
                        MessageBox.Show(Mess);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Thành công");
                    }
                }
            }
        }
    }
}
