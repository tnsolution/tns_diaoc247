﻿using Lib.Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.SYS;
using OfficeOpenXml;
using System.IO;

namespace Tools
{
    public partial class FrmProductExport : Form
    {
        DataTable _TableData = new DataTable();
        public FrmProductExport()
        {
            InitializeComponent();
            InitListView(LVData);
            LoadDataToToolbox.ComboBoxData(cboProject, "SELECT ProjectKey, ProjectName FROM PUL_Project", false);
        }

        private void FrmProduct_Load(object sender, EventArgs e)
        {
            
        }

        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã căn";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tháp";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tầng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số căn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diện tích tim tường";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diện tích thông thủy";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá mua";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại sản phẩm";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phòng ngủ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hướng View";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hướng cửa";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //

            //colHead = new ColumnHeader();
            //colHead.Text = "Họ tên";
            //colHead.Width = 150;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "SĐT1";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "SĐT2";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Email1";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Email2";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "ĐC1";
            //colHead.Width = 300;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "ĐC2";
            //colHead.Width = 300;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);
        }
        void LoadDataListView(DataTable InTable)
        {
            LVData.Items.Clear();
            int i = 1;
            foreach (DataRow nRow in InTable.Rows)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Text = (i++).ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AssetID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID1"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID2"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID3"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AreaWall"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AreaWater"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PricePurchase"].ToDoubleString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Room"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["View"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Door"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LVData.Items.Add(lvi);
            }
        }

        private void btnViewData_Click(object sender, EventArgs e)
        {
            _TableData = Product_Data.Get(cboProject.SelectedValue.ToInt());
            LoadDataListView(_TableData);
        }

        private void btnExportData_Click(object sender, EventArgs e)
        {
            if (_TableData.Rows.Count > 0)
            {
                _TableData.Columns["AssetID"].ColumnName = "Mã căn";
                _TableData.Columns["ID1"].ColumnName = "Tháp";
                _TableData.Columns["ID2"].ColumnName = "Tầng";
                _TableData.Columns["ID3"].ColumnName = "Căn";
                _TableData.Columns["Room"].ColumnName = "Phòng";
                _TableData.Columns["AreaWall"].ColumnName = "DT tim tường";
                _TableData.Columns["AreaWater"].ColumnName = "DT thông thủy";
                _TableData.Columns["PricePurchase"].ColumnName = "Giá mua";
                _TableData.Columns["Door"].ColumnName = "Hướng cửa";
                _TableData.Columns["View"].ColumnName = "Hướng view";
                _TableData.Columns["CategoryName"].ColumnName = "Loại sản phẩm";
                _TableData.Columns["ProjectName"].ColumnName = "Dự án";

                SaveFileDialog zFBrowser = new SaveFileDialog();
                zFBrowser.InitialDirectory = @"C:\";
                zFBrowser.Title = "Save Excel Files";
                zFBrowser.DefaultExt = "xls";
                zFBrowser.Filter = "Excel 2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx";
                if (zFBrowser.ShowDialog() == DialogResult.OK)
                {
                    string Path = zFBrowser.FileName;
                    FileInfo fi = new FileInfo(Path);
                    try
                    {
                        using (ExcelPackage pck = new ExcelPackage(fi))
                        {
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Data");
                            ws.Cells.AutoFitColumns(100, 300);
                            ws.Cells["A1"].LoadFromDataTable(_TableData, true);
                            pck.Save();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    finally
                    {
                        _TableData = new DataTable();
                    }
                }
            }
            else
            {
                MessageBox.Show("Chưa có data", "Thông báo");
            }
        }
    }
}
