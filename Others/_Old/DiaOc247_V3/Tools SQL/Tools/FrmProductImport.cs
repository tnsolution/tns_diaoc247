﻿using Lib.Misc;
using System;
using System.Data;
using System.Windows.Forms;
using TNLibrary.SYS;

namespace Tools
{
    public partial class FrmProductImport : Form
    {
        DataTable _TableData = new DataTable();

        public FrmProductImport()
        {
            InitializeComponent();
            InitListView(LVData1);
            InitListView(LVData2);
            LoadDataToToolbox.ComboBoxData(cboProject, "SELECT ProjectKey, ProjectName FROM PUL_Project", false);
        }

        private void FrmProductImport_Load(object sender, EventArgs e)
        {

        }
        private void btnImportData_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "Excel 2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                DataTable zTable = Excel.ImportToDataTable(zOpf.FileName, string.Empty);
                LoadDataListView_Excel(zTable);
            }
        }
        private void btnViewData_Click(object sender, EventArgs e)
        {
            _TableData = Product_Data.Get(cboProject.SelectedValue.ToInt());
            LoadDataListView(_TableData);
        }
        private void btnUpdateData_Click(object sender, EventArgs e)
        {
            string SQL = "";
            for (int i = 0; i < LVData2.Items.Count; i++)
            {
                ListViewItem item = LVData2.Items[i];
                string[] temp = item.Tag.ToString().Split(',');
                int AssetKey = temp[0].ToInt();
                int ProjectKey = temp[1].ToInt();
                int CategoryKey = temp[2].ToInt();
                string AssetID = item.SubItems[1].Text;
                string ID1 = item.SubItems[2].Text;
                string ID2 = item.SubItems[3].Text;
                string ID3 = item.SubItems[4].Text;
                string Room = item.SubItems[5].Text;
                string AreaWall = item.SubItems[6].Text;
                string AreaWater = item.SubItems[7].Text;
                string Price = item.SubItems[8].Text;
                string Door = item.SubItems[9].Text;
                string View = item.SubItems[10].Text;

                if (AssetKey != 0)
                {
                    //update
                    SQL += "\r\n UPDATE PUL_Resale_Apartment SET";
                    SQL += " AssetID ='" + AssetID + "',";
                    SQL += " ID1 ='" + ID1 + "',";
                    SQL += " ID2 ='" + ID2 + "',";
                    SQL += " ID3 ='" + ID3 + "',";
                    SQL += " NumberBed =" + Room.ToInt() + ",";
                    SQL += " WallArea ='" + AreaWall + "',";
                    SQL += " WaterArea ='" + AreaWater + "',";
                    SQL += " PricePurchase ='" + Price + "',";
                    SQL += " DirectionDoor =N'" + Door + "',";
                    SQL += " DirectionView =N'" + View + "'";
                    SQL += " WHERE AssetKey =" + AssetKey;
                }
                else
                {
                    //insert
                    SQL += "\r\n INSERT PUL_Resale_Apartment ";
                    SQL += " (ProjectKey, CategoryKey, AssetID, ID1, ID2, ID3, A.NumberBed, A.WallArea, A.WaterArea, A.PricePurchase, A.DirectionDoor, A.DirectionView) ";
                    SQL += " VALUES ";
                    SQL += " (" + ProjectKey + "," + CategoryKey + ",'" + AssetID + "','" + ID1 + "','" + ID2 + "','" + ID3 + "'," + Room.ToInt() + ",'" + AreaWall + "','" + AreaWater + "','" + Price + "',N'" + Door + "',N'" + View + "') ";
                }
            }

            string Message = UpdateData.InsertUpdate(SQL);
            if (Message != string.Empty)
                MessageBox.Show(Message, "Lỗi nhập data");
            else
                MessageBox.Show("Đã nhập data thành công");
        }

        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã căn";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tháp";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tầng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số căn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phòng ngủ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diện tích tim tường";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diện tích thông thủy";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giá mua";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);     

            colHead = new ColumnHeader();
            colHead.Text = "Hướng cửa";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hướng View";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại sản phẩm";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //

            //colHead = new ColumnHeader();
            //colHead.Text = "Họ tên";
            //colHead.Width = 150;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "SĐT1";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "SĐT2";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Email1";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Email2";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "ĐC1";
            //colHead.Width = 300;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "ĐC2";
            //colHead.Width = 300;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);
        }
        void LoadDataListView(DataTable InTable)
        {
            LVData1.Items.Clear();
            int i = 1;
            foreach (DataRow nRow in InTable.Rows)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Text = (i++).ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AssetID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID1"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID2"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ID3"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Room"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AreaWall"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AreaWater"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PricePurchase"].ToDoubleString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Door"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["View"].ToString().Trim();
                lvi.SubItems.Add(lvsi);             

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);
                LVData1.Items.Add(lvi);
            }
        }
        void LoadDataListView_Excel(DataTable InTable)
        {
            LVData2.Items.Clear();
            int i = 1;
            foreach (DataRow nRow in InTable.Rows)
            {
                ListViewItem lvi;
                ListViewItem.ListViewSubItem lvsi;

                lvi = new ListViewItem();
                lvi.Tag = nRow[0].ToString() + "," + nRow[1].ToString() + "," + nRow[2].ToString();
                lvi.Text = (i++).ToString();

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[3].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[4].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[5].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[6].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[7].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[8].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[9].ToDoubleString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[10].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[11].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[12].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow[13].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LVData2.Items.Add(lvi);
            }
        }
    }
}
