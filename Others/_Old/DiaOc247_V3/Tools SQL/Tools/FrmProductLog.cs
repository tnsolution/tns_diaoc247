﻿using Lib.Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.SYS;

namespace Tools
{
    public partial class FrmProductLog : Form
    {
        public FrmProductLog()
        {
            InitializeComponent();
            InitListView(LVData);
        }

        private void FrmProductLog_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cboEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees", true);
        }

        void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "Số TT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân viên trước khi gửi";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân viên sau khi gửi";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }



        private void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime(dtFromDate.Value.Year, dtFromDate.Value.Month, dtFromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dtToDate.Value.Year, dtToDate.Value.Month, dtToDate.Value.Day, 23, 59, 59);
            int Employee = Convert.ToInt32(cboEmployee.SelectedValue);

            string SQL = "SELECT *, dbo.FNC_GetNameEmployee(OldStaff) OldName, dbo.FNC_GetNameEmployee(NewStaff) NewName FROM SYS_LogTranfer WHERE OldStaff =" + Employee + " AND CreatedDate BETWEEN '" + FromDate + "' AND '" + ToDate + "'";
            DataTable zTable = Database_Data.GetData(SQL);

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LVData.Items.Clear();
            int n = zTable.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = zTable.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = "";

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AssetName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OldName"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NewName"].ToString();
                lvi.SubItems.Add(lvsi);

                LVData.Items.Add(lvi);
            }
        }
    }
}
