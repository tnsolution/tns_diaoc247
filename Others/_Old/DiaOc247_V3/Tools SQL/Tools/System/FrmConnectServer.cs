using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using TNConfig;

namespace Tools
{
    public partial class FrmConnectServer : Form
    {
        public ConnectDataBaseInfo _ConnectDataBaseInfo = new ConnectDataBaseInfo();
        public bool _IsConnect = false;
        string strMess01 = "Kết nối thành công";
        string strMess02 = "Không thể kết nối, vui lòng chọn kiểu kết nối khác ";
        string strMess03 = "Connecting...";
        string strMess04 = "Bạn phải chọn File Data ";
        public FrmConnectServer()
        {
            InitializeComponent();
        }
        private void FrmConnectServer_Load(object sender, EventArgs e)
        {

            RWConfig nFileConfig = new RWConfig();
            nFileConfig.ReadConfig();
            _ConnectDataBaseInfo = nFileConfig.ConnectInfo;
            if (!_ConnectDataBaseInfo.IsConnectLocal)
            {
                radioConnectServer.Checked = true;
                coSQLServer.Text = _ConnectDataBaseInfo.DataSource;
                txtDataBase.Text = _ConnectDataBaseInfo.DataBase;
                txtUserName.Text = _ConnectDataBaseInfo.UserName;
                txtPass.Text = _ConnectDataBaseInfo.Password;
            }        
        }

        private void cmdConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }
        private void Connect()
        {
            this.Cursor = Cursors.WaitCursor;
            lblMessage.Text = strMess03;

            if (CheckConnect())
            {
                _IsConnect = true;
                this.DialogResult = DialogResult.Yes;
                this.Close();

            }
            else
            {
                _IsConnect = false;
                lblMessage.Text = strMess02;
            }
            this.Cursor = Cursors.Default;
        }
        private bool CheckConnect()
        {

            _ConnectDataBaseInfo = new ConnectDataBaseInfo();

            bool isConnected = false;

            if (radioConnectServer.Checked)
            {
                _ConnectDataBaseInfo.DataSource = coSQLServer.Text;
                _ConnectDataBaseInfo.DataBase = txtDataBase.Text;
                _ConnectDataBaseInfo.UserName = txtUserName.Text;
                _ConnectDataBaseInfo.Password = txtPass.Text;
                _ConnectDataBaseInfo.IsConnectLocal = false;
                _ConnectDataBaseInfo.AttachDbFilename = "Nofile";

                _ConnectDataBaseInfo.ConnectionString = "Data Source = " + _ConnectDataBaseInfo.DataSource + ";DataBase= " + _ConnectDataBaseInfo.DataBase + ";User=" + _ConnectDataBaseInfo.UserName + ";Password= " + _ConnectDataBaseInfo.Password;// +";Integrated Security=SSPI";

            }         
            SqlConnection nConSQL = new SqlConnection();
            try
            {
                nConSQL.ConnectionString = _ConnectDataBaseInfo.ConnectionString;
                nConSQL.Open();

                if (nConSQL.State == ConnectionState.Open)
                {
                    isConnected = true;
                    MessageBox.Show(strMess01);
                    RWConfig nFileConfig = new RWConfig();
                    nFileConfig.ConnectInfo = _ConnectDataBaseInfo;
                    nFileConfig.SaveConfig();
                }
            }
            catch (System.Exception err)
            {
                //   lbStatueConnect.Text = "Error Connect DataBase : " + err.Message;
                MessageBox.Show(err.Message);
                isConnected = false;
            }
            finally
            {
                nConSQL.Close();
            }
            return isConnected;
        }
    }
}