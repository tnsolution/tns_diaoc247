﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.ADM
{
    public partial class PositionEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AutoKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_AutoKey.Value != "0")
            {
                Positions_Info zInfo = new Positions_Info(HID_AutoKey.Value.ToInt());
                txt_Rank.Value = zInfo.Rank.ToString();
                txt_Name.Value = zInfo.Position;
                txt_Description.Value = zInfo.Description;

                Lit_TitlePage.Text = "Sửa thông tin " + zInfo.Position;
            }
        }

        [WebMethod]
        public static ItemResult SavePosition(int AutoKey, string Name, string Description, int Rank)
        {
            ItemResult zResult = new ItemResult();
            Positions_Info zInfo = new Positions_Info(AutoKey);
            zInfo.Position = Name;
            zInfo.Description = Description;
            zInfo.Rank = Rank.ToInt();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult DeletePosition(int AutoKey, int Rank)
        {
            ItemResult zResult = new ItemResult();
            if (Positions_Data.IsExists(Rank))
            {
                zResult.Message = "Chức vụ này đã được sử dụng, bạn không thể xóa !";
                return zResult;
            }

            Positions_Info zInfo = new Positions_Info();
            zInfo.Key = AutoKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}