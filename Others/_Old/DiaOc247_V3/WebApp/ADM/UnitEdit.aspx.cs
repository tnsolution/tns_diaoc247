﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.ADM
{
    public partial class UnitEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AutoKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_AutoKey.Value != "0")
            {
                Unit_Info zInfo = new Unit_Info(HID_AutoKey.Value.ToInt());
                txt_Rank.Value = zInfo.Rank.ToString();
                txt_Name.Value = zInfo.Unit;
                txt_Description.Value = zInfo.Description;

                Lit_TitlePage.Text = "Sửa thông tin " + zInfo.Unit;
            }
        }

        [WebMethod]
        public static ItemResult SaveUnit(int AutoKey, string Name, string Description, int Rank)
        {
            ItemResult zResult = new ItemResult();
            Unit_Info zInfo = new Unit_Info(AutoKey);
            zInfo.Unit = Name;
            zInfo.Description = Description;
            zInfo.Rank = Rank.ToInt();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult DeleteUnit(int AutoKey, int Rank)
        {
            ItemResult zResult = new ItemResult();
            if (Unit_Data.IsExists(Rank))
            {
                zResult.Message = "Cấp bậc này đã được sử dụng, bạn không thể xóa !";
                return zResult;
            }

            Unit_Info zInfo = new Unit_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}