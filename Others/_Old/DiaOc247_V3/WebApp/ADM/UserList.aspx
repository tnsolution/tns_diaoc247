﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="WebApp.ADM.UserList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Danh sách tài khoản              
                <span class="tools pull-right">
                    <a href="UserEdit.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $("#tblData tbody tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "UserEdit.aspx?ID=" + trid;
            });

            $("img[check]").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                var trid = $(this).closest('tr').attr('id');

                $.ajax({
                    type: 'POST',
                    url: '/SYS/UserList.aspx/SetStatus',
                    data: JSON.stringify({
                        'UserKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>
</asp:Content>
