﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.ADM
{
    public partial class UserList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = User_Data.List();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên tài khoản</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("        <th>Ngày đăng nhập</th>");
            zSb.AppendLine("        <th>Ngày hết hạn</th>");
            zSb.AppendLine("        <th>Số lần đăng nhập không đúng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    string img = "<img id='" + r["UserKey"].ToString() + "' check='" + r["Activated"].ToInt() + "' alt='' src='" + String.Format("../template/custom-image/{0}.png", r["Activated"].ToInt() == 1 ? "true" : "false") + "' />";
                    zSb.AppendLine("            <tr id='" + r["UserKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + i++ + "</td>");
                    zSb.AppendLine("            <td>" + r["UserName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + img + "</td>");
                    zSb.AppendLine("            <td>" + r["LastLoginDate"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["ExpireDate"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FailedPasswordAttemptCount"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemResult SetStatus(string UserKey)
        {
            ItemResult zResult = new ItemResult();
            User_Info zInfo = new User_Info();
            zInfo.Key = UserKey;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            zInfo.SetActivated();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}