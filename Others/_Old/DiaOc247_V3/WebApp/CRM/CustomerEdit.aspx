﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CustomerEdit.aspx.cs" Inherits="WebApp.CRM.CustomerEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSaveCustomer">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <a class="btn btn-white btn-default btn-bold" href="CustomerList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin cơ bản
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#want">
                                <i class="blue ace-icon fa fa-circle bigger-110"></i>
                                Quan tâm & chăm sóc
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info2">
                                <i class="blue ace-icon fa fa-phone bigger-110"></i>
                                Chi tiết liên lạc
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#share">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Chia thông tin
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane in active">
                            <div class="col-xs-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" custype="0">Họ tên</label>
                                        <label class="col-sm-3 control-label" custype="1">Đại diện doanh nghiệp</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="txt_CustomerName" placeholder="Nhập text" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">SĐT1</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Phone1" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">SĐT2</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nguồn khách</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Source" runat="server" CssClass="select2" AppendDataBoundItems="true" required>
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_SourceNote" placeholder="Chi tiết nguồn" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Mức độ tiềm năng</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Status" runat="server" CssClass="select2" AppendDataBoundItems="true" required>
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ghi chú</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="txt_Description" placeholder="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="want" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Thông tin quan tâm của khách hàng
                                              <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mRecord" data-toggle="modal">
                                                  <i class="ace-icon fa fa-plus"></i>
                                                  Add
                                              </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hr hr2 hr-double"></div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_TableWant" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="hr hr2 hr-double"></div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Nội dung làm việc, trao đổi với khách hàng
                                              <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mCare" data-toggle="modal" style="display: none">
                                                  <i class="ace-icon fa fa-plus"></i>
                                                  Add
                                              </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_TableCare" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="info2" class="tab-pane">
                            <div class="col-xs-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ngày sinh</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_Birthday" placeholder="Chọn ngày tháng năm" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">CMND/Passport</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_CardID" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ngày cấp</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_CardDate" placeholder="Chọn ngày tháng năm" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nơi cấp</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_CardPlace" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group" custype="1">
                                        <label class="col-sm-3 control-label">Tên doanh nghiệp</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="txt_CompanyName" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group" custype="1">
                                        <label class="col-sm-3 control-label">Mã số thuế</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Tax" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group" custype="0">
                                        <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="txt_Address1" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group" custype="0">
                                        <label class="col-sm-3 control-label">Địa chỉ liên lạc</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group" custype="1">
                                        <label class="col-sm-3 control-label">Địa chỉ</label>
                                        <div class="col-sm-8">
                                            <input type="text" runat="server" class="form-control" id="txt_Address3" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="share" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Cho phép nhân viên chia sẽ được cập nhật thông tin này</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-8">
                                            <button type='button' class='btn btn-white btn-default btn-round btn-sm' id='btnSaveShare'>
                                                <i class="ace-icon fa fa-plus"></i>Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_AllowSearch" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mCare" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Nội dung chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <textarea id="txt_Care" rows="5" cols="20" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveCare" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mRecord" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin quan tâm</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nhu cầu</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Want" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn nhu cầu--"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án</label>
                            <div class="col-sm-9">
                                <asp:ListBox ID="LB_Project" runat="server" SelectionMode="Multiple" CssClass="select2" data-placeholder="--Chọn--"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án khác</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại sản phẩm</label>
                            <div class="col-sm-9">
                                <asp:ListBox ID="LB_Category" runat="server" SelectionMode="Multiple" CssClass="form-control select2" data-placeholder="--Chọn--" Style="width: 100%"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Giá (VNĐ)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Price" placeholder="Nhập số" moneyinput value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Diện tích (m<sup>2</sup>)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Area" placeholder="Nhập số" areainput value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số PN</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Bed" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1PN"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2PN"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3PN"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4PN"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5PN"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nội thất</label>
                            <div class="col-sm-9">
                                <asp:ListBox ID="LB_Furniture" runat="server" SelectionMode="Multiple" CssClass="form-control select2" data-placeholder="--Chọn--" Style="width: 100%"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Quan tâm khác
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 60px;" placeholder="...."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveRecord" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>Cập nhật                   
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_RecordKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CareKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerType" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script src="/CRM/CustomerEdit.js"></script>
    <script>
        $(document).ready(function () {
            $('input[moneyinput]').number(true, 0);
            $('input[areainput]').number(true, 2);
        });
    </script>
</asp:Content>
