﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.CRM
{
    public partial class CustomerEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                {
                    HID_CustomerKey.Value = Request["ID"];
                    HID_CustomerType.Value = Request["Type"];
                }

                Tools.ListBox(LB_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName");
                Tools.ListBox(LB_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName");
                Tools.ListBox(LB_Furniture, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 7");
                Tools.DropDown_DDL(DDL_Want, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 14", false);
                Tools.DropDown_DDL(DDL_Status, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 12", false);
                Tools.DropDown_DDL(DDL_Source, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 11", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE isWorking = 2 ORDER BY LastName", true);

                LoadData();
                LoadWant();
                LoadCare();
                LoadShare();
            }
        }
        void LoadData()
        {
            Customer_Info zInfo = new Customer_Info(HID_CustomerKey.Value.ToInt());
            Lit_TitlePage.Text = "Cập nhật thông tin khách " + zInfo.CustomerName;
            HID_CustomerKey.Value = zInfo.Key.ToString();

            txt_CustomerName.Value = zInfo.CustomerName;
            txt_Address3.Value = zInfo.Address3;
            txt_Tax.Value = zInfo.TaxCode;
            txt_Birthday.Value = zInfo.Birthday.ToString("dd/MM/yyyy");
            txt_CardPlace.Value = zInfo.CardPlace;
            txt_CardDate.Value = zInfo.CardDate.ToString("dd/MM/yyyy");
            txt_CardID.Value = zInfo.CardID;
            txt_SourceNote.Value = zInfo.SourceNote;
            txt_Phone1.Value = zInfo.Phone1;
            txt_Phone2.Value = zInfo.Phone2;
            txt_CompanyName.Value = zInfo.CompanyName;
            txt_Address1.Value = zInfo.Address1;
            txt_Address2.Value = zInfo.Address2;

            DDL_Status.SelectedValue = zInfo.Status.ToString();
            DDL_Source.SelectedValue = zInfo.CategorySource.ToString();                       

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người khởi tạo:</td><td>" + zInfo.CreatedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày khởi tạo:</td><td>" + zInfo.CreatedDate + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }
        void LoadWant()
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemConsent> zList = Consents_Data.List(HID_CustomerKey.Value.ToInt());

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblWant'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Dự án</th>");
            zSb.AppendLine("        <th>Loại</th>");
            zSb.AppendLine("        <th>Giá</th>");
            zSb.AppendLine("        <th>Số phòng</th>");
            zSb.AppendLine("        <th>Ngày cập nhật</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int n = 1;
            foreach (ItemConsent r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.RecordKey + "'>");
                zSb.AppendLine("               <td>" + (n++) + "</td>");
                zSb.AppendLine("               <td>" + r.Note + "</td>");
                zSb.AppendLine("               <td>" + r.Project + "</td>");
                zSb.AppendLine("               <td>" + r.Category + "</td>");
                zSb.AppendLine("               <td class='giatien'>" + r.Price.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td>" + r.Room + "</td>");
                zSb.AppendLine("               <td>" + r.CreatedDate.ToDateString() + "</td>");
                zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDelWant' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_TableWant.Text = zSb.ToString();
        }
        void LoadCare()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zList = Care_Data.List(HID_CustomerKey.Value.ToInt());

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblCare'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Ngày cập nhật</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int n = 1;
            foreach (DataRow r in zList.Rows)
            {
                zSb.AppendLine("            <tr id='" + r["ID"].ToString() + "'>");
                zSb.AppendLine("               <td>" + (n++) + "</td>");
                zSb.AppendLine("               <td>" + r["ContentDetail"].ToString() + "</td>");
                zSb.AppendLine("               <td>" + r["CreatedDate"].ToDateString() + "</td>");
                zSb.AppendLine("               <td><div class='action-buttons pull-right'><a href='#' btn='btnDelCare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_TableCare.Text = zSb.ToString();
        }
        void LoadShare()
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(HID_CustomerKey.Value.ToInt(), "Customer");
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblShare'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>Chia thông tin cho </div><div class='profile-info-value'>" + zTable.Rows[i]["EmployeeName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDelShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            Lit_AllowSearch.Text = zSb.ToString();
        }
        #region [Share]
        [WebMethod]
        public static ItemResult SaveShare(int CustomerKey, string Type, int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            Share_Permition_Info zInfo = new Share_Permition_Info(CustomerKey, EmployeeKey, Type);
            if (zInfo.AutoKey > 0)
            {
                zResult.Message = "Trùng thông tin, vui lòng chọn lại";
                return zResult;
            }

            zInfo.ObjectTable = Type;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.AssetKey = CustomerKey;
            zInfo.Create();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemWeb[] GetShare(int CustomerKey, string Type)
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(CustomerKey, Type);
            List<ItemWeb> ListAsset = new List<ItemWeb>();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb iwe = new ItemWeb();
                iwe.Value = zTable.Rows[i]["AutoKey"].ToString();
                iwe.Text = zTable.Rows[i]["EmployeeName"].ToString();

                ListAsset.Add(iwe);
            }
            return ListAsset.ToArray();
        }
        [WebMethod]
        public static ItemResult DeleteShare(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            Share_Permition_Info zInfo = new Share_Permition_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        #endregion
        #region [Care]
        [WebMethod]
        public static ItemResult SaveCare(int AutoKey, int CustomerKey, string Content)
        {
            Care_Info zInfo = new Care_Info(AutoKey);
            zInfo.CustomerKey = CustomerKey;
            zInfo.ContentDetail = Content.Trim();
            zInfo.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.ModifiedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.Save();

            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult GetCare(int AutoKey)
        {
            Care_Info zInfo = new Care_Info(AutoKey);
            ItemResult zResult = new ItemResult();
            zResult.Result = zInfo.ContentDetail;
            return zResult;
        }
        [WebMethod]
        public static ItemResult DeleteCare(int AutoKey)
        {
            Care_Info zInfo = new Care_Info();
            zInfo.ID = AutoKey;
            zInfo.Delete();

            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string GetCareList(int CustomerKey)
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zList = Care_Data.List(CustomerKey);
            int n = 1;
            foreach (DataRow r in zList.Rows)
            {
                zSb.AppendLine("            <tr id='" + r["ID"].ToString() + "'>");
                zSb.AppendLine("               <td>" + (n++) + "</td>");
                zSb.AppendLine("               <td>" + r["ContentDetail"].ToString() + "</td>");
                zSb.AppendLine("               <td>" + r["CreatedDate"].ToDateString() + "</td>");
                zSb.AppendLine("               <td><div class='action-buttons pull-right'><a href='#' btn='btnDelCare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }
        #endregion
        [WebMethod]
        public static ItemResult InitCustomer(string Type, string Trade, string Name)
        {
            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info(Name, string.Empty);
            if (zInfo.Key > 0)
                zResult.Message = "Đã hiện hữu thông tin khách tên [" + Name + "] !.";
            else
            {
                zInfo = new Customer_Info();
                zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
                zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.CategoryKey = Type.ToInt();
                zInfo.CustomerName = Name;
                zInfo.Create();
            }

            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveCustomer(string CustomerKey, string CustomerName, string Address3, string Tax, string Birthday, string CardID, string CardDate, string CardPlace, string Source, string SourceNote, string WantShow, string WantSearch, string Status, string Description, string Phone1, string Phone2, string CompanyName, string Address1, string Address2)
        {
            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey.ToInt());
            zInfo.WantSearch = WantSearch;
            zInfo.WantShow = WantShow;
            zInfo.Status = Status.ToInt();
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.CompanyName = CompanyName;
            zInfo.TaxCode = Tax;
            zInfo.Phone1 = Phone1;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.Address3 = Address3;
            zInfo.Description = Description;
            zInfo.SourceNote = SourceNote;
            zInfo.CategorySource = Source.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult DeleteCustomer(string CustomerKey)
        {
            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info();
            zInfo.Key = CustomerKey.ToInt();
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveRecord(int CustomerKey, int RecordKey, string Note, string ProjectKey, string Project, string ProjectOther, string CategoryKey, string Category, string FurnitureKey, string Furniture, string Room, string Price, string Area, string WantKey)
        {
            ItemResult zResult = new ItemResult();
            Consents_Info zConsent = new Consents_Info(RecordKey);
            zConsent.Note = Note;
            zConsent.CategoryNeed = WantKey;
            zConsent.Price = Convert.ToDouble(Price);
            zConsent.ConsentBedRoom = Room;
            zConsent.ConsentAera = Area;
            zConsent.CategoryInside = FurnitureKey;
            zConsent.Name_Inside = Furniture;
            zConsent.Project = ProjectKey;
            zConsent.ProjectOther = ProjectOther;
            zConsent.Name_Project = Project;
            zConsent.CategoryAsset = CategoryKey;
            zConsent.Name_Asset = Category;
            zConsent.CustomerKey = CustomerKey;
            zConsent.Save();
            zResult.Message = zConsent.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult DeleteRecord(int RecordKey)
        {
            ItemResult zResult = new ItemResult();
            Consents_Info zInfo = new Consents_Info();
            zInfo.RecordKey = RecordKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemConsent GetRecord(int RecordKey)
        {
            Consents_Info zInfo = new Consents_Info(RecordKey);
            ItemConsent zConsent = new ItemConsent();
            zConsent.Want = zInfo.CategoryNeed;
            zConsent.RecordKey = zInfo.RecordKey.ToString();
            zConsent.ProjectKey = zInfo.Project;
            zConsent.Project = zInfo.Name_Project;
            zConsent.FurnitureKey = zInfo.CategoryInside;
            zConsent.Furniture = zInfo.Name_Inside;
            zConsent.Note = zInfo.Note;
            zConsent.Aera = zInfo.ConsentAera;
            zConsent.Price = zInfo.Price.ToDoubleString();
            zConsent.Room = zInfo.ConsentBedRoom;
            zConsent.CategoryKey = zInfo.CategoryAsset;
            zConsent.Category = zInfo.Name_Asset;
            return zConsent;
        }
        [WebMethod]
        public static string GetRecordList(int CustomerKey)
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemConsent> zList = Consents_Data.List(CustomerKey);
            int n = 1;
            foreach (ItemConsent r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.RecordKey + "'>");
                zSb.AppendLine("               <td>" + (n++) + "</td>");
                zSb.AppendLine("               <td>" + r.Note + "</td>");
                zSb.AppendLine("               <td>" + r.Project + "</td>");
                zSb.AppendLine("               <td>" + r.Category + "</td>");
                zSb.AppendLine("               <td class='giatien'>" + r.Price.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td>" + r.Room + "</td>");
                zSb.AppendLine("               <td>" + r.CreatedDate.ToDateString() + "</td>");
                zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDelWant' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }
    }
}