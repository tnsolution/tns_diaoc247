﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.CRM
{
    public partial class CustomerView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_CustomerKey.Value = Request["ID"];

                Tools.ListBox(LB_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName");
                Tools.ListBox(LB_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName");
                Tools.ListBox(LB_Furniture, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 7");
                Tools.DropDown_DDL(DDL_Status, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 12", false);
                Tools.DropDown_DDL(DDL_Source, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 11", false);
                Tools.DropDown_DDL(DDL_Want, "SELECT AutoKey, Product FROM SYS_Categories WHERE TYPE = 14", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE isWorking = 2 ORDER BY LastName", true);

                CheckRoles();
                LoadInfo();
                LoadTrade();
                LoadData();
            }
        }
        void LoadInfo()
        {
            Customer_Info zInfo = new Customer_Info(HID_CustomerKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();

            txt_Name.Text = zInfo.CustomerName;
            #region [thông tin cơ bản]
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Họ tên</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='hoten'>" + zInfo.CustomerName + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Số điện thoại</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='sdt'>" + zInfo.Phone1.HtmlPhone() + zInfo.Phone2.HtmlPhone() + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Email</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='email'>" + zInfo.Email1 + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Nguồn khách</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='nguon'>" + zInfo.CustomerSource + " : " + zInfo.SourceNote + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Mức độ</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='tiennang'>" + zInfo.StatusName + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='nhucau'>" + zInfo.WantShow + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Ghi chú</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='ghichu'>" + zInfo.Description + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                            </div>");
            #endregion
            Lit_InfoBasic.Text = zSb.ToString();
            zSb = new StringBuilder();
            #region [Quan tâm]
            string html = "";
            List<ItemConsent> zList2 = Consents_Data.List(HID_CustomerKey.Value.ToInt());
            if (zList2.Count > 0)
            {
                html += "<div class='col-xs-12 cuonchamsoc'>";

                for (var j = 0; j < zList2.Count; j++)
                {
                    html += "<div class='row'>";
                    html += "   <div class='col-xs-5 no-padding'>";
                    #region [danh sách quan tâm]
                    html += "       <div class='widget-box transparent'>";
                    html += "           <div class='widget-header'><h6 class='widget-title grey lighter'><i class='ace-icon fa fa-bell red'></i>Quan tâm</h6><div class='widget-toolbar no-border'>" + zList2[j].CreatedDate.ToDateString() + "</div></div>";
                    html += "           <div class='widget-body'>";
                    html += "              <div class='widget-main padding-0'>";
                    html += "                  <div class='profile-user-info profile-user-info-striped'>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Nhu cầu</div><div class='profile-info-value'>" + zList2[j].Want + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Dự án</div><div class='profile-info-value' id='loai'>" + zList2[j].Project + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Loại</div><div class='profile-info-value' id='loai'>" + zList2[j].Category + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Số phòng</div><div class='profile-info-value' id='sophong'>" + zList2[j].Room + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Nội thất</div><div class='profile-info-value' id='noithat'>" + zList2[j].Furniture + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Giá</div><div class='profile-info-value giatien' id='giatien'>" + zList2[j].Price.ToDoubleString() + "</div></div>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "          </div>";
                    html += "    </div>";
                    #endregion
                    html += "   </div>";
                    //---
                    html += "   <div class='col-xs-7 no-padding-right' id='divcare'>";
                    html += "       <div class='widget-box transparent'>";
                    html += "           <div class='widget-header'><h6 class='widget-title grey lighter'><i class='ace-icon fa fa-comments green'></i>Nội dung trao đổi</h6><div class='widget-toolbar no-border'><a href='#' tabindex='-1' class='center' isedit='' onclick='AddNewInfo(" + zList2[j].RecordKey + ")'><i class='ace-icon fa fa-plus orange bigger-130'></i>&nbsp;Thêm</a></div></div>";
                    html += "           <div class='widget-body'>";
                    DataTable zList = Care_Data.List(HID_CustomerKey.Value.ToInt(), zList2[j].RecordKey.ToInt());
                    foreach (DataRow r in zList.Rows)
                    {
                        html += "   <div class='widget-box transparent' style='border: 1px rgba(67, 142, 185, 0.5) solid !important;'>";
                        html += "       <div class='widget-header' style='min-height:auto; border-bottom:0px'><h6 class='widget-title grey lighter' style='line-height:0px'><i class='ace-icon fa fa-angle-right bigger-130 green'></i>" + r["CreatedBy"].ToString() + "</h6><div class='widget-toolbar no-border' style='line-height:17px'>" + r["CreatedDate"].ToDateString() + "</div></div>";
                        html += "       <div class='widget-body'><div class='widget-main no-padding-top no-padding-bottom'>" + r["ContentDetail"].ToString() + "</div></div>";
                        html += "   </div>";
                    }
                    html += "           </div>";
                    html += "           </div>";
                    html += "       </div>";
                    html += "</div>";
                    html += "<div class='hr hr-double no-margin'></div>";
                }

            }
            html += "</div>";
            #endregion            
            zSb.AppendLine(html);
            Lit_InfoConsent.Text = zSb.ToString();
            zSb = new StringBuilder();
            #region [Chi tiết liên lạc]
            zSb.AppendLine("  <div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Ngày sinh</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='ngaysinh'>" + zInfo.Birthday.ToDateString() + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>CMND/Passport</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='cmnd'>" + zInfo.CardID + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Ngày cấp</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='ngaycap'>" + zInfo.CardDate.ToDateString() + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Nơi cấp</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='noicap'>" + zInfo.CardPlace + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Địa chỉ thường trú</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='diachi1'>" + zInfo.Address1 + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='profile-info-row'>");
            zSb.AppendLine("                                    <div class='profile-info-name'>Địa chỉ Địa chỉ liên lạc</div>");
            zSb.AppendLine("                                    <div class='profile-info-value' id='diachi2'>" + zInfo.Address2 + "</div>");
            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                                <div class='space-6'></div>");
            if (zInfo.CategoryKey == 1)
            {
                zSb.AppendLine("                                <div class='profile-info-row'>");
                zSb.AppendLine("                                    <div class='profile-info-name'>Tên công ty</div>");
                zSb.AppendLine("                                    <div class='profile-info-value' id='tencongty'>" + zInfo.CompanyName + "</div>");
                zSb.AppendLine("                                </div>");
                zSb.AppendLine("                                <div class='profile-info-row'>");
                zSb.AppendLine("                                    <div class='profile-info-name'>Địa chỉ</div>");
                zSb.AppendLine("                                    <div class='profile-info-value' id='diachi3'>" + zInfo.Address3 + "</div>");
                zSb.AppendLine("                                </div>");
                zSb.AppendLine("                                <div class='profile-info-row'>");
                zSb.AppendLine("                                    <div class='profile-info-name'>MS thuế</div>");
                zSb.AppendLine("                                    <div class='profile-info-value' id='masothue'>" + zInfo.TaxCode + "</div>");
                zSb.AppendLine("                                </div>");
            }
            zSb.AppendLine("                            </div>");
            #endregion
            Lit_InfoDetail.Text = zSb.ToString();
            #region [Mana Info]
            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người khởi tạo</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoikhoitao' value>" + zInfo.CreatedName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày khởi tạo</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaykhoitao' value>" + zInfo.CreatedDate + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người cập nhật</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoicapnhat' value>" + zInfo.ModifiedName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày cập nhật</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaycapnhat' value>" + zInfo.ModifiedDate + "</div>");
            zSb.AppendLine("    </div>");
            //zSb.AppendLine("    <div class='profile-info-row'>");
            //zSb.AppendLine("        <div class='profile-info-name'>Người quản lý</div>");
            //zSb.AppendLine("        <div class='profile-info-value' id='nguoiquanly' value>" + zInfo.EmployeeName + "</div>");
            //zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            #endregion
            Lit_Manager.Text = zSb.ToString();
            #region [AllowInfo]
            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id=tblShare>");
            DataTable zTable = Share_Permition_Data.List_ShareDepartment(HID_CustomerKey.Value.ToInt(), "Customer");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'>");
                zSb.AppendLine("    <div class='profile-info-name'>Chia thông tin cho </div><div class='profile-info-value'>" + zTable.Rows[i]["EmployeeName"].ToString() + "</div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            #endregion           
            Lit_Allow.Text = zSb.ToString();
        }
        void LoadTrade()
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemTrade> zTable = Customer_Data.Get_Asset(HID_CustomerKey.Value.ToInt());
            #region [View Table]
            //if (zTable.Count > 0)
            //{
            //    zSb.AppendLine("<table class='table table-hover table-bordered' id='tblTrade'>");
            //    zSb.AppendLine("   <thead>");
            //    zSb.AppendLine("    <tr>");
            //    zSb.AppendLine("        <th>STT</th>");
            //    zSb.AppendLine("        <th>Ngày ký HĐ</th>");
            //    zSb.AppendLine("        <th>Dự án</th>");
            //    zSb.AppendLine("        <th>Căn hộ</th>");
            //    zSb.AppendLine("        <th>Diện tích (m2)</th>");
            //    zSb.AppendLine("        <th>Loại giao dịch</th>");
            //    zSb.AppendLine("        <th>Doanh thu</th>");
            //    zSb.AppendLine("        <th>Khách là</th>");
            //    zSb.AppendLine("    </tr>");
            //    zSb.AppendLine("   </thead>");
            //    zSb.AppendLine("        <tbody>");
            //    int i = 1;
            //    foreach (ItemTrade r in zTable)
            //    {
            //        zSb.AppendLine("            <tr id='" + r.TradeKey + "'>");

            //        zSb.AppendLine("               <td>" + (i++) + "</td>");
            //        zSb.AppendLine("               <td>" + r.ContractDate.ToDateString() + "</td>");
            //        zSb.AppendLine("               <td>" + r.ProjectName + "</td>");
            //        zSb.AppendLine("               <td>" + r.AssetID + "</td>");
            //        zSb.AppendLine("               <td>" + r.Area + "</td>");
            //        zSb.AppendLine("               <td>" + r.Category + "</td>");
            //        zSb.AppendLine("               <td>" + r.InCome.ToDoubleString() + "</td>");
            //        zSb.AppendLine("               <td>" + r.Owner + "</td>");
            //        zSb.AppendLine("            </tr>");
            //    }

            //    zSb.AppendLine("        </tbody>");
            //    zSb.AppendLine("</table>");

            //    txt_TotalTrade.Text = zTable.Count.ToString();
            //   
            //}
            #endregion
            #region [View Detail]     
            string html = "";
            foreach (ItemTrade r in zTable)
            {
                html += "<div class='col-xs-6'>";
                html += "<div class='widget-box transparent'>";
                html += "   <div class='widget-header'><h5 class='widget-title grey lighter'><i class='ace-icon fa fa-leaf green'></i>Giao dịch</h5><div class='widget-toolbar no-border'>Ngày ký HĐ: " + r.ContractDate.ToDateString() + "</div></div>";
                html += "   <div class='widget-body'>";
                html += "   <div class='widget-main padding-0'>";
                html += "   <div class='profile-user-info profile-user-info-striped' style='width:100%'>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Loại giao dịch</div><div class='profile-info-value'>" + r.Category + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Mã căn/ Sản phẩm</div><div class='profile-info-value'>" + r.AssetID + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Khách là </div><div class='profile-info-value'>" + r.Owner + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Dự án</div><div class='profile-info-value'>" + r.ProjectName + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Doanh thu</div><div class='profile-info-value giatien'>" + r.InCome.ToDoubleString() + "</div>";
                html += "       </div>";
                html += "   </div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
            }
            #endregion

            txt_TotalTrade.Text = zTable.Count.ToString();
            Lit_InfoTrade.Text = zSb.AppendLine(html).ToString();
        }
        void LoadData()
        {
            string txt_Name = "<input type='text' id='txt_Name' class='form-control input-sm' placeholder='Tên' />";
            string txt_Phone = "<input type='text' id='txt_Phone' class='form-control input-sm' placeholder='Số điện thoại' />";
            string txt_duan = Tools.Html_Select("DDL_Project", "Dự án", "form-control input-sm", false, "SELECT C.ProjectKey, C.ProjectName FROM PUL_Project C", false);
            string txt_loai = Tools.Html_Select("DDL_Category", "Loại SP", "form-control input-sm", false, "SELECT C.CategoryKey, C.CategoryName FROM PUL_Category C", false);
            string btn_Search = "<a href='#' class='btn-block btn btn-sm btn-purple' id='btnSearch'><i class='ace-icon fa fa-search'></i>Lọc</a>";

            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            if (UnitLevel < 2)
            {
                Employee = 0;
                Department = 0;
            }
            List<ItemCustomer> zList = new List<ItemCustomer>();
            if (Session["SearchCRM"] != null)
            {
                #region [GET OLD SEARCH]
                ItemCRMSearch ISearch = (ItemCRMSearch)Session["SearchCRM"];
                switch (ISearch.Category.ToInt())
                {
                    case 0:
                        zList = Customer_Data.Search(Department, Employee, ISearch.Name, ISearch.Phone, string.Empty, string.Empty);
                        break;

                    case 1:
                        zList = Customer_Data.Search(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ISearch.ListKey);
                        break;

                    case 2:
                        zList = Customer_Data.Search(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ISearch.ListKey);
                        break;

                    case 3:
                        zList = Customer_Data.Search(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ISearch.ListKey);
                        break;

                    default:
                        break;
                }
                #endregion
            }
            else
            {
                zList = Customer_Data.Search(Department, Employee, string.Empty, string.Empty, string.Empty, string.Empty, 50);
            }
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblExtraData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th colspan=2>" + txt_Name + "</th>");
            zSb.AppendLine("        <th>" + txt_Phone + "</th>");
            zSb.AppendLine("        <th>" + txt_duan + "</th>");
            zSb.AppendLine("        <th>" + txt_loai + "</th>");
            zSb.AppendLine("        <th>" + btn_Search + "</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            int n = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "' type='" + r.CategoryKey + "'>");
                zSb.AppendLine("               <td>" + (n++).ToString() + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br>" + r.Phone2 + "</td>");
                zSb.AppendLine("               <td class='td20'>" + r.ProjectName + "</td>");
                zSb.AppendLine("               <td class='td2'>" + r.CategoryName + "</td>");
                zSb.AppendLine("               <td class='td2'>" + r.Status + "</td>");
                zSb.AppendLine("            </tr>");
            }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_RightTable.Text = zSb.ToString();
        }
        #region [Xử lý box bên phải]
        [WebMethod]
        public static string Search(string Name, string Phone, string Project, string Category)
        {
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            if (UnitLevel < 2)
            {
                Employee = 0;
                Department = 0;
            }

            List<ItemCustomer> zList = new List<ItemCustomer>();
            zList = Customer_Data.Search(Department, Employee, Name, Phone, string.Empty, string.Empty, Project, Category);
            StringBuilder zSb = new StringBuilder();
            int n = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "' type='" + r.CategoryKey + "'>");
                zSb.AppendLine("               <td>" + (n++).ToString() + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br>" + r.Phone2 + "</td>");
                zSb.AppendLine("               <td class='td20'>" + r.ProjectName + "</td>");
                zSb.AppendLine("               <td class='td2'>" + r.CategoryName + "</td>");
                zSb.AppendLine("               <td class='td2'>" + r.Status + "</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }
        [WebMethod]
        public static ItemCustomer GetView(int CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            ItemCustomer zItem = new ItemCustomer();
            zItem.CustomerKey = zInfo.Key.ToString();
            zItem.CategoryKey = zInfo.CategoryKey.ToString();
            zItem.CustomerName = zInfo.CustomerName;
            zItem.Email1 = zInfo.Email1;
            zItem.Phone1 = zInfo.Phone1;
            zItem.Phone2 = zInfo.Phone2;
            zItem.Consent = zInfo.CustomerSource + " " + zInfo.SourceNote;
            zItem.WantShow = zInfo.WantShow;
            zItem.Status = zInfo.StatusName;
            zItem.Birthday = zInfo.Birthday.ToDateString();
            zItem.CardID = zInfo.CardID;
            zItem.CardDate = zInfo.CardDate.ToDateString();
            zItem.CardPlace = zInfo.CardPlace;
            zItem.Address1 = zInfo.Address1;
            zItem.Address2 = zInfo.Address2;
            zItem.Address3 = zInfo.Address3;
            zItem.CompanyName = zInfo.CompanyName;
            zItem.Tax = zInfo.TaxCode;

            zItem.CreatedName = zInfo.CreatedName;
            zItem.ModifiedName = zInfo.ModifiedName;
            zItem.CreatedDate = zInfo.CreatedDate.ToDateString();
            zItem.ModifiedDate = zInfo.ModifiedDate.ToDateString();

            return zItem;
        }
        [WebMethod]
        public static string GetConsent(int Key)
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemConsent> zList2 = Consents_Data.List(Key);
            string html = "";          
            if (zList2.Count > 0)
            {
                html += "<div class='col-xs-12 cuonchamsoc'>";

                for (var j = 0; j < zList2.Count; j++)
                {
                    html += "<div class='row'>";
                    html += "   <div class='col-xs-5 no-padding'>";
                    #region [danh sách quan tâm]
                    html += "       <div class='widget-box transparent'>";
                    html += "           <div class='widget-header'><h6 class='widget-title grey lighter'><i class='ace-icon fa fa-bell red'></i>Quan tâm</h6><div class='widget-toolbar no-border'>" + zList2[j].CreatedDate.ToDateString() + "</div></div>";
                    html += "           <div class='widget-body'>";
                    html += "              <div class='widget-main padding-0'>";
                    html += "                  <div class='profile-user-info profile-user-info-striped'>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Nhu cầu</div><div class='profile-info-value'>" + zList2[j].Want + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Dự án</div><div class='profile-info-value' id='loai'>" + zList2[j].Project + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Loại</div><div class='profile-info-value' id='loai'>" + zList2[j].Category + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Số phòng</div><div class='profile-info-value' id='sophong'>" + zList2[j].Room + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Nội thất</div><div class='profile-info-value' id='noithat'>" + zList2[j].Furniture + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Giá</div><div class='profile-info-value giatien' id='giatien'>" + zList2[j].Price.ToDoubleString() + "</div></div>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "          </div>";
                    html += "    </div>";
                    #endregion
                    html += "   </div>";
                    //---
                    html += "   <div class='col-xs-7 no-padding-right' id='divcare'>";
                    html += "       <div class='widget-box transparent'>";
                    html += "           <div class='widget-header'><h6 class='widget-title grey lighter'><i class='ace-icon fa fa-comments green'></i>Nội dung trao đổi</h6><div class='widget-toolbar no-border'><a href='#' tabindex='-1' class='center' isedit='' onclick='AddNewInfo(" + zList2[j].RecordKey + ")'><i class='ace-icon fa fa-plus orange bigger-130'></i>&nbsp;Thêm</a></div></div>";
                    html += "           <div class='widget-body'>";
                    DataTable zList = Care_Data.List(Key, zList2[j].RecordKey.ToInt());
                    foreach (DataRow r in zList.Rows)
                    {
                        html += "   <div class='widget-box transparent' style='border: 1px rgba(67, 142, 185, 0.5) solid !important;'>";
                        html += "       <div class='widget-header' style='min-height:auto; border-bottom:0px'><h6 class='widget-title grey lighter' style='line-height:0px'><i class='ace-icon fa fa-angle-right bigger-130 green'></i>" + r["CreatedBy"].ToString() + "</h6><div class='widget-toolbar no-border' style='line-height:17px'>" + r["CreatedDate"].ToDateString() + "</div></div>";
                        html += "       <div class='widget-body'><div class='widget-main no-padding-top no-padding-bottom'>" + r["ContentDetail"].ToString() + "</div></div>";
                        html += "   </div>";
                    }
                    html += "           </div>";
                    html += "           </div>";
                    html += "       </div>";
                    html += "</div>";
                    html += "<div class='hr hr-double no-margin'></div>";
                }

            }
            html += "</div>";
            zSb.AppendLine(html);
            return zSb.ToString();
        }
        [WebMethod]
        public static ItemResult GetTrade(int Key)
        {
            ItemResult zResult = new ItemResult();

            StringBuilder zSb = new StringBuilder();
            List<ItemTrade> zTable = Customer_Data.Get_Asset(Key);

            #region [View Detail]     
            string html = "";
            foreach (ItemTrade r in zTable)
            {
                html += "<div class='col-xs-6'>";
                html += "<div class='widget-box transparent'>";
                html += "   <div class='widget-header'><h5 class='widget-title grey lighter'><i class='ace-icon fa fa-leaf green'></i>Giao dịch</h5><div class='widget-toolbar no-border'>Ngày ký HĐ: " + r.ContractDate.ToDateString() + "</div></div>";
                html += "   <div class='widget-body'>";
                html += "   <div class='widget-main padding-0'>";
                html += "   <div class='profile-user-info profile-user-info-striped' style='width:100%'>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Loại giao dịch</div><div class='profile-info-value'>" + r.Category + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Mã căn/ Sản phẩm</div><div class='profile-info-value'>" + r.AssetID + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Khách là </div><div class='profile-info-value'>" + r.Owner + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Dự án</div><div class='profile-info-value'>" + r.ProjectName + "</div>";
                html += "       </div>";
                html += "       <div class='profile-info-row'>";
                html += "           <div class='profile-info-name'>Doanh thu</div><div class='profile-info-value giatien'>" + r.InCome.ToDoubleString() + "</div>";
                html += "       </div>";
                html += "   </div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
            }
            #endregion

            zResult.Result = html;
            zResult.Result2 = zTable.Count.ToString();
            return zResult;
        }
        #endregion
        #region [Xử lý share]
        [WebMethod]
        public static ItemResult SaveShare(int CustomerKey, string TableName, int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            Share_Permition_Info zInfo = new Share_Permition_Info(CustomerKey, EmployeeKey, TableName);
            if (zInfo.AutoKey > 0)
            {
                zResult.Message = "Trùng thông tin, vui lòng chọn lại";
                return zResult;
            }

            zInfo.ObjectTable = TableName;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.AssetKey = CustomerKey;
            zInfo.Create();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemWeb[] GetShare(int CustomerKey, string TableName)
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(CustomerKey, TableName);
            List<ItemWeb> ListAsset = new List<ItemWeb>();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb iwe = new ItemWeb();
                iwe.Value = zTable.Rows[i]["AutoKey"].ToString();
                iwe.Text = zTable.Rows[i]["EmployeeName"].ToString();

                ListAsset.Add(iwe);
            }
            return ListAsset.ToArray();
        }
        #endregion
        #region [Xử lý thông tin, liên lạc]
        [WebMethod]
        public static ItemCustomer GetInfoCustomer(int CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            ItemCustomer zItem = new ItemCustomer();
            zItem.CustomerName = zInfo.CustomerName;
            zItem.Phone1 = zInfo.Phone1;
            zItem.Phone2 = zInfo.Phone2;
            zItem.StatusKey = zInfo.Status.ToString();
            zItem.SourceKey = zInfo.CategorySource.ToString();
            zItem.SourceName = zInfo.CustomerSource;
            zItem.SourceNote = zInfo.SourceNote;
            zItem.WantSearch = zInfo.WantSearch;
            zItem.Description = zInfo.Description;
            return zItem;
        }
        [WebMethod]
        public static ItemCustomer GetInfoAddress(int CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            ItemCustomer zItem = new ItemCustomer();
            zItem.Birthday = zInfo.Birthday.ToDateString();
            zItem.CompanyName = zInfo.CompanyName;
            zItem.Tax = zInfo.TaxCode;
            zItem.CardID = zInfo.CardID;
            zItem.CardPlace = zInfo.CardPlace;
            zItem.CardDate = zInfo.CardDate.ToDateString();
            zItem.Address1 = zInfo.Address1;
            zItem.Address2 = zInfo.Address2;
            zItem.Address3 = zInfo.Address3;
            return zItem;
        }
        [WebMethod]
        public static ItemResult SaveCustomer(int CustomerKey, string CustomerName, string Source, string SourceNote, string Phone1, string Phone2, string Status, string Description)
        {
            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.CustomerName = CustomerName;
            zInfo.CategorySource = Source.ToInt();
            zInfo.SourceNote = SourceNote;
            zInfo.Phone1 = Phone1;
            zInfo.Phone2 = Phone2;
            zInfo.Status = Status.ToInt();
            zInfo.Description = Description;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Update();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveAddress(int CustomerKey, string Birthday, string CardID, string CardDate, string CardPlace, string Address1, string Address2, string Address3, string CompanyName, string Tax)
        {
            ItemResult zResult = new ItemResult();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.CompanyName = CompanyName;
            zInfo.TaxCode = Tax;
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.Address3 = Address3;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.Update();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        #endregion
        [WebMethod]
        public static ItemResult SaveCare(int CustomerKey, int ConsentKey, string Content)
        {
            Care_Info zInfo = new Care_Info();
            zInfo.ConsentKey = ConsentKey;
            zInfo.CustomerKey = CustomerKey;
            zInfo.ContentDetail = Content.Trim();
            zInfo.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.ModifiedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.Create();

            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveWant(int CustomerKey, string Note, string ProjectKey, string Project, string ProjectOther, string CategoryKey, string Category, string FurnitureKey, string Furniture, string Room, string Price, string Area, string WantKey)
        {
            ItemResult zResult = new ItemResult();
            Consents_Info zConsent = new Consents_Info();           
            zConsent.CategoryNeed = WantKey;
            zConsent.ConsentBedRoom = Room;
            zConsent.ConsentAera = Area;
            zConsent.CategoryInside = FurnitureKey;
            zConsent.CategoryAsset = CategoryKey;
            zConsent.CustomerKey = CustomerKey;
            zConsent.Note = Note;
            zConsent.ProjectOther = ProjectOther;
            zConsent.Price = Convert.ToDouble(Price);
            zConsent.Project = ProjectKey;
            zConsent.Name_Project = Project;
            zConsent.Name_Inside = Furniture;
            zConsent.Name_Asset = Category;
            zConsent.Create();
            zResult.Message = zConsent.Message;
            return zResult;
        }

        #region [Xử lý nhắc nhở]
        [WebMethod]
        public static ItemResult CheckReminder(int CustomerKey)
        {
            ItemResult zResult = new ItemResult();
            int Exists = Customer_Data.CheckReminder(CustomerKey);
            if (Exists > 0)
            {
                zResult.Message = "Khách này đã đến hẹn, chưa được xử lý, vui lòng kiểm tra ngày liên quan !";
                zResult.Result = Exists.ToString();
            }
            return zResult;
        }
        [WebMethod]
        public static ItemResult ExeReminder(int RemindKey)
        {
            ItemResult zResult = new ItemResult();
            Notification_Info zInfo = new Notification_Info();
            zInfo.ID = RemindKey;
            zInfo.IsRead = 1;
            zInfo.ReadDate = DateTime.Now;
            zInfo.UpdateReaded();
            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
                zResult.Result = "0";
            }
            else
            {
                zResult.Message = "Đã xử lý thành công !";
                zResult.Result = "1";
            }
            return zResult;
        }
        #endregion

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1    
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL03";
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Chỉnh sửa
                        </button>";
            }
        }
        #endregion                
    }
}