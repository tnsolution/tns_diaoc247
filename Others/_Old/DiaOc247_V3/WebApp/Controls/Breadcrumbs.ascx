﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Breadcrumbs.ascx.cs" Inherits="WebApp.Controls.Breadcrumbs" %>

<ul class="breadcrumb">
    <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="Default.aspx">Home</a>
    </li>
    <asp:Literal ID="Literal_Breadcrumb" runat="server"></asp:Literal>
</ul>

