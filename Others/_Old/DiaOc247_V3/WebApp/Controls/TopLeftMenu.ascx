﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopLeftMenu.ascx.cs" Inherits="WebApp.Controls.TopLeftMenu" %>
<ul class="nav navbar-nav">
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
            <i class="ace-icon fa fa-bell icon-animated-bell"></i>
            Nhắc nhở
            <span class="badge badge-warning">
                <asp:Literal ID="Lit_TotalRemind" runat="server"></asp:Literal></span>
        </a>
        <ul class="dropdown-menu dropdown-light-blue dropdown-caret">
            <li>
                <a href="../Remind.aspx?Type=1">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Sinh nhật khách
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_Birthday" runat="server"></asp:Literal></span>
                </a>
            </li>
            <li>
                <a href="../Remind.aspx?Type=2">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Giao dịch hết hạn
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_TradeExpired" runat="server"></asp:Literal></span>
                </a>
            </li>
            <li>
                <a href="../Remind.aspx?Type=3">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Sản phẩm tới hẹn
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_AssetExpired" runat="server"></asp:Literal></span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
            <i class="ace-icon fa fa-info-circle"></i>
            Công việc
            <span class="badge badge-warning icon-animated-vertical">
                <asp:Literal ID="Lit_TotalTask" runat="server"></asp:Literal></span>
        </a>
        <ul class="dropdown-menu dropdown-light-blue dropdown-caret">
            <li>
                <a href="/INFO/InfoList.aspx">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Công việc mới giao
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_Task" runat="server"></asp:Literal></span>
                </a>
            </li>
            <li>
                <a href="../INFO/Transfer.aspx">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Sản phẩm mới giao
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_Transfer" runat="server"></asp:Literal></span>
                </a>
            </li>
           <%-- <li>
                <a href="#mReportBox" data-toggle="modal">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Lập báo cáo                                    
                </a>
            </li>
            <li>
                <a href="#mScheduleBox" data-toggle="modal">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Lập kế hoạch
                </a>
            </li>--%>
        </ul>
    </li>
    <li>
        <a href="/HRM/InfomationList.aspx"><i class="ace-icon fa fa-bullhorn"></i>Thông tin nội bộ</a>
    </li>
    <asp:Literal ID="LitMenuFNC" runat="server"></asp:Literal>
    <li>
        <a href="../FNC/FeePlay.aspx"><i class="ace-icon fa fa-usd bigger-110"></i>&nbsp;Quỹ xập xình
        </a>
    </li>
</ul>
