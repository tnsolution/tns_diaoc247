﻿using Lib.SYS;
using System;
using System.Web;

namespace WebApp.Controls
{
    public partial class TopLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                int NumCapital = 0;
                int NumBirthday = 0;
                int NumTrade = 0;
                int NumAsset = 0;
                int NumTask = 0;
                int NumReceipt = 0;
                int NumPayment = 0;
                int NumTransfer = 0;

                switch (UnitLevel)
                {
                    case 0:
                    case 1:                        
                        NumTransfer = Notification_Data.Count_Transfer(0, 0);
                        NumBirthday = Notification_Data.Count_Notification(1, 0, 0);
                        NumTrade = Notification_Data.Count_Notification(2, 0, 0);
                        NumAsset = Notification_Data.Count_Notification(3, 0, 0);
                        NumTask = Notification_Data.Count_Task(0, 0);
                        NumReceipt = Notification_Data.Count_Receipt(DateTime.Now.Year, DateTime.Now.Month, 0, 1);
                        NumPayment = Notification_Data.Count_Payment(DateTime.Now.Year, DateTime.Now.Month, 0, 1);
                        NumCapital = Notification_Data.Count_Capital(DateTime.Now.Year, DateTime.Now.Month);
                        break;

                    case 2:
                        NumTransfer = Notification_Data.Count_Transfer(0, DepartmentKey);
                        NumBirthday = Notification_Data.Count_Notification(1, 0, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, 0, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, 0, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(0, DepartmentKey);
                        NumReceipt = Notification_Data.Count_Receipt(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        break;

                    case 3:
                        NumBirthday = Notification_Data.Count_Notification(1, EmployeeKey, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, EmployeeKey, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, EmployeeKey, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(EmployeeKey, DepartmentKey);
                        NumReceipt = Notification_Data.Count_Receipt(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        break;

                    default:
                        NumBirthday = Notification_Data.Count_Notification(1, EmployeeKey, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, EmployeeKey, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, EmployeeKey, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(EmployeeKey, DepartmentKey);
                        NumReceipt = Notification_Data.Count_Receipt(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        NumTransfer = Notification_Data.Count_Transfer(EmployeeKey, DepartmentKey);
                        break;
                }

                MenuFNC(NumPayment.ToString(), NumReceipt.ToString(), (NumPayment).ToString(), NumCapital.ToString());

                Lit_Transfer.Text = NumTransfer.ToString();
                Lit_Birthday.Text = NumBirthday.ToString();
                Lit_TradeExpired.Text = NumTrade.ToString();
                Lit_AssetExpired.Text = NumAsset.ToString();
                Lit_Task.Text = NumTask.ToString();
                Lit_TotalRemind.Text = (NumBirthday + NumTrade + NumAsset).ToString();
                Lit_TotalTask.Text = NumTask.ToString();
            }
        }

        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void MenuFNC(string PayCount, string ReceiptCount, string TotalCount, string CapitalCount)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (result[0].ToInt() == 0)
            {
                return;
            }

            if (result[3].ToInt() == 1)
            {
                LitMenuFNC.Text = @"
    <li id='menuFNC'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-usd bigger-110'></i>&nbsp;Thu - Chi
            <span class='badge badge-warning icon-animated-vertical'>" + TotalCount + @"</span>&nbsp;<i class='ace-icon fa fa-angle-down bigger-110'></i>
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>
            <li><a href='../FNC/Capital.aspx'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Quỹ công ty <span class='badge badge-warning'>" + CapitalCount + @"</span></a></li>         
            <li><a href='../FNC/Receipt.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Thu <span class='badge badge-warning'>" + ReceiptCount + @"</span></a></li>
            <li><a href='../FNC/Payment.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Chi <span class='badge badge-warning'>" + TotalCount + @"</span></a></li>
            <li id='subFNC'><a href='../FNC/FeeCheck.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Bảng cân đối</a></li>
        </ul>
    </li>";
            }
            else
            {
                LitMenuFNC.Text = @"
    <li id='menuFNC'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-usd bigger-110'></i>&nbsp;Thu - Chi
            <span class='badge badge-warning icon-animated-vertical'>" + TotalCount + @"</span>&nbsp;<i class='ace-icon fa fa-angle-down bigger-110'></i>
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>          
            <li><a href='../FNC/Receipt.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Thu <span class='badge badge-warning'>" + ReceiptCount + @"</span></a></li>
            <li><a href='../FNC/Payment.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Chi <span class='badge badge-warning'>" + TotalCount + @"</span></a></li>
            <li id='subFNC'><a href='../FNC/FeeCheck.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Bảng cân đối</a></li>
        </ul>
    </li>";
            }
        }
    }
}