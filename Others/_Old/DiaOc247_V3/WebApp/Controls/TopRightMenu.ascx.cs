﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.Controls
{
    public partial class TopRightMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Literal_UserInfo.Text = @"
        <a data-toggle='dropdown' href='#' class='dropdown-toggle'>
            <img class='nav-user-photo' src='" + HttpContext.Current.Request.Cookies["UserLog"]["ImgThumb"] + @"' alt='Jason's Photo' />
            <span class='user-info'>
                <small>Welcome,</small>
           " + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + @"
            </span>
            <i class='ace-icon fa fa-caret-down'></i>
        </a>
        <ul class='user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close'> 
            <li>
                <a href='../HRM/EmployeeView.aspx?ID=" + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + @"'>
                    <i class='ace-icon fa fa-user'></i>
                    Profile
                </a>
            </li>
            <li class='divider'></li>
            <li>
                <a href='../Login.aspx'>
                    <i class='ace-icon fa fa-power-off'></i>
                    Logout
                </a>
            </li>
        </ul>";
        }
    }
}