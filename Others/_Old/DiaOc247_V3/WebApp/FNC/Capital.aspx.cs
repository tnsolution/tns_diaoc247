﻿using Lib.FNC;
using Lib.SYS;
using Lib.SYS.Report;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class Capital : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
            Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
            Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
            Tools.DropDown_DDL(DDL_Employee3, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);

            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            CheckRoles();
            LoadData(FromDate, ToDate);
        }
        void LoadData(DateTime FromDate, DateTime ToDate)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            double tongthu = 0, tongchi = 0, conlai = RptHelper.TotalBalanceCapital(FromDate.AddMonths(-1), ToDate.AddMonths(-1));
            DataTable zTable = new DataTable();
            StringBuilder zSb = new StringBuilder();

            #region [Thu Quỹ]
            zTable = Capital_Input_Data.List(FromDate, ToDate);
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblInCapital'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th class='td20'>Ngày nhập</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th class='td20'>Số tiền nhập</th>");
            zSb.AppendLine("        <th isdelete class='td10'>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    string BackColor = "";
                    string Jquery = "";
                    string Status = "";
                    string DeleteButton = "";
                    int Duyet = r["IsApproved"].ToInt();                  

                    if (UnitLevel <= 1)
                        Jquery = "style='cursor:pointer;float:right' onclick='ApproveCheckIn(" + r["AutoKey"].ToString() + ")'";
                    else
                        Jquery = "style='float:right'";

                    if (Duyet == 1)
                    {
                        DeleteButton = "";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        DeleteButton = "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["AutoKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }
                    if (Duyet == 0)
                        BackColor = "style=background-color:#c5d0dc";

                    zSb.AppendLine("            <tr " + BackColor + " id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["CapitalDate"].ToDateString() + "<div class='action-buttons pull-right'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>");
                    zSb.AppendLine("               <td>" + r["Note"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td isdelete>" + DeleteButton + "</td>");
                    zSb.AppendLine("            </tr>");

                    zSb.AppendLine("            <tr class='detail-row'>");
                    zSb.AppendLine("                <td colspan='5' class='noclick'>");
                    zSb.AppendLine("                    <div class='table-detail'>");
                    zSb.AppendLine("                        <table class='table table-bordered'>");
                    zSb.AppendLine("                            <tr><td class='td20 noclick'>Người gửi: " + r["FromName"].ToString() + "</td></tr>");
                    zSb.AppendLine("                            <tr><td class='td20 noclick'>Người nhận: " + r["ToName"].ToString() + "</td></tr>");
                    zSb.AppendLine("                            <tr><td class='td20 noclick'>Xác nhận: " + r["ApprovedName"].ToString() + Status + "</td></tr>");
                    zSb.AppendLine("                        </table>");
                    zSb.AppendLine("                    </div>");
                    zSb.AppendLine("                </td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='2' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien'>" + Total.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td></td>");
                zSb.AppendLine("            </tr>");

                tongthu = Total;
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='3'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable1.Text = zSb.ToString();

            #region [Chi Quỹ]
            zTable = Capital_Output_Data.List(FromDate, ToDate);
            zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblOutCapital'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th class='td20'>Ngày chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th class='td20'>Số tiền chi</th>");
            zSb.AppendLine("        <th isdelete class='td10'>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    string DeleteButton = "";
                    string Jquery = "";
                    string Status = "";
                    string BackColor = "";
                    int Duyet = r["IsApproved"].ToInt();
                    if (UnitLevel <= 1)
                        Jquery = "style='cursor:pointer;float:right' onclick='ApproveCheckOut(" + r["AutoKey"].ToString() + ")'";
                    else
                        Jquery = "style='float:right'";
                    if (Duyet == 1)
                    {
                        DeleteButton = "";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        DeleteButton = "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteOutput(" + r["AutoKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }

                    if (Duyet == 0)
                        BackColor = "style=background-color:#c5d0dc";

                    zSb.AppendLine("            <tr " + BackColor + " id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["CapitalDate"].ToDateString() + "<div class='action-buttons pull-right'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td isdelete>" + DeleteButton + "</td>");
                    zSb.AppendLine("            </tr>");

                    zSb.AppendLine("            <tr class='detail-row'>");
                    zSb.AppendLine("                <td colspan='5' class='noclick'>");
                    zSb.AppendLine("                    <div class='table-detail'>");
                    zSb.AppendLine("                        <table class='table table-bordered'>");
                    zSb.AppendLine("                            <tr><td class='td20 noclick'>Người gửi: " + r["FromName"].ToString() + "</td></tr>");
                    zSb.AppendLine("                            <tr><td class='td20 noclick'>Người nhận: " + r["ToName"].ToString() + "</td></tr>");
                    zSb.AppendLine("                            <tr><td class='td20 noclick'>Xác nhận: " + r["ApprovedName"].ToString() + Status + "</td></tr>");
                    zSb.AppendLine("                        </table>");
                    zSb.AppendLine("                    </div>");
                    zSb.AppendLine("                </td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='2' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien'>" + Total.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td></td>");
                zSb.AppendLine("            </tr>");

                tongchi = Total;
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='3'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable2.Text = zSb.ToString();

            LitPageHead.Text = "Quỹ CTY tháng " + FromDate.Month + " | " + (tongthu - tongchi + conlai).ToString("n0");
        }
        [WebMethod]
        public static ItemReceipt GetInCapital(int AutoKey)
        {
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.CapitalDate.ToDateString();
            zItem.Contents = zInfo.Note;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.FromEmployee.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static ItemResult GetInCapitalStatus(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
            {
                zResult.Message = "Thông tin này đã duyệt bạn không được thực hiện lại !.";
                return zResult;
            }
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string SaveInCapital(int AutoKey, string Employee1, string Employee2, string Date, string Contents, string Description, string Amount)
        {
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            zInfo.CapitalDate = Tools.ConvertToDate(Date);
            zInfo.FromEmployee = Employee1.ToInt();
            zInfo.ToEmployee = Employee2.ToInt();
            zInfo.Note = Contents.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.Description = Description.Trim();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }
        [WebMethod]
        public static ItemReceipt GetOutCapital(int AutoKey)
        {
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.CapitalDate.ToString("dd/MM/yyyy");
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.ToEmployee.ToString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static ItemResult GetOutCapitalStatus(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
            {
                zResult.Message = "Thông tin này đã duyệt bạn không được thực hiện lại !.";
                return zResult;
            }
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();

            #region [auto create phieu thu]
            Receipt_Detail_Info zReceipt = new Receipt_Detail_Info(zInfo.AutoKey, true);
            zReceipt.ReceiptDate = zInfo.CapitalDate;
            zReceipt.ReceiptFrom = zInfo.ToEmployee;
            zReceipt.ReceiptBy = zInfo.FromEmployee;
            zReceipt.Contents = zInfo.Contents.Trim();
            zReceipt.Description = zInfo.Description.Trim();
            zReceipt.Amount = zInfo.Amount;
            zReceipt.CategoryKey = 1;
            zReceipt.DepartmentKey = zInfo.DepartmentKey;
            zReceipt.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zReceipt.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zReceipt.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zReceipt.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zReceipt.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            if (zReceipt.AutoKey != 0)
                zReceipt.Update();
            else
            {
                zReceipt.FromCapital = zInfo.AutoKey;
                zReceipt.Create();
            }
            #endregion

            if (zInfo.Message != string.Empty)
                zResult.Message = "Lỗi duyệt chi quỹ: " + zInfo.Message;
            if (zReceipt.Message != string.Empty)
                zReceipt.Message = "Lỗi tạo phiếu thu" + zReceipt.Message;
            return zResult;
        }
        [WebMethod]
        public static string SaveOutCapital(int AutoKey, int Department, int Employee1, string Date, string Contents, string Description, string Amount)
        {
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            zInfo.CapitalDate = Tools.ConvertToDate(Date);
            zInfo.FromEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = Department;
            zInfo.ToEmployee = Employee1;
            zInfo.Contents = Contents.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.Description = Description.Trim();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string sFromDate = HID_Date.Value.Split('/')[0];
            string sToDate = HID_Date.Value.Split('/')[1];

            DateTime FromDate = Tools.ConvertToDate(sFromDate);
            DateTime ToDate = Tools.ConvertToDate(sToDate);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            LoadData(FromDate, ToDate);
        }
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');

            if (UnitLevel <= 1)
            {
                LitButton.Text = @"
                <span class='tools pull-right'><div class='btn-group'><button data-toggle='dropdown' class='btn btn-info btn-white dropdown-toggle' aria-expanded='false'><span class='ace-icon fa fa-caret-down icon-only'></span>&nbsp;Thêm mới</button>
                    <ul class='dropdown-menu dropdown-info dropdown-menu-right'>
						<li><a href = '#' class='' id='thuquy'><i class='ace-icon fa fa-plus blue'></i>&nbsp;Thu</a></li>
            			<li><a href = '#' class='' id='chiquy'><i class='ace-icon fa fa-plus blue'></i>&nbsp;Chi</a></li></ul></div></span>";
            }

            if (result[3].ToInt() == 0)
            {
                LitScript.Text = "<script>$(document).ready(function () { $('[isdelete]').hide(); });</script>";
            }
        }
    }
}