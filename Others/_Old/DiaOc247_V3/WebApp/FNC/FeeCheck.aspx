﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="FeeCheck.aspx.cs" Inherits="WebApp.FNC.FeeCheck" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .giatien {
            font-weight: normal !important;
        }

        .table {
            margin-bottom: 0px !important;
        }

        .td15 {
            width: 15%;
        }

        .tdr {
            width: 10%;
            text-align: right !important;
        }

        .td1 {
            width: 1%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_Month" runat="server"></asp:Literal>
                <span class="pull-right action-buttons">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="faq-list-1" class="accordion-style1 panel-group">
                    <asp:Literal ID="LitTable_Capital" runat="server"></asp:Literal>
                    <div class="space-6"></div>
                    <div class="hr hr2 hr-double"></div>
                    <div class="space-6"></div>
                    <asp:Literal ID="LitTable_Data" runat="server"></asp:Literal>
                    <div class="space-6"></div>
                    <div class="hr hr2 hr-double"></div>
                    <div class="space-6"></div>
                    <asp:Literal ID="LitTotal" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Category" runat="server" Value="0" />
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $('.panel').on('shown.bs.collapse', function (e) {
                var id = e.currentTarget.id;
                if (!$.trim($('div[department=' + id + ']').html()).length) {
                    $('.se-pre-con').fadeIn('slow');
                    $.ajax({
                        type: "POST",
                        url: "/FNC/FeeCheck.aspx/GetMoreDetail",
                        data: JSON.stringify({
                            "Key": id,
                            "FromDate": $("[id$=HID_FromDate]").val(),
                            "ToDate": $("[id$=HID_ToDate]").val(),
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                        },
                        success: function (r) {
                            $('div[department=' + id + ']').empty();
                            $('div[department=' + id + ']').append($(r.d));
                        },
                        complete: function () {
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
                else {
                    return false;
                }
            });
            $(".panel #btncloseMonth").click(function (e) {
                var depart = $(this).attr("depart");
                var row = $(this).closest("tr");
                var amount = row.find('td[amount]').attr("amount");             
                if (confirm("Bạn đồng ý kết chuyển số tiền còn lại sang tháng sau ?")) {
                    $.ajax({
                        type: "POST",
                        url: "/FNC/FeeCheck.aspx/SaveCloseMonth",
                        data: JSON.stringify({
                            'Date': $("[id$=HID_ToDate]").val(),
                            'Amount': amount,
                            'Department': depart,
                            'Category': 1
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (r) {
                            if (r.d.Result == 'ERROR') {
                                $('.se-pre-con').fadeOut('slow');
                                Page.showNotiMessageError("Lỗi xin liên hệ Admin", r.d.Message);
                            }
                            else {
                                $("#btncloseMonth").hide();
                                Page.showNotiMessageInfo("..", r.d.Message);
                                window.location = '/FNC/FeeCheck.aspx?Category=' + $("[id$=HID_Category]").val();
                            }
                        },
                        complete: function () { },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
