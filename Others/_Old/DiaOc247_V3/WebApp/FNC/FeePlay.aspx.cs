﻿using Lib.FNC;
using Lib.SYS;
using Lib.SYS.Report;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

//2 là loại phí public quỹ ăn chơi.
namespace WebApp.FNC
{
    public partial class FeePlay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department2, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);

                CheckRoles();
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSb = new StringBuilder();
            #region thu
            double TotalThu = 0;
            DataTable zTable = Receipt_Detail_Data.List(2);
            zSb.AppendLine("<table class='table table-hover table-striped' id='tblReceipt'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Người đóng quỹ</th>");
            zSb.AppendLine("        <th>Ngày thu</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền thu</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {

                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");

                    TotalThu += r["Amount"].ToDouble();
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='3' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + TotalThu.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='4'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable1.Text = zSb.ToString();
            zSb = new StringBuilder();
            #region chi
            double TotalChi = 0;
            zTable = Payment_Detail_Data.List(2);
            zSb.AppendLine("<table class='table table-hover table-striped' id='tblPayment'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Người chi quỹ</th>");
            zSb.AppendLine("        <th>Ngày chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền chi</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {

                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");

                    TotalChi += r["Amount"].ToDouble();
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='3' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + TotalChi.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='4'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable2.Text = zSb.ToString();

            LitPageHead.Text = "Quỹ tháng " + DateTime.Now.Month + " | " + (TotalThu - TotalChi + RptHelper.TotalBalance(DateTime.Now.Month, DateTime.Now.Year)).ToString("n0");
        }

        [WebMethod]
        public static string SaveReceipt(int AutoKey, string Department, string Employee, string Category, string Date, string Contents, string Description, string Amount)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            zInfo.ReceiptDate = Tools.ConvertToDateTime(Date);
            zInfo.ReceiptFrom = Employee.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.CategoryKey = 2;
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (zInfo.Message == string.Empty)
            {
                return "OK";
            }
            else
            {
                return "Lỗi không lưu được";
            }
        }
        [WebMethod]
        public static string SavePayment(int AutoKey, string Department, string Employee, string Category, string Date, string Contents, string Description, string Amount)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            zInfo.PaymentDate = Tools.ConvertToDateTime(Date);
            zInfo.PaymentTo = Employee.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.CategoryKey = 2;
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (zInfo.Message == string.Empty)
            {
                return "OK";
            }
            else
            {
                return "Lỗi không lưu được";
            }
        }
        [WebMethod]
        public static ItemResult SaveCloseMonth(string Date, string Amount, int Category)
        {
            DateTime zDate = Convert.ToDateTime(Date);
            ItemResult zResult = new ItemResult();
            CloseMonth_Info zInfo = new CloseMonth_Info(zDate.Month, zDate.Year, HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt(), Category);
            if (zInfo.CloseFinish > 0)
            {
                zResult.Result = "OK";
                zResult.Message = "Tháng này đã kết chuyển rồi vui lòng chọn lại !.";
            }
            else
            {
                zInfo.Amount = double.Parse(Amount);
                zInfo.CloseDate = zDate;
                zInfo.CloseFinish = 1;
                zInfo.CategoryKey = 2;
                zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
                zInfo.CloseBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.Create();
                if (zInfo.Message != string.Empty)
                {
                    zResult.Result = "ERROR";
                    zResult.Message = zInfo.Message;
                }
                else
                {
                    zResult.Result = "OK";
                    zResult.Message = "Đã chuyển thành công !.";
                }
            }

            return zResult;
        }
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void CheckRoles()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');

            if (result[3].ToInt() == 1)
            {
                LitButton.Text = @"
                <span class='tools pull-right'><div class='btn-group'><button data-toggle='dropdown' class='btn btn-info btn-white dropdown-toggle' aria-expanded='false'><span class='ace-icon fa fa-caret-down icon-only'></span>&nbsp;Thêm mới</button>
                    <ul class='dropdown-menu dropdown-info dropdown-menu-right'>
						<li><a href = '#' class='' id='thuquy'><i class='ace-icon fa fa-plus blue'></i>&nbsp;Thu</a></li>
            			<li><a href = '#' class='' id='chiquy'><i class='ace-icon fa fa-plus blue'></i>&nbsp;Chi</a></li>
                        <li><a href = '#' class='' id='chuyen'><i class='ace-icon fa fa-share orange'></i>&nbsp;Chuyển số dư</a></li></ul></div></span>";
            }
        }
    }
}