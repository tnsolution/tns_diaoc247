﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="WebApp.FNC.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td10 {
            width: 10%;
        }

        .td20 {
            width: 20%;
        }

        .gachngang {
            text-decoration: line-through !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1 id="title">Thực chi
                <span class="tools pull-right">
                    <a href="#mPayment" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm thông tin
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả phòng --" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Month" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả tháng --" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="center">
                        <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                            <i class="ace-icon fa fa-search"></i>
                            Tìm
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade" id="mPayment" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin thực chi</h4>
                </div>
                <div class="modal-body">                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Phòng cần chi</label>
                                <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Người cần chi</label>
                                <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Ngày thực chi</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_PaymentDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Hóa Đơn</label>
                                <div class="row ">
                                    <div class="col-xs-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="ace" value="1" name="chkStatus" id="chkStatus1" />
                                                <span class="lbl">Có</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="ace" value="0" name="chkStatus" id="chkStatus0" />
                                                <span class="lbl">Không</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nội dung</label>
                        <input type="text" class="form-control" id="txt_PaymentContents" placeholder="Nhập text" />
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <label class="control-label">Số tiền</label>
                                <input type="text" class="form-control" id="txt_PaymentMoney" placeholder="Nhập số" value="0" moneyinput="true" />
                            </div>
                            <div class="col-xs-4">
                                <label class="control-label">% VAT</label>
                                <input type="text" class="form-control" id="txt_PercentVAT" placeholder="Nhập số" value="0" moneyinput="true" maxlength="2" />
                            </div>
                            <div class="col-xs-4">
                                <label class="control-label">Thành tiền</label>
                                <input type="text" class="form-control" id="txt_PaymentAmount" placeholder="Nhập số" value="0" moneyinput="true" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ghi chú</label>
                        <textarea class="form-control" id="txt_PaymentDescription" placeholder="Nhập text" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSavePayment" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mApprove" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" id="pophead">Bạn có đồng ý duyệt thông tin</h4>
                </div>
                <div class="modal-body">
                    <textarea id="txt_Note" rows="5" cols="20" class="form-control" placeholder="Nội dung, lý do, duyệt/ không duyệt"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnNotOK" data-dismiss="modal">
                        <i class="ace-icon fa fa-crosshairs"></i>
                        Không duyệt
                    </button>
                    <button type="button" class="btn btn-info" id="btnOK" data-dismiss="modal">
                        <i class="ace-icon fa fa-check"></i>
                        Duyệt
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Category" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Level" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });

            //$('[id$=DDL_Department1]').change(function () {
            //    var Key = $(this).val();
            //    $.ajax({
            //        type: "POST",
            //        url: "/Ajax.aspx/GetEmployees",
            //        data: JSON.stringify({
            //            DepartmentKey: $(this).val(),
            //        }),
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        beforeSend: function () {
            //        },
            //        success: function (msg) {
            //            var District = $("[id$=DDL_Employee1]");
            //            District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
            //            $.each(msg.d, function () {
            //                District.append($("<option></option>").val(this['Value']).html(this['Text']));
            //            });
            //        },
            //        complete: function () {
            //        },
            //        error: function (xhr, ajaxOptions, thrownError) {
            //            console.log(xhr.status);
            //            console.log(xhr.responseText);
            //            console.log(thrownError);
            //        }
            //    });
            //});

            $('input[moneyinput]').number(true, 0);
            $("[id$=txt_PercentVAT]").blur(function () {
                var vat = $('[id$=txt_PercentVAT]').val().replace(/,/g, '');
                var money = $('[id$=txt_PaymentMoney]').val().replace(/,/g, '');
                var amount = parseFloat(money) * parseFloat(vat) / 100 + parseFloat(money);
                $('[id$=txt_PaymentAmount]').val(Page.FormatMoney(amount));
            });
            $("[id$=txt_PaymentMoney]").blur(function () {
                var vat = $('[id$=txt_PercentVAT]').val().replace(/,/g, '');
                var money = $('[id$=txt_PaymentMoney]').val().replace(/,/g, '');
                var amount = parseFloat(money) * parseFloat(vat) / 100 + parseFloat(money);
                $('[id$=txt_PaymentAmount]').val(Page.FormatMoney(amount));
            });
            $("#txt_PaymentDate").val(Page.getCurrentDate());            
            //----
            $("#btnSearch").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Payment.aspx/Search',
                    data: JSON.stringify({
                        'Month': $("[id$=DDL_Month]").val(),
                        'Department': $("[id$=DDL_Department]").val(),
                        'Category': $("[id$=HID_Category]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $("#tblPayment tbody").empty();
                        $("#tblPayment tbody").append($(msg.d.Result2));
                        $("#title").text("Thực chi tháng " + $("[id$=DDL_Month]").val());
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#tblPayment").on("click", "tr td:not(.noclick)", function () {
                var trid = $(this).closest('tr').attr('id');
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Payment.aspx/GetPayment',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            alert(msg.d.Message);
                            return false;
                        }
                        $('#mPayment').modal('show');
                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                        $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                        $('[id$=DDL_Employee1]').val(msg.d.PaymentTo).trigger("change");
                        $('[id$=txt_PaymentDate]').val(msg.d.PaymentDate);
                        $('[id$=txt_PaymentContents]').val(msg.d.Contents);
                        $('[id$=txt_PaymentMoney]').val(msg.d.Money);
                        $('[id$=txt_PercentVAT]').val(msg.d.VAT);
                        $('[id$=txt_PaymentAmount]').val(msg.d.Amount);
                        $('[id$=txt_PaymentDescription]').val(msg.d.Description);
                        $('input[name="chkStatus"][value=' + msg.d.IsHasVAT + ']').prop('checked', true);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#tblPayment").on("click", ".show-details-btn", function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).closest('tr').next().toggleClass('open');
                $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
            });
            $("#btnSavePayment").on('click', function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Payment.aspx/SavePayment',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Department': $('[id$=DDL_Department1]').val(),
                        'Employee': $('[id$=DDL_Employee1]').val(),
                        'Category': $("[id$=HID_Category]").val(),
                        'Date': $('[id$=txt_PaymentDate]').val(),
                        'Contents': $('[id$=txt_PaymentContents]').val(),
                        'Description': $('[id$=txt_PaymentDescription]').val(),
                        'Money': $('[id$=txt_PaymentMoney]').val(),
                        'VAT': $('[id$=txt_PercentVAT]').val(),
                        'Amount': $('[id$=txt_PaymentAmount]').val(),
                        'Check': $('input[name="chkStatus"]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $("[id$=HID_AutoKey]").val(0);
                            location.href = '/FNC/Payment.aspx?Category=' + $("[id$=HID_Category]").val();
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });

            });
            $("#btnOK").click(function () {
                ApproveCheck(1)
            });
            $("#btnNotOK").click(function () {
                ApproveCheck(-1)
            });
        });
        function PopupCheck(id, lvl) {
            $("[id$=HID_AutoKey]").val(id);
            $("[id$=HID_Level]").val(lvl);
            if (lvl == 2) {
                $("#pophead").text("Bạn có muốn xác nhận thông tin !.");
            }
            $("#mApprove").modal("show");
        }
        function ApproveCheck(approve) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Payment.aspx/PaymentStatus',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Approve': approve,
                        'Note': $("#txt_Note").val(),
                        'Level': $("[id$=HID_Level]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showNotiMessageError("Thông báo !", msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
