﻿using Lib.FNC;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.FNC
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["Category"] != null)
                    HID_Category.Value = Request["Category"];

                Tools.DropDown_DDL_Month(DDL_Month);
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                LoadData();
            }
        }
        void LoadData()
        {
            DataTable zTable = new DataTable();
            int CurrentUnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int CurrentDepartmenty = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Category = HID_Category.Value.ToInt();
            switch (CurrentUnitLevel)
            {
                case 1:
                case 0:
                    zTable = Payment_Detail_Data.List(Category);
                    break;

                default:
                    zTable = Payment_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, CurrentDepartmenty, Category);
                    break;
            }

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblPayment'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Ngày chi</th>");
            zSb.AppendLine("        <th>Người chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền</th>");
            zSb.AppendLine("        <th>VAT</th>");
            zSb.AppendLine("        <th>Thành tiền</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total1 = 0, Total2 = 0, Total3 = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    double MoneyVat = (r["Money"].ToDouble() * r["VAT"].ToInt()) / 100;

                    #region [Kiểm tra tình trạng]
                    string BackColor = "";
                    int Duyet1 = r["IsApproved"].ToInt();
                    int Duyet2 = r["IsApproved2"].ToInt();
                    string ClassGachNgang = "";
                    string Jquery = "style='float:right'";
                    string Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    string Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";

                    if (Duyet1 == 0)
                        Jquery = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",1)'";
                    else
                    {
                        if (Duyet1 == 1 && Duyet2 == 0)
                        {
                            Jquery = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",2)'";
                        }
                    }

                    switch (Duyet1)
                    {
                        case -1:
                            Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;

                        case 1:
                            Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["ApprovedName"].ToString();
                            break;
                    }

                    switch (Duyet2)
                    {
                        case -1:
                            Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;

                        case 1:
                            Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["ApprovedName"].ToString();
                            break;
                    }

                    if (Duyet1 == -1 || Duyet2 == -1)
                        ClassGachNgang = "gachngang";

                    if (Duyet1 == 0 || Duyet2 == 0)
                        BackColor = "style=background-color:#c5d0dc";
                    #endregion

                    zSb.AppendLine("            <tr " + BackColor + " id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td class='noclick'>" + (No++) + "</td>");
                    zSb.AppendLine("               <td class='noclick'><div class='action-buttons pull-right'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + MoneyVat.ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                    zSb.AppendLine("            <tr class='detail-row'>");
                    zSb.AppendLine("                <td colspan='8' class='noclick'>");
                    zSb.AppendLine("                    <div class='table-detail'>");
                    zSb.AppendLine("                        <table class='table table-bordered'>");
                    zSb.AppendLine("                            <tr>");
                    zSb.AppendLine("                                <td class='td20 noclick'>Xác nhận: " + Status1 + "</td>");

                    if (r["ApproveNote"].ToString().Trim() != string.Empty)
                        zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote"].ToString() + "</td>");

                    zSb.AppendLine("                                <td class='td20 noclick'>Duyệt: " + Status2 + "</td>");

                    if (r["ApproveNote2"].ToString().Trim() != string.Empty)
                        zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote2"].ToString() + "</td>");

                    zSb.AppendLine("                                <td class='noclick'>Ghi chú: " + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("                            </tr>");
                    zSb.AppendLine("                        </table>");
                    zSb.AppendLine("                    </div>");
                    zSb.AppendLine("                </td>");
                    zSb.AppendLine("            </tr>");

                    //if (Duyet2 == 1)
                    //{
                    Total1 += r["Money"].ToDouble();
                    Total2 += MoneyVat;
                    Total3 += r["Amount"].ToDouble();
                    //}
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='4' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total1.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total2.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total3.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='8'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Literal_Table.Text = zSb.ToString();
        }
        [WebMethod]
        public static ItemResult Search(string Month, string Department, string Category)
        {
            ItemResult zResult = new ItemResult();
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Payment_Detail_Data.List(Month.ToInt(), DateTime.Now.Year, Department.ToInt(), Category.ToInt());
            if (zTable.Rows.Count > 0)
            {


                int No = 1;
                double Total1 = 0, Total2 = 0, Total3 = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    double MoneyVat = (r["Money"].ToDouble() * r["VAT"].ToInt()) / 100;

                    #region [Kiểm tra tình trạng]
                    string BackColor = "";
                    int Duyet1 = r["IsApproved"].ToInt();
                    int Duyet2 = r["IsApproved2"].ToInt();
                    string ClassGachNgang = "";
                    string Jquery = "style='float:right'";
                    string Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    string Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";

                    if (Duyet1 == 0)
                        Jquery = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",1)'";
                    else
                    {
                        if (Duyet1 == 1 && Duyet2 == 0)
                        {
                            Jquery = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",2)'";
                        }
                    }

                    switch (Duyet1)
                    {
                        case -1:
                            Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;

                        case 1:
                            Status1 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["ApprovedName"].ToString();
                            break;
                    }

                    switch (Duyet2)
                    {
                        case -1:
                            Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;

                        case 1:
                            Status2 = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["ApprovedName"].ToString();
                            break;
                    }

                    if (Duyet1 == -1 || Duyet2 == -1)
                        ClassGachNgang = "gachngang";

                    if (Duyet1 == 0 || Duyet2 == 0)
                        BackColor = "style=background-color:#c5d0dc";
                    #endregion

                    zSb.AppendLine("            <tr " + BackColor + " id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td class='noclick'>" + (No++) + "</td>");
                    zSb.AppendLine("               <td class='noclick'><div class='action-buttons pull-right'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + MoneyVat.ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                    zSb.AppendLine("            <tr class='detail-row'>");
                    zSb.AppendLine("                <td colspan='8' class='noclick'>");
                    zSb.AppendLine("                    <div class='table-detail'>");
                    zSb.AppendLine("                        <table class='table table-bordered'>");
                    zSb.AppendLine("                            <tr>");
                    zSb.AppendLine("                                <td class='td20 noclick'>Xác nhận: " + Status1 + "</td>");
                    zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote"].ToString() + "</td>");
                    zSb.AppendLine("                                <td class='td20 noclick'>Duyệt: " + Status2 + "</td>");
                    zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote2"].ToString() + "</td>");
                    zSb.AppendLine("                                <td class='noclick'>Ghi chú: " + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("                            </tr>");
                    zSb.AppendLine("                        </table>");
                    zSb.AppendLine("                    </div>");
                    zSb.AppendLine("                </td>");
                    zSb.AppendLine("            </tr>");

                    if (Duyet2 == 1)
                    {
                        Total1 += r["Money"].ToDouble();
                        Total2 += MoneyVat;
                        Total3 += r["Amount"].ToDouble();
                    }
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='4' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total1.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total2.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total3.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zResult.Result2 = zSb.ToString();
            return zResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AutoKey"></param>
        /// <param name="Approve"></param>
        /// <param name="Note"></param>
        /// <param name="Level">1 -duyệt, 2- là xác nhận</param>
        /// <returns></returns>
        [WebMethod]
        public static ItemResult PaymentStatus(int AutoKey, int Approve, string Note, int Level)
        {
            ItemResult zResult = new ItemResult();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (Permitsion[2].ToInt() == 0 && UnitLevel > 2)
            {
                zResult.Message = "Bạn chưa có quyền duyệt thông tin này xin vui lòng liên hệ quản lý !.";
                return zResult;
            }

            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            if (Level == 1)
            {
                zInfo.IsApproved = Approve;
                zInfo.ApproveNote = Note.Trim() + "\r\n";
                zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SetStatus();
            }
            else
            {
                zInfo.IsApproved2 = Approve;
                zInfo.ApproveNote2 = Note.Trim() + "\r\n";
                zInfo.Approved2By = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SetStatus2();
            }
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemPayment GetPayment(int AutoKey)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            ItemPayment zItem = new ItemPayment();
            zItem.VAT = zInfo.VAT.ToString();
            zItem.IsHasVAT = zInfo.IsHasVAT.ToString();
            zItem.Money = zInfo.Money.ToDoubleString();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.PaymentDate = zInfo.PaymentDate.ToDateString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.PaymentTo = zInfo.PaymentTo.ToString();
            if (zInfo.IsApproved == 1 || zInfo.IsApproved == -1 ||
                zInfo.IsApproved2 == 1 || zInfo.IsApproved2 == -1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static string SavePayment(int AutoKey, string Department, string Employee, string Category, string Date, string Contents, string Description, string Money, string VAT, string Amount, int Check)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            zInfo.PaymentDate = Tools.ConvertToDate(Date);
            zInfo.PaymentTo = Employee.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.VAT = VAT.ToInt();
            zInfo.Amount = double.Parse(Amount);
            zInfo.Money = double.Parse(Money);
            zInfo.IsHasVAT = Check;
            zInfo.CategoryKey = Category.ToInt();
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (zInfo.Message == string.Empty)
            {
                return "OK";
            }
            else
            {
                return "Lỗi không lưu được";
            }
        }
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');           
        }
    }
}
