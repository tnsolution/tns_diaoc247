﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Receipt.aspx.cs" Inherits="WebApp.FNC.Receipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td10 {
            width: 10%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1 id="title">Thu
                <span class="tools pull-right">
                    <a href="#mReceipt" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm thông tin
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả phòng --" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Month" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả tháng --" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="center">
                        <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                            <i class="ace-icon fa fa-search"></i>
                            Tìm
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade" id="mReceipt" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin thu</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group" id="divphong">
                                <label class="control-label">Phòng</label>
                                <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="control-label">Người giao tiền</label>
                                        <asp:DropDownList ID="DDL_Employee2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="control-label">Người nhận tiền</label>
                                        <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Ngày thu/ nhận tiền</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_ReceiptDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung</label>
                                <input type="text" class="form-control" id="txt_ReceiptContents" placeholder="Nhập text" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <input type="text" class="form-control" id="txt_ReceiptAmount" placeholder="Nhập số" moneyinput="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú</label>
                                <textarea class="form-control" id="txt_ReceiptDescription" placeholder="Nhập text" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveReceipt" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Category" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();
        });
        $(document).ready(function () {
            $('[id$=DDL_Department1]').change(function () {
                var Key = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        DepartmentKey: $(this).val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee1]");
                        District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                        $.each(msg.d, function () {
                            District.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $("input[moneyinput]").number(true, 0);
            $("#txt_ReceiptDate").val(Page.getCurrentDate());
            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });
            //----
            $("#btnSearch").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Receipt.aspx/Search',
                    data: JSON.stringify({
                        'Month': $("[id$=DDL_Month]").val(),
                        'Department': $("[id$=DDL_Department]").val(),
                        'Category': $("[id$=HID_Category]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $("#tblReceipt tbody").empty();
                        $("#tblReceipt tbody").append($(msg.d.Result));
                        $("#title").text("Chi phí tháng " + $("[id$=DDL_Month]").val());
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#tblReceipt tr td:not(:nth-child(7))").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Receipt.aspx/GetReceipt',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            alert(msg.d.Message);
                            return false;
                        }
                        $('#mReceipt').modal('show');

                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                        $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                        $('[id$=DDL_Employee1]').val(msg.d.ReceiptFrom).trigger("change");
                        $('[id$=DDL_Employee2]').val(msg.d.ReceiptBy).trigger("change");
                        $('[id$=txt_ReceiptDate]').val(msg.d.ReceiptDate);
                        $('[id$=txt_ReceiptContents]').val(msg.d.Contents);
                        $('[id$=txt_ReceiptAmount]').val(msg.d.Amount);
                        $('[id$=txt_ReceiptDescription]').val(msg.d.Description);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSaveReceipt").on('click', function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Receipt.aspx/SaveReceipt',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Department': $('[id$=DDL_Department1]').val(),
                        'Employee1': $('[id$=DDL_Employee1]').val(),
                        'Employee2': $('[id$=DDL_Employee2]').val(),
                        'Category': $("[id$=HID_Category]").val(),
                        'Date': $('[id$=txt_ReceiptDate]').val(),
                        'Contents': $('[id$=txt_ReceiptContents]').val(),
                        'Description': $('[id$=txt_ReceiptDescription]').val(),
                        'Amount': $('[id$=txt_ReceiptAmount]').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $("[id$=HID_AutoKey]").val(0);
                            location.href = '/FNC/Receipt.aspx?Category=' + $("[id$=HID_Category]").val();
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        function ApproveCheck(trid) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Receipt.aspx/ReceiptStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
