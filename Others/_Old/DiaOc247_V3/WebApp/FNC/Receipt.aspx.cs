﻿using Lib.FNC;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class Receipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL_Month(DDL_Month);
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                LoadData();
            }
        }
        void LoadData()
        {
            if (Request["Category"] != null)
                HID_Category.Value = Request["Category"];

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Category = HID_Category.Value.ToInt();

            DataTable zTable = new DataTable();
            if (UnitLevel <= 1)
                zTable = Receipt_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, 0, Category);
            else
                zTable = Receipt_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, Department, Category);
            StringBuilder zSb = new StringBuilder();

            #region [Thu]
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblReceipt'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Ngày thu</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th>Người giao tiền</th>");
            zSb.AppendLine("        <th>Người nhận tiền</th>");
            zSb.AppendLine("        <th>Số tiền thu</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    string DisableRow = "";
                    string Status = "";
                    if (r["IsApproved"].ToInt() == 1)
                    {
                        DisableRow = "class=\"disable\"";
                        Status = "<img width=20 style='float:right' id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        DisableRow = "'";
                        Status = "<img width=20 style='float:right' onclick='ApproveCheck(" + r["AutoKey"].ToString() + ")' id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' " + DisableRow + ">");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptByName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + Status + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='6' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='7'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            Literal_Table.Text = zSb.ToString();
        }
        [WebMethod]
        public static ItemResult Search(string Month, string Department, string Category)
        {
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();

            ItemResult zResult = new ItemResult();
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Receipt_Detail_Data.List(Month.ToInt(), DateTime.Now.Year, Department.ToInt(), Category.ToInt());
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    string DisableRow = "";
                    string Jquery = "";
                    string Status = "";
                    if (r["ReceiptFrom"].ToInt() == CurrentEmployee && r["IsApproved"].ToInt() == 0)
                        Jquery = "style='cursor:pointer;float:right' onclick='ApproveCheck(" + r["AutoKey"].ToString() + ")'";
                    else
                        Jquery = "style='float:right'";
                    if (r["IsApproved"].ToInt() == 1)
                    {
                        DisableRow = "class=\"disable\"";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        DisableRow = "'";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' " + DisableRow + ">");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptByName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + Status + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='7'>Chưa có dữ liệu</td></tr>"); }
            zResult.Result = zSb.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemReceipt GetReceipt(int AutoKey)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.ReceiptDate.ToDateString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.ReceiptFrom.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static ItemResult ReceiptStatus(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (Permitsion[2].ToInt() == 0 && UnitLevel > 2)
            {
                zResult.Message = "Bạn chưa có quyền duyệt thông tin này xin vui lòng liên hệ quản lý !.";
                return zResult;
            }

            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string SaveReceipt(int AutoKey, string Department, string Employee1, string Employee2, string Category, string Date, string Contents, string Description, string Amount)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            zInfo.ReceiptDate = Tools.ConvertToDate(Date);
            zInfo.ReceiptFrom = Employee1.ToInt();
            zInfo.ReceiptBy = Employee2.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.CategoryKey = Category.ToInt();
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }

        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');

            if (result[2].ToInt() == 1 || UnitLevel <= 2)
            {

            }
        }

        //static string createEmailBody(string title, string message)
        //{
        //    string body = string.Empty;
        //    using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Email/AlertEmail.html")))
        //    {
        //        body = reader.ReadToEnd();
        //    }
        //    body = body.Replace("{Title}", title);
        //    body = body.Replace("{message}", message);
        //    return body;
        //}
    }
}