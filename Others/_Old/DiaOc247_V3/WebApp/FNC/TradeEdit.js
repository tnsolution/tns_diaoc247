﻿var tran_isDevined = 0;
$(function () {
    $('#table').ace_scroll({
        size: 450
    });
    $("[id$=DDL_AssetProject]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetCategoryAsset",
                data: JSON.stringify({
                    "ProjectKey": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    var District = $("[id$=DDL_AssetCategory]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    $('input:checkbox').change(function () {
        CalcMoney();
    });
    $('Input[CalcMoney="yes"]').blur(function (e) {
        CalcMoney();
    });
    $("#txt_Phone1").blur(function () {
        var edit = $("[id$=HID_CustomerEdit]").val();
        var phone = $("#txt_Phone1").val();
        if (phone.length == 10 ||
            phone.length == 11) {
            if (edit == 2) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeEdit.aspx/CheckPhone',
                    data: JSON.stringify({
                        'Phone': phone,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        var object = r.d;
                        if (object == undefined) {
                            alert("Lỗi thực hiện");
                            return false;
                        }
                        if (object.Phone1.length > 0 &&
                            object.CustomerName.length > 0) {
                            $("[id$=HID_CustomerKey]").val(object.CustomerKey);
                            $("#txt_CustomerName").val(object.CustomerName);
                            $("#txt_CardID").val(object.CardID);
                            $("#txt_CardPlace").val(object.CardPlace);
                            $("#txt_Phone1").val(object.Phone1);
                            $("#txt_Phone2").val(object.Phone2);
                            $("#txt_Address1").val(object.Address1);
                            $("#txt_Address2").val(object.Address2);
                            $("#txt_CardDate").val(object.CardDate);
                            $("#txt_Birthday").val(object.Birthday);
                            //Page.showNotiMessageInfo("Đã có thông tin khách này rồi");
                        }
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Lỗi phát sinh !');
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    });
    $(".select2").select2({ width: "100%" });
    $("#btnAddGuest").click(function () {
        $("#mOwner").modal('show');
        $("[id$=HID_CustomerEdit]").val(2);
    });
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });
    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
    $(".iframe").colorbox({ iframe: true, width: "100%", height: "100%" });
    //save file
    $("#UploadFile").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=btnUploadFile]").trigger("click");
    });
    $("#UploadImg").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=btnUploadImg]").trigger("click");
    });

    //del picture
    $("a[btn='btnDeleteImg']").click(function () {
        var Key = $(this).closest('div').parent().attr('id');
        delImg(Key);
    });
    //del file
    $("a[btn='btnDeleteFile']").click(function (e) {
        var id = $(this).closest('tr').attr("id");
        delFile(id);
    });
    //----

    var $overflow = '';
    var colorbox_params = {
        rel: 'colorbox',
        reposition: true,
        scalePhotos: true,
        scrolling: false,
        previous: '<i class="ace-icon fa fa-arrow-left"></i>',
        next: '<i class="ace-icon fa fa-arrow-right"></i>',
        close: '&times;',
        current: '{current} of {total}',
        maxWidth: '100%',
        maxHeight: '100%',
        onOpen: function () {
            $overflow = document.body.style.overflow;
            document.body.style.overflow = 'hidden';
        },
        onClosed: function () {
            document.body.style.overflow = $overflow;
        },
        onComplete: function () {
            $.colorbox.resize();
        }
    };

    $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
    $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon

    $('#id-input-file-1, #id-input-file-2').ace_file_input({
        style: 'well',
        no_file: 'No File ...',
        no_icon: 'ace-icon fa fa-cloud-upload',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        droppable: true,
        thumbnail: true, //| true | large
        whitelist: 'gif|png|jpg|jpeg',
        blacklist: 'exe|php'
    }).on("change", function () {
        if ($('#id-input-file-1').val().length > 0) {
            var ext = $('#id-input-file-1').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['txt', 'pdf', 'xls', 'doc', 'docx', 'xlsx', 'ppt', 'pptx']) == -1) {
                $("#UploadFile").addClass("disabled");
                alert('Tập tin không hợp lệ !');
            }
            else {
                $("#UploadFile").removeClass("disabled");
            }
        }
        if ($('#id-input-file-2').val().length > 0) {
            var ext2 = $('#id-input-file-2').val().split('.').pop().toLowerCase();
            if ($.inArray(ext2, ['jpg', 'png', 'gif']) == -1) {
                $("#UploadImg").addClass("disabled");
                alert('Tập tin không hợp lệ !');
            }
            else {
                $("#UploadImg").removeClass("disabled");
            }
        }
    });
    //-------------
    if ($('[id$=DDL_Category]').val() != 227)
        $("[type='divExpire']").hide();
    $('[id$=DDL_Category]').on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected == 227) {
            $("[type='divExpire']").show();
            must_enddate = 1;
        }
        else {
            $("[type='divExpire']").hide();
            must_enddate = 0;
        }
    });
    //-------------

    $("[id$=chkDevined]").change(function () {
        if ($(this).is(':checked')) {
            tran_isDevined = 1;
        }
        else {
            tran_isDevined = 0;
        }
        console.log(tran_isDevined);
    });
    //xoa phieu giao dich
    $("#btnDel").click(function () {
        if (confirm("Bạn có chắc xóa giao dịch này !.")) {
            $.ajax({
                type: "POST",
                url: "/FNC/TradeEdit.aspx/DeleteTrade",
                data: JSON.stringify({
                    'TradeKey': $("[id$=HID_TradeKey]").val(),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Message != '') {
                        Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
                    } else {
                        window.location = "/FNC/TradeList.aspx";
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //luu phieu giao dich
    $("#btnEdit").click(function () {
        var tran_employee = $("[id$=DDL_Employee]").val();
        var tran_category = $("[id$=DDL_Category]").val();
        var tran_datesign = $("[id$=txt_DateSign]").val();
        var tran_expired = $("[id$=txt_Expired]").val();
        var tran_datepre = $("[id$=txt_DatePreOrder]").val();
        var tran_description = $("[id$=txt_Description]").val();
        var amount_vat = $("[id$=txt_AmountVAT]").val();
        var amount = $("[id$=txt_Amount]").val();
        var vat = $("[id$=txt_VAT]").val();
        var otherfee = $("[id$=txt_OtherFee]").val();
        var commision = $("[id$=txt_Commision]").val();
        var income = $("[id$=txt_Income]").val()
        if (amount_vat == "")
            amount_vat = 0;
        if (amount == "")
            amount = 0;
        if (vat == "")
            vat = 0;
        if (otherfee == "")
            otherfee = 0;
        if (commision == "")
            commision = 0;
        if (income == "")
            income = 0;
      
        if (tran_category == 227) {
            if (tran_expired == '01/01/0001') {
                Page.showNotiMessageError("Bạn phải nhập ngày hết hợp đồng !.");
                return false;
            }
        }

        $.ajax({
            type: 'POST',
            url: '/FNC/TradeEdit.aspx/SaveTrade',
            data: JSON.stringify({
                "TradeKey": $("[id$=HID_TradeKey]").val(),
                "TradeCategory": tran_category,
                "DateSign": tran_datesign,
                "DateExpired": tran_expired,
                "DatePre": tran_datepre,
                "Description": tran_description,
                "Employee": tran_employee,
                "AmountVAT": amount_vat,
                "Amount": amount,
                "OtherFee": otherfee,
                "VAT": vat,
                "Commision": commision,
                "Income": income,
                "IsDevined": tran_isDevined
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d.Result > 0) {
                    Page.showNotiMessageInfo("Đã cập nhật thành công");
                    window.location = "/FNC/TradeEdit.aspx?ID=" + r.d.Result;
                }
                else {
                    Page.showPopupMessage("Lỗi cập nhật .", r.d.Message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Lỗi khởi tạo dữ liệu !');
                $('.se-pre-con').fadeOut('slow');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });
    //tim sp bang tblAssetSearch chọn bảng tblAssetSearch
    $("#btnAssetSearch").click(function () {
        var project = $("[id$=DDL_AssetProject]").val();
        var category = $("[id$=DDL_AssetCategory]").val();
        var assetID = $("#txt_AssetID").val();
        if (project == null)
            project = "0";
        if (category == null)
            category = "0";
        if (assetID == null)
            assetID = "";
        $('#tblAssetSearch > tbody > tr').remove();
        $.ajax({
            type: "POST",
            url: "/FNC/TradeEdit.aspx/AssetSearch",
            data: JSON.stringify({
                Project: project,
                Category: category,
                AssetID: assetID
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                for (var i = 0; i < msg.d.length; i++) {
                    var id = msg.d[i].AssetID;
                    var category = msg.d[i].CategoryName;
                    var area = msg.d[i].Area;
                    $('#tblAssetSearch > tbody:last').append('<tr class="" id="' + msg.d[i].AssetKey + '" type="' + msg.d[i].AssetType + '" style="cursor:pointer">'
                            + '<td>' + i++ + '</td>'
                            + '<td>' + id + '</td>'
                            + '<td>' + category + '</td>'
                            + '<td>' + area + '</td></tr>');
                }
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(".se-pre-con").fadeOut("slow");
                Page.showPopupMessage("...", "Lỗi thực hiện !.")
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //chon sp bang tblAssetSearch chon ra bang tblAsset
    $("#tblAssetSearch > tbody").on("click", "tr", function () {
        var id = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('type');
        var key = $("[id$=HID_TradeKey]").val();
        $.ajax({
            type: 'POST',
            url: '/FNC/TradeEdit.aspx/AssetSelect',
            data: JSON.stringify({
                'Key': key,
                'AssetKey': id,
                'Type': type
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                $('#mAssetFilter').modal('hide');
                $('#tblAsset > tbody > tr').remove();
                $('#tblAsset > tbody:last').append('<tr class="" id="' + msg.d.AssetKey + '" type="' + msg.d.AssetType + '">'
                        + '<td>#</td>'
                        + '<td>' + msg.d.ProjectName + '</td>'
                        + '<td>' + msg.d.AssetID + '</td>'
                        + '<td>' + msg.d.CategoryName + '</td>'
                        + '<td>' + msg.d.AreaWall + '</td>'
                        + '<td>' + msg.d.Address + '</td><td><div class="hidden-sm hidden-xs action-buttons pull-right"><a btn="btnDelAsset" href="#" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i> Xóa</a></div></td></tr>');
                Page.showNotiMessageInfo("..", "Đã cập nhật thông tin sản phẩm vào giao dịch.");

                $("[id$=HID_AssetType]").val(msg.d.AssetType);
                //get list owner
                ListOwner(id, msg.d.AssetID, type);
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Lỗi phát sinh !');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //xóa sp bang data trong tblAsset
    $("#tblAsset > tbody").on("click", "a[btn='btnDelAsset']", function () {
        if (confirm("Bạn có muốn xóa sp ra khỏi giao dịch")) {
            var key = $("[id$=HID_TradeKey]").val();
            $.ajax({
                type: 'POST',
                url: '/FNC/TradeEdit.aspx/AssetDelete',
                data: JSON.stringify({
                    'Key': key,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        Page.showPopupMessage("Lỗi xóa thông tin.", msg.d.Message);
                    }
                    else {
                        $('#tblAsset> tbody > tr').remove();
                        Page.showNotiMessageInfo("..", "Đã xóa sản phẩm trong thông tin giao dịch.");
                    }
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //bang tblAsset click
    $("#tblAsset > tbody").on("click", "tr td:not(:last-child)", function () {
        var id = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('type');
        var name = $(this).find('td').eq(2).text();
        ListOwner(id, name, type)

        $("[id$=HID_AssetKey]").val(id);
    });
    //chon khach từ bang tblOwnerList ra modal
    $("#tblOwnerList > tbody").on("click", "tr", function () {
        var id = $(this).closest('tr').attr('id');
        var fid = $(this).closest('tr').attr('fid');
        var type = $(this).closest('tr').attr('type');

        $.ajax({
            type: 'POST',
            url: '/FNC/TradeEdit.aspx/OneOwner',
            data: JSON.stringify({
                'OwnerKey': fid,
                'Type': type,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (r) {
                var object = r.d;
                $("[id$=HID_CustomerKey]").val(object.CustomerKey);
                $("#txt_CustomerName").val(object.CustomerName);
                $("#txt_CardID").val(object.CardID);
                $("#txt_CardPlace").val(object.CardPlace);
                $("#txt_Phone1").val(object.Phone1);
                $("#txt_Phone2").val(object.Phone2);
                $("#txt_Address1").val(object.Address1);
                $("#txt_Address2").val(object.Address2);
                $("#txt_CardDate").val(object.CardDate);
                $("#txt_Birthday").val(object.Birthday);
                $("#chkIsOwner1").prop('checked', true);
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        $('#mOwnerList').modal('hide');
        $('#mOwner').modal({
            backdrop: true,
            show: true
        });
    });
    //luu khach
    $("#btnSaveCustomer").click(function () {
        var edit = $("[id$=HID_CustomerEdit]").val();
        switch (edit) {
            case 1:
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeEdit.aspx/SaveCustomer',
                    data: JSON.stringify({
                        'CustomerKey': $("[id$=HID_CustomerKey]").val(),
                        'AssetKey': $("[id$=HID_AssetKey]").val(),
                        'AssetType': $("[id$=HID_AssetType]").val(),
                        'CustomerName': $("#txt_CustomerName").val(),
                        'Phone': $("#txt_Phone1").val(),
                        'Phone2': $("#txt_Phone2").val(),
                        'Address': $("#txt_Address1").val(),
                        'Address2': $("#txt_Address2").val(),
                        'CardDate': $("#txt_CardDate").val(),
                        'CardID': $("#txt_CardID").val(),
                        'CardPlace': $("#txt_CardPlace").val(),
                        'Birthday': $("#txt_Birthday").val(),
                        'IsOwner': $('input[name=chkPurpose]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            Page.showNotiMessageInfo("...", "Lưu thông tin khách thành công");

                        ListGuest();
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                break;

            case 2:
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeEdit.aspx/SaveNewCustomer',
                    data: JSON.stringify({
                        'TradeKey': $("[id$=HID_TradeKey]").val(),
                        'CustomerName': $("#txt_CustomerName").val(),
                        'AssetKey': $("[id$=HID_AssetKey]").val(),
                        'AssetType': $("[id$=HID_AssetType]").val(),
                        'Phone': $("#txt_Phone1").val(),
                        'Phone2': $("#txt_Phone2").val(),
                        'Address': $("#txt_Address1").val(),
                        'Address2': $("#txt_Address2").val(),
                        'CardDate': $("#txt_CardDate").val(),
                        'CardID': $("#txt_CardID").val(),
                        'CardPlace': $("#txt_CardPlace").val(),
                        'Birthday': $("#txt_Birthday").val(),
                        'EmployeeKey': $("[id$=DDL_Employee]").val(),
                        'IsOwner': $('input[name=chkPurpose]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            Page.showNotiMessageInfo("...", "Lưu thông tin khách thành công");

                        ListGuest();
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                break;

            default:
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeEdit.aspx/SaveGuest',
                    data: JSON.stringify({
                        'TradeKey': $("[id$=HID_TradeKey]").val(),
                        'OwnerKey': $("[id$=HID_OwnerKey]").val(),
                        'CustomerKey': $("[id$=HID_CustomerKey]").val(),
                        'AssetKey': $("[id$=HID_AssetKey]").val(),
                        'AssetType': $("[id$=HID_AssetType]").val(),
                        'CustomerName': $("#txt_CustomerName").val(),
                        'Phone': $("#txt_Phone1").val(),
                        'Phone2': $("#txt_Phone2").val(),
                        'Address': $("#txt_Address1").val(),
                        'Address2': $("#txt_Address2").val(),
                        'CardDate': $("#txt_CardDate").val(),
                        'CardID': $("#txt_CardID").val(),
                        'CardPlace': $("#txt_CardPlace").val(),
                        'Birthday': $("#txt_Birthday").val(),
                        'EmployeeKey': $("[id$=DDL_Employee]").val(),
                        'IsOwner': $('input[name=chkPurpose]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            Page.showNotiMessageInfo("...", "Lưu thông tin khách thành công");

                        ListGuest();
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                break;
        }

        $('#mOwner').modal('hide');
        $('#mGuestFilter').modal('hide');
        $("[id$=HID_CustomerEdit]").val(0);
        Page.resetValue("mOwner");
    });
    //xoa khach
    $("#tblCustomer > tbody").on("click", "a[btn='btnDelCustomer']", function () {
        if (confirm("Bạn có muốn xóa khách ra khỏi giao dịch")) {
            var key = $(this).closest('tr').attr('id');
            $.ajax({
                type: 'POST',
                url: '/FNC/TradeEdit.aspx/GuestDelete',
                data: JSON.stringify({
                    'TradeKey': $("[id$=HID_TradeKey]").val(),
                    'CustomerKey': key,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Message == "") {
                        $('#tblCustomer > tbody > tr[id=' + key + ']').remove();
                    }
                    else {
                        Page.showPopupMessage("Lỗi xóa thông tin !.", msg.d.Message);
                    }
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //edit khach
    $("#tblCustomer > tbody").on("click", "tr td:not(:last-child)", function () {
        var id = $(this).closest('tr').attr('id');
        $("[id$=HID_CustomerEdit]").val(1);

        OpenGuest(id);
    });
    //tim khach vao bang tblGuestSearch
    $("#btnGuestSearch").click(function () {
        var name = $("#txt_Search_CustomerName").val();
        var phone = $("#txt_Search_CustomerID").val();

        $.ajax({
            type: "POST",
            url: "/FNC/TradeEdit.aspx/GuestSearch",
            data: JSON.stringify({
                Phone: phone,
                Name: name
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                $('#tblGuestSearch> tbody > tr').remove();
                var no = 1;
                for (var i = 0; i < msg.d.length; i++) {
                    $('#tblGuestSearch > tbody:last').append('<tr class="" ID="' + msg.d[i].CustomerKey + '">'
                            + '<td>' + (no++) + '</td>'
                            + '<td>' + msg.d[i].CustomerName + '</td>'
                            + '<td>' + msg.d[i].Phone1 + '<br/>' + msg.d[i].Phone2 + '</td></tr>');
                }
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Lỗi phát sinh !');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //chon khach da tim dc
    $("#tblGuestSearch > tbody").on("click", "tr", function () {
        var id = $(this).closest('tr').attr('id');
        OpenGuest(id);
    });
});
function ListOwner(id, name, type) {
    $.ajax({
        type: 'POST',
        url: '/FNC/TradeEdit.aspx/GetOwner',
        data: JSON.stringify({
            'AssetKey': id,
            'Type': type,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            if (r.d.length > 1) {
                var textmsg = "Lưu ý mã căn " + name + " có nhiều thông tin chủ nhà !. Vui lòng tự chọn thông tin.";
                textmsg += "(Thông tin mới gần nhất đầu tiên !.)";
                $("#note").text(textmsg);
                $("#tblOwnerList > tbody > tr").remove();
                for (var i = 0; i < r.d.length; i++) {
                    $("#tblOwnerList > tbody:last").append('<tr type=' + type + ' fid=' + r.d[i].OwnerKey + ' id=' + r.d[i].CustomerKey + '><td>' + i + '</td><td>' + r.d[i].CustomerName + '</td><td>' + r.d[i].Phone1 + '</td><td>' + r.d[i].CreatedDate + '</td></tr>');
                }

                $('#mOwnerList').modal({
                    backdrop: true,
                    show: true
                });
            }
            else {
                var object = r.d[0];
                if (object == undefined) {
                    alert("Lỗi thực hiện");
                    return false;
                }

                $("[id$=HID_OwnerKey]").val(object.OwnerKey);
                $("[id$=HID_CustomerKey]").val(object.CustomerKey);
                $("#txt_CustomerName").val(object.CustomerName);
                $("#txt_CardID").val(object.CardID);
                $("#txt_CardPlace").val(object.CardPlace);
                $("#txt_Phone1").val(object.Phone1);
                $("#txt_Phone2").val(object.Phone2);
                $("#txt_Address1").val(object.Address1);
                $("#txt_Address2").val(object.Address2);
                $("#txt_CardDate").val(object.CardDate);
                $("#txt_Birthday").val(object.Birthday);
                $("#chkIsOwner1").prop('checked', true);

                $('#mOwner').modal({
                    backdrop: true,
                    show: true
                });
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function ListGuest() {
    $("#tblCustomer > tbody").empty();
    $.ajax({
        type: "POST",
        url: "/FNC/TradeEdit.aspx/GetGuest",
        data: JSON.stringify({
            'TradeKey': $("[id$=HID_TradeKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (msg) {
            var no = 1;
            for (var i = 0; i < msg.d.length; i++) {
                $('#tblCustomer > tbody:last').append('<tr class="" ID="' + msg.d[i].CustomerKey + '">'
                    + '<td>' + (no++) + '</td>'
                    + '<td>' + msg.d[i].CustomerName + '<br/>' + msg.d[i].Owner + '</td>'
                    + '<td>' + msg.d[i].Phone1 + '<br/>' + msg.d[i].Phone2 + '</td>'
                    + '<td>' + msg.d[i].Birthday + '</td>'
                    + '<td>' + msg.d[i].CardID + '<br/>Nơi cấp:' + msg.d[i].CardPlace + '</td>'
                    + '<td> Liên hệ: ' + msg.d[i].Address1 + '<br/>Thường trú:' + msg.d[i].Address2 + '</td>'
                    + '<td><div class="hidden-sm hidden-xs action-buttons pull-right"><a btn="btnDelCustomer" href="#" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i> Xóa</a></div></td></tr>');
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Lỗi phát sinh !');
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function OpenGuest(id) {
    $.ajax({
        type: 'POST',
        url: '/FNC/TradeEdit.aspx/GetCustomer',
        data: JSON.stringify({
            'TradeKey': $("[id$=HID_TradeKey]").val(),
            'CustomerKey': id,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            var object = r.d;
            if (object == undefined) {
                alert("Lỗi thực hiện");
                return false;
            }

            $("[id$=HID_CustomerKey]").val(object.CustomerKey);
            $("#txt_CustomerName").val(object.CustomerName);
            $("#txt_CardID").val(object.CardID);
            $("#txt_CardPlace").val(object.CardPlace);
            $("#txt_Phone1").val(object.Phone1);
            $("#txt_Phone2").val(object.Phone2);
            $("#txt_Address1").val(object.Address1);
            $("#txt_Address2").val(object.Address2);
            $("#txt_CardDate").val(object.CardDate);
            $("#txt_Birthday").val(object.Birthday);
            if (object.IsOwner == 3)
                $("#chkIsOwner3").prop('checked', true);
            if (object.IsOwner == 2)
                $("#chkIsOwner2").prop('checked', true);
            if (object.IsOwner == 1)
                $("#chkIsOwner1").prop('checked', true);

            $('#mOwner').modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function CalcMoney() {
    //Giá đã VAT
    var AmountVAT = $('[id$=txt_AmountVAT]').val().replace(/,/g, '');
    var VAT = ($('[id$=txt_VAT]').val() / 100) + 1;
    //Giá chưa VAT
    var Amount = parseFloat(AmountVAT) / parseFloat(VAT);
    $('[id$=txt_Amount]').val(Page.FormatMoney(Amount));
    //Mức phí hoa hồng
    var Commision = $('[id$=txt_Commision]').val().replace(/,/g, '');
    $('[id$=txt_Commision]').val(Page.FormatMoney(Commision));
    //Phi giảm trừ
    var OtherFee = $('[id$=txt_OtherFee]').val().replace(/,/g, '');
    //doanh thu
    var Income = Commision - OtherFee;
    var checked_radio = $("[id$='chkDevined']:checked");
    var value = checked_radio.val();
    if (value == 1) {
        var temp = Commision / (1.1);
        Income = temp - OtherFee;
    }
    $('[id$=txt_Income]').val(Page.FormatMoney(Income));
};
function delFile(id) {
    $.ajax({
        type: "POST",
        url: "/FNC/TradeEdit.aspx/DeleteFile",
        data: JSON.stringify({
            'FileKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                $("#tblFile tr[id=" + id + "]").remove();
                //location.reload(true);
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delImg(id) {
    $.ajax({
        type: "POST",
        url: "/FNC/TradeEdit.aspx/DeleteFile",
        data: JSON.stringify({
            'FileKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                $("ul #" + id + "").remove();
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}