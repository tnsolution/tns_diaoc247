﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TradeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                if (Request["Type"] != null)
                    HID_TradeType.Value = Request["Type"];
                                                
                CheckRole();
                LoadData();
            }
        }
        void LoadData()
        {
            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int TradeType = HID_TradeType.Value.ToInt();

            if (TradeType == 1)
            {
                Lit_TitlePage.Text = "Các giao dịch chưa duyệt";
            }
            if (TradeType == 2)
            {
                Lit_TitlePage.Text = "Các giao dịch chưa thanh toán";
            }
            if (TradeType == 3 || TradeType == 0)
            {
                Lit_TitlePage.Text = "Các giao dịch";
            }
            DDL_Status.SelectedValue = TradeType.ToString();
            DataTable zTable = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = Trade_Data.Get(0, 0, TradeType, startDate, endDate);
                    break;

                case 2:
                    zTable = Trade_Data.Get(Department, 0, TradeType, startDate, endDate);
                    break;

                default:
                    zTable = Trade_Data.Get(Department, Employee, TradeType, startDate, endDate);
                    break;
            }

            ViewHtml(zTable);
        }
        [WebMethod]
        public static string Search(int Department, int Employee, string FromDate, string ToDate, int Project, int CategoryTrade, int TradeType)
        {
            DataTable Table = Trade_Data.Get(Department, Employee, TradeType, Tools.ConvertToDate(FromDate), Tools.ConvertToDate(ToDate), Project, CategoryTrade);
            StringBuilder zSb = new StringBuilder();

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td>");
                zSb.AppendLine("            <td colspan='10'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }
        void ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày lập GD</th>");
            zSb.AppendLine("        <th>Ngày ký hợp đồng</th>");
            zSb.AppendLine("        <th>Tên dự án</th>");
            zSb.AppendLine("        <th>Mã căn hộ</th>");
            zSb.AppendLine("        <th>Loại căn hộ</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Giao dịch</th>");
            zSb.AppendLine("        <th>Doanh thu</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td>");
                zSb.AppendLine("            <td colspan='10'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            //DataTable zTableDetail = new DataTable();
            //int Employee = DDL_Employee2.SelectedValue.ToInt();
            //int Department = DDL_Department.SelectedValue.ToInt();
            //int TradeType = HID_TradeType.Value.ToInt();

            //zTableDetail = Trade_Data.Get(Department, Employee, TradeType, Tools.ConvertToDate(txtFromDate.Value.Trim()), Tools.ConvertToDate(txtToDate.Value.Trim()));
            //ViewHtml(zTableDetail);
        }
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL04";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Employee2.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY DepartmentName", false);

                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Employee2.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    //DDL_Employee.Visible = false;
                    DDL_Employee2.Visible = true;
                    break;
            }
        }
    }
}