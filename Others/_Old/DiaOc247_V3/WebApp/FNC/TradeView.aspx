﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeView.aspx.cs" Inherits="WebApp.FNC.TradeView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết giao dịch
                <asp:Literal ID="txt_Name" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a type="button" class="btn btn-white btn-default btn-bold" href="TradeList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin giao dịch
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#folder">
                                <i class="blue ace-icon fa fa-folder bigger-110"></i>
                                Tập tin        
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#image">
                                <i class="blue ace-icon fa fa-image bigger-110"></i>
                                Hình ảnh
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane active">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        Thông tin giao dịch
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="profile-user-info profile-user-info-striped">
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Người lập</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_CreatedBy" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Ngày lập phiếu</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_DateTrade" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Loại giao dịch</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_TradeCategory" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Ngày đặt cọc</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_DatePreOrder" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Ngày ký hợp đồng</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_DateSign" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Ngày hết hợp đồng</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_DateExpired" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="profile-user-info profile-user-info-striped">
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Giá có VAT</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_PriceVAT" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Giá chưa VAT</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_NotVAT" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">VAT</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_Vat" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Hoa hồng</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_Commision" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Phí giảm trừ</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_OtherFee" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Doanh thu (VNĐ)</div>
                                                        <div class="profile-info-value">
                                                            <span class="giatien">
                                                                <asp:Literal ID="txt_Income" runat="server" Text="7,000,000"></asp:Literal>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="profile-user-info profile-user-info-striped">
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Duyệt giao dịch</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_Approve" runat="server" Text="..."></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Người duyệt giao dịch</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_ApproveBy" runat="server" Text="..."></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Ngày duyệt giao dịch</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_ApproveDate" runat="server" Text="..."></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Doanh thu</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_Finish" runat="server" Text="..."></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Người duyệt doanh thu</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_FinishBy" runat="server" Text="..."></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="profile-info-row">
                                                        <div class="profile-info-name">Ngày duyệt doanh thu</div>
                                                        <div class="profile-info-value">
                                                            <asp:Literal ID="txt_FinishDate" runat="server" Text="..."></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        Sản phẩm giao dịch
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <asp:Literal ID="Lit_TableProduct" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        Chủ nhà, khách giao dịch
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <asp:Literal ID="Lit_TableCustomer" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Ghi chú</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Description" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="folder" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="ace-thumbnails clearfix">
                                        <asp:Literal ID="Lit_ListFolder" runat="server"></asp:Literal>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="image" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="ace-thumbnails clearfix">
                                        <asp:Literal ID="Lit_ListImage" runat="server"></asp:Literal>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mApprove" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Xử lý duyệt giao dịch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <ul class="list-unstyled spaced">
                            <li class="text-warning bigger-110 orange">
                                <i class="ace-icon fa fa-exclamation-triangle"></i>
                                Thay đổi ngày xử lý.
                            </li>
                        </ul>
                        <div>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="txt_Date" type="text" id="txt_DateApprove" role="datepicker" class="form-control pull-right" placeholder="Chọn ngày tháng năm" required="" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnApproved">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mFinish" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Xử lý hoàn tất giao dịch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <ul class="list-unstyled spaced">
                            <li class="text-warning bigger-110 orange">
                                <i class="ace-icon fa fa-exclamation-triangle"></i>
                                Thay đổi ngày xử lý.
                            </li>
                        </ul>
                        <div>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="txt_Date" type="text" id="txt_DateFinish" role="datepicker" class="form-control pull-right" placeholder="Chọn ngày tháng năm" required="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnFinish">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TradeKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".iframe").colorbox({
                iframe: true,
                width: "100%",
                height: "100%",
                close: '&times;'
            });

            var $overflow = '';
            var colorbox_params = {
                rel: 'colorbox',
                reposition: true,
                scalePhotos: true,
                scrolling: false,
                previous: '<i class="ace-icon fa fa-arrow-left"></i>',
                next: '<i class="ace-icon fa fa-arrow-right"></i>',
                close: '&times;',
                current: '{current} of {total}',
                maxWidth: '100%',
                maxHeight: '100%',
                onOpen: function () {
                    $overflow = document.body.style.overflow;
                    document.body.style.overflow = 'hidden';
                },
                onClosed: function () {
                    document.body.style.overflow = $overflow;
                },
                onComplete: function () {
                    $.colorbox.resize();
                }
            };
            $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);

            $("#txt_DateApprove").val(Page.getCurrentDate());
            $("#txt_DateFinish").val(Page.getCurrentDate());

            $("#btnFinish").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeView.aspx/SaveFinish',
                    data: JSON.stringify({
                        'TradeKey': $("[id$=HID_TradeKey]").val(),
                        'DateFinish': $("[id$=txt_DateFinish]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (!isNaN(+msg.d) && isFinite(msg.d)) {
                            location.href = '/FNC/TradeView.aspx?ID=' + msg.d;
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Lỗi phát sinh !');
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnApproved").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeView.aspx/SaveApprove',
                    data: JSON.stringify({
                        'TradeKey': $("[id$=HID_TradeKey]").val(),
                        'DateApprove': $("[id$=txt_DateApprove]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (!isNaN(+msg.d) && isFinite(msg.d)) {
                            location.href = '/FNC/TradeView.aspx?ID=' + msg.d;
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Lỗi phát sinh !');
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#cancelFinish").click(function () {
                if (confirm("Bạn có chắc hủy lệnh hoàn thành giao dịch")) {
                    $.ajax({
                        type: 'POST',
                        url: '/FNC/TradeView.aspx/SaveFinish',
                        data: JSON.stringify({
                            'TradeKey': $("[id$=HID_TradeKey]").val(),
                            'DateFinish': $("[id$=txt_DateFinish]").val(),
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (!isNaN(+msg.d) && isFinite(msg.d)) {
                                location.href = '/FNC/TradeView.aspx?ID=' + msg.d;
                            }
                            else {
                                alert(msg.d);
                            }
                        },
                        complete: function () {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert('Lỗi phát sinh !');
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $("#cancelApprove").click(function () {
                if (confirm("Bạn có chắc hủy bỏ lệnh duyệt giao dịch")) {
                    $.ajax({
                        type: 'POST',
                        url: '/FNC/TradeView.aspx/SaveApprove',
                        data: JSON.stringify({
                            'TradeKey': $("[id$=HID_TradeKey]").val(),
                            'DateApprove': $("[id$=txt_DateApprove]").val(),
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (!isNaN(+msg.d) && isFinite(msg.d)) {
                                location.href = '/FNC/TradeView.aspx?ID=' + msg.d;
                            }
                            else {
                                alert(msg.d);
                            }
                        },
                        complete: function () {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert('Lỗi phát sinh !');
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
