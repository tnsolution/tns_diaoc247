﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.FNC
{
    public partial class ViewChart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1);
                DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31);

                txtFromDate.Value = startDate.ToString("dd/MM/yyyy");
                txtToDate.Value = endDate.ToString("dd/MM/yyyy");

                CheckRole();
                LoadData();
            }
        }
        void LoadData()
        {
            DataTable zTableChart = new DataTable();
            DataTable zTableDetail = new DataTable();
            int Employee = DDL_Employee.SelectedValue.ToInt();
            int Department = DDL_Department.SelectedValue.ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTableChart = Trade_Data.Chart(0, 0, Tools.ConvertToDate(txtFromDate.Value), Tools.ConvertToDate(txtToDate.Value));
                    zTableDetail = Trade_Data.ChartList(0, 0, Tools.ConvertToDate(txtFromDate.Value.Trim()), Tools.ConvertToDate(txtToDate.Value.Trim()));
                    break;

                case 2:
                    zTableChart = Trade_Data.Chart(Department, 0, Tools.ConvertToDate(txtFromDate.Value), Tools.ConvertToDate(txtToDate.Value));
                    zTableDetail = Trade_Data.ChartList(Department, 0, Tools.ConvertToDate(txtFromDate.Value.Trim()), Tools.ConvertToDate(txtToDate.Value.Trim()));
                    break;

                default:
                    zTableChart = Trade_Data.Chart(Department, Employee, Tools.ConvertToDate(txtFromDate.Value), Tools.ConvertToDate(txtToDate.Value));
                    zTableDetail = Trade_Data.ChartList(Department, Employee, Tools.ConvertToDate(txtFromDate.Value.Trim()), Tools.ConvertToDate(txtToDate.Value.Trim()));
                    break;
            }

            GenerateChart(zTableChart);
            GenerateHtml(zTableDetail);
        }
        void SearchData()
        {
            DataTable zTableChart = new DataTable();
            DataTable zTableDetail = new DataTable();
            int Employee = DDL_Employee.SelectedValue.ToInt();
            int Department = DDL_Department.SelectedValue.ToInt();       

            zTableChart = Trade_Data.Chart(Department, Employee, Tools.ConvertToDate(txtFromDate.Value), Tools.ConvertToDate(txtToDate.Value));
            zTableDetail = Trade_Data.ChartList(Department, Employee, Tools.ConvertToDate(txtFromDate.Value.Trim()), Tools.ConvertToDate(txtToDate.Value.Trim()));

            GenerateChart(zTableChart);
            GenerateHtml(zTableDetail);
        }
        void GenerateChart(DataTable Table)
        {
            lblNumber.Text = Table.Compute("Sum(Number)", "").ToString();
            lblFee.Text = Table.Compute("Sum(Money)", "").ToDoubleString();

            StringBuilder zSb = new StringBuilder();
            string zSblbl = "";
            string zSbdataNumber = "";
            string zSbdataInCome = "";

            DataTable zTemp = new DataTable();
            zTemp.Columns.Add("MM");
            zTemp.Columns.Add("Number");
            zTemp.Columns.Add("Money");

            if (Table.Rows.Count > 0)
            {
                #region [Temp]
                for (int i = 1; i <= 12; i++)
                {
                    double Money = 0;
                    int Number = 0;

                    foreach (DataRow r in Table.Rows)
                    {
                        if (r["MM"].ToInt() == i)
                        {
                            Money = Convert.ToDouble(r["Money"]);
                            Number = r["Number"].ToInt();
                            break;
                        }
                    }

                    DataRow rN = zTemp.NewRow();
                    rN["MM"] = i;
                    rN["Number"] = Number;
                    rN["Money"] = Money;
                    zTemp.Rows.Add(rN);
                }

                #endregion
            }
            else
            {
                #region [Temp]
                for (int i = 1; i <= 12; i++)
                {
                    double Money = 0;
                    int Number = 0;

                    DataRow rN = zTemp.NewRow();
                    rN["MM"] = i;
                    rN["Number"] = Number;
                    rN["Money"] = Money;
                    zTemp.Rows.Add(rN);
                }
                #endregion
            }

            for (int i = 0; i < zTemp.Rows.Count; i++)
            {
                DataRow r = zTemp.Rows[i];
                zSblbl += "'Tháng: " + r["MM"].ToString() + "',";
                zSbdataNumber += "'" + r["Number"].ToString() + "',";
                zSbdataInCome += "'" + r["Money"].ToString() + "',";
            }

            zSb.AppendLine(" var areaChartData1 = {");
            zSb.AppendLine("    labels: [" + zSblbl + "],");
            zSb.AppendLine("    datasets: [{");
            zSb.AppendLine("         fillColor: 'rgba(60, 141, 188, 0.5)',");
            zSb.AppendLine("         strokeColor: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointColor: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointStrokeColor: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointHighlightFill: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointHighlightStroke: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         data: [" + zSbdataNumber.Remove(zSbdataNumber.LastIndexOf(",")) + "]");
            zSb.AppendLine("    }]");
            zSb.AppendLine(" };");

            zSb.AppendLine(" var areaChartData2 = {");
            zSb.AppendLine("    labels: [" + zSblbl + "],");
            zSb.AppendLine("    datasets: [{");
            zSb.AppendLine("         fillColor: 'rgba(60, 141, 188, 0.5)',");
            zSb.AppendLine("         strokeColor: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointColor: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointStrokeColor: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointHighlightFill: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         pointHighlightStroke: 'rgba(60, 141, 188, 0.9)',");
            zSb.AppendLine("         data: [" + zSbdataInCome.Remove(zSbdataInCome.LastIndexOf(",")) + "]");
            zSb.AppendLine("    }]");
            zSb.AppendLine(" };");

            LitDataChart.Text = "<script>" + zSb.ToString() + "</script>";
        }
        void GenerateHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table fixed-table' id='tableData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày lập GD</th>");
            zSb.AppendLine("        <th>Ngày ký HĐ</th>");
            zSb.AppendLine("        <th>Dự án</th>");
            zSb.AppendLine("        <th>Mã căn</th>");
            zSb.AppendLine("        <th>Loại căn</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Doanh thu</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {                    
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (no++) + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Income"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");

                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td><td colspan='10'>Chưa có dữ liệu<td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            SearchData();
        }
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY DepartmentName", false);
                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}