﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class DepartmentEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_DepartmentKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_DepartmentKey.Value != string.Empty && HID_DepartmentKey.Value != "0")
            {
                Departments_Info zInfo = new Departments_Info(HID_DepartmentKey.Value.ToInt());
                txt_DepartmentName.Value = zInfo.DepartmentName;
                txt_Description.Value = zInfo.Description;

                Lit_TitlePage.Text = "Sửa thông tin " + zInfo.DepartmentName;
            }
        }

        [WebMethod]
        public static ItemResult SaveDepartment(int DepartmentKey, string DepartmentName, string Description, string Rank)
        {
            ItemResult zResult = new ItemResult();
            Departments_Info zInfo = new Departments_Info(DepartmentKey);
            zInfo.DepartmentName = DepartmentName;
            zInfo.Description = Description;
            zInfo.Rank = Rank.ToInt();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult DeleteDepartment(int DepartmentKey)
        {
            ItemResult zResult = new ItemResult();
            Departments_Info zInfo = new Departments_Info();
            zInfo.Key = DepartmentKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}