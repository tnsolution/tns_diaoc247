﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class DepartmentView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckRoles();

            if (Request["ID"] != null)
                HID_DepartmentKey.Value = Request["ID"];
        
            if (HID_DepartmentKey.Value != string.Empty && 
                HID_DepartmentKey.Value != "0")
            {
                Departments_Info zInfo = new Departments_Info(HID_DepartmentKey.Value.ToInt());
                txt_DepartmentName.Text = zInfo.DepartmentName;
                txt_Description.Text = zInfo.Description;                
            }
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS03";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Chỉnh sửa
                        </button>";
            }
        }
        #endregion
    }
}