﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="EmployeeEdit.aspx.cs" Inherits="WebApp.HRM.EmployeeEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnUser">
                        <i class="ace-icon fa fa-user-md blue"></i>
                        Tạo tài khoản
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin cá nhân
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#company">
                                <i class="blue ace-icon fa fa-user bigger-110"></i>
                                Công ty
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#contact">
                                <i class="blue ace-icon fa fa-phone bigger-110"></i>
                                Liên lạc
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane in active">
                            <div class="col-xs-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Họ tên đệm</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_LastName" placeholder="Nhập text" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tên</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_FirstName" placeholder="Nhập text" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Giới tính</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Gender" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Nam"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Nữ"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ngày sinh</label>

                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_Birthday" placeholder="Chọn ngày tháng năm" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Số CMND/Passport</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_IDCard" placeholder="Nhập text" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ngày cấp</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_IDDate" placeholder="Chọn ngày tháng năm" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nơi cấp</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_IDPlace" placeholder="Nhập text" required />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="company" class="tab-pane">
                            <div class="col-xs-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Phòng ban</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Department" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Quản lý trực tiếp</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Manager" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Chức vụ nhân viên</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Position" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6">
                                            <asp:Literal ID="Lit_PoisitionDescription" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Cấp độ (Level)</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Class" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Cấp sử dụng</label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="DDL_Unit" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" required>
                                                <asp:ListItem Value="-1" Text="--Chọn--" Selected="True" disabled="disabled"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tình trạng</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_IsWorking" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Đã nghĩ việc"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Đang làm việc"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ngày vào làm</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_DateStart" placeholder="Chọn ngày tháng năm" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ngày nghĩ việc</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_DateEnd" placeholder="Chọn ngày tháng năm" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="contact" class="tab-pane">
                            <div class="col-xs-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Address1" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Địa chỉ tạm trú</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Số xe</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_CarNumber" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">ĐT1</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Phone1" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Email1</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Email1" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_EmployeeKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        var EmployeeKey = $("[id$=HID_EmployeeKey]").val();
        $(function () {
            $("#btnUser").click(function () {
                if (EmployeeKey > 0) {
                    window.location = "/SYS/UserEdit.aspx?ID2=" + EmployeeKey;
                } else {
                    Page.showNotiMessageInfo("Thông báo !", "Chưa có thông tin nhân viên.");
                }
            });
            $("#btnDelete").click(function () {
                if (EmployeeKey > 0) {
                    if (confirm("Bạn có chắc xóa thông tin")) {
                        $.ajax({
                            type: "POST",
                            url: "/HRM/EmployeeEdit.aspx/DeleteEmployee",
                            data: JSON.stringify({
                                "EmployeeKey": EmployeeKey,
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#btnDelete").attr("disabled", "disabled");
                                $('.se-pre-con').fadeIn('slow');
                            },
                            success: function (msg) {
                                if (msg.d.Message != "") {
                                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                                }
                                else {
                                    window.location = "EmployeeList.aspx";                                 
                                }
                            },
                            complete: function () {
                                $("#btnDelete").removeAttr("disabled");
                                $('.se-pre-con').fadeOut('slow');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr.status);
                                console.log(xhr.responseText);
                                console.log(thrownError);
                            }
                        });
                    }
                }
                else {
                    Page.showNotiMessageInfo("Thông báo !", "Không có thông tin để xóa.");
                }
            });
            $("#btnSave").click(function () {
                var valid = validate();
                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/HRM/EmployeeEdit.aspx/SaveEmployee",
                        data: JSON.stringify({
                            "EmployeeKey": EmployeeKey,
                            "DepartmentKey": $('[id$=DDL_Department]').val(),
                            "ManagerKey": $('[id$=DDL_Manager]').val(),
                            "PositionKey": $('[id$=DDL_Position]').val(),
                            "Class": $('[id$=txt_Class]').val(),
                            "LastName": $('[id$=txt_LastName]').val(),
                            "FirstName": $('[id$=txt_FirstName]').val(),
                            "Gender": $("[id$=DDL_Gender]").val(),
                            "Birthday": $("[id$=txt_Birthday]").val(),
                            "IDCard": $("[id$=IDCard]").val(),
                            "IDDate": $("[id$=txt_IDDate]").val(),
                            "IDPlace": $("[id$=txt_IDPlace]").val(),
                            "Phone1": $("[id$=txt_Phone1]").val(),
                            "Email1": $("[id$=txt_Email1]").val(),
                            "Address1": $("[id$=txt_Address1]").val(),
                            "Address2": $("[id$=txt_Address2]").val(),
                            "DateStart": $("[id$=txt_DateStart]").val(),
                            "DateEnd": $("[id$=txt_DateEnd]").val(),
                            "CarNumber": $("[id$=txt_CarNumber]").val(),
                            "IsWorking": $('[id$=DDL_IsWorking]').val(),
                            "UnitLevel": $('[id$=DDL_Unit]').val(),
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#btnSave").attr("disabled", "disabled");
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d.Message != "") {
                                Page.showPopupMessage("Lỗi !", msg.d.Message);
                            }
                            else {
                                if (EmployeeKey == "0")
                                    Page.showNotiMessageInfo("Thông báo !", "Tạo mới Employee thành công");
                                else
                                    Page.showNotiMessageInfo("Thông báo !", "Đã Sửa thông tin thông tin thành công.");

                                EmployeeKey = msg.d.Result;
                            }
                        },
                        complete: function () {
                            $("#btnSave").removeAttr("disabled");
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
                else {
                    Page.showNotiMessageError("Bạn phải cập nhật thông tin còn thiếu !", "Kiểm tra lại thông tin cá nhân");
                }
            });
            $(".select2").select2({ width: "100%" });
        });

        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
