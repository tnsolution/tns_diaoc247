﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class EmployeeEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Position, "SELECT PositionKey, Position FROM HRM_Positions ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Unit, "SELECT Rank, Unit + ' | ' + Description FROM SYS_Unit ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);
                Tools.DropDown_DDL(DDL_Manager, "SELECT EmployeeKey, LastName  + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND PositionKey <=4", false);

                if (Request["ID"] != null)
                    HID_EmployeeKey.Value = Request["ID"];
                CheckRoles();
                Load_Employee();
            }
        }

        void Load_Employee()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_EmployeeKey.Value != string.Empty &&
                HID_EmployeeKey.Value != "0")
            {
                Employees_Info zInfo = new Employees_Info(HID_EmployeeKey.Value.ToInt());
                DDL_Position.SelectedValue = zInfo.PositionKey.ToString();
                DDL_Department.SelectedValue = zInfo.DepartmentKey.ToString();
                DDL_Manager.SelectedValue = zInfo.ManagerKey.ToString();
                DDL_IsWorking.SelectedValue = zInfo.IsWorking.ToString();
                DDL_Gender.SelectedValue = zInfo.Gender.ToString();
                DDL_Unit.SelectedValue = zInfo.UnitLevel;
                txt_Class.Value = zInfo.Class;
                txt_FirstName.Value = zInfo.FirstName;
                txt_LastName.Value = zInfo.LastName;
                txt_Birthday.Value = zInfo.Birthday.ToString("dd/MM/yyyy");
                txt_IDCard.Value = zInfo.IDCard;
                txt_IDDate.Value = zInfo.IDDate.ToString("dd/MM/yyyy");
                txt_IDPlace.Value = zInfo.IDPlace;              
                txt_DateEnd.Value = zInfo.DateEnd.ToString("dd/MM/yyyy");
                txt_DateStart.Value = zInfo.DateStart.ToString("dd/MM/yyyy");
                txt_Address1.Value = zInfo.Address1;
                txt_Address2.Value = zInfo.Address2;
                txt_Email1.Value = zInfo.Email1;
                txt_CarNumber.Value = zInfo.CarNumber;
                txt_Phone1.Value = zInfo.Phone1;

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();

                Lit_TitlePage.Text = "Sửa thông tin nhân viên " + zInfo.LastName + " " + zInfo.FirstName;
            }
        }

        [WebMethod]
        public static string CreateUser(int EmployeeKey, string UserName)
        {
            //string zUserName = Tools.RemoveVietnamese(UserName);
            //zUserName += Tools.Random();

            //SessionUser zUser = User_Data.CheckUser(UserName, "123456");
            //if (zUser.EmployeeKey > 0)
            //{
            //    return "Nhân viên này đã có tài khoản rồi !";
            //}

            //User_Info zInfo = new User_Info();
            //zInfo.EmployeeKey = EmployeeKey;
            //zInfo.Name = UserName;
            //zInfo.Password = "123456";
            //zInfo.Activated = 1;
            //zInfo.ExpireDate = DateTime.Now.AddYears(1);
            //zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            //zInfo.CreatedDate = DateTime.Now;

            //zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            //zInfo.ModifiedDate = DateTime.Now;

            //zInfo.Create();

            //if (zInfo.Message != string.Empty)
            //{
            //    return "Lỗi tạo user, xin liên hệ Admin !";
            //}

            return "OK";
        }

        [WebMethod]
        public static ItemResult DeleteEmployee(int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            Employees_Info zInfo = new Employees_Info();
            zInfo.Key = EmployeeKey;
            zInfo.Delete();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveEmployee(
            int EmployeeKey, int DepartmentKey, int ManagerKey, int PositionKey,
            string Class, string FirstName, string LastName, int Gender,
            string Birthday, string IDCard, string IDDate, string IDPlace,
            string Phone1, string Email1, string Address1, string Address2,
            string DateStart, string DateEnd, string CarNumber, int IsWorking, string UnitLevel)
        {
            ItemResult zResult = new ItemResult();
            Employees_Info zInfo = new Employees_Info(EmployeeKey);
            zInfo.UnitLevel = PositionKey.ToString();
            zInfo.PositionKey = PositionKey;
            zInfo.DepartmentKey = DepartmentKey;
            zInfo.ManagerKey = ManagerKey;
            zInfo.IsWorking = IsWorking;
            zInfo.Gender = Gender;
            zInfo.FirstName = FirstName;
            zInfo.LastName = LastName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.IDCard = IDCard;
            zInfo.IDPlace = IDPlace;
            zInfo.IDDate = Tools.ConvertToDate(IDDate);
            zInfo.Class = Class;
            zInfo.Phone1 = Phone1;
            zInfo.Email1 = Email1;
            zInfo.DateStart = Tools.ConvertToDate(DateStart);
            zInfo.DateEnd = Tools.ConvertToDate(DateEnd);
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.CarNumber = CarNumber;
            zInfo.UnitLevel = UnitLevel;

            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedDate = DateTime.Now;

            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedDate = DateTime.Now;

            zInfo.Save();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            if (_Permitsion[2].ToInt() == 0)
            {
                txt_Email1.Visible = false;
                txt_Phone1.Visible = false;
            }
        }
        #endregion
    }
}