﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfomationList.aspx.cs" Inherits="WebApp.HRM.InfomationList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Thông tin ...
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#faq-tab-1" class="center">
                                <i class="blue ace-icon fa fa-question-circle bigger-120"></i>
                                Giới thiệu
                            </a>
                        </li>
                        <li style="display: none">
                            <a data-toggle="tab" href="#faq-tab-2" class="center">
                                <i class="green ace-icon fa fa-folder bigger-120"></i>
                                Hợp đồng mẫu
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#rule" class="center">
                                <i class="orange ace-icon fa fa-archive bigger-120"></i>
                                Nội quy
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" class="center" href="#policy">
                                <i class="purple ace-icon fa fa-file bigger-120"></i>
                                Chính sách
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="faq-tab-1" class="tab-pane in active">
                            <div id="faq-list-1" class="panel-group accordion-style1 accordion-style2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a href="#faq-1-1" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle" aria-expanded="true">
                                            <i class="pull-right ace-icon fa fa-chevron-down" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>
                                            <i class="ace-icon fa fa-info bigger-130"></i>
                                            &nbsp; Giới thiệu về ĐỊA ỐC 247
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse in" id="faq-1-1" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <dl>
                                                <dd>Được thành lập vào ngày 11 tháng 11 năm 2014, với phương châm hướng tới sự chuyên nghiệp và minh bạch trong thông tin chuyên ngành của từng Dự án Bất Động Sản, nhằm góp phần phát triển thị trường bất động sản tại Thành Phố Hồ Chí Minh nói riêng và tại Việt Nam nói chung</dd>
                                                <dd>Công Ty Cổ Phần Đầu Tư <b>ĐỊA ỐC 247</b> hoạt động chuyên sâu trong lĩnh vực <b>Vận hành – Khai thác cho thuê & Quản Lý: “Căn hộ – Căn hộ dịch vụ”</b> tại Thành phố Hồ Chí Minh. Bên cạnh đó dịch vụ Chuyển nhượng & Phân phối dự án cho các Chủ đầu tư luôn là thế mạnh của Công Ty</dd>
                                                <center style="margin: 10px"><img src="/upload/jpg.jpg" alt="" /></center>
                                                <dd>Lấy Khách Hàng làm trọng tâm qua đó những dự án Công Ty lựa chọn phân phối luôn đảm bảo tính <b>An Toàn</b> về Pháp Lý; mức độ <b>Uy Tín</b> của <b>Chủ Đầu Tư và Giá Trị Gia Tăng</b> cho từng sản phẩm sao cho phù hợp và đáp ứng đúng nhu cầu đa dạng của từng Khách Hàng</dd>
                                                <dd><b>“Chính Trực – Kỷ Luật – Phát Triển”</b> là 3 yếu tố nhân sự cần có mà Công Ty luôn đặt quan tâm hàng đầu cho việc lựa chọn và đào tạo nhân sự có chất lượng, có cùng tâm huyết và mục tiêu chung</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a href="#faq-1-2" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed" aria-expanded="true">
                                            <i class="pull-right ace-icon fa fa-chevron-down" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>

                                            <i class="ace-icon fa fa-info bigger-130"></i>
                                            &nbsp; Tổ chức, nhân sự
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse" id="faq-1-2" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <div id="faq-list-nested-1" class="panel-group accordion-style1 accordion-style2">
                                                <asp:Literal ID="Lit_Staff" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default" style="display: none">
                                    <div class="panel-heading">
                                        <a href="#faq-1-3" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed" aria-expanded="true">
                                            <i class="pull-right ace-icon fa fa-chevron-down" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>
                                            <i class="ace-icon fa fa-info bigger-130"></i>
                                            &nbsp; Tầm nhìn sứ mệnh
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse" id="faq-1-3" aria-expanded="true" style="">
                                        <div class="panel-body">
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default" style="display: none">
                                    <div class="panel-heading">
                                        <a href="#faq-1-4" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed" aria-expanded="true">
                                            <i class="pull-right ace-icon fa fa-chevron-down" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>
                                            <i class="ace-icon fa fa-info bigger-130"></i>
                                            &nbsp; Dịch vụ
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse" id="faq-1-4" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <dl>
                                                <dt>“Dịch Vụ Là Phục Vụ Bằng Con Tim Và Khối Óc”</dt>
                                                <dd>+ Phục vụ bằng Con Tim giúp chúng tôi luôn chân thành và nhiệt tình trong tư vấn, luôn biết lắng nghe để hiểu điều Khách hàng mong muốn.</dd>
                                                <dd>+ Phục vụ với Khối Óc giúp Chúng tôi hiểu sâu và rõ sản phẩm để từ đó có sự nhận định đánh giá chính xác nhất với nhu cầu thực tế để phục phụ điều Khách hàng muốn.</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="policy" class="tab-pane">
                            <div>
                                <iframe src="http://docs.google.com/gview?url=http://crm.diaoc247.vn/Upload/Company/Rule/007%20Ch%C3%ADnh%20s%C3%A1ch%20nh%C3%A2n%20vi%C3%AAn%20ch%C3%ADnh%20th%E1%BB%A9c%2001.06.2017.pdf&embedded=true" style="width: 100%; height: 450px;" frameborder="0"></iframe>
                            </div>
                        </div>
                        <div id="rule" class="tab-pane">
                            <div>
                                <iframe src="http://docs.google.com/gview?url=http://crm.diaoc247.vn/Upload/Company/Rule/001%20N%E1%BB%99i%20quy%20l%C3%A0m%20vi%E1%BB%87c%202017.pdf&embedded=true" style="width: 100%; height: 450px;" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
