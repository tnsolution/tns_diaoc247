﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class InfomationList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadStaff();
        }

        void LoadStaff()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zDepartment = Departments_Data.List();
            int no = 1;
            foreach (DataRow r in zDepartment.Rows)
            {
                DataTable zTable = Employees_Data.Staff(r["DepartmentKey"].ToInt());

                zSb.AppendLine("<div class='panel panel-default'>");
                zSb.AppendLine("    <div class='panel-heading'>");
                zSb.AppendLine("        <a href='#depart" + no + "' data-parent='#faq-list-nested-1' data-toggle='collapse' class='accordion-toggle' aria-expanded='true'><i class='smaller-80 middle ace-icon fa fa-minus' data-icon-hide='ace-icon fa fa-minus' data-icon-show='ace-icon fa fa-plus'></i>" + r["DepartmentName"].ToString() + "</a>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("    <div class='panel-collapse collapse in' id='#depart" + no + "' aria-expanded='true'>");
                zSb.AppendLine("        <div class='panel-body'>");

                zSb.AppendLine("<table class='table table-hover' id='tblData'>");
                zSb.AppendLine("        <tbody>");

                foreach (DataRow rchild in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + rchild["EmployeeKey"].ToString() + "'>");
                    zSb.AppendLine("            <td style='width:10%'><img src='" + rchild["ImageThumb"].ToThumb() + "' style='width:50px;' /></td>");
                    zSb.AppendLine("            <td style='width:25%'>" + rchild["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            <td style='width:25%'>" + rchild["Position"].ToString() + "</td>");
                    zSb.AppendLine("            <td style='width:10%'>" + rchild["Phone1"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + rchild["Email1"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");

                zSb.AppendLine("        </div>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine(" </div>");

                no++;
            }
            Lit_Staff.Text = zSb.ToString();
        }
        string createEmailBody(string title, string message)
        {
            string body = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(Server.MapPath("~/Email/AlertEmail.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{TieuDe}", title); //replacing the required things  
            body = body.Replace("{NoiDung}", message);

            return body;
        }
    }
}