﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApp
{
    public static class Helper
    {
        public static string CreateEmailBody(string title, string message)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Email/AlertEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Title}", title);
            body = body.Replace("{Message}", message);
            return body;
        }
        /// <summary>
        /// Lấy tên bảng 
        /// </summary>
        /// <param name="TableName">PUL_Resale_Apartment, PUL_Resale_Ground,PUL_Resale_House</param>
        /// <returns>ResaleApartment, ResaleGround, ResaleHouse</returns>
        public static string ShortTable(string TableName)
        {
            switch (TableName)
            {
                case "PUL_Resale_Apartment":
                    return "ResaleApartment";

                case "PUL_Resale_Ground":
                    return "ResaleGround";

                case "PUL_Resale_House":
                    return "ResaleHouse";

                default:
                    return "ResaleApartment";
            }
        }
    }
}