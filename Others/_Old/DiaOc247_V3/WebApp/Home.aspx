﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WebApp.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/Home.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Trang chủ /tháng
                <asp:Literal ID="LitTitleMonth" runat="server"></asp:Literal>
                <span class="col-sm-offset-2" style="color: gray">CHÍNH TRỰC - KỶ LUẬT - PHÁT TRIỂN</span>
                <span class="pull-right action-buttons">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-2" style="padding: 0px">
                <div class="widget-box">
                    <div class="widget-header">
                        <h5 class="widget-title lighter smaller">
                            <i class="ace-icon fa fa-comments-o bigger-125 green"></i>
                            Thông báo
                        </h5>
                        <div class="widget-toolbar">
                            <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main no-padding">
                            <div class="comments">
                                <asp:Literal ID="Lit_Annou" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 widget-container-col ">
                        <div class="widget-box widget-color-blue ">
                            <div class="widget-header ">
                                <h5 class="widget-title">
                                    <i class="ace-icon fa fa-sort"></i>
                                    <a href="/FNC/TradeList.aspx?Type=1" style="color: white">Giao dịch chờ duyệt
                                    </a>
                                </h5>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="reload">
                                        <i class="ace-icon fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body" style="">
                                <div class="widget-main">
                                    <a href="/FNC/TradeList.aspx?Type=1">
                                        <div class="alert alert-info">
                                            <div class="infobox-icon">
                                                <i class="fa fa-shopping-cart fa-5x icon-animated-vertical pull-right"></i>
                                            </div>
                                            <div class="infobox-data">
                                                <h4>
                                                    <asp:Literal ID="Lit_TradeNeedApprove" runat="server"></asp:Literal></h4>
                                                <div class="infobox-content">Xem chi tiết</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 widget-container-col ">
                        <div class="widget-box widget-color-blue ">
                            <div class="widget-header ">
                                <h5 class="widget-title">
                                    <i class="ace-icon fa fa-sort"></i>
                                    <a href="/FNC/TradeList.aspx?Type=2" style="color: white">Giao dịch cần thanh toán
                                    </a>
                                </h5>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="reload">
                                        <i class="ace-icon fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body" style="">
                                <div class="widget-main">
                                    <a href="/FNC/TradeList.aspx?Type=2">
                                        <div class="alert alert-danger">

                                            <div class="infobox-icon">
                                                <i class="fa fa-hand-o-left fa-5x icon-animated-wrench pull-right"></i>
                                            </div>
                                            <div class="infobox-data">
                                                <h4>
                                                    <asp:Literal ID="Lit_TradeNeedCollect" runat="server"></asp:Literal></h4>
                                                <div class="infobox-content">Xem chi tiết</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 widget-container-col ">
                        <div class="widget-box widget-color-blue ">
                            <div class="widget-header ">
                                <h5 class="widget-title">
                                    <i class="ace-icon fa fa-sort"></i>
                                    <a href="/FNC/ViewChart.aspx" style="color: white">Doanh thu tháng
                                                <asp:Literal ID="LitMonth" runat="server"></asp:Literal>
                                    </a>
                                </h5>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="reload">
                                        <i class="ace-icon fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body" style="">
                                <div class="widget-main">
                                    <a href="/FNC/ViewChart.aspx">
                                        <div class="alert alert-success">
                                            <div class="infobox-icon">
                                                <i class="fa fa fa-usd fa-5x icon-animated-vertical pull-right"></i>
                                            </div>
                                            <div class="infobox-data">
                                                <h4>
                                                    <asp:Literal ID="Lit_Income" runat="server"></asp:Literal></h4>
                                                <div class="infobox-content">
                                                    Xem chi tiết
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 widget-container-col ">
                        <div class="widget-box widget-color-blue ">
                            <div class="widget-header ">
                                <h5 class="widget-title">
                                    <i class="ace-icon fa fa-sort"></i>
                                    KPI
                                </h5>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="reload">
                                        <i class="ace-icon fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body" style="">
                                <div class="widget-main">
                                    <div class="alert alert-info">
                                        <div class="infobox-icon">
                                            <i class="fa fa fa-puzzle-piece fa-5x icon-animated-vertical pull-right"></i>
                                        </div>
                                        <div class="infobox-data">
                                            <h4>70%</h4>
                                            <div class="infobox-content">Xem chi tiết</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 widget-container-col ">
                        <div class="widget-box widget-color-orange lighter">
                            <div class="widget-header ">
                                <h5 class="widget-title"><i class="fa fa-bullhorn"></i>&nbsp Bảng xếp hạng doanh số tháng
                                    <asp:Literal ID="LitMonthTopStaff" runat="server"></asp:Literal></h5>
                            </div>
                            <div class="widget-body ace-scroll" style="" id="BotLeft">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:Literal ID="Lit_Top3" runat="server"></asp:Literal>
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:Literal ID="Lit_Other" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 widget-container-col ">
                        <div class="widget-box widget-color-orange lighter">
                            <div class="widget-header ">
                                <h5 class="widget-title"><span class="col-xs-4">Mục tiêu<asp:Literal ID="LitMonthRequire" runat="server"></asp:Literal></span><span class="col-xs-8"><asp:Literal ID="LitTitleDay" runat="server"></asp:Literal></span></h5>
                            </div>
                            <div class="widget-body ace-scroll" style="" id="BotRight">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="col-sm-12 Target">
                                                <div class="TargetText">
                                                    <asp:Literal ID="Lit_Require" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" style="border-top: 1px solid #00add8;">
                                                <h6>
                                                    <asp:Literal ID="Lit_Result" runat="server"></asp:Literal>
                                                </h6>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:Literal ID="Lit_Schedule" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--modal-->
    <div class="modal fade" id="NewMessage" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">CHI TIẾT THÔNG BÁO</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:ListBox ID="DDL_Department" runat="server" SelectionMode="Multiple" CssClass="select2" data-placeholder="--Chọn--"></asp:ListBox>
                    </div>
                    <div class="form-group">
                        <textarea id="txt_Contents" rows="5" cols="20" class="form-control" runat="server"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSave_Message" runat="server" Text="Cập nhật" class="btn btn-warning pull-right" OnClick="btnSave_Message_Click" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:HiddenField ID="HID_Meesage" runat="server" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/Home.js"></script>
</asp:Content>
