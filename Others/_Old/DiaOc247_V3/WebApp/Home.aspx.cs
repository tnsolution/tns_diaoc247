﻿using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();

                LitTitleDay.Text = "Kế hoạch ngày " + DateTime.Now.ToString("dd");
                LitMonthTopStaff.Text = " " + FromDate.Month + "/" + FromDate.Year;
                LitTitleMonth.Text = ToDate.Month.ToString();
                LitMonth.Text = ToDate.Month.ToString();
                LitMonthRequire.Text = " tháng " + ToDate.Month.ToString();

                int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                LoadData(FromDate, ToDate, UnitLevel, Department, Employee);
                LoadMessage(UnitLevel, Department, Employee);
                LoadSchedule(Employee);
                LoadTarget(FromDate, ToDate, UnitLevel, Department, Employee);
                LoadStaff(FromDate, ToDate);
            }
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Convert.ToDateTime(HID_FromDate.Value);
                ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }
            if (ViewTime == -1)
            {
                FromDate = Convert.ToDateTime(HID_FromDate.Value);
                ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            LitTitleMonth.Text = ToDate.Month.ToString();
            LitMonth.Text = ToDate.Month.ToString();
            LoadTarget(FromDate, ToDate, UnitLevel, Department, Employee);
            LoadData(FromDate, ToDate, UnitLevel, Department, Employee);
            LoadStaff(FromDate, ToDate);
        }
        protected void btnSave_Message_Click(object sender, EventArgs e)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Key = HID_Meesage.Value.ToInt();

            foreach (ListItem i in DDL_Department.Items)
            {
                if (i.Selected)
                {
                    Announce_Info zInfo = new Announce_Info(Key);
                    zInfo.EmployeeKey = Employee;
                    zInfo.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                    zInfo.CreatedDate = DateTime.Now;
                    zInfo.Contents = txt_Contents.Value;
                    zInfo.DepartmentKey = i.Value.ToInt();
                    zInfo.Save();
                }
            }


            LoadMessage(UnitLevel, Department, Employee);
        }
        void LoadTarget(DateTime FromDate, DateTime ToDate, int UnitLevel, int Department, int Employee)
        {
            StringBuilder zSbTarget = new StringBuilder();
            StringBuilder zSbResult = new StringBuilder();

            LitMonthRequire.Text = " tháng " + ToDate.Month.ToString();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    int Result = 0;
                    int Doing = HomePage.Count_Success_Trade(FromDate, ToDate, 0, 0);
                    int Require = HomePage.Count_Require_Trade(FromDate, ToDate, 0, 0);
                    if (Require != 0)
                    {
                        zSbTarget.AppendLine("<div class='TargetText'><span>" + Require + "</span></div>");
                        if (Doing == 0)
                            zSbResult.Append("Bạn chưa có giao dịch nào. Cố lên...Cố lên!!!");
                        Result = Doing - Require;
                        if (Doing > 0 && Result < 0 && Result != 0)
                            zSbResult.Append("Cố lên!!! Bạn cần thêm " + -Result + " giao dịch nữa.");
                        else if (Result == 0)
                            zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                        else if (Result > 0)
                            zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                    }
                    break;
                case 2:
                    Result = 0;
                    Doing = HomePage.Count_Success_Trade(FromDate, ToDate, Department, 0);
                    Require = HomePage.Count_Require_Trade(FromDate, ToDate, Department, 0);
                    if (Require != 0)
                    {
                        zSbTarget.AppendLine("<div class='TargetText'><span>" + Require + "</span></div>");
                        if (Doing == 0)
                            zSbResult.Append("Bạn chưa có giao dịch nào. Cố lên...Cố lên!!!");
                        Result = Doing - Require;
                        if (Doing > 0 && Result < 0 && Result != 0)
                            zSbResult.Append("Cố lên!!! Bạn cần thêm " + -Result + " giao dịch nữa.");
                        else if (Result == 0)
                            zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");

                        else if (Result > 0)
                            zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                    }
                    break;

                default:
                    Result = 0;
                    Doing = HomePage.Count_Success_Trade(FromDate, ToDate, Department, Employee);
                    Require = HomePage.Count_Require_Trade(FromDate, ToDate, Department, Employee);
                    if (Require != 0)
                        zSbTarget.AppendLine("<div class='TargetText'><span>" + Require + "</span></div>");
                    if (Doing == 0)
                        zSbResult.Append("Bạn chưa có giao dịch nào. Cố lên...Cố lên!!!");
                    Result = Doing - Require;
                    if (Doing > 0 && Result < 0 && Result != 0)
                        zSbResult.Append("Cố lên!!! Bạn cần thêm " + -Result + " giao dịch nữa.");
                    else if (Result == 0)
                        zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                    else if (Result > 0)
                        zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                    break;
            }

            Lit_Require.Text = zSbTarget.ToString();
            Lit_Result.Text = zSbResult.ToString();
        }
        void LoadStaff(DateTime FromDate, DateTime ToDate)
        {
            LitMonthTopStaff.Text = " " + FromDate.Month + "/" + FromDate.Year;

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HomePage.Get_ListStaff(FromDate, ToDate);
            if (zTable.Rows.Count > 0)
            {
                DataRow[] temp = zTable.Select("RowNumber <= 3 AND INCOME >0");
                DataTable zTop3 = new DataTable();

                if (temp.Length > 0)
                {
                    zTop3 = zTable.Select("RowNumber <= 3 AND INCOME >0").CopyToDataTable();
                }

                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th colspan=2>Top 3</th>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("    <tbody>");

                string top3 = "";

                if (zTop3.Rows.Count > 0)
                {
                    for (int i = 0; i < zTop3.Rows.Count; i++)
                    {
                        DataRow r = zTop3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td><img src='Upload\\Image\\" + r["RowNumber"].ToString() + ".png' style='width:52px;' /></td>");
                        zSb.AppendLine("    <td><b>" + r["Name_Employee"].ToString() + "</br>");
                        zSb.AppendLine("    Doanh số: " + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</b></td>");
                        zSb.AppendLine("</tr>");

                        top3 += r["RowNumber"].ToString() + ",";
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\1.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\2.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\3.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                }
                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                Lit_Top3.Text = zSb.ToString();

                //================== còn lại 
                DataTable zOver3 = new DataTable();
                if (zTop3.Rows.Count > 0)
                    zOver3 = zTable.Select("RowNumber NOT IN (" + top3.Remove(top3.LastIndexOf(",")) + ")").CopyToDataTable();
                else
                    zOver3 = zTable;

                zSb = new StringBuilder();
                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th>Nhân viên</th>");
                zSb.AppendLine("        <th>Doanh số</th>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("    <tbody>");

                if (zOver3.Rows.Count > 0)
                {
                    for (int i = 0; i < zOver3.Rows.Count; i++)
                    {
                        DataRow r = zOver3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td>" + r["RowNumber"].ToString() + " : " + r["Name_Employee"].ToString() + "</td>");
                        zSb.AppendLine("    <td style='text-align:right'>" + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</td>");
                        zSb.AppendLine("</tr>");
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td colspan=2>Chưa có dữ liệu</td>");
                    zSb.AppendLine("</tr>");
                }

                zSb.AppendLine("    </tbody>");

                zSb.AppendLine("</table>");
                Lit_Other.Text = zSb.ToString();
            }
        }
        void LoadData(DateTime FromDate, DateTime ToDate, int UnitLevel, int Department, int Employee)
        {
            int TradeNeedApprove = 0;
            int TradeNeedCollect = 0;
            double InCome = 0;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    TradeNeedApprove = HomePage.Count_Trade_NeedApproved(0, 0, FromDate, ToDate);
                    TradeNeedCollect = HomePage.Count_Trade_NeedCollect(0, 0, FromDate, ToDate);
                    InCome = HomePage.Sum_Trade(0, 0, FromDate, ToDate);
                    break;

                case 2:
                    TradeNeedApprove = HomePage.Count_Trade_NeedApproved(Department, 0, FromDate, ToDate);
                    TradeNeedCollect = HomePage.Count_Trade_NeedCollect(Department, 0, FromDate, ToDate);
                    InCome = HomePage.Sum_Trade(Department, 0, FromDate, ToDate);
                    break;

                default:
                    TradeNeedApprove = HomePage.Count_Trade_NeedApproved(Department, Employee, FromDate, ToDate);
                    TradeNeedCollect = HomePage.Count_Trade_NeedCollect(Department, Employee, FromDate, ToDate);
                    InCome = HomePage.Sum_Trade(Department, Employee, FromDate, ToDate);
                    break;
            }

            Lit_Income.Text = InCome.ToDoubleString();
            Lit_TradeNeedApprove.Text = TradeNeedApprove.ToString();
            Lit_TradeNeedCollect.Text = TradeNeedCollect.ToString();
        }
        void LoadMessage(int UnitLevel, int Department, int Employee)
        {
            DataTable zTable = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = HomePage.Get_Annou(0);
                    break;
                default:
                    zTable = HomePage.Get_Annou(Department);
                    break;
            }


            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("    <div class='itemdiv commentdiv' key='" + r["ID"].ToString() + "'>");
                //zSb.AppendLine("    <div class='user'><img alt='User Name' src='" + HttpContext.Current.Request.Cookies["UserLog"]["ImgThumb"].ToUrlDecode() + "'></div>");
                zSb.AppendLine("    <div class='body'>");
                zSb.AppendLine("    <div class='time'>" + Convert.ToDateTime(r["CreatedDate"]).ToString("dd/MM hh:mm") + "</div>");
                zSb.AppendLine("    <div class='name'>" + r["CreatedBy"].ToString() + "   </div>");
                zSb.AppendLine("    <div class='text'>" + r["Contents"].ToString() + "</div>");

                if (UnitLevel <= 2 || r["EmployeeKey"].ToInt() == HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt())
                {
                    zSb.AppendLine("<div class='tools'><div class='action-buttons bigger-125'><a href='#' btn='btnEditMessage' key='" + r["ID"].ToString() + "'><i class='ace-icon fa fa-pencil blue'></i></a><a href='#' btn= 'btnDeleteMessage' key='" + r["ID"].ToString() + "'><i class='ace-icon fa fa-trash-o red'></i></a></div></div>");
                }

                zSb.AppendLine("</div>");
                zSb.AppendLine("</div>");
            }
            Lit_Annou.Text = zSb.ToString();
        }
        void LoadSchedule(int Employee)
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HomePage.Get_Schedule(Employee);

            zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-border' id='tableTime'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Thời gian</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    i++;
                    zSb.AppendLine("<tr><td>" + i + "</td><td>" + r["Title"].ToString() + "</td><td>" + r["Description"].ToString() + "</td></tr>");
                }
            }
            else
            {
                zSb.AppendLine("<tr><td colspan=3>Chưa có kế hoạch</td></tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            Lit_Schedule.Text = zSb.ToString();
        }     
        [WebMethod]
        public static string DeleteMessage(int id)
        {
            Announce_Info zInfo = new Announce_Info();
            zInfo.ID = id;
            zInfo.Delete();
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không xóa được";
        }
        void CheckRole()
        {
            if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                Response.Redirect("/Login.aspx");

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.ListBox(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName");
                    Lit_Button.Text = "<a href='#NewMessage' data-toggle='modal'><i class='ace-icon fa fa-plus'></i></a>";
                    break;

                default:
                    Tools.ListBox(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey + " ORDER BY DepartmentName");
                    Lit_Button.Text = "";
                    break;
            }
        }
    }
}