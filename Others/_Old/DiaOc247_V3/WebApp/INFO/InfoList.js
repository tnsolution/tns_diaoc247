﻿var _status = 0;    //tinh trang nguon tin
var _Usdban = 0, _Usdthue = 0;       //tinh gia usd
var _assetkey = 0; //ma san pham
var _relateKey = 0;
$(function () {
    $('[moneyonly]').number(true, 0);
    $(".select2").select2({ width: "100%" });
    $("[id$=txt_PriceRentUSD]").hide();
    $("[id$=txt_PriceUSD]").hide();
    //-------------------------------------Lay out   
    $("[id$=Usdswitch1]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PriceUSD]").show();
            $("[id$=txt_PriceVND]").hide();
            $("#giaban").text("Giá bán USD");
            _Usdban = 1;
        }
        else {
            $("[id$=txt_PriceUSD]").hide();
            $("[id$=txt_PriceVND]").show();
            $("#giaban").text("Giá bán VNĐ");
            _Usdban = 0;
        }
    });
    $("[id$=Usdswitch2]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PriceRentUSD]").show();
            $("[id$=txt_PriceRentVND]").hide();
            $("#giathue").text("Giá thuê USD");
            _Usdthue = 1;
        }
        else {
            $("[id$=txt_PriceRentUSD]").hide();
            $("[id$=txt_PriceRentVND]").show();
            $("#giathue").text("Giá thuê VNĐ");
            _Usdthue = 0;
        }
    });

    //$("[id$=txt_PriceUSD]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_PriceRentUSD]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_PriceVND]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_PriceRentVND]").blur(function () { ConvertMoney(); });

    //-------------------------------------Event
    $("a[btn='btndel']").click(function () {
        var taskid = $(this).closest('tr').attr('id');
        var tasktype = $(this).closest('tr').attr('type');
        if (confirm('Bạn có chắc xóa thông tin')) {
            $.ajax({
                type: "POST",
                url: "/INFO/InfoList.aspx/DeleteTask",
                data: JSON.stringify({
                    "ID": taskid,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () { },
                success: function (msg) { },
                complete: function () {
                    $("#tblData tbody tr#" + taskid).remove();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    $("a[data-target]").click(function () {
        var table = $("#tblData tbody").length;
        if (table < 0) {
            Page.showNotiMessageInfo("Thông báo", "Không có thông tin để xử lý gửi data")
        }
    });
    $("[id$=btnSend]").click(function () {
        var valid = validate();
        if (!valid)
            return false;
    });
    $("#btnProcess").click(function () {
        runAction();
    });
    $("#tblData tbody tr td:not(:last-child)").on("click", function () {

        var relateKey = $(this).closest('tr').attr('relatekey');
        var taskid = $(this).closest('tr').attr('id');
        var tasktype = $(this).closest('tr').attr('type');
        var assetid = $(this).closest('tr').attr('assetkey');
        var assettype = $(this).closest('tr').attr('assettype');

        $("[id$=HID_RelateKey]").val(relateKey);
        $("[id$=HID_AssetKey]").val(assetid);
        $("[id$=HID_AssetType]").val(assettype);
        $("[id$=HID_InfoKey]").val(taskid);
        $("[id$=HID_InfoType]").val(tasktype);

        $("#kygui").hide();
        $("#khachhang").hide();
        $("#chunha").hide();

        if (assetid != null &&
            assettype != null) {
            $(".se-pre-con").fadeIn('slow');
            //mở link đã xử lý        
            window.location = "/SAL/ProductView.aspx?ID=" + assetid + "&Type=" + assettype;
        }
        else {
            //mở xử lý thong tin
            getTask(taskid, tasktype);
        }
    });

    //-------------------------------------Action
});
function action(id, txt) {
    _status = id;
    $("#kygui").hide();
    $("#khachhang").hide();
    $("#chunha").hide();
    $("#msgAction").text("Bạn chọn xử lý: " + txt);

    //ky gui
    if (id == 1) {
        $("#kygui").show();
        $("#msgAction").text("Bạn chọn xử lý: " + txt + " vui lòng kiểm tra các thông tin liên quan !.");
    }
    //quan tam
    if (id == 2) {
        $("#khachhang").show();
        $("#msgAction").text("Bạn chọn xử lý: " + txt + " vui lòng kiểm tra các thông tin liên quan !.");
    }
    //chu nha
    if (id == 9) {
        $("#chunha").show();
        $("#msgAction").text("Bạn chọn xử lý: " + txt + " vui lòng kiểm tra các thông tin liên quan !.");
    }
}
function runAction() {
    console.log(_status);
    console.log($("[id$=HID_AssetKey]").val());
    switch (_status) {
        case 1:
            saveProduct();
            break;

        case 2:
            break;

        case 9:
            saveOwner();
            break;

        default:
            saveTask();
            break;
    }
}
function ConvertMoney() {
    if (_usd == 1) {
        var priceUsd = $("[id$=txt_PriceUSD]").val();
        var priceRentUsd = $("[id$=txt_PriceRentUSD]").val();
        var priceVnd = Page.RemoveComma(priceUsd) * usdRate;
        var priceRentVnd = Page.RemoveComma(priceRentUsd) * usdRate;

        $("[id$=txt_PriceRentVND]").val(Page.FormatMoney(priceRentVnd));
        $("[id$=txt_PriceVND]").val(Page.FormatMoney(priceVnd));
    }
    else {
        var priceVnd = $("[id$=txt_PriceVND]").val();
        var priceRentVnd = $("[id$=txt_PriceRentVND]").val();
        var priceUsd = Page.RemoveComma(priceVnd) / usdRate;
        var priceRentUsd = Page.RemoveComma(priceRentVnd) / usdRate;

        $("[id$=txt_PriceRentUSD]").val(Page.FormatMoney(priceRentUsd));
        $("[id$=txt_PriceUSD]").val(Page.FormatMoney(priceUsd));
    }
}
function validate() {
    $('input[required]').each(function (idx, item) {
        Page.checkError($(item));
    });
    $('select[required]').each(function (idx, item) {
        Page.checkSelect($(item));
    });
    if ($('div.error').length > 0) {
        return false;
    }
    return true;
}
function checkSave() {
    var status = $("[id$=DDL_StatusAsset]").val();
    var noithat = $("[id$=DDL_Furnitur]").val();
    var phaply = $("[id$=DDL_Legal]").val();

    if (phaply == null || phaply == undefined || phaply == 0) {
        Page.showNotiMessageError("Bạn phải chọn pháp lý !.");
        return false;
    }
    if (status == null || status == undefined || status == 0) {
        Page.showNotiMessageError("Bạn phải chọn tình trạng !.");
        return false;
    }
    //314 lien hệ lại
    if (status == 314) {
        if ($("#txt_DateContractEnd").val().length <= 0) {
            Page.showNotiMessageError("Bạn phải nhập ngày liên hệ lại !.");
            return false;
        }
    }
    else {
        if (!$('#chkCT').prop('checked') &&
            !$('#chkCN').prop('checked')) {
            Page.showNotiMessageError("Bạn phải nhập chọn nhu cầu");
            return false;
        }
        if ($('#chkCT').prop('checked') && _Usdthue == 0) {
            if ($('#txt_PriceRentVND').val() == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá thuê VNĐ");
                return false;
            }
        }
        if ($('#chkCT').prop('checked') && _Usdthue == 1) {
            if ($('#txt_PriceRentUSD').val() == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá thuê USD");
                return false;
            }
        }
        if ($('#chkCN').prop('checked') && _Usdban == 0) {
            if ($('#txt_PriceVND').val() == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá chuyển nhượng VNĐ");
                return false;
            }
        }
        if ($('#chkCN').prop('checked') && _Usdban == 1) {
            if ($('#txt_PriceUSD').val() == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá chuyển nhượng USD");
                return false;
            }
        }
        if (noithat == null || noithat == undefined || noithat == 0) {
            Page.showNotiMessageError("Bạn phải chọn nội thất !.");
            return false;
        }
    }

    return true;
}
function saveTask() {
    var id = $("[id$=HID_InfoKey]").val();
    $.ajax({
        type: 'POST',
        url: '/INFO/InfoList.aspx/SaveTask',
        data: JSON.stringify({
            'ID': id,
            'Status': _status,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu cập nhật thông tin thành công.");
                window.location = "/INFO/InfoList.aspx"
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.responseText.indexOf('network') != -1)
                Page.showNotiMessageInfo("Lỗi mạng", "Vui lòng thử lại sau ít phút !.");
            else
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function saveProduct() {
    if (!checkSave()) {
        return false;
    }

    var assetkey = $("[id$=HID_AssetKey]").val();
    var taskkey = $("[id$=HID_InfoKey]").val();
    var tasktype = $("[id$=HID_InfoType]").val();
    var assettype = $("[id$=HID_AssetType]").val();
    var Purpose = "";
    $("input[name='chkPurpose']:checked").each(function (i) {
        Purpose += $(this).val() + "/ ";
    });
    $.ajax({
        type: "POST",
        url: "/INFO/InfoList.aspx/SaveProduct",
        data: JSON.stringify({
            "ID": taskkey,
            "StatusInfo": _status,
            "TaskType": tasktype,
            "AssetKey": assetkey,
            "AssetType": assettype,
            "AssetStatus": $("[id$=DDL_StatusAsset]").val(),
            "AssetRemind": $("[id$=txt_DateContractEnd]").val(),
            "AssetPrice_VND": $("[id$=txt_PriceVND]").val(),
            "AssetPriceRent_VND": $("[id$=txt_PriceRentVND]").val(),
            "AssetPrice_USD": $("[id$=txt_PriceUSD]").val(),
            "AssetPriceRent_USD": $("[id$=txt_PriceRentUSD]").val(),
            "AssetFurnitur": $("[id$=DDL_Furnitur]").val(),
            "AssetPurpose": Purpose,
            "AssetDescription": $("[id$=txt_AssetDescription]").val(),
            "UsdBan": _Usdban,
            "UsdThue": _Usdthue,
            "Legal": $("[id$=DDL_Legal]").val()
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                window.location = "/INFO/InfoList.aspx"
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.se-pre-con').fadeOut('slow');
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function saveOwner() {
    if ($("[id$=HID_AssetKey]").val() != 0) {
        var id = $("[id$=HID_InfoKey]").val();
        var type = $("[id$=HID_InfoType]").val();

        $.ajax({
            type: 'POST',
            url: '/INFO/InfoList.aspx/SaveOwner',
            data: JSON.stringify({
                'ID': id,
                'Status': _status,
                'AssetKey': $("[id$=HID_AssetKey]").val(),
                'AssetType': $("[id$=HID_AssetType]").val(),
                'CustomerName': $("[id$=txt_InfoName]").val(),
                'Phone1': $("[id$=txt_InfoPhone1]").val(),
                'Phone2': $("[id$=txt_InfoPhone2]").val(),
                'Email': $("[id$=txt_InfoEmail]").val(),
                'Address1': $("[id$=txt_InfoAddress1]").val(),
                'Address2': $("[id$=txt_InfoAddress2]").val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message == "OK") {
                    Page.showNotiMessageInfo("Thông báo !", "Đã lưu thong tin chủ nhà thành công.");
                    window.location = "/INFO/InfoList.aspx"
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
    else
        Page.showNotiMessageInfo("Không thể cập nhật thông tin", "Chưa có sản phẩm này chưa có trong dữ liệu, vui lòng kiểm tra lại thông tin !.");
}
function getTask(ID, TaskType) {
    $.ajax({
        type: "POST",
        url: "/INFO/InfoList.aspx/GetTask",
        data: JSON.stringify({
            "ID": ID,
            "TaskType": TaskType,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            $("[id$=HID_AssetKey]").val(msg.d.Message);
            $("[id$=HID_ProjectKey]").val(msg.d.Result4);
            $("[id$=HID_TaskKey]").val(ID);

            $("#msgAction").text("...");

            $("#PageHead").empty();
            $("#PageHead").append($(msg.d.Result2));
            $("#PageInfo").empty();
            $("#PageInfo").append($(msg.d.Result));
            $("#mProcess").modal({
                backdrop: true,
                show: true
            });

            setTimeout(function () {
                getOwner(taskid, tasktype);
            }, 1000);
            setTimeout(function () {
                checkAsset(assetid, assettype);
            }, 1000);
            setTimeout(function () {
                getNearestTask();
            }, 1000);
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getOwner(ID, TaskType) {
    $.ajax({
        type: "POST",
        url: "/INFO/InfoList.aspx/GetOwner",
        data: JSON.stringify({
            "ID": ID,
            "TaskType": TaskType,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function (msg) {
            $("#txt_InfoName").val(msg.d.CustomerName);
            $("#txt_InfoPhone1").val(msg.d.Phone1);
            $("#txt_InfoPhone2").val(msg.d.Phone2);
            $("#txt_InfoEmail").val(msg.d.Email1);
            $("#txt_InfoAddress1").val(msg.d.Address1);
            $("#txt_InfoAddress2").val(msg.d.Address2);
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function checkAsset(AssetKey, AssetType) {
    console.log(AssetKey + AssetType);
    console.log($("[id$=HID_AssetKey]").val());
    console.log($("[id$=HID_AssetType]").val());
    if ($("[id$=HID_AssetKey]").val() != 0) {
        $.ajax({
            type: "POST",
            url: "/INFO/InfoList.aspx/CheckAsset",
            data: JSON.stringify({
                "AssetKey": $("[id$=HID_AssetKey]").val(),
                "AssetType": $("[id$=HID_AssetType]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (msg) {
                if (msg.d.IsResale == 1)
                    $("#msgAction").text("SP da co, vui long kiem tra cap nhat thong tin !.");
                else
                    $("#msgAction").text("Sản phẩm này chưa được ký gửi !. !.");

                $("[id$=DDL_StatusAsset]").val(msg.d.StatusKey);
                $("[id$=txt_DateContractEnd]").val(msg.d.DateContractEnd);
                $("[id$=txt_PriceRentVND]").val(msg.d.PriceRent_VND);
                $("[id$=txt_PriceRentUSD]").val(msg.d.PriceRent_USD);
                $("[id$=txt_PriceVND]").val(msg.d.Price_VND);
                $("[id$=txt_PriceUSD]").val(msg.d.Price_USD);
                $("[id$=DDL_Furnitur]").val(msg.d.FurnitureKey);
                $("[id$=DDL_Legal]").val(msg.d.LegalKey);
                var SetPurpose = msg.d.PurposeKey.split("/");
                for (var i in SetPurpose) {
                    var val = $.trim(SetPurpose[i]);
                    $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}
function getNearestTask() {
    $.ajax({
        type: "POST",
        url: "/INFO/InfoList.aspx/GetNearestTask",
        data: JSON.stringify({
            "InfoKey": _relateKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function (msg) {
            $("#ReplyInfoEarlier").text(msg.d.Message);
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}