﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Board.aspx.cs" Inherits="WebApp.JOB.Board" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Board
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    All board info
                                </small>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul id="tasks" class="item-list ui-sortable">
                    <li class="item-orange clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Answering customer questions</span>
                        </label>

                        <div class="pull-right easy-pie-chart percentage" data-size="30" data-color="#ECCB71" data-percent="42" style="height: 30px; width: 30px; line-height: 29px;">
                            <span class="percent">42</span>%
																	<canvas height="30" width="30"></canvas>
                        </div>
                    </li>

                    <li class="item-red clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Fixing bugs</span>
                        </label>

                        <div class="pull-right action-buttons">
                            <a href="#" class="blue">
                                <i class="ace-icon fa fa-pencil bigger-130"></i>
                            </a>

                            <span class="vbar"></span>

                            <a href="#" class="red">
                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            </a>

                            <span class="vbar"></span>

                            <a href="#" class="green">
                                <i class="ace-icon fa fa-flag bigger-130"></i>
                            </a>
                        </div>
                    </li>

                    <li class="item-default clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Adding new features</span>
                        </label>

                        <div class="pull-right pos-rel dropdown-hover">
                            <button class="btn btn-minier bigger btn-primary">
                                <i class="ace-icon fa fa-cog icon-only bigger-120"></i>
                            </button>

                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-caret dropdown-close dropdown-menu-right">
                                <li>
                                    <a href="#" class="tooltip-success" data-rel="tooltip" title="" data-original-title="Mark&nbsp;as&nbsp;done">
                                        <span class="green">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="tooltip-error" data-rel="tooltip" title="" data-original-title="Delete">
                                        <span class="red">
                                            <i class="ace-icon fa fa-trash-o bigger-110"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="item-blue clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Upgrading scripts used in template</span>
                        </label>
                    </li>

                    <li class="item-grey clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Adding new skins</span>
                        </label>
                    </li>

                    <li class="item-green clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Updating server software up</span>
                        </label>
                    </li>

                    <li class="item-pink clearfix ui-sortable-handle">
                        <label class="inline">
                            <input type="checkbox" class="ace">
                            <span class="lbl">Cleaning up</span>
                        </label>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {

        });
    </script>
</asp:Content>
