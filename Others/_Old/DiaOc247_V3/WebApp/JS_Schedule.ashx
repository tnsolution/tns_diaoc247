﻿<%@ WebHandler Language="C#" Class="JsonResponse" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;
using Lib.SAL;

public class JsonResponse : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        DateTime start = Convert.ToDateTime(context.Request.QueryString["start"]);
        DateTime end = Convert.ToDateTime(context.Request.QueryString["end"]);
        int Employee = Convert.ToInt32(context.Request.QueryString["Employee"]);
        int Department = Convert.ToInt32(context.Request.QueryString["Department"]);

        List<int> idList = new List<int>();
        List<CalendarItem> tasksList = new List<CalendarItem>();
        List<ScheduleItem> dataFetch = ScheduleDetail_Data.GetSchedule(start, end, Department, Employee);
        //Generate JSON serializable events
        foreach (ScheduleItem cevent in dataFetch)
        {
            tasksList.Add(new CalendarItem
            {
                id = cevent.ID,
                title = cevent.Description,
                start = String.Format("{0:s}", cevent.Start),
                end = String.Format("{0:s}", cevent.End),
                description = cevent.Title,
                allDay = cevent.AllDay,
            });
            idList.Add(cevent.ID);
        }

        context.Session["idList"] = idList;
        //Serialize events to string
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string sJSON = oSerializer.Serialize(tasksList);

        //Write JSON to response object
        context.Response.Write(sJSON);
    }

    public bool IsReusable
    {
        get { return false; }
    }
}