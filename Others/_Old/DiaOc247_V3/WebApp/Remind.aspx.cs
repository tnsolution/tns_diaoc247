﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class Remind : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["Type"] != null)
                    HID_Remind.Value = Request["Type"];

                LoadData();
            }
        }

        void LoadData()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            DataTable zTable = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = Notification_Data.List(HID_Remind.Value.ToInt(), 0, 0);
                    break;

                case 2:
                    zTable = Notification_Data.List(HID_Remind.Value.ToInt(), DepartmentKey, 0);
                    break;

                case 3:
                    zTable = Notification_Data.List(HID_Remind.Value.ToInt(), DepartmentKey, EmployeeKey);
                    break;

                default:
                    zTable = Notification_Data.List(HID_Remind.Value.ToInt(), DepartmentKey, EmployeeKey);
                    break;
            }

            switch (HID_Remind.Value.ToInt())
            {
                case 1:
                    lblTitle.Text = " Sinh nhật khách: " + DateTime.Now.ToString("dd/MM/yyyy");
                    ViewHtml_Customer(zTable);
                    break;

                case 2:
                    lblTitle.Text = " Giao dịch đến hẹn: " + DateTime.Now.ToString("dd/MM/yyyy");
                    break;

                case 3:
                    lblTitle.Text = " Sản phẩm tới hẹn: " + DateTime.Now.ToString("dd/MM/yyyy");
                    ViewHtml_Building(zTable);
                    break;
            }
        }

        void ViewHtml_Building(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tableData'>");
            zSb.AppendLine("   <thead>");

            #region [Table Header]
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày liên hệ lại</th>");
            zSb.AppendLine("        <th>Mã sản phẩm</th>");
            zSb.AppendLine("        <th>Dự án</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            #endregion

            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            #region [Table Body]
            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                string ahref = "";
                foreach (DataRow r in zTable.Rows)
                {
                    string Type = "";
                    if (r["ObjectTableName"].ToString() == "PUL_Resale_Apartment")
                        Type = "ResaleApartment";
                    if (r["ObjectTableName"].ToString() == "PUL_Resale_House")
                        Type = "ResaleHouse";
                    if (r["ObjectTableName"].ToString() == "PUL_Resale_Ground")
                        Type = "ResaleGround";

                    ahref = "/SAL/ProductView.aspx?ID=" + r["ObjectTable"].ToString() + "&Type=" + Type;
                    zSb.AppendLine("            <tr key='" + r["ID"].ToString() + "' id='" + r["ObjectTable"].ToString() + "' style='cursor:pointer' class='clickable-row' data-href='" + ahref + "'>");
                    zSb.AppendLine("               <td>" + i++ + "</td>");
                    zSb.AppendLine("               <td>" + r["ObjectDate"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ObjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ObjectProduct"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");

                    if (r["IsRead"].ToInt() == 0)
                    {
                        zSb.AppendLine("               <td>Chưa xử lý</td>");
                    }
                    else
                    {
                        zSb.AppendLine("               <td>Đã xử lý</td>");
                    }
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("<tr><td></td><td colspan='5'>Chưa có dữ liệu</td></tr>");
            }
            #endregion

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        void ViewHtml_Customer(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tableData'>");
            zSb.AppendLine("   <thead>");

            #region [Table Header]

            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày sinh nhật</th>");
            zSb.AppendLine("        <th>Tên khách</th>");
            zSb.AppendLine("        <th>Số điện thoại</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");

            #endregion

            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            #region [Table Body]
            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                string ahref = "";
                foreach (DataRow r in zTable.Rows)
                {
                    ahref = "/CRM/CustomerView.aspx?ID=" + r["ObjectTable"].ToString();
                    zSb.AppendLine("            <tr key='" + r["ID"].ToString() + "' id='" + r["ObjectTable"].ToString() + "' style='cursor:pointer' class='clickable-row' data-href='" + ahref + "'>");
                    zSb.AppendLine("               <td>" + i++ + "</td>");
                    zSb.AppendLine("               <td>" + r["ObjectDate"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ObjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ObjectProduct"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");

                    if (r["IsRead"].ToInt() == 0)
                    {
                        zSb.AppendLine("               <td>Chưa xử lý</td>");
                    }
                    else
                    {
                        zSb.AppendLine("               <td>Đã xử lý</td>");
                    }
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("<tr><td></td><td colspan='5'>Chưa có dữ liệu</td></tr>");
            }
            #endregion

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static string ExeNotification(int Key)
        {
            Notification_Info zInfo = new Notification_Info(Key);
            zInfo.IsRead = 1;
            zInfo.ReadDate = DateTime.Now;
            zInfo.UpdateReaded();
            return "OK";
        }
    }
}