﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModalProductEdit.aspx.cs" Inherits="WebApp.SAL.ModalProductEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal fade" id="mViewInput" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body no-padding">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="widget-box">
                                    <div class="widget-header widget-header-blue widget-header-flat">
                                        <h5 class="widget-title lighter red" id="msgInfo">....</h5>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main" id="divInput">
                                            <div class="space-6"></div>
                                            <div class="row">
                                                <div id="kygui" style="display: none">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Tình trạng</label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="DDL_StatusAsset2" CssClass="select2" runat="server" AppendDataBoundItems="true" required="">
                                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label class="col-sm-2 control-label">Nhu cầu</label>
                                                            <div class="col-sm-5">
                                                                <div class="checkbox col-sm-6 no-padding-left">
                                                                    <label>
                                                                        <input type="checkbox" class="ace" value="CN" name="chkPurpose" />
                                                                        <span class="lbl">Chuyển nhượng</span>
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox col-sm-5 no-padding-left">
                                                                    <label>
                                                                        <input type="checkbox" class="ace" value="CT" name="chkPurpose" />
                                                                        <span class="lbl">Cho thuê</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Pháp lý</label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="DDL_Legal" runat="server" CssClass="select2"></asp:DropDownList>
                                                            </div>
                                                            <label class="col-sm-2 control-label"><span id="updategiaban">Giá bán VNĐ</span><sup>*</sup></label>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control" id="txt_updatePriceVND" placeholder="Nhập số" role="numeric" data='money' />
                                                                <input type="text" class="form-control" id="txt_updatePriceUSD" placeholder="Nhập số" role="numeric" data='money' />
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="onoffswitch">
                                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch1" />
                                                                    <label class="onoffswitch-label" for="Usdswitch1">
                                                                        <span class="onoffswitch-inner"></span>
                                                                        <span class="onoffswitch-switch"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                                            <div class="col-sm-3">
                                                                <div class="input-group date">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" role="datepicker" class="form-control pull-right"
                                                                        id="txt_updateDateContractEnd" placeholder="dd/MM/yyyy" />
                                                                </div>
                                                            </div>
                                                            <label class="col-sm-2 control-label"><span id="updategiathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control" id="txt_updatePriceRentVND" placeholder="Nhập số" role="numeric" data='money' />
                                                                <input type="text" class="form-control" id="txt_updatePriceRentUSD" placeholder="Nhập số" role="numeric" data='money' />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Nội thất</label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="DDL_Furnitur2" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label class="col-sm-2 control-label"><span id="updategiamua">Giá mua VNĐ</span><sup>*</sup></label>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control" id="txt_updatePricePurchaseVND" placeholder="Nhập số" role="numeric" data='money' />
                                                                <input type="text" class="form-control" id="txt_updatePricePurchaseUSD" placeholder="Nhập số" role="numeric" data='money' />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Ghi chú</label>
                                                            <div class="col-sm-9">
                                                                <textarea class="form-control" id="txt_updateAssetDescription" placeholder="..." rows="4"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="hr hr2 hr-double"></div>
                                                <div id="chunha" style="display: none">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Họ và tên(*)</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="txt_InfoName" placeholder="Nhập text" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">SĐT1</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="txt_InfoPhone1" placeholder="Nhập text" role="phone" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">SĐT2</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="txt_InfoPhone2" placeholder="Nhập text" role="phone" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Email</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="txt_InfoEmail" placeholder="Nhập text" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="txt_InfoAddress1" placeholder="Nhập text" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Địa chỉ liên hệ</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="txt_InfoAddress2" placeholder="Nhập text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Đóng</button>
                        <button type="button" class="btn btn-primary" id="btnProcessInfo">
                            Lưu thông tin</button>
                        <button type="button" class="btn btn-primary" id="btnProcessTask">
                            Lưu công việc</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
