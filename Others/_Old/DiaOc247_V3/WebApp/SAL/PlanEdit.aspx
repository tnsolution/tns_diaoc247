﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="PlanEdit.aspx.cs" Inherits="WebApp.SAL.PlanEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tháng</label>
                        <div class="col-sm-3">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" role="monthpicker" class="form-control pull-right" runat="server"
                                    id="txt_Month" placeholder="Chọn tháng năm" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Phòng</label>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Chọn phòng--" Selected="True" disabled="disabled"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nhân viên</label>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True" disabled="disabled"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ghi chú</label>
                        <div class="col-sm-5">
                            <input name="txt_Description" type="text" id="txt_Description" runat="server" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="space-6"></div>
                <h4 class="header smaller lighter blue">
                    <a href="#mPlanDetail" data-toggle="modal"><i class="ace-icon fa fa-plus green"></i>Thêm mục tiêu</a></h4>
                <div class="row">
                    <div class="col-sm-9">
                        <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mPlanDetail" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Cài đặt yêu cầu mục tiêu</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Mục tiêu</label>
                        <asp:DropDownList ID="DDL_Category" CssClass="form-control select2" runat="server">
                            <asp:ListItem Value="0" Text="--Chọn 1 mục tiêu--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Yêu cầu</label>
                        <div>
                            <input name="txt_Number" type="text" id="txt_Number" class="form-control" placeholder="Chỉ nhập số" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSaveRecord">OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_PlanRecord" runat="server" Value="0" />
    <asp:HiddenField ID="HID_PlanKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $("[role='monthpicker']").datepicker({
                autoclose: true,
                format: 'mm/yyyy',
                startDate: '01/01/1900',
                todayHighlight: true
            });
            $(".select2").select2({ width: "100%" });
            $("#btnSaveRecord").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/SAL/PlanEdit.aspx/SaveDetailRecord',
                    data: JSON.stringify({
                        'Key': $("[id$=HID_PlanKey]").val(),
                        'CategoryKey': $("[id$=DDL_Category]").val(),
                        'Value': $("#txt_Number").val()
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        if (msg.d.Message.length <= 0) {
                            populateTable();
                        }
                        else {
                            Page.showPopupMessage("Lỗi nhập dòng tin", msg.d.Message);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSave").click(function () {
                if (validate()) {
                    $.ajax({
                        type: 'POST',
                        url: '/SAL/PlanEdit.aspx/SavePlan',
                        data: JSON.stringify({
                            'Key': $("[id$=HID_PlanKey]").val(),
                            'Department': $("[id$=DDL_Department]").val(),
                            'Employee': $("[id$=DDL_Employee]").val(),
                            'Description': $("[id$=txt_Description]").val(),
                            'Date': $('[id$=txt_Month]').val()
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        beforeSend: function () {
                        },
                        success: function (msg) {
                            if (msg.d.Message.length <= 0) {
                                Page.showNotiMessageInfo("Lưu thành công", "Mục tiêu đã được cài đặt.");
                            }
                            else {
                                Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                            }
                        },
                        complete: function () {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            //delete toan bo
            $("#btnDelete").click(function () {
                if ($("[id$=HID_PlanKey]").val() > 0) {
                    if (confirm("Bạn có chắc xóa.")) {
                        $.ajax({
                            type: "POST",
                            url: "/SAL/PlanEdit.aspx/DeletePlan",
                            data: JSON.stringify({
                                "Key": $("[id$=HID_PlanKey]").val(),
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $(".se-pre-con").fadeIn("slow");
                            },
                            success: function (msg) {
                                if (msg.d.Message != "") {
                                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                                }
                                else {
                                    location.href = "/SAL/PlanList.aspx";
                                }
                            },
                            complete: function () {
                                $(".se-pre-con").fadeOut("slow");
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr.status);
                                console.log(xhr.responseText);
                                console.log(thrownError);
                            }
                        });
                    }
                }
            });
            //hien thi popup chi tiet
            $("#tblPlanRecord tbody").on("click", "tr td:not(:last-child)", function (e) {
                var trid = $(this).closest('tr').attr('id');                
                $.ajax({
                    type: "POST",
                    url: "/SAL/PlanEdit.aspx/GetRecord",
                    data: JSON.stringify({
                        "AutoKey": trid,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            $('#mPlanDetail').modal({
                                backdrop: true,
                                show: true
                            });
                            alert(msg.d.CategoryKey);
                            $("#txt_Number").val(msg.d.Value);
                            $("[id$=DDL_Category]").val(msg.d.CategoryKey);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            //delete dong chi tiet
            $("#tblPlanRecord tbody").on("click", "a[btn='btnDeleteRec']", function (e) {
                if (confirm("Bạn có chắc xóa.")) {
                    var Key = $(this).closest('tr').attr('id');
                    $.ajax({
                        type: "POST",
                        url: "/SAL/PlanEdit.aspx/DeleteRecord",
                        data: JSON.stringify({
                            'AutoKey': Key,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {

                        },
                        success: function (msg) {
                            if (msg.d.Message != '') {
                                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
                            } else {
                                populateTable();
                                //$('#' + Key).remove();
                            }
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
        });
        function populateTable() {
            $.ajax({
                type: 'POST',
                url: 'PlanEdit.aspx/GetListRecord',
                data: JSON.stringify({
                    'Key': $("[id$=HID_PlanKey]").val(),
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (msg) {
                    if (msg.d.length > 0) {
                        var n = 1
                        $('#tblPlanRecord > tbody').empty();
                        for (var i = 0; i < msg.d.length; i++) {
                            $('#tblPlanRecord > tbody:last').append('<tr class="" id="' + msg.d[i].ID + '" style="cursor:pointer">'
                                    + '<td>' + n++ + '</td>'
                                    + '<td>' + msg.d[i].CategoryName + '</td>'
                                    + '<td>' + msg.d[i].Value + '</td>'
                                    + '<td><div class="hidden-sm hidden-xs action-buttons pull-right"><a class="red" btn="btnDeleteRec" href="#"><i class="ace-icon fa fa-trash-o bigger-130"></i> Xóa</a></div></td>'
                                    + '</tr>');
                        }
                    } else {
                        Page.showPopupMessage("Lỗi liên hệ Admin", "Lỗi lấy dữ liệu kế hoạch.")
                    }
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
