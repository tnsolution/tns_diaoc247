﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class PlanList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();

                LoadData();
            }
        }

        void LoadData()
        {
            lblDateView.Text = " tháng " + Convert.ToDateTime(HID_ToDate.Value).ToString("MM/yyyy");

            int EmployeeKey = DDL_Employee.SelectedValue.ToInt();
            int DepartmentKey = DDL_Department.SelectedValue.ToInt();

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Plan_Data.List(DepartmentKey, EmployeeKey,
                Convert.ToDateTime(HID_FromDate.Value),
                Convert.ToDateTime(HID_ToDate.Value));

            zSb.Append("<table class='table  table-bordered table-hover' id='tblPlanList'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tháng</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th>Xem chi tiết</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");
            int i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                string detail = @"
<div class='action-buttons'>
    <a href='#' class='green bigger-140 show-details-btn' title='Show Details'>
        <i class='ace-icon fa fa-angle-double-up'></i>
        <span class='sr-only'>Details</span>
    </a>
    <a href='#' class='green bigger-140' btn='btnCopy' title='Copy mục tiêu sang tháng mới'>
        <i class='ace-icon fa fa-share'></i>
        <span class='sr-only'>Copy</span>
    </a>
</div>";

                #region [Master TR]
                zSb.Append("    <tr id='" + r["PlanKey"].ToString() + "' fid='" + r["EmployeeKey"].ToString() + "' style='cursor:pointer'>");
                zSb.AppendLine("    <td>" + (i++) + "</td>");
                zSb.AppendLine("    <td>" + Convert.ToDateTime(r["PlanDate"]).ToString("MM/yyyy") + "</td>");
                zSb.AppendLine("    <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("    <td>" + r["Note"].ToString() + "</td>");
                zSb.AppendLine("    <td>" + detail + "</td>");
                zSb.Append("    </tr>");
                #endregion

                #region [Child Tr]
                DataTable zChild = Plan_Rec_Data.List(r["PlanKey"].ToInt());

                zSb.AppendLine("            <tr class='detail-row'>");
                zSb.AppendLine("                <td colspan='5'>");
                zSb.AppendLine("                    <div class='table-detail'>");
                zSb.AppendLine("                        <div class='row'>");
                zSb.AppendLine("                            <div class='col-xs-12 col-sm-7'>");
                zSb.AppendLine("                                <div class='profile-user-info profile-user-info-striped'>");

                if (zChild.Rows.Count > 0)
                {
                    int j = 1;
                    foreach (DataRow rChild in zChild.Rows)
                    {
                        zSb.AppendLine("                                <div class='profile-info-row' id='" + rChild["ID"].ToString() + "'>");
                        zSb.AppendLine("                                    <div class='profile-info-name'>" + rChild["CategoryName"].ToString() + "</div>");
                        zSb.AppendLine("                                        <div class='profile-info-value'>");
                        zSb.AppendLine("                                            <span>" + rChild["Value"].ToString() + "</span>");
                        zSb.AppendLine("                                        </div>");
                        zSb.AppendLine("                                    </div>");
                    }
                }

                zSb.AppendLine("                                </div>");
                zSb.AppendLine("                            </div>");
                zSb.AppendLine("                        </div>");
                zSb.AppendLine("                    </div>");
                zSb.AppendLine("                </td>");
                zSb.AppendLine("            </tr>");
                #endregion
            }
            zSb.AppendLine("    </tbody>");
            zSb.Append("</table>");
            Literal_Table.Text = zSb.ToString();
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            int ViewNextPrev = HID_ViewNextPrev.Value.ToInt();
            if (ViewNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HID_FromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();

            }

            if (ViewNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HID_FromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = ToDate.AddMonths(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }

            LoadData();
        }

        [WebMethod]
        public static ItemResult CopyPlan(int PlanKey, int EmployeeKey)
        {
            DateTime zCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime zNextMonth = zCurrentMonth.AddMonths(1);

            ItemResult zResult = new ItemResult();
            Plan_Info zMaster = new Plan_Info(EmployeeKey, zNextMonth.Month, zNextMonth.Year);
            if (zMaster.Key > 0)
            {
                zResult.Message = "Mục tiêu tháng " + zNextMonth.ToString("MM/yyyy") + " của nhân viên " + zMaster.EmployeeName + " đã có rồi, hãy chọn lại thông tin khác.";
                return zResult;
            }
            else
            {
                zMaster = new Plan_Info(PlanKey);
                zMaster.PlanDate = zNextMonth;
                zMaster.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zMaster.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zMaster.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zMaster.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zMaster.Create();

                if (zMaster.Message != string.Empty)
                {
                    zResult.Message = zMaster.Message;
                    return zResult;
                }

                List<ItemPlan> zDetail = Plan_Rec_Data.List(PlanKey).DataTableToList<ItemPlan>();
                foreach (ItemPlan item in zDetail)
                {
                    Plan_Rec_Info zRec = new Plan_Rec_Info();
                    zRec.PlanKey = zMaster.Key;
                    zRec.CategoryKey = item.CategoryKey.ToInt();
                    zRec.CategoryName = item.CategoryName;
                    zRec.Value = item.Value.ToInt();
                    zRec.Create();

                    if (zRec.Message != string.Empty)
                    {
                        zResult.Message = zRec.Message;
                        return zResult;
                    }
                }
            }

            return zResult;
        }

        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking = 2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}