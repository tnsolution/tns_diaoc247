﻿using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Lit_Title.Text = "Sản phẩm..";
                Tools.DropDown_DDL(DDL_Table2, "SELECT TableName, CategoryName FROM SYS_Table WHERE TYPE = 2 ORDER BY Rank", false);
                Tools.DropDown_DDL(DDL_Project2, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                DDL_Table2.SelectedIndex = 1;
                LoadData();
                CheckRole();
            }
        }
        void LoadData()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            List<Product_Item> zList = new List<Product_Item>();
            if (Session["Search"] != null)
            {
                ItemSearch zSession = (ItemSearch)Session["Search"];
                zList = Product_Data.Search(zSession.ProjectKey.ToInt(),
                    zSession.CategoryKey.ToInt(), zSession.ID1, zSession.ID2, zSession.ID3,
                    zSession.Bed, zSession.Name, zSession.Status,
                    zSession.Purpose, zSession.Personal.ToInt(), zSession.EmployeeKey.ToInt(), 0);
            }
            else
            {
                zList = Product_Data.Search(DDL_Project.SelectedValue.ToInt(), 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, 100);
            }

            #region [HTML TABLE]
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (Product_Item r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.ProjectName + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + " PN, " + r.Area + " m<sup>2</sup></td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("                <td>" + r.EmployeeName + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='8'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }
            #endregion

            LitTable_Body.Text = zSb.ToString();
        }
        //loc lay gia va nhu cau
        static string GetPrice(Product_Item Item)
        {
            string PurposeShow = "";
            if (Item.Purpose.Contains(","))
            {
                string[] temp = Item.Purpose.Split(',');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "Chuyển nhượng":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                                break;

                            case "Cho thuê":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                                break;
                        }
                    }
                }
            }
            else
            {
                string s = Item.Purpose;
                switch (s.Trim())
                {
                    case "Chuyển nhượng":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                        break;

                    case "Cho thuê":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                        break;
                }
            }

            return PurposeShow;
        }
        [WebMethod(EnableSession = true)]
        public static string Search(int Project, int Category, string ID1, string ID2, string ID3, string Bed, string Name, string Status, string Purpose, int Personal, int Employee, string View, string Door)
        {
            List<Product_Item> zList = new List<Product_Item>();

            #region [Search]
            Status = Status.Contains(',') ? Status.Remove(Status.LastIndexOf(','), 1) : string.Empty;
            if (Employee == 0)
                Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            zList = Product_Data.Search(Project, Category, ID1, ID2, ID3, Bed, Name, Status, Purpose, Personal, Employee, View, Door);
            ItemSearch ISearch = new ItemSearch();
            ISearch.Personal = Personal.ToString();
            ISearch.ProjectKey = Project.ToString();
            ISearch.CategoryKey = Category.ToString();
            ISearch.EmployeeKey = Employee.ToString();
            ISearch.Status = Status;
            ISearch.Purpose = Purpose;
            ISearch.Product = Name;
            ISearch.Name = Name;
            ISearch.Bed = Bed;
            ISearch.ID1 = ID1;
            ISearch.ID2 = ID2;
            ISearch.ID3 = ID3;
            HttpContext.Current.Session.Add("Search", ISearch);
            #endregion           

            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (Product_Item r in zList)
                {
                    string hot = "";
                    if (r.Hot.ToInt() == 5)
                        hot = "&nbsp;<i class='fa fa-thumbs-o-up icon-animated-vertical bigger-130 orange' aria-hidden='true'></i>";
                    zSb.AppendLine("            <tr id=\"" + r.AssetKey + "\" fid=\"" + r.AssetType.Trim() + "\">");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.ProjectName.Trim() + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID.Trim() + hot + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName.Trim() + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + " PN, " + r.Area + " m<sup>2</sup></td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("                <td>" + r.EmployeeName + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan=8>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }
            return zSb.ToString();
        }
        [WebMethod]
        public static string[] GetAsset(string prefix, string project)
        {
            List<string> customers = new List<string>();
            DataTable zTable = Product_Data.GetPrefix_AssetName(project.ToInt(), prefix);
            foreach (DataRow r in zTable.Rows)
            {
                customers.Add(string.Format("{0};{1}", r["AssetKey"].ToString(), r["AssetID"].ToString().Trim()));
            }
            return customers.ToArray();
        }
        [WebMethod]
        public static string PrimaryProject(int ProjectKey)
        {
            Project_Info zInfo = new Project_Info(ProjectKey);
            return zInfo.IsPrimary.ToString();
        }

        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentID <> 'x' ORDER BY [RANK]", false);
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                    DDL_Department.SelectedIndex = 1;
                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Project, "SELECT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey = " + HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt() + " ORDER BY ProjectName", false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Employee.Visible = true;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking = 2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Project, "SELECT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey = " + HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt() + " ORDER BY ProjectName", false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}