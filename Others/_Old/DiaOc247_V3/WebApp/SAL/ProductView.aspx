﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductView.aspx.cs" Inherits="WebApp.SAL.ProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/Gallery-master/css/blueimp-gallery.min.css" />
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
    <style>
        .profile-user-info {
            width: 100% !important;
        }

        .widget-box {
            margin: 0px !important;
        }

        .profile-info-name {
            width: 150px !important;
        }

        #bangphai table th {
            padding: 0px !important;
        }

        #bangphai table th {
            padding: 0px !important;
        }

        #bangphai table td {
            padding-left: 4px !important;
            padding-right: 4px !important;
        }

        #bangphai table .td1 {
            width: 10%;
        }

        #bangphai table .td2 {
            width: 15%;
        }

        #bangphai table .td5 {
            width: 15%;
        }

        .widget-main {
            padding: 4px !important;
        }

        .col-xs-6 {
            padding-left: 10px;
            padding-right: 10px;
        }

        .indam {
            font-weight: bolder;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết sản phẩm                              
                <span id="tieudetrang">
                    <asp:Literal ID="txt_Asset" runat="server"></asp:Literal></span>
                <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                <a class="btn btn-white btn-info btn-bold" id="btnExeReminder" style="display: none">
                    <i class="ace-icon fa fa-adjust blue"></i>
                    Xử lý nhắc nhở
                </a>
                <a href="#" class="btn btn-white btn-bold" id="btnExpand">Mở rộng 
                </a>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:goBackWithRefresh(); $('.se-pre-con').fadeIn('slow');">
                        <i class="ace-icon fa fa-reply info"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6" id="leftbox">
                <table id="tblMain" class="table table-bordered table-hover">
                    <tbody>
                        <tr class="action indam">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin làm việc</td>
                            <td style="width: 1%">
                                <a href="#" tabindex="-1" class="center" id="UpdateAsset" isedit><i class='ace-icon fa fa-pencil-square-o orange bigger-130'></i></a>
                            </td>
                        </tr>
                        <tr class="detail-row open">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Literal ID="Lit_InfoAsset" runat="server"></asp:Literal>
                                            <asp:Literal ID="Lit_InfoOwner" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin cơ bản</td>
                            <td></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Literal ID="Lit_ExtraAsset" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action" id="img">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Hình ảnh</td>
                            <td>
                                <a href="#mUpload" tabindex="-1" class="center" data-toggle="modal" isedit><i class='ace-icon fa fa-plus orange bigger-130'></i></a>
                            </td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="hinhanh">
                                                <asp:Literal ID="Lit_File" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Chủ nhà</td>
                            <td>
                                <a href="#" tabindex="-1" class="center" id="AddGuest" isedit><i class="ace-icon fa fa-plus orange bigger-130"></i></a>
                            </td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="chunha">
                                                <asp:Literal ID="Lit_Owner" runat="server"></asp:Literal>
                                            </div>
                                            <%--<div id="chunha" style="display: inline-flex; width: 100%; overflow-x: scroll"></div>--%>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Tin đăng</td>
                            <td>
                                <a href="#" tabindex="-1" class="center" id="AddPost" isedit><i class="ace-icon fa fa-pencil-square-o orange bigger-130"></i></a>
                            </td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="tindang">
                                            <asp:Literal ID="Lit_InfoArticle" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin quản lý</td>
                            <td>
                                <a href="#" tabindex="-1" class="center" id="AddShare" isedit><i class="ace-icon fa fa-plus orange bigger-130"></i></a></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="quanly">
                                            <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Literal ID="Lit_AllowSearch" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-6" id="rightbox">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Các sản phẩm cùng dự án:
                            <asp:Literal ID="Lit_RightTitle" runat="server"></asp:Literal></h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div id="bangphai">
                                <asp:Literal ID="Lit_RightTable" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mProcess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" id="PageHead">
                    <h5>Cập nhật nhanh thông tin làm việc</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tình trạng</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_StatusAsset2" CssClass="select2" runat="server" AppendDataBoundItems="true" required="">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Nhu cầu</label>
                                    <div class="col-sm-5">
                                        <div class="checkbox col-sm-6 no-padding-left">
                                            <label>
                                                <input type="checkbox" class="ace" value="CN" name="chkPurpose" id="chkCN" />
                                                <span class="lbl">Chuyển nhượng</span>
                                            </label>
                                        </div>
                                        <div class="checkbox col-sm-5 no-padding-left">
                                            <label>
                                                <input type="checkbox" class="ace" value="CT" name="chkPurpose" id="chkCT" />
                                                <span class="lbl">Cho thuê</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pháp lý</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_Legal" runat="server" CssClass="select2"></asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label"><span id="updategiaban">Giá bán VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_updatePriceVND" placeholder="Nhập số" moneyinput="true" />
                                        <input type="text" class="form-control" id="txt_updatePriceUSD" placeholder="Nhập số" moneyinput="true" />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch1" />
                                            <label class="onoffswitch-label" for="Usdswitch1">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_updateDateContractEnd" placeholder="dd/MM/yyyy" />
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label"><span id="updategiathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_updatePriceRentVND" placeholder="Nhập số" moneyinput="true" />
                                        <input type="text" class="form-control" id="txt_updatePriceRentUSD" placeholder="Nhập số" moneyinput="true" />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch2" />
                                            <label class="onoffswitch-label" for="Usdswitch2">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nội thất</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="DDL_Furnitur2" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label"><span id="updategiamua">Giá mua VNĐ</span><sup>*</sup></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txt_updatePricePurchaseVND" placeholder="Nhập số" moneyinput="true" />
                                        <input type="text" class="form-control" id="txt_updatePricePurchaseUSD" placeholder="Nhập số" moneyinput="true" />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="Usdswitch3" />
                                            <label class="onoffswitch-label" for="Usdswitch3">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="txt_updateAssetDescription" placeholder="..." rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnProcess">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mGuest">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin liên hệ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Họ tên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT1</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT2</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Email" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ liên lạc</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveGuest" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mUpload">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Tải ảnh</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="text-warning list-unstyled spaced">
                                <li><i class="ace-icon fa fa-exclamation-triangle"></i>Hình ảnh sản phẩm (bạn có thể chọn nhiều tập tin)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
                        </div>
                        <div class="col-xs-4">
                            <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
                                <i class="ace-icon fa fa-plus"></i>
                                Upload
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mPost">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Đăng tin</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <asp:DropDownList ID="DDL_WebChon" CssClass="form-control select2" runat="server">
                                    <asp:ListItem Value="0" Text="Không đăng tin"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đăng tin"></asp:ListItem>
                                </asp:DropDownList>
                                <input type="text" class="form-control" id="txt_WebTieuDe" placeholder="Tiêu đề tin đăng" />
                                <textarea class="form-control" id="txt_WebNoiDung" placeholder="Nội dung tin đăng"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' id='btnSavePost' data-dismiss='modal'>
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="mShare">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chia sẽ thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal' id='btnSaveShare'>
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <asp:HiddenField ID="HID_Edit" runat="server" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
    <asp:HiddenField ID="HID_RemindKey" runat="server" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" />
    <asp:HiddenField ID="HID_EmployeeKey" runat="server" />
    <asp:HiddenField ID="HID_GuestKey" runat="server" />
    <asp:Button ID="btnUploadImg" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadImg_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/Gallery-master/js/blueimp-gallery.min.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script src="/template/tinymce/tinymce.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <script src="/SAL/ProductView.js"></script>
    <script>
        document.getElementById('hinhanh').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = { index: link, event: event },
                links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    </script>
</asp:Content>
