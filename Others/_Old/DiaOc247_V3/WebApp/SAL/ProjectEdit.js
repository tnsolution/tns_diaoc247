﻿var map;
var marker;
var infowindow;
function initialize() {
    var mapProp = {
        center: myCenter,
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });
    google.maps.event.addListener(map, 'click', function (event) {
        placeMarker(event.latLng);
    });
}
function placeMarker(location) {
    if (!marker || !marker.setPosition) {
        marker = new google.maps.Marker({
            position: location,
            map: map,
        });
    } else {
        marker.setPosition(location);
    }
    if (!!infowindow && !!infowindow.close) {
        infowindow.close();
    }
    infowindow = new google.maps.InfoWindow({
        content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
    });
    infowindow.open(map, marker);
    $("[id$=HID_LatLng]").val(location.lat() + ", " + location.lng());
}
google.maps.event.addDomListener(window, 'load', initialize);

$(function () {
    tinymce.init({
        selector: '[id$=txt_Content]',
        height: 400,
        theme: 'modern',
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        image_advtab: true,
        templates: [
          { title: 'Test template 1', content: 'Test 1' },
          { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'
        ],
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    $(".iframe").colorbox({ iframe: true, width: "100%", height: "100%" });
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
});
$(function () {
    $('[moneyonly]').number(true, 0);

    $("#UpdateLocation").click(function () {
        var latlng = $("[id$=HID_LatLng]").val();
        var Key = $("[id$=HID_ProjectKey]").val();
        if (latlng == 0) {
            Page.showNotiMessageError("Bạn chưa chọn vị trí.", "....");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/SAL/ProjectEdit.aspx/UpdateLatLng",
            data: JSON.stringify({
                "Key": Key,
                "LatLng": latlng
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                //$("#UpdateLocation").attr("disabled", "disabled");
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                }
                else {
                    Page.showNotiMessageInfo("Đã cập nhật vị trí", "...");
                }
            },
            complete: function () {
                //$("#UpdateLocation").removeAttr("disabled");
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("[id$=txt_Address]").blur(function () {
        var fullAddress = $("[id$=txt_Address]").val() + ', ' + $('[id$=DDL_District] option:selected').text() + ', ' + $('[id$=DDL_Province] option:selected').text();
        $("[id$=txt_AddressFull]").val(fullAddress);
    });
    $(".select2").select2({ width: "100%" });
    //save file
    $("#UploadFile").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=btnUploadFile]").trigger("click");
    });
    $("#UploadImg").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=btnUploadImg]").trigger("click");
    });
    //del project
    $("#btnDelete").click(function () {
        var Key = $("[id$=HID_ProjectKey]").val();
        if (Key > 0) {
            if (confirm("Bạn có chắc xóa thông tin !")) {
                $.ajax({
                    type: "POST",
                    url: "/SAL/ProjectEdit.aspx/DeleteProject",
                    data: JSON.stringify({
                        "Key": Key,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        //$("#btnDelete").attr("disabled", "disabled");
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            window.location = "ProjectList.aspx";
                        }
                    },
                    complete: function () {
                        $("#btnDelete").removeAttr("disabled");
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        else {
            Page.showNotiMessageInfo("Thông báo !", "Không có thông tin để xóa.");
        }
    });
    //save project
    $("#btnSave").click(function () {

        console.log($('[id$=txt_Content]').val());
        if (Page.validate()) {
            $.ajax({
                type: 'POST',
                url: '/SAL/ProjectEdit.aspx/SaveProject',
                data: JSON.stringify({
                    'Key': $("[id$=HID_ProjectKey]").val(),
                    'ProjectName': $("[id$=txt_ProjectName]").val(),
                    'Investor': $("[id$=txt_Investor]").val(),
                    'BasicPrice': $("[id$=BasicPrice]").val(),
                    'Street': $("[id$=txt_Address]").val(),
                    'Province': $("[id$=DDL_Province]").val(),
                    'District': $("[id$=DDL_District]").val(),
                    'AddressFull': $("[id$=txt_AddressFull]").val(),
                    'Content': $('[id$=txt_Content]').val(),
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message.length <= 0) {
                        Page.showNotiMessageInfo("Lưu thành công", "Thông tin dự án cơ bản đã lưu, các thông tin còn lại vui lòng cập nhật từng phần.");
                    }
                    else {
                        Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //save category
    $("#btnSaveCategory").on('click', function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProjectEdit.aspx/SaveCategory",
            data: JSON.stringify({
                "Key": $('[id$=HID_ProjectKey]').val(),
                "CategoryKey": $('[id$=DDL_Category]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getCategory();
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //del category
    $("#tblCategory").on("click", "a[btn='btnDeleteCategory']", function (e) {
        var Key = $(this).closest('div').parent().parent().attr('id');
        delCategory(Key);
    });
    //save asset
    $("#btnSaveAsset").on('click', function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProjectEdit.aspx/SaveAsset",
            data: JSON.stringify({
                "Key": $('[id$=HID_ProjectKey]').val(),
                "AssetKey": $('[id$=HID_AssetKey]').val(),
                "CategoryKey": $('[id$=DDL_AssetCategory]').val(),
                "Bedroom": $('#txt_BedRoom').val(),
                "Amount": $('#txt_AssetAmount').val(),
                "Amount2": $('#txt_AssetAmount2').val(),
                "Area": $('#txt_AssetArea').val(),
                "Description": $('#txt_AssetDescription').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getAsset();
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //edit asset
    $("#tblAsset tbody tr td:not(:last-child)").on('click', function () {
        var Key = $(this).closest('tr').attr('id');
        openAsset(Key);
    });
    //del asset
    $("#tblAsset").on("click", "a[btn='btnDeleteAsset']", function (e) {
        var id = $(this).closest('tr').attr("id");
        var fid = $(this).closest('tr').attr("fid");
        delAsset(id);
    });
    //save allow
    $("#btnSaveShare").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProjectEdit.aspx/SaveShare",
            data: JSON.stringify({
                "Key": $('[id$=HID_ProjectKey]').val(),
                "DepartmentKey": $('[id$=DDL_Department]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getShare();
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //del allow
    $("#tblShare").on("click", "a[btn='btnDeleteShare']", function (e) {
        var Key = $(this).closest('div').parent().parent().attr('id');
        delShare(Key);
    });
    //del picture
    $("a[btn='btnDeleteImg']").click(function () {
        var Key = $(this).closest('div').parent().attr('id');
        delImg(Key);
    });
    //del file
    $("a[btn='btnDeleteFile']").click(function (e) {
        var id = $(this).closest('tr').attr("id");
        delFile(id);
    });
    //save fee
    $("#btnSaveFee").click(function () {
        saveFee();
    });
    //delete fee
    //del category
    $("#tblFee").on("click", "a[btn='btnDelFee']", function (e) {
        var Key = $(this).closest('div').parent().parent().attr('id');
        delFee(Key);
    });
    $("#showMap").on('shown.bs.tab', function () {
        /* Trigger map resize event */
        google.maps.event.trigger(map, 'resize');
    });

    var $overflow = '';
    var colorbox_params = {
        rel: 'colorbox',
        reposition: true,
        scalePhotos: true,
        scrolling: false,
        previous: '<i class="ace-icon fa fa-arrow-left"></i>',
        next: '<i class="ace-icon fa fa-arrow-right"></i>',
        close: '&times;',
        current: '{current} of {total}',
        maxWidth: '100%',
        maxHeight: '100%',
        onOpen: function () {
            $overflow = document.body.style.overflow;
            document.body.style.overflow = 'hidden';
        },
        onClosed: function () {
            document.body.style.overflow = $overflow;
        },
        onComplete: function () {
            $.colorbox.resize();
        }
    };

    $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
    $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon              

    $('#id-input-file-1, #id-input-file-2').ace_file_input({
        style: 'well',
        no_file: 'No File ...',
        no_icon: 'ace-icon fa fa-cloud-upload',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        droppable: true,
        thumbnail: true, //| true | large
        whitelist: 'gif|png|jpg|jpeg',
        blacklist: 'exe|php'
    }).on("change", function () {
        if ($('#id-input-file-1').val().length > 0) {
            var ext = $('#id-input-file-1').val().split('.').pop().toLowerCase();
            if ($.inArray(ext, ['txt', 'pdf', 'xls', 'doc', 'docx', 'xlsx', 'ppt', 'pptx']) == -1) {
                $("#UploadFile").addClass("disabled");
                alert('Tập tin không hợp lệ !');
            }
            else {
                $("#UploadFile").removeClass("disabled");
            }
        }
        if ($('#id-input-file-2').val().length > 0) {
            var ext2 = $('#id-input-file-2').val().split('.').pop().toLowerCase();
            if ($.inArray(ext2, ['jpg', 'png', 'gif']) == -1) {
                $("#UploadImg").addClass("disabled");
                alert('Tập tin không hợp lệ !');
            }
            else {
                $("#UploadImg").removeClass("disabled");
            }
        }
    });
});

function getAsset() {

    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/GetAsset",
        data: JSON.stringify({
            'Key': $('[id$=HID_ProjectKey]').val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $('#tblAsset > tbody').empty();
            var num = 1;
            for (var i = 0; i < msg.d.length; i++) {
                $('#tblAsset > tbody:last').append($(msg.d));
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function openAsset(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/OpenAsset",
        data: JSON.stringify({
            'Key': id
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $('#mAsset').modal('show');
            $("[id$=HID_AssetKey]").val(msg.d.AssetKey);
            $("#txt_BedRoom").val(msg.d.Room);
            $("#txt_AssetArea").val(msg.d.Area);
            $("#txt_AssetAmount").val(msg.d.Price);
            $("#txt_AssetAmount2").val(msg.d.PricePurchase);
            $("#txt_AssetDescription").val(msg.d.Description);
            $("#DDL_AssetCategory").val(msg.d.CategoryKey).trigger('change');
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delAsset(id) {
    if (confirm('Bạn có chắc xóa !.')) {
        $.ajax({
            type: "POST",
            url: "/SAL/ProjectEdit.aspx/DeleteAsset",
            data: JSON.stringify({
                'Key': $("[id$=HID_ProjectKey]").val(),
                'AssetKey': id,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message != '') {
                    Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
                } else {
                    getAsset();
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}
function getCategory() {
    $('#tblCategory').empty();
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/GetCategory",
        data: JSON.stringify({
            'Key': $("[id$=HID_ProjectKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            //fetch table
            for (var i = 0; i < msg.d.length; i++) {
                var html = "<div class='profile-info-row' id='" + msg.d[i].CategoryKey + "'>";
                html += "<div class='profile-info-name'>Loại căn </div><div class='profile-info-value'>" + msg.d[i].CategoryName;
                html += "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteCategory' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                html += "</div>";
                $('#tblCategory').append(html);
            }
            //fetch dropdownlist
            var dropdown = $("[id$=DDL_AssetCategory]");
            dropdown.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
            $.each(msg.d, function () {
                var object = this;
                if (object !== '') {
                    dropdown.append($("<option></option>").val(object.CategoryKey).html(object.CategoryName));
                }
            });
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delCategory(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/DeleteCategory",
        data: JSON.stringify({
            'Key': $("[id$=HID_ProjectKey]").val(),
            'CategoryKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                getCategory();
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delFile(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/DeleteFile",
        data: JSON.stringify({
            'FileKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                location.reload(true);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getShare() {
    $('#tblShare').empty();
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/GetShare",
        data: JSON.stringify({
            'Key': $("[id$=HID_ProjectKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            for (var i = 0; i < msg.d.length; i++) {
                var html = "<div class='profile-info-row' id='" + msg.d[i].Value + "'>";
                html += "<div class='profile-info-name'>Loại căn </div><div class='profile-info-value'>" + msg.d[i].Text;
                html += "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                html += "</div>";
                $('#tblShare').append(html);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delShare(id) {
    $.ajax({
        type: 'POST',
        url: 'ProjectEdit.aspx/DeleteShare',
        data: JSON.stringify({
            'AutoKey': id
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                getShare();
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delImg(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/DeleteFile",
        data: JSON.stringify({
            'FileKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                $("ul #" + id + "").remove();
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function saveFee() {
    $.ajax({
        type: 'POST',
        url: '/SAL/ProjectEdit.aspx/SaveFee',
        data: JSON.stringify({
            'Key': $("[id$=HID_ProjectKey]").val(),
            'Name': $("#txt_Name").val(),
            'Money': $("#txt_Money").val(),
            'Description': $("#txt_FeeDescription").val(),
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == '') {
                getFee();
            }
            else {
                Page.showPopupMessage("Lỗi lưu thông tin !", msg.d.Message);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delFee(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/DeleteFee",
        data: JSON.stringify({
            'AutoKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                getFee();
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getFee() {
    $.ajax({
        type: "POST",
        url: "/SAL/ProjectEdit.aspx/GetFee",
        data: JSON.stringify({
            'Key': $("[id$=HID_ProjectKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $('#tblFee').empty();
            $('#tblFee').append($(msg.d));
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}