﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProjectList.aspx.cs" Inherits="WebApp.SAL.ProjectList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        sup {
            vertical-align: sup;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Dự án
                <span class="tools pull-right">
                    <a href="#mCreateNew" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <!--Search-->
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="closeLeft">&times;</button>
                    <h4 class="modal-title blue">Điều kiện tìm kiếm</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_SearchProvince" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tỉnh thành--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_SearchDistrict" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Quận huyện--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Category" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <input id="txt_BedRoom" class="form-control" type="text" placeholder="Số phòng ngủ" />
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Price" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Khoảng giá bán--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Price2" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Khoảng giá thuê--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search-plus"></i>
                        Tìm
                    </button>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade" id="mCreateNew" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Khởi tạo thông tin dự án</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tên dư án</label>
                                    <div class="col-sm-8">
                                        <input id="txt_ProjectName" runat="server" class="form-control" placeholder="Nhập text" type="text" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tỉnh/ Thành phố</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="DDL_Province" runat="server" CssClass="select2" required></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Quận/ Huyện</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="DDL_District" runat="server" CssClass="select2" required></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Số, đường</label>
                                    <div class="col-sm-8">
                                        <input type="text" runat="server" class="form-control" id="txt_Address" placeholder="Nhập text" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Dự án làm mô hình biểu tượng</label>
                                    <div class="col-sm-8">
                                        <input name="switch-field-1" class="ace ace-switch ace-switch-6" type="checkbox" id="chkPrimary" />
                                        <span class="lbl"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Loại dự án</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="DDL_Table" runat="server" CssClass="form-control" required>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnCreated">
                        <i class="ace-icon fa fa-plus"></i>
                        Tiếp tục
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/SAL/ProjectList.js"></script>
</asp:Content>
