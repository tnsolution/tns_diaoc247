﻿var fullAddress = "", isPrimary = 0, Loaigia = 0;
$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(document).ready(function () {
    $(".select2").select2({ width: "100%" });
    $('.modal.aside').ace_aside();
    //--------------------------End Layout
    $("[id$=chkPrimary]").change(function () {
        if ($(this).is(':checked')) { isPrimary = 1; }
    });

    $("#tblData tbody").on("click", "td:not(:last-child) ", function () {
        var trid = $(this).closest('tr').attr('id');
        window.location = "ProjectView.aspx?ID=" + trid;
    });
    $('#tblData tbody').on('click', '.show-details-btn', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).closest('tr').next().hasClass("open")) {
            var id = $(this).closest('tr').attr("id");
            var room = $("#txt_BedRoom").val();
            var price = $("[id$=DDL_Price]").val();
            var category = $("[id$=DDL_Category]").val();
            if (category == null)
                category = 0;
            if (price == null)
                price = "";
            $.ajax({
                type: "POST",
                url: "/SAL/ProjectList.aspx/GetDetail",
                data: JSON.stringify({
                    "ProjectKey": id,
                    "Category": category,
                    "Room": room,
                    "Price": price,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (r) {
                    $('div[project=' + id + ']').empty();
                    $('div[project=' + id + ']').append($(r.d));
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }

        $(this).closest('tr').next().toggleClass('open');
        $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
    });

    $('[id$=DDL_SearchProvince]').on('change', function () {
        $.ajax({
            type: 'POST',
            url: '/ajax.aspx/GetDistricts',
            data: JSON.stringify({
                ProvinceKey: $(this).val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (r) {
                var District = $("[id$=DDL_SearchDistrict]");
                District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                $.each(r.d, function () {
                    District.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            },
            error: function () {
                alert('Lỗi lấy dữ liệu tỉnh/thành phố!');
            },
            complete: function () {
            }
        });
    });
    $('[id$=DDL_Province]').on('change', function () {
        $.ajax({
            type: 'POST',
            url: '/ajax.aspx/GetDistricts',
            data: JSON.stringify({
                ProvinceKey: $(this).val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (r) {
                var District = $("[id$=DDL_District]");
                District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                $.each(r.d, function () {
                    District.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            },
            error: function () {
                alert('Lỗi lấy dữ liệu tỉnh/thành phố!');
            },
            complete: function () {
            }
        });
    });

    $("[id$=txt_Address]").blur(function () {
        fullAddress = $("[id$=txt_Address]").val() + ', ' + $('[id$=DDL_District] option:selected').text() + ', ' + $('[id$=DDL_Province] option:selected').text();
    });
    //-------------------------End Event

    $("[id$=DDL_Price2]").on('change', function () {
        Loaigia = 2;
    });

    $("[id$=DDL_Price]").on('change', function () {
        Loaigia = 1;
    });

    $("#btnCreated").click(function () {
        if (Page.validate()) {
            $.ajax({
                type: 'POST',
                url: '/SAL/ProjectEdit.aspx/SaveProject',
                data: JSON.stringify({
                    'Key': 0,
                    'ProjectName': $("[id$=txt_ProjectName]").val(),
                    'Investor': "",
                    'BasicPrice': 0,
                    'Street': $("[id$=txt_Address]").val(),
                    'Province': $("[id$=DDL_Province]").val(),
                    'District': $("[id$=DDL_District]").val(),
                    'IsPrimary': isPrimary,
                    'MainTable': $("[id$=DDL_Table]").val(),
                    'AddressFull': fullAddress,
                    'Content': '',
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message.length <= 0) {
                        window.location = "ProjectEdit.aspx?ID=" + msg.d.Result;
                    }
                    else {
                        Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                    }
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    $("#btnSearch").click(function () {
        var district = $("[id$=DDL_SearchDistrict]").val();
        var province = $("[id$=DDL_SearchProvince]").val();
        var room = $("#txt_BedRoom").val();
        var category = $("[id$=DDL_Category]").val();
        var timgia = 0;
        if (district == null)
            district = 0;
        if (province == null)
            province = 0;
        if (category == null)
            category = 0;

        if (Loaigia == 1)
            timgia = $("[id$=DDL_Price]").val();
        else
            timgia = $("[id$=DDL_Price2]").val();

        $.ajax({
            type: 'POST',
            url: '/SAL/ProjectList.aspx/SearchDetail',
            data: JSON.stringify({
                'Province': province,
                'District': district,
                'Category': category,
                'Room': room,
                'Price': timgia,
                'LoaiGia': Loaigia,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $('#tblData > tbody').empty();
                $('#tblData > tbody').append($(msg.d));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
});