﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProjectView.aspx.cs" Inherits="WebApp.SAL.ProjectView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết dự án
                <asp:Literal ID="txt_ProjectName" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a type="button" class="btn btn-white btn-default btn-bold" href="ProjectList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin cơ bản
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#fee">
                                <i class="blue ace-icon fa fa-product-hunt bigger-110"></i>
                                Chi phí
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#product">
                                <i class="blue ace-icon fa fa-product-hunt bigger-110"></i>
                                Sản phẩm 
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#folder">
                                <i class="blue ace-icon fa fa-folder bigger-110"></i>
                                Tập tin
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#picture">
                                <i class="blue ace-icon fa fa-folder bigger-110"></i>
                                Hình ảnh
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#resale">
                                <i class="blue ace-icon fa fa-circle-o bigger-110"></i>
                                Sản phẩm đang ký gửi
                                    <span class="badge badge-purple">
                                        <asp:Literal ID="txt_AmountProduct" runat="server"></asp:Literal>
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#gmap" id="showMap">
                                <i class="blue ace-icon fa fa-location-arrow bigger-110"></i>
                                Vị trí trên Gmap
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane active">
                            <div class="col-xs-12">
                                <asp:Literal ID="txt_Content" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div id="product" class="tab-pane">
                            <div class="col-xs-12">
                                <asp:Literal ID="Lit_Product" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div id="picture" class="tab-pane">
                            <div class="col-xs-12">
                                <div id="accordion2" class="accordion-style1 panel-group">
                                    <asp:Literal ID="Lit_ListPicture" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="folder" class="tab-pane">
                            <div class="col-xs-12">
                                <div id="accordion" class="accordion-style1 panel-group">
                                    <asp:Literal ID="Lit_Folder" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="resale" class="tab-pane">
                            <div class="col-xs-12" id="divResale">
                                <asp:Literal ID="Lit_Resale" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div id="gmap" class="tab-pane">
                            <div class="col-xs-12">
                                <div id="googleMap" style="width: 100%; height: 400px;"></div>
                            </div>
                        </div>
                        <div id="fee" class="tab-pane">
                            <div class="col-xs-12">
                                <asp:Literal ID="Lit_Fee" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3" style="display: none">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mView" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" id="PageHead">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue" id="assethead"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="assetInfo">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ProjectKey" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeFsZQuxAyCGnlcXLu5CyOfxwXfKtVxD0"></script>
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <asp:Literal ID="Lit_Script" runat="server"></asp:Literal>
    <script>        
        var map;
        var marker;
        var _usd = 0, _assetkey = 0;       //tinh gia usd
        $(document).on('change', '[type=checkbox]', function (e) {
            // code here
            if ($(this).is(':checked')) {
                $("div[view='usd']").show();
                $("div[view='vnd']").hide();
                _usd = 1;
            }
            else {
                $("div[view='vnd']").show();
                $("div[view='usd']").hide();
                _usd = 0;
            }
        });
        $(document).one('ajaxloadstart.page', function (e) {
            $('#colorbox, #cboxOverlay').remove();
        });
        $(function () {
            initMap();
            $(".iframe").colorbox({ iframe: true, width: "100%", height: "100%" });
            $("#tblExtraData tbody").on("click", "tr", function () {
                var trid = $(this).closest('tr').attr('id');
                var type = $(this).closest('tr').attr('fid');

                $.ajax({
                    type: "POST",
                    url: "/SAL/ProjectView.aspx/GetAssetInfo",
                    data: JSON.stringify({
                        "AssetKey": trid,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (r) {
                        $("#assetInfo").empty();
                        $("#assetInfo").append($(r.d.Result));
                        $("#assethead").empty();
                        $("#assethead").append($(r.d.Result2));

                        $("#mView").modal({
                            backdrop: true,
                            show: true
                        });
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });

                //window.location = "ProductView.aspx?ID=" + trid + "&Type=" + type;
            });
            $("#divResale").height($(document).height() / 1.5);
            $("#tblExtraData").tableHeadFixer();
            $("#btnEdit").click(function () {
                window.location = "ProjectEdit.aspx?ID=" + Page.getUrlParameter("ID");
            });
            $("#showMap").on('shown.bs.tab', function () {
                /* Trigger map resize event */
                google.maps.event.trigger(map, 'resize');
                map.setCenter(myCenter);
            });
            var $overflow = '';
            var colorbox_params = {
                rel: 'colorbox',
                reposition: true,
                scalePhotos: true,
                scrolling: false,
                previous: '<i class="ace-icon fa fa-arrow-left"></i>',
                next: '<i class="ace-icon fa fa-arrow-right"></i>',
                close: '&times;',
                current: '{current} of {total}',
                maxWidth: '100%',
                maxHeight: '100%',
                onOpen: function () {
                    $overflow = document.body.style.overflow;
                    document.body.style.overflow = 'hidden';
                },
                onClosed: function () {
                    document.body.style.overflow = $overflow;
                },
                onComplete: function () {
                    $.colorbox.resize();
                }
            };
            $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
            $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon
            $("#btnSearch").click(function () {
                var Category = $("[id$=DDL_Category]").val();
                var Purpose = $("[id$=DDL_Purpose]").val();
                var Room = $("[id$=DDL_Room]").val();
                if (Category == null)
                    Category = 0;
                if (Purpose == null)
                    Purpose = "";
                if (Room == null)
                    Room = "";

                $.ajax({
                    type: "POST",
                    url: "/SAL/ProjectView.aspx/SearchAsset",
                    data: JSON.stringify({
                        "Project": $("[id$=HID_ProjectKey]").val(),
                        "AssetID": $("#txt_Name").val(),
                        "Room": Room,
                        "Category": Category,
                        "Purpose": Purpose,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#tblExtraData tbody').empty();
                        $('#tblExtraData tbody').append($(msg.d));
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });      
        function initMap() {
            var mapProp = {
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoom: 16,
            };
            map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }
    </script>
</asp:Content>
