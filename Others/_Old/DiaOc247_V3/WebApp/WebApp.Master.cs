﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class WebApp : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AutoNotified();
            AutoAlert();
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");               
            }
        }

        void AutoAlert()
        {
            DateTime zDateNow = DateTime.Now;
            DateTime zDateEnd = new DateTime(zDateNow.Year, zDateNow.Month, zDateNow.Day, 17,0, 0);
            DateTime zDateNext = zDateEnd.AddMinutes(10);

            double zTimeOut = zDateEnd.Subtract(zDateNow).TotalMilliseconds;
            double zTimeOutNext = zDateNext.Subtract(zDateNow).TotalMilliseconds;

            if (zTimeOut < 0)
            {
                zTimeOut = 0;
            }

            HiddenField3.Value = zTimeOut.ToString();
            HiddenField4.Value = zTimeOutNext.ToString();
        }
        void AutoNotified()
        {
            DateTime zDateNow = DateTime.Now;
            DateTime zDateEnd = new DateTime(zDateNow.Year, zDateNow.Month, zDateNow.Day, 18, 0, 0);
            DateTime zDateNext = zDateEnd.AddDays(1);

            double zTimeOut = zDateEnd.Subtract(zDateNow).TotalMilliseconds;
            double zTimeOutNext = zDateNext.Subtract(zDateNow).TotalMilliseconds;

            if (zTimeOut < 0)
            {
                zTimeOut = 0;
            }

            HiddenField1.Value = zTimeOut.ToString();
            HiddenField2.Value = zTimeOutNext.ToString();
        }
    }
}