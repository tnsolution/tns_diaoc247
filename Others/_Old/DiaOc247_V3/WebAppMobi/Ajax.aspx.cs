﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class Ajax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<ItemWeb> GetEmployees(int DepartmentKey)
        {
            List<ItemWeb> zDistrict = Tools.GetEmployee(DepartmentKey);
            return zDistrict;
        }
        [WebMethod]
        public static ItemWeb[] GetCategoryAsset(int ProjectKey)
        {
            List<ItemWeb> zList = new List<ItemWeb>();
            DataTable zTable = Project_Data.Category_Product(ProjectKey);
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb item = new ItemWeb();
                item.Text = zTable.Rows[i]["CategoryName"].ToString();
                item.Value = zTable.Rows[i]["CategoryKey"].ToString();
                zList.Add(item);
            }
            return zList.ToArray();
        }
    }
}