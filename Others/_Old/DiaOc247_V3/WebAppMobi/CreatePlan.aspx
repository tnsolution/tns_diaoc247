﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreatePlan.aspx.cs" Inherits="WebAppMobi.CreatePlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/themes/jquery.timepicker.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content-header">
            <h1>Kế hoạch ngày kế tiếp
            </h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="box box-success flat">
                    <div class="box-header with-border" style="padding: 0px !important;">
                        <!-- /.box-tools -->
                        <div id="datepairExample">
                            <input type="text" class="form-control flat time start" placeholder="-Từ giờ-" id="txt_Fromhours" style="float: left; width: 50%" />
                            <input type="text" class="form-control flat time end" placeholder="-Đến giờ-" id="txt_Tohours" style="float: right; width: 50%" />
                        </div>
                        <div class="input-group flat">
                            <input type="text" class="form-control flat" placeholder="Nội dung" id="txt_Content" />
                            <span class="input-group-btn flat">
                                <button type="button" class="btn btn-info btn-flat" id="addrow">Thêm !</button>
                            </span>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="divdata">
                            <asp:Literal ID="LiteralEditTable" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button id="btnSaveTimeSheet" class="btn btn-warning pull-right" data-dismiss="modal">Lưu</button>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HFTimeKey" runat="server" Value="0" />
    <script type="text/javascript" src="/themes/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/themes/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/themes/datepair.js"></script>
    <script type="text/javascript" src="/themes/jquery.datepair.js"></script>
    <script>
        $(document).ready(function () {
            $('#txt_Fromhours').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });
            $('#txt_Tohours').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });

            $('#datepairExample').datepair({ 'defaultTimeDelta': 1800000 });

            $('#tableEdit').editableTableWidget();

            $('#addrow').click(function () {
                var num = $('#tableEdit tr').length;

                $('#tableEdit tbody').append('<tr><td tabindex=1>' + (num++) + '</td><td tabindex=1>' + $('#txt_Fromhours').val() + '-' + $('#txt_Tohours').val() + '</td><td tabindex=1>' + $('#txt_Content').val() + '</td></tr>');

                $('#txt_Fromhours').val("");
                $('#txt_Tohours').val("");
                $('#txt_Content').val("");
            });
            $("#btnSaveTimeSheet").click(function () {
                var total = $('#tableEdit tbody tr').length;
                var num = 0;
                var tabledata = '';
                var id = $('[id$=HFTimeKey]').val();

                $('#tableEdit tbody tr').each(function () {
                    num++;
                    if (num == total) {
                        $(this).find('td').each(function () {
                            tabledata += $(this).html() + ',';
                        });
                    }
                    else {
                        $(this).find('td').each(function () {
                            tabledata += $(this).html() + ',';
                        });
                        tabledata += ';';
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: 'CreatePlan.aspx/SaveData',
                    data: JSON.stringify({
                        'tabledata': tabledata,
                        'id': id,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            alert("Kế hoạch bạn sẽ hiển thị trong ngày kế tiếp");
                            window.location.href = "DashBroad.aspx";
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    error: function () {
                        //alert('Lỗi xin liên hệ Admin !');
                    },
                    complete: function () {
                        window.location.href = "DashBroad.aspx";
                    }
                });
            });
        });
    </script>
</asp:Content>
