﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class CreatePlan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Init_TableTime();
            }
        }

        void Init_TableTime()
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-striped table-bordered' id='tableEdit'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th class='clsNo'>STT</th>");
            zSb.AppendLine("        <th class='clsTime'>Thời gian</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("    <tbody>");
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            LiteralEditTable.Text = zSb.ToString();
        }
        [WebMethod]
        public static string SaveData(string tabledata, string id)
        {
            try
            {
                DateTime NextDay = new DateTime();
                if ((DateTime.Now.DayOfWeek == DayOfWeek.Saturday) ||
                    (DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                    NextDay = DateTime.Now.AddDays(2).Date;
                else
                    NextDay = DateTime.Now.AddDays(1).Date;

                Schedule_Info zInfo = new Schedule_Info();
                if (id.ToInt() == 0)
                {
                    #region [Lưu mới]
                    string EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                    string DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                    string Name = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                    string SQL = "";

                    string Start = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
                    string End = DateTime.Now.AddDays(2).ToString("yyyy/MM/dd");

                    SQL += @"
Declare @ID INT;
INSERT INTO HRM_Schedule (EmployeeKey, DepartmentKey, ScheduleDate, Title, [Start], [End], AllDay, CreatedBy, CreatedName, CreatedDate, ModifiedDate, ModifiedBy, ModifiedName) 
VALUES (" + EmployeeKey + "," + DepartmentKey + ",'" + NextDay + "',N'" + Name + "','" + Start + "','" + End + "',1," + EmployeeKey + ",N'" + Name + "', GETDATE(),GETDATE()," + EmployeeKey + ",N'" + Name + "')";

                    SQL += " SET @ID = (SELECT TimeKey FROM HRM_Schedule WHERE TimeKey = SCOPE_IDENTITY())";

                    string[] row = tabledata.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string item in row)
                    {
                        string[] col = item.Split(',');

                        SQL += @" 
INSERT INTO HRM_ScheduleDetail (TimeKey, DepartmentKey, EmployeeKey, Title, [Description], [Start], [End], Allday, CreatedBy, CreatedName, CreatedDate, ModifiedDate, ModifiedBy, ModifiedName) 
VALUES ( @ID, " + DepartmentKey + "," + EmployeeKey + ",N'" + col[1] + "',N'" + col[2] + "','" + Start + "','" + End + "',1," + EmployeeKey + ",N'" + Name + "', GETDATE(),GETDATE()," + EmployeeKey + ",N'" + Name + "') \r\n";
                    }

                    string Message = CustomInsert.Exe(SQL).Message;
                    if (Message != string.Empty)
                        return "Lỗi khi lưu data !";

                    return "OK";
                    #endregion
                }
                else
                {
                    #region [Cập nhật đã có]
                    //string Date = DateTime.Now.ToString("yyyy-MM-dd");
                    string EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                    string DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                    string Name = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                    string SQL = "";
                    string Start = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
                    string End = DateTime.Now.AddDays(2).ToString("yyyy/MM/dd");

                    SQL += @"DELETE HRM_ScheduleDetail WHERE TimeKey = " + id;
                    SQL += " UPDATE HRM_Schedule SET ModifiedDate = GETDATE(), ModifiedBy = " + EmployeeKey + ", ModifiedName = N'" + Name + "' WHERE TimeKey = " + id + " \r\n";

                    string[] row = tabledata.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string item in row)
                    {
                        string[] col = item.Split(',');

                        SQL += @" 
INSERT INTO HRM_ScheduleDetail (TimeKey, DepartmentKey, EmployeeKey, Title, [Description], [Start], [End], Allday, CreatedBy, CreatedName, CreatedDate, ModifiedDate, ModifiedBy, ModifiedName) 
VALUES ( @ID, " + DepartmentKey + "," + EmployeeKey + ",N'" + col[1] + "',N'" + col[2] + "','" + Start + "','" + End + "',true," + EmployeeKey + ",N'" + Name + "', GETDATE(),GETDATE()," + EmployeeKey + ",N'" + Name + "') \r\n";
                    }

                    string Message = CustomInsert.Exe(SQL).Message;
                    if (zInfo.Message != string.Empty)
                        return "Lỗi khi update data !";

                    return "OK";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                return "Có lỗi khi lưu báo cáo (try catch) !";
            }
        }
    }
}