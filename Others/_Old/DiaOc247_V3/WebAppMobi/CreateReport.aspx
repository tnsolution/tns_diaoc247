﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreateReport.aspx.cs" Inherits="WebAppMobi.CreateReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="box box-success flat">
                    <div class="box-header with-border" style="padding: 0px !important;">
                        <asp:Literal ID="LitTitleReport" runat="server"></asp:Literal>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="divdata">
                            <asp:Literal ID="LitDataReport" runat="server"></asp:Literal>
                        </div>
                        <textarea id="txt_ReportNote" cols="20" rows="4" class="form-control" placeholder="Ghi chú" runat="server"></textarea>
                    </div>
                    <div class="box-footer">
                        <button id="btnReport" class="btn btn-warning pull-right" data-dismiss="modal">Gửi & Đóng</button>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ReportKey" runat="server" Value="0" />
    <script type="text/javascript" src="/themes/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/themes/numeric-input-example.js"></script>
    <script>
        $(document).ready(function () {
            $('#reportData').editableTableWidget().numericInputExample().find('td:nth-child(2)').focus(function () { $(this).select(); });

            $('#btnReport').click(function () {
                var total = $('#reportData tbody tr').length;
                var num = 0;
                var tabledata = '';
                var note = $('[id$=txt_ReportNote]').val();
                $('#reportData tbody tr').each(function () {
                    num++;
                    if (num == total) {
                        $(this).find('td').each(function () {
                            tabledata += $(this).html() + ',';
                        });
                    }
                    else {
                        $(this).find('td').each(function () {
                            tabledata += $(this).html() + ',';
                        });
                        tabledata += ';';
                    }
                });
                //if (confirm("Báo cáo gửi sẽ không chỉnh sửa được")) {
                    $.ajax({
                        type: 'POST',
                        url: 'CreateReport.aspx/SendReport',
                        data: JSON.stringify({
                            'Report': $("[id$=HID_ReportKey]").val(),
                            'tabledata': tabledata,
                            'note': note,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d == 'OK') {
                                window.location.href = "DashBroad.aspx";
                            }
                            else {
                                alert(msg.d);
                            }
                        },
                        error: function () {
                            //alert('Lỗi xin liên hệ Admin !');
                        },
                        complete: function () {
                            window.location.href = "DashBroad.aspx";
                        }
                    });
                //}
            });
        });
    </script>
</asp:Content>
