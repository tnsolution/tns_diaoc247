﻿using Lib.CRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class Customer : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                Tools.DropDown_DDL(DDL_Project, @"SELECT ProjectKey, ProjectName FROM PUL_Project A", false);
                //Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);                             
            }
        }

        [WebMethod]
        public static ItemResult Search(string CustomerName, string Room, string Project, string Category)
        {
            StringBuilder stringBuilder = new StringBuilder();
            List<Customer_ItemMobi> list = new List<Customer_ItemMobi>();
            ItemResult itemResult = new ItemResult();
            int num = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            bool flag = num == 0 || num == 1;
            if (flag)
            {
                list = Customer_Data.Search_Mobile(0, 0, CustomerName, string.Empty, string.Empty, Room, Project, Category);
            }
            bool flag2 = num >= 2;
            if (flag2)
            {
                list = Customer_Data.Search_Mobile(department, employee, CustomerName, string.Empty, string.Empty, Room, Project, Category);
            }
            foreach (Customer_ItemMobi current in list)
            {
                stringBuilder.AppendLine(" <a loading='yes' href='CustomerDetail.aspx?ID=" + current.CustomerKey + "'>");
                stringBuilder.AppendLine("   <table class='table table-hover table-bordered' style='margin-bottom:0px'>");
                stringBuilder.AppendLine("       <tr>");
                stringBuilder.AppendLine("           <td><b>Tên khách hàng: </b>" + current.CustomerName + "<br />");
                stringBuilder.AppendLine("               <b>Tiềm năng: </b><span style='color: #d0021b; font-weight: bolder'>" + current.Status + "</span><br />");
                stringBuilder.AppendLine("               <b>Dự án: </b><span style='color: #d0021b; font-weight: bolder'>" + current.ProjectName + "</span><br />");
                stringBuilder.AppendLine("               <b>Loại căn: </b><span style='color: #d0021b; font-weight: bolder'>" + current.CategoryName + "</span><br />");
                stringBuilder.AppendLine("               <b>Số PN: </b><span style='color: #d0021b; font-weight: bolder'>" + current.Room + "</span>");
                stringBuilder.AppendLine(string.Concat(new string[]{"               <a href='sms:",current.Phone1,"' class='btn btn-primary pull-right btn-flat'>Nhắn tin</a><a href='tel: '",current.Phone1,"' class='btn btn-primary pull-right btn-flat' style='margin-right:2px;'>Gọi khách</a>"
                }));
                stringBuilder.AppendLine("           </td>");
                stringBuilder.AppendLine("       </tr>");
                stringBuilder.AppendLine("   </table>");
                stringBuilder.AppendLine("</a>");
            }
            itemResult.Result = stringBuilder.ToString();
            return itemResult;
        }

    }
}