﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CustomerChart.aspx.cs" Inherits="WebAppMobi.CustomerChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content-header">
            <h1>Khách
                    <small>
                        <asp:Literal ID="Lit_TimeText" runat="server"></asp:Literal></small>
                <span class="box-tools btn-group pull-right">
                    <a loading="yes" class="btn btn-default btn-sm" id="ViewPrevious"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="true">
                        Chọn xem
                    <span class="fa fa-caret-down"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a loading="yes" href="#" id="ViewYear">Năm</a></li>
                        <li><a loading="yes" href="#" id="ViewMonth">Tháng</a></li>
                    </ul>
                    <a loading="yes" class="btn btn-default btn-sm" id="ViewNext"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </span>
            </h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="myChart1"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HiddenFromDate" runat="server" Value="" />
    <asp:HiddenField ID="HiddenToDate" runat="server" Value="" />
    <asp:HiddenField ID="Hidden_ViewTime" runat="server" Value="0" />
    <asp:HiddenField ID="Hidden_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnActionViewTime" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewTime_Click" />
    <asp:Button ID="btnActionViewNextPrev" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewNextPrev_Click" />
    <asp:Literal ID="LitDataChart" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(-1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });

            $("#ViewNext").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });

            $("#ViewYear").click(function () {
                $("[id$=Hidden_ViewTime]").val(1);
                $("[id$=btnActionViewTime]").trigger("click");
            });

            $("#ViewMonth").click(function () {
                $("[id$=Hidden_ViewTime]").val(2);
                $("[id$=btnActionViewTime]").trigger("click");
            });
        });
        $(window).scroll(function () {
            //var scrollPos = $(window).scrollTop();
            //if (scrollPos > 0) {
            //    $("#navtop").fadeOut();
            //}
            //else {
            //    $("#navtop").fadeIn();
            //}
        });
        function ViewActivity() {
            // Type 1 Year, 2 Month, 3 Week


        }
        function FormatMoney(num) {
            var str = num.toString().replace("$", "").replace(/ /g, ''),
            parts = false,
            output = [],
            i = 1,
            formatted = null;

            if (str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] != ",") {
                    output.push(str[j]);
                    if (i % 3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");

            return (formatted);
        };
    </script>    
    <script>
        var ctx1 = document.getElementById("myChart1");

        var myChart1 = new Chart(ctx1, {
            type: 'doughnut',
            data: areaChartData
        });

        // Define a plugin to provide data labels
        Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 16;
                            var fontStyle = 'normal';
                            var fontFamily = 'Helvetica Neue';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });
    </script>
</asp:Content>
