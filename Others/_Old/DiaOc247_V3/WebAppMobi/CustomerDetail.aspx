﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CustomerDetail.aspx.cs" Inherits="WebAppMobi.CustomerDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }

        .money {
            color: #dd4b39;
            font-weight: bold;
        }

        table > tbody > tr > td {
            white-space: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success flat">
                        <div class="box-header">
                            <a href="javascript: history.go(-1);" class="btn btn-default"><i class="ion-arrow-left-b"></i>&nbsp Quay về</a>
                        </div>
                        <div class="box-body">
                            <div id="divdata">
                                <asp:Literal ID="LiteralData" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div class="box-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Nội dung chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <textarea id="txt_Contents" rows="5" cols="20" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveCare" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Thêm
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_CustomerKey" runat="server" />
    <asp:HiddenField ID="HID_ConsentKey" runat="server" />
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <script>
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        $(function () {
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('#back-to-top').tooltip('show');
            $("#btnSaveCare").click(function () { SaveCare(); });
        });
        function OpenModal(id) {
            $("[id$=HID_ConsentKey]").val(id);
            $('#myModal').modal({
                backdrop: true,
                show: true
            });
        }
        function SaveCare() {
            $.ajax({
                type: "POST",
                url: "/CustomerDetail.aspx/SaveCare",
                data: JSON.stringify({
                    "CustomerKey": $("[id$=HID_CustomerKey]").val(),
                    "ConsentKey": $("[id$=HID_ConsentKey]").val(),
                    "Content": $("[id$=txt_Contents]").val(),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        Page.showPopupMessage("Lỗi !", msg.d.Message);
                    } else {
                        location.reload();
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    </script>
</asp:Content>
