﻿using Lib.CRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class CustomerDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_CustomerKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        // WebAppMobi.CustomerDetail
        void LoadInfo()
        {
            StringBuilder stringBuilder = new StringBuilder();
            Customer_Info customer_Info = new Customer_Info(HID_CustomerKey.Value.ToInt());
            stringBuilder.AppendLine("          <div class='table-responsive'>");
            stringBuilder.AppendLine("            <table class='table'>");
            stringBuilder.AppendLine("              <tbody><tr><th colspan='2' style='background: #f4f4f4;'>Thông tin cơ bản</th><tr><tr>");
            stringBuilder.AppendLine("                <th style='width:30%'>Họ tên:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.CustomerName + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Số điện thoại :</th>");
            stringBuilder.AppendLine(string.Concat(new string[]
            {
        "                <td><a href='tel:",
        customer_Info.Phone1,
        "'>",
        customer_Info.Phone1,
        "</a><br /><a href='tel:",
        customer_Info.Phone2,
        "'>",
        customer_Info.Phone2,
        "</a><br /></td>"
            }));
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Email:</th>");
            stringBuilder.AppendLine(string.Concat(new string[]
            {
        "                <td>",
        customer_Info.Email1,
        "<br />",
        customer_Info.Email2,
        "</td>"
            }));
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Mức độ quan tâm:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.StatusName + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Ngày sinh:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.Birthday.ToString("dd/MM/yyyy") + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Nguồn khách hàng:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.CustomerSource + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Chi tiết nguồn:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.SourceNote + "</td>");
            stringBuilder.AppendLine("              </tr>");
            bool flag = customer_Info.HasTrade == 1;
            if (flag)
            {
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Số CMND/Passport</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CardID + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Ngày cấp</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CardDate.ToString("dd/MM/yyyy") + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Nơi cấp</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CardPlace + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Địa chỉ thường trú</th>");
                stringBuilder.AppendLine("                <td style='word-wrap: break-word'>" + customer_Info.Address1 + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Địa chỉ liên lạc</th>");
                stringBuilder.AppendLine("                <td style='word-wrap: break-word'>" + customer_Info.Address2 + "</td>");
                stringBuilder.AppendLine("              </tr>");
            }
            bool flag2 = customer_Info.CategoryKey == 1;
            if (flag2)
            {
                stringBuilder.AppendLine("              <tr><th colspan='2'>Thông tin doanh nghiệp</th><tr><tr>");
                stringBuilder.AppendLine("                <th>Tên công ty</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CompanyName + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Mã số thuế</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.TaxCode + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr>");
                stringBuilder.AppendLine("                <th>Địa chỉ liên lạc</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.Address3 + "</td>");
                stringBuilder.AppendLine("              </tr>");
            }
            List<ItemConsent> list = Consents_Data.List(HID_CustomerKey.Value.ToInt());
            int num;
            for (int i = 0; i < list.Count; i = num + 1)
            {
                stringBuilder.AppendLine(string.Concat(new string[]
                {
            "<tr><th colspan='2' style='background: #f4f4f4;'><span style='float:left'>Quan tâm: ",
            list[i].Project,
            "</span><a href='#' onclick='OpenModal(",
            list[i].RecordKey,
            "); return false;' style='float:right'>Chăm sóc <span class='fa fa-plus-square-o'></span></a></th><tr>"
                }));
                stringBuilder.AppendLine("<tr>");
                stringBuilder.AppendLine("    <td>Nhu cầu</td><td>" + list[i].Want + "</td>");
                stringBuilder.AppendLine("</tr>");
                stringBuilder.AppendLine("<tr>");
                stringBuilder.AppendLine("    <td>Loại</td><td>" + list[i].Category + "</td>");
                stringBuilder.AppendLine("</tr>");
                stringBuilder.AppendLine("<tr>");
                stringBuilder.AppendLine("    <td>Phòng</td><td>" + list[i].Room + "</td>");
                stringBuilder.AppendLine("</tr>");
                stringBuilder.AppendLine("<tr>");
                stringBuilder.AppendLine("    <td>Nội thất</td><td>" + list[i].Furniture + "</td>");
                stringBuilder.AppendLine("</tr>");
                stringBuilder.AppendLine("<tr>");
                stringBuilder.AppendLine("    <td>Giá</td><td>" + list[i].Price.ToDoubleString() + "</td>");
                stringBuilder.AppendLine("</tr>");
                DataTable dataTable = Care_Data.List(HID_CustomerKey.Value.ToInt(), list[i].RecordKey.ToInt());
                bool flag3 = dataTable.Rows.Count > 0;
                if (flag3)
                {
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        stringBuilder.AppendLine("<tr>");
                        stringBuilder.AppendLine(string.Concat(new string[]
                        {
                    "       <td colspan=2>",
                    dataRow["CreatedBy"].ToString(),
                    " | ",
                    dataRow["CreatedDate"].ToDateString(),
                    " </td>"
                        }));
                        stringBuilder.AppendLine("</tr>");
                        stringBuilder.AppendLine("<tr>");
                        stringBuilder.AppendLine("        <td colspan=2 style='word-wrap: break-word'> " + dataRow["ContentDetail"].ToString() + "</td>");
                        stringBuilder.AppendLine("</tr>");
                    }
                }
                num = i;
            }
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <th>Ghi chú</th>");
            stringBuilder.AppendLine("                <td style='word-wrap: break-word'></td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr>");
            stringBuilder.AppendLine("                <td colspan='2' style='word-wrap: break-word'>" + customer_Info.Description + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine(string.Concat(new string[]
            {
        "                <tr><th colspan='2' style='background: #f4f4f4; text-align:center'> <a class='btn btn-info' href='tel:",
        customer_Info.Phone1,
        "'>Gọi khách</a> <a class='btn btn-info' href='sms:",
        customer_Info.Phone1,
        "'>Nhắn tin</a></th><tr>"
            }));
            stringBuilder.AppendLine("            </tbody></table>");
            stringBuilder.AppendLine("          </div>");
            this.LiteralData.Text = stringBuilder.ToString();
        }

        [WebMethod]
        public static ItemResult SaveCare(int CustomerKey, int ConsentKey, string Content)
        {
            Care_Info care_Info = new Care_Info();
            care_Info.ConsentKey = ConsentKey;
            care_Info.CustomerKey = CustomerKey;
            care_Info.ContentDetail = Content.Trim();
            care_Info.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            care_Info.ModifiedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            care_Info.Create();
            return new ItemResult
            {
                Message = care_Info.Message
            };
        }
    }
}