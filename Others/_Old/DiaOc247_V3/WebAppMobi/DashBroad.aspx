﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="DashBroad.aspx.cs" Inherits="WebAppMobi.DashBroad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .chart {
            height: 50vh;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content-header">
            <h1>Hoạt động   
                    <small>
                        <asp:Literal ID="Lit_TimeText" runat="server"></asp:Literal></small>
                <span class="box-tools btn-group pull-right">
                    <a loading="yes" class="btn btn-default btn-sm" id="ViewPrevious"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="true">
                        Chọn xem
                    <span class="fa fa-caret-down"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a loading="yes" href="#" id="ViewYear">Năm</a></li>
                        <li><a loading="yes" href="#" id="ViewMonth">Tháng</a></li>
                    </ul>
                    <a loading="yes" class="btn btn-default btn-sm" id="ViewNext"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </span>
            </h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <a href="Transaction.aspx?Type=1">
                            <span class="info-box-icon bg-yellow-gradient"><i class="ion ion-ios-folder-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Giao dịch chờ duyệt</span>
                                <span class="info-box-number">
                                    <asp:Literal ID="Lit_TradeNotApprove" runat="server"></asp:Literal></span>
                            </div>
                            <!-- /.info-box-content -->
                        </a>
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <a href="Transaction.aspx?Type=2">
                            <span class="info-box-icon bg-red-gradient"><i class="ion ion-ios-checkmark-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Giao dịch cần thanh toán</span>
                                <span class="info-box-number">
                                    <asp:Literal ID="Lit_TradeNotFinish" runat="server"></asp:Literal></span>
                            </div>
                        </a>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>             
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <a loading="yes" href="Income.aspx">
                            <span class="info-box-icon bg-green-gradient"><i class="ion-social-usd-outline"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Doanh thu</span>
                                <span class="info-box-number">
                                    <asp:Literal ID="Lit_TotalIncome" runat="server"></asp:Literal></span>
                            </div>
                        </a>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>                
            </div>
            <div class="row" style="display:none">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bảng xếp hạng doanh số</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HiddenFromDate" runat="server" Value="" />
    <asp:HiddenField ID="HiddenToDate" runat="server" Value="" />
    <asp:HiddenField ID="Hidden_ViewTime" runat="server" Value="0" />
    <asp:HiddenField ID="Hidden_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnActionViewTime" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewTime_Click" />
    <asp:Button ID="btnActionViewNextPrev" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewNextPrev_Click" />
    <asp:Literal ID="LitDataChart" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(-1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewYear").click(function () {
                $("[id$=Hidden_ViewTime]").val(1);
                $("[id$=btnActionViewTime]").trigger("click");
            });
            $("#ViewMonth").click(function () {
                $("[id$=Hidden_ViewTime]").val(2);
                $("[id$=btnActionViewTime]").trigger("click");
            });
        });      
       
        function FormatMoney(num) {
            var str = num.toString().replace("$", "").replace(/ /g, ''),
            parts = false,
            output = [],
            i = 1,
            formatted = null;

            if (str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] != ",") {
                    output.push(str[j]);
                    if (i % 3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");

            return (formatted);
        };
    </script>
    <script>
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: areaChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });
        Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    var meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index) {
                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgb(0, 0, 0)';

                            var fontSize = 16;
                            var fontStyle = 'normal';
                            var fontFamily = 'Tahoma';
                            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            var dataString = FormatMoney(dataset.data[index]).toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            var padding = 5;
                            var position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                        });
                    }
                });
            }
        });
    </script>
</asp:Content>
