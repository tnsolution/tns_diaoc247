﻿using Lib.CRM;
using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class DashBroad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                ViewCurrentMonth();
            }
        }
        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }
        void ViewActivity(DateTime FromDate, DateTime ToDate)
        {
            DataTable Table = new DataTable();

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            int TradeNeedApprove = 0;
            int TradeNeedCollect = 0;         
            double InCome = 0;

            //Table = Trade_Data.Chart_Mobile(0, 0, FromDate, ToDate);

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    //Table = Trade_Data.Chart_Mobile(0, 0, FromDate, ToDate);
                    TradeNeedApprove = HomePage.Count_Trade_NeedApproved(0, 0, FromDate, ToDate);
                    TradeNeedCollect = HomePage.Count_Trade_NeedCollect(0, 0, FromDate, ToDate);
                    InCome = HomePage.Sum_Trade(0, 0, FromDate, ToDate);
                    break;

                case 2:
                    //Table = Trade_Data.Chart_Mobile(Department, 0, FromDate, ToDate);
                    TradeNeedApprove = HomePage.Count_Trade_NeedApproved(Department, 0, FromDate, ToDate);
                    TradeNeedCollect = HomePage.Count_Trade_NeedCollect(Department, 0, FromDate, ToDate);
                    InCome = HomePage.Sum_Trade(Department, 0, FromDate, ToDate);
                    break;

                default:                    
                    TradeNeedApprove = HomePage.Count_Trade_NeedApproved(Department, Employee, FromDate, ToDate);
                    TradeNeedCollect = HomePage.Count_Trade_NeedCollect(Department, Employee, FromDate, ToDate);
                    InCome = HomePage.Sum_Trade(Department, Employee, FromDate, ToDate);
                    break;
            }

            Lit_TotalIncome.Text = InCome.ToString("n0");
            Lit_TradeNotFinish.Text = TradeNeedCollect.ToString();
            Lit_TradeNotApprove.Text = TradeNeedApprove.ToString();

            //StringBuilder zSb = new StringBuilder();
            //string zSblbl = "";
            //string zSbdataInCome = "";

            //for (int i = 0; i < Table.Rows.Count; i++)
            //{
            //    DataRow r = Table.Rows[i];
            //    zSblbl += "'" + r["EmployeeName"].ToString() + "',";
            //    zSbdataInCome += "'" + r["Money"].ToString() + "',";
            //}

            //zSb.AppendLine(" var areaChartData = {");
            //zSb.AppendLine("    labels: [" + zSblbl + "],");
            //zSb.AppendLine("    datasets: [{");
            //zSb.AppendLine("         label: '# VNĐ',");
            //zSb.AppendLine("         backgroundColor: 'rgba(60, 141, 188, 0.5)',");
            //zSb.AppendLine("         borderColor: 'rgba(60, 141, 188, 0.9)',");
            //zSb.AppendLine("         borderWidth: '1',");
            //zSb.AppendLine("         data: [" + zSbdataInCome.Remove(zSbdataInCome.LastIndexOf(",")) + "]");
            //zSb.AppendLine("    }]");
            //zSb.AppendLine(" };");

            //LitDataChart.Text = "<script>" + zSb.ToString() + "</script>";
        }
        void ViewCurrentMonth()
        {
            Lit_TimeText.Text = "Tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "2";
        }
        void ViewCurrentYear()
        {
            Lit_TimeText.Text = "Năm " + DateTime.Now.Year;

            DateTime FromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "1";
        }
        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                return;
            }
        }
        void ViewYear(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = ToDate.AddYears(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate.AddYears(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
        }
        //xem năm, hoặc tháng
        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewCurrentYear();
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }
        //xem nam, tháng trước, hoặc sau,
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewYear(ViewTimeNextPrev);
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }
    }
}