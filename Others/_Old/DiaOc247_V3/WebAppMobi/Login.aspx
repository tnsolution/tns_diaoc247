﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebAppMobi.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link href="themes/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link href="themes/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <!-- iCheck -->
    <link href="themes/plugins/iCheck/square/blue.css" rel="stylesheet" />
    <style>
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(/themes/Preloader_2.gif) center no-repeat #fff;
        }
    </style>
</head>
<body class="hold-transition login-page" style="background: rgba(167, 167, 167, 0);">
    <div class="login-box">
        <div class="login-logo">
            <a href="#">
                <img src="/_Resource/Img/Logo.png" style="width: 55%" /></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body" style="background: rgba(175, 175, 175, 0.42);">
            <p class="login-box-msg">...</p>
            <form id="form1" runat="server">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Tài khoản" id="txt_UserName" />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Mật khẩu" id="txt_Password" />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="button" class="btn btn-primary btn-block btn-flat" id="btnSubmit">Đăng nhập</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.social-auth-links -->
        </div>
    </div>
    <!-- jQuery 2.2.3 -->
    <script src="themes/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="themes/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script>
        var error_obj = null;
        var error_prefix = 'Phải nhập ';
        var error_message = '<div class="text-danger error">{msg}</div>';
        $(function () {
            //$(".se-pre-con").fadeOut("slow");
            $("input").on("keypress", function (e) {
                if (e.keyCode == 13) {
                    $("#btnSubmit").trigger("click")
                }
            });
            $('#btnSubmit').on('click', function () {
                var UserName = $('[id$=txt_UserName]').val();
                var Password = $('[id$=txt_Password]').val();
                if (UserName == '' || Password == '') {
                    alert('Bạn phải nhập tên tài khoản và mật khẩu');
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "Login.aspx/CheckLogin",
                    data: JSON.stringify({
                        "UserName": UserName,
                        "Password": Password,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        //$(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d == "OK")
                            window.location = "/Default.aspx";
                        else
                            alert(msg.d);
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>
</body>
</html>
