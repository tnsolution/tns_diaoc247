﻿using Lib.SYS;
using System;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }

        [WebMethod]
        public static string CheckLogin(string UserName, string Password)
        {
            ItemUser zResult = User_Data.CheckUser(UserName, Password);

            if (zResult.Message == "ERR")
            {
                switch (zResult.MessageCode)
                {
                    case "CheckUser_Error01":
                        return "Vui lòng kiểm tra Username và Password";

                    case "CheckUser_Error02":
                        return "User này chưa kích hoạt, vui lòng liên hệ Administrator";

                    case "CheckUser_Error03":
                        return "User này đã hết hạn, vui lòng liên hệ Administrator";

                    default:
                        return "Lỗi đăng nhập !";
                }
            }
            else
            {
                HttpCookie zCook = new HttpCookie("UserLog");
                zCook.Values["UnitLevel"] = zResult.UnitLevel;              
                zCook.Values["DepartmentKey"] = zResult.DepartmentKey.ToString();
                zCook.Values["EmployeeKey"] = zResult.EmployeeKey.ToString();
                zCook.Values["EmployeeName"] = HttpUtility.UrlEncode(zResult.EmployeeName);
                zCook.Values["UserKey"] = zResult.UserKey.ToString();
                zCook.Values["UserName"] = HttpUtility.UrlEncode(zResult.UserName);           
                zCook.Expires = DateTime.Now.AddHours(8);
                HttpContext.Current.Response.Cookies.Add(zCook);

                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    return "Error";
                return "OK";
            }
        }
    }
}