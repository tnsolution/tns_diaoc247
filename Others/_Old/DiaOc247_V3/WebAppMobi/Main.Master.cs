﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppMobi
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadUser();
        }

        void LoadUser()
        {
            StringBuilder zSbMenu = new StringBuilder();
            #region [UserName]
            zSbMenu.AppendLine("<li class='dropdown user user-menu'>");
            zSbMenu.AppendLine("    <a href = '#' class='dropdown-toggle' data-toggle='dropdown'>");
            zSbMenu.AppendLine("          <img src = '/themes/dist/img/avatar5.png' class='user-image' alt='User Image' />");
            zSbMenu.AppendLine("          <span class='hidden-xs'>" + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "</span>");
            zSbMenu.AppendLine("     </a>");

            zSbMenu.AppendLine("    <ul class='dropdown-menu'>");
            zSbMenu.AppendLine("        <li class='user-header'>");
            zSbMenu.AppendLine("            <img src='/themes/dist/img/avatar5.png' class='user-image' class='img-circle' alt='User Image'>");
            zSbMenu.AppendLine("            <p>");
            zSbMenu.AppendLine(HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + " </br> " + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["Position"]));
            zSbMenu.AppendLine("                <small>Thành viên DIAOC 247</small>");
            zSbMenu.AppendLine("            </p>");
            zSbMenu.AppendLine("        </li>");

            zSbMenu.AppendLine("       <li class='user-footer'>");
            zSbMenu.AppendLine("           <div class='pull-left'>");
            zSbMenu.AppendLine("           <a data-toggle='modal'  href='#mProfile' class='btn btn-default btn-flat'>Profile</a>");
            zSbMenu.AppendLine("           </div>");
            zSbMenu.AppendLine("           <div class='pull-right'>");
            zSbMenu.AppendLine("           <a href='/Login.aspx' class='btn btn-default btn-flat'>Sign out</a>");
            zSbMenu.AppendLine("          </div>");
            zSbMenu.AppendLine("        </li>");
            zSbMenu.AppendLine("    </ul>");
            zSbMenu.AppendLine("</li>");
            #endregion

            LitMenu.Text = zSbMenu.ToString();
        }
    }
}