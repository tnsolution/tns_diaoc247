﻿using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class Plan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");
                CheckRole();
                ViewCurrentDay();
            }
        }

        void ViewCurrentDay()
        {
            Lit_TimeText.Text = "Ngày " + DateTime.Now.Day;

            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewTarget(FromDate, ToDate);

            Hidden_ViewTime.Value = "3";
        }

        void ViewDay(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddDays(1);
                ToDate = ToDate.AddDays(1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Ngày " + FromDate.Day;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddDays(-1);
                ToDate = ToDate.AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Ngày " + FromDate.Day;
                return;
            }
        }

        void ViewTarget(DateTime FromDate, DateTime ToDate)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = int.Parse(DDL_Employee.SelectedValue);
            int Department = int.Parse(DDL_Department.SelectedValue);

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Employees_Data.List(Department, Employee);

            zSb.AppendLine("<table class='table table-bordered table-striped dataTable'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");
            foreach (DataRow r in zTable.Rows)
            {
                int Key = r["EmployeeKey"].ToInt();
                zSb.AppendLine("<tr><td style='border-top: 3px solid #00c0ef;'><b>" + r["LastName"].ToString() + " " + r["FirstName"].ToString() + "</b><br />" + r["DepartmentName"].ToString() + "<br /></td></tr>");
                zSb.AppendLine("<tr>");
                List<ScheduleItem> ListItem = ScheduleDetail_Data.GetSchedule_Mobile(FromDate, ToDate, Department, Key);
                zSb.AppendLine("<td>");

                for (int i = 0; i < ListItem.Count; i++)
                {
                    ScheduleItem item = ListItem[i];
                    zSb.AppendLine("<b>" + item.Title + "</b><br/>" + item.Description + "<br/>");
                }

                zSb.AppendLine("</td>");
                zSb.AppendLine("</tr>");
            }
            zSb.AppendLine("</table>");
            Literal_Target.Text = zSb.ToString();
        }

        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            ViewCurrentDay();
        }
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            ViewDay(ViewTimeNextPrev);
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
            DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = int.Parse(DDL_Employee.SelectedValue);
            int Department = int.Parse(DDL_Department.SelectedValue);

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Employees_Data.List(Department, Employee);

            zSb.AppendLine("<table class='table table-bordered table-striped dataTable'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr><td><b>" + r["LastName"].ToString() + " " + r["FirstName"].ToString() + "</b><br />" + r["DepartmentName"].ToString() + "<br /></td></tr>");

                zSb.AppendLine("<tr>");
                int Key = r["EmployeeKey"].ToInt();
                List<ScheduleItem> ListItem = ScheduleDetail_Data.GetSchedule_Mobile(FromDate, ToDate, Department, Key);
                zSb.AppendLine("<td>");

                for (int i = 0; i < ListItem.Count; i++)
                {
                    ScheduleItem item = ListItem[i];
                    zSb.AppendLine("<b>" + item.Title + "</b><br/>" + item.Description + "<br/>");
                }

                zSb.AppendLine("</td>");
                zSb.AppendLine("</tr>");
            }
            zSb.AppendLine("</table>");
            Literal_Target.Text = zSb.ToString();
        }

        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            if (UnitLevel >= 3)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking=2", false);
                DDL_Employee.SelectedValue = EmployeeKey.ToString();
                DDL_Department.Visible = false;
                DDL_Employee.Visible = false;
                btnView.Visible = false;
            }

            if (UnitLevel == 2)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey, false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);

                DDL_Department.SelectedValue = DepartmentKey.ToString();
                DDL_Employee.SelectedValue = EmployeeKey.ToString();

                DDL_Employee.Visible = true;
                DDL_Department.Visible = false;
            }

            if (UnitLevel == 0 || UnitLevel == 1)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments", false);

                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;
            }
        }
    }
}