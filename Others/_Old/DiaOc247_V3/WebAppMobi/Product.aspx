﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="WebAppMobi.Product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="box box-success flat">
                    <div class="box-header with-border" style="padding: 0px !important;">
                        <div style="width: 50%; float: left">
                            <asp:DropDownList ID="DDL_Project" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="Chọn dự án" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="width: 25%; float: left">
                            <asp:DropDownList ID="DDL_PriceType" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="" Text="Thuê/Bán" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="CN" Text="Bán"></asp:ListItem>
                                <asp:ListItem Value="CT" Text="Thuê"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="width: 25%; float: right">
                            <asp:DropDownList ID="DDL_Number" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="" Text="Chọn PN" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="width: 50%; float: left">
                            <asp:DropDownList ID="DDL_Category" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="Chọn loại sản phẩm" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="input-group flat" style="width: 50%; float: right">
                            <input type="text" class="form-control flat" placeholder="Mã sản phẩm" id="txt_Search" />
                            <span class="input-group-btn flat">
                                <button type="button" class="btn btn-info btn-flat" id="btnSearch" loading="yes">Tìm !</button>
                            </span>
                        </div>                      
                    </div>                  
                    <div class="box-body">
                        <div id="divdata">
                        </div>
                    </div>
                    <div class="box-footer">
                    </div>                   
                </div>
            </div>
        </div>
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <script>
        var sIndex = 1, offSet = 10;
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }

            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                sIndex++;
                SearchProduct();
            }
        });

        $(function () {
            $("[id$=DDL_Project]").on('change', function (e) {
                if (this.value != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/Ajax.aspx/GetCategoryAsset",
                        data: JSON.stringify({
                            "ProjectKey": this.value,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () { },
                        success: function (msg) {
                            var District = $("[id$=DDL_Category]");
                            District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                            $.each(msg.d, function () {
                                var object = this;
                                if (object !== '') {
                                    District.append($("<option></option>").val(object.Value).html(object.Text));
                                }
                            });
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            // scroll body to 0px on click
            $("#back-to-top").click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $("#back-to-top").tooltip('show');
            $("#btnSearch").click(function () {
                $('#divdata').empty();
                sIndex = 1;
                offSet = 10;
                SearchProduct();
            });
            SearchProduct();
        });
        function SearchProduct() {
            var project = $("[id$=DDL_Project]").val();
            var assetID = $("[id$=txt_Search]").val();
            var category = $("[id$=DDL_Category]").val();
            var bed = $("[id$=DDL_Number]").val();
            var pricetype = $("[id$=DDL_PriceType]").val();
            if (bed == null)
                bed = "";
            if (project == null)
                project = "0";
            if (category == null)
                category = "0";
            if (assetID == null)
                assetID = "";
            $.ajax({
                type: "POST",
                url: "/Product.aspx/Search",
                data: JSON.stringify({
                    Project: project,
                    Category: category,
                    Room: bed,
                    Asset: assetID,
                    Purpose: pricetype,
                    Index: sIndex,
                    Show: offSet,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (r) {
                    if (r.d.Result == 0) {
                        alert('Không còn data');
                        return;
                    }
                    $('#divdata').append($(r.d.Result));
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    </script>
</asp:Content>
