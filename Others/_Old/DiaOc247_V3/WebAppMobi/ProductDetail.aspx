﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="WebAppMobi.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/themes/plugins/Gallery-master/css/blueimp-gallery.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Carousel</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group-sm pull-left">
                        <a href="javascript: history.go(-1);" class="btn btn-default"><i class="ion-arrow-left-b"></i>&nbsp Quay về</a>
                    </div>                    
                </div>
            </div>
            <div id="blueimp-gallery-carousel" class="blueimp-gallery blueimp-gallery-carousel">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
            <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
        </div>
        <div class="box-footer">
        </div>
        <!-- /.box-body -->
    </div>

    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <script type="text/javascript" src="/themes/plugins/Gallery-master/js/blueimp-gallery.min.js"></script>
    <script>
        $(function () {
            $('img[draggable="false"]').click(function () {
                $('#links').trigger('click');
            });
        });

        blueimp.Gallery(document.getElementById('links').getElementsByTagName('a'), {
            container: '#blueimp-gallery-carousel',
            carousel: true
        });
        document.getElementById('links').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = { index: link, event: event },
                links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    </script>
</asp:Content>
