﻿using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                HID_AssetKey.Value = Request["ID"];
                HID_AssetType.Value = Request["Type"];

                LoadAsset();
            }
        }

        void LoadAsset()
        {
            int AssetKey = HID_AssetKey.Value.ToInt();
            string Type = HID_AssetType.Value;
            Product_Info zAsset = new Product_Info(AssetKey, Type);
            List<ItemDocument> zTable = Document_Data.List(135, AssetKey, Type);
            ItemAsset Item = zAsset.ItemAsset;
            LoadInfo(Item.AssetID, Item.CustomerName, 
                Item.PricePurchase_VND, Item.PriceRent_USD, Item.Status, Item.ProjectName, 
                Item.Address, Item.AreaWater, Item.AreaWall, Item.View, Item.Door, 
                Item.Furniture, Item.Description, Item.Phone1, Item.Room, Item.EmployeePhone,
                Item.EmployeeKey, Item.AssetKey, Item.AssetType, zTable);
        }

        void LoadInfo(string MaSP, string Tenchunha, string GiaBan, string GiaThue, string TinhTrang, string DuAn, string DiaChi,
            string DienTichthongthuy, string DienTichtimtuong, string View, string Cua, string NoiThat, string GhiChu, 
            string PhoneKhach, string PhongNgu, string PhoneNhanVien,
            string EmployeeKey, string AssetKey, string AssetType,
            List<ItemDocument> ListImg)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='form-group'>");

            zSb.AppendLine("<table style='width:100%'>");

            zSb.AppendLine("        <tr><td style='width:35%'>Chủ nhà: </td><td>" + Tenchunha + "</td></tr>");
            zSb.AppendLine("        <tr><td>Mã sản phẩm:</td><td>" + MaSP + "</td></tr>");
            zSb.AppendLine("        <tr><td>Tình trang:</td><td>" + TinhTrang + "</td></tr>");
            zSb.AppendLine("        <tr><td>Giá bán:</td><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + GiaBan + "</span></td></tr>");
            zSb.AppendLine("        <tr><td>Giá thuê:</td><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + GiaThue + "</span></td></tr>");
            zSb.AppendLine("        <tr><td>Dự án:</td><td>" + DuAn + "</td></tr>");
            zSb.AppendLine("        <tr><td>Diện tích thông thủy:</td><td>" + DienTichthongthuy + "</td></tr>");
            zSb.AppendLine("        <tr><td>Diện tích tim tường:</td><td>" + DienTichtimtuong + "</td></tr>");
            zSb.AppendLine("        <tr><td>Hướng cửa:</td><td>" + Cua + "</td></tr>");
            zSb.AppendLine("        <tr><td>Hướng View:</td><td>" + View + "</td></tr>");
            zSb.AppendLine("        <tr><td>Nội thất:</td><td>" + NoiThat + "</td></tr>");
            zSb.AppendLine("        <tr><td>Phòng ngủ:</td><td>" + PhongNgu + "</td></tr>");
            zSb.AppendLine("        <tr><td>Ghi chú:</td><td>" + GhiChu + "</td></tr>");

            zSb.AppendLine("</table>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("<div class='footer'>");

            bool flag= CheckViewPhone(EmployeeKey.ToInt(), AssetKey.ToInt(), AssetType);
            if (flag)
            {
                zSb.AppendLine("                <table class='table table-bordered' style='margin-bottom:0px !important;'>");
                zSb.AppendLine("                    <tr>");
                zSb.AppendLine("                        <td style='width: 50%'>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='tel:" + PhoneKhach + "'>");
                zSb.AppendLine("                                <i class='fa fa-phone'></i>&nbsp Gọi chủ nhà.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                        <td>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='sms:" + PhoneKhach + "'>");
                zSb.AppendLine("                                <i class='fa fa-send'></i>&nbsp Nhắn chủ nhà.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                    </tr>");
                zSb.AppendLine("                </table>");
            }
            else
            {
                zSb.AppendLine("                <table class='table table-bordered' style='margin-bottom:0px !important;'>");
                zSb.AppendLine("                    <tr>");
                zSb.AppendLine("                        <td style='width: 50%'>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='tel:" + PhoneNhanVien + "'>");
                zSb.AppendLine("                                <i class='fa fa-phone'></i>&nbsp Gọi chuyên viên.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                        <td>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='sms:" + PhoneNhanVien + "'>");
                zSb.AppendLine("                                <i class='fa fa-send'></i>&nbsp Nhắn chuyên viên.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                    </tr>");
                zSb.AppendLine("                </table>");
            }

            zSb.AppendLine("            </div>");
            zSb.AppendLine("<div id='links' style='display: none'>");

            if (ListImg.Count > 0)
            {
                foreach (ItemDocument r in ListImg)
                {
                    string url = "http://crm.diaoc247.vn/" + r.ImageUrl.ToString();
                    zSb.AppendLine("                <a href='" + url + "' title='" + r.ImageName.ToString() + "'><img width='96' height='72' src='" + url + "'></a>");
                }
            }
            else
            {
                string url = @"/_Resource/Img/noimage.png";
                zSb.AppendLine("                <a href='" + url + "' title='Chưa có hình'><img width='96' height='72' src='" + url + "'></a>");
            }
            zSb.AppendLine("            </div>");
            Lit_Info.Text = zSb.ToString();
        }

        bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Position = HttpContext.Current.Request.Cookies["UserLog"]["PositionKey"].ToInt();

            if (UnitLevel == 0 ||
                UnitLevel == 1 ||
                Position == 2)
            {
                return true;
            }

            if (CurrentAgent == OwnerAgent)
            {
                return true;
            }
            else
            {
                DataTable zTableSharePermition = Share_Permition_Data.List_ShareDepartment(AssetKey, Type);
                {
                    for (int i = 0; i < zTableSharePermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zTableSharePermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }       
    }
}