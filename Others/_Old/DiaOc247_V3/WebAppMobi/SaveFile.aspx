﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaveFile.aspx.cs" Inherits="WebAppMobi.SaveFile" %>

<%
    try
    {
        string directory = @"C:\inetpub\web\crm.247.v3\Upload\File\" + Request["type"] + "\\" + Request["key"] + "\\";
        HttpFileCollection files = HttpContext.Current.Request.Files;
        for (int index = 0; index < files.Count; index++)
        {

            HttpPostedFile uploadfile = files[index];
            string Name = uploadfile.FileName;
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(directory);
            if (!nDir.Exists)
            {
                nDir.Create();
            }
            uploadfile.SaveAs(directory + Name);

            string thumb = Name + "_thumb" + System.IO.Path.GetExtension(uploadfile.FileName);

            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(uploadfile.InputStream);
            System.Drawing.Image objImage = resizeImage(bitmap, new System.Drawing.Size(150, 150));
            objImage.Save(directory + thumb, System.Drawing.Imaging.ImageFormat.Jpeg);


        }

        HttpContext.Current.Response.Write(directory + "Upload successfully!");
    }
    catch (Exception ex)
    {
        HttpContext.Current.Response.Write(ex.ToString());
    }
%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>
