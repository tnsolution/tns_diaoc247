﻿using Lib.HRM;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class Target : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                CheckRole();
                ViewCurrentDay();
            }
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }
        void ViewCurrentMonth()
        {
            Lit_TimeText.Text = "Tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewTarget(FromDate, ToDate);

            Hidden_ViewTime.Value = "2";
        }
        void ViewCurrentYear()
        {
            Lit_TimeText.Text = "Năm " + DateTime.Now.Year;

            DateTime FromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewTarget(FromDate, ToDate);

            Hidden_ViewTime.Value = "1";
        }
        void ViewCurrentDay()
        {
            Lit_TimeText.Text = "Ngày " + DateTime.Now.Day;

            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewTarget(FromDate, ToDate);

            Hidden_ViewTime.Value = "3";
        }
        void ViewDay(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddDays(1);
                ToDate = ToDate.AddDays(1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Ngày " + FromDate.Day;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddDays(-1);
                ToDate = ToDate.AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Ngày " + FromDate.Day;
                return;
            }
        }
        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                return;
            }
        }
        void ViewYear(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = ToDate.AddYears(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate.AddYears(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewTarget(FromDate, ToDate);

                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
        }
        void ViewTarget(DateTime FromDate, DateTime ToDate)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = int.Parse(DDL_Employee.SelectedValue);
            int Department = int.Parse(DDL_Department.SelectedValue);

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Employees_Data.List(Department, Employee);

            zSb.AppendLine("<table class=''>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th style='width:70%'>Mục tiêu</th>");
            zSb.AppendLine("        <th>Kết quả</th>");
            zSb.AppendLine("        <th>Yêu cầu/ Tháng</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");
            foreach (DataRow r in zTable.Rows)
            {
                int Key = r["EmployeeKey"].ToInt();

                zSb.AppendLine("<tr><td colspan='3'><b>" + r["LastName"].ToString() + " " + r["FirstName"].ToString() + "</b><br />" + r["DepartmentName"].ToString() + "<br /></td></tr>");

                zSb.AppendLine("<tr>");
                DataTable zReport = Report_Rec_Data.ListMobile(Key, FromDate, ToDate);
                zSb.AppendLine("<td>");

                for (int i = 0; i < zReport.Rows.Count; i++)
                {
                    zSb.AppendLine(zReport.Rows[i]["CategoryName"].ToString() + "<br/>");
                }

                zSb.AppendLine("</td>");

                zSb.AppendLine("<td>");

                for (int i = 0; i < zReport.Rows.Count; i++)
                {
                    zSb.AppendLine(zReport.Rows[i]["Result"].ToString() + "<br/>");
                }

                zSb.AppendLine("</td>");

                zSb.AppendLine("<td>");

                for (int i = 0; i < zReport.Rows.Count; i++)
                {
                    zSb.AppendLine(zReport.Rows[i]["Require"].ToString() + "<br/>");
                }

                zSb.AppendLine("</td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr><td colspan='3'><hr/></td></tr>");
            }
            zSb.AppendLine("</table>");
            Literal_Target.Text = zSb.ToString();
        }
        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewCurrentYear();
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentDay();
                    break;
            }
        }
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewYear(ViewTimeNextPrev);
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewDay(ViewTimeNextPrev);
                    break;
            }
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
            DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = int.Parse(DDL_Employee.SelectedValue);
            int Department = int.Parse(DDL_Department.SelectedValue);

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Employees_Data.List(Department, Employee);

            zSb.AppendLine("<table class=''>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th style='width:70%'>Mục tiêu</th>");
            zSb.AppendLine("        <th>Kết quả</th>");
            zSb.AppendLine("        <th>Yêu cầu/ Tháng</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr><td colspan='3' style='border-top: 3px solid #00c0ef;'><b>" + r["LastName"].ToString() + " " + r["FirstName"].ToString() + "</b><br />" + r["DepartmentName"].ToString() + "<br /></td></tr>");

                zSb.AppendLine("<tr>");
                int Key = r["EmployeeKey"].ToInt();
                DataTable zReport = Report_Rec_Data.ListMobile(Key, FromDate, ToDate);
                zSb.AppendLine("<td>");

                for (int i = 0; i < zReport.Rows.Count; i++)
                {
                    zSb.AppendLine(zReport.Rows[i]["CategoryName"].ToString() + "<br/>");
                }

                zSb.AppendLine("</td>");

                zSb.AppendLine("<td>");

                for (int i = 0; i < zReport.Rows.Count; i++)
                {
                    zSb.AppendLine(zReport.Rows[i]["Result"].ToString() + "<br/>");
                }

                zSb.AppendLine("</td>");

                zSb.AppendLine("<td>");

                for (int i = 0; i < zReport.Rows.Count; i++)
                {
                    zSb.AppendLine(zReport.Rows[i]["Require"].ToString() + "<br/>");
                }

                zSb.AppendLine("</td>");
                zSb.AppendLine("</tr>");
                zSb.AppendLine("<tr><td colspan='3'><hr/></td></tr>");
            }
            zSb.AppendLine("</table>");
            Literal_Target.Text = zSb.ToString();
        }
        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            if (UnitLevel >= 3)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking=2", false);
                DDL_Employee.SelectedValue = EmployeeKey.ToString();
                DDL_Department.Visible = false;
                DDL_Employee.Visible = false;
                btnView.Visible = false;
            }

            if (UnitLevel == 2)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey, false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey, false);

                DDL_Department.SelectedValue = DepartmentKey.ToString();
                DDL_Employee.SelectedValue = EmployeeKey.ToString();

                DDL_Employee.Visible = true;
                DDL_Department.Visible = false;
            }

            if (UnitLevel == 0 || UnitLevel == 1)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments", false);

                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;
            }
        }
    }
}