﻿using Lib.Item;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppMobi
{
    public partial class Task : System.Web.UI.Page
    {
        public static int _PageSize = 15;
        public static int _TotalRec = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                LoadToolBox();
                InitScript();
            }
        }

        void LoadToolBox()
        {

        }
        void InitScript()
        {
            _TotalRec = Customer_Total(string.Empty);
            int ToTalPage = _TotalRec / _PageSize;
            if (ToTalPage == 0)
                ToTalPage = 1;
            LiteralPage.Text = @"
            <script>
                $(function () {
                    $('#pagination-demo').twbsPagination({
                        totalPages: " + ToTalPage + @",
                        visiblePages: 7,
                        first: false,
                        last: false,
                        onPageClick: function (event, page) {
                            searchCustomer(page);
                        }
                    });
                });
            </script>";
        }

        protected static int Customer_Total(string CustomerName)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Result = 0;

            if (UnitLevel == 0 ||
             UnitLevel == 1)
                Result = Note_Data.List_Note_Count(DepartmentKey, EmployeeKey, 6, string.Empty, DateTime.MinValue, DateTime.MinValue, string.Empty);
            if (UnitLevel >= 2)
                Result = Note_Data.List_Note_Count(DepartmentKey, EmployeeKey, 6, string.Empty, DateTime.MinValue, DateTime.MinValue, string.Empty);

            return Result;
        }
        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            InitScript();
        }

        [WebMethod]
        public static ITask[] Customer_Search(string CustomerName, int Page)
        {
            #region [Paging]
            int From = 0, To = 0;
            To = Page * _PageSize;
            From = To - _PageSize;

            if (To > _TotalRec)
                To = _TotalRec;
            if (From < 0)
                From = 1;
            #endregion

            DataTable zTable = new DataTable();

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            if (UnitLevel == 0 ||
                UnitLevel == 1)
            {
                zTable = Note_Data.List_Note(DepartmentKey, EmployeeKey, 6, string.Empty, DateTime.MinValue, DateTime.MinValue, string.Empty, From, To);
            }

            if (UnitLevel >= 2)
            {
                zTable = Note_Data.List_Note(DepartmentKey, EmployeeKey, 6, string.Empty, DateTime.MinValue, DateTime.MinValue, string.Empty, From, To);
            }

            List<ITask> ListAsset = zTable.DataTableToList<ITask>();
            return ListAsset.ToArray();
        }
    }
}