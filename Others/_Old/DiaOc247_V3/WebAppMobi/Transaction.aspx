﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Transaction.aspx.cs" Inherits="WebAppMobi.Transaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }

        .money {
            color: #dd4b39;
            font-weight: bold;
        }

        table > tbody > tr > td {
            white-space: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success flat">

                        <div class="box-header with-border">
                            <asp:Literal ID="Lit_TimeText" runat="server"></asp:Literal>
                            <span class="box-tools btn-group pull-right">
                                <a loading="yes" class="btn btn-default btn-sm" id="ViewPrevious"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="true">
                                    Chọn xem
                    <span class="fa fa-caret-down"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a loading="yes" href="#" id="ViewYear">Năm</a></li>
                                    <li><a loading="yes" href="#" id="ViewMonth">Tháng</a></li>
                                </ul>
                                <a loading="yes" class="btn btn-default btn-sm" id="ViewNext"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            </span>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="divdata">
                                <asp:Literal ID="LiteralData" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div class="box-footer">
                            <ul id="pagination-demo" class="pagination-sm"></ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <asp:HiddenField ID="HiddenFromDate" runat="server" Value="" />
    <asp:HiddenField ID="HiddenToDate" runat="server" Value="" />
    <asp:HiddenField ID="Hidden_ViewTime" runat="server" Value="0" />
    <asp:HiddenField ID="Hidden_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnActionViewTime" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewTime_Click" />
    <asp:Button ID="btnActionViewNextPrev" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewNextPrev_Click" />
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <script src="jquery.twbsPagination.min.js"></script>
    <asp:Literal ID="LiteralPage" runat="server"></asp:Literal>
    <script>
        function updateApprove(id) {
            $.ajax({
                type: 'POST',
                url: '/Transaction.aspx/SaveApprove',
                data: JSON.stringify({
                    'MasterKey': id,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (!isNaN(+msg.d) && isFinite(msg.d)) {
                        location.reload();
                    }
                    else {
                        alert(msg.d);
                    }
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function updateFinish(id) {
            $.ajax({
                type: 'POST',
                url: '/Transaction.aspx/SaveFinish',
                data: JSON.stringify({
                    'MasterKey': id,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (!isNaN(+msg.d) && isFinite(msg.d)) {
                        location.reload();
                    }
                    else {
                        alert(msg.d);
                    }
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }

        $(document).ready(function () {
            $("button[btn='DuyetGD']").click(function () { updateApprove($(this).attr("key")); });
            $("button[btn='DuyetThu']").click(function () { updateFinish($(this).attr("key")); });

            $("#ViewPrevious").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(-1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });

            $("#ViewNext").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });

            $("#ViewYear").click(function () {
                $("[id$=Hidden_ViewTime]").val(1);
                $("[id$=btnActionViewTime]").trigger("click");
            });

            $("#ViewMonth").click(function () {
                $("[id$=Hidden_ViewTime]").val(2);
                $("[id$=btnActionViewTime]").trigger("click");
            });
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        $(function () {
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('#back-to-top').tooltip('show');
        });
    </script>
</asp:Content>
