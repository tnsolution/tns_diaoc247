﻿using System;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;

namespace Lib.CRM
{
    public class Contact_Personal_Info
    {
        private int _ContactKey = 0;
        private int _CustomerKey = 0;

        private string _ContactName = "";
        private string _MobiPhone = "";
        private string _DepartmentName = "";
        private string _Position = "";
        private string _Fax = "";
        private string _Email = "";
        private string _Notes = "";
        private string _Message = "";

        public Contact_Personal_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public Contact_Personal_Info(int ContactKey)
        {
            string zSQL = "SELECT * FROM CRM_ContactPersonal WHERE ContactKey =@ContactKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ContactKey", SqlDbType.Int).Value = ContactKey;

                SqlDataReader nReader = zCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    _CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    _ContactKey = int.Parse(nReader["ContactKey"].ToString());
                    _ContactName = nReader["ContactName"].ToString();

                    _MobiPhone = nReader["MobiPhone"].ToString();
                    _DepartmentName = nReader["DepartmentName"].ToString();
                    _Position = nReader["Position"].ToString();
                    _Fax = nReader["Fax"].ToString();
                    _Email = nReader["Email"].ToString();
                    _Notes = nReader["Notes"].ToString();


                }
                //---- Close Connect SQL ----
                nReader.Close();
                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #region [ Properties ]
        // Info Customer
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public int ContactKey
        {
            get { return _ContactKey; }
            set { _ContactKey = value; }
        }
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        public string MobiPhone
        {
            get { return _MobiPhone; }
            set { _MobiPhone = value; }
        }

        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public string Position
        {
            get { return _Position; }
            set { _Position = value; }
        }
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }
        public string Message
        {
            get { return _Message; }
        }
        #endregion

        #region [ Methor ]
        public string Update()
        {
            string zResult = "";


            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE CRM_ContactPersonal SET "
                        + " ContactName = @ContactName, "
                        + " CustomerKey = @CustomerKey, "

                        + " MobiPhone = @MobiPhone ,"
                        + " DepartmentName = @DepartmentName ,"
                        + " Position = @Position ,"
                        + " Fax= @Fax, "
                        + " Email = @Email,  "
                        + " Notes = @Notes"

                        + " WHERE ContactKey = @ContactKey";


            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@ContactKey", SqlDbType.NVarChar).Value = _ContactKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = _CustomerKey;
                zCommand.Parameters.Add("@ContactName", SqlDbType.NVarChar).Value = _ContactName;

                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = _Position;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = _Fax;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = _Notes;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_ContactPersonal( "
                        + " CustomerKey,ContactName,"
                        + " MobiPhone,DepartmentName,Position,Fax,Email,Notes)"
                        + " VALUES( @CustomerKey,@ContactName,"
                        + " @MobiPhone,@DepartmentName,@Position,@Fax,@Email,@Notes)"
                        + " SELECT ContactKey FROM CRM_ContactPersonal WHERE ContactKey = SCOPE_IDENTITY()";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = _CustomerKey;
                zCommand.Parameters.Add("@ContactName", SqlDbType.NVarChar).Value = _ContactName;
                zCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = _MobiPhone;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName;
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = _Position;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = _Fax;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = _Notes;


                zResult = zCommand.ExecuteScalar().ToString();
                _ContactKey = int.Parse(zResult);

                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ContactKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_ContactPersonal WHERE ContactKey = @ContactKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ContactKey", SqlDbType.Int).Value = _ContactKey;
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();

            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
