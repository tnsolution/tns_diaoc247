﻿using System;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;

namespace Lib.CRM
{
    public class Customer_Info
    {
        #region [Columns]
        private int _CustomerKey = 0;
        private string _CustomerID = "";
        private string _CustomerName = "";

        private string _AccountBank = "";
        private string _TaxNumber = "";

        private string _Address = "";
        private string _CityName = "";
        private int _CountryKey = 1;
        private string _CountryName = "";

        private string _Phone = "";
        private string _Fax = "";
        private string _Email = "";
        private string _WebSite = "";

        private string _Note = "";

        private int _CategoryKey = 0;
        private string _CategoryName = "";

        private int _CustomerType = 1;
        private string _Message = "";

        private string _CreatedBy = "";
        private DateTime _CreatedDateTime;
        private string _ModifiedBy = "";
        private DateTime _ModifiedDateTime;
        #endregion        

        #region [ Properties ]

        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }

        public string AccountBank
        {
            get { return _AccountBank; }
            set { _AccountBank = value; }
        }
        public string TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; }
        }

        public int CountryKey
        {
            get { return _CountryKey; }
            set { _CountryKey = value; }
        }
        public string CountryName
        {
            get { return _CountryName; }
        }

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string WebSite
        {
            get { return _WebSite; }
            set { _WebSite = value; }
        }

        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
        }

        public int CustomerType
        {
            get { return _CustomerType; }
            set { _CustomerType = value; }
        }

        public string CreatedBy
        {
            set { _CreatedBy = value; }
            get { return _CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { _CreatedDateTime = value; }
            get { return _CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { _ModifiedBy = value; }
            get { return _ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { _ModifiedDateTime = value; }
            get { return _ModifiedDateTime; }
        }

        public string Message
        {
            set { _Message = value; }
            get { return _Message; }
        }


        #endregion

        #region [ Constructor Get Information ]
        public Customer_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public Customer_Info(int CustomerKey)
        {
            string zSQL = @" 
SELECT A.*,C.CategoryName FROM CRM_Customers A
LEFT JOIN CRM_CustomerCategories C ON C.CategoryKey = A.CategoryKey 
WHERE A.CustomerKey = @CustomerKey ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();

                    _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CustomerID = zReader["CustomerID"].ToString().Trim();
                    _CustomerName = zReader["CustomerName"].ToString().Trim();

                    _TaxNumber = zReader["TaxNumber"].ToString();
                    _AccountBank = zReader["AccountBank"].ToString();

                    _Address = zReader["Address"].ToString();
                    _CityName = zReader["CityName"].ToString();
                    _CountryKey = (int)zReader["CountryKey"];

                    _Phone = zReader["Phone"].ToString();
                    _Fax = zReader["Fax"].ToString();
                    _Email = zReader["Email"].ToString();
                    _WebSite = zReader["WebSite"].ToString();

                    _Note = zReader["Note"].ToString();

                    _CategoryKey = (int)zReader["CategoryKey"];
                    _CategoryName = zReader["CategoryName"].ToString();

                    _CustomerType = (int)zReader["CustomerType"];

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)zReader["CreatedDateTime"];

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)zReader["ModifiedDateTime"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Customer_Info(string CustomerID)
        {
            string zSQL = @" 
SELECT A.*,C.CategoryName FROM CRM_Customers A
LEFT JOIN CRM_CustomerCategories C ON C.CategoryKey = A.CategoryKey
WHERE A.CustomerID = @CustomerID ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = CustomerID;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();

                    _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CustomerID = zReader["CustomerID"].ToString().Trim();
                    _CustomerName = zReader["CustomerName"].ToString().Trim();

                    _TaxNumber = zReader["TaxNumber"].ToString();
                    _AccountBank = zReader["AccountBank"].ToString();

                    _Address = zReader["Address"].ToString();
                    _CityName = zReader["CityName"].ToString();
                    _CountryKey = (int)zReader["CountryKey"];

                    _Phone = zReader["Phone"].ToString();
                    _Fax = zReader["Fax"].ToString();
                    _Email = zReader["Email"].ToString();
                    _WebSite = zReader["WebSite"].ToString();

                    _Note = zReader["Note"].ToString();

                    _CategoryKey = (int)zReader["CategoryKey"];
                    _CategoryName = zReader["CategoryName"].ToString();

                    _CustomerType = (int)zReader["CustomerType"];

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)zReader["CreatedDateTime"];

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)zReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE CRM_Customers SET "
                        + " CustomerID = @CustomerID,"
                        + " CustomerName = @CustomerName ,"

                        + " TaxNumber = @TaxNumber, "
                        + " AccountBank = @AccountBank, "

                        + " Address = @Address ,"
                        + " CityName = @CityName ,"
                        + " CountryKey = @CountryKey ,"

                        + " Phone = @Phone ,"
                        + " Fax= @Fax, "
                        + " Email = @Email  ,"
                        + " Website = @Website ,"
                        + " Note = @Note, "

                        + " CategoryKey = @CategoryKey,"
                        + " CustomerType = @CustomerType,"

                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "

                        + " WHERE CustomerKey = @CustomerKey";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = _CustomerID;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;

                zCommand.Parameters.Add("@AccountBank", SqlDbType.NChar).Value = _AccountBank;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;

                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = _CityName;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;

                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = _Fax;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = _WebSite;

                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = _CustomerType;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create()
        {

            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Customers( "
                        + " CustomerID, CustomerName, AccountBank, TaxNumber,"
                        + " Address, CityName, CountryKey, "
                        + " Phone,Fax,Email,Website,  Note, CategoryKey,CustomerType, "
                        + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                        + " VALUES(@CustomerID, @CustomerName, @AccountBank, @TaxNumber,"
                        + " @Address, @CityName,  @CountryKey, "
                        + " @Phone,@Fax,@Email,@Website,  @Note,@CategoryKey,@CustomerType, "
                        + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                        + " SELECT CustomerKey FROM CRM_Customers WHERE CustomerKey = SCOPE_IDENTITY()";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = _CustomerID;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;

                zCommand.Parameters.Add("@AccountBank", SqlDbType.NChar).Value = _AccountBank;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;

                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = _CityName;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;

                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                zCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = _Fax;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = _WebSite;

                zCommand.Parameters.Add("@Note", SqlDbType.NText).Value = _Note;

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = _CustomerType;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;

                zResult = zCommand.ExecuteScalar().ToString();
                _CustomerKey = int.Parse(zResult);
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_CustomerKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM CRM_Customers WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Message = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}

