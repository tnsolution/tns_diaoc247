﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Lib.Config
{
    public class ConnectDataBase
    {
        private static SqlConnection _SQLConnect;
        private static string _ConnectionString = ConfigurationManager.ConnectionStrings["TNSolutionConnectionString"].ConnectionString;
        private string _Message = "";

        public ConnectDataBase()
        {
            _SQLConnect = new SqlConnection();
        }

        public ConnectDataBase(string StrConnect)
        {
            try
            {
                _SQLConnect = new SqlConnection();
                _SQLConnect.ConnectionString = StrConnect;
                _SQLConnect.Open();
                _ConnectionString = StrConnect;
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                _SQLConnect.Close();
            }

        }
        public void CloseConnect()
        {
            _SQLConnect.Close();
        }
        public string Message
        {
            get
            {
                return _Message;
            }
        }


        public static string ConnectionString
        {
            set
            {
                _ConnectionString = value;
            }
            get
            {
                return _ConnectionString;
            }

        }
        public static bool StillConnect
        {
            get
            {
                if (_SQLConnect == null || _SQLConnect.State == ConnectionState.Closed)
                    return false;
                else
                    return true;
            }
        }
    }
}
