﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.HRM
{
    public class Departments_Info
    {
        #region [ Field Name ]
        private int _DepartmentKey = 0;
        private string _DepartmentID = "";
        private string _DepartmentName = "";
        private int _Parent = 0;
        private string _Description = "";
        private int _Rank = 0;
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Departments_Info()
        {
        }
        public Departments_Info(int DepartmentKey)
        {
            string zSQL = "SELECT * FROM HRM_Departments WHERE DepartmentKey = @DepartmentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentID = zReader["DepartmentID"].ToString();
                    _DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        _Parent = int.Parse(zReader["Parent"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                } 
                zReader.Close(); 
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Departments_Info(string DepartmentID)
        {
            string zSQL = "SELECT * FROM HRM_Departments WHERE DepartmentID = @DepartmentID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.Char).Value = DepartmentID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentID = zReader["DepartmentID"].ToString();
                    _DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        _Parent = int.Parse(zReader["Parent"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Departments ("
        + " DepartmentID ,DepartmentName ,Parent ,Description ,Rank ) "
         + " VALUES ( "
         + "@DepartmentID ,@DepartmentName ,@Parent ,@Description ,@Rank ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.Char).Value = _DepartmentID;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE HRM_Departments SET "
                        + " DepartmentID = @DepartmentID,"
                        + " DepartmentName = @DepartmentName,"
                        + " Parent = @Parent,"
                        + " Description = @Description,"
                        + " Rank = @Rank"
                       + " WHERE DepartmentKey = @DepartmentKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.Char).Value = _DepartmentID;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = _Parent;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_DepartmentKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Departments WHERE DepartmentKey = @DepartmentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
