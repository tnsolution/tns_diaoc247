﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.HRM
{
    public class Employees_Info
    {
        #region [ Field Name ]
        private int _EmployeeKey = 0;

        private int _PositionKey = 0;
        private string _Position = "";        

        private int _DepartmentKey = 0;
        private string _DepartmentName = "";

        private string _UnitLevel = "";
        private string _UnitDescription = "";

        private int _ManagerKey = 0;
        private string _ManagerName = "";
        private string _LastName = "";
        private string _FirstName = "";
        private int _Gender = 0;
        private DateTime _Birthday;
        private string _IDCard = "";
        private DateTime _IDDate;
        private string _IDPlace = "";
        private string _Class = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Phone3 = "";
        private string _Phone4 = "";
        private string _Email1 = "";
        private string _Email2 = "";
        private string _Families = "";
        private string _TaxNumber = "";
        private DateTime _DateStart;
        private DateTime _DateEnd;
        private int _IsWorking = 0;
        private string _Address1 = "";
        private string _Address2 = "";
        private string _CarNumber = "";
        private string _Banks = "";
        private string _LoginID = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string UnitLevel
        {
            get { return _UnitLevel; }
            set { _UnitLevel = value; }
        }
        public int ManagerKey
        {
            get { return _ManagerKey; }
            set { _ManagerKey = value; }
        }
        public string ManagerName
        {
            get { return _ManagerName; }
            set { _ManagerName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public DateTime Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        public string IDCard
        {
            get { return _IDCard; }
            set { _IDCard = value; }
        }
        public DateTime IDDate
        {
            get { return _IDDate; }
            set { _IDDate = value; }
        }
        public string IDPlace
        {
            get { return _IDPlace; }
            set { _IDPlace = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }
        public string Phone3
        {
            get { return _Phone3; }
            set { _Phone3 = value; }
        }
        public string Phone4
        {
            get { return _Phone4; }
            set { _Phone4 = value; }
        }
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }
        public string Email2
        {
            get { return _Email2; }
            set { _Email2 = value; }
        }
        public string Families
        {
            get { return _Families; }
            set { _Families = value; }
        }
        public string TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }
        public DateTime DateStart
        {
            get { return _DateStart; }
            set { _DateStart = value; }
        }
        public DateTime DateEnd
        {
            get { return _DateEnd; }
            set { _DateEnd = value; }
        }
        public int IsWorking
        {
            get { return _IsWorking; }
            set { _IsWorking = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public string CarNumber
        {
            get { return _CarNumber; }
            set { _CarNumber = value; }
        }
        public string Banks
        {
            get { return _Banks; }
            set { _Banks = value; }
        }
        public string LoginID
        {
            get { return _LoginID; }
            set { _LoginID = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }

            set
            {
                _DepartmentName = value;
            }
        }       

        public string Position
        {
            get
            {
                return _Position;
            }

            set
            {
                _Position = value;
            }
        }

        public string UnitDescription
        {
            get
            {
                return _UnitDescription;
            }

            set
            {
                _UnitDescription = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Employees_Info()
        {
        }
        public Employees_Info(int EmployeeKey)
        {
            string zSQL = @"
SELECT A.*, B.DepartmentName, C.Position, D.Unit + ' | ' +D.Description AS UnitDescription
FROM HRM_Employees A
LEFT JOIN HRM_Departments B ON A.DepartmentKey = B.DepartmentKey 
LEFT JOIN HRM_Positions C ON A.PositionKey = C.PositionKey
LEFT JOIN SYS_Unit D ON A.UnitLevel = D.Rank
WHERE EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    DepartmentName = zReader["DepartmentName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    _Position = zReader["Position"].ToString();                    
                    _LastName = zReader["LastName"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    if (zReader["Gender"] != DBNull.Value)
                        _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["Birthday"] != DBNull.Value)
                        _Birthday = (DateTime)zReader["Birthday"];
                    _IDCard = zReader["IDCard"].ToString();
                    if (zReader["IDDate"] != DBNull.Value)
                        _IDDate = (DateTime)zReader["IDDate"];
                    _IDPlace = zReader["IDPlace"].ToString();

                    _UnitLevel = zReader["UnitLevel"].ToString();
                    _UnitDescription = zReader["UnitDescription"].ToString();

                    _Class = zReader["Class"].ToString();
                    _Phone1 = zReader["Phone1"].ToString();
                    _Email1 = zReader["Email1"].ToString();
                    _TaxNumber = zReader["TaxNumber"].ToString();
                    if (zReader["DateStart"] != DBNull.Value)
                        _DateStart = (DateTime)zReader["DateStart"];
                    if (zReader["DateEnd"] != DBNull.Value)
                        _DateEnd = (DateTime)zReader["DateEnd"];
                    if (zReader["IsWorking"] != DBNull.Value)
                        _IsWorking = int.Parse(zReader["IsWorking"].ToString());
                    _Address1 = zReader["Address1"].ToString();
                    _Address2 = zReader["Address2"].ToString();
                    _CarNumber = zReader["CarNumber"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO HRM_Employees (
DepartmentKey ,PositionKey,@UnitLevel ,Position ,
ManagerKey ,ManagerName ,LastName ,FirstName ,
Gender ,Birthday ,IDCard ,IDDate ,IDPlace ,Class ,
Phone1 ,Phone2 ,Phone3 ,Phone4 ,Email1 ,Email2 ,Families ,TaxNumber ,
DateStart ,DateEnd ,IsWorking ,Address1 ,Address2 ,CarNumber ,Banks ,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName)
VALUES (
@DepartmentKey ,@PositionKey,@UnitLevel,@Position ,
@ManagerKey ,@ManagerName ,@LastName ,@FirstName ,@Gender ,@Birthday ,
@IDCard ,@IDDate ,@IDPlace ,@Class ,@Phone1 ,@Phone2 ,@Phone3 ,@Phone4 ,@Email1 ,@Email2 ,@Families ,
@TaxNumber ,@DateStart ,@DateEnd ,@IsWorking ,@Address1 ,@Address2 ,@CarNumber ,@Banks ,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName )
SELECT EmployeeKey FROM HRM_Employees WHERE EmployeeKey = SCOPE_IDENTITY()";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@UnitLevel", SqlDbType.NVarChar).Value = _PositionKey.ToString();
                zCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = Position;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@ManagerName", SqlDbType.NVarChar).Value = _ManagerName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = _IDCard;
                if (_IDDate.Year == 0001)
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = _IDDate;
                zCommand.Parameters.Add("@IDPlace", SqlDbType.NVarChar).Value = _IDPlace;
                zCommand.Parameters.Add("@Class", SqlDbType.NVarChar).Value = _Class;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Phone3", SqlDbType.NVarChar).Value = _Phone3;
                zCommand.Parameters.Add("@Phone4", SqlDbType.NVarChar).Value = _Phone4;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
                zCommand.Parameters.Add("@Families", SqlDbType.NVarChar).Value = _Families;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                if (_DateStart.Year == 0001)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = _DateStart;
                if (_DateEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = _DateEnd;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Int).Value = _IsWorking;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@CarNumber", SqlDbType.NVarChar).Value = _CarNumber;
                zCommand.Parameters.Add("@Banks", SqlDbType.NVarChar).Value = _Banks;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                _EmployeeKey = int.Parse(zCommand.ExecuteScalar().ToString());

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                        + " DepartmentKey = @DepartmentKey,"
                        + " PositionKey = @PositionKey,"
                        + " UnitLevel = @UnitLevel,"
                        + " ManagerKey = @ManagerKey,"
                        + " LastName = @LastName,"
                        + " FirstName = @FirstName,"
                        + " Gender = @Gender,"
                        + " Birthday = @Birthday,"
                        + " IDCard = @IDCard,"
                        + " IDDate = @IDDate,"
                        + " IDPlace = @IDPlace,"
                        + " Class = @Class,"
                        + " Phone1 = @Phone1,"
                        + " Email1 = @Email1,"
                        + " TaxNumber = @TaxNumber,"
                        + " DateStart = @DateStart,"
                        + " DateEnd = @DateEnd,"
                        + " IsWorking = @IsWorking,"
                        + " Address1 = @Address1,"
                        + " Address2 = @Address2,"
                        + " CarNumber = @CarNumber,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@UnitLevel", SqlDbType.NVarChar).Value = _PositionKey.ToString();
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.Int).Value = _ManagerKey;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@IDCard", SqlDbType.NVarChar).Value = _IDCard;
                if (_IDDate.Year == 0001)
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IDDate", SqlDbType.DateTime).Value = _IDDate;
                zCommand.Parameters.Add("@IDPlace", SqlDbType.NVarChar).Value = _IDPlace;
                zCommand.Parameters.Add("@Class", SqlDbType.NVarChar).Value = _Class;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                zCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = _TaxNumber;
                if (_DateStart.Year == 0001)
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateStart", SqlDbType.DateTime).Value = _DateStart;
                if (_DateEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateEnd", SqlDbType.DateTime).Value = _DateEnd;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Int).Value = _IsWorking;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@CarNumber", SqlDbType.NVarChar).Value = _CarNumber;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_EmployeeKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Employees WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetActivated()
        {
            string zSQL = "UPDATE HRM_Employees SET "
                        + " IsWorking = @IsWorking,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE EmployeeKey = @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@IsWorking", SqlDbType.Int).Value = _IsWorking;

                if (_ModifiedDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
