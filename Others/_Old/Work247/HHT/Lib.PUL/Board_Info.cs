﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.PUL
{
    public class Board_Info
    {

        #region [ Field Name ]
        private int _BoardID = 0;
        private string _BoardTitle = "";
        private int _IsPublic = 0;
        private int _IsActive = 0;
        private int _IsFinish = 0;
        private DateTime _DateFinish;
        private DateTime _DateActive;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Board_Info()
        {
        }
        public Board_Info(int BoardID)
        {
            string zSQL = "SELECT * FROM PUL_Board WHERE BoardID = @BoardID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BoardID", SqlDbType.Int).Value = BoardID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["BoardID"] != DBNull.Value)
                        _BoardID = int.Parse(zReader["BoardID"].ToString());
                    _BoardTitle = zReader["BoardTitle"].ToString();
                    if (zReader["IsPublic"] != DBNull.Value)
                        _IsPublic = int.Parse(zReader["IsPublic"].ToString());
                    if (zReader["IsActive"] != DBNull.Value)
                        _IsActive = int.Parse(zReader["IsActive"].ToString());
                    if (zReader["IsFinish"] != DBNull.Value)
                        _IsFinish = int.Parse(zReader["IsFinish"].ToString());
                    if (zReader["DateFinish"] != DBNull.Value)
                        _DateFinish = (DateTime)zReader["DateFinish"];
                    if (zReader["DateActive"] != DBNull.Value)
                        _DateActive = (DateTime)zReader["DateActive"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }    
        #endregion

        #region [ Properties ]
        public int BoardID
        {
            get { return _BoardID; }
            set { _BoardID = value; }
        }
        public string BoardTitle
        {
            get { return _BoardTitle; }
            set { _BoardTitle = value; }
        }
        public int IsPublic
        {
            get { return _IsPublic; }
            set { _IsPublic = value; }
        }
        public int IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        public int IsFinish
        {
            get { return _IsFinish; }
            set { _IsFinish = value; }
        }
        public DateTime DateFinish
        {
            get { return _DateFinish; }
            set { _DateFinish = value; }
        }
        public DateTime DateActive
        {
            get { return _DateActive; }
            set { _DateActive = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PUL_Board ("
        + " BoardTitle ,IsPublic ,IsActive ,IsFinish ,DateFinish ,DateActive ,CreatedBy ,CreatedName ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@BoardTitle ,@IsPublic ,@IsActive ,@IsFinish ,@DateFinish ,@DateActive ,@CreatedBy ,@CreatedName ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT BoardID FROM PUL_Board WHERE BoardID = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BoardID", SqlDbType.Int).Value = _BoardID;
                zCommand.Parameters.Add("@BoardTitle", SqlDbType.NVarChar).Value = _BoardTitle;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;
                zCommand.Parameters.Add("@IsActive", SqlDbType.Int).Value = _IsActive;
                zCommand.Parameters.Add("@IsFinish", SqlDbType.Int).Value = _IsFinish;
                if (_DateFinish.Year == 0001)
                    zCommand.Parameters.Add("@DateFinish", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateFinish", SqlDbType.DateTime).Value = _DateFinish;
                if (_DateActive.Year == 0001)
                    zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = _DateActive;
            
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;            
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _BoardID = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE PUL_Board SET "
                        + " BoardTitle = @BoardTitle,"
                        + " IsPublic = @IsPublic,"
                        + " IsActive = @IsActive,"
                        + " IsFinish = @IsFinish,"
                        + " DateFinish = @DateFinish,"
                        + " DateActive = @DateActive,"                      
                        + " ModifiedDate = @ModifiedDate,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE BoardID = @BoardID";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@BoardID", SqlDbType.Int).Value = _BoardID;
                zCommand.Parameters.Add("@BoardTitle", SqlDbType.NVarChar).Value = _BoardTitle;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;
                zCommand.Parameters.Add("@IsActive", SqlDbType.Int).Value = _IsActive;
                zCommand.Parameters.Add("@IsFinish", SqlDbType.Int).Value = _IsFinish;
                if (_DateFinish.Year == 0001)
                    zCommand.Parameters.Add("@DateFinish", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateFinish", SqlDbType.DateTime).Value = _DateFinish;
                if (_DateActive.Year == 0001)
                    zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateActive", SqlDbType.DateTime).Value = _DateActive;                       
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_BoardID == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Board WHERE BoardID = @BoardID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@BoardID", SqlDbType.Int).Value = _BoardID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
