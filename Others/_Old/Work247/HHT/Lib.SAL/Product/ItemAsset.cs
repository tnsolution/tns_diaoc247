﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ItemAsset
    {
        public string AssetKey = "0";

        public string ProjectKey = "0";
        public string ProjectName = "";

        public string CategoryKey = "0";
        public string CategoryName = "";

        public string AssetType = "";
        public string AssetID = "";

        public string ID1 = "";
        public string ID2 = "";
        public string ID3 = "";

        public string Address = "";

        public string StatusKey = "0";
        public string Status = "";
        public string LegalKey = "0";
        public string Legal = "";

        public string PurposeKey = "";
        public string Purpose = "";

        public string EmployeeKey = "0";
        public string EmployeeName = "";
        public string DepartmentKey = "0";

        public string Highway = "";
        public string Structure = "";
        public string View = "";
        public string Door = "";
        public string Room = "";
        public string FurnitureKey = "0";
        public string Furniture = "";
        public string Description = "";
        public string DateContractEnd = "";

        public string AreaBuild = "0";
        public string Area = "0";
        public string AreaWall = "0";
        public string AreaWater = "0";

        public string PriceRent = "0";
        public string PricePurchase = "0";
        public string Price = "0";

        public string WebPublished = "0";
        public string WebTitle = "";
        public string WebContent = "";
        public string Hot = "0";

        public string CreatedDate = "";
        public string CreatedBy = "";
        public string CreatedName = "";
        public string ModifiedDate = "";
        public string ModifiedBy = "";
        public string ModifiedName = "";

        public string Message = "";
    }
}
