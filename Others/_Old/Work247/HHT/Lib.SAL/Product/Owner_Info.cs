﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class Owner_Info
    {
        #region [ Field Name ]
        private int _Type = 0;
        private int _AssetKey = 0;
        private int _OwnerKey = 0;
        private int _ProductKey = 0;
        private string _ProductID = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _CustomerName = "";
        private string _Phone = "";
        private string _Email = "";
        private string _Address = "";
        private string _Note = "";
        private string _Message = "";

        #endregion
        #region [ Properties ]

        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public int OwnerKey
        {
            get { return _OwnerKey; }
            set { _OwnerKey = value; }
        }
        public int ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }

        public int Type
        {
            get
            {
                return _Type;
            }

            set
            {
                _Type = value;
            }
        }

        public string Note
        {
            get
            {
                return _Note;
            }

            set
            {
                _Note = value;
            }
        }
        #endregion

        public Owner_Info() { }
        public Owner_Info(int Type, int OwnerKey)
        {
            this.Type = Type;
            if (Type == 1)
            {
                #region Building
                string zSQL = "SELECT * FROM PUL_Resale_Apartment_Owners WHERE OwnerKey = @OwnerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = OwnerKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        if (zReader["OwnerKey"] != DBNull.Value)
                            _OwnerKey = int.Parse(zReader["OwnerKey"].ToString());
                        if (zReader["ProductKey"] != DBNull.Value)
                            _ProductKey = int.Parse(zReader["ProductKey"].ToString());
                        if (zReader["AssetKey"] != DBNull.Value)
                            _ProductKey = int.Parse(zReader["AssetKey"].ToString());
                        _ProductID = zReader["ProductID"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _CustomerName = zReader["CustomerName"].ToString();
                        _Phone = zReader["Phone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Address = zReader["Address"].ToString();
                        _Note = zReader["Note"].ToString();
                    }
                    zReader.Close(); zCommand.Dispose();
                }
                catch (Exception Err) { _Message = Err.ToString(); }
                finally { zConnect.Close(); }
                #endregion
            }
            if (Type == 2)
            {
                #region Ground
                string zSQL = "SELECT * FROM PUL_Resale_Ground_Owners WHERE OwnerKey = @OwnerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = OwnerKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        if (zReader["OwnerKey"] != DBNull.Value)
                            _OwnerKey = int.Parse(zReader["OwnerKey"].ToString());
                        if (zReader["ProductKey"] != DBNull.Value)
                            _ProductKey = int.Parse(zReader["ProductKey"].ToString());
                        if (zReader["AssetKey"] != DBNull.Value)
                            _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                        _ProductID = zReader["ProductID"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _CustomerName = zReader["CustomerName"].ToString();
                        _Phone = zReader["Phone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Address = zReader["Address"].ToString();
                        _Note = zReader["Note"].ToString();
                    }
                    zReader.Close(); zCommand.Dispose();
                }
                catch (Exception Err) { _Message = Err.ToString(); }
                finally { zConnect.Close(); }
                #endregion
            }
            if (Type == 3)
            {
                #region House
                string zSQL = "SELECT * FROM PUL_Resale_House_Owners WHERE OwnerKey = @OwnerKey";
                string zConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                try
                {
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.Text;
                    zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = OwnerKey;
                    SqlDataReader zReader = zCommand.ExecuteReader();
                    if (zReader.HasRows)
                    {
                        zReader.Read();
                        if (zReader["OwnerKey"] != DBNull.Value)
                            _OwnerKey = int.Parse(zReader["OwnerKey"].ToString());
                        if (zReader["ProductKey"] != DBNull.Value)
                            _ProductKey = int.Parse(zReader["ProductKey"].ToString());
                        if (zReader["AssetKey"] != DBNull.Value)
                            _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                        _CustomerName = zReader["CustomerName"].ToString();
                        _ProductID = zReader["ProductID"].ToString();
                        _FirstName = zReader["FirstName"].ToString();
                        _LastName = zReader["LastName"].ToString();
                        _Phone = zReader["Phone"].ToString();
                        _Email = zReader["Email"].ToString();
                        _Address = zReader["Address"].ToString();
                        _Note = zReader["Note"].ToString();
                    }
                    zReader.Close(); zCommand.Dispose();
                }
                catch (Exception Err) { _Message = Err.ToString(); }
                finally { zConnect.Close(); }
                #endregion
            }
        }

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            if (Type == 1)
            {
                zSQL = "INSERT INTO PUL_Resale_Apartment_Owners ("
                + "AssetKey, ProductKey ,ProductID ,FirstName ,LastName, CustomerName ,Phone ,Email ,Address ) "
                + " VALUES ( "
                + "@AssetKey,@ProductKey ,@ProductID ,@FirstName ,@LastName, @CustomerName ,@Phone ,@Email ,@Address ) "
                + " SELECT OwnerKey FROM PUL_Resale_Apartment_Owners WHERE OwnerKey = SCOPE_IDENTITY() ";
            }
            if (Type == 3)
            {
                zSQL = "INSERT INTO PUL_Resale_Ground_Owners ("
                + "AssetKey, ProductKey ,ProductID ,FirstName ,LastName ,Phone ,Email ,Address ) "
                + " VALUES ( "
                + "@AssetKey, @ProductKey ,@ProductID ,@FirstName ,@LastName ,@Phone ,@Email ,@Address ) "
                + " SELECT OwnerKey FROM PUL_Resale_Ground_Owners WHERE OwnerKey = SCOPE_IDENTITY() ";
            }
            if (Type == 2)
            {
                zSQL = "INSERT INTO PUL_Resale_House_Owners ("
                + "AssetKey, ProductKey ,ProductID ,FirstName ,LastName, CustomerName ,Phone ,Email ,Address ) "
                + " VALUES ( "
                + "@AssetKey, @ProductKey ,@ProductID ,@FirstName ,@LastName, @CustomerName ,@Phone ,@Email ,@Address ) "
                + " SELECT OwnerKey FROM PUL_Resale_House_Owners WHERE OwnerKey = SCOPE_IDENTITY() ";
            }

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = _OwnerKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = _ProductID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                _OwnerKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "";
            if (Type == 1)
            {
                zSQL = "UPDATE PUL_Resale_Apartment_Owners SET AssetKey = @AssetKey,"
                    + " ProductKey = @ProductKey,"
                    + " ProductID = @ProductID,"
                    + " FirstName = @FirstName,"
                    + " LastName = @LastName, CustomerName = @CustomerName,"
                    + " Phone = @Phone,"
                    + " Email = @Email,"
                    + " Address = @Address"
                   + " WHERE OwnerKey = @OwnerKey";
            }
            if (Type == 2)
            {
                zSQL = "UPDATE PUL_Resale_Ground_Owners SET AssetKey = @AssetKey,"
                    + " ProductKey = @ProductKey,"
                    + " ProductID = @ProductID,"
                    + " FirstName = @FirstName,"
                    + " LastName = @LastName, CustomerName = @CustomerName,"
                    + " Phone = @Phone,"
                    + " Email = @Email,"
                    + " Address = @Address"
                   + " WHERE OwnerKey = @OwnerKey";
            }
            if (Type == 3)
            {
                zSQL = "UPDATE PUL_Resale_House_Owners SET AssetKey = @AssetKey,"
                    + " ProductKey = @ProductKey,"
                    + " ProductID = @ProductID,"
                    + " FirstName = @FirstName,"
                    + " LastName = @LastName, CustomerName = @CustomerName,"
                    + " Phone = @Phone,"
                    + " Email = @Email,"
                    + " Address = @Address"
                   + " WHERE OwnerKey = @OwnerKey";
            }
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = _OwnerKey;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = _ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = _ProductID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone;
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            if (Type == 1)
                zSQL = "DELETE FROM PUL_Resale_Apartment_Owners WHERE OwnerKey = @OwnerKey";
            if (Type == 2)
                zSQL = "DELETE FROM PUL_Resale_Ground_Owners WHERE OwnerKey = @OwnerKey";
            if (Type == 3)
                zSQL = "DELETE FROM PUL_Resale_House WHERE OwnerKey = @OwnerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OwnerKey", SqlDbType.Int).Value = _OwnerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(int AssetKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            if (Type == 1)
                zSQL = "DELETE FROM PUL_Resale_Apartment_Owners WHERE AssetKey = @AssetKey";
            if (Type == 2)
                zSQL = "DELETE FROM PUL_Resale_Ground_Owners WHERE AssetKey = @AssetKey";
            if (Type == 3)
                zSQL = "DELETE FROM PUL_Resale_House WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_OwnerKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
    }
}
