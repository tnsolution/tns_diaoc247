﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Lib.SAL
{
    public class Product_Data
    {
        public static DataTable CheckExsist(string AssetID)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetData(@"
SELECT A.AssetKey, 'ResaleApartment' AS [Type], A.AssetID,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_GetOwnerInfo(A.AssetKey, 'ResaleApartment' ) AS OwnerInfo
FROM PUL_Resale_Apartment A 
WHERE RTRIM(A.AssetID) = N'" + AssetID + @"'
UNION
SELECT A.AssetKey, 'ResaleHouse' AS [Type], A.AssetID,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_GetOwnerInfo(A.AssetKey, 'ResaleHouse' ) AS OwnerInfo 
FROM PUL_Resale_Ground A 
WHERE RTRIM(A.AssetID) =N'" + AssetID + @"'
UNION
SELECT A.AssetKey, 'ResaleGround' AS [Type], A.AssetID,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_GetOwnerInfo(A.AssetKey, 'ResaleGround' ) AS OwnerInfo 
FROM PUL_Resale_House A 
WHERE RTRIM(A.AssetID) =N'" + AssetID + "'");
        }

        public static bool CheckReminder(int AssetKey)
        {
            SqlContext Sql = new SqlContext();
            return Sql.IsExist("SELECT COUNT(*)  FROM SYS_Notification WHERE ObjectTable ='" + AssetKey + "' AND IsRead =0");
        }
    }
}