﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class Product_Info
    {
        ItemAsset _ItemAsset = new ItemAsset();
        public ItemAsset ItemAsset
        {
            get
            {
                return _ItemAsset;
            }

            set
            {
                _ItemAsset = value;
            }
        }

        public Product_Info()
        {

        }
        public Product_Info(int Key, string Type)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.*, B.CategoryName, C.Product AS Furniture, 
D.Product AS [Status], E.Product AS LegalStatus,
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName
FROM PUL_Resale_Apartment A
LEFT JOIN PUL_Category B ON A.CategoryKey = B.CategoryKey
LEFT JOIN SYS_Categories C ON A.CategoryInside = C.AutoKey
LEFT JOIN SYS_Categories D ON A.ApartmentStatus = D.AutoKey
LEFT JOIN SYS_Categories E ON A.Legal = E.AutoKey
WHERE A.AssetKey = @AssetKey";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["ApartmentStatus"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["Legal"].ToString();
                            _ItemAsset.Legal = zReader["LegalStatus"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.FurnitureKey = zReader["CategoryInside"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["DirectionView"].ToString();
                            _ItemAsset.Door = zReader["DirectionDoor"].ToString();
                            _ItemAsset.Room = zReader["NumberBed"].ToString();
                            _ItemAsset.Area = zReader["Area"].ToString();
                            _ItemAsset.PriceRent = zReader["PriceRent"].ToDoubleString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.Price = zReader["Price"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                case "ResaleHouse":
                    #region [Get Resale House]
                    zSQL = @"
SELECT A.*, 
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
G.CategoryName,
D.Product AS [Status],
F.Product AS LegalStatus,
C.Product AS Furniture
FROM PUL_Resale_House A
LEFT JOIN dbo.SYS_Categories C ON C.AutoKey = A.CategoryInside
LEFT JOIN dbo.SYS_Categories D ON A.HouseStatus = D.AutoKey
LEFT JOIN dbo.SYS_Categories F ON A.Legal = F.AutoKey
LEFT JOIN dbo.PUL_Category G ON G.CategoryKey = A.CategoryKey
WHERE A.AssetKey = @AssetKey";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["HouseStatus"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["Legal"].ToString();
                            _ItemAsset.Legal = zReader["LegalStatus"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.FurnitureKey = zReader["CategoryInside"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.Highway = zReader["Highway"].ToString();
                            _ItemAsset.Structure = zReader["Structure"].ToString();
                            _ItemAsset.Door = zReader["MainDoor"].ToString();
                            _ItemAsset.Room = zReader["NumberRoom"].ToString();
                            _ItemAsset.AreaBuild = zReader["GroundTotal"].ToString();
                            _ItemAsset.Area = zReader["GroundArea"].ToString();
                            _ItemAsset.PriceRent = zReader["PriceRent"].ToDoubleString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.Price = zReader["Price"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }

                    break;
                #endregion
                case "ResaleGround":

                    break;

                default:
                    break;
            }
        }
    }
}
