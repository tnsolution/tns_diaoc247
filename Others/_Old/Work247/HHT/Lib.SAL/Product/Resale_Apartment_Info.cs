﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Resale_Apartment_Info
    {
        #region [ Field Name ]
        private string _Title = "";
        private int _WebPublic = 0;
        private int _ProjectKey = 0;
        private string _ProjectName = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";

        private string _ID1 = "";
        private string _ID2 = "";
        private string _ID3 = "";

        private string _WallArea = "";
        private string _WaterArea = "";

        private string _AssetSalesStatus = "";
        private int _AssetSalesStatusKey = 0;
        private string _AssetLegal = "";
        private int _AssetLegalKey = 0;

        private string _InsideHouseStatus = "";
        private int _InsideHouseStatusKey = 0;

        private DateTime _DateContractEnd;
        private int _AssetKey = 0;
        private string _AssetID = "";
        private string _ApartmentName = "";

        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private string _Street = "";
        private int _WardKey = 0;
        private string _Ward = "";
        private int _DistrictKey = 0;
        private string _District = "";
        private int _ProvinceKey = 0;
        private string _Province = "";
        private string _Address = "";
        private string _PurposeSearch = "";
        private string _PurposeShow = "";
        private string _Floor = "";
        private string _DirectionView = "";
        private string _DirectionDoor = "";
        private int _NumberBed = 0;
        private float _Area;
        private string _Details = "";
        private string _ContractBooking = "";
        private string _ContractPayment = "";
        private string _CertificatePaper = "";
        private string _Description = "";
        private double _PriceRent = 0;
        private double _PricePurchase = 0;
        private double _Price = 0;
        private int _IsPublic = 0;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private int _IsDraft = 1;
        private string _Web = "";
        #endregion
        #region [ Properties ]
        public DateTime DateContractEnd
        {
            get { return _DateContractEnd; }
            set { _DateContractEnd = value; }
        }

        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public string AssetID
        {
            get { return _AssetID; }
            set { _AssetID = value; }
        }
        public string ApartmentName
        {
            get { return _ApartmentName; }
            set { _ApartmentName = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }
        public int WardKey
        {
            get { return _WardKey; }
            set { _WardKey = value; }
        }
        public string Ward
        {
            get { return _Ward; }
            set { _Ward = value; }
        }
        public int DistrictKey
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public string District
        {
            get { return _District; }
            set { _District = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public string Province
        {
            get { return _Province; }
            set { _Province = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string PurposeSearch
        {
            get { return _PurposeSearch; }
            set { _PurposeSearch = value; }
        }
        public string PurposeShow
        {
            get { return _PurposeShow; }
            set { _PurposeShow = value; }
        }
        public string Floor
        {
            get { return _Floor; }
            set { _Floor = value; }
        }
        public string DirectionView
        {
            get { return _DirectionView; }
            set { _DirectionView = value; }
        }
        public string DirectionDoor
        {
            get { return _DirectionDoor; }
            set { _DirectionDoor = value; }
        }
        public int NumberBed
        {
            get { return _NumberBed; }
            set { _NumberBed = value; }
        }
        public float Area
        {
            get { return _Area; }
            set { _Area = value; }
        }
        public string Details
        {
            get { return _Details; }
            set { _Details = value; }
        }
        public string ContractBooking
        {
            get { return _ContractBooking; }
            set { _ContractBooking = value; }
        }
        public string ContractPayment
        {
            get { return _ContractPayment; }
            set { _ContractPayment = value; }
        }
        public string CertificatePaper
        {
            get { return _CertificatePaper; }
            set { _CertificatePaper = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public double PriceRent
        {
            get { return _PriceRent; }
            set { _PriceRent = value; }
        }
        public double PricePurchase
        {
            get { return _PricePurchase; }
            set { _PricePurchase = value; }
        }
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public int IsPublic
        {
            get { return _IsPublic; }
            set { _IsPublic = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int IsDraft
        {
            get
            {
                return _IsDraft;
            }

            set
            {
                _IsDraft = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public int ProjectKey
        {
            get
            {
                return _ProjectKey;
            }

            set
            {
                _ProjectKey = value;
            }
        }

        public string ID1
        {
            get
            {
                return _ID1;
            }

            set
            {
                _ID1 = value;
            }
        }

        public string ID2
        {
            get
            {
                return _ID2;
            }

            set
            {
                _ID2 = value;
            }
        }

        public string ID3
        {
            get
            {
                return _ID3;
            }

            set
            {
                _ID3 = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public string WallArea
        {
            get
            {
                return _WallArea;
            }

            set
            {
                _WallArea = value;
            }
        }

        public string WaterArea
        {
            get
            {
                return _WaterArea;
            }

            set
            {
                _WaterArea = value;
            }
        }

        public int AssetLegalKey
        {
            get
            {
                return _AssetLegalKey;
            }

            set
            {
                _AssetLegalKey = value;
            }
        }

        public string AssetLegal
        {
            get
            {
                return _AssetLegal;
            }

            set
            {
                _AssetLegal = value;
            }
        }

        public int AssetSalesStatusKey
        {
            get
            {
                return _AssetSalesStatusKey;
            }

            set
            {
                _AssetSalesStatusKey = value;
            }
        }

        public string AssetSalesStatus
        {
            get
            {
                return _AssetSalesStatus;
            }

            set
            {
                _AssetSalesStatus = value;
            }
        }

        public string InsideHouseStatus
        {
            get
            {
                return _InsideHouseStatus;
            }

            set
            {
                _InsideHouseStatus = value;
            }
        }

        public int InsideHouseStatusKey
        {
            get
            {
                return _InsideHouseStatusKey;
            }

            set
            {
                _InsideHouseStatusKey = value;
            }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string Web
        {
            get
            {
                return _Web;
            }

            set
            {
                _Web = value;
            }
        }

        public int DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public int WebPublic
        {
            get
            {
                return _WebPublic;
            }

            set
            {
                _WebPublic = value;
            }
        }

        public string Title
        {
            get
            {
                return _Title;
            }

            set
            {
                _Title = value;
            }
        }

        #endregion
        #region [ Constructor Get Information ]
        public Resale_Apartment_Info()
        {
        }
        public Resale_Apartment_Info(int AssetKey)
        {
            string zSQL = @"
SELECT A.*,
H.CategoryName,B.ProjectName,
E.Product AS AssetSalesStatus,
F.Product AS AssetLegalStatus,
C.Product AS InsideHouseStatus
FROM dbo.PUL_Resale_Apartment A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN dbo.SYS_Categories C ON C.AutoKey = A.CategoryInside
LEFT JOIN dbo.SYS_Categories E ON A.ApartmentStatus = E.AutoKey 
LEFT JOIN dbo.SYS_Categories F ON A.Legal = F.AutoKey 
LEFT JOIN dbo.PUL_Category H ON H.CategoryKey = A.CategoryKey
WHERE A.AssetKey = @AssetKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Title = zReader["Title"].ToString();
                    if (zReader["WebPublic"] != DBNull.Value)
                        _WebPublic = int.Parse(zReader["WebPublic"].ToString());

                    _Web = zReader["Web"].ToString();
                    _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();
                    _ProjectName = zReader["ProjectName"].ToString();

                    _ID1 = zReader["ID1"].ToString();
                    _ID2 = zReader["ID2"].ToString();
                    _ID3 = zReader["ID3"].ToString();

                    _AssetSalesStatus = zReader["AssetSalesStatus"].ToString();
                    if (zReader["ApartmentStatus"] != DBNull.Value)
                        _AssetSalesStatusKey = int.Parse(zReader["ApartmentStatus"].ToString());
                    _AssetLegal = zReader["AssetLegalStatus"].ToString();
                    if (zReader["Legal"] != DBNull.Value)
                        _AssetLegalKey = int.Parse(zReader["Legal"].ToString());

                    _WallArea = zReader["WallArea"].ToString();
                    _WaterArea = zReader["WaterArea"].ToString();

                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    if (zReader["DateContractEnd"] != DBNull.Value)
                        _DateContractEnd = (DateTime)zReader["DateContractEnd"];

                    InsideHouseStatus = zReader["InsideHouseStatus"].ToString();
                    if (zReader["CategoryInside"] != DBNull.Value)
                        InsideHouseStatusKey = int.Parse(zReader["CategoryInside"].ToString());

                    _AssetID = zReader["AssetID"].ToString();
                    _ApartmentName = zReader["ApartmentName"].ToString();
                    _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _Street = zReader["Street"].ToString();
                    if (zReader["WardKey"] != DBNull.Value)
                        _WardKey = int.Parse(zReader["WardKey"].ToString());
                    _Ward = zReader["Ward"].ToString();
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    _District = zReader["District"].ToString();
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    _Province = zReader["Province"].ToString();
                    _Address = zReader["Address"].ToString();
                    _PurposeSearch = zReader["PurposeSearch"].ToString();
                    _PurposeShow = zReader["PurposeShow"].ToString();
                    _Floor = zReader["Floor"].ToString();
                    _DirectionView = zReader["DirectionView"].ToString();
                    _DirectionDoor = zReader["DirectionDoor"].ToString();
                    if (zReader["NumberBed"] != DBNull.Value)
                        _NumberBed = int.Parse(zReader["NumberBed"].ToString());
                    if (zReader["Area"] != DBNull.Value)
                        _Area = float.Parse(zReader["Area"].ToString());
                    _Details = zReader["Details"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["PriceRent"] != DBNull.Value)
                        _PriceRent = double.Parse(zReader["PriceRent"].ToString());
                    if (zReader["PricePurchase"] != DBNull.Value)
                        _PricePurchase = double.Parse(zReader["PricePurchase"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["IsPublic"] != DBNull.Value)
                        _IsPublic = int.Parse(zReader["IsPublic"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO PUL_Resale_Apartment (
ProjectKey,CategoryKey, ID1,ID2,ID3,
AssetID ,ApartmentName ,Legal ,ApartmentCategory ,ApartmentStatus ,ProductKey ,ProductCategory ,ProductID ,EmployeeKey , DepartmentKey,
EmployeeID ,EmployeeName ,Street ,WardKey ,Ward ,DistrictKey ,District ,ProvinceKey ,Province ,Address ,PurposeSearch ,PurposeShow ,Floor ,
DirectionView ,DirectionDoor ,NumberBed ,Area ,CategoryInside ,Details ,Description ,PriceRent ,PricePurchase ,Price ,IsPublic ,DateContractEnd,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName, IsDraft, Web,Title,WebPublic ) 
VALUES ( 
@ProjectKey, @CategoryKey, @ID1,@ID2,@ID3,
@AssetID ,@ApartmentName ,@Legal ,@ApartmentCategory ,@ApartmentStatus ,@ProductKey ,@ProductCategory ,@ProductID ,@EmployeeKey ,@DepartmentKey,
@EmployeeID ,@EmployeeName ,@Street ,@WardKey ,@Ward ,@DistrictKey ,@District ,@ProvinceKey ,@Province ,@Address ,@PurposeSearch ,@PurposeShow ,@Floor ,
@DirectionView ,@DirectionDoor ,@NumberBed ,@Area ,@CategoryInside ,@Details ,@Description ,@PriceRent ,@PricePurchase ,@Price ,@IsPublic , @DateContractEnd, GETDATE() ,
@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName, @IsDraft, @Web,@Title,@WebPublic ) 
SELECT AssetKey FROM PUL_Resale_Apartment WHERE AssetKey = SCOPE_IDENTITY() ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _WebPublic;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _Web;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ID1;
                zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ID2;
                zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ID3;

                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.Char).Value = _AssetID;
                zCommand.Parameters.Add("@ApartmentName", SqlDbType.NVarChar).Value = _ApartmentName;
                zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _AssetLegalKey;
                zCommand.Parameters.Add("@ApartmentCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.Int).Value = _AssetSalesStatusKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = 0;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = _Ward;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = _District;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _PurposeSearch;
                zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _PurposeShow;
                zCommand.Parameters.Add("@Floor", SqlDbType.NVarChar).Value = _Floor;
                zCommand.Parameters.Add("@DirectionView", SqlDbType.NVarChar).Value = _DirectionView;
                zCommand.Parameters.Add("@DirectionDoor", SqlDbType.NVarChar).Value = _DirectionDoor;
                zCommand.Parameters.Add("@NumberBed", SqlDbType.Int).Value = _NumberBed;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@CategoryInside", SqlDbType.Int).Value = _InsideHouseStatusKey;
                zCommand.Parameters.Add("@Details", SqlDbType.NVarChar).Value = _Details;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@PriceRent", SqlDbType.Money).Value = _PriceRent;
                zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = _PricePurchase;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;

                if (_DateContractEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = _DateContractEnd;

                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _AssetKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PUL_Resale_Apartment SET Legal = @Legal,"
                        + " CategoryKey =@CategoryKey, ProjectKey =@ProjectKey, ID1=@ID1, ID2=@ID2, ID3=@ID3, WallArea=@WallArea,WaterArea=@WaterArea, DepartmentKey=@DepartmentKey,"
                        + " AssetID = @AssetID,"
                        + " ApartmentName = @ApartmentName,"
                        + " ApartmentCategory = @ApartmentCategory,"
                        + " ApartmentStatus = @ApartmentStatus,"
                        + " ProductKey = @ProductKey,"
                        + " ProductCategory = @ProductCategory,"
                        + " ProductID = @ProductID,"
                        + " Street = @Street,"
                        + " WardKey = @WardKey,"
                        + " Ward = @Ward,"
                        + " DistrictKey = @DistrictKey,"
                        + " District = @District,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " Province = @Province,"
                        + " Address = @Address,"
                        + " PurposeSearch = @PurposeSearch,"
                        + " PurposeShow = @PurposeShow,"
                        + " Floor = @Floor,"
                        + " DirectionView = @DirectionView,"
                        + " DirectionDoor = @DirectionDoor,"
                        + " NumberBed = @NumberBed,"
                        + " Area = @Area,"
                        + " CategoryInside = @CategoryInside,"
                        + " Details = @Details,"
                        + " Description = @Description,"
                        + " PriceRent = @PriceRent,"
                        + " PricePurchase = @PricePurchase,"
                        + " Price = @Price,"
                        + " IsPublic = @IsPublic, DateContractEnd = @DateContractEnd,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName, IsDraft = @IsDraft, Web=@Web, WebPublic = @WebPublic, Title = @Title"
                       + " WHERE AssetKey = @AssetKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _WebPublic;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;

                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _Web;

                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;

                zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ID1;
                zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ID2;
                zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ID3;

                zCommand.Parameters.Add("@WallArea", SqlDbType.NVarChar).Value = _WallArea;
                zCommand.Parameters.Add("@WaterArea", SqlDbType.NVarChar).Value = _WaterArea;

                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.Char).Value = _AssetID;
                zCommand.Parameters.Add("@ApartmentName", SqlDbType.NVarChar).Value = _ApartmentName;
                zCommand.Parameters.Add("@ApartmentCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.Int).Value = _AssetSalesStatusKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductCategory", SqlDbType.Int).Value = 0;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = 0;
                zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _AssetLegalKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@Ward", SqlDbType.NVarChar).Value = _Ward;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@District", SqlDbType.NVarChar).Value = _District;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@Province", SqlDbType.NVarChar).Value = _Province;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _PurposeSearch;
                zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _PurposeShow;
                zCommand.Parameters.Add("@Floor", SqlDbType.NVarChar).Value = _Floor;
                zCommand.Parameters.Add("@DirectionView", SqlDbType.NVarChar).Value = _DirectionView;
                zCommand.Parameters.Add("@DirectionDoor", SqlDbType.NVarChar).Value = _DirectionDoor;
                zCommand.Parameters.Add("@NumberBed", SqlDbType.Int).Value = _NumberBed;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@CategoryInside", SqlDbType.Int).Value = _InsideHouseStatusKey;
                zCommand.Parameters.Add("@Details", SqlDbType.NVarChar).Value = _Details;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@PriceRent", SqlDbType.Money).Value = _PriceRent;
                zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = _PricePurchase;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;
                if (_DateContractEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = _DateContractEnd;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AssetKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Resale_Apartment WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Share()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE PUL_Resale_Apartment SET 
IsPublic = @IsPublic,
ModifiedName = @ModifiedName,
ModifiedDate = @ModifiedDate,
ModifiedBy = @ModifiedBy
WHERE AssetKey = @AssetKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@IsPublic", SqlDbType.Int).Value = _IsPublic;

                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Status()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PUL_Resale_Apartment SET ApartmentStatus = @ApartmentStatus WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.Int).Value = _AssetSalesStatusKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string PostWeb()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PUL_Resale_Apartment SET WebPublic = @WebPublic, Web = @Web, Title = @Title WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _WebPublic;
                zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _Web;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Custom(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------    
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
