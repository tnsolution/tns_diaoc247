﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SYS
{
    public class CustomInsert
    {
        public static string Exe(string Query)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------        
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(Query, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
