﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using System.Linq;
namespace Lib.SYS
{
    public class Notification_Data
    {
        public static int Count_Notification(int Type, int EmployeeKey, int DepartmentKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(COUNT(A.ID),0) FROM SYS_Notification A WHERE A.[TYPE] = @Type AND IsRead = 0";

            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @Key";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartKey", SqlDbType.Int).Value = DepartmentKey;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = -1;
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static int Count_Task(int EmployeeKey, int DepartmentKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @" 
SELECT COUNT(A.InfoKey) 
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Note B ON A.NoteKey = B.NoteKey 
LEFT JOIN HRM_Employees C ON C.EmployeeKey = B.SendTo
WHERE StatusInfo = 6";

            if (DepartmentKey != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += "AND B.SendTo = @EmployeeKey";

            // lay thong tin chua thuc hien va chua lien lac duoc
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }

        public static DataTable List(int Type, int DepartmentKey, int EmployeeKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT * FROM SYS_Notification A WHERE A.[TYPE] = @Type AND IsShow = 1 AND IsRead = 0";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @Key ";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception Err)
            {
                zResult = -1;
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
    }
}
