﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SYS
{
    public class WebMenu
    {
        public static DataTable Get()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM Web_Menu WHERE Active = 1 ORDER BY RANK";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zAdapter.Dispose();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception exception)
            {
                string str3 = exception.ToString();
            }
            return zTable;
        }

        public static DataTable GetMenu(string UserKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.RoleName 
FROM SYS_User_Roles A 
LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
WHERE A.UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zAdapter.Dispose();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception exception)
            {
                string str3 = exception.ToString();
            }
            return zTable;
        }
        public static int IsAllow_Menu(string UserKey, string Module)
        {
            int Result = 0;
            string zSQL = @"
SELECT IsRead
FROM SYS_User_Roles A 
LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
WHERE A.UserKey = @UserKey AND B.RoleID = @Module";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                Result = Convert.ToInt32(zCommand.ExecuteScalar());

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception Ex)
            {

            }
            return Result;
        }        
    }
}
