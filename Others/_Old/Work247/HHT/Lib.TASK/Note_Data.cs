﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;

namespace Lib.TASK
{
    public class Note_Data
    {
        //----------------------------- V3
        public static DataTable ListInfo_New(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.InfoKey AS [KEY], '0' AS [RelateKey], '2' AS [TYPE], '' [Status], '' EmployeeName,
A.SirName + ' ' + A.LastName + ' ' + A.FirstName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product
FROM TASK_Excel_Detail A
WHERE A.Status = 0";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_New(string ProjectName, string AssetName, string CustomerName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.InfoKey AS [KEY], '2' AS [TYPE], '0' AS [RelateKey], '' [Status], '' EmployeeName,
A.SirName + ' ' + A.LastName + ' ' + A.FirstName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product
FROM TASK_Excel_Detail A
WHERE A.Status = 0";
            if (ProjectName != string.Empty)
                zSQL += " AND A.Product = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND A.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (A.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_New(int Amount, string ProjectName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.InfoKey AS [KEY], '0' AS [RelateKey], '2' AS [TYPE], '' [Status], '' EmployeeName,
A.SirName + ' ' + A.LastName + ' ' + A.FirstName AS CustomerName,  
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, 
A.Asset, A.Product
FROM TASK_Excel_Detail A
WHERE A.Status = 0";
            if (ProjectName != string.Empty)
                zSQL += " AND A.Product = @Product";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable ListInfo_Sent(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP " + Amount + @" A.DetailKey AS [Key], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status],
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.SirName + ' ' + B.LastName + ' ' + B.FirstName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey 
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey WHERE B.Status = 1 ORDER BY A.DateRespone DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_Sent(string ProjectName, string AssetName, string CustomerName, int EmployeeKey, int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.DetailKey AS [KEY], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status], 
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.SirName + ' ' + B.LastName + ' ' + B.FirstName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey WHERE B.Status = 1";
            if (EmployeeKey != 0)
                zSQL += " AND C.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND A.StatusInfo = @Status";
            if (ProjectName != string.Empty)
                zSQL += " AND B.Product = @Product";
            if (AssetName != string.Empty)
                zSQL += " AND B.Asset = @Asset";
            if (CustomerName != string.Empty)
                zSQL += " AND (B.LastName LIKE @Name OR A.FirstName LIKE @Name)";

            zSQL += " ORDER BY A.DateRespone DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = AssetName;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInfo_Sent(int Amount, string ProjectName)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP " + Amount + @" A.DetailKey AS [KEY], A.InfoKey AS [RelateKey], '1' AS [TYPE],
dbo.FNC_GetStatusInfoData(A.StatusInfo) [Status], 
dbo.FNC_GetNameEmployee(C.SendTo) EmployeeName,
B.SirName + ' ' + B.LastName + ' ' + B.FirstName AS CustomerName,  
B.Phone1, B.Phone2, B.Email1, B.Email2, B.Address1, B.Address2, 
B.Asset, B.Product
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON A.NoteKey = C.NoteKey WHERE B.Status = 1";
           
            if (ProjectName != string.Empty)
                zSQL += " AND B.Product = @Product";         

            zSQL += " ORDER BY A.DateRespone DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = ProjectName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //------------------------------

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM TASK_Note ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM TASK_Note ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Note_New_Paging(int PageSize, int PageNumber)
        {
            string zSQL = @"SELECT * FROM 
(SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, '' NoteDate, '' StatusJob, '' SendTo ,
A.SirName + ' ' + A.FirstName + ' ' + A.LastName AS CustomerName, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, A.ProductID, A.Product, A.Asset, A.Description,
'' EmployeeName
FROM dbo.TASK_Excel_Detail A 
WHERE A.InfoKey NOT IN (SELECT InfoKey FROM TASK_Note_Detail)) X
WHERE X.RowNumber BETWEEN (((@Index - 1) * @PageSize) + 1) AND @Index * @PageSize";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Index ", SqlDbType.Int).Value = PageNumber;
                zCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = PageSize;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note_New()
        {
            string zSQL = @"SELECT * FROM 
(SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, '' NoteDate, '' StatusJob, '' SendTo ,
A.SirName + ' ' + A.FirstName + ' ' + A.LastName AS CustomerName, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, A.ProductID, A.Product, A.Asset, A.Description,
'' EmployeeName
FROM dbo.TASK_Excel_Detail A 
WHERE A.InfoKey NOT IN (SELECT InfoKey FROM TASK_Note_Detail)) X";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note_New(int ExcelKey, int No)
        {
            string zSQL = @"SELECT TOP 100 * FROM 
(SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, '' NoteDate, '' StatusJob, '' SendTo ,
A.SirName + ' ' + A.FirstName + ' ' + A.LastName AS CustomerName, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, A.ProductID, A.Product, A.Asset, A.Description,
'' EmployeeName, A.InfoKey AS [KEY], '2' AS [TYPE]
FROM dbo.TASK_Excel_Detail A 
WHERE A.InfoKey NOT IN (SELECT InfoKey FROM TASK_Note_Detail)";
            if (ExcelKey != 0)
                zSQL += "AND D.ExcelKey = @ExcelKey ";
            zSQL += ") X";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note_New(int ExcelKey, string ProjectName)
        {
            string zSQL = @"SELECT * FROM 
(SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, '' NoteDate, '' StatusJob, '' SendTo ,
A.SirName + ' ' + A.FirstName + ' ' + A.LastName AS CustomerName, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Address1, A.Address2, A.ProductID, A.Product, A.Asset, A.Description,
'' EmployeeName, A.InfoKey AS [KEY], '2' AS [TYPE]
FROM dbo.TASK_Excel_Detail A 
WHERE A.InfoKey NOT IN (SELECT InfoKey FROM TASK_Note_Detail)";
            if (ExcelKey != 0)
                zSQL += " AND A.ExcelKey = @ExcelKey ";
            if (ProjectName != string.Empty)
                zSQL += " AND A.Product = @Name";
            zSQL += ") X";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = ProjectName;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int All_Note_New()
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(A.ExcelKey)
FROM dbo.TASK_Excel_Detail A 
WHERE A.InfoKey NOT IN (SELECT InfoKey FROM TASK_Note_Detail)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static DataTable List_Note(int ExcelKey, int EmployeeKey, int Status, DateTime FromDate, DateTime ToDate, int No)
        {
            string zSQL = @"
SELECT TOP 100 B.NoteDate, 
dbo.FNC_GetStatusInfoData(A.StatusInfo) AS StatusJob, B.SendTo ,
D.SirName + ' ' + D.FirstName + ' ' + D.LastName AS CustomerName, 
D.Phone1, D.Phone2, D.Email1, D.Email2, D.Address1, D.Address2, D.ProductID, D.Product, D.Asset, D.Description,
dbo.FNC_GetNameEmployee(B.SENDTO) AS EmployeeName, A.DetailKey AS [Key], '1' AS [TYPE]
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
WHERE 1= 1";

            if (ExcelKey != 0)
                zSQL += " AND D.ExcelKey = @ExcelKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND NoteDate Between @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND A.StatusInfo = @Status";

            zSQL += " ORDER BY A.DateRespone, StatusJob";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note(int ExcelKey, int EmployeeKey, int Status, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = @"
SELECT B.NoteDate, 
dbo.FNC_GetStatusInfoData(A.StatusInfo) AS StatusJob, B.SendTo ,
D.SirName + ' ' + D.FirstName + ' ' + D.LastName AS CustomerName, 
D.Phone1, D.Phone2, D.Email1, D.Email2, D.Address1, D.Address2, D.ProductID, D.Product, D.Asset, D.Description,
dbo.FNC_GetNameEmployee(B.SENDTO) AS EmployeeName, A.DetailKey AS [Key], '1' AS [TYPE]
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
WHERE 1= 1";

            if (ExcelKey != 0)
                zSQL += " AND D.ExcelKey = @ExcelKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND NoteDate Between @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND A.StatusInfo = @Status";

            zSQL += " ORDER BY A.DateRespone, StatusJob";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note(int EmployeeKey, int Status, string ProjectName, DateTime FromDate, DateTime ToDate, string Name)
        {
            string zSQL = @"
SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, A.*, B.NoteDate, C.Name AS StatusJob, B.SendTo ,
D.SirName + ' ' + D.FirstName + ' ' + D.LastName AS CustomerName, 
D.Phone1, D.Phone2, D.Email1, D.Email2, D.Address1, D.Address2, D.ProductID, D.Product, D.Asset, D.Description,
dbo.FNC_GetNameEmployee(B.SENDTO) AS EmployeeName
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
LEFT JOIN dbo.TASK_Categories C ON C.ID = A.StatusInfo
WHERE 1= 1";

            if (Name != string.Empty)
                zSQL += " AND D.Asset LIKE @Asset";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND NoteDate Between @FromDate AND @ToDate";
            if (ProjectName != string.Empty)
                zSQL += " AND D.Product LIKE @ProjectName";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND C.ID = @Status";

            zSQL += " ORDER BY RowNumber, A.DateRespone, StatusJob";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + ProjectName + "%";
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note(int DepartmentKey, int EmployeeKey, int Status, string ProjectName, DateTime FromDate, DateTime ToDate, string Name)
        {
            string zSQL = @"
SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, A.*, B.NoteDate, C.Name AS StatusJob, B.SendTo ,
D.SirName + ' ' + D.FirstName + ' ' + D.LastName AS CustomerName, 
D.Phone1, D.Phone2, D.Email1, D.Email2, D.Address1, D.Address2, D.ProductID, D.Product, D.Asset, D.Description,
dbo.FNC_GetNameEmployee(B.SENDTO) AS EmployeeName
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
LEFT JOIN dbo.TASK_Categories C ON C.ID = A.StatusInfo
LEFT JOIN dbo.HRM_Employees F ON F.EmployeeKey = B.SendTo
WHERE 1= 1 ";

            if (Name != string.Empty)
                zSQL += " AND D.Asset LIKE @Asset";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND NoteDate Between @FromDate AND @ToDate";
            if (ProjectName != string.Empty)
                zSQL += " AND D.Product LIKE @ProjectName";
            if (DepartmentKey != 0)
                zSQL += " AND F.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND C.ID = @Status";

            zSQL += " ORDER BY RowNumber, A.DateRespone, StatusJob";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + ProjectName + "%";
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note(int EmployeeKey, int DepartmentKey)
        {
            string zSQL = @"
SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, A.*, B.NoteDate, C.Name AS StatusJob, B.SendTo ,
D.SirName + ' ' + D.FirstName + ' ' + D.LastName AS CustomerName, 
D.Phone1, D.Phone2, D.Email1, D.Email2, D.Address1, D.Address2, D.ProductID, D.Product, D.Asset, D.Description,
dbo.FNC_GetNameEmployee(B.SENDTO) AS EmployeeName
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
LEFT JOIN dbo.TASK_Categories C ON C.ID = A.StatusInfo
WHERE A.StatusInfo = 6";

            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";

            if (DepartmentKey != 0)
                zSQL += " AND E.DepartmentKey = @DepartmentKey ";

            zSQL += " ORDER BY StatusJob";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int List_Note_Count(int DepartmentKey, int EmployeeKey, int Status, string ProjectName, DateTime FromDate, DateTime ToDate, string Name)
        {
            string zSQL = @"
SELECT COUNT(A.DetailKey)
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
LEFT JOIN dbo.TASK_Categories C ON C.ID = A.StatusInfo
LEFT JOIN dbo.HRM_Employees F ON F.EmployeeKey = B.SendTo
WHERE 1= 1 ";

            if (Name != string.Empty)
                zSQL += " AND D.Asset LIKE @Asset";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND NoteDate Between @FromDate AND @ToDate";
            if (ProjectName != string.Empty)
                zSQL += " AND D.Product LIKE @ProjectName";
            if (DepartmentKey != 0)
                zSQL += " AND F.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND C.ID = @Status";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            int KQ = 0;
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + ProjectName + "%";
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;

                KQ = zCommand.ExecuteScalar().ToInt();

                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return KQ;
        }
        public static DataTable List_Note(int DepartmentKey, int EmployeeKey, int Status, string ProjectName, DateTime FromDate, DateTime ToDate, string Name, int FromPage, int ToPage)
        {
            string zSQL = @"
SELECT * FROM (
SELECT ROW_NUMBER() OVER (ORDER BY InfoDate DESC) AS RowNumber, A.*, B.NoteDate, C.Name AS StatusJob, B.SendTo ,
D.SirName + ' ' + D.FirstName + ' ' + D.LastName AS CustomerName, 
D.Phone1, D.Phone2, D.Email1, D.Email2, D.Address1, D.Address2, D.ProductID, D.Product, D.Asset,
dbo.FNC_GetNameEmployee(B.SENDTO) AS EmployeeName
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Excel_Detail D ON D.INFOKEY = A.INFOKEY
LEFT JOIN dbo.TASK_Categories C ON C.ID = A.StatusInfo
LEFT JOIN dbo.HRM_Employees F ON F.EmployeeKey = B.SendTo
WHERE 1= 1 ";

            if (Name != string.Empty)
                zSQL += " AND D.Asset LIKE @Asset";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND NoteDate Between @FromDate AND @ToDate";
            if (ProjectName != string.Empty)
                zSQL += " AND D.Product LIKE @ProjectName";
            if (DepartmentKey != 0)
                zSQL += " AND F.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            if (Status != 0)
                zSQL += " AND C.ID = @Status";
            zSQL += ") X WHERE X.RowNumber BETWEEN @FromPage AND @ToPage";
            zSQL += " ORDER BY RowNumber, A.DateRespone, StatusJob";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@FromPage", SqlDbType.Int).Value = FromPage;
                zCommand.Parameters.Add("@ToPage", SqlDbType.Int).Value = ToPage;

                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + ProjectName + "%";
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Note_Chart(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = @"
SELECT C.Name AS StatusJob, COUNT(A.DetailKey) AS [VALUE]
FROM dbo.TASK_Note_Detail A 
LEFT JOIN dbo.TASK_Note B ON B.NoteKey = A.NoteKey
LEFT JOIN dbo.TASK_Categories C ON C.ID = A.StatusInfo
LEFT JOIN dbo.HRM_Employees D ON D.EmployeeKey = B.SendTo
WHERE C.NAME IS NOT NULL AND EmployeeKey IS NOT NULL";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND B.CreatedDate BETWEEN @FromDate AND @ToDate";
            if (DepartmentKey != 0)
                zSQL += " AND D.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";
            zSQL += " GROUP BY C.Name";

            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                if (FromDate != DateTime.MinValue &&
                    ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
