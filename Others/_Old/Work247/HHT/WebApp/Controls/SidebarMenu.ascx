﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarMenu.ascx.cs" Inherits="WebApp.Controls.SidebarMenu" %>
<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse ace-save-state sidebar-fixed" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
    <script type="text/javascript">
        try { ace.settings.loadState('sidebar') } catch (e) { }
    </script>
    <div class="nav-wrap-up pos-rel">
        <div class="nav-wrap">
            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <%--    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
        <button class="btn btn-success">
            <i class="ace-icon fa fa-signal"></i>
        </button>
        <button class="btn btn-info tooltip-info">
            <i class="ace-icon fa fa-pencil"></i>
        </button>
        <button class="btn btn-warning">
            <i class="ace-icon fa fa-users"></i>
        </button>
        <button class="btn btn-danger">
            <i class="ace-icon fa fa-cogs"></i>
        </button>
    </div>
    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
        <span class="btn btn-success"></span>
        <span class="btn btn-info"></span>
        <span class="btn btn-warning"></span>
        <span class="btn btn-danger"></span>
    </div>--%>
            </div>
            <asp:Literal ID="Literal_Menu" runat="server"></asp:Literal>
            <%--<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
    <i id="sidebar-toggle-icon" class="ace-save-state ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>--%>
        </div>
    </div>
</div>
