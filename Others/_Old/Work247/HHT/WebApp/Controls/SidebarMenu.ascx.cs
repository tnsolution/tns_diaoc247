﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Lib.SYS;

namespace WebApp.Controls
{
    public partial class SidebarMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable zTable = WebMenu.Get();
                var sb = new StringBuilder();
                DataRow[] menu = zTable.Select("[Parent] = 0");
           
                Literal_Menu.Text = GenerateUL(menu, zTable, sb, 0);
            }
        }

        string GenerateUL(DataRow[] menu, DataTable table, StringBuilder sb, int level)
        {
            if (level == 0)
                sb.AppendLine("<ul class='nav nav-list'>");
            else
                sb.AppendLine("<ul class='submenu'>");

            if (menu.Length > 0)
            {
                string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

                foreach (DataRow dr in menu)
                {
                    string MenuID = dr["MenuID"].ToString();                    
                    int IsAllow = WebMenu.IsAllow_Menu(UserKey, MenuID);
                    if (IsAllow == 1)
                    {
                        string menuText = dr["MenuName"].ToString();
                        string icon = dr["Icon"].ToString() != "" ? "<i class='menu-icon " + dr["Icon"].ToString() + "'></i>" : "";
                        string handler = dr["Link"].ToString() == "#"
                            ? "<a class='dropdown-toggle' href=" + dr["Link"].ToString() + ">" + icon + "<span class='menu-text'>" + menuText + "</span></a>"
                            : "<a href=" + dr["Link"].ToString() + ">" + icon + "<span class='menu-text'>" + menuText + "</a>";
                        string line = "<li class='hover'>";
                        line += handler;
                        sb.Append(line);

                        string pid = dr["MenuKey"].ToString();
                        string parentId = dr["Parent"].ToString();

                        DataRow[] subMenu = table.Select("[Parent] = " + pid);
                        if (subMenu.Length > 0 && !pid.Equals(parentId))
                        {
                            var subMenuBuilder = new StringBuilder();
                            sb.Append(GenerateUL(subMenu, table, subMenuBuilder, 1));
                        }
                        sb.Append("</li>");
                    }
                }
            }
            sb.Append("</ul>");
            return sb.ToString();
        }
    }
}