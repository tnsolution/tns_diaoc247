﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopLeftMenu.ascx.cs" Inherits="WebApp.Controls.TopLeftMenu" %>
<ul class="nav navbar-nav">
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
            <i class="ace-icon fa fa-bell icon-animated-bell"></i>
            Nhắc nhở
            <span class="badge badge-warning">
                <asp:Literal ID="Lit_TotalRemind" runat="server"></asp:Literal></span>
        </a>
        <ul class="dropdown-menu dropdown-light-blue dropdown-caret">
            <li>
                <a href="../Remind.aspx?Type=1">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Sinh nhật khách
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_Birthday" runat="server"></asp:Literal></span>
                </a>
            </li>
            <li>
                <a href="../Remind.aspx?Type=2">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Giao dịch hết hạn
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_TradeExpired" runat="server"></asp:Literal></span>
                </a>
            </li>
            <li>
                <a href="../Remind.aspx?Type=3">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Sản phẩm tới hẹn
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_AssetExpired" runat="server"></asp:Literal></span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
            <i class="ace-icon fa fa-info-circle"></i>
            Công việc
            <span class="badge badge-warning icon-animated-vertical">
                <asp:Literal ID="Lit_TotalTask" runat="server"></asp:Literal></span>
        </a>
        <ul class="dropdown-menu dropdown-light-blue dropdown-caret">
            <li>
                <a href="/INFO/InfoList.aspx">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Gọi điện
                    <span class="badge badge-warning">
                        <asp:Literal ID="Lit_Task" runat="server"></asp:Literal></span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Lập báo cáo                                    
                </a>
            </li>
            <li>
                <a href="#mScheduleBox" data-toggle="modal">
                    <i class="ace-icon fa fa-info bigger-110 blue"></i>
                    Lập kế hoạch
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">
            <i class="ace-icon fa fa-bullhorn"></i>
            Thông báo
            <span class="badge badge-warning icon-animated-vertical">5</span>
        </a>
    </li>
</ul>
