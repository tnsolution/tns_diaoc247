﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.Controls
{
    public partial class TopLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                int NumBirthday = 0;
                int NumTrade = 0;
                int NumAsset = 0;
                int NumTask = 0;

                switch (UnitLevel)
                {
                    case 0:
                    case 1:
                        NumBirthday = Notification_Data.Count_Notification(1, 0, 0);
                        NumTrade = Notification_Data.Count_Notification(2, 0, 0);
                        NumAsset = Notification_Data.Count_Notification(3, 0, 0);
                        NumTask = Notification_Data.Count_Task(0, 0);
                        break;

                    case 2:
                        NumBirthday = Notification_Data.Count_Notification(1, 0, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, 0, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, 0, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(0, DepartmentKey);
                        break;

                    case 3:
                        NumBirthday = Notification_Data.Count_Notification(1, EmployeeKey, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, EmployeeKey, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, EmployeeKey, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(EmployeeKey, DepartmentKey);
                        break;

                    default:
                        NumBirthday = Notification_Data.Count_Notification(1, EmployeeKey, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, EmployeeKey, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, EmployeeKey, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(EmployeeKey, DepartmentKey);
                        break;
                }

                Lit_Birthday.Text = NumBirthday.ToString();
                Lit_TradeExpired.Text = NumTrade.ToString();
                Lit_AssetExpired.Text = NumAsset.ToString();
                Lit_Task.Text = NumTask.ToString();

                Lit_TotalRemind.Text = (NumBirthday + NumTrade + NumAsset).ToString();
                Lit_TotalTask.Text = NumTask.ToString();
            }
        }
    }
}