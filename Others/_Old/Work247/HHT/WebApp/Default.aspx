﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Trang chủ              
            </h1>
        </div>
       <%-- <div class="row">
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title smaller">Bảng xếp hạng doanh số tháng 8/2017</h4>
                                <div class="widget-toolbar">
                                    <div class="btn-group">
                                        <a class="btn btn-white btn-sm btn-primary"><i class="ace-icon fa fa-arrow-circle-o-left"></i>Trước</a>
                                        <a class="btn btn-white btn-sm btn-primary"><i class="ace-icon fa fa-arrow-circle-o-right"></i>Sau</a>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <table class="table  table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Top 3</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <img src="_Resource\Img\1.png" style="width: 52px;"></td>
                                                        <td><b>Nguyễn Thị Thanh Thủy<br>
                                                            Doanh số: 63,000,000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="_Resource\Img\2.png" style="width: 52px;"></td>
                                                        <td><b>Phạm Thị Kim Huệ<br>
                                                            Doanh số: 38,000,000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="_Resource\Img\3.png" style="width: 52px;"></td>
                                                        <td><b>Phạm Ngọc Quỳnh Hoa<br>
                                                            Doanh số: 36,500,000</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-xs-6">
                                            <table class="table  table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nhân viên</th>
                                                        <th>Doanh số</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>4 : Nguyễn Thị Ngọc Trinh</td>
                                                        <td style="text-align: right">25,500,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5 : Lê Thị Thùy Trang</td>
                                                        <td style="text-align: right">20,000,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6 : Vũ Thu Trang</td>
                                                        <td style="text-align: right">0</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-6"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat">
                                <div class="col-lg-4">
                                    <h4 class="widget-title smaller">Mục tiêu tháng 8</h4>
                                </div>
                                <div class="col-lg-6">
                                    <h4 class="widget-title smaller">Kế hoạch ngày 21/08/2017</h4>
                                </div>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="col-sm-12 col-xs-12 Target">
                                                <div class="TargetText"><span>3</span></div>

                                            </div>
                                            <div class="col-lg-12" style="border-top: 1px solid #00add8;">
                                                <h4>Cố lên!!! Bạn cần thêm 1 giao dịch nữa.
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <table class="table  table-bordered table-hover" id="tableTime">
                                                <thead>
                                                    <tr>
                                                        <th>STT</th>
                                                        <th>Thời gian</th>
                                                        <th>Nội dung</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>8h30-11h</td>
                                                        <td>Gọi điện cập nhật sản phẩm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>11h-12h</td>
                                                        <td>Đăng tin</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>13h30-16h30</td>
                                                        <td>Gọi điện cập nhật sản phẩm</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>17h-18h</td>
                                                        <td>Đăng tin</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="infobox-container">
                    <div class="infobox infobox-pink">
                        <div class="infobox-icon">
                            <i class="ace-icon fa fa-shopping-cart"></i>
                        </div>

                        <div class="infobox-data">
                            <span class="infobox-data-number">8</span>
                            <div class="infobox-content">new orders</div>
                        </div>
                        <div class="stat stat-important">4%</div>
                    </div>
                </div>
            </div>--%>
            <%-- <div class="col-xs-6">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title lighter smaller">
                            <i class="ace-icon fa fa-comment blue"></i>
                            Conversation
                        </h4>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main no-padding">
                            <div class="dialogs ace-scroll">
                                <div class="scroll-track scroll-active" style="display: block; height: 300px;">
                                    <div class="scroll-bar" style="height: 236px; top: 64px;"></div>
                                </div>
                                <div class="scroll-content" style="max-height: 300px;">
                                    <div class="itemdiv dialogdiv">
                                        <div class="user">
                                            <img alt="Alexa's Avatar" src="assets/images/avatars/avatar1.png">
                                        </div>

                                        <div class="body">
                                            <div class="time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span class="green">4 sec</span>
                                            </div>

                                            <div class="name">
                                                <a href="#">Alexa</a>
                                            </div>
                                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

                                            <div class="tools">
                                                <a href="#" class="btn btn-minier btn-info">
                                                    <i class="icon-only ace-icon fa fa-share"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="itemdiv dialogdiv">
                                        <div class="user">
                                            <img alt="John's Avatar" src="assets/images/avatars/avatar.png">
                                        </div>

                                        <div class="body">
                                            <div class="time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span class="blue">38 sec</span>
                                            </div>

                                            <div class="name">
                                                <a href="#">John</a>
                                            </div>
                                            <div class="text">Raw denim you probably haven't heard of them jean shorts Austin.</div>

                                            <div class="tools">
                                                <a href="#" class="btn btn-minier btn-info">
                                                    <i class="icon-only ace-icon fa fa-share"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="itemdiv dialogdiv">
                                        <div class="user">
                                            <img alt="Bob's Avatar" src="assets/images/avatars/user.jpg">
                                        </div>

                                        <div class="body">
                                            <div class="time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span class="orange">2 min</span>
                                            </div>

                                            <div class="name">
                                                <a href="#">Bob</a>
                                                <span class="label label-info arrowed arrowed-in-right">admin</span>
                                            </div>
                                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

                                            <div class="tools">
                                                <a href="#" class="btn btn-minier btn-info">
                                                    <i class="icon-only ace-icon fa fa-share"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="itemdiv dialogdiv">
                                        <div class="user">
                                            <img alt="Jim's Avatar" src="assets/images/avatars/avatar4.png">
                                        </div>

                                        <div class="body">
                                            <div class="time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span class="grey">3 min</span>
                                            </div>

                                            <div class="name">
                                                <a href="#">Jim</a>
                                            </div>
                                            <div class="text">Raw denim you probably haven't heard of them jean shorts Austin.</div>

                                            <div class="tools">
                                                <a href="#" class="btn btn-minier btn-info">
                                                    <i class="icon-only ace-icon fa fa-share"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="itemdiv dialogdiv">
                                        <div class="user">
                                            <img alt="Alexa's Avatar" src="assets/images/avatars/avatar1.png">
                                        </div>

                                        <div class="body">
                                            <div class="time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span class="green">4 min</span>
                                            </div>

                                            <div class="name">
                                                <a href="#">Alexa</a>
                                            </div>
                                            <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>

                                            <div class="tools">
                                                <a href="#" class="btn btn-minier btn-info">
                                                    <i class="icon-only ace-icon fa fa-share"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <form>
                                <div class="form-actions">
                                    <div class="input-group">
                                        <input placeholder="Type your message here ..." type="text" class="form-control" name="message">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-info no-radius" type="button">
                                                <i class="ace-icon fa fa-share"></i>
                                                Send
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.widget-main -->
                    </div>
                    <!-- /.widget-body -->
                </div>
            </div>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
