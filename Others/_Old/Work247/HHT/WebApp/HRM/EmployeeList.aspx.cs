﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class EmployeeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Employees_Data.List();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Chức vụ</th>");
            zSb.AppendLine("        <th>Cấp (Level)</th>");
            zSb.AppendLine("        <th>SĐT</th>");
            zSb.AppendLine("        <th>Ngày sinh</th>");
            zSb.AppendLine("        <th>Giới tính</th>");
            zSb.AppendLine("        <th>Ngày bắt đầu</th>");
            zSb.AppendLine("        <th>Ngày nghĩ việc</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Quản lý trực tiếp</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {

                    zSb.AppendLine("            <tr id='" + r["EmployeeKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (i++) + "</td>");
                    zSb.AppendLine("            <td>" + r["LastName"].ToString() + " " + r["FirstName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Position"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Class"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Phone1"].ToString() + "</td>");

                    if (r["Birthday"] != DBNull.Value)
                        zSb.AppendLine("            <td>" + Convert.ToDateTime(r["Birthday"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("            <td></td>");

                    if (r["Gender"].ToString() == "1")
                        zSb.AppendLine("            <td>Nam</td>");
                    else
                        zSb.AppendLine("            <td>Nữ</td>");

                    if (r["DateStart"] != DBNull.Value)
                        zSb.AppendLine("            <td>" + Convert.ToDateTime(r["DateStart"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("            <td></td>");

                    if (r["DateEnd"] != DBNull.Value)
                        zSb.AppendLine("            <td>" + Convert.ToDateTime(r["DateEnd"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("            <td></td>");

                    string img = "<img id='" + r["EmployeeKey"].ToString() + "' check='" + r["IsWorking"].ToInt() + "' alt='' src='" + String.Format("../template/custom-image/{0}.png", r["IsWorking"].ToInt() == 2 ? "true" : "false") + "' />";
                    zSb.AppendLine("            <td>" + img + "</td>");
                    zSb.AppendLine("            <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["ManagerName"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td colspan='10'>Chưa có dữ liệu<td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemResult Delete(int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            Employees_Info zInfo = new Employees_Info();
            zInfo.Key = EmployeeKey;
            zInfo.Delete();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult SetStatus(int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            Employees_Info zInfo = new Employees_Info();
            zInfo.Key = EmployeeKey;
            zInfo.SetActivated();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}