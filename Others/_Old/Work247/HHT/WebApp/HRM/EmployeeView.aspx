﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="EmployeeView.aspx.cs" Inherits="WebApp.HRM.EmployeeView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết nhân viên
                <asp:Literal ID="txt_EmployeeName" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trờ về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin cá nhân
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#company">
                                <i class="blue ace-icon fa fa-user bigger-110"></i>
                                Công ty
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#contact">
                                <i class="blue ace-icon fa fa-phone bigger-110"></i>
                                Liên lạc
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane in active">
                            <div class="col-xs-12">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Giới tính</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="DDL_Gender" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Ngày sinh</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Birthday" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            CMND/Passport
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_IDCard" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Ngày cấp
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_IDDate" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Nơi cấp
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_IDPlace" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="company" class="tab-pane">
                            <div class="col-xs-12">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Phòng ban</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="DDL_Department" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Quản lý trực tiếp</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="DDL_Manager" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Chức vụ nhân viên</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="DDL_Position" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Cấp độ (Level)</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Class" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                      <div class="profile-info-row">
                                        <div class="profile-info-name">Cấp sử dụng</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Unit" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Tình trạng</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="DDL_IsWorking" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Ngày vào làm</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_DateStart" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Ngày nghĩ việc</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_DateEnd" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="contact" class="tab-pane">
                            <div class="col-xs-12">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Mã số thuế
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_TaxNumber" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Địa chỉ thường trú
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Address1" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Địa chỉ tạm trú
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Address2" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Số xe
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_CarNumber" runat="server"></asp:Literal>
                                        </div>
                                    </div>


                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            ĐT1
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Phone1" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Email1
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Email1" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin hệ thống</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_EmployeeKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(function () {
            $("#btnEdit").click(function () {
                window.location = "EmployeeEdit.aspx?ID=" + Page.getUrlParameter("ID");
            });
        });
    </script>
</asp:Content>
