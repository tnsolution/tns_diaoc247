﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class EmployeeView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckRoles();

            if (Request["ID"] != null)
                HID_EmployeeKey.Value = Request["ID"];

            if (HID_EmployeeKey.Value != string.Empty &&
                HID_EmployeeKey.Value != "0")
            {
                Employees_Info zInfo = new Employees_Info(HID_EmployeeKey.Value.ToInt());
                txt_EmployeeName.Text = zInfo.LastName + " " + zInfo.FirstName;
                DDL_Position.Text = zInfo.Position;
                DDL_Department.Text = zInfo.DepartmentName;
                DDL_Manager.Text = zInfo.ManagerName;
                DDL_IsWorking.Text = zInfo.IsWorking == 2 ? "Đang làm việc" : "Đã nghĩ việc";
                DDL_Gender.Text = zInfo.Gender == 1 ? "Nam" : "Nữ";
                txt_Unit.Text = zInfo.UnitDescription;
                txt_Class.Text = zInfo.Class;
                txt_Birthday.Text = zInfo.Birthday.ToString("dd/MM/yyyy");
                txt_IDCard.Text = zInfo.IDCard;
                txt_IDDate.Text = zInfo.IDDate.ToString("dd/MM/yyyy");
                txt_IDPlace.Text = zInfo.IDPlace;
                txt_TaxNumber.Text = zInfo.TaxNumber;
                txt_DateEnd.Text = zInfo.DateEnd.ToString("dd/MM/yyyy");
                txt_DateStart.Text = zInfo.DateStart.ToString("dd/MM/yyyy");
                txt_Address1.Text = zInfo.Address1;
                txt_Address2.Text = zInfo.Address2;
                txt_CarNumber.Text = zInfo.CarNumber;
                txt_Phone1.Text = zInfo.Phone1;

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();
            }
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Điều chỉnh
                        </button>";
            }
        }
        #endregion
    }
}