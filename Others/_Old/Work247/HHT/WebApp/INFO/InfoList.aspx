﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfoList.aspx.cs" Inherits="WebApp.INFO.InfoList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Nguồn tin<small>cần khai tác, đã nhập, đã gửi, đang xử lý,...</small>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <!--Search-->
    <div class="modal fade" id="mSearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Điều kiện tìm kiếm</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Thông tin</label>
                                <asp:DropDownList ID="DDL_Type" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn đã gửi, chưa gửi--" disabled="disabled"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="--Đã gửi--" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="--Chưa gửi--"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group" type="1">
                                <label class="control-label no-padding-right" for="form-field-1">Tình trạng</label>
                                <asp:DropDownList ID="DDL_Status" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group" type="1">
                                <label class="control-label no-padding-right" for="form-field-1">Nhân viên</label>
                                <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Dự án</label>
                                <asp:DropDownList ID="DDL_Project" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txt_Name" runat="server" CssClass="form-control" Text="" placeholder="-- Mã sản phẩm--"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSearch" runat="server" onserverclick="btnSearch_ServerClick">
                        <i class="ace-icon fa fa-search-plus"></i>
                        Tìm
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mSend" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Gửi thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:Literal ID="Lit_mTitle" runat="server"></asp:Literal>
                            <div class="space-6"></div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Chọn dự án cần gửi</label>
                                <asp:DropDownList ID="DDL_ProjectToSend" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%" required>
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Nhập số thông tin cần gửi</label>
                                <asp:TextBox ID="txt_InfoNo" runat="server" CssClass="form-control" Text="" placeholder="..." required></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Nhân viên cần gửi đến</label>
                                <asp:DropDownList ID="DDL_ToEmployee" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%" required>
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                             <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Ghi chú</label>
                                <asp:TextBox ID="txt_Description" runat="server" CssClass="form-control" Text="" placeholder="..." ></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSend" runat="server" onserverclick="btnSend_ServerClick">
                        <i class="ace-icon fa fa-send-o"></i>
                        Gửi
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mExcel" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Chọn tập tin Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Tập tin excel</label>
                                <div class="row">
                                    <div class="col-md-8">
                                        <asp:FileUpload ID="FU" runat="server" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                    </div>
                                </div>
                                <p class="help-block">
                                    Tập tin excel bị trùng sẽ tự động xóa.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnImport" runat="server" onserverclick="btnImport_Click">
                        <i class="ace-icon fa fa-upload"></i>
                        Tải tập tin
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $(".select2").select2({ width: "100%" });
            $("[id$=btnSend]").click(function () {
                var valid = validate();
                if (!valid)
                    return false;
            });
            $("a[data-target]").click(function () {
                var table = $("#tblData tbody").length;
                if (table < 0) {
                    Page.showNotiMessageInfo("Thông báo", "Không có thông tin để xử lý gửi data")
                }
            });
            $("[id$=DDL_Type]").change(function (e) {
                var type = this.value;
                if (type == 1) {
                    $("div[type='1']").show();
                }
                else {
                    $("div[type='1']").hide();
                }
            });
            $("#tblData tbody tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                var trtype = $(this).closest('tr').attr('type');
                window.location = "InfoView.aspx?ID=" + trid + "&Type=" + trtype;
            });
            $("#mSearch").on('shown.bs.modal', function (e) {
                var type = $("[id$=DDL_Type]").val();
                if (type == 1) {
                    $("div[type='1']").show();
                }
                else {
                    $("div[type='1']").hide();
                }
            })
        });

        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
