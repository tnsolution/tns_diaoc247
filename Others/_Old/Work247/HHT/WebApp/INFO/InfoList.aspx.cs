﻿using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.INFO
{
    public partial class InfoList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();

                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_ToEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Status, "SELECT ID, Name FROM TASK_Categories", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT DISTINCT Product, Product FROM TASK_Excel_Detail", false);

                LoadData();
            }
        }

        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            SearchData();
        }

        void LoadData()
        {
            DataTable zTable = new DataTable();
            int Status = DDL_Status.SelectedValue.ToInt();
            int Type = DDL_Type.SelectedValue.ToInt();
            int Employee = DDL_Employee.SelectedValue.ToInt();
            string ProjectName = DDL_Project.SelectedItem.Text;
            if (Type == 1)
            {
                zTable = Note_Data.ListInfo_Sent(1000);
            }
            else
            {
                zTable = Note_Data.ListInfo_New(1000);
            }
            View_HtmlTable(zTable);
            View_HtmlSend(zTable);
        }
        void SearchData()
        {
            DataTable zTable = new DataTable();
            int Status = DDL_Status.SelectedValue.ToInt();
            int Type = DDL_Type.SelectedValue.ToInt();
            int Employee = DDL_Employee.SelectedValue.ToInt();
            string CustomerName = string.Empty;
            string ProjectName = string.Empty;
            if (DDL_Project.SelectedItem.Value.ToInt() > 0)
                ProjectName = DDL_Project.SelectedItem.Text;

            //1 truy xuất data đã gửi, 2 data chưa gửi
            if (Type == 1)
            {
                zTable = Note_Data.ListInfo_Sent(ProjectName, txt_Name.Text, CustomerName, Employee, Status);
            }
            else
            {
                zTable = Note_Data.ListInfo_New(ProjectName, txt_Name.Text, CustomerName);
            }
            View_HtmlTable(zTable);
            View_HtmlSend(zTable);
        }

        void View_HtmlTable(DataTable TableSearch)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Thông tin</th>");
            zSb.AppendLine("        <th style='width:40%'>Địa chỉ</th>");
            zSb.AppendLine("        <th>Sản phẩm</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (TableSearch.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in TableSearch.Rows)
                {
                    string phone1 = r["Phone1"].ToString() == string.Empty ? string.Empty : r["Phone1"].ToString() + "<br />";
                    string phone2 = r["Phone2"].ToString() == string.Empty ? string.Empty : r["Phone2"].ToString() + "<br />";
                    string mail1 = r["Email1"].ToString() == string.Empty ? string.Empty : r["Email1"].ToString() + "<br />";
                    string mail2 = r["Email2"].ToString() == string.Empty ? string.Empty : r["Email2"].ToString();
                    string address1 = r["Address1"].ToString() == string.Empty ? string.Empty : "Liên hệ: " + r["Address1"].ToString() + "<br />";
                    string address2 = r["Address2"].ToString() == string.Empty ? string.Empty : "Thường trú: " + r["Address2"].ToString();
                    string status = r["Status"].ToString() == string.Empty ? string.Empty : "Xử lý: " + r["Status"].ToString() + "<br/>";
                    string employee = r["EmployeeName"].ToString() == string.Empty ? string.Empty : r["EmployeeName"].ToString();

                    zSb.AppendLine("            <tr id=" + r["Key"].ToString() + " relatekey=" + r["RelateKey"] + " type=" + r["Type"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>Họ tên: " + r["CustomerName"].ToString() + "<br/>Điện thoại: " + phone1 + phone2 + mail1 + mail2 + "</td>");
                    zSb.AppendLine("               <td>" + address1 + address2 + " </td>");
                    zSb.AppendLine("               <td>" + r["Product"].ToString() + "<br/>" + r["Asset"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + status + employee + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                if (_Permitsion[1].ToInt() == 1)
                    Lit_Button.Text = ShowButton_Search() + ShowButton_Import() + ShowButton_Send();
                else
                    Lit_Button.Text = ShowButton_Search();
            }
            else
            {
                if (_Permitsion[1].ToInt() == 1)
                    Lit_Button.Text = ShowButton_Search() + ShowButton_Import();
                else
                    Lit_Button.Text = ShowButton_Search();
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        void View_HtmlSend(DataTable TableSearch)
        {
            DataView zView = new DataView(TableSearch);
            DataTable zTable = zView.ToTable(true, "Product");

            DDL_ProjectToSend.DataSource = zTable;
            DDL_ProjectToSend.DataTextField = "Product";
            DDL_ProjectToSend.DataValueField = "Product";
            DDL_ProjectToSend.DataBind();

            string zMess = "";
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow r = zTable.Rows[i];
                DataTable tbl = TableSearch.Select("[Product] = '" + r["Product"].ToString() + "'").CopyToDataTable();
                if (tbl.Rows.Count > 0)
                {
                    zMess += "Dự án " + r["Product"].ToString() + " có " + tbl.Rows.Count + " thông tin <br/>";
                }
            }

            Lit_mTitle.Text = "Kết quả tìm kiếm được là: <br/>" + zMess;
        }

        string ShowButton_Search()
        {
            return @"
                    <a href = '#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mSearch'>
                        <i class='ace-icon fa fa-search icon-only bigger-110'></i>
                        Tìm lọc
                    </a>";
        }
        string ShowButton_Send()
        {
            return @"
                    <a href = '#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mSend'>
                        <i class='ace-icon fa fa-send'></i>
                        Gửi data
                    </a>";
        }
        string ShowButton_Import()
        {
            return @"
                    <a href = '#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mExcel'>
                        <i class='ace-icon fa fa-plus'></i>
                        Nhập mới data
                    </a>";
        }

        #region [Send Info]

        /*
         * vừa gửi và vừa chuyển tiếp thông tin
         * Key relate là infokey
         * Key là detailkey
         * khi lưu mới thi lưu infokey
         * khi chuyển tiếp thì update detailkey và insert relatekey
         * type 1 là chuyển, 2 là lưu mới
         * TASK_Excel_Detail là bảng chưa thông tin 
         * TASK_Note_Detail là bảng gửi cho nhân viên lấy thông tin liên kết từ excel,
         */

        void SendData()
        {
            DataTable zTable = new DataTable();
            int Type = DDL_Type.SelectedValue.ToInt();
            int Amount = txt_InfoNo.Text.ToInt();
            string ProjectName = string.Empty;
            if (DDL_Project.SelectedItem.Value.ToInt() > 0)
                ProjectName = DDL_Project.SelectedItem.Text;
            if (Type == 1)
            {
                zTable = Note_Data.ListInfo_Sent(Amount, ProjectName);
            }
            else
            {
                zTable = Note_Data.ListInfo_New(Amount, ProjectName);
            }

            if (zTable.Rows.Count > 0)
            {
                #region [NoteInfo]
                Note_Info zInfo = new Note_Info();
                zInfo.SendFrom = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SendTo = DDL_ToEmployee.SelectedValue.ToInt();
                zInfo.Description = txt_Description.Text.Trim();
                zInfo.Product = DDL_Project.Text;
                zInfo.Amount = Amount;
                zInfo.NoteDate = DateTime.Now;
                zInfo.CreatedDate = DateTime.Now;
                zInfo.ModifiedDate = DateTime.Now;
                zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zInfo.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zInfo.Save();
                #endregion

                if (zInfo.Message != string.Empty)
                    Response.Write("<script>alert('Lỗi gửi thông tin !');</script>");

                string Insert = "";
                string Update = "";
                string Message = "";
                switch (Type)
                {
                    case 1:
                        foreach (DataRow r in zTable.Rows)
                        {
                            Update += " UPDATE TASK_Note_Detail SET StatusInfo = 8 WHERE DetailKey = " + r["KEY"].ToString();
                            //8 chuyen thong tin
                            Insert += " INSERT INTO TASK_Note_Detail (NoteKey ,InfoKey ,StatusInfo  ) VALUES ('" + zInfo.NoteKey + "' ,'" + r["RelateKey"].ToString() + "' , '" + 6 + "') ";
                            //6 mặc định chưa hoàn thành trong bảng SYS_Categories
                            Insert += " INSERT INTO SYS_LogTranfer (OldStaff,NewStaff,ObjectTable,ObjectID,Note,CreatedDate) VALUES (" + DDL_Employee.SelectedValue + "," + DDL_ToEmployee.SelectedValue + ",N'TASK_Note_Detail'," + r["RelateKey"].ToString() + ",N'Chuyen Tiep',GETDATE())";
                        }

                        Message = CustomInsert.Exe(Insert);
                        if (Message != string.Empty)
                            Response.Write("<script>alert('Lỗi gửi chi tiết thông tin !');</script>");
                        Message = CustomInsert.Exe(Update);
                        if (Message != string.Empty)
                            Response.Write("<script>alert('Lỗi gửi chi tiết thông tin !');</script>");
                        break;

                    case 2:
                        foreach (DataRow r in zTable.Rows)
                        {
                            Insert += " INSERT INTO TASK_Note_Detail (NoteKey ,InfoKey ,StatusInfo  ) VALUES ('" + zInfo.NoteKey + "' ,'" + r["KEY"].ToString() + "' , '" + 6 + "') ";
                            //6 mặc định chưa hoàn thành trong bảng TASK_Categories
                            Update += " UPDATE TASK_Excel_Detail SET Status = 1 WHERE InfoKey =" + r["KEY"].ToString();
                        }

                        Message = CustomInsert.Exe(Insert);
                        if (Message != string.Empty)
                            Response.Write("<script>alert('Lỗi gửi chi tiết thông tin !');</script>");

                        Message = CustomInsert.Exe(Update);
                        if (Message != string.Empty)
                            Response.Write("<script>alert('Lỗi gửi chi tiết thông tin !');</script>");
                        break;

                    default:
                        Response.Write("<script>alert('Không có thông tin gửi !');</script>");
                        Response.Redirect(Request.RawUrl);
                        break;
                }
            }

            Response.Write("<script>alert('Đã gửi thông tin thành công !');</script>");
            Response.Redirect(Request.RawUrl);
        }

        protected void btnSend_ServerClick(object sender, EventArgs e)
        {
            SendData();
        }
        #endregion

        #region [Upload Excel]
        void UploadExcel()
        {
            string pathSave = "";
            string FileName = "";
            if (FU.HasFile)
            {
                try
                {
                    #region[Upload]   
                    string virtualPath;
                    string nFullPathName = "/Upload/Excel/";
                    virtualPath = HttpContext.Current.Request.ApplicationPath + nFullPathName;
                    string virtualPathRoot = HttpContext.Current.Request.ApplicationPath;
                    string pathRoot = Server.MapPath(virtualPathRoot);
                    pathSave = Server.MapPath(virtualPath) + DateTime.Now.ToString("yyyy-MM-dd");
                    string PathRead = pathRoot + nFullPathName + FU.FileName;

                    // Check Foder
                    DirectoryInfo nDir = new DirectoryInfo(pathSave);
                    if (!nDir.Exists)
                        nDir.Create();
                    // Check File

                    int LengFile = FU.PostedFile.ContentLength;
                    FileName = FU.FileName;

                    if (File.Exists(pathSave + "\\" + FileName))
                        File.Delete(pathSave + "\\" + FileName);

                    FU.SaveAs(pathSave + "\\" + FileName);

                    LoadData();
                    #endregion

                    #region [Import]
                    string Message = ImportExcel(pathSave + "\\" + FileName, Path.GetExtension(FU.FileName));
                    if (Message != string.Empty)
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "Page.showNotiMessageError('Thông báo !','" + Message + "');", true);
                    #endregion                    
                }
                catch (Exception ex)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "Page.showNotiMessageError('Thông báo !','Lỗi trong khi tải tập tin');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "Page.showNotiMessageInfo('Thông báo !','Bạn chưa chọn tập tin');", true);
            }
        }
        string ImportExcel(string Path, string Extension)
        {
            DataTable zTable = Tools.ImportToDataTable(Path, "0");
            StringBuilder SQL = new StringBuilder();

            if (zTable != null &&
                zTable.Rows.Count != 0 &&
                zTable.Columns.Count > 1)
            {
                Excel_Info zExcelFile = new Excel_Info();
                zExcelFile.Date = DateTime.Now;
                zExcelFile.FileAttack = Path;
                zExcelFile.CreatedDate = DateTime.Now;
                zExcelFile.ModifiedDate = DateTime.Now;
                zExcelFile.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zExcelFile.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zExcelFile.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zExcelFile.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zExcelFile.Create();

                if (zExcelFile.Message == string.Empty)
                {
                    StringBuilder zSb = new StringBuilder();
                    zSb.AppendLine("<table class='table table-hover table-bordered' id='tableInfomation'>");
                    zSb.AppendLine("   <thead>");
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <th>STT</th>");
                    zSb.AppendLine("        <th>Họ tên</th>");
                    zSb.AppendLine("        <th>SDT</th>");
                    zSb.AppendLine("        <th>Email</th>");
                    zSb.AppendLine("        <th>Địa chỉ</th>");
                    zSb.AppendLine("        <th>Dự án</th>");
                    zSb.AppendLine("        <th>Sản phẩm</th>");
                    zSb.AppendLine("    </tr>");
                    zSb.AppendLine("   </thead>");
                    zSb.AppendLine("        <tbody>");

                    int i = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr>");
                        zSb.AppendLine("            <td>" + i++ + "</td>");
                        zSb.AppendLine("               <td>" + r[1].ToString() + " " + r[2].ToString() + " " + r[3].ToString() + "</td>");

                        string Phone1 = "";
                        string Phone2 = "";

                        if (r[4].ToString().Contains("/"))
                        {
                            Phone1 = r[4].ToString().Split('/')[0];
                            Phone2 = r[4].ToString().Split('/')[1];

                            zSb.AppendLine("               <td>" + Phone1 + "<br>" + Phone2 + "</td>");
                        }
                        else
                        {
                            Phone1 = r[4].ToString();
                            Phone2 = r[5].ToString();

                            zSb.AppendLine("               <td>" + Phone1 + "<br>" + Phone2 + "</td>");
                        }

                        string Email1 = "";
                        string Email2 = "";
                        if (r[6].ToString().Contains("/"))
                        {
                            Email1 = r[6].ToString().Split('/')[0];
                            Email2 = r[6].ToString().Split('/')[1];

                            zSb.AppendLine("               <td>" + Email1 + "<br>" + Email2 + "</td>");
                        }
                        else
                        {
                            Email1 = r[6].ToString();
                            Email2 = r[7].ToString();

                            zSb.AppendLine("               <td>" + Email1 + "<br>" + Email2 + "</td>");
                        }

                        zSb.AppendLine("               <td><b>Thường trú:</b> " + r[8].ToString() + "<br><b>Liên hệ:</b> " + r[9].ToString() + "</td>");
                        zSb.AppendLine("               <td>" + r[11].ToString() + "</td>");
                        zSb.AppendLine("               <td>" + r[12].ToString() + "</td>");
                        zSb.AppendLine("            </tr>");

                        SQL.AppendLine(@" 
INSERT INTO TASK_Excel_Detail (ExcelKey, InfoDate ,SirName ,LastName ,FirstName ,Phone1 ,Phone2 ,Email1 ,Email2 ,Address1 ,Address2, ProductID ,Product ,Asset, Status) 
VALUES ('" + zExcelFile.AutoKey + "','" + DateTime.Now + "', N'" + r[1].ToString().Trim() + "', N'" + r[2].ToString().Trim() + "', N'" + r[3].ToString().Trim() + "', N'" + r[4].ToString().Trim() + "', N'" + r[5].ToString().Trim() + "', N'" + Email1 + "', N'" + Email2 + "',N'" + r[8].ToString().Trim() + "', N'" + r[9].ToString().Trim() + "',N'" + r[10].ToString().Trim() + "',N'" + r[11].ToString().Trim() + "',N'" + r[12].ToString().Trim() + "', '0') ");
                    }

                    zSb.AppendLine("        </tbody>");
                    zSb.AppendLine("</table>");

                    string Mess = CustomInsert.Exe(SQL.ToString());
                    if (Mess != string.Empty)
                    {
                        return "Lỗi nhập chi tiết excel không thành công !";
                    }
                }
                else
                {
                    return "Lỗi nhập thông tin file excel không thành công !";
                }
            }
            else
            {
                return "Lỗi đọc file Excel !" + zTable.Rows[0].ToString();
            }

            return string.Empty;
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            UploadExcel();

            //sau khi upload load data mới upload chưa gửi
            DDL_Type.SelectedValue = "2";
            SearchData();
        }
        #endregion

        #region [Check roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS04";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
        }
        #endregion
    }
}