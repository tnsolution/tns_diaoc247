﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfoView.aspx.cs" Inherits="WebApp.INFO.InfoView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết thông tin
                <asp:Literal ID="txt_InfoName" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <span class="tools pull-right">
                        <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                        <button class="btn btn-white btn-info btn-bold">
                            <i class="ace-icon fa fa-adjust info"></i>
                            Cập nhật chủ nhà
                        </button>
                        <button class="btn btn-white btn-info btn-bold">
                            <i class="ace-icon fa fa-floppy-o blue"></i>
                            Xử lý tin
                        </button>
                        <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                            <i class="ace-icon fa fa-reply blue"></i>
                            Trờ về
                        </button>
                    </span>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="profile-user-info profile-user-info-striped">
                    <div class="profile-info-row">
                        <div class="profile-info-name">Dự án</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_ProjectName" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Sản phẩm</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_AssetName" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Họ tên</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_CustomerName" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">SĐT</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_Phone" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Địa chỉ liên lạc</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_Address1" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Địa chỉ thường trú</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_Address2" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Tình trạng</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_Status" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Nhân viên xử lý</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_EmployeeName" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="space-6"></div>
                <div class="col-xs-12">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#process">
                                    <i class="blue ace-icon fa fa-adjust bigger-110"></i>
                                    Xử lý thông tin
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#relate">
                                    <i class="blue ace-icon fa fa-info bigger-110"></i>
                                    Thông tin liên quan
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="process" class="tab-pane in active">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <asp:DropDownList ID="DDL_Status" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Phản hồi--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" id="Product">
                                        <asp:DropDownList ID="DDL_Category_Product" class="form-control select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Sản phẩm là--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" id="Project">
                                        <asp:DropDownList ID="DDL_Project" class="form-control select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" id="Category">
                                        <asp:DropDownList ID="DDL_Category" CssClass="form-control select2" runat="server">
                                            <asp:ListItem Value="0" Text="--Chọn loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="relate" class="tab-pane">
                                <h6>Các thông tin liên quan cùng mã sản phẩm đã có trong phần mềm !.</h6>
                                <asp:Literal ID="Lit_Relate" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin hệ thống</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Key" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Type" runat="server" Value="1" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $(".select2").select2({ width: "100%" });

            $("#btnEdit").click(function () {
                window.location = "InfoEdit.aspx?ID=" + Page.getUrlParameter("ID");
            });

            $("#tblData tbody tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                var trtype = $(this).closest('tr').attr('type');
                if (trid != undefined && trtype != undefined) {
                    window.location = "/SAL/ProductView.aspx?ID=" + trid + "&Type=" + trtype;
                }
            });

            $('#Product').hide();
            $('#Category').hide();
            $('#Project').hide();
            $('[id$=DDL_Status]').on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected == 1) {
                    $('#Product').show();
                    $('#Category').show();
                    $('#Project').show();
                }
                else {
                    $('#Product').hide();
                    $('#Category').hide();
                    $('#Project').hide();
                }
            });
        });
    </script>
</asp:Content>
