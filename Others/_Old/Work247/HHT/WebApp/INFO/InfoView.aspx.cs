﻿using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.INFO
{
    public partial class InfoView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRoles();

                if (Request["ID"] != null)
                    HID_Key.Value = Request["ID"];

                if (Request["Type"] != null)
                    HID_Type.Value = Request["Type"];

                Tools.DropDown_DDL(DDL_Status, "SELECT ID, Name FROM TASK_Categories ORDER BY Name", false);
                Tools.DropDown_DDL(DDL_Category_Product, "SELECT TableName, CategoryName FROM SYS_Table WHERE TYPE = 2 ORDER BY Rank", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);

                LoadInfo();
                LoadRelate();
            }
        }

        void LoadInfo()
        {
            if (HID_Key.Value != string.Empty &&
                HID_Key.Value != "0")
            {
                //HID_Type: 1 truy xuất data đã gửi, 2 data chưa gửi
                Excel_Detail_Info zInfo = new Excel_Detail_Info(HID_Key.Value.ToInt(), HID_Type.Value.ToInt());
                txt_ProjectName.Text = zInfo.Product;
                txt_AssetName.Text = zInfo.Asset;
                txt_CustomerName.Text = zInfo.SirName + " " + zInfo.LastName + " " + zInfo.FirstName;
                txt_Address1.Text = zInfo.Address1;
                txt_Address2.Text = zInfo.Address2;
                txt_Phone.Text = zInfo.Phone1 + " " + zInfo.Phone2;
                txt_Status.Text = zInfo.Status.ToString();
                txt_EmployeeName.Text = zInfo.EmployeeName;

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();
            }
        }
        void LoadRelate()
        {
            DataTable zTable = Product_Data.CheckExsist(txt_AssetName.Text);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {                    
                    zSb.AppendLine("<tr id='" + r["AssetKey"].ToString() + "' type='" + r["Type"].ToString() + "'><td><a href='#'>Dự án: " + r["ProjectName"].ToString() + "<br/> Sản phẩm: " + r["AssetID"].ToString() + "<br/>Chủ nhà: " + r["OwnerInfo"].ToString() + "</a></td></tr>");
                }
            }
            else
            {
                zSb.AppendLine("<tr><td>Chưa có thông tin</td></tr>");
            }
            zSb.AppendLine("</table>");

            Lit_Relate.Text = zSb.ToString();
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS04";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Điều chỉnh
                        </button>";
            }
        }
        #endregion
    }
}