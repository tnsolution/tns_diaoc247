﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProjectEdit.aspx.cs" Inherits="WebApp.JOB.ProjectEdit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
        <div class="breadcrumb">
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1>Project Edit
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Edit project info
                                </small>
                <span class="tools pull-right">
                    <button class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Save
                    </button>
                    <button class="btn btn-white btn-default btn-bold" onclick="goBack()">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Back
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs " id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#general" aria-expanded="true">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                General Info
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#customer" aria-expanded="false">
                                <i class="blue ace-icon fa fa-user bigger-110"></i>
                                Customer Info
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#documents" aria-expanded="false">
                                <i class="ace-icon glyphicon glyphicon-file"></i>
                                Documents
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#gallery" aria-expanded="false">
                                <i class="ace-icon fa fa-picture-o"></i>
                                Gallery
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#staff" aria-expanded="false">
                                <i class="ace-icon fa fa-users"></i>
                                Staffs involved
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#task" aria-expanded="false">
                                <i class="blue ace-icon fa fa-envelope-o bigger-110"></i>
                                Tasks
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#Source" aria-expanded="false">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Source
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#payment" aria-expanded="false">
                                <i class="blue ace-icon fa fa-credit-card bigger-110"></i>
                                Payment
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active">
                            <div class="form-horizontal" role="form">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Project Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="txtProject" runat="server" placeholder="Name of project" class="col-xs-10 col-sm-12" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Content</label>
                                        <div class="col-sm-10">
                                            <textarea id="txtNote" runat="server" class="col-xs-10 col-sm-12" placeholder="Project detail infomation"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="col-lg-6 ">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Start date</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="txtStartDate" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Due date</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="Text1" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Warranty date</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="Text4" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Status</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="Text2" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Priority</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="Text3" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Money</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="txtMoney" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="customer" class="tab-pane">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Name</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control search-query" placeholder="Company, customer name" />
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-purple btn-sm">
                                                            <span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
                                                            Search
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Address</label>

                                            <div class="col-sm-9">
                                                <input type="text" id="txtAddress" runat="server" placeholder="Address info" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Email</label>

                                            <div class="col-sm-9">
                                                <input type="text" id="txtEmail" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Phone</label>

                                            <div class="col-sm-9">
                                                <input type="text" id="txtPhone" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Note</label>
                                            <div class="col-sm-9">
                                                <textarea placeholder="" id="Textarea1" runat="server" class="col-xs-10 col-sm-12"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="widget-box">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title">Contact</h4>
                                    <div class="widget-toolbar">
                                        <a data-toggle="modal" href="#mdtask" data-action="">
                                            <i class="ace-icon fa fa-plus purple"></i>
                                            Add New
                                        </a>
                                        <a href="#" data-action="reload">
                                            <i class="ace-icon fa fa-refresh"></i>
                                            Reload
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <table class="table table-bordered table-hover" id="tableContact">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Department</th>
                                                    <th>Notes</th>
                                                    <th>..</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="task" class="tab-pane">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title">Create task for staff</h4>
                                    <div class="widget-toolbar">
                                        <a data-toggle="modal" href="#mdtask" data-action="">
                                            <i class="ace-icon fa fa-plus purple"></i>
                                            Create
                                        </a>
                                        <a href="#" data-action="reload">
                                            <i class="ace-icon fa fa-refresh"></i>
                                            Reload
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <table class="table table-bordered table-hover" id="tableContact">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Task</th>
                                                    <th>Staff</th>
                                                    <th>Status</th>
                                                    <th>File attacks</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <asp:Literal ID="Literal_ContactTable" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="documents" class="tab-pane">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title">Contract, test records, ...</h4>
                                    <div class="widget-toolbar">
                                        <a data-toggle="modal" href="#mdContact" data-action="">
                                            <i class="ace-icon fa fa-plus purple"></i>
                                            Select
                                        </a>
                                        <a href="#" data-action="reload">
                                            <i class="ace-icon fa fa-refresh"></i>
                                            Reload
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-main">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="gallery" class="tab-pane">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title">Picture diagram, design, ...</h4>
                                    <div class="widget-toolbar">
                                        <a data-toggle="modal" href="#mdContact" data-action="">
                                            <i class="ace-icon fa fa-plus purple"></i>
                                            Select
                                        </a>
                                        <a href="#" data-action="reload">
                                            <i class="ace-icon fa fa-refresh"></i>
                                            Reload
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <ul class="ace-thumbnails clearfix">
                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-1.jpg" title="Photo Title" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-1.jpg">
                                                </a>

                                                <div class="tags">
                                                    <span class="label-holder">
                                                        <span class="label label-info">breakfast</span>
                                                    </span>

                                                    <span class="label-holder">
                                                        <span class="label label-danger">fruits</span>
                                                    </span>

                                                    <span class="label-holder">
                                                        <span class="label label-success">toast</span>
                                                    </span>

                                                    <span class="label-holder">
                                                        <span class="label label-warning arrowed-in">diet</span>
                                                    </span>
                                                </div>

                                                <div class="tools">
                                                    <a href="#">
                                                        <i class="ace-icon fa fa-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-paperclip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-times red"></i>
                                                    </a>
                                                </div>
                                            </li>

                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-2.jpg" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-2.jpg">
                                                    <div class="text">
                                                        <div class="inner">Sample Caption on Hover</div>
                                                    </div>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-3.jpg" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-3.jpg">
                                                    <div class="text">
                                                        <div class="inner">Sample Caption on Hover</div>
                                                    </div>
                                                </a>

                                                <div class="tools tools-bottom">
                                                    <a href="#">
                                                        <i class="ace-icon fa fa-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-paperclip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-times red"></i>
                                                    </a>
                                                </div>
                                            </li>

                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-4.jpg" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-4.jpg">
                                                    <div class="tags">
                                                        <span class="label-holder">
                                                            <span class="label label-info arrowed">fountain</span>
                                                        </span>

                                                        <span class="label-holder">
                                                            <span class="label label-danger">recreation</span>
                                                        </span>
                                                    </div>
                                                </a>

                                                <div class="tools tools-top">
                                                    <a href="#">
                                                        <i class="ace-icon fa fa-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-paperclip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-times red"></i>
                                                    </a>
                                                </div>
                                            </li>

                                            <li>
                                                <div>
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-5.jpg">
                                                    <div class="text">
                                                        <div class="inner">
                                                            <span>Some Title!</span>

                                                            <br>
                                                            <a href="/template/ace-master/assets//images/gallery/image-5.jpg" data-rel="colorbox" class="cboxElement">
                                                                <i class="ace-icon fa fa-search-plus"></i>
                                                            </a>

                                                            <a href="#">
                                                                <i class="ace-icon fa fa-user"></i>
                                                            </a>

                                                            <a href="#">
                                                                <i class="ace-icon fa fa-share"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-6.jpg" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-6.jpg">
                                                </a>

                                                <div class="tools tools-right">
                                                    <a href="#">
                                                        <i class="ace-icon fa fa-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-paperclip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-times red"></i>
                                                    </a>
                                                </div>
                                            </li>

                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-1.jpg" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-1.jpg">
                                                </a>

                                                <div class="tools">
                                                    <a href="#">
                                                        <i class="ace-icon fa fa-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-paperclip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-times red"></i>
                                                    </a>
                                                </div>
                                            </li>

                                            <li>
                                                <a href="/template/ace-master/assets//images/gallery/image-2.jpg" data-rel="colorbox" class="cboxElement">
                                                    <img width="150" height="150" alt="150x150" src="/template/ace-master/assets//images/gallery/thumb-2.jpg">
                                                </a>

                                                <div class="tools tools-top in">
                                                    <a href="#">
                                                        <i class="ace-icon fa fa-link"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-paperclip"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                        <i class="ace-icon fa fa-times red"></i>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="staff" class="tab-pane">
                            <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>
                        <div id="payment" class="tab-pane">
                            <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>

                        <div id="Source" class="tab-pane">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title">Source code of project</h4>
                                    <div class="widget-toolbar">
                                        <a data-toggle="modal" href="#mdSource" data-action="">
                                            <i class="ace-icon fa fa-plus purple"></i>
                                            Select
                                        </a>
                                        <a href="#" data-action="reload">
                                            <i class="ace-icon fa fa-refresh"></i>
                                            Reload
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <table class="table table-bordered table-hover" id="tableSource">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Source</th>
                                                    <th>Description</th>
                                                    <th>Version</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <asp:Literal ID="Literal_TableSource" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdtask" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contact Info                     
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Send to</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDLSendTo" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Task name</label>
                            <div class="col-sm-9">
                                <input type="text" id="txtTask" placeholder="task name, mission" class="col-xs-10 col-sm-12" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Task content</label>
                            <div class="col-sm-9">
                                <textarea id="txtTaskcontent" placeholder="Task detail infomation" class="col-xs-10 col-sm-12" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">File attack</label>
                            <div class="col-sm-9">
                                <label class="ace-file-input">
                                    <input type="file" id="fileTask" />
                                    <span class="ace-file-container" data-title="Choose">
                                        <span class="ace-file-name" data-title="No File ...">
                                            <i class=" ace-icon fa fa-upload"></i>
                                        </span>
                                    </span>
                                    <a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info pull-right" id="btnSave">Send</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mddocument" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Document Info                     
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Send to</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Task name</label>
                            <div class="col-sm-9">
                                <input type="text" id="txtTask" placeholder="task name, mission" class="col-xs-10 col-sm-12" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Task content</label>
                            <div class="col-sm-9">
                                <textarea id="txtTaskcontent" placeholder="Task detail infomation" class="col-xs-10 col-sm-12" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">File attack</label>
                            <div class="col-sm-9">
                                <label class="ace-file-input">
                                    <input type="file" id="fileTask" />
                                    <span class="ace-file-container" data-title="Choose">
                                        <span class="ace-file-name" data-title="No File ...">
                                            <i class=" ace-icon fa fa-upload"></i>
                                        </span>
                                    </span>
                                    <a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info pull-right" id="btnSave">Send</button>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblCustomerKey" runat="server" Text="0"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">

    <script type="text/javascript">
     
    </script>
</asp:Content>
