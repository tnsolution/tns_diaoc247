﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProjectList.aspx.cs" Inherits="WebApp.JOB.ProjectList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
        <div class="breadcrumb">
            <a class="btn btn-info btn-xs" href="/JOB/ProjectEdit.aspx?ID=0">
                <i class="ace-icon glyphicon glyphicon-plus"></i>Add new
            </a>
        </div>
        <!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
            <div class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                </span>
            </div>
        </div>
        <!-- /.nav-search -->
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1>Projects
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    List of project
                                </small>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="no-more-tables">
                    <asp:Literal ID="Literal_ProjectTable" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdProjectQuickStatus" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Quick status project update</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10">
                            <select class="form-control">
                                <option value="">Disscuss</option>
                                <option value="">Starting</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-sm btn-primary">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $("button[btnUpdate]").click(function () {
                $("#mdProjectQuickStatus").modal({
                    backdrop: true,
                    show: true
                });
            });

            $("button[btnEdit]").on("click", function () {
                var trid = $(this).closest("tr").attr("id");
                var url = "/JOB/ProjectEdit.aspx?ID=" + trid;
                window.location = url;
            });
        });
    </script>
</asp:Content>
