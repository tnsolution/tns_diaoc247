﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TaskList.aspx.cs" Inherits="WebApp.JOB.TaskList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
        <div class="breadcrumb">
            <a class="btn btn-info btn-xs" data-toggle="modal" data-target="#mdQuickTask">
                <i class="ace-icon glyphicon glyphicon-plus"></i>Add new
            </a>
        </div>
        <!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
            <div class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                </span>
            </div>
        </div>
        <!-- /.nav-search -->
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1>Tasks
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    List of task
                                </small>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="no-more-tables">
                    <asp:Literal ID="Literal_TaskTable" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdQuickTask" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Quick task infomation</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label no-padding-right" for="form-field-1">Task category</label>
                        <asp:DropDownList ID="DDL" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label no-padding-right" for="form-field-1">Note</label>
                        <textarea placeholder="Type job, mission description infomation" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label no-padding-right" for="form-field-1">Send to</label>
                        <asp:DropDownList ID="DDL_SendTo" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="ace-file-input">
                            <input type="file" id="fileTask" />
                            <span class="ace-file-container" data-title="Choose">
                                <span class="ace-file-name" data-title="No File ...">
                                    <i class=" ace-icon fa fa-upload"></i>
                                </span>
                            </span>
                            <a class="remove" href="#"><i class=" ace-icon fa fa-times"></i></a>
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default">
                        <i class="ace-icon fa fa-floppy-o"></i>
                        Save
                    </button>
                    <button class="btn btn-default">
                        <i class="ace-icon fa fa-send-o"></i>
                        Send
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
