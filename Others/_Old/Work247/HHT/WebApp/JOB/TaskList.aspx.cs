﻿using Lib.JOB;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.JOB
{
    public partial class TaskList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSbTable = new StringBuilder();
            DataTable zTable = Tasks_Data.List_2017();

            zSbTable.AppendLine("<table class='table table-bordered table-hover' id='tableTask'>");
            zSbTable.AppendLine("   <thead>");
            zSbTable.AppendLine("       <tr>");
            zSbTable.AppendLine("           <th>No.</th>");
            zSbTable.AppendLine("           <th>Task</th>");
            zSbTable.AppendLine("           <th>Content</th>");
            zSbTable.AppendLine("           <th>Staff</th>");
            zSbTable.AppendLine("           <th>Category</th>");
            zSbTable.AppendLine("           <th>Status</th>");
            zSbTable.AppendLine("           <th>Action</th>");
            zSbTable.AppendLine("       </tr>");
            zSbTable.AppendLine("   </thead>");
            zSbTable.AppendLine("   <tbody>");
            if (zTable.Rows.Count > 0)
            {
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    DataRow r = zTable.Rows[i];

                    zSbTable.AppendLine("       <tr id=" + r["TaskKey"].ToString() + ">");
                    zSbTable.AppendLine("           <td data-title='No.'>" + i.ToString() + " </td>");
                    zSbTable.AppendLine("           <td data-title='Task.'>" + r["TaskName"].ToString() + " </td>");
                    zSbTable.AppendLine("           <td data-title='Content.'>" + Utils.GetShortContent(r["TaskContent"].ToString(), 30) + " </td>");
                    zSbTable.AppendLine("           <td data-title='Staff.'>" + r["EmployeeName"].ToString() + " </td>");
                    zSbTable.AppendLine("           <td data-title='Category.'>" + r["CategoryName"].ToString() + " </td>");

                    #region [Status & Action]
                    string Status = "";
                    string Action = "";
                    if (r["IsDraft"].ToString().ToInt() == 0)
                    {
                        Status = "Not send";
                        Action = @"<button type='button' class='btn btn-xs btn-white btn-info' btnSend>
												<i class='ace-icon fa fa-send-o bigger-120 info'></i>
												Send
											</button>
                                            <button type='button' class='btn btn-xs btn-white btn-warning' btnEdit>
												<i class='ace-icon fa fa-pencil bigger-120 orange'></i>
												Edit
											</button>
                                            <button type='button' class='btn btn-xs btn-white btn-danger' btnDelete>
												<i class='ace-icon fa fa-trash-o bigger-120 red2'></i>
												Delete
											</button>";
                    }
                    else
                    {
                        Status = "<button type='button' class='btn btn-xs btn-white btn-default'><i class='" + r["Icon"].ToString() + "'></i>&nbsp" + r["StatusName"] + "</button>";
                        Action = @"<button type='button' class='btn btn-xs btn-white btn-warning' btnEdit>
												<i class='ace-icon fa fa-pencil bigger-120 orange'></i>
												Edit
											</button>
                                            <button type='button' class='btn btn-xs btn-white btn-danger' btnDelete>
												<i class='ace-icon fa fa-trash-o bigger-120 red2'></i>
												Delete
											</button>";
                    }
                    #endregion

                    zSbTable.AppendLine(@"        <td data-title='Status'>" + Status + "</td>");
                    zSbTable.AppendLine(@"        <td data-title='Action'>" + Action + "</td>");
                    zSbTable.AppendLine("       </tr>");
                }
            }
            else
            {
                zSbTable.AppendLine("       <tr><td colspan=7>No Record</td></tr>");                
            }
            zSbTable.AppendLine("   </tbody>");
            zSbTable.AppendLine("</table>");
            Literal_TaskTable.Text = zSbTable.ToString();
        }
    }
}