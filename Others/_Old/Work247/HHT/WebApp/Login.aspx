﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApp.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-rtl.min.css" />
</head>
<body class="login-layout light-login">
    <form id="form1" runat="server">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf green"></i>
                                    <span class="red">ADMIN</span>
                                    <span class="white" id="id-text2">Application</span>
                                </h1>
                                <h4 class="blue" id="id-company-text">© HHT</h4>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Please Enter Your Information
                                            </h4>

                                            <div class="space-6"></div>

                                            <div>
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="Username" id="txt_UserName" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Password" id="txt_Password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>
                                                    <div class="space"></div>
                                                    <div class="clearfix">
                                                        <button type="button" class="width-35 pull-right btn btn-sm btn-primary" id="btnSubmit">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Login</span>
                                                        </button>
                                                    </div>
                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/template/ace-master/assets/js/jquery-2.1.4.min.js"></script>
        <script>
            var error_obj = null;
            var error_prefix = 'Phải nhập ';
            var error_message = '<div class="text-danger error">{msg}</div>';
            $(function () {
                //$(".se-pre-con").fadeOut("slow");
                $("input").on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        $("#btnSubmit").trigger("click")
                    }
                });
                $('#btnSubmit').on('click', function () {
                    var UserName = $('[id$=txt_UserName]').val();
                    var Password = $('[id$=txt_Password]').val();
                    if (UserName == '' || Password == '') {
                        alert('Bạn phải nhập tên tài khoản và mật khẩu');
                        return false;
                    }

                    $.ajax({
                        type: "POST",
                        url: "Login.aspx/CheckLogin",
                        data: JSON.stringify({
                            "UserName": UserName,
                            "Password": Password,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            //$(".se-pre-con").fadeIn("slow");
                        },
                        success: function (msg) {
                            if (msg.d.Result == "1")
                                window.location = "/Default.aspx";
                            else
                                alert(msg.d.Message);
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
                });
            });
    </script>
    </form>
</body>
</html>
