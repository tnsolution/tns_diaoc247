﻿using Lib.SYS;
using System;
using System.Web;
using System.Web.Services;

namespace WebApp
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ItemResult CheckLogin(string UserName, string Password)
        {
            ItemUser zUser = User_Data.CheckUser(UserName, Password);
            ItemResult zResult = new ItemResult();
            if (zUser.Message == "ERR")
            {
                switch (zUser.MessageCode)
                {
                    case "CheckUser_Error01":
                        zResult.Message = "Vui lòng kiểm tra Username và Password";
                        return zResult;

                    case "CheckUser_Error02":
                        zResult.Message = "User này chưa kích hoạt, vui lòng liên hệ Administrator";
                        return zResult;

                    case "CheckUser_Error03":
                        zResult.Message = "User này đã hết hạn, vui lòng liên hệ Administrator";
                        return zResult;

                    default:
                        zResult.Message = "Lỗi đăng nhập !";
                        return zResult;
                }
            }
            else
            {
                HttpCookie zCook = new HttpCookie("UserLog");
                zCook.Values["UserKey"] = zUser.UserKey;
                zCook.Values["UserName"] = HttpUtility.UrlEncode(zUser.UserName);
                zCook.Values["EmployeeName"] = HttpUtility.UrlEncode(zUser.EmployeeName);
                zCook.Expires = DateTime.Now.AddHours(8);
                HttpContext.Current.Response.Cookies.Add(zCook);

                zResult.Message = "Success";
                zResult.Result = "1";
                return zResult;
            }
        }
    }
}