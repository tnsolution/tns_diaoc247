﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="WebApp.SAL.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        sup {
            vertical-align: sup;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Sản phẩm
                <span class="tools pull-right">
                    <a href="#" class="btn btn-white btn-info btn-bold" data-toggle="modal" data-target="#mSearch">
                        <i class="ace-icon fa fa-search icon-only bigger-110"></i>
                        Tìm lọc
                    </a>
                    <a href="ProductEdit.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <!--Search-->
    <div class="modal fade" id="mSearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Điều kiện tìm kiếm</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label no-padding-right" for="form-field-1">Sản phẩm thuộc</label>
                        <div class="radio">
                            <label>
                                <input name="form-field-radio" type="radio" class="ace input" value="0" />
                                <span class="lbl">Cá nhân</span>
                            </label>
                            <small>Bao gồm cả các sản phẩm được chia sẽ</small>
                        </div>
                        <div class="radio">
                            <label>
                                <input name="form-field-radio" type="radio" class="ace input" value="1" />
                                <span class="lbl">Công ty</span>
                            </label>
                            <small>Các sản phẩm từ công ty cung cấp</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Table" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Hình thức sản phẩm --" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Project" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Tất cả dự án--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Category" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txt_ID1" runat="server" CssClass="form-control" Text="" placeholder="-- Tháp/ Khu --"></asp:TextBox>
                        <asp:TextBox ID="txt_ID2" runat="server" CssClass="form-control" Text="" placeholder="-- Tầng/ Dãy --"></asp:TextBox>
                        <asp:TextBox ID="txt_ID3" runat="server" CssClass="form-control" Text="" placeholder="-- Số căn --"></asp:TextBox>
                        <asp:TextBox ID="txt_Bed" runat="server" CssClass="form-control" Text="" placeholder="-- Số phòng ngủ --"></asp:TextBox>
                        <asp:TextBox ID="txt_Name" runat="server" CssClass="form-control" Text="" placeholder="-- Mã sản phẩm--"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Nhu cầu</label>
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" />
                                        <span class="lbl">Chuyển nhượng</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" />
                                        <span class="lbl">Khai thác kinh doanh</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" />
                                        <span class="lbl">Cho thuê</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <label>Tình trạng</label>
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" />
                                        <span class="lbl">Đã bán</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" />
                                        <span class="lbl">Đã cho thuê</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" />
                                        <span class="lbl">Chưa giao dịch</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" id="btnCreate">
                        <i class="ace-icon fa fa-search-plus"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $("#tblData tbody tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "ProductView.aspx?ID=" + trid;
            });
        });
    </script>
</asp:Content>
