﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = new DataTable();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Dự án</th>");
            zSb.AppendLine("        <th>Mã sản phẩm</th>");
            zSb.AppendLine("        <th>Loại sản phẩm</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Mục đích</th>");
            zSb.AppendLine("        <th>Diện tích (m<sup>2</sup>)</th>");
            zSb.AppendLine("        <th>Giá</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AssetKey"].ToString() + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Pupose"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Area"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Price"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Status"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='9'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
    }
}