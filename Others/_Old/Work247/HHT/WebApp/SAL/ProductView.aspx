﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductView.aspx.cs" Inherits="WebApp.SAL.ProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        .cancel-off-png, .cancel-on-png, .star-half-png, .star-off-png, .star-on-png {
            font-size: 1.2em !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết sản phẩm
                <asp:Literal ID="txt_Asset" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnExeReminder">
                        <i class="ace-icon fa fa-adjust orange2"></i>
                        Xử lý nhắc nhở
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trờ về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div id="accordion" class="accordion-style1 panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Thông tin cơ bản
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseOne" aria-expanded="true">
                            <div class="panel-body">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Dự án</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_ProjectName" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Địa chỉ</div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_Address" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Mã sản phẩm
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_AssetID" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Loại sản phẩm
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_CategoryName" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Số phòng ngủ
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_room" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Giá bán
                                        </div>
                                        <div class="profile-info-value giatien">
                                            <asp:Literal ID="txt_Price" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Giá thuê
                                        </div>
                                        <div class="profile-info-value giatien">
                                            <asp:Literal ID="txt_PriceRent" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Giá mua
                                        </div>
                                        <div class="profile-info-value giatien">
                                            <asp:Literal ID="txt_PricePurchase" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Thông tin thêm (diện tích, hướng, nhu cầu, ...)
                                </a>
                            </h4>
                        </div>

                        <div class="panel-collapse collapse" id="collapseTwo" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <asp:Literal ID="Lit_ExtraAsset" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Hình ảnh
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThree" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <asp:Literal ID="Lit_File" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Chủ nhà
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse4" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <asp:Literal ID="Lit_Owner" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Tin đăng 247
                                </a>
                            </h4>
                        </div>

                        <div class="panel-collapse collapse" id="collapse5" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Tiêu đề
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_WebTitle" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Nội dung tin
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_WebContent" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Xuất bản
                                        </div>
                                        <div class="profile-info-value">
                                            <asp:Literal ID="txt_WebPublish" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin hệ thống</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(function () {
            $("#btnExeReminder").hide();
            $("#btnExeReminder").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/SAL/ProductView.aspx/ExeReminder",
                    data: JSON.stringify({
                        "AssetKey": Page.getUrlParameter("ID"),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                            $("#btnExeReminder").hide();
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnEdit").click(function () {
                window.location = "EmployeeEdit.aspx?ID=" + Page.getUrlParameter("ID");
            });
            $.ajax({
                type: "POST",
                url: "/SAL/ProductView.aspx/CheckReminder",
                data: JSON.stringify({
                    "AssetKey": Page.getUrlParameter("ID"),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                        $("#btnExeReminder").show();
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
    </script>
</asp:Content>
