﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ProductView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AssetKey.Value = Request["ID"];
                if (Request["Type"] != null)
                    HID_AssetType.Value = Request["Type"];

                CheckRoles();
                LoadAsset();
                LoadOwner();
                LoadFile();
            }
        }

        void LoadAsset()
        {
            Product_Info zAsset = new Product_Info(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            txt_ProjectName.Text = zAsset.ItemAsset.ProjectName;
            txt_Address.Text = zAsset.ItemAsset.Address;
            txt_Asset.Text = zAsset.ItemAsset.AssetID;
            txt_AssetID.Text = zAsset.ItemAsset.AssetID;
            if (zAsset.ItemAsset.Hot.ToInt() > 0)
            {
                StringBuilder zStar = new StringBuilder();
                zStar.AppendLine("<div class='rating inline'>");
                double Current = Math.Round(double.Parse(zAsset.ItemAsset.Hot), MidpointRounding.AwayFromZero);
                for (int i = 0; i < 5; i++)
                {
                    if (i < Current)
                    {
                        zStar.AppendLine(" <i data-alt='" + i + "' class='star-on-png'></i>&nbsp;");
                    }
                    else
                    {
                        zStar.AppendLine(" <i data-alt='" + i + "' class='star-off-png'></i>&nbsp;");
                    }
                }
                zStar.AppendLine("<input name='score' type='hidden'>");
                zStar.AppendLine("</div>");
            }
            else
            {
                txt_AssetID.Text += @"
<div class='rating inline'>
    <i data-alt='1' class='star-off-png' title='bad'></i>&nbsp;
    <i data-alt='2' class='star-off-png' title='poor'></i>&nbsp;
    <i data-alt='3' class='star-off-png' title='regular'></i>&nbsp;
    <i data-alt='4' class='star-off-png' title='good'></i>&nbsp;
    <i data-alt='5' class='star-off-png' title='gorgeous'></i>
<input name='score' type='hidden'>
</div>
";
            }
            txt_CategoryName.Text = zAsset.ItemAsset.CategoryName;
            txt_room.Text = zAsset.ItemAsset.Room;
            txt_Price.Text = zAsset.ItemAsset.Price;
            txt_PricePurchase.Text = zAsset.ItemAsset.PricePurchase;
            txt_PriceRent.Text = zAsset.ItemAsset.PriceRent;

            #region Extra Info
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.Status + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.DateContractEnd + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.AreaWall + " </div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Diện tích thông thủy</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.AreaWater + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Hướng view</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.View + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Hướng cửa</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.Door + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.Furniture + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.Purpose + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                    <div class='profile-info-row'>");
            zSb.AppendLine("                                        <div class='profile-info-name'> Ghi chú</div>");
            zSb.AppendLine("                                        <div class='profile-info-value'>" + zAsset.ItemAsset.Description + "</div>");
            zSb.AppendLine("                                    </div>");
            zSb.AppendLine("                                </div>");
            Lit_ExtraAsset.Text = zSb.ToString();
            #endregion

            zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zAsset.ItemAsset.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zAsset.ItemAsset.ModifiedDate + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }
        void LoadOwner()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTableOwners = Owner_Data.List(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            if (zTableOwners.Rows.Count > 0)
            {
                zSb.AppendLine("<table class='table table-bordered' id='tableOwner'>");
                zSb.AppendLine("   <thead>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th>STT</th>");
                zSb.AppendLine("        <th>Họ tên</th>");
                zSb.AppendLine("        <th>Số điện thoại</th>");
                zSb.AppendLine("        <th>Email</th>");
                zSb.AppendLine("        <th>Địa chỉ</th>");
                zSb.AppendLine("        <th>Ngày cập nhật</th>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");

                int i = 1;
                foreach (DataRow r in zTableOwners.Rows)
                {
                    zSb.AppendLine("            <tr>");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["CustomerName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Phone"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Email"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CreatedDate"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
            }

            Lit_Owner.Text = zSb.ToString();
        }
        void LoadFile()
        {
            DataTable zDocument = Document_Data.List(135, HID_AssetKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            if (zDocument.Rows.Count > 0)
            {
                zSb.AppendLine("<div id='links'>");
                for (int i = 0; i < zDocument.Rows.Count; i++)
                {
                    DataRow r = zDocument.Rows[i];
                    zSb.AppendLine("<a href='" + r["ImageUrl"].ToString() + "' title='" + r["ImageName"].ToString() + "'><img width='96' height='72' src='" + r["ImageUrl"].ToString() + "'></a>");
                }
                zSb.AppendLine("    </div>");
            }
            Lit_File.Text = zSb.ToString();
        }

        //kiem tra nhac nhở san phẩm
        [WebMethod]
        public static ItemResult CheckReminder(int AssetKey)
        {
            ItemResult zResult = new ItemResult();
            if (Product_Data.CheckReminder(AssetKey))
            {
                zResult.Message = "Sản phẩm này đã đến hẹn, chưa được xử lý, vui lòng kiểm tra ngày liên hệ !";
                zResult.Result = "1";
            }
            return zResult;
        }
        [WebMethod]
        public static ItemResult ExeReminder(int AssetKey)
        {
            ItemResult zResult = new ItemResult();
            Notification_Info zInfo = new Notification_Info(AssetKey);
            zInfo.IsRead = 1;
            zInfo.ReadDate = DateTime.Now;
            zInfo.UpdateReaded();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "Đã xử lý thành công !";
            zResult.Result = "1";
            return zResult;
        }

        //kiem tra user hien tai co duoc phep xem so dt cua thong tin sản phẩm dang xem
        bool CheckViewPhone(int EmployeeKey)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            if (UnitLevel == 0 || UnitLevel == 1 || CurrentAgent == OwnerAgent)
            {
                return true;
            }

            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <a href='/SAL/ProductEdit.aspx?ID=" + HID_AssetKey.Value + "&" + HID_AssetType.Value + @"' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Điều chỉnh
                        </a>";
            }
        }
        #endregion
    }
}