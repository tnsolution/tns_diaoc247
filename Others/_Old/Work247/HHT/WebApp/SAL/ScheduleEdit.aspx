﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleEdit.aspx.cs" Inherits="WebApp.SAL.ScheduleEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap-timepicker.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày hiển thị kế hoạch</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                        id="txt_Date" placeholder="Chọn ngày tháng năm" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mục tiêu kế hoạch</label>
                            <div class="col-sm-5">
                                <input type="text" runat="server" class="form-control" id="txt_Description" placeholder="..." required />
                            </div>
                        </div>

                    </div>
                </div>
                <h4 class="header smaller lighter blue">
                    <a href="#mScheduleDetail" data-toggle="modal"><i class="ace-icon fa fa-plus green"></i>Thêm nội dung</a></h4>
                <div class="row">
                    <div class="col-sm-9">
                        <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin hệ thống</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    Khởi tạo: Lê Thị Thùy Trang<br />
                                    SĐT: 0937247932<br />
                                    Cập nhật: System Administrator<br />
                                    Ngày cập nhật: 24/06/2017
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mScheduleDetail" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Chỉnh sữa nội dụng</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Thời gian </label>
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" id="txt_From" class="form-control col-sm-6" placeholder="Nhập giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                            <input type="text" id="txt_To" class="form-control col-sm-6" placeholder="Nhập giờ phút" required="" />
                            <span class="input-group-addon">
                                <i class="fa fa-clock-o bigger-110"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                        <div>
                            <input name="txt_Description" type="text" id="txt_Content" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnScheduleNext">OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ScheduleKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/bootstrap-timepicker.min.js"></script>
    <script>
        jQuery(function ($) {

        });
    </script>
</asp:Content>
