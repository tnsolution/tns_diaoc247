﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ScheduleEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ScheduleKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Lập kế hoạch.";
            Schedule_Info zInfo = new Schedule_Info();


            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy") + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemResult InitInfo(string Date, string Description)
        {
            ItemResult zResult = new ItemResult();
            Schedule_Info zInfo = new Schedule_Info();
            zInfo.ScheduleDate = Tools.ConvertToDate(Date);
            zInfo.Description = Description;

            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];

            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "OK";
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
    }
}