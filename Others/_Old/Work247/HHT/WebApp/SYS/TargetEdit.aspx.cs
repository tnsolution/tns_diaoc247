﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SYS
{
    public partial class TargetEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_TargetKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_TargetKey.Value != string.Empty && HID_TargetKey.Value != "0")
            {
                Categories_Info zInfo = new Categories_Info(HID_TargetKey.Value.ToInt());
                txt_Target.Value = zInfo.Product;
                txt_Description.Value = zInfo.Description;
                txt_Rank.Value = zInfo.Rank.ToString();
                Lit_TitlePage.Text = "Điều chỉnh " + zInfo.Product;
            }
        }

        [WebMethod]
        public static ItemResult SaveTarget(int TargetKey, string TargetName, string Description, string Rank)
        {
            ItemResult zResult = new ItemResult();
            Categories_Info zInfo = new Categories_Info(TargetKey);
            zInfo.Product = TargetName;
            zInfo.Description = Description;
            zInfo.Rank = Rank.ToInt();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult DeleteTarget(int TargetKey)
        {
            ItemResult zResult = new ItemResult();
            Categories_Info zInfo = new Categories_Info();
            zInfo.AutoKey = TargetKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}