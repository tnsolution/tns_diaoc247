﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="UnitList.aspx.cs" Inherits="WebApp.SYS.UnitList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Cấp sử dụng.
                <span class="tools pull-right">
                    <a href="UnitEdit.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $("#tblData tbody tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "UnitEdit.aspx?ID=" + trid;
            });
        });
    </script>
</asp:Content>
