﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

/*
 * ID: UserKey
 * ID2: EmployeeKey
 */
namespace WebApp.SYS
{
    public partial class UserEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2", true);

                if (Request["ID"] != null)
                    HID_UserKey.Value = Request["ID"];
                if (Request["ID2"] != null)
                    DDL_Employee.SelectedValue = Request["ID2"];

                Load_User();
                Load_Roles();
            }
        }

        void Load_User()
        {
            Lit_TitlePage.Text = "Tạo mới tài khoản.";
            if (HID_UserKey.Value != string.Empty &&
                HID_UserKey.Value != "0")
            {
                User_Info zInfo = new User_Info(HID_UserKey.Value);
                txt_UserName.Value = zInfo.Name;
                txt_Description.Value = zInfo.Description;
                txt_ExpireDate.Value = zInfo.ExpireDate.ToString("dd/MM/yyyy");
                DDL_Employee.SelectedValue = zInfo.EmployeeKey.ToString();
                DDL_Status.SelectedValue = zInfo.Activated.ToString();

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedBy + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();
                Lit_TitlePage.Text = "Điều chỉnh tài khoản " + zInfo.Name;
            }
        }
        void Load_Roles()
        {
            if (HID_UserKey.Value != string.Empty || HID_UserKey.Value != "0")
            {
                StringBuilder zSb = new StringBuilder();
                DataTable zTable = User_Data.ListRoles(HID_UserKey.Value);

                zSb.AppendLine("<table class='table  table-bordered table-hover'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <tr>");
                zSb.AppendLine("            <th>No</th>");
                zSb.AppendLine("            <th>Quyền</th>");
                zSb.AppendLine("            <th>Diễn giải</th>");
                zSb.AppendLine("            <th>Xem</th>");
                zSb.AppendLine("            <th>Thêm</th>");
                zSb.AppendLine("            <th>Sửa</th>");
                zSb.AppendLine("            <th>Xóa</th>");
                zSb.AppendLine("        </tr>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("    <tbody>");

                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    string style = "";
                    string group = "<i class='ace-icon fa fa-angle-right bigger-110'></i>";
                    DataRow r = zTable.Rows[i];
                    if (r["RoleID"].ToString().Length == 3)
                    {
                        style = "style='background-color: rgba(98, 168, 209, 0.5);color: #000;'";
                        group = "<i class='ace-icon fa fa-caret-right blue'></i>";
                    }

                    zSb.AppendLine("    <tr " + style + ">");
                    zSb.AppendLine("        <td>" + (i + 1) + "</td>");
                    zSb.AppendLine("        <td>" + group + " " + r["RoleName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + Html_CheckBox("", r["RoleKey"].ToString(), "Read", r["IsRead"].ToString()) + "</td>");
                    zSb.AppendLine("        <td>" + Html_CheckBox("", r["RoleKey"].ToString(), "Add", r["IsAdd"].ToString()) + "</td>");
                    zSb.AppendLine("        <td>" + Html_CheckBox("", r["RoleKey"].ToString(), "Edit", r["IsEdit"].ToString()) + "</td>");
                    zSb.AppendLine("        <td>" + Html_CheckBox("", r["RoleKey"].ToString(), "Del", r["IsDelete"].ToString()) + " </td> ");
                    zSb.AppendLine("    </tr>");
                }

                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                Lit_TableRole.Text = zSb.ToString();
            }
        }

        string Html_CheckBox(string Label, string ID, string Attr, string Value)
        {
            string isCheck = "";
            if (Value == "1")
                isCheck = "Checked";

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='checkbox'>");
            zSb.AppendLine("    <label>");
            zSb.AppendLine("        <input name='form-field-checkbox' type='checkbox' class='ace' attr='" + Attr + "' id='" + ID + "' " + isCheck + "><span class='lbl'>" + Label + "</span>");
            zSb.AppendLine("    </label>");
            zSb.AppendLine("<div>");
            return zSb.ToString();
        }

        [WebMethod]
        public static ItemResult UpdateRoles(string UserKey, string RoleKey, string Column, string Value)
        {
            ItemResult zResult = new ItemResult();
            User_Roles_Info zInfo = new User_Roles_Info(UserKey, RoleKey);
            switch (Column)
            {
                case "Read":
                    zInfo.Read = Value.ToInt();
                    break;

                case "Add":
                    zInfo.Add = Value.ToInt();
                    break;

                case "Edit":
                    zInfo.Edit = Value.ToInt();
                    break;

                case "Del":
                    zInfo.Del = Value.ToInt();
                    break;
            }

            if (zInfo.AutoID == string.Empty)
            {
                zInfo.UserKey = UserKey;
                zInfo.RoleKey = RoleKey;
                zInfo.Create();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
                zResult.Result = "1";
            }

            return zResult;
        }
        [WebMethod]
        public static ItemResult SaveUser(string UserKey, string UserName, string Description, string ExpireDate, string Employee, string Status)
        {
            ItemResult zResult = new ItemResult();
            User_Info zInfo = new User_Info(UserKey);
            zInfo.Name = UserName;
            if (zInfo.Password == string.Empty)
                zInfo.Password = MyCryptography.HashPass("123456");
            zInfo.Description = Description;
            zInfo.ExpireDate = Tools.ConvertToDate(ExpireDate);
            zInfo.EmployeeKey = Employee.ToInt();
            zInfo.Activated = Status.ToInt();

            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];

            zInfo.Save();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key;
            return zResult;
        }
        [WebMethod]
        public static ItemResult DeleteUser(string UserKey)
        {
            ItemResult zResult = new ItemResult();
            User_Info zInfo = new User_Info();
            zInfo.Key = UserKey;
            zInfo.Delete();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult ResetPass(string UserKey)
        {
            ItemResult zResult = new ItemResult();
            User_Info zUser = new User_Info();
            zUser.Key = UserKey;
            zUser.Password = "123456";
            zUser.ModifiedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zUser.UpdatePass();

            if (zUser.Message != string.Empty)
                zResult.Message = zUser.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemResult CreateUserName(string Name)
        {
            ItemResult zResult = new ItemResult();
            zResult.Result = Utils.ToEnglish(Name);
            if (User_Data.CheckUser(zResult.Result))
                zResult.Message = "Tên tài khoản này đã có rồi, vui lòng chọn tên tài khoản khác !";
            return zResult;
        }
    }
}