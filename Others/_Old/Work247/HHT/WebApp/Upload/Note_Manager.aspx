﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="Note_Manager.aspx.cs" Inherits="WebApp.TASK.Note_Manager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/themes/plugins/datepicker/datepicker3.css" />
    <link rel="stylesheet" href="/themes/plugins/select2/select2.css" />
    <style>
        .select2 {
            width: 100% !important;
        }

        .col-xs-12 {
            padding-left: 0px;
            padding-right: 0px;
        }

        table {
            overflow-x: scroll;
        }

            table > thead > tr > th.clsName {
                min-width: 200px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-header">
        <h1>Quản lý nguồn tin
            <span class="pull-right">
                <asp:Literal ID="Literal_Action" runat="server"></asp:Literal>
            </span>
        </h1>
    </div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                id="txt_FromDate" placeholder="Từ ngày" />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                id="txt_ToDate" placeholder="Đến ngày" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="DDL_Status" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả tình trạng--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="DDL_Employee_Search" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-1">
                        <button id="btnSearch" type="button" class="btn btn-primary" runat="server" onserverclick="btnSearch_ServerClick"><i class="fa fa-search"></i>&nbsp Tìm kiếm</button>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class='col-xs-12 table-responsive' id="tableContent">
                    <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="box-footer">
                <asp:DataList ID="PageNumbers" runat="server" BorderStyle="None" BorderWidth="0"
                    DataKeyField="PageNumberName" CellPadding="0" CellSpacing="0" OnItemCommand="PageNumber_ItemCommand"
                    RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <asp:LinkButton ID="PageNumber" runat="server" Text='<%# Eval("PageNumberName") %>'
                            Width="25px" Height="18px"></asp:LinkButton>
                    </ItemTemplate>
                    <SelectedItemTemplate>
                        <asp:Label ID="PageNumberSelect" runat="server" Text='<%# Eval("PageNumberName") %>'
                            Width="25px" Height="18px"></asp:Label>
                    </SelectedItemTemplate>
                    <ItemStyle CssClass="PageNumber" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <SelectedItemStyle CssClass="PageNumberSelect" />
                    <SeparatorTemplate>
                    </SeparatorTemplate>
                    <SeparatorStyle Width="5px" />
                </asp:DataList>
            </div>
        </div>
    </div>
    <div class='modal' id='modal-Excel'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>×</span></button>
                    <h4 class='modal-title'>Nhập excel</h4>
                </div>
                <div class='modal-body'>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Tập tin excel</label>
                                <div class="row">
                                    <div class="col-md-8">
                                        <asp:FileUpload ID="FU" runat="server" />
                                    </div>
                                </div>
                                <p class="help-block">
                                    Tập tin excel bị trùng sẽ tự động xóa.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default pull-left' data-dismiss='modal'>
                        Đóng</button>
                    <button id="btnImport" type="button" class='btn btn-primary' data-dismiss='modal' runat="server" onserverclick="btnImport_ServerClick"><i class="fa fa-file-excel-o"></i>&nbsp Tải Excel</button>
                </div>
            </div>
        </div>
    </div>
    <div class='modal' id='modal-Send'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>×</span></button>
                    <h4 class='modal-title'>Phiếu thông tin</h4>
                </div>
                <div class='modal-body'>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ngày gửi</label>
                                    <div class="col-sm-8">
                                        <input type="text" runat="server" class="form-control" id="txt_Datetime_Send" readonly="true" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Người gửi</label>
                                    <div class="col-sm-8">
                                        <input type="text" runat="server" class="form-control" id="txt_SendFrom" readonly="true" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gửi đển</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="DDL_Employee_Send" runat="server" class="form-control select2"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Dự án</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="DDL_Project" runat="server" class="form-control select2"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Số thông tin gửi</label>
                                    <div class="col-sm-4">
                                        <input type="text" runat="server" class="form-control" id="txt_Amount" placeholder="Nhập số" checkerror />
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" runat="server" class="form-control" id="txt_Amount_Left" placeholder="Số còn lại: " readonly="true" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea runat="server" class="form-control" id="txt_Description" placeholder="Ghi chú cho người nhận" rows="4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default pull-left' data-dismiss='modal'>
                        Đóng</button>
                    <button type='button' class='btn btn-primary' id='btnSend' data-dismiss='modal' runat="server" onserverclick="btnSend_ServerClick">
                        Gửi</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HF_CheckUser" runat="server" />
    <asp:Label ID="lblPageSize" runat="server" Text="50" Visible="false"></asp:Label>
    <asp:Label ID="lblPageNumber" runat="server" Text="1" Visible="false"></asp:Label>

    <script type="text/javascript" src="/themes/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/themes/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Script/Plugin/tableHeadFixer.js"></script>
    <script>
        $(document).ready(function () {
            $("#tableInfomation").tableHeadFixer({ 'left': 3 });

            $("#tableContent").height($(window).height() - 295);
            $("#tableContent").css("overflow-y", "scroll");

            $("body").addClass("sidebar-collapse");
            $(".select2").select2({ placeholder: "--Chọn--", });

            $('[role="datepicker"]').attr('readonly', 'readonly');
            $('[role="datepicker"]').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                startDate: '01/01/2010',
            }).on('changeDate', function (e) {
                //var date = $(this).datepicker('getDate');
                //$('#txtthang').val(date.getMonth() + 1);
            });

            $('#modal-Send').on('shown.bs.modal', function () {

            });
            $('#btnOpenExcel').on("click", function () {
                $('#modal-Excel').modal({
                    backdrop: true,
                    show: true
                });
            });
            $('#btnOpenSend').on("click", function () {
                $('#modal-Send').modal({
                    backdrop: true,
                    show: true
                });
                $(".select2").select2();
                $('[id$=btnSend]').attr('disabled', 'disabled');
            });

            $('[id$=DDL_Project]').change(function () {
                var Name = $('[id$=DDL_Project] option:selected').text();
                $.ajax({
                    type: "POST",
                    url: "Note_Manager.aspx/CountNumber",
                    data: JSON.stringify({
                        "Name": Name
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (!isNaN(+msg.d) && isFinite(msg.d)) {
                            $('[id$=txt_Amount_Left]').val(msg.d);
                            $('[id$=btnSend]').show();
                            $('[id$=btnSend]').removeAttr('disabled');
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $('[id$=btnSend]').on("click", function () {
                var Send = $('[id$=txt_Amount]').val();
                var Remain = $('[id$=txt_Amount_Left]').val();
                if (Send > Remain) {
                    alert("Số lượng thông tin gửi không được vượt quá thông tin còn lại !");
                    return false;
                }
                $(".se-pre-con").fadeIn("slow");
            });
            $('[id$=btnSearch]').on("click", function () {
                $(".se-pre-con").fadeIn("slow");
            });
            $('[id$=btnImport]').on("click", function () {
                $(".se-pre-con").fadeIn("slow");
            });
            $('[id$=btnViewImport]').on("click", function () {
                //$('#modal-Excel').modal({
                //    backdrop: true,
                //    show: true
                //});
            });

            $('a.action').click(function (e) {
                var StatusInfo = $(this).attr('id');
                var DetailKey = $(this).closest('tr').attr('id'); // table row ID 
                var status = $(e.target).text();

                if (StatusInfo == 49) {
                    window.location.replace('/CRM/CustomerEdit.aspx?DetailKey=' + DetailKey + '&StatusInfo=' + StatusInfo);
                    return false;
                }

                if (StatusInfo == 48) {
                    window.location.replace('/PUL/ResaleHouseEdit.aspx?DetailKey=' + DetailKey + '&StatusInfo=' + StatusInfo);
                    return false;
                }

                if (StatusInfo == 47) {
                    window.location.replace('/PUL/ResaleGroundEdit.aspx?DetailKey=' + DetailKey + '&StatusInfo=' + StatusInfo);
                    return false;
                }

                if (StatusInfo == 46) {
                    window.location.replace('/PUL/ResaleApartmentEdit.aspx?DetailKey=' + DetailKey + '&StatusInfo=' + StatusInfo);
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "Note_Manager.aspx/UpdateStatus",
                    data: JSON.stringify({
                        "DetailKey": DetailKey,
                        "StatusInfo": StatusInfo,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn();
                    },
                    success: function (msg) {
                        if (msg.d == "OK") {
                            $('#tableInfomation tr[id=' + DetailKey + '] td.clsStatus').text(status);
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {
                        
                        $(".se-pre-con").fadeOut();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>
</asp:Content>
