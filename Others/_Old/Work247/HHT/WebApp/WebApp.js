﻿var error_obj = null;
var error_prefix = "Phải nhập ";
var error_message = "<div class='text-danger error'>{msg}</div>";
var Page = {
    initialize: function () {

    },
    checkError: function (txt) {
        if (txt.attr("role") === "datepicker") {
            txt.parent().next().remove();
        } else {
            txt.next().remove();
        }
        var val = $.trim(txt.val());
        if (val === "") {
            var title = error_prefix + $.trim(txt.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            if (txt.attr("role") === "datepicker") {
                txt.parent().parent().append($(error));
            } else {
                txt.parent().append($(error));
            }
            if (error_obj === null) {
                error_obj = txt;
            }
        }
    },
    checkSelect: function (select) {
        select.next().next().remove();
        var val = select.val();
        if (val < 0) {
            var title = error_prefix + $.trim(select.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            select.parent().append($(error));
            if (error_obj === null) {
                error_obj = select;
            }
        }
    },
    showNotiMessageInfo: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-info gritter-light"
        });
    },
    showNotiMessageError: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-error gritter-light"
        });
    },
    showPopupMessage: function (title, content) {
        $("#titContent").text(content);
        $("#titMessage").text(title);
        $("#mMessageBox").modal({
            backdrop: true,
            show: true
        });
    },
    getUrlParameter: function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split("&"),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split("=");

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
};
$(document).ready(Page.initialize);