﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CustomerEdit.aspx.cs" Inherits="WebApp.CRM.CustomerEdit1" %>

<%@ Register Src="~/Controls/Breadcrumbs.ascx" TagPrefix="uc1" TagName="Breadcrumbs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
        <div class="breadcrumb">
            <uc1:Breadcrumbs runat="server" ID="Breadcrumbs1" />
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1>Customer Edit
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Edit customer info
                                </small>
                <span class="tools pull-right">
                    <button class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Save
                    </button>
                    <button class="btn btn-white btn-default btn-bold" onclick="goBack()">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Back
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs " id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#general" aria-expanded="true">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                General Info
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" href="#Contact" aria-expanded="false">
                                <i class="blue ace-icon fa fa-user bigger-110"></i>
                                Contact
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" href="#project" aria-expanded="false">
                                <i class="ace-icon fa fa-rocket"></i>
                                Project History
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active">
                            <div class="col-lg-6 form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="txtCustomerName" runat="server" placeholder="Full name" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Address</label>

                                    <div class="col-sm-9">
                                        <input type="text" id="txtAddress" runat="server" placeholder="Address info" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Email</label>

                                    <div class="col-sm-9">
                                        <input type="text" id="txtEmail" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Phone</label>

                                    <div class="col-sm-9">
                                        <input type="text" id="txtPhone" runat="server" placeholder="" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Note</label>
                                    <div class="col-sm-9">
                                        <textarea placeholder="" id="txtNote" runat="server" class="col-xs-10 col-sm-12"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Contact" class="tab-pane">
                            <span class="toolbar pull-right">
                                <a data-toggle="modal" href="#mdContact">
                                    <i class="menu-icon fa fa-plus purple"></i>
                                    Add Contact
                                </a>
                            </span>
                            <table class="table table-bordered table-hover" id="tableContact">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Department</th>
                                        <th>Notes</th>
                                        <th>..</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <asp:Literal ID="Literal_ContactTable" runat="server"></asp:Literal>
                        </div>
                        <div id="project" class="tab-pane">
                            <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdContact" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Contact Info                     
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Name</label>
                            <div class="col-sm-9">
                                <input type="text" id="txtContactName" placeholder="Full name" class="col-xs-10 col-sm-12" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Address</label>

                            <div class="col-sm-9">
                                <input type="text" id="txtContactDepartment" placeholder="Address info" class="col-xs-10 col-sm-12" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Email</label>

                            <div class="col-sm-9">
                                <input type="text" id="txtContactPhone" placeholder="" class="col-xs-10 col-sm-12" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Note</label>
                            <div class="col-sm-9">
                                <textarea placeholder="" id="txtContactNote" class="col-xs-10 col-sm-12"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info pull-right" id="btnSave">Save</button>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblCustomerKey" runat="server" Text="0"></asp:Label>
    <asp:Button ID="btnSaveCustomer" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnSaveCustomer_Click" />
    <asp:Button ID="btnSaveContact" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnSaveContact_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            
        });
    </script>
</asp:Content>
