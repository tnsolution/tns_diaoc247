﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CustomerList.aspx.cs" Inherits="WebApp.CRM.CustomerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + "px";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">    
    <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
        <div class="breadcrumb">
            <a class="btn btn-info btn-xs" href="/CRM/CustomerEdit.aspx?ID=0">
                <i class="ace-icon glyphicon glyphicon-plus"></i>Add new
            </a>
        </div>
        <!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
            <div class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                </span>
            </div>
        </div>
        <!-- /.nav-search -->
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1>Customers
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    List of customer
                                </small>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="no-more-tables">
                    <asp:Literal ID="Literal_CustomerTable" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdCustomer" role="dialog">
        <div class="modal-dialog modal-lg">
            <iframe src="/CRM/CustomerEdit.aspx?ID=0" frameborder="0" id="icontent" width="100%" onload="resizeIframe(this)"></iframe>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        var closeIFrame = function () {
            $("#mdCustomer").modal("hide");
        };
        $(document).ready(function () {          
            $("button[btnEdit]").on("click", function () {
                var trid = $(this).closest("tr").attr("id");
                var url = "/CRM/CustomerEdit.aspx?ID=" + trid;
                window.location = url;
                //$("iframe").attr("src", url);
                //$("#mdCustomer").modal({
                //    backdrop: true,
                //    show: true
                //});
            });
        });
    </script>
</asp:Content>
