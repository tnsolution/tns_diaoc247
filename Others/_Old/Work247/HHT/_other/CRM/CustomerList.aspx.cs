﻿using Lib.CRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.CRM
{
    public partial class CustomerList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSbTable = new StringBuilder();
            DataTable zTable = Customer_Data.List();

            zSbTable.AppendLine("<table class='table table-bordered table-hover' id='tableCustomer'>");
            zSbTable.AppendLine("   <thead>");
            zSbTable.AppendLine("       <tr>");
            zSbTable.AppendLine("           <th>No.</th>");
            zSbTable.AppendLine("           <th>CustomerName</th>");
            zSbTable.AppendLine("           <th>Address</th>");
            zSbTable.AppendLine("           <th>Action</th>");
            zSbTable.AppendLine("       </tr>");
            zSbTable.AppendLine("   </thead>");
            zSbTable.AppendLine("   <tbody>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow r = zTable.Rows[i];

                zSbTable.AppendLine("       <tr id=" + r["CustomerKey"].ToString() + ">");
                zSbTable.AppendLine("           <td data-title='No.'>" + i.ToString() + " </td>");
                zSbTable.AppendLine("           <td data-title='CustomerName.'>" + r["CustomerName"].ToString() + " </td>");
                zSbTable.AppendLine("           <td data-title='Address.'>" + r["Address"].ToString() + " </td>");
                zSbTable.AppendLine(@"        <td data-title='...'>
                                            <button type='button' class='btn btn-xs btn-white btn-warning btn-bold' btnEdit>
												<i class='ace-icon fa fa-pencil bigger-120 orange'></i>
												Edit
											</button>
                                            <button type='button' class='btn btn-xs btn-white btn-danger btn-bold' btnDelete>
												<i class='ace-icon fa fa-trash-o bigger-120 orange'></i>
												Delete
											</button></td>");
                zSbTable.AppendLine("       </tr>");
            }
            zSbTable.AppendLine("   </tbody>");
            zSbTable.AppendLine("</table>");
            Literal_CustomerTable.Text = zSbTable.ToString();
        }      
    }
}