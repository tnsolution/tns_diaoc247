﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerEdit.aspx.cs" Inherits="WebApp.CRM.CustomerEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-rtl.min.css" />
    <!-- inline styles related to this page -->
    <link href="/WebApp.css" rel="stylesheet" />
    <!-- ace settings handler -->
    <script src="/template/ace-master/assets/js/ace-extra.min.js"></script>
</head>
<body>
    <div class="se-pre-con"></div>
    <form id="form1" runat="server">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Info                     
                </h4>
            </div>
            <div class="modal-body">
                <div class="tabbable ">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#general" aria-expanded="true">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                General Info
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" href="#Contact" aria-expanded="false">
                                <i class="blue ace-icon fa fa-user bigger-110"></i>
                                Contact
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" href="#project" aria-expanded="false">
                                <i class="ace-icon fa fa-rocket"></i>
                                Project History
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="txtCustomerName" placeholder="Full name" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Address</label>

                                    <div class="col-sm-9">
                                        <input type="text" id="txtAddress" placeholder="Address info" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Email</label>

                                    <div class="col-sm-9">
                                        <input type="text" id="txtEmail" placeholder="" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Phone</label>

                                    <div class="col-sm-9">
                                        <input type="text" id="txtPhone" placeholder="" class="col-xs-10 col-sm-12" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-1">Note</label>
                                    <div class="col-sm-9">
                                        <textarea placeholder="" id="txtNote" class="col-xs-10 col-sm-12"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Contact" class="tab-pane">
                            <span class="toolbar pull-right">
                                <a href="#" id="Addrow">
                                    <i class="menu-icon fa fa-plus purple"></i>
                                    Add Contact
                                </a>
                            </span>
                            <table class="table table-bordered table-hover" id="tableContact">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Department</th>
                                        <th>Notes</th>
                                        <th>..</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                        <div id="project" class="tab-pane">
                            <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
                            <p>Raw denim you probably haven't heard of them jean shorts Austin.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info pull-right" id="btnSave">Save</button>
            </div>
        </div>
        <script src="/template/ace-master/assets/js/jquery-2.1.4.min.js"></script>
        <script src="/template/ace-master/assets/js/bootstrap.min.js"></script>
        <script src="/template/ace-master/assets/js/ace-elements.min.js"></script>
        <script src="/template/ace-master/assets/js/ace.min.js"></script>
        <script src="/template/ace-master/plugins/mindmup-editabletable.js"></script>
        <script type="text/javascript">
            var CustomerKey = getUrlParameter("ID");
            var Error = false;
            function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
            function FetchContact(object) {
                if (object != null) {
                    var rowCount = $('#tableContact tbody tr').length + 1;
                    var row = "<tr>"
                    row += "<td tabindex='1'>" + rowCount + "</td>"
                    row += "<td tabindex='1'>" + object.ContactName + "</td>"
                    row += "<td tabindex='1'>" + object.MobiPhone + "</td>"
                    row += "<td tabindex='1'>" + object.DepartmentName + "</td>"
                    row += "<td tabindex='1'>" + object.Notes + "</td>"
                    row += "<td data-editable='false'>"
                    row += "<button class='btn btn-xs btn-white btn-danger btn-bold'>"
                    row += "<i class='ace-icon fa fa-trash-o bigger-120 danger'></i>Delete"
                    row += "</button>"
                    row += "</td>"
                    row += "</tr>";
                    $("#tableContact").append(row);

                    return;
                }

                var rowCount = $('#tableContact tbody tr').length + 1;
                var row = "<tr>"
                row += "<td tabindex='1'>" + rowCount + "</td>"
                row += "<td tabindex='1'></td>"
                row += "<td tabindex='1'></td>"
                row += "<td tabindex='1'></td>"
                row += "<td tabindex='1'></td>"
                row += "<td data-editable='false'>"
                row += "<button class='btn btn-xs btn-white btn-danger btn-bold'>"
                row += "<i class='ace-icon fa fa-trash-o bigger-120 danger'></i>Delete"
                row += "</button>"
                row += "</td>"
                row += "</tr>";
                $("#tableContact").append(row);
            }
            function LoadCustomer() {
                $.ajax({
                    type: "POST",
                    url: "/CRM/CustomerEdit.aspx/LoadData",
                    data: JSON.stringify({
                        "Key": CustomerKey,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d.CustomerKey != 0) {
                            $("#txtCustomerName").val(msg.d.CustomerName);
                            $("#txtAddress").val(msg.d.Address);
                            $("#txtEmail").val(msg.d.Email);
                            $("#txtPhone").val(msg.d.Phone);
                            $("#txtNote").val(msg.d.Note);
                        }
                        else {
                            if (msg.d == 'ERROR') {
                                alert('Error getting data !');
                            }
                        }
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
            function LoadContact() {
                $.ajax({
                    type: "POST",
                    url: "/CRM/CustomerEdit.aspx/LoadContact",
                    data: JSON.stringify({
                        "Key": CustomerKey,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d.length != 0) {
                            for (var i = 0; i < msg.d.length; i++) {
                                FetchContact(msg.d[i]);
                            }
                        }
                        else {
                            if (msg.d == 'ERROR') {
                                alert('Error getting data !');
                            }
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
            function SaveCustomer() {
                $.ajax({
                    type: 'POST',
                    url: '/CRM/CustomerEdit.aspx/SaveData',
                    data: JSON.stringify({
                        'CustomerKey': CustomerKey,
                        'CustomerName': $("#txtCustomerName").val(),
                        'Address': $("#txtAddress").val(),
                        'Email': $("#txtEmail").val(),
                        'Phone': $("#txtPhone").val(),
                        'Note': $("#txtNote").val()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        if (msg.d != "ERROR")
                            CustomerKey = msg.d;
                        else
                            Error = true;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert('Lỗi liên hệ Admin');
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    },
                    complete: function () {
                    }
                });
            }
            function SaveContact() {
                var total = $('#tableContact tbody tr').length;
                var num = 0;
                var tabledata = '';
                $('#tableContact tbody tr').each(function () {
                    num++;
                    if (num == total) {
                        $(this).find('td').each(function () {
                            tabledata += $(this).html() + ',';
                        });
                    }
                    else {
                        $(this).find('td').each(function () {
                            tabledata += $(this).html() + ',';
                        });
                        tabledata += ';';
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: 'CustomerEdit.aspx/SaveContact',
                    data: JSON.stringify({
                        'table': tabledata,
                        'CustomerKey': CustomerKey,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            return;
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    error: function () {
                        alert('Lỗi xin liên hệ Admin !');
                    },
                    complete: function () {
                    }
                });
            }
            $(document).ready(function () {
                if (CustomerKey != 0) {
                    LoadCustomer();
                    LoadContact();
                }
                $("button[data-dismiss='modal']").click(function (e) {
                    parent.closeIFrame();
                });

                $("#tableContact").editableTableWidget();
                $("#Addrow").click(function () {
                    FetchContact(null);
                });
                $("#btnSave").click(function () {
                    $(".se-pre-con").fadeIn("slow");
                    Error = SaveCustomer();
                    if (!Error) {
                        SaveContact();
                        parent.closeIFrameRefesh();
                    }
                });
            });
        </script>
    </form>
</body>
</html>
