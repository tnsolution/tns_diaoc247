﻿using Lib.CRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.CRM
{
    public partial class CustomerEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static Customer_Info LoadData(int Key)
        {
            Customer_Info zInfo = new Customer_Info(Key);
            return zInfo;
        }

        [WebMethod]
        public static Contact_Personal_Info[] LoadContact(int Key)
        {
            List<Contact_Personal_Info> zTable = Customer_Data.List_Contact(Key).DataTableToList<Contact_Personal_Info>();
            return zTable.ToArray();
        }

        [WebMethod]
        public static string SaveData(string CustomerKey, string CustomerName, string Address, string Email, string Phone, string Note)
        {
            Customer_Info zInfo;
            if (CustomerKey != string.Empty)
                zInfo = new Customer_Info(CustomerKey.ToInt());
            else
                zInfo = new Customer_Info();

            zInfo.CustomerName = CustomerName;
            zInfo.Phone = Phone;
            zInfo.Address = Address;
            zInfo.Email = Email;
            zInfo.Note = Note;
            zInfo.Save();

            if (zInfo.Message == string.Empty)
                return zInfo.CustomerKey.ToString();
            else
                return "ERROR";
        }

        [WebMethod]
        public static string SaveContact(string table, string CustomerKey)
        {
            string zSql = " DELETE FROM CRM_ContactPersonal WHERE CustomerKey = " + CustomerKey;

            string[] row = table.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            if (row.Count() > 0)
            {
                foreach (string item in row)
                {
                    string[] col = item.Split(',');

                    zSql += " INSERT INTO CRM_ContactPersonal(CustomerKey,ContactName,MobiPhone,DepartmentName,Notes) VALUES";
                    zSql += " (" + CustomerKey + ",N'" + col[1] + "',N'" + col[2] + "',N'" + col[3] + "',N'" + col[4] + "')";
                }
            }

            int Result = 0;
            int.TryParse(CustomInsert.Exe(zSql), out Result);
            if (Result >= 0)
                return "OK";
            else
                return "ERROR";
        }

        //[WebMethod]
        //public static string SaveContact(string ContactKey, string CustomerKey, string ContactName, string DepartmentName, string MobiPhone, string Note)
        //{
        //    Contact_Personal_Info zInfo;
        //    if (ContactKey != string.Empty)
        //        zInfo = new Contact_Personal_Info(ContactKey.ToInt());
        //    else
        //        zInfo = new Contact_Personal_Info();

        //    zInfo.CustomerKey = CustomerKey.ToInt();
        //    zInfo.ContactName = ContactName;
        //    zInfo.MobiPhone = MobiPhone;
        //    zInfo.Notes = Note;

        //    return zInfo.ContactKey.ToString();
        //}
    }
}