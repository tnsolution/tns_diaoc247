﻿--tinh KPI cá nhân theo thời gian và từ thời gian
DECLARE @Employee INT
DECLARE @Department INT
DECLARE @FromDate DATETIME
DECLARE @ToDate DATETIME
DECLARE @Category INT

SET @Category			= 306 -- 291 -- 290
SET @Employee			= 12
SET @Department		= 15
SET @FromDate			= '2018-03-01 00:00:00' --= '2018-03-14 00:00:00'
SET @ToDate			= '2018-03-31 23:59:59' --= '2018-03-14 23:59:59'

SELECT AutoKey, Product AS [Name],
dbo.KPI_DoingV2(@Department, @Employee, AutoKey, @FromDate, @ToDate) AS [Report],
dbo.KPI_RequireV2(@Department, @Employee, AutoKey, @FromDate, @ToDate) AS [Plan]
FROM SYS_Categories 
WHERE [TYPE] = 34 AND [VALUE] = 1 
ORDER BY [RANK]
-- KHÁCH HÀNG Chăm sóc Khách hàng
SELECT A.[Description], A.CategoryKey, A.CategoryPlan,
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_SysCategoryName(A.CategoryPlan) [Type],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.EmployeeKey = @Employee
AND A.DepartmentKey = @Department
AND A.CreatedDate BETWEEN @FromDate AND @ToDate 
AND CategoryPlan IN (306)
-- KHÁCH HÀNG Khách phát sinh mới
SELECT A.[Description], A.CategoryKey, A.CategoryPlan,
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_SysCategoryName(A.CategoryPlan) [Type],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.EmployeeKey = @Employee
AND A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate 
AND @ToDate 
AND CategoryPlan IN (291)
-- KHÁCH HÀNG Khách dự án
SELECT A.[Description], A.CategoryKey, A.CategoryPlan,
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_SysCategoryName(A.CategoryPlan) [Type],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.EmployeeKey = @Employee
AND A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate 
AND @ToDate 
AND CategoryPlan IN (290)
-- GIAO DỊCH CỌC (DUYỆT)
SELECT A.[Description], B.Income, B.AssetID, 
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetProjectName(B.ProjectKey) ProjectName
FROM KPI_Trades A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.EmployeeKey = @Employee
AND A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate AND @ToDate 
AND CategoryPlan IN (298)
-- GIAO DỊCH CỌC (HOÀN TẤT)
SELECT A.[Description], B.Income, B.AssetID, 
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetProjectName(B.ProjectKey) ProjectName
FROM KPI_Trades A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.EmployeeKey = @Employee
AND A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate AND @ToDate 
AND CategoryPlan IN (295)