﻿--tinh KPI phòng theo thời gian và từ thời gian

DECLARE @Employee INT
DECLARE @Department INT
DECLARE @FromDate DATETIME
DECLARE @ToDate DATETIME
DECLARE @Category INT

SET @Category			= 306 -- 291 -- 290
SET @Employee			= 0
SET @Department		= 15
SET @FromDate			= '2018-03-01 00:00:00' --= '2018-03-14 00:00:00'
SET @ToDate			= '2018-03-31 23:59:59' --= '2018-03-14 23:59:59'

--SELECT AutoKey, Product AS [Name],
--dbo.KPI_DoingV2(@Department, @Employee, AutoKey, @FromDate, @ToDate) AS [Report],
--dbo.KPI_RequireV2(@Department, @Employee, AutoKey, @FromDate, @ToDate) AS [Plan]
--FROM SYS_Categories 
--WHERE [TYPE] = 34 AND [VALUE] = 1 
--ORDER BY [RANK]

SELECT A.EmployeeKey, A.LastName + ' ' + A.FirstName AS [EmployeeName], 
dbo.KPI_DoingV2(A.DepartmentKey, A.EmployeeKey, 306, @FromDate, @ToDate) AS [CSKH],
dbo.KPI_RequireV2(A.DepartmentKey, A.EmployeeKey, 306, @FromDate, @ToDate) AS [Plan_CSKH],
dbo.KPI_DoingV2(A.DepartmentKey, A.EmployeeKey, 291, @FromDate, @ToDate) AS [KHPS],
dbo.KPI_RequireV2(A.DepartmentKey, A.EmployeeKey, 291, @FromDate, @ToDate) AS [Plan_KHPS],
dbo.KPI_DoingV2(A.DepartmentKey, A.EmployeeKey, 290, @FromDate, @ToDate) AS [KHDA],
dbo.KPI_RequireV2(A.DepartmentKey, A.EmployeeKey, 290, @FromDate, @ToDate) AS [Plan_KHDA],
dbo.KPI_DoingV2(A.DepartmentKey, A.EmployeeKey, 307, @FromDate, @ToDate) AS [CNSP],
dbo.KPI_RequireV2(A.DepartmentKey, A.EmployeeKey, 307, @FromDate, @ToDate) AS [Plan_CNSP],
dbo.KPI_DoingV2(A.DepartmentKey, A.EmployeeKey, 298, @FromDate, @ToDate) AS [GDC],
dbo.KPI_RequireV2(A.DepartmentKey, A.EmployeeKey, 298, @FromDate, @ToDate) AS [Plan_GDC],
dbo.KPI_DoingV2(A.DepartmentKey, A.EmployeeKey, 295, @FromDate, @ToDate) AS [GDTC],
dbo.KPI_RequireV2(A.DepartmentKey, A.EmployeeKey, 295, @FromDate, @ToDate) AS [Plan_GDTC]
FROM HRM_Employees A 
WHERE A.DepartmentKey = 15 
ORDER BY A.UnitLevel
-- KHÁCH HÀNG Chăm sóc Khách hàng
SELECT A.[Description], A.CategoryKey, A.CategoryPlan,
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_SysCategoryName(A.CategoryPlan) [Type],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.DepartmentKey = @Department
AND A.CreatedDate BETWEEN @FromDate AND @ToDate 
AND CategoryPlan IN (306)
-- KHÁCH HÀNG Khách phát sinh mới
SELECT A.[Description], A.CategoryKey, A.CategoryPlan,
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_SysCategoryName(A.CategoryPlan) [Type],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate 
AND @ToDate 
AND CategoryPlan IN (291)
-- KHÁCH HÀNG Khách dự án
SELECT A.[Description], A.CategoryKey, A.CategoryPlan,
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_SysCategoryName(A.CategoryPlan) [Type],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate 
AND @ToDate 
AND CategoryPlan IN (290)
-- GIAO DỊCH CỌC (DUYỆT)
SELECT A.[Description], B.Income, B.AssetID, 
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetProjectName(B.ProjectKey) ProjectName
FROM KPI_Trades A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate AND @ToDate 
AND CategoryPlan IN (298)
-- GIAO DỊCH CỌC (HOÀN TẤT)
SELECT A.[Description], B.Income, B.AssetID, 
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetProjectName(B.ProjectKey) ProjectName
FROM KPI_Trades A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.DepartmentKey = @Department 
AND A.CreatedDate BETWEEN @FromDate AND @ToDate 
AND CategoryPlan IN (295)