﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="WorkList.aspx.cs" Inherits="WebApp.JOB.WorkList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="breadcrumbs ace-save-state breadcrumbs-fixed" id="breadcrumbs">
        <div class="breadcrumb">
            <a class="btn btn-info btn-xs" href="/JOB/ProjectEdit.aspx?ID=0">
                <i class="ace-icon glyphicon glyphicon-plus"></i>Add new
            </a>
        </div>
        <!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
            <div class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                </span>
            </div>
        </div>
        <!-- /.nav-search -->
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1>Works
								<small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    List of work
                                </small>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="no-more-tables">
                    <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
