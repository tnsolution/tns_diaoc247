﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="WebApp.SAL.Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/fullcalendar.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" />
    <style>
        .fc-content {
            white-space: inherit !important;
        }

        .tooltipfont {
            font-size: 14px !important;
        }
    </style>
    <link rel="stylesheet" href="/SAL/KPI.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="page-content">
        <div class="page-header">
            <h1><span id="title">
                <asp:Literal ID="LiteralTitle" runat="server"></asp:Literal></span>
                <asp:Literal ID="LiteralButtonTop" runat="server"></asp:Literal>


            </h1>
        </div>
        <div class="row">
            <div class="col-sm-12" id="noidung">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4 id="text">Chi tiết mục tiêu</h4>
                <table class='table table-bordered' id="noidung2">
                    <tbody>
                        <asp:Literal ID="Literal_Table_Sub" runat="server"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:Literal ID="Literal_Button" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="modal fade modal-default" id="mProduct" role="dialog" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            ×</button>
                        <h4 class="modal-title">Xem lịch biểu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row" id="hide">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <asp:DropDownList ID="DDL_Department" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                                            <asp:ListItem Value="0" Text="--Tất cả--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <a class="btn btn-primary" id="btnView">
                                            <i class="ace-icon fa fa-eyes-o"></i>
                                            Xem lịch 
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id='calendar'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="LoadBaoCao" runat="server" Text="Button" Style="display: none" OnClick="LoadBaoCao_Click" />
    <asp:Label ID="lblMuctieu" runat="server" Text="" Style="display: none"></asp:Label>
    <asp:HiddenField ID="HIDFromDate" runat="server" />
    <asp:HiddenField ID="HIDToDate" runat="server" />
    <asp:HiddenField ID="HIDEmployee" runat="server" />
    <asp:HiddenField ID="HIDDepartment" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    <script>
        var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
        var addStartDate;
        var addEndDate;
        var Employee = 0;
        var Department = 0;
        var Unit = 0;
        var CoLich = 0;

        function updateEvent(event, element) {
            addStartDate = event.start.toJSON();
            addEndDate = event.end.toJSON();
            $('.se-pre-con').fadeIn('slow');
            window.location.replace(url + "?Start=" + addStartDate + "&End=" + addEndDate);
        }
        function selectDate(start, end, allDay) {
            addStartDate = start.toJSON();
            addEndDate = end.toJSON();
            $('.se-pre-con').fadeIn('slow');
            window.location.replace(url + "?Start=" + addStartDate + "&End=" + addEndDate);
        }
        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }
        function qTipText(start, end, description) {
            var text;

            //if (end !== null)
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") + "<br/><br/>" + description;
            //else
            //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><br/>" + description;

            return description;
        }
        //---------
        function taobaocao() {
            $('.se-pre-con').fadeIn('slow');
            $("[id$=LoadBaoCao]").trigger("click");
        }
        //---------
        function MergeCommonRows(table) {
            var topMatchTd;
            var previousValue = "";
            var rowSpan = 1;

            $('.phong').each(function () {

                if ($(this).text() == previousValue) {
                    rowSpan++;
                    $(topMatchTd).attr('rowspan', rowSpan);
                    $(this).remove();
                }
                else {
                    topMatchTd = $(this);
                    rowSpan = 1;
                }

                previousValue = $(this).text();
            });
        }
        //---------                      
        $(document).ready(function () {
            taobaocao();
            MergeCommonRows($('#tbl'));
            $('#noidung2').on('click', 'tr', function (e) {
                $(this).closest('tr').next().toggle();
            });
            Unit = Page.getUnitLevel();
            if (Unit < 7) {
                Department = Page.getDepartmentRole();
                $("#hide").show();
            }
            else {
                Employee = Page.getEmployee();
                $("#hide").hide();
            }

            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });
            $("[id$=DDL_Department]").change(function () {
                var Key = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        DepartmentKey: $(this).val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee]");
                        District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                        $.each(msg.d, function () {
                            District.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    }
                });
            });
            $("[id$=btnView]").click(function () {
                Department = $('[id$=DDL_Department]').val();
                $('#calendar').fullCalendar('destroy');
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''//month,agendaWeek,agendaDay
                    },
                    defaultView: 'month',
                    eventClick: updateEvent,
                    selectable: true,
                    selectHelper: true,
                    select: selectDate,
                    editable: true,
                    events: '/JS_Report.ashx?Employee=' + Employee + '&Department=' + Department,
                    eventRender: function (event, element) {
                        element.qtip({
                            content: {
                                text: qTipText(event.title),
                                title: '<strong>' + event.description + '</strong>'
                            },
                            position: {
                                my: 'bottom center',
                                at: 'top center'
                            },
                            style: { classes: 'qtip-shadow qtip-rounded tooltipfont' }
                        });
                    }
                });
            });
            $('#mProduct').on('shown.bs.modal', function (e) {
                if (CoLich == 0) {
                    CoLich = 1;
                    $('#calendar').fullCalendar('destroy');
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: ''//month,agendaWeek,agendaDay
                        },
                        defaultView: 'month',
                        eventClick: updateEvent,
                        selectable: true,
                        selectHelper: true,
                        select: selectDate,
                        editable: true,
                        events: '/JS_Report.ashx?Employee=' + Employee + '&Department=' + Department,
                        eventRender: function (event, element) {
                            //alert(event.title);
                            element.qtip({
                                content: {
                                    text: qTipText(event.title),
                                    title: '<strong>' + event.description + '</strong>'
                                },
                                position: {
                                    my: 'bottom center',
                                    at: 'top center'
                                },
                                style: { classes: 'qtip-shadow qtip-rounded tooltipfont' }
                            });
                        }
                    });
                }
            })
        });

        $('#btnSave').click(function () {
            var key = Page.getEmployee();

            console.log(key);
            console.log($("[id$=HIDFromDate]").val());
            console.log($("[id$=HIDToDate]").val());
            $.ajax({
                type: "POST",
                url: "/SAL/Report.aspx/SendReport",
                data: JSON.stringify({
                    "Employee": key,
                    "FromDate": $("[id$=HIDFromDate]").val(),
                    "ToDate": $("[id$=HIDToDate]").val(),
                    "Text": $("[id$=txtGhichut]").val()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    location.reload(true);
                },
                complete: function () {
                    //$('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                    $('.se-pre-con').fadeOut('slow');
                }
            });
        });
        $('#noidung').on("click", "tr", function () {
            var key = $(this).closest('tr').attr('key');
            if (key != undefined || key != '') {
                $.ajax({
                    type: "POST",
                    url: "/SAL/Report.aspx/ListItem",
                    data: JSON.stringify({
                        "Employee": key,
                        "FromDate": $("[id$=HIDFromDate]").val(),
                        "ToDate": $("[id$=HIDToDate]").val(),
                        "Muctieu": $("[id$=lblMuctieu]").text()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $("#ghichu").empty();
                        $("#noidung2").empty();
                        //$("#ghichu").append(msg.d.Result2);
                        $("#noidung2").append(msg.d);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    }
                });
            }
        });
    </script>
</asp:Content>
