﻿using Lib.HRM;
using Lib.Salary;
using Lib.SYS;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
// 1 truc tiep
// 2 lien ket
//phat sinh giao dich ban mới chạy lũy tiến + vào lãnh lần 1
//giao dich bán mới tính hoa hồng = doanh số nhân số %
namespace WebApp.HRM
{
    public partial class SalaryEdit : System.Web.UI.Page
    {
        static double _HoaHongPhongHoTro = 100 * 1000;
        static double _HoaHongTruongPhong = 500 * 1000;
        static double _HoaHongHoTro = 250 * 1000;

        protected void Page_Load(object sender, EventArgs e)
        {
            //biến tạm thời zay
            bool isManager = CheckRoles();
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                LoadInfo(isManager);
            }
        }

        void LoadInfo(bool isManager)
        {
            if (Request["ID"] != null)
            {
                int Key = Request["ID"].ToInt();
                HID_AutoKey.Value = Key.ToString();
                Salary_Info zInfo = new Salary_Info(Key);

                txtFromDate.Value = zInfo.FromDate.ToString("dd/MM/yyyy");
                txtToDate.Value = zInfo.ToDate.ToString("dd/MM/yyyy");
                txtdescription.Value = zInfo.Description;
                txtNote1.Value = zInfo.Note1;
                DDLTag.SelectedValue = zInfo.Tag.ToString();
                DDL_Employee.SelectedValue = zInfo.EmployeeKey.ToString();

                #region [Bảng thông tin]
                txt1.Value = zInfo.Param1.ToString("n0");
                txt2.Value = zInfo.Param2.ToString("n0");
                txt3.Value = zInfo.Param3.ToString("n1");
                txt4.Value = zInfo.Param4.ToString("n1");
                txt5.Value = zInfo.Param5.ToString();
                txt51.Value = zInfo.Param51.ToString("n0");
                txt6.Value = zInfo.Param6.ToString("n0");
                txt7.Value = zInfo.Param7.ToString("n0");
                txt8.Value = zInfo.Param8.ToString("n0");
                txt9.Value = zInfo.Param9.ToString("n0");
                txt10.Value = zInfo.Param10.ToString("n0");
                txt11.Value = zInfo.Param11.ToString("n0");
                txt12.Value = zInfo.Param12.ToString("n0");
                txt13.Value = zInfo.Param13.ToString("n0");
                txt14.Value = zInfo.Param14.ToString("n0");
                #endregion

                DateTime FromDate = zInfo.FromDate;
                DateTime ToDate = zInfo.ToDate;
                StringBuilder zSb = new StringBuilder();
                DataTable zTable = Salary_Data.GetTable(zInfo.AutoKey);
                zSb.AppendLine(" <table class='table table-bordered table-hover' id=tblCommission>");
                zSb.AppendLine(" <tr>");
                zSb.AppendLine("    <th>STT</th>");
                zSb.AppendLine("    <th class='td10'>Mã căn</th>");
                zSb.AppendLine("    <th>Loại hoa hồng</th>");
                zSb.AppendLine("    <th>Nguồn giao dịch</th>");
                zSb.AppendLine("    <th>Tổng giá chưa VAT</th>");
                zSb.AppendLine("    <th>Phí nội bộ</th>");
                zSb.AppendLine("    <th>Doanh thu thực tế</th>");
                zSb.AppendLine("    <th>Hoa Hồng</th>");
                zSb.AppendLine(" </tr>");

                #region [Trích lọc thông tin]
                DataRow[] rDirect = zTable.Select("[Type]='TT'");
                DataRow[] rSupport = zTable.Select("[Type]='HT'");
                DataRow[] rShortTerm = zTable.Select("[Type]='NH'");
                DataRow[] rDirectCommission = null;
                DataRow[] rReferenceCommission = null;
                DataTable ztblDirect = new DataTable();
                if (rDirect.Count() > 0)
                {
                    ztblDirect = rDirect.CopyToDataTable();
                    rDirectCommission = ztblDirect.Select("[Source]=1");
                    rReferenceCommission = ztblDirect.Select("[Source]=2");
                }
                #endregion                                

                double TotalCommission =
                    MoneyCommissionDirect(rDirectCommission) +
                    MoneyCommissionReference(rReferenceCommission) +
                    MoneyCommissionSupport(rSupport) +
                    MoneyCommissionSupportShortTerm(rShortTerm);

                zSb.Append(CommissionReference(rReferenceCommission));
                zSb.Append(CommissionDirect(rDirectCommission));
                zSb.Append(CommissionSupport(rSupport));
                zSb.Append(CommissionSupportShortTerm(rShortTerm));

                zSb.AppendLine(" <tr class=tong>");
                zSb.AppendLine("    <td colspan=7>Tổng</td>");
                zSb.AppendLine("    <td class='text-right giatien' id=hieuqua>" + TotalCommission.ToString("n0") + "</td>");
                zSb.AppendLine(" </tr>");
                //
                zSb.AppendLine(" </table>");
                Lit_Commission.Text = zSb.ToString();

                #region [Duyet]
                string html = "";
                string[] GetAllManager = zInfo.Step.Split(',');
                foreach (string s in GetAllManager)
                {
                    int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    int EmployeeKey = s.ToInt();
                    string EmployeeName = Employees_Data.GetManagerName(EmployeeKey);
                    int Unit = Employees_Data.GetLevel(EmployeeKey);
                    int Accept = Notification_Data.Check_Salary(EmployeeKey, zInfo.AutoKey);

                    string flag = "";
                    if (Accept == 0)
                    { flag = "false"; }
                    else
                    { flag = "true"; }

                    if (Accept == 0)
                    {
                        flag = "false";
                        if (CurrentEmployee == EmployeeKey &&
                            CurrentEmployee != zInfo.EmployeeKey &&
                            Unit <= 2)
                        {
                            html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='cursor:pointer; width: 32px' action='1' id='" + EmployeeKey + @"' alt='' src='../template/custom-image/" + flag + @".png' />
                            <img style='cursor:pointer; width: 32px' action='2' id='" + EmployeeKey + @"' alt='' src='../template/custom-image/cross.png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                        }
                        else
                        {
                            html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='cursor:pointer; width: 32px' action=1 id='" + EmployeeKey + @"' alt='' src='../template/custom-image/" + flag + @".png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                        }
                    }
                    if (Accept == 1)
                    {
                        flag = "true";
                        html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='width: 32px' alt='' src='../template/custom-image/" + flag + @".png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                    }
                    if (Accept == 2)
                    {
                        flag = "no";
                        html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='width: 32px' alt='' src='../template/custom-image/cross.png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                    }
                }
                LitProcess.Text = html;
                #endregion

                if (!isManager)
                    Html(zInfo);
            }
        }
        void Html(Salary_Info zInfo)
        {
            lblTitle1.Text = zInfo.EmployeeName + " Tổng lương đã nhận trong năm: <b style='color: #c74444'>" + Salary_Data.SumSalary(zInfo.EmployeeKey, zInfo.FromDate.Year).ToString("n0") + "</b>";
            lblTitle.Text = zInfo.FromDate.ToString("MM/yyyy");
            Literal1.Text = @"<table class='table table-bordered' id='header'>
                <tr>
                    <td>" + Employees_Data.GetManagerName(zInfo.EmployeeKey) + @"</td>
                    <td style='width: 200px'>" + zInfo.FromDate.ToString("dd/MM/yyyy") + @"</td>
                    <td style='width: 200px'>" + zInfo.FromDate.ToString("dd/MM/yyyy") + "</td></tr></table>";
            Literal2.Text = @"<table class='table table-bordered table-striped table-hover' id='body'>
                            <thead>
                                <tr>
                                    <th class='td10'>Thông tin</th>
                                    <th colspan=2>Nội dung</th>
                                </tr>
                            </thead>
                            <tr>
                                <td>1. Lương cơ bản</td><td></td>
                                <td class='text-right' style='width:150px'>" + zInfo.Param1.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>2. Phụ cấp</td><td></td>
                                <td class='text-right'>" + zInfo.Param14.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>3. Phụ cấp trách nhiệm</td><td></td>
                                <td class='text-right'>" + zInfo.Param6.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>4. Ngày công chuẩn</td><td></td>
                                <td class='text-right'>" + zInfo.Param2.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>5. Số ngày nghỉ trong tháng</td><td></td>
                                <td class='text-right'>" + zInfo.Param3.ToString("n1") + @"</td>
                            </tr>
                            <tr>
                                <td>6. Ngày công thực tế</td><td></td>
                                <td class='text-right'>" + zInfo.Param4.ToString("n1") + @"</td>
                            </tr>
                            <tr>
                                <td>7. Bảo hiểm (" + zInfo.Param5 + @"%)</td><td></td>
                                <td class='text-right'>" + zInfo.Param51.ToString("n0") + @"</td>
                            </tr>
                           
                            <tr>
                                <td>8. Lương cơ bản thực nhận</td><td></td>
                                <td class='text-right'>" + zInfo.Param7.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>9. Hiệu quả công việc</td><td></td>
                                <td class='text-right'>" + zInfo.Param8.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>10. Thuế TNCN</td><td></td>
                                <td class='text-right'>" + zInfo.Param9.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>11. Thưởng</td><td style='text-align:right'>" + zInfo.Note1 + @"</td>
                                <td class='text-right'>" + zInfo.Param13.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>12. Lương thực nhận sau thuế</td><td></td>
                                <td class='text-right' style='color: #c74444'>" + zInfo.Param10.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>13. Lãnh lương lần 1</td><td></td>
                                <td class='text-right' style='color: #c74444'>" + zInfo.Param11.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td>14. Lãnh lương lần 2</td><td></td>
                                <td class='text-right' style='color: #c74444'>" + zInfo.Param12.ToString("n0") + @"</td>
                            </tr>
                            <tr>
                                <td colspan='3'>" + zInfo.Description + @"</td>
                            </tr>
                        </table>";
        }

        [WebMethod]
        public static int CheckEmployee(int sEmployee, int Month, int Year)
        {
            Salary_Info zInfo = new Salary_Info(sEmployee, Month, Year);
            return zInfo.AutoKey;
        }

        [WebMethod]
        public static ItemReturn Approve(int AutoKey, int Employee, int Action)
        {
            ItemReturn zResult = new ItemReturn();
            Salary_Info zInfo = new Salary_Info(AutoKey);
            string[] temp = zInfo.Step.Split(',');
            string SQL = "";

            int Step = 0;
            foreach (string s in temp)
            {
                int IndexStep = s.ToInt();
                if (Employee == IndexStep)
                {
                    break;
                }
                Step++;
            }

            //số 1 là key nhan viên có cấp lớn nhất hiện tại
            //là cấp cuối cùng ko gửi nữa
            if (Step == 2 && Employee == 1)
            {
                #region [Update Current]
                int Key = Notification_Data.GetMessageSalary(AutoKey, Employee);
                if (Key == 0)
                {
                    SQL = @"INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey, Readed,ReadedDate,Accept,AcceptDate, Description) 
            VALUES (N'Cần duyệt lương',N'Cần duyệt lương', GETDATE(), N'HRM_Salary'," + AutoKey + "," + Employee + ",1,GETDATE()," + Action + ",GETDATE(),N'Tình Trạng lương')";
                    CustomInsert.Exe(SQL);
                }
                else
                {
                    SQL = @"UPDATE SYS_Message SET Readed = 1, ReadedDate =GETDATE(), Accept=" + Action + ", AcceptDate=GETDATE() WHERE ObjectKey =" + AutoKey + " AND EmployeeKey = " + Employee;
                    CustomInsert.Exe(SQL);
                }
                #endregion
            }
            else
            {
                #region [Update Current]
                int Key = Notification_Data.GetMessageSalary(AutoKey, Employee);
                if (Key == 0)
                {
                    SQL = @"INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey, Readed,ReadedDate,Accept,AcceptDate, Description) 
            VALUES (N'Cần duyệt lương',N'Cần duyệt lương', GETDATE(), N'HRM_Salary'," + AutoKey + "," + Employee + ",1,GETDATE()," + Action + ",GETDATE(),N'Tình Trạng lương')";
                    CustomInsert.Exe(SQL);
                }
                else
                {
                    SQL = @"UPDATE SYS_Message SET Readed = 1, ReadedDate =GETDATE(), Accept=" + Action + ", AcceptDate=GETDATE() WHERE ObjectKey =" + AutoKey + " AND EmployeeKey = " + Employee;
                    CustomInsert.Exe(SQL);
                }
                #endregion

                #region [Send Next]
                int NextEmployee = temp[Step + 1].ToInt();
                SQL = @"INSERT INTO SYS_Message 
           (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey, Readed,ReadedDate,Accept,AcceptDate, Description) 
            VALUES (N'Cần duyệt lương',N'Cần duyệt lương', GETDATE(), N'HRM_Salary'," + AutoKey + "," + NextEmployee + ",0,GETDATE(),0,GETDATE(),N'Tình Trạng lương')";
                CustomInsert.Exe(SQL);
                #endregion
            }
            return zResult;
        }

        [WebMethod]
        public static ItemSalary Process(string sEmployee, string sFromDate, string sToDate, string sthuong)
        {
            DateTime FromDate = Tools.ConvertToDate(sFromDate);
            DateTime ToDate = Tools.ConvertToDate(sToDate);
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            StringBuilder zSb = new StringBuilder();
            ItemSalary Obj = new ItemSalary();

            int PositionKey = 0;
            int DepartmentKey = 0;
            int EmployeeKey = 0;

            Employees_Info zEmployee = new Employees_Info(sEmployee.ToInt());
            PositionKey = zEmployee.PositionKey;
            DepartmentKey = zEmployee.DepartmentKey;
            EmployeeKey = zEmployee.Key;
            DataTable zTable = Salary_Data.GetCommission(DepartmentKey, EmployeeKey, FromDate, ToDate);

            //-----------------

            double KQ = 0;
            #region [table header]
            zSb.AppendLine(" <table class='table table-bordered table-hover' id=tblCommission>");
            zSb.AppendLine(" <tr class=tong>");
            zSb.AppendLine("    <th>STT</th>");
            zSb.AppendLine("    <th class='td10'>Mã căn</th>");
            zSb.AppendLine("    <th>Loại hoa hồng</th>");
            zSb.AppendLine("    <th>Nguồn giao dịch</th>");
            zSb.AppendLine("    <th>Tổng giá chưa VAT</th>");
            zSb.AppendLine("    <th>Phí nội bộ</th>");
            zSb.AppendLine("    <th>Doanh thu thực tế</th>");
            zSb.AppendLine("    <th>Hoa Hồng</th>");
            zSb.AppendLine(" </tr>");
            #endregion
            if (zTable.Rows.Count > 0)
            {
                //xử lý ngắn hạn
                #region [shortTerm]
                DataTable ztblShortTerm = new DataTable();
                ztblShortTerm.Columns.Add("AssetID", typeof(string));
                ztblShortTerm.Columns.Add("Type", typeof(string));
                ztblShortTerm.Columns.Add("Source", typeof(string));
                ztblShortTerm.Columns.Add("Income", typeof(string));          //1
                ztblShortTerm.Columns.Add("InternalCost", typeof(string));    //2
                ztblShortTerm.Columns.Add("Result", typeof(string));              //3=1-2
                ztblShortTerm.Columns.Add("Commission", typeof(string));

                DataTable zTableShortTerm1 = new DataTable();
                DataTable zTableShortTerm2 = new DataTable();
                if (PositionKey <= 3 || DepartmentKey == 13)
                {
                    if (DepartmentKey == 13)
                    {
                        // tinh hoa hong neu user (hiện tại) thuộc bo phan ho tro

                        //int DepartmentCheck = rPhong["DepartmentKey"].ToInt();
                        int MonthGet = 0;
                        int Result = 0;
                        zTableShortTerm1 = Salary_Data.CommissionShortTerm1(FromDate, ToDate, DepartmentKey, 0);
                        zTableShortTerm2 = Salary_Data.CommissionShortTerm2(FromDate, ToDate, DepartmentKey, 0);

                        #region ngan han truc tiep
                        Result = zTableShortTerm1.Rows.Count;
                        if (Result > 0)
                        {
                            MonthGet = Result / 6;
                            if (MonthGet >= 1)
                            {
                                double HoaHong = _HoaHongPhongHoTro * MonthGet;
                                double GiaChuaVAT = 0;
                                double DoanhThu = 0;
                                string AssetTemp = "";
                                string AssetName = "";
                                int Num = 0;

                                DataView view = new DataView(zTableShortTerm1);
                                DataTable distinctValues = view.ToTable(true, "Income", "AssetID");

                                AssetTemp = distinctValues.Rows[0]["AssetID"].ToString();
                                foreach (DataRow r in distinctValues.Rows)
                                {
                                    Num++;
                                    if (AssetTemp != r["AssetID"].ToString().Trim())
                                    {
                                        AssetName = AssetTemp + "(" + Num + ")" + ", ";
                                        AssetTemp = r["AssetID"].ToString().Trim();

                                        Num = 0;
                                    }

                                    GiaChuaVAT += r["Income"].ToDouble();
                                    DoanhThu += r["InCome"].ToDouble();
                                }

                                DataRow rShortTerm = ztblShortTerm.NewRow();
                                rShortTerm["AssetID"] = AssetName.Remove(AssetName.LastIndexOf(","), 1) + " <br/> Tổng thời gian thuê " + Result + " tháng";
                                rShortTerm["Type"] = "Hỗ trợ ngắn hạn";
                                rShortTerm["Source"] = "1";
                                rShortTerm["Income"] = GiaChuaVAT.ToString("n0");
                                rShortTerm["Result"] = DoanhThu.ToString("n0");
                                rShortTerm["InternalCost"] = 0;
                                rShortTerm["Commission"] = HoaHong.ToString("n0");
                                ztblShortTerm.Rows.Add(rShortTerm);
                            }
                        }
                        #endregion
                        #region ngan han lien ket
                        Result = zTableShortTerm2.Rows.Count;
                        if (Result > 0)
                        {
                            MonthGet = Result / 12;
                            if (MonthGet >= 1)
                            {
                                double HoaHong = _HoaHongHoTro * MonthGet;
                                double GiaChuaVAT = 0;
                                double DoanhThu = 0;
                                string AssetTemp = "";
                                string AssetName = "";
                                int Num = 0;
                                DataView view = new DataView(zTableShortTerm2);
                                DataTable distinctValues = view.ToTable(true, "Income", "Money", "AssetID");
                                AssetTemp = distinctValues.Rows[0]["AssetID"].ToString();
                                foreach (DataRow r in distinctValues.Rows)
                                {
                                    Num++;
                                    if (AssetTemp != r["AssetID"].ToString().Trim())
                                    {
                                        AssetName = AssetTemp + "(" + Num + ")" + ", ";
                                        AssetTemp = r["AssetID"].ToString().Trim();

                                        Num = 0;
                                    }
                                    GiaChuaVAT += r["Money"].ToDouble();
                                    DoanhThu += r["InCome"].ToDouble();
                                }

                                DataRow rShortTerm = ztblShortTerm.NewRow();
                                rShortTerm["AssetID"] = AssetName.Remove(AssetName.LastIndexOf(","), 1);
                                rShortTerm["Type"] = "Hỗ trợ ngắn hạn";
                                rShortTerm["Source"] = "2";
                                rShortTerm["Income"] = GiaChuaVAT.ToString("n0");
                                rShortTerm["Result"] = DoanhThu.ToString("n0");
                                rShortTerm["InternalCost"] = 0;
                                rShortTerm["Commission"] = HoaHong.ToString("n0");
                                ztblShortTerm.Rows.Add(rShortTerm);
                            }
                        }
                        #endregion

                        #region 02-05-2018 bỏ vòng for
                        //DataTable ztblPhong = Departments_Data.List();
                        //foreach (DataRow rPhong in ztblPhong.Rows)
                        //{
                        //    int DepartmentCheck = rPhong["DepartmentKey"].ToInt();
                        //    int MonthGet = 0;
                        //    int Result = 0;
                        //    zTableShortTerm1 = Salary_Data.CommissionShortTerm1(FromDate, ToDate, DepartmentCheck, 0);
                        //    zTableShortTerm2 = Salary_Data.CommissionShortTerm2(FromDate, ToDate, DepartmentCheck, 0);

                        //    #region ngan han truc tiep
                        //    Result = zTableShortTerm1.Rows.Count;
                        //    if (Result > 0)
                        //    {
                        //        MonthGet = Result / 6;
                        //        if (MonthGet >= 1)
                        //        {
                        //            double HoaHong = _HoaHongHoTro * MonthGet;
                        //            double GiaChuaVAT = 0;
                        //            double DoanhThu = 0;
                        //            string Asset = "";

                        //            DataView view = new DataView(zTableShortTerm1);
                        //            DataTable distinctValues = view.ToTable(true, "Income", "AssetID");

                        //            foreach (DataRow r in distinctValues.Rows)
                        //            {
                        //                Asset += r["AssetID"].ToString().Trim() + ", ";
                        //                GiaChuaVAT += r["Income"].ToDouble();
                        //                DoanhThu += r["InCome"].ToDouble();
                        //            }

                        //            DataRow rShortTerm = ztblShortTerm.NewRow();
                        //            rShortTerm["AssetID"] = Asset.Remove(Asset.LastIndexOf(","), 1) + " <br/> Tổng thời gian thuê " + Result + " tháng";
                        //            rShortTerm["Type"] = "Hỗ trợ ngắn hạn";
                        //            rShortTerm["Source"] = "1";
                        //            rShortTerm["Income"] = GiaChuaVAT.ToString("n0");
                        //            rShortTerm["Result"] = DoanhThu.ToString("n0");
                        //            rShortTerm["InternalCost"] = 0;
                        //            rShortTerm["Commission"] = HoaHong.ToString("n0");
                        //            ztblShortTerm.Rows.Add(rShortTerm);
                        //        }
                        //    }
                        //    #endregion
                        //    #region ngan han lien ket
                        //    Result = zTableShortTerm2.Rows.Count;
                        //    if (Result > 0)
                        //    {
                        //        MonthGet = Result / 12;
                        //        if (MonthGet >= 1)
                        //        {
                        //            double HoaHong = _HoaHongHoTro * MonthGet;
                        //            double GiaChuaVAT = 0;
                        //            double DoanhThu = 0;
                        //            string Asset = "";

                        //            DataView view = new DataView(zTableShortTerm2);
                        //            DataTable distinctValues = view.ToTable(true, "Income", "Money", "AssetID");

                        //            foreach (DataRow r in zTableShortTerm2.Rows)
                        //            {
                        //                Asset += r["AssetID"].ToString().Trim() + ", ";
                        //                GiaChuaVAT += r["Money"].ToDouble();
                        //                DoanhThu += r["InCome"].ToDouble();
                        //            }

                        //            DataRow rShortTerm = ztblShortTerm.NewRow();
                        //            rShortTerm["AssetID"] = Asset.Remove(Asset.LastIndexOf(","), 1);
                        //            rShortTerm["Type"] = "Hỗ trợ ngắn hạn";
                        //            rShortTerm["Source"] = "2";
                        //            rShortTerm["Income"] = GiaChuaVAT.ToString("n0");
                        //            rShortTerm["Result"] = DoanhThu.ToString("n0");
                        //            rShortTerm["InternalCost"] = 0;
                        //            rShortTerm["Commission"] = HoaHong.ToString("n0");
                        //            ztblShortTerm.Rows.Add(rShortTerm);
                        //        }
                        //    }
                        //    #endregion
                        //}
                        #endregion
                    }
                    else
                    {
                        // tinh hoa hong neu user (hiện tại) là cap quan ly chỉ danh cho trượng phòng 
                        // chi lay các giao dịch không phải của trưởng phòng (lay của nhân viên trực thuộc phòng)

                        int MonthGet = 0;
                        int Result = 0;
                        zTableShortTerm1 = Salary_Data.CommissionShortTerm1(FromDate, ToDate, DepartmentKey, EmployeeKey);
                        zTableShortTerm2 = Salary_Data.CommissionShortTerm2(FromDate, ToDate, DepartmentKey, EmployeeKey);

                        #region ngan han truc tiep
                        Result = zTableShortTerm1.Rows.Count;
                        if (Result > 0)
                        {
                            MonthGet = Result / 6;
                            if (MonthGet >= 1)
                            {
                                int Num = 0;
                                double HoaHong = _HoaHongTruongPhong * MonthGet;
                                double GiaChuaVAT = 0;
                                double DoanhThu = 0;
                                string AssetTemp = "";
                                string AssetName = "";
                                AssetTemp = zTableShortTerm1.Rows[0]["AssetID"].ToString().Trim();
                                foreach (DataRow r in zTableShortTerm1.Rows)
                                {
                                    Num++;
                                    if (AssetTemp != r["AssetID"].ToString().Trim())
                                    {
                                        AssetName = AssetTemp + "(" + Num + ")" + ", ";
                                        AssetTemp = r["AssetID"].ToString().Trim();

                                        Num = 0;
                                    }
                                    GiaChuaVAT += r["Income"].ToDouble();
                                    DoanhThu += r["Income"].ToDouble();
                                }

                                DataRow rShortTerm = ztblShortTerm.NewRow();
                                rShortTerm["AssetID"] = AssetName.Remove(AssetName.LastIndexOf(","), 1) + " <br /> Tổng thời gian thuê " + Result + " tháng";
                                rShortTerm["Type"] = "Hỗ trợ ngắn hạn";
                                rShortTerm["Source"] = "1";
                                rShortTerm["Income"] = GiaChuaVAT.ToString("n0");
                                rShortTerm["Result"] = DoanhThu.ToString("n0");
                                rShortTerm["InternalCost"] = 0;
                                rShortTerm["Commission"] = HoaHong.ToString("n0");
                                ztblShortTerm.Rows.Add(rShortTerm);
                            }
                        }
                        #endregion
                        #region ngan han lien ket
                        Result = zTableShortTerm2.Rows.Count;
                        if (Result > 0)
                        {
                            MonthGet = Result / 12;
                            if (MonthGet >= 1)
                            {
                                int Num = 0;
                                double HoaHong = _HoaHongTruongPhong * MonthGet;
                                double GiaChuaVAT = 0;
                                double DoanhThu = 0;
                                string AssetTemp = "";
                                string AssetName = "";
                                AssetTemp = zTableShortTerm1.Rows[0]["AssetID"].ToString().Trim();

                                foreach (DataRow r in zTableShortTerm2.Rows)
                                {
                                    Num++;
                                    if (AssetTemp != r["AssetID"].ToString().Trim())
                                    {
                                        AssetName = AssetTemp + "(" + Num + ")" + ", ";
                                        AssetTemp = r["AssetID"].ToString().Trim();

                                        Num = 0;
                                    }
                                    GiaChuaVAT += r["Income"].ToDouble();
                                    DoanhThu += r["Income"].ToDouble();
                                }

                                DataRow rShortTerm = ztblShortTerm.NewRow();
                                rShortTerm["AssetID"] = AssetName.Remove(AssetName.LastIndexOf(","), 1) + " <br/>Tổng thời gian thuê " + Result + " tháng";
                                rShortTerm["Type"] = "Hỗ trợ ngắn hạn";
                                rShortTerm["Source"] = "2";
                                rShortTerm["Income"] = GiaChuaVAT.ToString("n0");
                                rShortTerm["Result"] = DoanhThu.ToString("n0");
                                rShortTerm["InternalCost"] = 0;
                                rShortTerm["Commission"] = HoaHong.ToString("n0");
                                ztblShortTerm.Rows.Add(rShortTerm);
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                //tinh tách nguồn
                if (zEmployee.Method == 1)
                {
                    #region [Bảng hiệu quả]

                    #region [Trích lọc thông tin]
                    DataRow[] rDirect = zTable.Select("[Type]='TT'");
                    DataRow[] rSupport = zTable.Select("[Type]='HT'");
                    DataRow[] rDirectCommission = null;
                    DataRow[] rReferenceCommission = null;
                    DataTable ztblDirect = new DataTable();
                    if (rDirect.Count() > 0)
                    {
                        ztblDirect = rDirect.CopyToDataTable();
                        rDirectCommission = ztblDirect.Select("[Source]=1");
                        rReferenceCommission = ztblDirect.Select("[Source]=2");
                    }
                    #endregion

                    double MoneyDirectQuery = 0;    //dung chay bang
                    double MoneyReferenceQuery = 0; //dung chay bang
                    double MoneySupportTotal = 0;//chi dung de hien thi số tổng doanh thu hổ trợ
                    double MoneyCommission = 0;
                    double AmountCommission = 0;

                    #region [Giao dịch cá nhân liên Kết]
                    MoneyCommission = 0;
                    if (rReferenceCommission != null && rReferenceCommission.Count() > 0)
                    {
                        zSb.Append(MultiBroadCommissionReference(rReferenceCommission));
                        //tinh tong danh thu thuc te
                        MoneyReferenceQuery = MultiBroadCommissionReferenceMoney(rReferenceCommission);
                        //tinh hoa hong chay bang
                        MoneyCommission = MoneyProgressiveReference(EmployeeKey, MoneyReferenceQuery);
                        #region [Footer Tong lien ket]
                        zSb.AppendLine(" <tr style='background-color: #a5d4f4' Source=2 Type=TT>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng hổ trợ</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneyReferenceQuery.ToString("n0") + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneyCommission.ToString("n0") + "</td>");
                        zSb.AppendLine(" </tr>");
                        #endregion
                        Obj.Note1 = "Hoa hồng hổ trợ:" + MoneyCommission + ";";
                    }
                    AmountCommission += MoneyCommission;
                    #endregion

                    #region [Giao dịch cá nhân trực tiếp]
                    MoneyCommission = 0;
                    if (rDirectCommission != null && rDirectCommission.Count() > 0)
                    {
                        zSb.Append(MultiBroadCommissionDirect(rDirectCommission));
                        MoneyDirectQuery = MultiBroadCommissionDirectMoney(rDirectCommission);
                        MoneyCommission = MoneyProgressive(EmployeeKey, MoneyDirectQuery);
                        #region [Footer tong truc tiếp]
                        zSb.AppendLine(" <tr style='background-color: #a5d4f4' Source=1 Type=TT>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng cá nhân</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneyDirectQuery.ToString("n0") + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneyCommission.ToString("n0") + "</td>");
                        zSb.AppendLine(" </tr>");
                        #endregion
                        Obj.Note2 = "Hoa hồng cá nhân:" + MoneyCommission + ";";
                    }
                    AmountCommission += MoneyCommission;
                    #endregion

                    #region [Hỗ trợ]
                    MoneyCommission = 0;
                    if (rSupport.Count() > 0)
                    {
                        zSb.Append(MultiBroad_Commission_Support_Html(rSupport));
                        MoneySupportTotal = MultiBroad_Income_Support_Total(rSupport);
                        MoneyCommission = MultiBroad_Commission_Support_Total(rSupport);

                        zSb.AppendLine(" <tr style='background-color: #a5d4f4' Source=1 Type=HT>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng hổ trợ</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneySupportTotal.ToString("n0") + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneyCommission.ToString("n0") + "</td>");
                        zSb.AppendLine(" </tr>");

                        Obj.Note3 = "Hoa hồng hổ trợ:" + MoneyCommission + ";";
                    }
                    AmountCommission += MoneyCommission;
                    #endregion

                    #region [Ngắn hạn]
                    MoneyCommission = 0;
                    zSb.Append(OneBroad_Commission_ShortTerm_Html(ztblShortTerm));
                    MoneyCommission = OneBroad_ShortTermMoney(ztblShortTerm);
                    AmountCommission += MoneyCommission;
                    #endregion

                    zSb.AppendLine(" <tr class=tong>");
                    zSb.AppendLine("    <td colspan=7>Tổng</td>");
                    zSb.AppendLine("    <td class='text-right giatien' id=hieuqua>" + AmountCommission.ToString("n0") + "</td>");
                    zSb.AppendLine(" </tr>");
                    #endregion
                    KQ += AmountCommission;
                }
                //tinh gộp nguồn
                else
                {
                    #region [Trích lọc thông tin]
                    DataRow[] rDirect = zTable.Select("[Type]='TT'");
                    DataRow[] rSupport = zTable.Select("[Type]='HT'");
                    #endregion

                    double MoneySupport = 0;
                    double MoneyDirectQuery = 0;
                    double MoneyCommission = 0;
                    double AmountCommission = 0;

                    #region [Trực tiếp]
                    MoneyCommission = 0;
                    zSb.Append(OneBroad_Commission_Html(rDirect));
                    MoneyDirectQuery = OneBroad_CommissionMoney(rDirect);
                    MoneyCommission = MoneyProgressive(EmployeeKey, MoneyDirectQuery);

                    #region [Footer Tổng trực tiếp]
                    zSb.AppendLine(" <tr style='background-color: #a5d4f4' Source=1 Type=TT>");
                    zSb.AppendLine("    <td colspan=6>Hoa hồng cá nhân</td>");
                    zSb.AppendLine("    <td class='text-right giatien'>" + MoneyDirectQuery.ToString("n0") + "</td>");
                    zSb.AppendLine("    <td class='text-right giatien'>" + MoneyCommission.ToString("n0") + "</td>");
                    zSb.AppendLine(" </tr>");
                    #endregion

                    Obj.Note2 = "Hoa hồng cá nhân:" + MoneyCommission + ";";
                    AmountCommission += MoneyCommission;
                    #endregion

                    #region [Hổ trợ]
                    MoneyCommission = 0;
                    if (rSupport.Count() > 0)
                    {
                        MoneySupport = OneBroad_TotalSupportMoney(rSupport);
                        MoneyCommission = OneBroad_CommissionSupportTotal(rSupport);

                        zSb.Append(OneBroad_Commission_Support_Html(rSupport));

                        zSb.AppendLine(" <tr style='background-color: #a5d4f4' Source=1 Type=HT>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng hổ trợ</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneySupport.ToString("n0") + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + MoneyCommission.ToString("n0") + "</td>");
                        zSb.AppendLine(" </tr>");

                        Obj.Note3 = "Hoa hồng hổ trợ:" + MoneyCommission + ";";
                    }
                    AmountCommission += MoneyCommission;
                    #endregion

                    #region [Ngắn hạn]
                    MoneyCommission = 0;
                    zSb.Append(OneBroad_Commission_ShortTerm_Html(ztblShortTerm));
                    MoneyCommission = OneBroad_ShortTermMoney(ztblShortTerm);
                    AmountCommission += MoneyCommission;
                    #endregion

                    zSb.AppendLine(" <tr class=tong>");
                    zSb.AppendLine("    <td colspan=7>Tổng</td>");
                    zSb.AppendLine("    <td class='text-right giatien' id=hieuqua>" + AmountCommission.ToString("n0") + "</td>");
                    zSb.AppendLine(" </tr>");

                    zSb.AppendLine(" </table>");

                    KQ += AmountCommission;
                }
            }
            #region [textbox]
            float ngaynghi = Employees_Data.OffNoMoney_Salary(EmployeeKey, FromDate);
            double luongcoban = zEmployee.BasicSalary;
            float ngaycongchuan = 26;
            float ngaythucte = ngaycongchuan - ngaynghi;
            float baohiem = zEmployee.Insurance;
            double luongngay = zEmployee.BasicSalary / 26;
            double phucap = zEmployee.SupportFee;
            double phucaptrachnhiem = zEmployee.ExtraFee;

            Obj.TableDetail = zSb.ToString();
            Obj.Param1 = luongcoban.ToString("n0");
            Obj.Param2 = ngaycongchuan.ToString();
            Obj.Param3 = ngaynghi.ToString("n1");
            Obj.Param4 = ngaythucte.ToString("n1");
            Obj.Param51 = baohiem.ToString("n0");
            Obj.Param6 = phucaptrachnhiem.ToString("n0");
            Obj.Param7 = (phucaptrachnhiem + luongcoban + phucap - baohiem).ToString("n0");
            Obj.Param8 = KQ.ToString("n0");
            Obj.Param10 = (
                sthuong.ToDouble()
                + Obj.Param7.ToDouble()
                + Obj.Param8.ToDouble()
                + Obj.Param6.ToDouble()).ToString("n0");
            Obj.Param14 = phucap.ToString();

            if (zEmployee.Insurance != 0)
            {
                Obj.Param11 = Obj.Param7.ToDouble().ToString("n0");
                Obj.Param12 = (Obj.Param10.ToDouble() - Obj.Param11.ToDouble()).ToString("n0");
            }
            else
            {
                Obj.Param11 = (
                    Obj.Param7.ToDouble() +
                    Obj.Param8.ToDouble() -
                    Obj.Param9.ToDouble()).ToString("n0");
                Obj.Param12 = "0";
            }
            #endregion            

            float CheckOff = Employees_Data.OffChecked(EmployeeKey, FromDate);
            if (CheckOff > 0)
                Obj.Message = zEmployee.LastName + " " + zEmployee.FirstName + " có " + CheckOff + " ngày phép chưa được duyệt !.";
            return Obj;
        }

        [WebMethod]
        public static string Save(string sEmployee, string sFromDate, string sToDate, string sDescription, string sTable,
            string Param1, string Param2, string Param3, string Param4, string Param5, string Param51, string Param6,
            string Param7, string Param8, string Param9, string Param10, string Param11, string Param12, string Param13,
            string Param14, string Tag, string NoteBonus)
        {
            int Employee = sEmployee.ToInt();
            DateTime FromDate = Tools.ConvertToDate(sFromDate);
            DateTime ToDate = Tools.ConvertToDate(sToDate);
            Salary_Info zInfo = new Salary_Info(Employee, FromDate.Month, FromDate.Year);
            zInfo.EmployeeKey = Employee;
            zInfo.FromDate = FromDate;
            zInfo.ToDate = ToDate;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(Employee);
            zInfo.Description = sDescription;
            zInfo.Param1 = Param1.ToDouble();
            zInfo.Param2 = Param2.ToDouble();
            zInfo.Param3 = Param3.ToDouble();
            zInfo.Param4 = Param4.ToDouble();

            float zOut5 = 0;
            float.TryParse(Param5, out zOut5);
            zInfo.Param5 = zOut5;

            zInfo.Param51 = Param51.ToDouble();
            zInfo.Param6 = Param6.ToDouble();
            zInfo.Param7 = Param7.ToDouble();
            zInfo.Param8 = Param8.ToDouble();
            zInfo.Param9 = Param9.ToDouble();
            zInfo.Param10 = Param10.ToDouble();
            zInfo.Param11 = Param11.ToDouble();
            zInfo.Param12 = Param12.ToDouble();
            zInfo.Param13 = Param13.ToDouble();
            zInfo.Param14 = Param14.ToDouble();
            zInfo.Tag = Tag.ToInt();
            zInfo.Note1 = NoteBonus;

            zInfo.Description = sDescription;
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            //là cấp thường lưu 3 cấp, hoặc là admin, là admin thì chỉ lưu 2 cấp
            //nhan viên > admin > giam doc
            //admin > giam doc
            if (_UserKey.ToInt() >= 2)
                zInfo.Step = sEmployee + ",24,1";
            else
                zInfo.Step = sEmployee + ",1";

            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save();

            if (zInfo.Message == string.Empty)
            {
                string[] row = sTable.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                if (row.Length > 0)
                {
                    string SQL = "DELETE HRM_Salary_Detail WHERE ParentKey = " + zInfo.AutoKey;
                    for (int i = 0; i < row.Count(); i++)
                    {
                        string[] col = row[i].Split(':');
                        string TradeKey = col[8];
                        string Type = col[9];
                        string Source = col[10];

                        string AssetID = col[1];
                        string Note1 = col[2];
                        string Note2 = col[3];

                        double temp1 = 0;
                        double.TryParse(col[4], out temp1);
                        string cParam1 = temp1.ToString();

                        temp1 = 0;
                        double.TryParse(col[5], out temp1);
                        string cParam2 = temp1.ToString();

                        temp1 = 0;
                        double.TryParse(col[6], out temp1);
                        string cParam3 = temp1.ToString();

                        temp1 = 0;
                        double.TryParse(col[7], out temp1);
                        string cParam4 = temp1.ToString();

                        SQL += @"
INSERT INTO HRM_Salary_Detail(
ParentKey, TradeKey, AssetID, Note1, Note2, Type, Source, Param1, Param2, Param3, Param4 ) VALUES 
(" + zInfo.AutoKey + " ," + TradeKey + ",N'" + AssetID.Trim() + "',N'" + Note1 + "' ,N'" + Note2 + "',N'" + Type + "' ,N'" + Source + "' ," + cParam1 + " ," + cParam2 + " ," + cParam3 + " ," + cParam4 + " ) ";
                    }

                    SQL = SQL.Replace("undefined", "0");
                    string Message = CustomInsert.Exe(SQL).Message;
                    if (Message == string.Empty)
                    {
                        //SendMessage(zInfo.AutoKey, zInfo.EmployeeKey, zInfo.DepartmentKey, "");
                        return zInfo.AutoKey.ToString();
                    }
                    else
                    {
                        return "ERROR " + Message;
                    }
                }

                return string.Empty;
            }
            else
            {
                return "ERROR " + zInfo.Message;
            }
        }

        [WebMethod]
        public static string SendMessage(int sAutoKey)
        {

            Salary_Info zInfo = new Salary_Info(sAutoKey);

            string Title = "Bảng lương " + zInfo.EmployeeName;

            string SQL = @"
            DELETE SYS_Message WHERE ObjectTable=N'HRM_Salary' AND ObjectKey=" + sAutoKey + " AND EmployeeKey = " + zInfo.EmployeeKey + @"
            INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey,Readed, Description) 
            VALUES (N'" + Title + "',N'" + Title + "', GETDATE(), N'HRM_Salary'," + sAutoKey + "," + zInfo.EmployeeKey + ",0,N'Tình Trạng bảng lương')";

            string Message = CustomInsert.Exe(SQL).Message;
            if (Message == string.Empty)
                return "OK";
            else
                return "ERROR";
        }

        DataRow Row_Shorterm(DataTable InTable)
        {
            DataRow RowReturn = InTable.NewRow();

            int MonthGet = 0;
            int Result = 0;
            Result = InTable.Rows.Count;
            if (Result > 0)
            {
                MonthGet = Result / 6;
                if (MonthGet >= 1)
                {
                    int Num = 0;
                    double HoaHong = _HoaHongTruongPhong * MonthGet;
                    double GiaChuaVAT = 0;
                    double DoanhThu = 0;
                    string AssetTemp = "";
                    string AssetName = "";
                    AssetTemp = InTable.Rows[0]["AssetID"].ToString().Trim();

                    foreach (DataRow r in InTable.Rows)
                    {
                        Num++;
                        if (AssetTemp != r["AssetID"].ToString().Trim())
                        {
                            AssetName = AssetTemp + "(" + Num + ")" + ", ";
                            AssetTemp = r["AssetID"].ToString().Trim();

                            Num = 0;
                        }
                        GiaChuaVAT += r["Income"].ToDouble();
                        DoanhThu += r["Income"].ToDouble();
                    }

                    
                    RowReturn["AssetID"] = AssetName.Remove(AssetName.LastIndexOf(","), 1) + " <br /> Tổng thời gian thuê " + Result + " tháng";
                    RowReturn["Type"] = "Hỗ trợ ngắn hạn";
                    RowReturn["Source"] = "1";
                    RowReturn["Income"] = GiaChuaVAT.ToString("n0");
                    RowReturn["Result"] = DoanhThu.ToString("n0");
                    RowReturn["InternalCost"] = 0;
                    RowReturn["Commission"] = HoaHong.ToString("n0");
                    InTable.Rows.Add(RowReturn);
                }
            }

            return RowReturn;
        }
        DataRow Row_Longterm(DataTable InTable)
        {
            DataRow RowReturn = InTable.NewRow();

            int MonthGet = 0;
            int Result = 0;
            Result = InTable.Rows.Count;
            if (Result > 0)
            {
                MonthGet = Result / 12;
                if (MonthGet >= 1)
                {
                    int Num = 0;
                    double HoaHong = _HoaHongTruongPhong * MonthGet;
                    double GiaChuaVAT = 0;
                    double DoanhThu = 0;
                    string AssetTemp = "";
                    string AssetName = "";
                    AssetTemp = InTable.Rows[0]["AssetID"].ToString().Trim();

                    foreach (DataRow r in InTable.Rows)
                    {
                        Num++;
                        if (AssetTemp != r["AssetID"].ToString().Trim())
                        {
                            AssetName = AssetTemp + "(" + Num + ")" + ", ";
                            AssetTemp = r["AssetID"].ToString().Trim();

                            Num = 0;
                        }
                        GiaChuaVAT += r["Income"].ToDouble();
                        DoanhThu += r["Income"].ToDouble();
                    }


                    RowReturn["AssetID"] = AssetName.Remove(AssetName.LastIndexOf(","), 1) + " <br /> Tổng thời gian thuê " + Result + " tháng";
                    RowReturn["Type"] = "Hỗ trợ ngắn hạn";
                    RowReturn["Source"] = "1";
                    RowReturn["Income"] = GiaChuaVAT.ToString("n0");
                    RowReturn["Result"] = DoanhThu.ToString("n0");
                    RowReturn["InternalCost"] = 0;
                    RowReturn["Commission"] = HoaHong.ToString("n0");
                    InTable.Rows.Add(RowReturn);
                }
            }

            return RowReturn;
        }
        
        string CommissionReference(DataRow[] ArrayRow)
        {
            int o = 1;
            StringBuilder zSb = new StringBuilder();
            if (ArrayRow != null && ArrayRow.Count() > 0)
            {
                int totalReference = ArrayRow.Count();
                for (int i = 0; i < totalReference; i++)
                {
                    DataRow r = ArrayRow[i];
                    if (i < totalReference - 1)
                    {
                        zSb.AppendLine(" <tr source=" + r["source"].ToString() + " type=" + r["type"].ToString() + ">");
                        zSb.AppendLine("    <td>" + o++ + "</td>");
                        zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                        zSb.AppendLine("    <td>" + r["Note1"].ToString() + "</td>");
                        zSb.AppendLine("    <td>" + r["Note2"].ToString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param1"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param2"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param3"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td></td>");
                        zSb.AppendLine(" </tr>");

                        //MoneyReferenceQuery += r["Param3"].ToDouble();
                    }
                    else
                    {
                        zSb.AppendLine(" <tr style='background-color: #a5d4f4'>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng hổ trợ</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + r[3].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + r[4].ToDoubleString() + "</td>");
                        zSb.AppendLine(" </tr>");
                    }
                }
            }
            return zSb.ToString();
        }
        string CommissionDirect(DataRow[] ArrayRow)
        {
            int o = 1;
            StringBuilder zSb = new StringBuilder();
            if (ArrayRow != null && ArrayRow.Count() > 0)
            {
                int totalDirect = ArrayRow.Count();
                for (int i = 0; i < ArrayRow.Count(); i++)
                {
                    DataRow r = ArrayRow[i];
                    if (i < totalDirect - 1)
                    {
                        zSb.AppendLine(" <tr source=" + r["source"].ToString() + " type=" + r["type"].ToString() + ">");
                        zSb.AppendLine("    <td>" + o++ + "</td>");
                        zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                        zSb.AppendLine("    <td>" + r["Note1"].ToString() + "</td>");
                        zSb.AppendLine("    <td>" + r["Note2"].ToString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param1"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param2"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param3"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td></td>");
                        zSb.AppendLine(" </tr>");
                    }
                    else
                    {
                        zSb.AppendLine(" <tr style='background-color: #a5d4f4'>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng cá nhân</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + r[3].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + r[4].ToDoubleString() + "</td>");
                        zSb.AppendLine(" </tr>");
                    }
                }
            }
            return zSb.ToString();
        }
        string CommissionSupport(DataRow[] ArrayRow)
        {
            StringBuilder zSb = new StringBuilder();
            if (ArrayRow != null && ArrayRow.Count() > 0)
            {
                int o = 1;
                int totalSupport = ArrayRow.Count();
                for (int i = 0; i < ArrayRow.Count(); i++)
                {
                    DataRow r = ArrayRow[i];
                    if (i < totalSupport - 1)
                    {
                        zSb.AppendLine(" <tr Type=HT Source=" + r["Source"].ToString() + ">");
                        zSb.AppendLine("    <td>" + o++ + "</td>");
                        zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                        zSb.AppendLine("    <td>" + r["Note1"].ToString() + "</td>");
                        zSb.AppendLine("    <td>" + r["Note2"].ToString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param1"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param2"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param3"].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right'>" + r["Param4"].ToDoubleString() + "</td>");
                        zSb.AppendLine(" </tr>");
                    }
                    else
                    {
                        zSb.AppendLine(" <tr style='background-color: #a5d4f4' Type=HT>");
                        zSb.AppendLine("    <td colspan=6>Hoa hồng hổ trợ</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + r[3].ToDoubleString() + "</td>");
                        zSb.AppendLine("    <td class='text-right giatien'>" + r[4].ToDoubleString() + "</td>");
                        zSb.AppendLine(" </tr>");
                    }
                }
            }
            return zSb.ToString();
        }
        string CommissionSupportShortTerm(DataRow[] ArrayRow)
        {
            StringBuilder zSb = new StringBuilder();
            if (ArrayRow != null && ArrayRow.Count() > 0)
            {
                int o = 1;
                for (int i = 0; i < ArrayRow.Count(); i++)
                {
                    DataRow r = ArrayRow[i];

                    zSb.AppendLine(" <tr Type=NH Source=" + r["Source"].ToString() + ">");
                    zSb.AppendLine("    <td>" + o++ + "</td>");
                    zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("    <td>" + r["Note1"].ToString() + "</td>");
                    zSb.AppendLine("    <td>" + r["Note2"].ToString() + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + r["Param1"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + r["Param2"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + r["Param3"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + r["Param4"].ToDoubleString() + "</td>");
                    zSb.AppendLine(" </tr>");
                }
            }

            return zSb.ToString();
        }

        double MoneyCommissionDirect(DataRow[] ArrayRow)
        {
            if (ArrayRow != null && ArrayRow.Length > 0)
                return ArrayRow[ArrayRow.Length - 1][4].ToDouble();
            else
                return 0;
        }
        double MoneyCommissionReference(DataRow[] ArrayRow)
        {
            if (ArrayRow != null && ArrayRow.Length > 0)
                return ArrayRow[ArrayRow.Length - 1][4].ToDouble();
            else
                return 0;
        }
        double MoneyCommissionSupport(DataRow[] ArrayRow)
        {
            if (ArrayRow != null && ArrayRow.Length > 0)
                return ArrayRow[ArrayRow.Length - 1][4].ToDouble();
            else
                return 0;
        }
        double MoneyCommissionSupportShortTerm(DataRow[] ArrayRow)
        {
            if (ArrayRow != null && ArrayRow.Length > 0)
                return ArrayRow[ArrayRow.Length - 1]["Param4"].ToDouble();
            else
                return 0;
        }

        //xử lý bang lũy tiến trực tiếp
        static double MoneyProgressive(int EmployeeKey, double MoneyQuery)
        {
            double MoneyCommission = 0;
            DataTable ztblBoard = Progressive_Board_Data.List(EmployeeKey, 1);
            foreach (DataRow r in ztblBoard.Rows)
            {
                double FromMoney = r["FromMoney"].ToDouble();
                double ToMoney = r["ToMoney"].ToDouble();
                double StepMoney = ToMoney - FromMoney;
                double Commission = 0;
                int Rate = r["Rate"].ToInt();

                if (MoneyQuery >= StepMoney)
                {
                    Commission = (StepMoney * Rate) / 100;
                    MoneyQuery = MoneyQuery - StepMoney;
                    MoneyCommission += Commission;
                }
                else
                {
                    Commission = (MoneyQuery * Rate) / 100;
                    MoneyCommission += Commission;
                    break;
                }
            }

            return MoneyCommission;
        }

        //xử lý bang luy tien lien kết
        static double MoneyProgressiveReference(int EmployeeKey, double MoneyQuery)
        {
            double Result = (Progressive_Board_Data.List(EmployeeKey, 2).Rows[0]["Rate"].ToInt() * MoneyQuery) / 100;
            return Result;
        }

        //xử lý hoa hồng trực tiếp
        static string MultiBroadCommissionDirect(DataRow[] ArrayRow)
        {
            int i = 1;
            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();

                zSb.AppendLine(" <tr tradekey=" + r["TransactionKey"] + " Source=1 Type=TT>");
                zSb.AppendLine("    <td>" + i++ + "</td>");
                zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("    <td>Cá nhân</td>");
                zSb.AppendLine("    <td>Trực tiếp</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["InternalCost"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + Kq.ToString("n0") + "</td>");
                zSb.AppendLine("    <td></td>");
                zSb.AppendLine(" </tr>");
            }
            return zSb.ToString();
        }
        static double MultiBroadCommissionDirectMoney(DataRow[] ArrayRow)
        {
            double MoneyDirectQuery = 0;
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();
                MoneyDirectQuery += Kq;
            }
            return MoneyDirectQuery;
        }

        //xủ lý hoa hồng liên kết
        static string MultiBroadCommissionReference(DataRow[] ArrayRow)
        {
            int i = 1;
            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();

                zSb.AppendLine(" <tr tradekey=" + r["TransactionKey"] + " Source=2 Type=TT>");
                zSb.AppendLine("    <td>" + i++ + "</td>");
                zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("    <td>Cá nhân</td>");
                zSb.AppendLine("    <td>Liên kết</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["InternalCost"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + Kq.ToString("n0") + "</td>");
                zSb.AppendLine("    <td></td>");
                zSb.AppendLine(" </tr>");
            }
            return zSb.ToString();
        }
        static double MultiBroadCommissionReferenceMoney(DataRow[] ArrayRow)
        {
            double Result = 0;
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();
                Result += Kq;
            }
            return Result;
        }

        //xử lý hoa hồng hổ trợ
        static string MultiBroad_Commission_Support_Html(DataRow[] ArrayRow)
        {
            int i = 1;
            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();

                zSb.AppendLine(" <tr tradekey=" + r["TransactionKey"] + " Type=HT Source=" + r["Source"].ToString() + ">");
                zSb.AppendLine("    <td>" + i++ + "</td>");
                zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("    <td>" + r["CommissionName"].ToString() + "</td>");
                if (r["Source"].ToInt() == 1)
                {
                    zSb.AppendLine("    <td>Trực tiếp</td>");
                }
                else
                {
                    zSb.AppendLine("    <td>Liên kết</td>");
                }
                zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["InternalCost"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + Kq.ToString("n0") + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["Money"].ToDoubleString() + "</td>");
                zSb.AppendLine(" </tr>");
            }
            return zSb.ToString();
        }
        //tong hoa hong
        static double MultiBroad_Income_Support_Total(DataRow[] ArrayRow)
        {
            double MoneySupport = 0;
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();
                MoneySupport += Kq;
            }
            return MoneySupport;
        }
        //tong doanh thu
        static double MultiBroad_Commission_Support_Total(DataRow[] ArrayRow)
        {
            double MoneyCommission = 0;
            foreach (DataRow r in ArrayRow)
            {
                MoneyCommission += r["Money"].ToDouble();
            }
            return MoneyCommission;
        }

        //xử lý hoa hồng hổ trợ dài hạn
        static string OneBroad_Commission_Support_Html(DataRow[] ArrayRow)
        {
            int i = 1;
            StringBuilder zSb = new StringBuilder();
            if (ArrayRow.Count() > 0)
            {
                foreach (DataRow r in ArrayRow)
                {
                    double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();

                    zSb.AppendLine(" <tr tradekey=" + r["TransactionKey"] + " Type=HT Source=" + r["Source"].ToString() + ">");
                    zSb.AppendLine("    <td>" + i++ + "</td>");
                    zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("    <td>Hỗ trợ</td>");
                    if (r["Source"].ToInt() == 1)
                        zSb.AppendLine("    <td>Trực tiếp</td>");
                    else
                        zSb.AppendLine("    <td>Liên kết</td>");

                    zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + r["InternalCost"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + Kq.ToString("n0") + "</td>");
                    zSb.AppendLine("    <td class='text-right'>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine(" </tr>");
                }
            }

            return zSb.ToString();
        }
        //tong doanh thu
        static double OneBroad_TotalSupportMoney(DataRow[] ArrayRow)
        {
            double MoneySupport = 0;
            if (ArrayRow.Count() > 0)
            {
                foreach (DataRow r in ArrayRow)
                {
                    double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();
                    MoneySupport += Kq;
                }
            }
            return MoneySupport;
        }
        //tong hoa hong
        static double OneBroad_CommissionSupportTotal(DataRow[] ArrayRow)
        {
            double MoneyCommission = 0;
            if (ArrayRow.Count() > 0)
            {
                foreach (DataRow r in ArrayRow)
                {
                    MoneyCommission += r["Money"].ToDouble();
                }
            }
            return MoneyCommission;
        }

        //xử lý hoa hồng hổ trợ ngắn hạn
        static string OneBroad_Commission_ShortTerm_Html(DataTable InTable)
        {
            int i = 1;
            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in InTable.Rows)
            {
                zSb.AppendLine(" <tr Type=NH Source=" + r["Source"].ToString() + ">");
                zSb.AppendLine("    <td>" + i++ + "</td>");
                zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("    <td>" + r["Type"].ToString() + "</td>");
                if (r["Source"].ToInt() == 1)
                {
                    zSb.AppendLine("    <td>Trực tiếp</td>");
                }
                else
                {
                    zSb.AppendLine("    <td>Liên kết</td>");
                }
                zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["InternalCost"].ToString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["Commission"].ToDoubleString() + "</td>");
                zSb.AppendLine(" </tr>");
            }
            return zSb.ToString();
        }
        static double OneBroad_ShortTermMoney(DataTable InTable)
        {
            double Result = 0;
            foreach (DataRow r in InTable.Rows)
            {
                Result += r["Commission"].ToDouble();
            }
            return Result;
        }

        //xử lý hoa hồng trực tiếp
        static string OneBroad_Commission_Html(DataRow[] ArrayRow)
        {
            int i = 1;
            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();

                zSb.AppendLine(" <tr tradekey=" + r["TransactionKey"] + " Source=1 Type=TT>");
                zSb.AppendLine("    <td>" + i++ + "</td>");
                zSb.AppendLine("    <td class='td10'>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("    <td>Cá nhân</td>");
                zSb.AppendLine("    <td>Trực tiếp</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["Income"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + r["InternalCost"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td class='text-right'>" + Kq.ToString("n0") + "</td>");
                zSb.AppendLine("    <td></td>");
                zSb.AppendLine(" </tr>");
            }

            return zSb.ToString();
        }
        static double OneBroad_CommissionMoney(DataRow[] ArrayRow)
        {
            double Result = 0;
            foreach (DataRow r in ArrayRow)
            {
                double Kq = r["Income"].ToDouble() - r["InternalCost"].ToDouble();
                Result += Kq;
            }
            return Result;
        }

        bool CheckRoles()
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            string RolePage = "ACC01";
            string[] result = User_Data.RolesCheck(_UserKey, RolePage).Split(',');
            if (result[3].ToInt() == 1)
            {
                LitButton.Text = @" 
<button type='button' class='btn btn-white btn-info btn-bold' id='btnSave'><i class='ace-icon fa fa-save blue'></i>Lưu</button>
<button type='button' class='btn btn-white btn-info btn-bold' id='btnSend'><i class='ace-icon fa fa-send blue'></i>Gửi</button>";
                LitScript.Text = @"
<script>$(document).ready(function () { 
$('#header').show(); 
$('#body').show(); 
$('#tinhtoanluong').show(); 
$('#thongtin').show(); });
</script>";
                return true;
            }
            else
            {
                LitScript.Text = @"
<script>$(document).ready(function () { 
$('#header').hide(); 
$('#body').hide(); 
$('#tinhtoanluong').hide(); 
$('#thongtin').hide(); });
</script>";
                return false;
            }
        }
    }
}