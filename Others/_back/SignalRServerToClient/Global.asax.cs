﻿using Lib.Config;
using System;
using System.Data.SqlClient;
using System.Web.Routing;

namespace WebApp
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            JobScheduler.Start();
            SqlDependency.Start(ConnectDataBase.ConnectionString);
        }
    }
}