﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace WebApp
{
    public class NotificationsHub : Hub, IDisposable
    {
        //public void NotifyAllClients(string msg)
        //{
        //    IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
        //    context.Clients.All.displayNotification(msg);
        //}
        public void NotifyAllClients(string Name, string Time, string Msg)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
            context.Clients.All.displayNotification(Name, Time, Msg);
        }
    }
}