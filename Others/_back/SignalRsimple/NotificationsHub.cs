﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;

namespace WebApp
{
    public class NotificationsHub : Hub
    {
        //string _Group = "";
        //public void Join(string groupName)
        //{
        //    _Group = groupName;
        //    IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
        //    context.Groups.Add(Context.ConnectionId, groupName);
        //}
        //public void Send(string name, string time, string message)
        //{
        //    IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
        //    context.Clients.Group(_Group).addNewMessageToPage(name, time, message);
        //}
        public void Send(string name, string time, string message)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
            context.Clients.All.addNewMessageToPage(name, time, message);
        }
    }
}