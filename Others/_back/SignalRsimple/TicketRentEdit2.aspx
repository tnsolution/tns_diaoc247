﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true"
    CodeBehind="TicketRentEdit2.aspx.cs" Inherits="WebApp.FNC.TicketRentEdit2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <link rel="stylesheet" href="../TicketCSS.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="row">
            <div class="col-lg-9" id="printable">
                <asp:Literal ID="LitMessage" runat="server"></asp:Literal>
                <div class="row" id="noidung">
                    <table class="table">
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <img src="http://diaoc247.vn/wp-content/themes/central/images/logo.png" height="60" /></td>
                            <td class="center">
                                <b>CÔNG TY CỔ PHẦN ĐẦU TƯ ĐỊA ỐC 247</b>
                            </td>

                            <td class="center">
                                <b>PHIẾU BÁN HÀNG</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM</b>
                            </td>
                            <td class="center">
                                <b>Số:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>Điện thoại:</b> (028) 6685 1818 &nbsp;&nbsp;&nbsp;&nbsp; <b>Website</b>  : www.diaoc247.vn
                            </td>
                            <td class="center">
                                <b>Ngày lập:<asp:Label ID="lblNgayLap" runat="server" Text=".."></asp:Label></b>
                            </td>
                        </tr>
                    </table>
                    <table class="table">
                        <tr>
                            <th class="td15">Dự án:</th>
                            <td>
                                <asp:DropDownList ID="DDLDuAn" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th class="td15">Giao dịch:</th>
                            <td>Cho thuê</td>
                            <th class="td15">Ngày cọc:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCoc" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLPhong" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Trưởng phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLTruongPhong" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Nhân viên:</th>
                            <td class="td200">
                                <asp:DropDownList ID="DDLNhanVien" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Bên A</b>
                                <small>(Cho thuê)</small>:</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtHotenA" runat="server" CssClass="form-control" placeholder="Họ tên bên chuyển nhượng" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày sinh:</th>
                            <td>
                                <asp:TextBox ID="txtNgaySinhA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td>
                                <asp:TextBox ID="txtCMNDA" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Nơi cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNoiCapA" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiLienHeA" runat="server" CssClass="form-control" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <th>Điện thoại:</th>
                            <td>
                                <asp:TextBox ID="txtDienThoaiA" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ thường trú:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiThuongTruA" runat="server" CssClass="form-control" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <th>Email:</th>
                            <td>
                                <asp:TextBox ID="txtEmailA" runat="server" CssClass="form-control noneed" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div class="hr hr-dotted hr1"></div>
                            </th>
                        </tr>
                        <tr>
                            <td><b>Bên B</b><small>(Bên thuê)</small>:</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtHotenB" runat="server" CssClass="form-control" placeholder="Họ tên bên nhận chuyển nhượng" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày sinh:</th>
                            <td>
                                <asp:TextBox ID="txtNgaySinhB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td>
                                <asp:TextBox ID="txtCMNDB" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Nơi cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNoiCapB" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiLienHeB" runat="server" CssClass="form-control" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                            <th>Điện thoại:</th>
                            <td>
                                <asp:TextBox ID="txtDienThoaiB" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Công ty:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtCongTy" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Email:</th>
                            <td>
                                <asp:TextBox ID="txtEmailB" runat="server" CssClass="form-control noneed" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Mã số thuế:</th>
                            <td>
                                <asp:TextBox ID="txtMaSoThue" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Người đại điện</th>
                            <td>
                                <asp:TextBox ID="txtDaiDien" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Chức vụ</th>
                            <td>
                                <asp:TextBox ID="txtChucVu" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </th>
                        </tr>
                        <tr>
                            <th>Mã căn:</th>
                            <td>
                                <asp:TextBox ID="txtMaCan" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>DT tim tường:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichTimTuong" runat="server" placeholder="Nhập số" require="true" Style="width: 50px"></asp:TextBox>m2
                            </td>
                            <th>DT thông thủy:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichThongThuy" runat="server" placeholder="Nhập số" require="true" Style="width: 50px"></asp:TextBox>m2
                            </td>
                        </tr>
                        <tr>
                            <th>Loại hình:</th>
                            <td>
                                <asp:DropDownList ID="DDLLoaiCanHo" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Giá cho thuê:</th>
                            <td>
                                <asp:TextBox ID="txtGiaThue" runat="server" placeholder="Nhập số" moneyinput require="true" Style="width: 70px"></asp:TextBox>
                                <asp:DropDownList ID="DDLDonVi" runat="server" Style="width: 50px; border: 0px;">
                                    <asp:ListItem Value="1" Text="VNĐ"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="USD"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="2"><b>Bằng chữ: </b>
                                <asp:Label ID="giathuebangchu" runat="server" Text="..."></asp:Label></td>
                        </tr>
                        <tr>
                            <th>Bao gồm:</th>
                            <td colspan="5">
                                <asp:TextBox ID="txtBaoGom" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <b>Phương thức thanh toán:</b> Bằng tiền mặt hoặc chuyển khoản qua tài khoản ngân hàng theo lịch thanh toán như sau
                                    <span class="pull-right noprint">
                                        <button class="btn btn-minier btn-white" type="button" id="AddRow"><i class="ace-icon fa fa-plus-circle blue"></i>Thêm dòng</button>
                                    </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <b>Chủ tài khoản:</b>
                                <asp:TextBox ID="txtChuTaiKhoan" runat="server" CssClass="" placeholder="Nhập text" require="true"></asp:TextBox>
                                <b>STK</b>
                                <asp:TextBox ID="txtSoTaiKhoan" runat="server" CssClass="" placeholder="Số tài khoản" require="true"></asp:TextBox>
                                <b>NH:</b><asp:TextBox ID="txtNganHang" runat="server" CssClass="" placeholder="Ngân hàng" require="true"></asp:TextBox>
                                <b>CN</b><asp:TextBox ID="txtChiNhanh" runat="server" CssClass="" placeholder="Chi nhánh" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:Literal ID="LitBangThanhToan" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <th>Các đợt tiếp theo:</th>
                            <td colspan="5">Bên B thanh toán bên A số tiền &nbsp;<asp:TextBox ID="txtSoTienThanhToan" runat="server" Style="width: 70px" placeholder="Nhập số" moneyinput></asp:TextBox>
                                <asp:Label ID="lblDonvi" runat="server" Text="VNĐ"></asp:Label>
                                , thanh toán
                                <asp:TextBox ID="txtLan" runat="server" Style="width: 15px" placeholder="1,2 lần" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                &nbsp;lần,
                            từ ngày&nbsp;<asp:TextBox ID="txtTuNgay" runat="server" Style="width: 15px" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                đến ngày&nbsp;<asp:TextBox ID="txtDenNgay" runat="server" Style="width: 15px" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                đầu mỗi
                            <asp:TextBox ID="txtMoiThang" runat="server" Style="width: 15px" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                tháng</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">Thời điểm giao nhận, cho thuê</th>
                        </tr>
                        <tr>
                            <td colspan="6"><b>Thời gian thuê:</b> từ ngày
                                <asp:TextBox ID="txtThueTuNgay" runat="server" placeholder="Nhập text" CssClass="" require="true" Style="width: 70px"></asp:TextBox>
                                đến ngày  
                                <asp:TextBox ID="txtThueDengnay" runat="server" placeholder="Nhập text" CssClass="" require="true" Style="width: 70px"></asp:TextBox><b>Thời gian bàn giao:</b>
                                <asp:TextBox ID="txtThoiGianBanGiao" runat="server" placeholder="Chọn" CssClass="" require="true" Style="width: 70px"></asp:TextBox><b>Thời gian tính tiền thuê:</b>
                                <asp:TextBox ID="txtThoiGianTinhTienThue" runat="server" placeholder="Chọn" CssClass="" require="true" Style="width: 70px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Thỏa thuận khác:</th>
                            <td colspan="5">
                                <asp:TextBox ID="txtThoaThuanKhac" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-3 noprint" id="right">
                <div class="row">
                    <div class="col-md-9">
                        <asp:DropDownList ID="DDLDaiDienC" runat="server" CssClass="select2" require="true" AppendDataBoundItems="true">
                            <asp:ListItem Value="--0--" Text="Chọn đại diện bên C" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-white dropdown-toggle">
                                Xử lý<i class="ace-icon fa fa-angle-down icon-on-right"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a id="btnSave" href="#"><i class="ace-icon fa fa-floppy-o"></i>&nbsp;Lưu</a>
                                </li>
                                <li style="display: none">
                                    <a id="btnSendApprove" href="#"><i class="ace-icon fa fa-send-o"></i>&nbsp;Gửi Duyệt</a>
                                </li>
                                <li>
                                    <a id="btnIn" href="javascript:window.print()"><i class="ace-icon fa fa-print"></i>&nbsp;In</a>
                                </li>
                                <li>
                                    <a id="btnSendTrade" href="#"><i class="ace-icon fa fa-send"></i>&nbsp;Lập giao dịch</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Thông tin</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <asp:Literal ID="LitInfo" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Xử lý file <small>Chọn file cần xuất thông tin</small></h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="DDLFile" runat="server" CssClass="select2" require="true">
                                                    <asp:ListItem Value="001MauHopDongChoThue.docx" Text="Hợp đồng cho thuê"></asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td class="text-right"><a id="btndownload" href="#"><i class="ace-icon fa fa-download"></i>&nbsp;Xuất file</a></td>
                                        </tr>
                                    </table>
                                    <div class="space-2"></div>
                                    <asp:Literal ID="Lit_ListFolder" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Lịch sử xử lý</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <asp:Literal ID="LitStatus" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Thảo luận</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="discussion" class="ace-scroll" style="max-height: 350px; overflow-y: scroll">
                                        <asp:Literal ID="Literal_Chat" runat="server"></asp:Literal>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="input-group">
                                        <input type="text" name="message" placeholder="Type Message ..." class="form-control" id="txt_Message" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-warning btn-sm" id="buttonSend" type="button">Send</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_LoaiCan" runat="server" Value="loaihinh" />
    <asp:HiddenField ID="HID_BangThanhToan" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TienThanhToan" runat="server" Value="0" />
    <asp:Button ID="btnTriggerSave" runat="server" Text="Save" OnClick="btnTriggerSave_Click" Style="display: none; visibility: hidden" />
    <asp:Button ID="btnWord" runat="server" Text="Save" OnClick="btnWord_Click" Style="display: none; visibility: hidden" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/numeric-input-example.js"></script>
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="/SoSangChu.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script type="text/javascript" src="TicketRentEdit2.js"></script>
    <script type="text/javascript" src='<%=ResolveClientUrl("~/Scripts/jquery.signalR-2.1.2.min.js") %>'></script>
    <script type="text/javascript" src='<%=ResolveClientUrl("~/signalr/hubs") %>'></script>
    <script type="text/javascript" src='https://cdn.rawgit.com/admsev/jquery-play-sound/master/jquery.playSound.js'></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script type="text/javascript">
        $(function () {
            var notify = $.connection.notificationsHub;
            notify.client.addNewMessageToPage = function (name, time, msg) {
                var html = "<div class='direct-chat-msg well well-sm'>";
                html += "<div class='direct-chat-info clearfix'><span class='direct-chat-name pull-left'>" + name + "</span><span class='direct-chat-timestamp pull-right'>" + time + "</span></div>";
                html += "<div class='direct-chat-text'>" + msg + "</div>";
                html += "</div>";
                $("#discussion").append(html);
                $.playSound("<%=ResolveClientUrl("~/upload/mp3/Timer-Bell.mp3") %>")
            };

            $("#buttonSend").click(function () {
                $.ajax({
                    url: '/FNC/TicketRentEdit2.aspx/SaveChat',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        "Key": $("[id$=HID_TicketKey]").val(),
                        "Message": $("#txt_Message").val(),
                    }),
                    beforeSend: function (xhr) {
                        console.log(xhr);
                    },
                    success: function (response) {
                        console.log("success");
                        console.log(response.result);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log("error");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    },
                    complete: function (response) {
                        console.log("complete");
                        console.log(response.result);
                    }
                });

                var dt = new Date();
                var time = dt.getHours() + ":" + dt.getMinutes();

                var name = getName();
                if (name == "")
                    name = "System Administrator";
                var time = time;
                var msg = $("#txt_Message").val();
                notify.server.send(name, time, msg);
                $("#txt_Message").val("").focus();

                $("#discussion").animate({ scrollTop: $('#discussion').prop("scrollHeight") }, 1000);
            });

            $.connection.hub.start().done(function () {                
                console.log("connection started");
                //notify.server.Join(5);
            }).fail(function (e) {
                alert(e);
            });
            
            //$.connection.hub.start();
        });
        function getName() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[4].split('=');
                    return cookies[1];
                }
            }
        }
    </script>
</asp:Content>
