﻿using Lib.Config;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.CRM
{
    public class Care_Data
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerKey"></param>
        /// <param name="RecordKey">mã key quan tam</param>
        /// <returns></returns>
        public static DataTable List(int CustomerKey, int ConsentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP 8 * FROM CRM_Customer_Care where CustomerKey = @CustomerKey AND ConsentKey = @ConsentKey ORDER BY CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                zCommand.Parameters.Add("@ConsentKey", SqlDbType.Int).Value = ConsentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int CustomerKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP 8 * FROM CRM_Customer_Care where CustomerKey = @CustomerKey ORDER BY CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
