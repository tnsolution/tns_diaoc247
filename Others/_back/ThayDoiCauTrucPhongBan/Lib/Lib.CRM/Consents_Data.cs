﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;
namespace Lib.CRM
{
    public class Consents_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM CRM_Customer_Consents ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static List<ItemConsent> List(int CustomerKey)
        {
            List<ItemConsent> zList = new List<ItemConsent>();
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT 
A.RecordKey, dbo.FNC_SysCategoryName(A.CategoryNeed) AS Want,
Note, A.Price, A.CreatedDate,
A.CategoryAsset AS CategoryKey, 
A.Name_Asset AS Category, 
A.CategoryInside AS FurnitureKey,
A.Name_Inside AS Furniture,
A.ConsentAera AS Aera,
A.ConsentBedRoom AS Room,
A.Project AS ProjectKey,
A.Name_Project AS Project
FROM CRM_Customer_Consents A
WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                zList = zTable.DataTableToList<ItemConsent>();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zList;
        }
    }
}
