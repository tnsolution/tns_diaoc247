﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;

namespace Lib.CRM
{
    public class Customer_Data
    {
        public static DataTable ListCustomer(string Inside, string Project, string PriceRent)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetData(@"
SELECT A.CategoryInside, A.CategoryNeed, A.CustomerKey,B.CustomerName, B.Phone1,dbo.FNC_SysCategoryName(A.CategoryNeed) 
FROM CRM_Customer_Consents A
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey 
WHERE A.CategoryInside LIKE '%"+ Inside + "%' AND A.Project LIKE '%"+ Project + "%' AND Price <= '"+ PriceRent + "'");
        }

        public static int CheckReminder(int CustomerKey)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetObject("SELECT ID FROM SYS_Notification WHERE ObjectTableName = 'CRM_Customer' AND ObjectTable ='" + CustomerKey + "' AND IsRead =0").ToInt();
        }

        //searching ver 4
        /// <summary>
        /// tìm thông tin quan tâm
        /// </summary>
        /// <param name="Project">dự án</param>
        /// <param name="Category">loại sản phẩm</param>
        /// <returns></returns>
        public static List<ItemConsent> SearchWant(string Project, string Category)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.Price, 
A.CategoryAsset AS CategoryKey, 
A.Name_Asset AS Category,
A.Project AS ProjectKey, 
A.Name_Project AS Project,
A.ConsentBedRoom AS Room,
A.ConsentAera AS Area
FROM CRM_Customer_Consents A
WHERE 1 = 1";

            if (Project != string.Empty)
                zSQL += " AND Project LIKE @Project";
            if (Category != string.Empty && Category != "0")
                zSQL += " AND CategoryAsset LIKE @Category";

            DataTable zTable = new DataTable();
            List<ItemConsent> zList = new List<ItemConsent>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Project", SqlDbType.NVarChar).Value = "%" + Project + "%";
                zCommand.Parameters.Add("@Category", SqlDbType.NVarChar).Value = "%" + Category + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemConsent>();
            return zList;
        }
        /// <summary>
        /// tìm thông tin giao dịch
        /// </summary>
        /// <param name="Project">dự án</param>
        /// <param name="Category">loại san phẩm</param>
        /// <param name="TradeType">loại giao dịch, cho thuê, chuyển nhượng, bán mới chuỗi đầu vào ví dụ: 227,228,230 </param>
        /// <param name="IsOwner">chu nha, khach thue</param>
        /// <returns></returns>
        public static List<ItemTrade> SearchTrade(string Project, string Category, string TradeType, int IsOwner)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.IsOwner, B.ProjectKey, B.AssetCategory, B.TransactionCategory,
CASE A.IsOwner WHEN 1 THEN N'Chủ nhà' WHEN 2 THEN N'Khách thuê nhà' WHEN 3 THEN N'Khách mua' END AS [Owner]
FROM FNC_Transaction_Customer A
LEFT JOIN FNC_Transaction B ON A.TransactionKey = B.TransactionKey
WHERE 1 = 1";
            if (IsOwner != 0)
                zSQL += " AND A.IsOwner = @IsOwner";
            if (Project != string.Empty)
                zSQL += " AND B.ProjectKey LIKE @Project";
            if (Category != string.Empty)
                zSQL += " AND B.AssetCategory LIKE @Category";
            if (TradeType != string.Empty)
                zSQL += " AND B.TransactionCategory IN (" + TradeType + ")";

            DataTable zTable = new DataTable();
            List<ItemTrade> zList = new List<ItemTrade>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@IsOwner", SqlDbType.Int).Value = IsOwner;
                zCommand.Parameters.Add("@Project", SqlDbType.NVarChar).Value = "%" + Project + "%";
                zCommand.Parameters.Add("@Category", SqlDbType.NVarChar).Value = "%" + Category + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemTrade>();
            return zList;
        }
        /// <summary>
        /// tim thông tin khách hàng
        /// </summary>
        /// <param name="Department">phòng</param>
        /// <param name="Employee">nhân viên</param>
        /// <param name="Name">tên</param>
        /// <param name="Phone">sđt</param>
        /// <param name="Status">muc do quan tam khach vi du: 1,2,3</param>
        /// <param name="ListKey">mảng key khách ví dụ: 1,2,3,4,5</param>
        /// <returns></returns>
        public static List<ItemCustomer> Search(int Department, int Employee, string Name, string Phone, string Status, string ListKey)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.[Status] AS StatusKey, A.HasTrade, 
A.CustomerName, A.Phone1, A.Phone2, A.Email2, A.Email1, A.CategoryKey,
dbo.FNC_SysCategoryName(A.[Status]) AS [Status], A.ModifiedDate,
C.Room, C.CategoryName, C.ProjectName
FROM CRM_Customer A 
LEFT JOIN SYS_Categories B ON A.[Status] = B.AutoKey 
OUTER APPLY dbo.[GetWant](A.CustomerKey) C
WHERE 1 = 1";
            if (Status != string.Empty)
                zSQL += " AND A.Status IN (" + Status + ")";
            if (Name != string.Empty)
                zSQL += " AND A.CustomerName LIKE @Name";
            if (Phone != string.Empty)
                zSQL += " AND (A.Phone1 LIKE @Phone OR A.Phone2 LIKE @Phone)";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @Employee";
            if (ListKey != string.Empty)
                zSQL += " AND A.CustomerKey IN (" + ListKey + ")";
            zSQL += " ORDER BY B.[Rank]";
            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }

        public static List<ItemCustomer> Search(int Department, int Employee, string Name, string Phone, string Status, string ListKey, string Project, string Category)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.[Status] AS StatusKey, A.HasTrade, 
A.CustomerName, A.Phone1, A.Phone2, A.Email2, A.Email1, A.CategoryKey,
dbo.FNC_SysCategoryName(A.[Status]) AS [Status], A.ModifiedDate,
C.Room, C.CategoryName, C.ProjectName
FROM CRM_Customer A 
LEFT JOIN SYS_Categories B ON A.[Status] = B.AutoKey 
OUTER APPLY dbo.[GetWant](A.CustomerKey) C
WHERE 1 = 1";
            if (Category != string.Empty)
                zSQL += " AND C.CategoryName LIKE @CategoryName";
            if (Project != string.Empty)
                zSQL += " AND C.ProjectName LIKE @ProjectName";
            if (Status != string.Empty)
                zSQL += " AND A.Status IN (" + Status + ")";
            if (Name != string.Empty)
                zSQL += " AND A.CustomerName LIKE @Name";
            if (Phone != string.Empty)
                zSQL += " AND (A.Phone1 LIKE @Phone OR A.Phone2 LIKE @Phone)";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @Employee";
            if (ListKey != string.Empty)
                zSQL += " AND A.CustomerKey IN (" + ListKey + ")";
            zSQL += " ORDER BY B.[Rank]";
            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + Project + "%";
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = "%" + Category + "%";
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Department"></param>
        /// <param name="Employee"></param>
        /// <param name="Name"></param>
        /// <param name="Phone"></param>
        /// <param name="Status"></param>
        /// <param name="ListKey"></param>
        /// <param name="Amount">50</param>
        /// <returns></returns>
        public static List<ItemCustomer> Search(int Department, int Employee, string Name, string Phone, string Status, string ListKey, int Amount)
        {
            string getTop = "";
            if (Amount != 0)
                getTop = "TOP " + Amount;
            string zSQL = @"
SELECT " + getTop + @" A.CustomerKey, A.[Status] AS StatusKey, A.HasTrade, 
A.CustomerName, A.Phone1, A.Phone2, A.Email2, A.Email1, A.CategoryKey,
dbo.FNC_SysCategoryName(A.[Status]) AS [Status], A.ModifiedDate,
C.Room, C.CategoryName, C.ProjectName
FROM CRM_Customer A 
LEFT JOIN SYS_Categories B ON A.[Status] = B.AutoKey 
OUTER APPLY dbo.[GetWant](A.CustomerKey) C
WHERE 1 = 1";

            if (Status != string.Empty)
                zSQL += " AND A.Status IN (" + Status + ")";
            if (Name != string.Empty)
                zSQL += " AND A.CustomerName LIKE @Name";
            if (Phone != string.Empty)
                zSQL += " AND (A.Phone1 LIKE @Phone OR A.Phone2 LIKE @Phone)";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @Employee";
            if (ListKey != string.Empty)
                zSQL += " AND A.CustomerKey IN (" + ListKey + ")";
            zSQL += " ORDER BY B.[Rank]";
            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }





        public static List<ItemCustomer> Get()
        {
            string zSQL = @"
SELECT A.CustomerKey, A.CustomerName, 
B.Project AS ProjectKey, 
B.Name_Project AS ProjectName,
NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, 
B.CategoryConsent AS ConsentKey, 
C.Product AS Consent, 
B.[CategoryAsset] AS CategoryAssetKey, C.[RANK],
A.CreatedName, A.CreatedDate, A.ModifiedDate, A.ModifiedName
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent 
WHERE 1 = 1";

            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }

        public static List<ItemCustomer> Get(int Department, int Employee)
        {
            string zSQL = @"
SELECT A.CategoryKey, A.CustomerKey, A.CustomerName, 
B.Project AS ProjectKey, 
B.Name_Project AS ProjectName,
NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, 
B.CategoryConsent AS ConsentKey, 
C.Product AS Consent, 
B.[CategoryAsset] AS CategoryAssetKey, C.[RANK],
A.CreatedName, A.CreatedDate, A.ModifiedDate, A.ModifiedName
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent 
WHERE 1 = 1";

            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @Employee";
            zSQL += " ORDER BY A.CreatedDate DESC";
            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }

        //tim khach quan tam va tat ca
        public static List<ItemCustomer> Get(string Type, string Project, string Asset, string Name, string Phone, string Status, int Department, int Employee)
        {
            string zSQL = @"
SELECT A.CustomerKey, A.CategoryKey, A.CustomerName, 
B.Project AS ProjectKey, 
B.Name_Project AS ProjectName,
NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, 
B.CategoryConsent AS ConsentKey, 
C.Product AS Consent, 
B.[CategoryAsset] AS CategoryAssetKey, C.[RANK],
A.CreatedName, A.CreatedDate, A.ModifiedDate, A.ModifiedName
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent 
WHERE 1 = 1";

            switch (Type.ToInt())
            {
                //khach quan tam
                case 1:
                    zSQL += " AND B.CategoryConsent IN (" + Status.Remove(Status.LastIndexOf(","), 1) + ") ";
                    break;

                // tat ca khach
                default:
                    break;
            }
            if (Name != string.Empty)
                zSQL += " AND A.CustomerName LIKE @Name";
            if (Phone != string.Empty)
                zSQL += " AND (A.Phone1 LIKE @Phone OR A.Phone2 LIKE @Phone)";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @Employee";
            zSQL += " ORDER BY A.CreatedDate DESC";
            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }
        //tim khach da co giao dich
        public static List<ItemCustomer> Get_Trade(string Project, string Asset, string Name, string Phone, string Type, int Department, int Employee)
        {
            string zSQL = @"
SELECT C.CustomerKey, C.CategoryKey, C.CustomerName, 
C.Phone1, C.Phone2, C.Email1, C.Email2, C.Address1, C.Birthday,
C.ModifiedDate, C.ModifiedName, COUNT(A.TransactionKey) AS TimeTrade, C.CreatedDate
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey= B.TransactionKey
INNER JOIN CRM_Customer C ON C.CustomerKey = A.CustomerKey
WHERE 1 = 1";
            if (Type != string.Empty && Type.Contains(","))
                zSQL += " AND B.TransactionCategory IN (" + Type.Remove(Type.LastIndexOf(",")) + ")";
            if (Name != string.Empty)
                zSQL += " AND C.CustomerName LIKE @Name";
            if (Phone != string.Empty)
                zSQL += " AND (C.Phone1 LIKE @Phone OR C.Phone2 LIKE @Phone)";
            if (Department != 0)
                zSQL += " AND C.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND C.EmployeeKey = @Employee";
            zSQL += @"
GROUP BY C.CustomerKey, C.CategoryKey, C.CustomerName, C.Phone1, C.Phone2, C.Email1, C.Email2, 
C.Address1, C.Birthday, C.ModifiedDate, C.ModifiedName, C.CreatedDate 
ORDER BY C.CreatedDate DESC";

            DataTable zTable = new DataTable();
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            zList = zTable.DataTableToList<ItemCustomer>();
            return zList;
        }

        public static List<ItemTrade> Get_Asset(int Customer)
        {
            string zSQL = @"
SELECT A.AssetKey, A.ProjectKey, A.AssetID, A.TransactionKey AS TradeKey, A.Income,
CASE B.IsOwner WHEN 1 THEN N'Chủ nhà' WHEN 2 THEN N'Khách thuê nhà' WHEN 3 THEN N'Khách mua' END AS [Owner], 
CASE A.TransactionCategory WHEN 227 THEN N'Cho thuê' WHEN 229 THEN N'Cho thuê' WHEN 230 THEN N'Bán mới' WHEN 228 THEN N'Chuyển nhượng' END AS [Category],
C.CustomerName,A.DepartmentKey, A.EmployeeKey, A.DateContract AS ContractDate,
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) [Address]
FROM FNC_Transaction A 
LEFT JOIN FNC_Transaction_Customer B ON A.TransactionKey = B.TransactionKey
LEFT JOIN CRM_Customer C ON B.CustomerKey = C.CustomerKey
WHERE C.CustomerKey = @CustomerKey";
            //zSQL += " ORDER BY A.CreatedDate";
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = Customer;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<ItemTrade> zList = zTable.DataTableToList<ItemTrade>();
            return zList;
        }


        //---------------------------------
        public static DataTable CustomerChart(int EmployeeKey, int DepartmentKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zWhere = "";
            if (DepartmentKey != 0)
                zWhere += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zWhere += " AND A.EmployeeKey = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zWhere += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            string zSQL = @"
WITH X AS
(
SELECT N'Quan tâm' AS [NAME], COUNT(A.CustomerKey) AS [VALUE]
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent 
WHERE B.CategoryConsent IN (68,69,70) " + zWhere + @"
UNION
SELECT  N'Đã giao dịch' AS [NAME], COUNT(A.CustomerKey) AS [VALUE]
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey= B.TransactionKey
INNER JOIN CRM_Customer C ON C.CustomerKey = A.CustomerKey
WHERE A.IsOwner IN (2,3)" + zWhere + @"
UNION
SELECT  N'Ký gửi đã giao dịch' AS [NAME], COUNT(A.CustomerKey) AS [VALUE]
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey= B.TransactionKey
INNER JOIN CRM_Customer C ON C.CustomerKey = A.CustomerKey
WHERE A.IsOwneR = 1 " + zWhere + @"
) 
SELECT * FROM X
";


            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Count(int EmployeeKey, int DepartmentKey, DateTime FromDate, DateTime ToDate)
        {
            int KQ = 0;
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer A WHERE 1 = 1";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                KQ = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return KQ;
        }

        //check
        public static DataTable List(int EmployeeKey, string ID, string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 0 AutoKey, A.CustomerKey, A.CustomerName, 
Phone1, Phone2, Email1, Email2, A.Birthday, A.CreatedName, A.CreatedDate, A.Birthday, Address1, CardID, CardDate, CardPlace
FROM CRM_Customer A 
WHERE 1=1";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (ID != string.Empty)
                zSQL += " AND CustomerID = @CustomerID";
            if (Name != string.Empty)
                zSQL += " AND (CustomerName LIKE @FirstName)";

            zSQL += @"
UNION ALL
SELECT 0 AutoKey, A.CustomerKey, A.CustomerName, 
Phone1, Phone2, Email1, Email2, A.Birthday, A.CreatedName, A.CreatedDate, A.Birthday, Address1, CardID, CardDate, CardPlace
FROM [dbo].[PUL_SharePermition] B LEFT JOIN CRM_Customer A ON A.CustomerKey = B.AssetKey
WHERE B.ObjectTable = 'Customer' ";
            if (EmployeeKey != 0)
                zSQL += " AND B.EmployeeKey = @EmployeeKey";
            if (ID != string.Empty)
                zSQL += " AND CustomerID = @CustomerID";
            if (Name != string.Empty)
                zSQL += " AND (CustomerName LIKE @FirstName)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + ID + "%";
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable AssetOwner(int EmployeeKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, C.Product , B.DateContractEnd, 0 CategoryConsent
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey = B.TransactionKey
LEFT JOIN dbo.SYS_Categories C ON B.ProjectKey = C.AutoKey 
WHERE IsOwner = 1";

            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (FirstName LIKE @NAME OR LastName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.ProjectKey = @ProjectKey";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;

                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable AssetTransaction(int EmployeeKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, C.Product , B.DateContractEnd, 0 CategoryConsent
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey = B.TransactionKey
LEFT JOIN dbo.SYS_Categories C ON B.ProjectKey = C.AutoKey 
WHERE IsOwner = 0";

            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (FirstName LIKE @NAME OR LastName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.ProjectKey = @ProjectKey";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int EmployeeKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate, string Potenial)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.CustomerKey AS AutoKey, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent,
Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset],  A.CreatedDate, A.Birthday
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE 1 = 1 ";

            if (EmployeeKey != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey ";
            if (Potenial != string.Empty && Potenial != "0")
            {
                zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            }
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (FirstName LIKE @NAME OR LastName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.ProjectKey = @ProjectKey";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " ORDER BY CategoryConsent ASC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static string AutoID()
        {
            string zResult = "";
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(ConnectDataBase.ConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand("SELECT dbo.[FNC_AutoCustomerID]()", zConnect);
                return zCommand.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                zResult = ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static DataTable List(int EmployeeKey, string email, string name, string phone, string bulding, string NameSale, string Aparment, string FromDate, string ToDate, string Potenial)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 0 AutoKey, A.CustomerKey, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent,
Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset],  A.CreatedDate, A.Birthday
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE 1 = 1 ";

            if (EmployeeKey != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey ";
            if (Potenial != string.Empty)
            {
                zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            }
            if (!string.IsNullOrEmpty(name))
            {
                zSQL += " AND (FirstName LIKE @NAME OR LastName LIKE @NAME) ";
            }
            if (!string.IsNullOrEmpty(email))
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (!string.IsNullOrEmpty(phone))
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (bulding != null && bulding != "null")
            {
                zSQL += " AND B.Project LIKE '%" + bulding + "%'";
            }

            if (!string.IsNullOrEmpty(NameSale) && NameSale != "undefined")
            {
                zSQL += " AND CreatedName LIKE @CreatedName ";
            }

            if (Aparment != null && Aparment != "0")
            {
                zSQL += " AND B.[CategoryAsset]  LIKE '%" + Aparment + "%'"; ;
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " ORDER BY CategoryConsent ASC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                if (!string.IsNullOrEmpty(name))
                {
                    zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + name + "%";
                }
                if (!string.IsNullOrEmpty(email))
                {
                    zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + email + "%";
                }
                if (!string.IsNullOrEmpty(phone))
                {
                    zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + phone + "%";
                }
                if (!string.IsNullOrEmpty(Potenial))
                {
                    zCommand.Parameters.Add("@CategoryConsent", SqlDbType.Int).Value = Potenial;
                }
                if (!string.IsNullOrEmpty(NameSale))
                {
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = "%" + NameSale + "%";
                }
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //public static DataTable ListCustomerTransfer(string EmployName, string CustommerName)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT CRM_Customer.CustomerKey, CRM_Customer.CustomerID, CRM_Customer.CustomerID, CRM_Customer.LastName + ' ' + CRM_Customer.FirstName As CustomerName, CRM_Customer.Birthday, CRM_Customer.Email1, CRM_Customer.Email2, CRM_Customer.Phone1, CRM_Customer.Phone2  , HRM_Employees.LastName +' ' + HRM_Employees.FirstName As EmployeeName, HRM_Employees.EmployeeKey
        //                    FROM
        //                    CRM_Customer
        //                    LEFT JOIN 
        //                    HRM_Employees
        //                    ON CRM_Customer.EmployeeKey = HRM_Employees.EmployeeKey";

        //    if (!string.IsNullOrEmpty(CustommerName))
        //        zSQL += " WHERE (CRM_Customer.FirstName LIKE '%'+@NAME + '%' OR CRM_Customer.LastName LIKE '%'+@NAME + '%' OR (CRM_Customer.LastName + ' ' + CRM_Customer.FirstName) LIKE '%'+@NAME + '%' )";

        //    if (!zSQL.ToUpper().Contains(" WHERE "))
        //    {
        //        zSQL += " WHERE 1 = 1 ";
        //    }
        //    if (!string.IsNullOrEmpty(EmployName))
        //    {
        //        zSQL += " AND(HRM_Employees.LastName LIKE '%'+@EM_NAME + '%' OR HRM_Employees.LastName LIKE '%'+@EM_NAME + '%' OR (HRM_Employees.LastName +' ' + HRM_Employees.FirstName) LIKE '%'+@EM_NAME + '%') ";
        //    }
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        if (!string.IsNullOrEmpty(EmployName))
        //        {
        //            zCommand.Parameters.Add("@EM_NAME", SqlDbType.NVarChar).Value = "%" + EmployName + "%";
        //        }
        //        if (!string.IsNullOrEmpty(CustommerName))
        //        {
        //            zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustommerName + "%";
        //        }
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}

        //public static DataTable ListCustomerTransfer(string ListCustomerIds)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = @"SELECT CRM_Customer.CustomerKey, CRM_Customer.CustomerID, CRM_Customer.CustomerID, CRM_Customer.LastName + ' ' + CRM_Customer.FirstName As CustomerName, CRM_Customer.Birthday, CRM_Customer.Email1, CRM_Customer.Email2, CRM_Customer.Phone1, CRM_Customer.Phone2  , HRM_Employees.LastName +' ' + HRM_Employees.FirstName As EmployeeName, HRM_Employees.EmployeeKey
        //                    FROM
        //                    CRM_Customer
        //                    LEFT JOIN 
        //                    HRM_Employees
        //                    ON CRM_Customer.EmployeeKey = HRM_Employees.EmployeeKey Where CRM_Customer.CustomerKey IN(" + ListCustomerIds + ")";

        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}

        public static DataTable AllCustomerBirthDayNextDay()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM CRM_Customer A 
WHERE 
CONVERT(VARCHAR(10), A.Birthday, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 1, GETDATE()), 112)) AND 
(SELECT PositionKey FROM HRM_Employees WHERE EmployeeKey = A.EmployeeKey) IN (5, 6)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable AllTransactionIn30Day()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM FNC_Transaction WHERE CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 30, GETDATE()), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable AllProductIn30Day()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.* FROM PUL_Resale_Apartment A 
WHERE A. DateContractEnd IS NOT NULL AND 
CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 30, GETDATE()), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //====================

        public static DataTable ListTrade(int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,
 STUFF((
	SELECT ';' + S2.AssetID FROM FNC_Transaction_Customer S1 LEFT JOIN FNC_Transaction S2 ON S1.TransactionKey = S2.TransactionKey
    WHERE S1.CustomerKey = A.CustomerKey
    FOR XML PATH ('')) ,1,1,'') AS Asset
FROM CRM_Customer A
WHERE A.CustomerKey IN (SELECT CustomerKey FROM FNC_Transaction_Customer WHERE CustomerKey = A.CustomerKey AND IsOwner = 0)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListTrade(int EmployeeKey, int IsOwner)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,
 STUFF((
	SELECT ';' + S3.PRODUCT + ' (' +
		 STUFF((
			SELECT ';' +S2.AssetID FROM FNC_Transaction_Customer S1 
			LEFT JOIN FNC_Transaction S2 ON S1.TransactionKey = S2.TransactionKey	
			WHERE S1.CustomerKey = A.CustomerKey AND S1.IsOwner = @IsOwner AND S1.EmployeeKey = @EmployeeKey
			FOR XML PATH ('')) ,1,1,'') + ')'
	FROM FNC_Transaction_Customer S1 
	LEFT JOIN FNC_Transaction S2 ON S1.TransactionKey = S2.TransactionKey	
	LEFT JOIN [dbo].[SYS_Categories] S3 ON S3.AUTOKEY = S2.PROJECTKEY
    WHERE S1.CustomerKey = A.CustomerKey AND S1.IsOwner = @IsOwner AND S1.EmployeeKey = @EmployeeKey
    FOR XML PATH ('')) ,1,1,'') AS Trade
FROM CRM_Customer A 
INNER JOIN FNC_Transaction_Customer B ON B.CustomerKey = A.CustomerKey
WHERE B.IsOwner = @IsOwner AND A.EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //====================

        public static int Count(string EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Bed,
            string Phone, string FromDate, string ToDate, string Potenial)
        {
            int KQ = 0;
            string zSQL = @"SELECT COUNT(CustomerKey) FROM CRM_Customer A WHERE 1 = 1";
            //if (Bed != string.Empty)
            //    zSQL += " AND B.ConsentBedRoom = @Bed";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != string.Empty)
                zSQL += " AND EmployeeKey IN (" + EmployeeKey + ")";

            //if (Potenial != string.Empty && Potenial != "0")
            //{
            //    zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            //}

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (CustomerName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            //if (Project != 0)
            //{
            //    zSQL += " AND B.Project LIKE '%" + Project + "%'";
            //}

            //if (AssetCategory != 0)
            //{
            //    zSQL += " AND B.AssetCategory = @AssetCategory";
            //}

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Bed", SqlDbType.NVarChar).Value = Bed;

                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                KQ = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return KQ;
        }

        public static DataTable ListCustomer(string EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Bed,
            string Phone, string FromDate, string ToDate, string Potenial, bool ViewAll, int Page1, int Page2)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS(

SELECT NULL AutoKey, A.CustomerKey, A.CustomerName, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent, Product AS Name_CategoryConsent, B.ConsentBedRoom, B.Name_Asset, B.Name_Project,
A.CreatedName, B.[CategoryAsset], A.CreatedDate, A.ModifiedDate, C.RANK
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE 1 = 1";

            //if (ViewAll)
            //    zSQL += " AND A.CustomerKey NOT IN (SELECT CustomerKey FROM FNC_Transaction_Customer)";
            if (Bed != "0")
                zSQL += " AND B.ConsentBedRoom = @Bed";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != string.Empty)
                zSQL += " AND EmployeeKey IN (" + EmployeeKey + ")";

            if (Potenial != string.Empty && Potenial != "0")
            {
                zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            }

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (CustomerName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.CategoryAsset LIKE '%" + AssetCategory + "%'";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " UNION ";
            zSQL += @"
SELECT B.AutoKey, A.CustomerKey, A.CustomerName, 
C.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Birthday, C.CategoryConsent, Product AS Name_CategoryConsent, C.ConsentBedRoom, C.Name_Asset, C.Name_Project,
A.CreatedName, C.[CategoryAsset], A.CreatedDate, A.ModifiedDate, D.RANK
FROM PUL_SharePermition B
LEFT JOIN CRM_Customer A ON A.CustomerKey = B.AssetKey
LEFT JOIN CRM_Customer_Consents C ON A.CustomerKey = C.CustomerKey
LEFT JOIN SYS_Categories D ON D.[AutoKey] = C.CategoryConsent
LEFT JOIN HRM_Employees F ON F.EmployeeKey = B.EmployeeKey
WHERE B.ObjectTable = 'Customer'";
            if (Bed != "0")
                zSQL += " AND C.ConsentBedRoom = @Bed";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (CustomerName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND C.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND C.CategoryAsset LIKE '%" + AssetCategory + "%'";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            if (EmployeeKey != string.Empty)
                zSQL += " AND B.EmployeeKey IN (" + EmployeeKey + ")";
            zSQL += @") 
SELECT * FROM
    (
    SELECT ROW_NUMBER() OVER ( ORDER BY RANK ASC, ModifiedDate DESC ) AS RowNum, * FROM X) 
Z WHERE Z.RowNum >= @Page1 AND Z.RowNum <= @Page2";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@Page1", SqlDbType.Int).Value = Page1;
                zCommand.Parameters.Add("@Page2", SqlDbType.Int).Value = Page2;

                zCommand.Parameters.Add("@Bed", SqlDbType.NVarChar).Value = Bed;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Count(string EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName,
            string Phone, string FromDate, string ToDate, string Potenial, bool ViewAll)
        {
            int zResult = 0;
            string zSQL = @"
WITH X AS(

SELECT NULL AutoKey, A.CustomerKey, A.CustomerName, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent, Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset], A.CreatedDate, A.ModifiedDate, C.RANK
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE 1 = 1";

            //if (ViewAll)
            //    zSQL += " AND A.CustomerKey NOT IN (SELECT CustomerKey FROM FNC_Transaction_Customer)";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != string.Empty)
                zSQL += " AND EmployeeKey IN (" + EmployeeKey + ")";

            if (Potenial != string.Empty && Potenial != "0")
            {
                zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            }

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (CustomerName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " UNION ALL ";
            zSQL += @"
SELECT B.AutoKey, A.CustomerKey, A.CustomerName, 
C.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Birthday, C.CategoryConsent, Product AS Name_CategoryConsent, A.CreatedName, C.[CategoryAsset], A.CreatedDate, A.ModifiedDate, D.RANK
FROM PUL_SharePermition B
LEFT JOIN CRM_Customer A ON A.CustomerKey = B.AssetKey
LEFT JOIN CRM_Customer_Consents C ON A.CustomerKey = C.CustomerKey
LEFT JOIN SYS_Categories D ON D.[AutoKey] = C.CategoryConsent
LEFT JOIN HRM_Employees F ON F.EmployeeKey = B.EmployeeKey
WHERE B.ObjectTable = 'Customer'";

            if (EmployeeKey != string.Empty)
                zSQL += " AND B.EmployeeKey IN (" + EmployeeKey + ")";
            zSQL += @") 

SELECT COUNT(X.CustomerKey) FROM X";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }


        public static DataTable ListCustomer(int EmployeeKey, int DepartmentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*, B.LastName + ' ' + B.FirstName AS EmployeeName FROM CRM_Customer A LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey WHERE 1 = 1";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer(int EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate, string Potenial, bool ViewAll)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS(

SELECT NULL AutoKey, A.CustomerKey, A.CustomerName, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent, Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset], A.CreatedDate, A.ModifiedDate, C.RANK
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE 1 = 1";

            //if (ViewAll)
            //    zSQL += " AND A.CustomerKey NOT IN (SELECT CustomerKey FROM FNC_Transaction_Customer)";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey ";

            if (Potenial != string.Empty && Potenial != "0")
            {
                zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            }

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (CustomerName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " UNION ALL ";
            zSQL += @"
                SELECT B.AutoKey, A.CustomerKey, A.CustomerName, 
C.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, C.CategoryConsent, Product AS Name_CategoryConsent, A.CreatedName, C.[CategoryAsset], A.CreatedDate, A.ModifiedDate, D.RANK
FROM PUL_SharePermition B
LEFT JOIN CRM_Customer A ON A.CustomerKey = B.AssetKey
LEFT JOIN CRM_Customer_Consents C ON A.CustomerKey = C.CustomerKey
LEFT JOIN SYS_Categories D ON D.[AutoKey] = C.CategoryConsent
LEFT JOIN HRM_Employees F ON F.EmployeeKey = B.EmployeeKey
WHERE B.ObjectTable = 'Customer'
)";
            if (EmployeeKey == 0)
                zSQL += " AND B.EmployeeKey";
            zSQL += "SELECT * FROM X ORDER BY RANK ASC, ModifiedDate DESC ";


            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer(string EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate, string Potenial, bool ViewAll)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS(

SELECT NULL AutoKey, A.CustomerKey, A.CustomerName, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent, Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset], A.CreatedDate, A.ModifiedDate, C.RANK
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE 1 = 1";

            //if (ViewAll)
            //    zSQL += " AND A.CustomerKey NOT IN (SELECT CustomerKey FROM FNC_Transaction_Customer)";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != string.Empty)
                zSQL += " AND EmployeeKey IN (" + EmployeeKey + ")";

            if (Potenial != string.Empty && Potenial != "0")
            {
                zSQL += " AND B.CategoryConsent IN (" + Potenial.Remove(Potenial.LastIndexOf(","), 1) + ") ";
            }

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (CustomerName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " UNION ";
            zSQL += @"
SELECT B.AutoKey, A.CustomerKey, A.CustomerName, 
C.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
A.Phone1, A.Phone2, A.Email1, A.Email2, A.Birthday, C.CategoryConsent, Product AS Name_CategoryConsent, A.CreatedName, C.[CategoryAsset], A.CreatedDate, A.ModifiedDate, D.RANK
FROM PUL_SharePermition B
LEFT JOIN CRM_Customer A ON A.CustomerKey = B.AssetKey
LEFT JOIN CRM_Customer_Consents C ON A.CustomerKey = C.CustomerKey
LEFT JOIN SYS_Categories D ON D.[AutoKey] = C.CategoryConsent
LEFT JOIN HRM_Employees F ON F.EmployeeKey = B.EmployeeKey
WHERE B.ObjectTable = 'Customer'";
            if (EmployeeKey != string.Empty)
                zSQL += " AND B.EmployeeKey IN (" + EmployeeKey + ")";
            zSQL += ") SELECT * FROM X ORDER BY RANK ASC, ModifiedDate DESC ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer_Trade(int EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate, int TypeOwner, int TypeTransaction)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT C.CustomerKey, C.CustomerName AS Name, C.Phone1, C.Email1, C.Address1, C.Birthday, COUNT(B.TransactionKey) AS Num, C.ModifiedDate
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey= B.TransactionKey
INNER JOIN CRM_Customer C ON C.CustomerKey = A.CustomerKey
WHERE 1 = 1";

            if (TypeOwner == 2)
                zSQL += " AND A.IsOwner IN (2,3)";
            else
                zSQL += " AND A.IsOwner = 1";
            if (TypeTransaction == 0)
                zSQL += " AND B.TransactionCategory IN (227, 229)";
            else if (TypeTransaction == 228)
                zSQL += " AND B.TransactionCategory = 228";
            else if (TypeTransaction == 230)
                zSQL += " AND B.TransactionCategory = 230";
            if (DepartmentKey != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND C.EmployeeKey = @EmployeeKey ";
            if (CustomerName != string.Empty)
                zSQL += " AND (C.CustomerName LIKE @NAME) ";
            if (Email != string.Empty)
                zSQL += " AND (C.Email1 LIKE @EMAIL OR C.Email2 LIKE @EMAIL) ";
            if (Phone != string.Empty)
                zSQL += " AND (C.Phone1 LIKE @PHONE OR C.Phone1 LIKE @PHONE) ";
            if (Project != 0)
                zSQL += " AND B.ProjectKey = @ProjectKey";
            if (AssetCategory != 0)
                zSQL += " AND B.AssetCategory = @AssetCategory";
            if (!string.IsNullOrEmpty(FromDate) &&
                !string.IsNullOrEmpty(ToDate))
                zSQL += " AND B.CreatedDate BETWEEN @FromDate AND @ToDate";

            zSQL += " GROUP BY C.CustomerKey, C.CustomerName, C.Phone1, C.Email1, C.Address1, C.Birthday, C.ModifiedDate, C.CreatedDate ORDER BY C.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                //zCommand.Parameters.Add("@IsOwner", SqlDbType.Int).Value = IsOwner;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project;

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer_Trade(string EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate, int TypeOwner, int TypeTransaction)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT C.CustomerKey, C.CustomerName AS Name, C.Phone1, C.Email1, C.Address1, C.Birthday, COUNT(B.TransactionKey) AS Num, C.ModifiedDate
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey= B.TransactionKey
INNER JOIN CRM_Customer C ON C.CustomerKey = A.CustomerKey
WHERE 1 = 1";

            if (TypeOwner == 2)
                zSQL += " AND A.IsOwner IN (2,3)";
            else
                zSQL += " AND A.IsOwner = 1";
            if (TypeTransaction == 0)
                zSQL += " AND B.TransactionCategory IN (227, 229)";
            else if (TypeTransaction == 228)
                zSQL += " AND B.TransactionCategory = 228";
            else if (TypeTransaction == 230)
                zSQL += " AND B.TransactionCategory = 230";
            if (DepartmentKey != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != string.Empty)
                zSQL += " AND C.EmployeeKey IN (" + EmployeeKey + ")";
            if (CustomerName != string.Empty)
                zSQL += " AND (C.CustomerName LIKE @NAME) ";
            if (Email != string.Empty)
                zSQL += " AND (C.Email1 LIKE @EMAIL OR C.Email2 LIKE @EMAIL) ";
            if (Phone != string.Empty)
                zSQL += " AND (C.Phone1 LIKE @PHONE OR C.Phone1 LIKE @PHONE) ";
            if (Project != 0)
                zSQL += " AND B.ProjectKey = @ProjectKey";
            if (AssetCategory != 0)
                zSQL += " AND B.AssetCategory = @AssetCategory";
            if (!string.IsNullOrEmpty(FromDate) &&
                !string.IsNullOrEmpty(ToDate))
                zSQL += " AND B.CreatedDate BETWEEN @FromDate AND @ToDate";

            zSQL += " GROUP BY C.CustomerKey, C.CustomerName, C.Phone1, C.Email1, C.Address1, C.Birthday, C.ModifiedDate, C.CreatedDate ORDER BY C.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                //zCommand.Parameters.Add("@IsOwner", SqlDbType.Int).Value = IsOwner;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project;

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer_Trade_Detail(int CustomerKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT B.*, 
C.ProjectName AS Name_Project,
D.CategoryName AS Name_Category,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
A.IsOwner,
Case A.IsOwner 
    WHEN 1 THEN N'Chủ nhà'
    WHEN 2 THEN N'Khách thuê'
    WHEN 3 THEN N'Khách mua'
END AS [OwnerName]
FROM FNC_Transaction_Customer A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey = B.TransactionKey
LEFT JOIN PUL_Project C ON C.ProjectKey = B.ProjectKey
LEFT JOIN PUL_Category D ON D.CategoryKey = B.AssetCategory
LEFT JOIN HRM_Employees F ON A.EmployeeKey = F.EmployeeKey
WHERE A.CustomerKey = @CustomerKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer_Consent(int EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.CustomerKey, LastName + ' '+ FirstName AS CustomerName, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent,
Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset],  A.CreatedDate, A.Birthday, A.ModifiedDate, C.Rank
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE B.CategoryConsent IN (68,69,70)";
            //if (ViewAll)
            //    zSQL += " AND A.CustomerKey NOT IN (SELECT CustomerKey FROM FNC_Transaction_Customer)";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";

            if (EmployeeKey != 0)
                zSQL += " AND EmployeeKey = @EmployeeKey ";

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (FirstName LIKE @NAME OR LastName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " ORDER BY RANK ASC, ModifiedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListCustomer_Consent(string EmployeeKey, int DepartmentKey, int Project, int AssetCategory, string Email, string CustomerName, string Phone, string FromDate, string ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.CustomerKey, LastName + ' '+ FirstName AS CustomerName, 
B.Project AS Product, ProjectOther, NULL AS DateContractEnd, 
Phone1, Phone2, Email1, Email2, A.Birthday, B.CategoryConsent,
Product AS Name_CategoryConsent, A.CreatedName, B.[CategoryAsset],  A.CreatedDate, A.Birthday, A.ModifiedDate, C.Rank
FROM CRM_Customer A 
LEFT JOIN dbo.CRM_Customer_Consents B ON A.CustomerKey = B.CustomerKey
LEFT JOIN dbo.SYS_Categories C ON C.[AutoKey] = B.CategoryConsent WHERE B.CategoryConsent IN (68,69,70)";
            //if (ViewAll)
            //    zSQL += " AND A.CustomerKey NOT IN (SELECT CustomerKey FROM FNC_Transaction_Customer)";

            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";

            if (EmployeeKey != string.Empty)
                zSQL += " AND A.EmployeeKey IN (" + EmployeeKey + ")";

            if (CustomerName != string.Empty)
            {
                zSQL += " AND (FirstName LIKE @NAME OR LastName LIKE @NAME) ";
            }
            if (Email != string.Empty)
            {
                zSQL += " AND (Email1 LIKE @EMAIL OR Email2 LIKE @EMAIL) ";
            }
            if (Phone != string.Empty)
            {
                zSQL += " AND (Phone1 LIKE @PHONE OR Phone1 LIKE @PHONE) ";
            }

            if (Project != 0)
            {
                zSQL += " AND B.Project LIKE '%" + Project + "%'";
            }

            if (AssetCategory != 0)
            {
                zSQL += " AND B.AssetCategory = @AssetCategory";
            }

            if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
            {
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";
            }

            zSQL += " ORDER BY RANK ASC, ModifiedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = AssetCategory;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@NAME", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                zCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar).Value = "%" + Email + "%";
                zCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar).Value = "%" + Phone + "%";

                if (!string.IsNullOrEmpty(FromDate) && !string.IsNullOrEmpty(ToDate))
                {
                    DateTime sqlFromDate = DateTime.Parse(FromDate);
                    DateTime sqlToDate = DateTime.Parse(ToDate);
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = sqlFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = sqlToDate;
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //====================

        public static DataTable ListConsents(int CustomerKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM CRM_Customer_Consents WHERE CustomerKey = @CustomerKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }



        //-------------15/11
        public static List<Customer_ItemMobi> Search_Mobile(int Department, int Employee, string Name, string Phone, string Status, string Room, string Project, string Category)
        {
            string text = @" 
SELECT A.CustomerKey, A.[Status] AS StatusKey, A.HasTrade,  
A.CustomerName, A.Phone1, A.Phone2, A.Email2, A.Email1, A.CategoryKey, 
dbo.FNC_SysCategoryName(A.[Status]) AS [Status], A.ModifiedDate, 
C.Room, C.CategoryName, C.ProjectName 
FROM CRM_Customer A  
LEFT JOIN SYS_Categories B ON A.[Status] = B.AutoKey  
OUTER APPLY dbo.[GetWant](A.CustomerKey) C 
WHERE 1 = 1";
            bool flag = Category != string.Empty;
            if (flag)
            {
                text += " AND C.CategoryName LIKE @CategoryName";
            }
            bool flag2 = Project != string.Empty;
            if (flag2)
            {
                text += " AND C.ProjectName LIKE @ProjectName";
            }
            bool flag3 = Status != string.Empty;
            if (flag3)
            {
                text = text + " AND A.Status IN (" + Status + ")";
            }
            bool flag4 = Name != string.Empty;
            if (flag4)
            {
                text += " AND A.CustomerName LIKE @Name";
            }
            bool flag5 = Phone != string.Empty;
            if (flag5)
            {
                text += " AND (A.Phone1 LIKE @Phone OR A.Phone2 LIKE @Phone)";
            }
            bool flag6 = Department != 0;
            if (flag6)
            {
                text += " AND A.DepartmentKey = @Department";
            }
            bool flag7 = Employee != 0;
            if (flag7)
            {
                text += " AND A.EmployeeKey = @Employee";
            }
            bool flag8 = Room != string.Empty;
            if (flag8)
            {
                text += " AND C.Room = @Room";
            }
            text += " ORDER BY B.[Rank]";
            DataTable dataTable = new DataTable();
            List<Customer_ItemMobi> list = new List<Customer_ItemMobi>();
            string connectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand(text, sqlConnection);
                sqlCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Room;
                sqlCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                sqlCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                sqlCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = "%" + Project + "%";
                sqlCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = "%" + Category + "%";
                sqlCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                sqlCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataTable);
                sqlCommand.Dispose();
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                string text2 = ex.ToString();
            }
            return dataTable.DataTableToList<Customer_ItemMobi>();
        }

        public static int CheckExists(string CheckString)
        {
            SqlContext sql = new SqlContext();
            sql.CMD.Parameters.Add("@Check", SqlDbType.NVarChar).Value = CheckString;
            int result = sql.GetObject("SELECT A.CustomerKey FROM CRM_Customer A WHERE A.CardID = @Check OR A.Phone1 = @Check").ToInt();
            return result;
        }
    }
}
