﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.CRM
{
    public class Customer_Info
    {
        #region [ Field Name ]
        private int _CustomerKey = 0;
        private int _CategoryKey = 0;
        private int _HasTrade = 0;
        private int _Status = 0;
        private string _StatusName = "";
        private string _WantSearch = "";
        private string _WantShow = "";
        private string _CustomerID = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _CustomerName = "";
        private DateTime _Birthday;
        private string _CardID = "";
        private DateTime _CardDate;
        private string _CardPlace = "";
        private string _SourceNote = "";
        private string _Email1 = "";
        private string _Email2 = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Address3 = "";
        private string _CompanyName = "";
        private string _TaxCode = "";
        private string _Description = "";
        private string _Street1 = "";
        private int _WardKey1 = 0;
        private int _DistrictKey1 = 0;
        private int _ProvinceKey1 = 0;
        private int _WardKey3 = 0;
        private int _DistrictKey3 = 0;
        private int _ProvinceKey3 = 0;
        private string _Street2 = "";
        private string _Street3 = "";
        private int _WardKey2 = 0;
        private int _DistrictKey2 = 0;
        private int _ProvinceKey2 = 0;
        private int _EmployeeKey = 0;
        private string _EmployeeID = "";
        private int _DepartmentKey = 0;
        private string _DepartmentID = "";
        private int _IsDraft = 0;
        private int _CategoryCustomer = 0;
        private int _CategorySource = 0;
        private string _CustomerSource = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string ID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }
        public DateTime Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        public string CardID
        {
            get { return _CardID; }
            set { _CardID = value; }
        }
        public DateTime CardDate
        {
            get { return _CardDate; }
            set { _CardDate = value; }
        }
        public string CardPlace
        {
            get { return _CardPlace; }
            set { _CardPlace = value; }
        }
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }
        public string Email2
        {
            get { return _Email2; }
            set { _Email2 = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public string Address3
        {
            get { return _Address3; }
            set { _Address3 = value; }
        }
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }
        public string TaxCode
        {
            get { return _TaxCode; }
            set { _TaxCode = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Street1
        {
            get { return _Street1; }
            set { _Street1 = value; }
        }
        public int WardKey1
        {
            get { return _WardKey1; }
            set { _WardKey1 = value; }
        }
        public int DistrictKey1
        {
            get { return _DistrictKey1; }
            set { _DistrictKey1 = value; }
        }
        public int ProvinceKey1
        {
            get { return _ProvinceKey1; }
            set { _ProvinceKey1 = value; }
        }
        public string Street2
        {
            get { return _Street2; }
            set { _Street2 = value; }
        }
        public int WardKey2
        {
            get { return _WardKey2; }
            set { _WardKey2 = value; }
        }
        public int DistrictKey2
        {
            get { return _DistrictKey2; }
            set { _DistrictKey2 = value; }
        }
        public int ProvinceKey2
        {
            get { return _ProvinceKey2; }
            set { _ProvinceKey2 = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }
        public int IsDraft
        {
            get { return _IsDraft; }
            set { _IsDraft = value; }
        }
        public int CategoryCustomer
        {
            get { return _CategoryCustomer; }
            set { _CategoryCustomer = value; }
        }
        public int CategorySource
        {
            get { return _CategorySource; }
            set { _CategorySource = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string SourceNote
        {
            get
            {
                return _SourceNote;
            }

            set
            {
                _SourceNote = value;
            }
        }

        public string Street3
        {
            get
            {
                return _Street3;
            }

            set
            {
                _Street3 = value;
            }
        }

        public int WardKey3
        {
            get
            {
                return _WardKey3;
            }

            set
            {
                _WardKey3 = value;
            }
        }

        public int DistrictKey3
        {
            get
            {
                return _DistrictKey3;
            }

            set
            {
                _DistrictKey3 = value;
            }
        }

        public int ProvinceKey3
        {
            get
            {
                return _ProvinceKey3;
            }

            set
            {
                _ProvinceKey3 = value;
            }
        }

        public string CustomerSource
        {
            get
            {
                return _CustomerSource;
            }

            set
            {
                _CustomerSource = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public int HasTrade
        {
            get
            {
                return _HasTrade;
            }

            set
            {
                _HasTrade = value;
            }
        }

        public string WantShow
        {
            get
            {
                return _WantShow;
            }

            set
            {
                _WantShow = value;
            }
        }

        public string WantSearch
        {
            get
            {
                return _WantSearch;
            }

            set
            {
                _WantSearch = value;
            }
        }

        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public string StatusName
        {
            get
            {
                return _StatusName;
            }

            set
            {
                _StatusName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Customer_Info()
        {
        }
        public Customer_Info(int CustomerKey)
        {
            string zSQL = @"
SELECT A.*, 
dbo.FNC_SysCategoryName(A.CategorySource) AS CustomerSource, 
dbo.FNC_SysCategoryName(A.[Status]) AS StatusName,
dbo.FNC_GetCustomerWant(A.CustomerKey) AS Needed
FROM CRM_Customer A 
WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["Status"] != DBNull.Value)
                        _Status = int.Parse(zReader["Status"].ToString());
                    _WantSearch = zReader["WantSearch"].ToString();
                    _StatusName = zReader["StatusName"].ToString();
                    _WantShow = zReader["Needed"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["HasTrade"] != DBNull.Value)
                        _HasTrade = int.Parse(zReader["HasTrade"].ToString());
                    _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CustomerID = zReader["CustomerID"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    _LastName = zReader["LastName"].ToString();
                    _SourceNote = zReader["SourceNote"].ToString();
                    _CustomerName = zReader["CustomerName"].ToString();
                    if (zReader["Birthday"] != DBNull.Value)
                        _Birthday = (DateTime)zReader["Birthday"];
                    _CardID = zReader["CardID"].ToString();
                    if (zReader["CardDate"] != DBNull.Value)
                        _CardDate = (DateTime)zReader["CardDate"];
                    _CardPlace = zReader["CardPlace"].ToString();
                    _Email1 = zReader["Email1"].ToString();
                    _Email2 = zReader["Email2"].ToString();
                    _Phone1 = zReader["Phone1"].ToString();
                    _Phone2 = zReader["Phone2"].ToString();
                    _Address1 = zReader["Address1"].ToString();
                    _Address2 = zReader["Address2"].ToString();
                    _Address3 = zReader["Address3"].ToString();
                    _CompanyName = zReader["CompanyName"].ToString();
                    _TaxCode = zReader["TaxCode"].ToString();
                    _Description = zReader["Description"].ToString();
                    _Street1 = zReader["Street1"].ToString();
                    _CustomerSource = zReader["CustomerSource"].ToString();
                    if (zReader["WardKey3"] != DBNull.Value)
                        _WardKey3 = int.Parse(zReader["WardKey3"].ToString());
                    if (zReader["DistrictKey3"] != DBNull.Value)
                        _DistrictKey3 = int.Parse(zReader["DistrictKey3"].ToString());
                    if (zReader["ProvinceKey3"] != DBNull.Value)
                        _ProvinceKey3 = int.Parse(zReader["ProvinceKey3"].ToString());
                    _Street3 = zReader["Street3"].ToString();

                    if (zReader["WardKey1"] != DBNull.Value)
                        _WardKey1 = int.Parse(zReader["WardKey1"].ToString());
                    if (zReader["DistrictKey1"] != DBNull.Value)
                        _DistrictKey1 = int.Parse(zReader["DistrictKey1"].ToString());
                    if (zReader["ProvinceKey1"] != DBNull.Value)
                        _ProvinceKey1 = int.Parse(zReader["ProvinceKey1"].ToString());
                    _Street2 = zReader["Street2"].ToString();
                    if (zReader["WardKey2"] != DBNull.Value)
                        _WardKey2 = int.Parse(zReader["WardKey2"].ToString());
                    if (zReader["DistrictKey2"] != DBNull.Value)
                        _DistrictKey2 = int.Parse(zReader["DistrictKey2"].ToString());
                    if (zReader["ProvinceKey2"] != DBNull.Value)
                        _ProvinceKey2 = int.Parse(zReader["ProvinceKey2"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentID = zReader["DepartmentID"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["CategoryCustomer"] != DBNull.Value)
                        _CategoryCustomer = int.Parse(zReader["CategoryCustomer"].ToString());
                    if (zReader["CategorySource"] != DBNull.Value)
                        _CategorySource = int.Parse(zReader["CategorySource"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Customer_Info(string Phone)
        {
            string zSQL = @"SELECT A.*, 
dbo.FNC_SysCategoryName(A.CategorySource) AS CustomerSource, 
dbo.FNC_SysCategoryName(A.[Status]) AS StatusName
FROM CRM_Customer A WHERE Phone1 = @Phone1 OR Phone2 =@Phone2";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = Phone;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = Phone;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["Status"] != DBNull.Value)
                        _Status = int.Parse(zReader["Status"].ToString());
                    _StatusName = zReader["StatusName"].ToString();
                    _WantSearch = zReader["WantSearch"].ToString();
                    _WantShow = zReader["WantShow"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["HasTrade"] != DBNull.Value)
                        _HasTrade = int.Parse(zReader["HasTrade"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CustomerID = zReader["CustomerID"].ToString();
                    _FirstName = zReader["FirstName"].ToString();
                    _LastName = zReader["LastName"].ToString();
                    _CustomerName = zReader["CustomerName"].ToString();
                    _SourceNote = zReader["SourceNote"].ToString();
                    if (zReader["Birthday"] != DBNull.Value)
                        _Birthday = (DateTime)zReader["Birthday"];
                    _CardID = zReader["CardID"].ToString();
                    if (zReader["CardDate"] != DBNull.Value)
                        _CardDate = (DateTime)zReader["CardDate"];
                    _CardPlace = zReader["CardPlace"].ToString();
                    _Email1 = zReader["Email1"].ToString();
                    _Email2 = zReader["Email2"].ToString();
                    _Phone1 = zReader["Phone1"].ToString();
                    _Phone2 = zReader["Phone2"].ToString();
                    _Address1 = zReader["Address1"].ToString();
                    _Address2 = zReader["Address2"].ToString();
                    _Address3 = zReader["Address3"].ToString();
                    _CompanyName = zReader["CompanyName"].ToString();
                    _TaxCode = zReader["TaxCode"].ToString();
                    _Description = zReader["Description"].ToString();
                    _Street1 = zReader["Street1"].ToString();
                    if (zReader["WardKey3"] != DBNull.Value)
                        _WardKey3 = int.Parse(zReader["WardKey3"].ToString());
                    if (zReader["DistrictKey3"] != DBNull.Value)
                        _DistrictKey3 = int.Parse(zReader["DistrictKey3"].ToString());
                    if (zReader["ProvinceKey3"] != DBNull.Value)
                        _ProvinceKey3 = int.Parse(zReader["ProvinceKey3"].ToString());
                    _CustomerSource = zReader["CustomerSource"].ToString();
                    _Street3 = zReader["Street3"].ToString();
                    if (zReader["WardKey1"] != DBNull.Value)
                        _WardKey1 = int.Parse(zReader["WardKey1"].ToString());
                    if (zReader["DistrictKey1"] != DBNull.Value)
                        _DistrictKey1 = int.Parse(zReader["DistrictKey1"].ToString());
                    if (zReader["ProvinceKey1"] != DBNull.Value)
                        _ProvinceKey1 = int.Parse(zReader["ProvinceKey1"].ToString());
                    _Street2 = zReader["Street2"].ToString();
                    if (zReader["WardKey2"] != DBNull.Value)
                        _WardKey2 = int.Parse(zReader["WardKey2"].ToString());
                    if (zReader["DistrictKey2"] != DBNull.Value)
                        _DistrictKey2 = int.Parse(zReader["DistrictKey2"].ToString());
                    if (zReader["ProvinceKey2"] != DBNull.Value)
                        _ProvinceKey2 = int.Parse(zReader["ProvinceKey2"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentID = zReader["DepartmentID"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["CategoryCustomer"] != DBNull.Value)
                        _CategoryCustomer = int.Parse(zReader["CategoryCustomer"].ToString());
                    if (zReader["CategorySource"] != DBNull.Value)
                        _CategorySource = int.Parse(zReader["CategorySource"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Customer_Info(string Name, string CheckExists)
        {
            string zSQL = "SELECT A.CustomerKey FROM CRM_Customer A WHERE CustomerName LIKE @CustomerName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["HasTrade"] != DBNull.Value)
                        _HasTrade = int.Parse(zReader["HasTrade"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value) ;
                    _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO CRM_Customer ("
         + " CategoryKey ,HasTrade,Status,WantSearch,WantShow ,CustomerID ,FirstName ,LastName ,CustomerName ,Birthday ,CardID ,CardDate ,CardPlace ,Email1 ,Email2 ,Phone1 ,Phone2 ,Address1 ,Address2 ,Address3 ,CompanyName ,TaxCode ,Description ,Street1 ,WardKey1 ,DistrictKey1 ,ProvinceKey1 ,Street2 ,WardKey2 ,DistrictKey2 ,ProvinceKey2 ,Street3 ,WardKey3 ,DistrictKey3 ,ProvinceKey3 ,EmployeeKey ,EmployeeID ,DepartmentKey ,DepartmentID ,IsDraft ,CategoryCustomer ,CategorySource ,SourceNote ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@CategoryKey ,@HasTrade,@Status,@WantSearch,@WantShow ,@CustomerID ,@FirstName ,@LastName ,@CustomerName ,@Birthday ,@CardID ,@CardDate ,@CardPlace ,@Email1 ,@Email2 ,@Phone1 ,@Phone2 ,@Address1 ,@Address2 ,@Address3 ,@CompanyName ,@TaxCode ,@Description ,@Street1 ,@WardKey1 ,@DistrictKey1 ,@ProvinceKey1 ,@Street2 ,@WardKey2 ,@DistrictKey2 ,@ProvinceKey2 ,@Street3 ,@WardKey3 ,@DistrictKey3 ,@ProvinceKey3 ,@EmployeeKey ,@EmployeeID ,@DepartmentKey ,@DepartmentID ,@IsDraft ,@CategoryCustomer ,@CategorySource ,@SourceNote ,GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT CustomerKey FROM CRM_Customer WHERE CustomerKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@WantSearch", SqlDbType.NVarChar).Value = _WantSearch;
                zCommand.Parameters.Add("@WantShow", SqlDbType.NVarChar).Value = _WantShow;

                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@HasTrade", SqlDbType.Int).Value = _HasTrade;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.Char).Value = _CustomerID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@SourceNote", SqlDbType.NVarChar).Value = _SourceNote;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = _CardID;
                if (_CardDate.Year == 0001)
                    zCommand.Parameters.Add("@CardDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CardDate", SqlDbType.DateTime).Value = _CardDate;
                zCommand.Parameters.Add("@CardPlace", SqlDbType.NVarChar).Value = _CardPlace;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@Address3", SqlDbType.NVarChar).Value = _Address3;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName;
                zCommand.Parameters.Add("@TaxCode", SqlDbType.NVarChar).Value = _TaxCode;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Street1", SqlDbType.NVarChar).Value = _Street1;
                zCommand.Parameters.Add("@WardKey1", SqlDbType.Int).Value = _WardKey1;
                zCommand.Parameters.Add("@DistrictKey1", SqlDbType.Int).Value = _DistrictKey1;
                zCommand.Parameters.Add("@ProvinceKey1", SqlDbType.Int).Value = _ProvinceKey1;
                zCommand.Parameters.Add("@Street3", SqlDbType.NVarChar).Value = _Street3;
                zCommand.Parameters.Add("@WardKey3", SqlDbType.Int).Value = _WardKey3;
                zCommand.Parameters.Add("@DistrictKey3", SqlDbType.Int).Value = _DistrictKey3;
                zCommand.Parameters.Add("@ProvinceKey3", SqlDbType.Int).Value = _ProvinceKey3;

                zCommand.Parameters.Add("@Street2", SqlDbType.NVarChar).Value = _Street2;
                zCommand.Parameters.Add("@WardKey2", SqlDbType.Int).Value = _WardKey2;
                zCommand.Parameters.Add("@DistrictKey2", SqlDbType.Int).Value = _DistrictKey2;
                zCommand.Parameters.Add("@ProvinceKey2", SqlDbType.Int).Value = _ProvinceKey2;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.Char).Value = _DepartmentID;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@CategoryCustomer", SqlDbType.Int).Value = _CategoryCustomer;
                zCommand.Parameters.Add("@CategorySource", SqlDbType.Int).Value = _CategorySource;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _CustomerKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE CRM_Customer SET SourceNote = @SourceNote, Status=@Status, WantSearch=@WantSearch, WantShow=@WantShow,"
                        + " CustomerName = @CustomerName,"
                        + " CategoryKey = @CategoryKey,"
                        + " HasTrade = @HasTrade,"
                        + " Birthday = @Birthday,"
                        + " CardID = @CardID,"
                        + " CardDate = @CardDate,"
                        + " CardPlace = @CardPlace,"
                        + " Email1 = @Email1,"
                        + " Email2 = @Email2,"
                        + " Phone1 = @Phone1,"
                        + " Phone2 = @Phone2,"
                        + " Address1 = @Address1,"
                        + " Address2 = @Address2,"
                        + " Address3 = @Address3,"
                        + " CompanyName = @CompanyName,"
                        + " TaxCode = @TaxCode,"
                        + " Description = @Description,"
                        + " Street1 = @Street1,"
                        + " WardKey1 = @WardKey1,"
                        + " DistrictKey1 = @DistrictKey1,"
                        + " ProvinceKey1 = @ProvinceKey1,"
                        + " Street3 = @Street3,"
                        + " WardKey3 = @WardKey3,"
                        + " DistrictKey3 = @DistrictKey3,"
                        + " ProvinceKey3 = @ProvinceKey3,"
                        + " Street2 = @Street2,"
                        + " WardKey2 = @WardKey2,"
                        + " DistrictKey2 = @DistrictKey2,"
                        + " ProvinceKey2 = @ProvinceKey2,"
                        + " IsDraft = @IsDraft,"
                        + " CategoryCustomer = @CategoryCustomer,"
                        + " CategorySource = @CategorySource,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE CustomerKey = @CustomerKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@WantSearch", SqlDbType.NVarChar).Value = _WantSearch;
                zCommand.Parameters.Add("@WantShow", SqlDbType.NVarChar).Value = _WantShow;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@HasTrade", SqlDbType.Int).Value = _HasTrade;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@SourceNote", SqlDbType.NVarChar).Value = _SourceNote;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.Char).Value = _CustomerID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = _CustomerName;
                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = _CardID;
                if (_CardDate.Year == 0001)
                    zCommand.Parameters.Add("@CardDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CardDate", SqlDbType.DateTime).Value = _CardDate;
                zCommand.Parameters.Add("@CardPlace", SqlDbType.NVarChar).Value = _CardPlace;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@Address3", SqlDbType.NVarChar).Value = _Address3;
                zCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = _CompanyName;
                zCommand.Parameters.Add("@TaxCode", SqlDbType.NVarChar).Value = _TaxCode;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Street1", SqlDbType.NVarChar).Value = _Street1;
                zCommand.Parameters.Add("@Street3", SqlDbType.NVarChar).Value = _Street3;
                zCommand.Parameters.Add("@WardKey3", SqlDbType.Int).Value = _WardKey3;
                zCommand.Parameters.Add("@DistrictKey3", SqlDbType.Int).Value = _DistrictKey3;
                zCommand.Parameters.Add("@ProvinceKey3", SqlDbType.Int).Value = _ProvinceKey3;
                zCommand.Parameters.Add("@WardKey1", SqlDbType.Int).Value = _WardKey1;
                zCommand.Parameters.Add("@DistrictKey1", SqlDbType.Int).Value = _DistrictKey1;
                zCommand.Parameters.Add("@ProvinceKey1", SqlDbType.Int).Value = _ProvinceKey1;
                zCommand.Parameters.Add("@Street2", SqlDbType.NVarChar).Value = _Street2;
                zCommand.Parameters.Add("@WardKey2", SqlDbType.Int).Value = _WardKey2;
                zCommand.Parameters.Add("@DistrictKey2", SqlDbType.Int).Value = _DistrictKey2;
                zCommand.Parameters.Add("@ProvinceKey2", SqlDbType.Int).Value = _ProvinceKey2;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.Char).Value = _DepartmentID;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@CategoryCustomer", SqlDbType.Int).Value = _CategoryCustomer;
                zCommand.Parameters.Add("@CategorySource", SqlDbType.Int).Value = _CategorySource;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_CustomerKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM CRM_Customer_Consents WHERE CustomerKey = @CustomerKey
DELETE FROM CRM_Customer WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string UpdateMobile()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE CRM_Customer SET 
Status = @Status, 
Description = @Description, 
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy
WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
