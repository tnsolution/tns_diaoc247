﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class Capital_Input_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private double _Amount = 0;
        private string _Name = "";
        private int _FromEmployee = 0;
        private int _ToEmployee = 0;
        private string _Note = "";
        private string _Description = "";
        private DateTime _CapitalDate;        
        private int _IsApproved = 0;
        private int _ApprovedBy = 0;
        private DateTime _ApprovedDate;    
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public int FromEmployee
        {
            get { return _FromEmployee; }
            set { _FromEmployee = value; }
        }
        public int ToEmployee
        {
            get { return _ToEmployee; }
            set { _ToEmployee = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CapitalDate
        {
            get { return _CapitalDate; }
            set { _CapitalDate = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public int IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }
        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        public DateTime ApprovedDate
        {
            get { return _ApprovedDate; }
            set { _ApprovedDate = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Capital_Input_Info()
        {
        }
        public Capital_Input_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Capital_Input WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    _Name = zReader["Name"].ToString();
                    if (zReader["FromEmployee"] != DBNull.Value)
                        _FromEmployee = int.Parse(zReader["FromEmployee"].ToString());
                    if (zReader["ToEmployee"] != DBNull.Value)
                        _ToEmployee = int.Parse(zReader["ToEmployee"].ToString());
                    _Note = zReader["Note"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["CapitalDate"] != DBNull.Value)
                        _CapitalDate = (DateTime)zReader["CapitalDate"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["IsApproved"] != DBNull.Value)
                        _IsApproved = int.Parse(zReader["IsApproved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Capital_Input ("
         + " Amount ,Name ,FromEmployee ,ToEmployee ,Note ,Description ,CapitalDate ,CreatedDate ,CreatedBy ,CreatedName ,IsApproved ,ApprovedBy ,ApprovedDate, ModifiedDate,ModifiedBy,ModifiedName ) "
         + " VALUES ( "
         + "@Amount ,@Name ,@FromEmployee ,@ToEmployee ,@Note ,@Description ,@CapitalDate ,GETDATE() ,@CreatedBy ,@CreatedName ,@IsApproved ,@ApprovedBy ,GETDATE(), GETDATE(),@ModifiedBy, @ModifiedName) "
         + " SELECT AutoKey FROM FNC_Capital_Input WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@FromEmployee", SqlDbType.Int).Value = _FromEmployee;
                zCommand.Parameters.Add("@ToEmployee", SqlDbType.Int).Value = _ToEmployee;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_CapitalDate.Year == 0001)
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = _CapitalDate;            
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;            
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Capital_Input SET "
                        + " Amount = @Amount,"
                        + " Name = @Name,"
                        + " FromEmployee = @FromEmployee,"
                        + " ToEmployee = @ToEmployee,"
                        + " Note = @Note,"
                        + " Description = @Description,"
                        + " CapitalDate = @CapitalDate,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " IsApproved = @IsApproved,"
                        + " ApprovedBy = @ApprovedBy,"
                        + " ApprovedDate = @ApprovedDate"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@FromEmployee", SqlDbType.Int).Value = _FromEmployee;
                zCommand.Parameters.Add("@ToEmployee", SqlDbType.Int).Value = _ToEmployee;
                zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = _Note;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_CapitalDate.Year == 0001)
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = _CapitalDate;              
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Capital_Input WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Capital_Input SET
IsApproved = @IsApproved,
ApprovedBy = @ApprovedBy,
ApprovedDate = GETDATE(),
ModifiedDate = GETDATE(),
ModifiedName = @ModifiedName,
ModifiedBy = @ModifiedBy
WHERE AutoKey = @AutoKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
