﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.FNC
{
    public class Capital_Item
    {
        public string AutoKey { get; set; }
        public string CapitalFrom { get; set; }
        public string CapitalBy { get; set; }
        public string CapitalDate { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string DepartmentKey { get; set; }
        public string EmployeeKey { get; set; }
        public string Message { get; set; }

    }
}
