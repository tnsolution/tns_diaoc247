﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class Capital_Output_Info
    {
        #region [ Field Name ]    
        private int _AutoKey = 0;
        private int _FromEmployee = 0;
        private int _ToEmployee = 0;
        private DateTime _CapitalDate;
        private string _Contents = "";
        private string _Description = "";
        private double _Amount = 0;
        private int _IsApproved = 0;
        private int _ApprovedBy = 0;
        private DateTime _ApprovedDate;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _BranchKey = 0;
        private int _CustomerKey = 0;
        private string _CreatedName = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int FromEmployee
        {
            get { return _FromEmployee; }
            set { _FromEmployee = value; }
        }
        public int ToEmployee
        {
            get { return _ToEmployee; }
            set { _ToEmployee = value; }
        }
        public DateTime CapitalDate
        {
            get { return _CapitalDate; }
            set { _CapitalDate = value; }
        }
        public string Contents
        {
            get { return _Contents; }
            set { _Contents = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public int IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }
        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        public DateTime ApprovedDate
        {
            get { return _ApprovedDate; }
            set { _ApprovedDate = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public DateTime ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }

            set
            {
                _ModifiedDate = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]        
        public Capital_Output_Info()
        {
        }
        public Capital_Output_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Capital_Output WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["FromEmployee"] != DBNull.Value)
                        _FromEmployee = int.Parse(zReader["FromEmployee"].ToString());
                    if (zReader["ToEmployee"] != DBNull.Value)
                        _ToEmployee = int.Parse(zReader["ToEmployee"].ToString());
                    if (zReader["CapitalDate"] != DBNull.Value)
                        _CapitalDate = (DateTime)zReader["CapitalDate"];
                    _Contents = zReader["Contents"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["IsApproved"] != DBNull.Value)
                        _IsApproved = int.Parse(zReader["IsApproved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]   
        public string SetStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE FNC_Capital_Output SET
IsApproved = @IsApproved,
ApprovedBy = @ApprovedBy,
ApprovedDate = GETDATE() ,
ModifiedDate = GETDATE(),
ModifiedName = @ModifiedName,
ModifiedBy = @ModifiedBy
WHERE AutoKey = @AutoKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Capital_Output ("
        + " FromEmployee ,ToEmployee ,CapitalDate ,Contents ,Description ,Amount ,IsApproved ,ApprovedBy ,ApprovedDate ,EmployeeKey ,DepartmentKey ,BranchKey ,CustomerKey ,CreatedName ,CreatedDate ,CreatedBy, ModifiedDate, ModifiedBy,ModifiedName ) "
         + " VALUES ( "
         + "@FromEmployee ,@ToEmployee ,@CapitalDate ,@Contents ,@Description ,@Amount ,@IsApproved ,@ApprovedBy ,@ApprovedDate ,@EmployeeKey ,@DepartmentKey ,@BranchKey ,@CustomerKey ,@CreatedName ,GETDATE() ,@CreatedBy, GETDATE() ,@ModifiedBy, @ModifiedName) "
         + " SELECT AutoKey FROM FNC_Capital_Output WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@FromEmployee", SqlDbType.Int).Value = _FromEmployee;
                zCommand.Parameters.Add("@ToEmployee", SqlDbType.Int).Value = _ToEmployee;
                if (_CapitalDate.Year == 0001)
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = _CapitalDate;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Capital_Output SET "
                        + " FromEmployee = @FromEmployee,"
                        + " ToEmployee = @ToEmployee,"
                        + " CapitalDate = @CapitalDate,"
                        + " Contents = @Contents,"
                        + " Description = @Description,"
                        + " Amount = @Amount,"
                        + " IsApproved = @IsApproved,"
                        + " ApprovedBy = @ApprovedBy,"
                        + " ApprovedDate = @ApprovedDate,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " BranchKey = @BranchKey,"
                        + " CustomerKey = @CustomerKey,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@FromEmployee", SqlDbType.Int).Value = _FromEmployee;
                zCommand.Parameters.Add("@ToEmployee", SqlDbType.Int).Value = _ToEmployee;
                if (_CapitalDate.Year == 0001)
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CapitalDate", SqlDbType.DateTime).Value = _CapitalDate;
                zCommand.Parameters.Add("@Contents", SqlDbType.NVarChar).Value = _Contents;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Capital_Output WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
