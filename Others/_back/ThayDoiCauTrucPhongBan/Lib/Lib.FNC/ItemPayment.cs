﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.FNC
{
    public class ItemPayment
    {
        public string AutoKey { get; set; }
        public string PaymentTo { get; set; }
        public string PaymentDate { get; set; }
        public string EmployeeKey { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string DepartmentKey { get; set; }
        public string Money { get; set; }
        public string IsHasVAT { get; set; }
        public string VAT { get; set; }
        public string Message { get; set; }
    }
}
