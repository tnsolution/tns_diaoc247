﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class Payment_Detail_Data
    {
        public static DataTable List(int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.PaymentDate, A.Description, A.Money, A.IsHasVAT, A.VAT, A.DepartmentKey, A.ApproveNote, A.ApproveNote2,
A.PaymentTo, A.PaymentBy, A.Amount, A.Contents, 
A.IsApproved, A.IsApproved2, A.ApprovedDate, A.Approved2Date, A.ApprovedBy, A.Approved2By,
dbo.FNC_GetNameEmployee(A.PaymentTo) AS PaymentToName,
dbo.FNC_GetNameEmployee(A.PaymentBy) AS PaymentByName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
dbo.FNC_GetNameEmployee(A.Approved2By) AS Approved2Name
FROM FNC_Payment_Detail A
WHERE A.CategoryKey = @Category ORDER BY A.CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Month, int Year, int Department, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.PaymentDate, A.Description, A.Money, A.IsHasVAT, A.VAT, A.DepartmentKey, A.ApproveNote, A.ApproveNote2,
A.PaymentTo, A.PaymentBy, A.Amount, A.Contents, 
A.IsApproved, A.IsApproved2, A.ApprovedDate, A.Approved2Date, A.ApprovedBy, A.Approved2By,
dbo.FNC_GetNameEmployee(A.PaymentTo) AS PaymentToName,
dbo.FNC_GetNameEmployee(A.PaymentBy) AS PaymentByName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
dbo.FNC_GetNameEmployee(A.Approved2By) AS Approved2Name
FROM FNC_Payment_Detail A WHERE A.CategoryKey = @Category";
            if (Month != 0)
                zSQL += " AND MONTH(A.PaymentDate) = @Month AND Year(A.PaymentDate) = @Year";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Month, int Year, int Department, int Category, int IsFinal)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.PaymentDate, A.Description, A.Money, A.IsHasVAT, A.VAT, A.DepartmentKey, A.ApproveNote, A.ApproveNote2,
A.PaymentTo, A.PaymentBy, A.Amount, A.Contents, 
A.IsApproved, A.IsApproved2, A.ApprovedDate, A.Approved2Date, A.ApprovedBy, A.Approved2By,
dbo.FNC_GetNameEmployee(A.PaymentTo) AS PaymentToName,
dbo.FNC_GetNameEmployee(A.PaymentBy) AS PaymentByName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
dbo.FNC_GetNameEmployee(A.Approved2By) AS Approved2Name
FROM FNC_Payment_Detail A 
WHERE A.CategoryKey = @Category AND IsApproved2 = @IsFinal";
            if (Month != 0)
                zSQL += " AND MONTH(A.PaymentDate) = @Month AND Year(A.PaymentDate) = @Year";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@IsFinal", SqlDbType.Int).Value = IsFinal;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(DateTime FromDate, DateTime ToDate, int Department, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AutoKey, A.PaymentDate, A.Description, A.Money, A.IsHasVAT, A.VAT, A.DepartmentKey, A.ApproveNote, A.ApproveNote2,
A.PaymentTo, A.PaymentBy, A.Amount, A.Contents, 
A.IsApproved, A.IsApproved2, A.ApprovedDate, A.Approved2Date, A.ApprovedBy, A.Approved2By,
dbo.FNC_GetNameEmployee(A.PaymentTo) AS PaymentToName,
dbo.FNC_GetNameEmployee(A.PaymentBy) AS PaymentByName,
dbo.FNC_GetDepartmentName(DepartmentKey) AS Department,
dbo.FNC_GetNameEmployee(A.ApprovedBy) AS ApprovedName,
dbo.FNC_GetNameEmployee(A.Approved2By) AS Approved2Name
FROM FNC_Payment_Detail A WHERE A.CategoryKey = @Category";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.PaymentDate BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static double Sum(int Month, int Year, int Department, int Category)
        {
            double Sum = 0;
            string zSQL = @"
SELECT SUM(A.Amount)
FROM FNC_Payment_Detail A 
WHERE A.CategoryKey = @Category";

            if (Month != 0)
                zSQL += " AND MONTH(A.PaymentDate) = @Month AND Year(A.PaymentDate) = @Year";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                double.TryParse(zCommand.ExecuteScalar().ToString(), out Sum);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return Sum;
        }
    }
}
