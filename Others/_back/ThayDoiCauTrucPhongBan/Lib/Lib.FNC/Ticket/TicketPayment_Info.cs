﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.FNC
{
    public class TicketPayment_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _Title = "";
        private DateTime _TimePayment;
        private string _Description = "";
        private string _Amount = "";
        private DateTime _CreatedDate;
        private DateTime _ModifiedDate;
        private string _ModifiedName = "";
        private string _CreatedName = "";
        private int _TicketKey = 0;
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public TicketPayment_Info()
        {
        }
        public TicketPayment_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_TicketPayment WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _Title = zReader["Title"].ToString();
                    if (zReader["TimePayment"] != DBNull.Value)
                        _TimePayment = (DateTime)zReader["TimePayment"];
                    _Description = zReader["Description"].ToString();
                    _Amount = zReader["Amount"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["TicketKey"] != DBNull.Value)
                        _TicketKey = int.Parse(zReader["TicketKey"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public DateTime TimePayment
        {
            get { return _TimePayment; }
            set { _TimePayment = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public int TicketKey
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_TicketPayment ("
        + " Title ,TimePayment ,Description ,Amount ,CreatedDate ,ModifiedDate ,ModifiedName ,CreatedName ,TicketKey ) "
         + " VALUES ( "
         + "@Title ,@TimePayment ,@Description ,@Amount ,GETDATE() ,GETDATE() ,@ModifiedName ,@CreatedName ,@TicketKey ) "
         + " SELECT AutoKey FROM FNC_TicketPayment WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                if (_TimePayment.Year == 0001)
                    zCommand.Parameters.Add("@TimePayment", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TimePayment", SqlDbType.DateTime).Value = _TimePayment;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.NVarChar).Value = _Amount;             
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE FNC_TicketPayment SET "
                        + " Title = @Title,"
                        + " TimePayment = @TimePayment,"
                        + " Description = @Description,"
                        + " Amount = @Amount,"                 
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedName = @ModifiedName,"                  
                        + " TicketKey = @TicketKey"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                if (_TimePayment.Year == 0001)
                    zCommand.Parameters.Add("@TimePayment", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TimePayment", SqlDbType.DateTime).Value = _TimePayment;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@Amount", SqlDbType.NVarChar).Value = _Amount;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_TicketPayment WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(int AutoKey, int TicketKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_TicketPayment WHERE AutoKey = @AutoKey AND TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
