﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.FNC
{
    public class TicketTrade_Customer_Info
    {
        #region [ Field Name ]
        private int _IsOwner = 0;
        private string _Owner = "";
        private int _AutoKey = 0;
        private int _TicketKey = 0;
        private int _CustomerKey = 0;
        private string _Description = "";
        private string _CustomerID = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _CustomerName = "";
        private DateTime _Birthday;
        private string _CardID = "";
        private DateTime _CardDate;
        private string _CardPlace = "";
        private string _Email1 = "";
        private string _Email2 = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private int _EmployeeKey = 0;
        private string _EmployeeID = "";
        private int _DepartmentKey = 0;
        private string _DepartmentID = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int IsOwner
        {
            get { return _IsOwner; }
            set { _IsOwner = value; }
        }
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int Key
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public DateTime Birthday
        {
            get { return _Birthday; }
            set { _Birthday = value; }
        }
        public string CardID
        {
            get { return _CardID; }
            set { _CardID = value; }
        }
        public DateTime CardDate
        {
            get { return _CardDate; }
            set { _CardDate = value; }
        }
        public string CardPlace
        {
            get { return _CardPlace; }
            set { _CardPlace = value; }
        }
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }
        public string Email2
        {
            get { return _Email2; }
            set { _Email2 = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string CustomerName
        {
            get
            {
                return _CustomerName;
            }

            set
            {
                _CustomerName = value;
            }
        }

        public string Owner
        {
            get
            {
                return _Owner;
            }

            set
            {
                _Owner = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]       
        public TicketTrade_Customer_Info()
        { }
        public TicketTrade_Customer_Info(int TicketKey, int CustomerKey)
        {
            string zSQL = "SELECT * FROM FNC_TicketTrade_Customer WHERE TicketKey = @TicketKey AND CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["IsOwner"] != DBNull.Value)
                        _IsOwner = int.Parse(zReader["IsOwner"].ToString());
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["TicketKey"] != DBNull.Value)
                        _TicketKey = int.Parse(zReader["TicketKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());            
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO FNC_TicketTrade_Customer (
TicketKey,CustomerKey,Description,IsOwner,CreatedDate,CreatedBy,CreatedName,ModifiedDate,ModifiedBy,ModifiedName) 
 VALUES (
@TicketKey,@CustomerKey,@Description,@IsOwner,GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName) 
SELECT AutoKey FROM FNC_TicketTrade_Customer WHERE AutoKey = SCOPE_IDENTITY() ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@IsOwner", SqlDbType.Int).Value = _IsOwner;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;           
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;            
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_TicketTrade_Customer SET "
                        + " TicketKey = @TicketKey,"
                        + " CustomerKey = @CustomerKey,"
                        + " Description = @Description,"
                        + " CustomerID = @CustomerID,"
                        + " FirstName = @FirstName,"
                        + " LastName = @LastName,"
                        + " Birthday = @Birthday,"
                        + " CardID = @CardID,"
                        + " CardDate = @CardDate,"
                        + " CardPlace = @CardPlace,"
                        + " Email1 = @Email1,"
                        + " Email2 = @Email2,"
                        + " Phone1 = @Phone1,"
                        + " Phone2 = @Phone2,"
                        + " Address1 = @Address1,"
                        + " Address2 = @Address2,"
                        + " IsOwner =@IsOwner,"
                        + " ModifiedDate = @ModifiedDate,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@IsOwner", SqlDbType.Int).Value = _IsOwner;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CustomerID", SqlDbType.Char).Value = _CustomerID;
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName;
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName;
                if (_Birthday.Year == 0001)
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Birthday", SqlDbType.DateTime).Value = _Birthday;
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = _CardID;
                if (_CardDate.Year == 0001)
                    zCommand.Parameters.Add("@CardDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CardDate", SqlDbType.DateTime).Value = _CardDate;
                zCommand.Parameters.Add("@CardPlace", SqlDbType.NVarChar).Value = _CardPlace;
                zCommand.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = _Email1;
                zCommand.Parameters.Add("@Email2", SqlDbType.NVarChar).Value = _Email2;
                zCommand.Parameters.Add("@Phone1", SqlDbType.NVarChar).Value = _Phone1;
                zCommand.Parameters.Add("@Phone2", SqlDbType.NVarChar).Value = _Phone2;
                zCommand.Parameters.Add("@Address1", SqlDbType.NVarChar).Value = _Address1;
                zCommand.Parameters.Add("@Address2", SqlDbType.NVarChar).Value = _Address2;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.Char).Value = _EmployeeID;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.Char).Value = _DepartmentID;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_TicketTrade_Customer WHERE CustomerKey = @CustomerKey AND TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = Key;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(int CustomerKey, int TicketKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_TicketTrade_Customer WHERE CustomerKey = @CustomerKey AND TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Relate(int CustomerKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM FNC_TicketTrade WHERE TicketKey IN (SELECT TicketKey FROM FNC_TicketTrade_Customer WHERE CustomerKey = @CustomerKey)
DELETE FROM FNC_TicketTrade_Customer WHERE CustomerKey = @CustomerKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
