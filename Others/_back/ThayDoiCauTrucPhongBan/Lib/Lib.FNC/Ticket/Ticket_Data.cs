﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.FNC
{
    public class Ticket_Data
    {
        public static DataTable List(int Employee, int Department, string AssetName, int Project, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TicketKey, A.NgayCoc, A.Department, A.EmployeeName, A.HotenA, A.HotenB, A.CreatedName, A.Category,
CASE A.Category 
	WHEN 1 THEN N'Cho thuê'
	WHEN 2 THEN N'Chuyển nhượng'
END AS CategoryName,
CASE A.Category 
	WHEN 1 THEN A.GiaChoThue
	WHEN 2 THEN A.GiaBan
END AS GiaTriHopDong
FROM FNC_Ticket A WHERE Deleted=0";
            if (AssetName != string.Empty)
                zSQL += " AND A.MaCan LIKE @Asset";

            if (Project != 0)
                zSQL += " AND A.Duan = @Project";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                zCommand.Parameters.Add("@Project", SqlDbType.Int).Value = Project;
                zCommand.Parameters.Add("@Asset", SqlDbType.NVarChar).Value = "%" + AssetName + "%";
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Ticket";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Tracking(int Key, string Chat)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Message WHERE ObjectTable = 'CHAT' AND ObjectKey=@TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = Key;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Tracking(int Key)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Message WHERE ObjectTable = 'FNC_Ticket' AND ObjectKey=@TicketKey ORDER BY Date DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = Key;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
