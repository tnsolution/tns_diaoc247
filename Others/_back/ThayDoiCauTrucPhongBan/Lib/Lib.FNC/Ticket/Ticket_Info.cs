﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.FNC
{
    public class Ticket_Info
    {
        #region [ Field Name ]
        private int _TicketKey = 0;
        private int _Category = 0;
        private DateTime _NgayCoc;
        private string _DepartmentKey = "";
        private string _Department = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _ManagerKey = "";
        private string _ManagerName = "";
        private string _CustomerKeyA = "";
        private string _HotenA = "";
        private string _NgaySinhA = "";
        private string _CMNDA = "";
        private string _NoiCapA = "";
        private string _DiaChiLienHeA = "";
        private string _DiaChiLienLacA = "";
        private string _DienThoaiA = "";
        private string _EmailA = "";
        private string _NgayCapA = "";
        private string _CustomerKeyB = "";
        private string _HotenB = "";
        private string _NgaySinhB = "";
        private string _CMNDB = "";
        private string _NoiCapB = "";
        private string _DiaChiLienHeB = "";
        private string _DiaChiLienLacB = "";
        private string _DienThoaiB = "";
        private string _EmailB = "";
        private string _NgayCapB = "";
        private string _Duan = "";
        private string _MaCan = "";
        private string _LoaiCan = "";
        private string _DienTichTimTuong = "";
        private string _DienTichThongThuy = "";
        private string _GiaChoThue = "";
        private string _GiaChoThueBangChu = "";
        private string _ThueBaoGom = "";
        private string _ChuTaiKhoan = "";
        private string _SoTaiKhoan = "";
        private string _NganHang = "";
        private string _ChiNhanh = "";
        private string _BThanhToanThueA = "";
        private string _SoLan = "";
        private string _TuNgay = "";
        private string _DenNgay = "";
        private string _DauMoi = "";
        private string _Thang = "";
        private string _ThoiGianThue = "";
        private string _ThoiGianBanGiao = "";
        private string _ThoiGianTinhTien = "";
        private string _ThoaThuanThueKhac = "";
        private string _GiaBan = "";
        private string _GiaBanBangChu = "";
        private string _BanBaoGom = "";
        private string _BThanhToanBanA = "";
        private string _BThanhToanBanABangChu = "";
        private string _BDongChuDauTu = "";
        private string _BDongChuDauTuBangChu = "";
        private string _PhiDichVuC = "";
        private string _PhiDichVuCBangChu = "";
        private string _NgayCongChung1 = "";
        private string _NgayCongChung2 = "";
        private string _GiaCongChung = "";
        private string _ThoaThuanBanKhac = "";
        private string _Status = "";
        private string _StatusDate = "";
        private string _TicketID = "";
        private string _CodeDuAn = "";
        private string _CodeCan = "";
        private string _CongTy = "";
        private string _MaSoThue = "";
        private string _DaiDien = "";
        private string _ChucVu = "";
        private int _Approved = 0;
        private int _ApprovedBy = 0;
        private DateTime _ApprovedDate;
        private string _Unit = "";
        private string _ThoiGianKyHD = "";
        private string _NgayThueThangDau = "";
        private string _TienThueThangDau = "";
        private string _SoTienThueThangDau = "";
        private string _SoTienThueThangBangChu = "";
        private string _SoThangThueDau = "";
        private string _ThueTuNgay = "";
        private string _ThueDenNgay = "";
        private string _HotenC = "";
        private string _MaNhanVienC = "";
        private string _ChucVuC = "";
        private string _DiaChiC = "";
        private string _DiaChiLienHeC = "";
        private string _GiayUyQuyenC = "";
        private string _CreatedBy = "";
        private DateTime _CreatedDate;
        private string _CreatedName = "";
        private string _ModifiedBy = "";
        private DateTime _ModifiedDate;
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int TicketKey
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        public DateTime NgayCoc
        {
            get { return _NgayCoc; }
            set { _NgayCoc = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string ManagerKey
        {
            get { return _ManagerKey; }
            set { _ManagerKey = value; }
        }
        public string ManagerName
        {
            get { return _ManagerName; }
            set { _ManagerName = value; }
        }
        public string CustomerKeyA
        {
            get { return _CustomerKeyA; }
            set { _CustomerKeyA = value; }
        }
        public string HotenA
        {
            get { return _HotenA; }
            set { _HotenA = value; }
        }
        public string NgaySinhA
        {
            get { return _NgaySinhA; }
            set { _NgaySinhA = value; }
        }
        public string CMNDA
        {
            get { return _CMNDA; }
            set { _CMNDA = value; }
        }
        public string NoiCapA
        {
            get { return _NoiCapA; }
            set { _NoiCapA = value; }
        }
        public string DiaChiLienHeA
        {
            get { return _DiaChiLienHeA; }
            set { _DiaChiLienHeA = value; }
        }
        public string DiaChiLienLacA
        {
            get { return _DiaChiLienLacA; }
            set { _DiaChiLienLacA = value; }
        }
        public string DienThoaiA
        {
            get { return _DienThoaiA; }
            set { _DienThoaiA = value; }
        }
        public string EmailA
        {
            get { return _EmailA; }
            set { _EmailA = value; }
        }
        public string NgayCapA
        {
            get { return _NgayCapA; }
            set { _NgayCapA = value; }
        }
        public string CustomerKeyB
        {
            get { return _CustomerKeyB; }
            set { _CustomerKeyB = value; }
        }
        public string HotenB
        {
            get { return _HotenB; }
            set { _HotenB = value; }
        }
        public string NgaySinhB
        {
            get { return _NgaySinhB; }
            set { _NgaySinhB = value; }
        }
        public string CMNDB
        {
            get { return _CMNDB; }
            set { _CMNDB = value; }
        }
        public string NoiCapB
        {
            get { return _NoiCapB; }
            set { _NoiCapB = value; }
        }
        public string DiaChiLienHeB
        {
            get { return _DiaChiLienHeB; }
            set { _DiaChiLienHeB = value; }
        }
        public string DiaChiLienLacB
        {
            get { return _DiaChiLienLacB; }
            set { _DiaChiLienLacB = value; }
        }
        public string DienThoaiB
        {
            get { return _DienThoaiB; }
            set { _DienThoaiB = value; }
        }
        public string EmailB
        {
            get { return _EmailB; }
            set { _EmailB = value; }
        }
        public string NgayCapB
        {
            get { return _NgayCapB; }
            set { _NgayCapB = value; }
        }
        public string Duan
        {
            get { return _Duan; }
            set { _Duan = value; }
        }
        public string MaCan
        {
            get { return _MaCan; }
            set { _MaCan = value; }
        }
        public string LoaiCan
        {
            get { return _LoaiCan; }
            set { _LoaiCan = value; }
        }
        public string DienTichTimTuong
        {
            get { return _DienTichTimTuong; }
            set { _DienTichTimTuong = value; }
        }
        public string DienTichThongThuy
        {
            get { return _DienTichThongThuy; }
            set { _DienTichThongThuy = value; }
        }
        public string GiaChoThue
        {
            get { return _GiaChoThue; }
            set { _GiaChoThue = value; }
        }
        public string GiaChoThueBangChu
        {
            get { return _GiaChoThueBangChu; }
            set { _GiaChoThueBangChu = value; }
        }
        public string ThueBaoGom
        {
            get { return _ThueBaoGom; }
            set { _ThueBaoGom = value; }
        }
        public string ChuTaiKhoan
        {
            get { return _ChuTaiKhoan; }
            set { _ChuTaiKhoan = value; }
        }
        public string SoTaiKhoan
        {
            get { return _SoTaiKhoan; }
            set { _SoTaiKhoan = value; }
        }
        public string NganHang
        {
            get { return _NganHang; }
            set { _NganHang = value; }
        }
        public string ChiNhanh
        {
            get { return _ChiNhanh; }
            set { _ChiNhanh = value; }
        }
        public string BThanhToanThueA
        {
            get { return _BThanhToanThueA; }
            set { _BThanhToanThueA = value; }
        }
        public string SoLan
        {
            get { return _SoLan; }
            set { _SoLan = value; }
        }
        public string TuNgay
        {
            get { return _TuNgay; }
            set { _TuNgay = value; }
        }
        public string DenNgay
        {
            get { return _DenNgay; }
            set { _DenNgay = value; }
        }
        public string DauMoi
        {
            get { return _DauMoi; }
            set { _DauMoi = value; }
        }
        public string Thang
        {
            get { return _Thang; }
            set { _Thang = value; }
        }
        public string ThoiGianThue
        {
            get { return _ThoiGianThue; }
            set { _ThoiGianThue = value; }
        }
        public string ThoiGianBanGiao
        {
            get { return _ThoiGianBanGiao; }
            set { _ThoiGianBanGiao = value; }
        }
        public string ThoiGianTinhTien
        {
            get { return _ThoiGianTinhTien; }
            set { _ThoiGianTinhTien = value; }
        }
        public string ThoaThuanThueKhac
        {
            get { return _ThoaThuanThueKhac; }
            set { _ThoaThuanThueKhac = value; }
        }
        public string GiaBan
        {
            get { return _GiaBan; }
            set { _GiaBan = value; }
        }
        public string GiaBanBangChu
        {
            get { return _GiaBanBangChu; }
            set { _GiaBanBangChu = value; }
        }
        public string BanBaoGom
        {
            get { return _BanBaoGom; }
            set { _BanBaoGom = value; }
        }
        public string BThanhToanBanA
        {
            get { return _BThanhToanBanA; }
            set { _BThanhToanBanA = value; }
        }
        public string BThanhToanBanABangChu
        {
            get { return _BThanhToanBanABangChu; }
            set { _BThanhToanBanABangChu = value; }
        }
        public string BDongChuDauTu
        {
            get { return _BDongChuDauTu; }
            set { _BDongChuDauTu = value; }
        }
        public string BDongChuDauTuBangChu
        {
            get { return _BDongChuDauTuBangChu; }
            set { _BDongChuDauTuBangChu = value; }
        }
        public string PhiDichVuC
        {
            get { return _PhiDichVuC; }
            set { _PhiDichVuC = value; }
        }
        public string NgayCongChung1
        {
            get { return _NgayCongChung1; }
            set { _NgayCongChung1 = value; }
        }
        public string NgayCongChung2
        {
            get { return _NgayCongChung2; }
            set { _NgayCongChung2 = value; }
        }
        public string GiaCongChung
        {
            get { return _GiaCongChung; }
            set { _GiaCongChung = value; }
        }
        public string ThoaThuanBanKhac
        {
            get { return _ThoaThuanBanKhac; }
            set { _ThoaThuanBanKhac = value; }
        }
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public string StatusDate
        {
            get { return _StatusDate; }
            set { _StatusDate = value; }
        }
        public string TicketID
        {
            get { return _TicketID; }
            set { _TicketID = value; }
        }
        public string CodeDuAn
        {
            get { return _CodeDuAn; }
            set { _CodeDuAn = value; }
        }
        public string CodeCan
        {
            get { return _CodeCan; }
            set { _CodeCan = value; }
        }
        public string CongTy
        {
            get { return _CongTy; }
            set { _CongTy = value; }
        }
        public string MaSoThue
        {
            get { return _MaSoThue; }
            set { _MaSoThue = value; }
        }
        public string DaiDien
        {
            get { return _DaiDien; }
            set { _DaiDien = value; }
        }
        public string ChucVu
        {
            get { return _ChucVu; }
            set { _ChucVu = value; }
        }
        public int Approved
        {
            get { return _Approved; }
            set { _Approved = value; }
        }
        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        public DateTime ApprovedDate
        {
            get { return _ApprovedDate; }
            set { _ApprovedDate = value; }
        }
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }
        public string ThoiGianKyHD
        {
            get { return _ThoiGianKyHD; }
            set { _ThoiGianKyHD = value; }
        }
        public string NgayThueThangDau
        {
            get { return _NgayThueThangDau; }
            set { _NgayThueThangDau = value; }
        }
        public string TienThueThangDau
        {
            get { return _TienThueThangDau; }
            set { _TienThueThangDau = value; }
        }
        public string SoTienThueThangDau
        {
            get { return _SoTienThueThangDau; }
            set { _SoTienThueThangDau = value; }
        }
        public string SoTienThueThangBangChu
        {
            get { return _SoTienThueThangBangChu; }
            set { _SoTienThueThangBangChu = value; }
        }
        public string SoThangThueDau
        {
            get { return _SoThangThueDau; }
            set { _SoThangThueDau = value; }
        }
        public string ThueTuNgay
        {
            get { return _ThueTuNgay; }
            set { _ThueTuNgay = value; }
        }
        public string ThueDenNgay
        {
            get { return _ThueDenNgay; }
            set { _ThueDenNgay = value; }
        }
        public string HotenC
        {
            get { return _HotenC; }
            set { _HotenC = value; }
        }
        public string MaNhanVienC
        {
            get { return _MaNhanVienC; }
            set { _MaNhanVienC = value; }
        }
        public string ChucVuC
        {
            get { return _ChucVuC; }
            set { _ChucVuC = value; }
        }
        public string DiaChiC
        {
            get { return _DiaChiC; }
            set { _DiaChiC = value; }
        }
        public string DiaChiLienHeC
        {
            get { return _DiaChiLienHeC; }
            set { _DiaChiLienHeC = value; }
        }
        public string GiayUyQuyenC
        {
            get { return _GiayUyQuyenC; }
            set { _GiayUyQuyenC = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string PhiDichVuCBangChu { get => _PhiDichVuCBangChu; set => _PhiDichVuCBangChu = value; }
        #endregion

        #region [ Constructor Get Information ]
        public Ticket_Info()
        {
        }
        public Ticket_Info(int TicketKey)
        {
            string zSQL = "SELECT * FROM FNC_Ticket WHERE TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["TicketKey"] != DBNull.Value)
                        _TicketKey = int.Parse(zReader["TicketKey"].ToString());
                    if (zReader["Category"] != DBNull.Value)
                        _Category = int.Parse(zReader["Category"].ToString());
                    if (zReader["NgayCoc"] != DBNull.Value)
                        _NgayCoc = (DateTime)zReader["NgayCoc"];
                    _DepartmentKey = zReader["DepartmentKey"].ToString();
                    _Department = zReader["Department"].ToString();
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _ManagerKey = zReader["ManagerKey"].ToString();
                    _ManagerName = zReader["ManagerName"].ToString();
                    _CustomerKeyA = zReader["CustomerKeyA"].ToString();
                    _HotenA = zReader["HotenA"].ToString();
                    _NgaySinhA = zReader["NgaySinhA"].ToString();
                    _CMNDA = zReader["CMNDA"].ToString();
                    _NoiCapA = zReader["NoiCapA"].ToString();
                    _DiaChiLienHeA = zReader["DiaChiLienHeA"].ToString();
                    _DiaChiLienLacA = zReader["DiaChiLienLacA"].ToString();
                    _DienThoaiA = zReader["DienThoaiA"].ToString();
                    _EmailA = zReader["EmailA"].ToString();
                    _NgayCapA = zReader["NgayCapA"].ToString();
                    _CustomerKeyB = zReader["CustomerKeyB"].ToString();
                    _HotenB = zReader["HotenB"].ToString();
                    _NgaySinhB = zReader["NgaySinhB"].ToString();
                    _CMNDB = zReader["CMNDB"].ToString();
                    _NoiCapB = zReader["NoiCapB"].ToString();
                    _DiaChiLienHeB = zReader["DiaChiLienHeB"].ToString();
                    _DiaChiLienLacB = zReader["DiaChiLienLacB"].ToString();
                    _DienThoaiB = zReader["DienThoaiB"].ToString();
                    _EmailB = zReader["EmailB"].ToString();
                    _NgayCapB = zReader["NgayCapB"].ToString();
                    _Duan = zReader["Duan"].ToString();
                    _MaCan = zReader["MaCan"].ToString();
                    _LoaiCan = zReader["LoaiCan"].ToString();
                    _DienTichTimTuong = zReader["DienTichTimTuong"].ToString();
                    _DienTichThongThuy = zReader["DienTichThongThuy"].ToString();
                    _GiaChoThue = zReader["GiaChoThue"].ToString();
                    _GiaChoThueBangChu = zReader["GiaChoThueBangChu"].ToString();
                    _ThueBaoGom = zReader["ThueBaoGom"].ToString();
                    _ChuTaiKhoan = zReader["ChuTaiKhoan"].ToString();
                    _SoTaiKhoan = zReader["SoTaiKhoan"].ToString();
                    _NganHang = zReader["NganHang"].ToString();
                    _ChiNhanh = zReader["ChiNhanh"].ToString();
                    _BThanhToanThueA = zReader["BThanhToanThueA"].ToString();
                    _SoLan = zReader["SoLan"].ToString();
                    _TuNgay = zReader["TuNgay"].ToString();
                    _DenNgay = zReader["DenNgay"].ToString();
                    _DauMoi = zReader["DauMoi"].ToString();
                    _Thang = zReader["Thang"].ToString();
                    _ThoiGianThue = zReader["ThoiGianThue"].ToString();
                    _ThoiGianBanGiao = zReader["ThoiGianBanGiao"].ToString();
                    _ThoiGianTinhTien = zReader["ThoiGianTinhTien"].ToString();
                    _ThoaThuanThueKhac = zReader["ThoaThuanThueKhac"].ToString();
                    _GiaBan = zReader["GiaBan"].ToString();
                    _GiaBanBangChu = zReader["GiaBanBangChu"].ToString();
                    _BanBaoGom = zReader["BanBaoGom"].ToString();
                    _BThanhToanBanA = zReader["BThanhToanBanA"].ToString();
                    _BThanhToanBanABangChu = zReader["BThanhToanBanABangChu"].ToString();
                    _BDongChuDauTu = zReader["BDongChuDauTu"].ToString();
                    _BDongChuDauTuBangChu = zReader["BDongChuDauTuBangChu"].ToString();
                    _PhiDichVuC = zReader["PhiDichVuC"].ToString();
                    _PhiDichVuCBangChu = zReader["PhiDichVuCBangChu"].ToString();
                    _NgayCongChung1 = zReader["NgayCongChung1"].ToString();
                    _NgayCongChung2 = zReader["NgayCongChung2"].ToString();
                    _GiaCongChung = zReader["GiaCongChung"].ToString();
                    _ThoaThuanBanKhac = zReader["ThoaThuanBanKhac"].ToString();
                    _Status = zReader["Status"].ToString();
                    _StatusDate = zReader["StatusDate"].ToString();
                    _TicketID = zReader["TicketID"].ToString();
                    _CodeDuAn = zReader["CodeDuAn"].ToString();
                    _CodeCan = zReader["CodeCan"].ToString();
                    _CongTy = zReader["CongTy"].ToString();
                    _MaSoThue = zReader["MaSoThue"].ToString();
                    _DaiDien = zReader["DaiDien"].ToString();
                    _ChucVu = zReader["ChucVu"].ToString();
                    if (zReader["Approved"] != DBNull.Value)
                        _Approved = int.Parse(zReader["Approved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                    _Unit = zReader["Unit"].ToString();
                    _ThoiGianKyHD = zReader["ThoiGianKyHD"].ToString();
                    _NgayThueThangDau = zReader["NgayThueThangDau"].ToString();
                    _TienThueThangDau = zReader["TienThueThangDau"].ToString();
                    _SoTienThueThangDau = zReader["SoTienThueThangDau"].ToString();
                    _SoTienThueThangBangChu = zReader["SoTienThueThangBangChu"].ToString();
                    _SoThangThueDau = zReader["SoThangThueDau"].ToString();
                    _ThueTuNgay = zReader["ThueTuNgay"].ToString();
                    _ThueDenNgay = zReader["ThueDenNgay"].ToString();
                    _HotenC = zReader["HotenC"].ToString();
                    _MaNhanVienC = zReader["MaNhanVienC"].ToString();
                    _ChucVuC = zReader["ChucVuC"].ToString();
                    _DiaChiC = zReader["DiaChiC"].ToString();
                    _DiaChiLienHeC = zReader["DiaChiLienHeC"].ToString();
                    _GiayUyQuyenC = zReader["GiayUyQuyenC"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedName = zReader["CreatedName"].ToString();
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }      
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Ticket ("
        + " Category ,NgayCoc ,DepartmentKey ,Department ,EmployeeKey ,EmployeeName ,ManagerKey ,ManagerName ,CustomerKeyA ,HotenA ,NgaySinhA ,CMNDA ,NoiCapA ,DiaChiLienHeA ,DiaChiLienLacA ,DienThoaiA ,EmailA ,NgayCapA ,CustomerKeyB ,HotenB ,NgaySinhB ,CMNDB ,NoiCapB ,DiaChiLienHeB ,DiaChiLienLacB ,DienThoaiB ,EmailB ,NgayCapB ,Duan ,MaCan ,LoaiCan ,DienTichTimTuong ,DienTichThongThuy ,GiaChoThue ,GiaChoThueBangChu ,ThueBaoGom ,ChuTaiKhoan ,SoTaiKhoan ,NganHang ,ChiNhanh ,BThanhToanThueA ,SoLan ,TuNgay ,DenNgay ,DauMoi ,Thang ,ThoiGianThue ,ThoiGianBanGiao ,ThoiGianTinhTien ,ThoaThuanThueKhac ,GiaBan ,GiaBanBangChu ,BanBaoGom ,BThanhToanBanA ,BThanhToanBanABangChu ,BDongChuDauTu ,BDongChuDauTuBangChu ,PhiDichVuC, PhiDichVuCBangChu ,NgayCongChung1 ,NgayCongChung2 ,GiaCongChung ,ThoaThuanBanKhac ,Status ,StatusDate ,TicketID ,CodeDuAn ,CodeCan ,CongTy ,MaSoThue ,DaiDien ,ChucVu ,Approved ,ApprovedBy ,ApprovedDate ,Unit ,ThoiGianKyHD ,NgayThueThangDau ,TienThueThangDau ,SoTienThueThangDau ,SoTienThueThangBangChu ,SoThangThueDau ,ThueTuNgay ,ThueDenNgay ,HotenC ,MaNhanVienC ,ChucVuC ,DiaChiC ,DiaChiLienHeC ,GiayUyQuyenC ,CreatedBy ,CreatedDate ,CreatedName ,ModifiedBy ,ModifiedDate ,ModifiedName ) "
         + " VALUES ( "
         + "@Category ,@NgayCoc ,@DepartmentKey ,@Department ,@EmployeeKey ,@EmployeeName ,@ManagerKey ,@ManagerName ,@CustomerKeyA ,@HotenA ,@NgaySinhA ,@CMNDA ,@NoiCapA ,@DiaChiLienHeA ,@DiaChiLienLacA ,@DienThoaiA ,@EmailA ,@NgayCapA ,@CustomerKeyB ,@HotenB ,@NgaySinhB ,@CMNDB ,@NoiCapB ,@DiaChiLienHeB ,@DiaChiLienLacB ,@DienThoaiB ,@EmailB ,@NgayCapB ,@Duan ,@MaCan ,@LoaiCan ,@DienTichTimTuong ,@DienTichThongThuy ,@GiaChoThue ,@GiaChoThueBangChu ,@ThueBaoGom ,@ChuTaiKhoan ,@SoTaiKhoan ,@NganHang ,@ChiNhanh ,@BThanhToanThueA ,@SoLan ,@TuNgay ,@DenNgay ,@DauMoi ,@Thang ,@ThoiGianThue ,@ThoiGianBanGiao ,@ThoiGianTinhTien ,@ThoaThuanThueKhac ,@GiaBan ,@GiaBanBangChu ,@BanBaoGom ,@BThanhToanBanA ,@BThanhToanBanABangChu ,@BDongChuDauTu ,@BDongChuDauTuBangChu ,@PhiDichVuC, @PhiDichVuCBangChu ,@NgayCongChung1 ,@NgayCongChung2 ,@GiaCongChung ,@ThoaThuanBanKhac ,@Status ,@StatusDate ,@TicketID ,@CodeDuAn ,@CodeCan ,@CongTy ,@MaSoThue ,@DaiDien ,@ChucVu ,@Approved ,@ApprovedBy ,@ApprovedDate ,@Unit ,@ThoiGianKyHD ,@NgayThueThangDau ,@TienThueThangDau ,@SoTienThueThangDau ,@SoTienThueThangBangChu ,@SoThangThueDau ,@ThueTuNgay ,@ThueDenNgay ,@HotenC ,@MaNhanVienC ,@ChucVuC ,@DiaChiC ,@DiaChiLienHeC ,@GiayUyQuyenC ,@CreatedBy ,GETDATE() ,@CreatedName ,@ModifiedBy ,GETDATE() ,@ModifiedName ) "
         + " SELECT TicketKey FROM FNC_Ticket WHERE TicketKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                if (_NgayCoc.Year == 0001)
                    zCommand.Parameters.Add("@NgayCoc", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayCoc", SqlDbType.DateTime).Value = _NgayCoc;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = _Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.NVarChar).Value = _ManagerKey;
                zCommand.Parameters.Add("@ManagerName", SqlDbType.NVarChar).Value = _ManagerName;
                zCommand.Parameters.Add("@CustomerKeyA", SqlDbType.NVarChar).Value = _CustomerKeyA;
                zCommand.Parameters.Add("@HotenA", SqlDbType.NVarChar).Value = _HotenA;
                zCommand.Parameters.Add("@NgaySinhA", SqlDbType.NVarChar).Value = _NgaySinhA;
                zCommand.Parameters.Add("@CMNDA", SqlDbType.NVarChar).Value = _CMNDA;
                zCommand.Parameters.Add("@NoiCapA", SqlDbType.NVarChar).Value = _NoiCapA;
                zCommand.Parameters.Add("@DiaChiLienHeA", SqlDbType.NVarChar).Value = _DiaChiLienHeA;
                zCommand.Parameters.Add("@DiaChiLienLacA", SqlDbType.NVarChar).Value = _DiaChiLienLacA;
                zCommand.Parameters.Add("@DienThoaiA", SqlDbType.NVarChar).Value = _DienThoaiA;
                zCommand.Parameters.Add("@EmailA", SqlDbType.NVarChar).Value = _EmailA;
                zCommand.Parameters.Add("@NgayCapA", SqlDbType.NVarChar).Value = _NgayCapA;
                zCommand.Parameters.Add("@CustomerKeyB", SqlDbType.NVarChar).Value = _CustomerKeyB;
                zCommand.Parameters.Add("@HotenB", SqlDbType.NVarChar).Value = _HotenB;
                zCommand.Parameters.Add("@NgaySinhB", SqlDbType.NVarChar).Value = _NgaySinhB;
                zCommand.Parameters.Add("@CMNDB", SqlDbType.NVarChar).Value = _CMNDB;
                zCommand.Parameters.Add("@NoiCapB", SqlDbType.NVarChar).Value = _NoiCapB;
                zCommand.Parameters.Add("@DiaChiLienHeB", SqlDbType.NVarChar).Value = _DiaChiLienHeB;
                zCommand.Parameters.Add("@DiaChiLienLacB", SqlDbType.NVarChar).Value = _DiaChiLienLacB;
                zCommand.Parameters.Add("@DienThoaiB", SqlDbType.NVarChar).Value = _DienThoaiB;
                zCommand.Parameters.Add("@EmailB", SqlDbType.NVarChar).Value = _EmailB;
                zCommand.Parameters.Add("@NgayCapB", SqlDbType.NVarChar).Value = _NgayCapB;
                zCommand.Parameters.Add("@Duan", SqlDbType.NVarChar).Value = _Duan;
                zCommand.Parameters.Add("@MaCan", SqlDbType.NVarChar).Value = _MaCan;
                zCommand.Parameters.Add("@LoaiCan", SqlDbType.NVarChar).Value = _LoaiCan;
                zCommand.Parameters.Add("@DienTichTimTuong", SqlDbType.NVarChar).Value = _DienTichTimTuong;
                zCommand.Parameters.Add("@DienTichThongThuy", SqlDbType.NVarChar).Value = _DienTichThongThuy;
                zCommand.Parameters.Add("@GiaChoThue", SqlDbType.NVarChar).Value = _GiaChoThue;
                zCommand.Parameters.Add("@GiaChoThueBangChu", SqlDbType.NVarChar).Value = _GiaChoThueBangChu;
                zCommand.Parameters.Add("@ThueBaoGom", SqlDbType.NVarChar).Value = _ThueBaoGom;
                zCommand.Parameters.Add("@ChuTaiKhoan", SqlDbType.NVarChar).Value = _ChuTaiKhoan;
                zCommand.Parameters.Add("@SoTaiKhoan", SqlDbType.NVarChar).Value = _SoTaiKhoan;
                zCommand.Parameters.Add("@NganHang", SqlDbType.NVarChar).Value = _NganHang;
                zCommand.Parameters.Add("@ChiNhanh", SqlDbType.NVarChar).Value = _ChiNhanh;
                zCommand.Parameters.Add("@BThanhToanThueA", SqlDbType.NVarChar).Value = _BThanhToanThueA;
                zCommand.Parameters.Add("@SoLan", SqlDbType.NVarChar).Value = _SoLan;
                zCommand.Parameters.Add("@TuNgay", SqlDbType.NVarChar).Value = _TuNgay;
                zCommand.Parameters.Add("@DenNgay", SqlDbType.NVarChar).Value = _DenNgay;
                zCommand.Parameters.Add("@DauMoi", SqlDbType.NVarChar).Value = _DauMoi;
                zCommand.Parameters.Add("@Thang", SqlDbType.NVarChar).Value = _Thang;
                zCommand.Parameters.Add("@ThoiGianThue", SqlDbType.NVarChar).Value = _ThoiGianThue;
                zCommand.Parameters.Add("@ThoiGianBanGiao", SqlDbType.NVarChar).Value = _ThoiGianBanGiao;
                zCommand.Parameters.Add("@ThoiGianTinhTien", SqlDbType.NVarChar).Value = _ThoiGianTinhTien;
                zCommand.Parameters.Add("@ThoaThuanThueKhac", SqlDbType.NVarChar).Value = _ThoaThuanThueKhac;
                zCommand.Parameters.Add("@GiaBan", SqlDbType.NVarChar).Value = _GiaBan;
                zCommand.Parameters.Add("@GiaBanBangChu", SqlDbType.NVarChar).Value = _GiaBanBangChu;
                zCommand.Parameters.Add("@BanBaoGom", SqlDbType.NVarChar).Value = _BanBaoGom;
                zCommand.Parameters.Add("@BThanhToanBanA", SqlDbType.NVarChar).Value = _BThanhToanBanA;
                zCommand.Parameters.Add("@BThanhToanBanABangChu", SqlDbType.NVarChar).Value = _BThanhToanBanABangChu;
                zCommand.Parameters.Add("@BDongChuDauTu", SqlDbType.NVarChar).Value = _BDongChuDauTu;
                zCommand.Parameters.Add("@BDongChuDauTuBangChu", SqlDbType.NVarChar).Value = _BDongChuDauTuBangChu;
                zCommand.Parameters.Add("@PhiDichVuC", SqlDbType.NVarChar).Value = _PhiDichVuC;
                zCommand.Parameters.Add("@PhiDichVuCBangChu", SqlDbType.NVarChar).Value = _PhiDichVuCBangChu;
                zCommand.Parameters.Add("@NgayCongChung1", SqlDbType.NVarChar).Value = _NgayCongChung1;
                zCommand.Parameters.Add("@NgayCongChung2", SqlDbType.NVarChar).Value = _NgayCongChung2;
                zCommand.Parameters.Add("@GiaCongChung", SqlDbType.NVarChar).Value = _GiaCongChung;
                zCommand.Parameters.Add("@ThoaThuanBanKhac", SqlDbType.NVarChar).Value = _ThoaThuanBanKhac;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = _Status;
                zCommand.Parameters.Add("@StatusDate", SqlDbType.NVarChar).Value = _StatusDate;
                zCommand.Parameters.Add("@TicketID", SqlDbType.NVarChar).Value = _TicketID;
                zCommand.Parameters.Add("@CodeDuAn", SqlDbType.NVarChar).Value = _CodeDuAn;
                zCommand.Parameters.Add("@CodeCan", SqlDbType.NVarChar).Value = _CodeCan;
                zCommand.Parameters.Add("@CongTy", SqlDbType.NVarChar).Value = _CongTy;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = _MaSoThue;
                zCommand.Parameters.Add("@DaiDien", SqlDbType.NVarChar).Value = _DaiDien;
                zCommand.Parameters.Add("@ChucVu", SqlDbType.NVarChar).Value = _ChucVu;
                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@Unit", SqlDbType.NVarChar).Value = _Unit;
                zCommand.Parameters.Add("@ThoiGianKyHD", SqlDbType.NVarChar).Value = _ThoiGianKyHD;
                zCommand.Parameters.Add("@NgayThueThangDau", SqlDbType.NVarChar).Value = _NgayThueThangDau;
                zCommand.Parameters.Add("@TienThueThangDau", SqlDbType.NVarChar).Value = _TienThueThangDau;
                zCommand.Parameters.Add("@SoTienThueThangDau", SqlDbType.NVarChar).Value = _SoTienThueThangDau;
                zCommand.Parameters.Add("@SoTienThueThangBangChu", SqlDbType.NVarChar).Value = _SoTienThueThangBangChu;
                zCommand.Parameters.Add("@SoThangThueDau", SqlDbType.NVarChar).Value = _SoThangThueDau;
                zCommand.Parameters.Add("@ThueTuNgay", SqlDbType.NVarChar).Value = _ThueTuNgay;
                zCommand.Parameters.Add("@ThueDenNgay", SqlDbType.NVarChar).Value = _ThueDenNgay;
                zCommand.Parameters.Add("@HotenC", SqlDbType.NVarChar).Value = _HotenC;
                zCommand.Parameters.Add("@MaNhanVienC", SqlDbType.NVarChar).Value = _MaNhanVienC;
                zCommand.Parameters.Add("@ChucVuC", SqlDbType.NVarChar).Value = _ChucVuC;
                zCommand.Parameters.Add("@DiaChiC", SqlDbType.NVarChar).Value = _DiaChiC;
                zCommand.Parameters.Add("@DiaChiLienHeC", SqlDbType.NVarChar).Value = _DiaChiLienHeC;
                zCommand.Parameters.Add("@GiayUyQuyenC", SqlDbType.NVarChar).Value = _GiayUyQuyenC;

                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;             
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;              
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                _TicketKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Ticket SET "
                        + " Category = @Category,"
                        + " NgayCoc = @NgayCoc,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " Department = @Department,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " ManagerKey = @ManagerKey,"
                        + " ManagerName = @ManagerName,"
                        + " CustomerKeyA = @CustomerKeyA,"
                        + " HotenA = @HotenA,"
                        + " NgaySinhA = @NgaySinhA,"
                        + " CMNDA = @CMNDA,"
                        + " NoiCapA = @NoiCapA,"
                        + " DiaChiLienHeA = @DiaChiLienHeA,"
                        + " DiaChiLienLacA = @DiaChiLienLacA,"
                        + " DienThoaiA = @DienThoaiA,"
                        + " EmailA = @EmailA,"
                        + " NgayCapA = @NgayCapA,"
                        + " CustomerKeyB = @CustomerKeyB,"
                        + " HotenB = @HotenB,"
                        + " NgaySinhB = @NgaySinhB,"
                        + " CMNDB = @CMNDB,"
                        + " NoiCapB = @NoiCapB,"
                        + " DiaChiLienHeB = @DiaChiLienHeB,"
                        + " DiaChiLienLacB = @DiaChiLienLacB,"
                        + " DienThoaiB = @DienThoaiB,"
                        + " EmailB = @EmailB,"
                        + " NgayCapB = @NgayCapB,"
                        + " Duan = @Duan,"
                        + " MaCan = @MaCan,"
                        + " LoaiCan = @LoaiCan,"
                        + " DienTichTimTuong = @DienTichTimTuong,"
                        + " DienTichThongThuy = @DienTichThongThuy,"
                        + " GiaChoThue = @GiaChoThue,"
                        + " GiaChoThueBangChu = @GiaChoThueBangChu,"
                        + " ThueBaoGom = @ThueBaoGom,"
                        + " ChuTaiKhoan = @ChuTaiKhoan,"
                        + " SoTaiKhoan = @SoTaiKhoan,"
                        + " NganHang = @NganHang,"
                        + " ChiNhanh = @ChiNhanh,"
                        + " BThanhToanThueA = @BThanhToanThueA,"
                        + " SoLan = @SoLan,"
                        + " TuNgay = @TuNgay,"
                        + " DenNgay = @DenNgay,"
                        + " DauMoi = @DauMoi,"
                        + " Thang = @Thang,"
                        + " ThoiGianThue = @ThoiGianThue,"
                        + " ThoiGianBanGiao = @ThoiGianBanGiao,"
                        + " ThoiGianTinhTien = @ThoiGianTinhTien,"
                        + " ThoaThuanThueKhac = @ThoaThuanThueKhac,"
                        + " GiaBan = @GiaBan,"
                        + " GiaBanBangChu = @GiaBanBangChu,"
                        + " BanBaoGom = @BanBaoGom,"
                        + " BThanhToanBanA = @BThanhToanBanA,"
                        + " BThanhToanBanABangChu = @BThanhToanBanABangChu,"
                        + " BDongChuDauTu = @BDongChuDauTu,"
                        + " BDongChuDauTuBangChu = @BDongChuDauTuBangChu,"
                        + " PhiDichVuC = @PhiDichVuC, PhiDichVuCBangChu = @PhiDichVuCBangChu,"
                        + " NgayCongChung1 = @NgayCongChung1,"
                        + " NgayCongChung2 = @NgayCongChung2,"
                        + " GiaCongChung = @GiaCongChung,"
                        + " ThoaThuanBanKhac = @ThoaThuanBanKhac,"
                        + " Status = @Status,"
                        + " StatusDate = @StatusDate,"
                        + " TicketID = @TicketID,"
                        + " CodeDuAn = @CodeDuAn,"
                        + " CodeCan = @CodeCan,"
                        + " CongTy = @CongTy,"
                        + " MaSoThue = @MaSoThue,"
                        + " DaiDien = @DaiDien,"
                        + " ChucVu = @ChucVu,"
                        + " Approved = @Approved,"
                        + " ApprovedBy = @ApprovedBy,"
                        + " ApprovedDate = @ApprovedDate,"
                        + " Unit = @Unit,"
                        + " ThoiGianKyHD = @ThoiGianKyHD,"
                        + " NgayThueThangDau = @NgayThueThangDau,"
                        + " TienThueThangDau = @TienThueThangDau,"
                        + " SoTienThueThangDau = @SoTienThueThangDau,"
                        + " SoTienThueThangBangChu = @SoTienThueThangBangChu,"
                        + " SoThangThueDau = @SoThangThueDau,"
                        + " ThueTuNgay = @ThueTuNgay,"
                        + " ThueDenNgay = @ThueDenNgay,"
                        + " HotenC = @HotenC,"
                        + " MaNhanVienC = @MaNhanVienC,"
                        + " ChucVuC = @ChucVuC,"
                        + " DiaChiC = @DiaChiC,"
                        + " DiaChiLienHeC = @DiaChiLienHeC,"
                        + " GiayUyQuyenC = @GiayUyQuyenC,"                      
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE TicketKey = @TicketKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                if (_NgayCoc.Year == 0001)
                    zCommand.Parameters.Add("@NgayCoc", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@NgayCoc", SqlDbType.DateTime).Value = _NgayCoc;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.NVarChar).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Department", SqlDbType.NVarChar).Value = _Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@ManagerKey", SqlDbType.NVarChar).Value = _ManagerKey;
                zCommand.Parameters.Add("@ManagerName", SqlDbType.NVarChar).Value = _ManagerName;
                zCommand.Parameters.Add("@CustomerKeyA", SqlDbType.NVarChar).Value = _CustomerKeyA;
                zCommand.Parameters.Add("@HotenA", SqlDbType.NVarChar).Value = _HotenA;
                zCommand.Parameters.Add("@NgaySinhA", SqlDbType.NVarChar).Value = _NgaySinhA;
                zCommand.Parameters.Add("@CMNDA", SqlDbType.NVarChar).Value = _CMNDA;
                zCommand.Parameters.Add("@NoiCapA", SqlDbType.NVarChar).Value = _NoiCapA;
                zCommand.Parameters.Add("@DiaChiLienHeA", SqlDbType.NVarChar).Value = _DiaChiLienHeA;
                zCommand.Parameters.Add("@DiaChiLienLacA", SqlDbType.NVarChar).Value = _DiaChiLienLacA;
                zCommand.Parameters.Add("@DienThoaiA", SqlDbType.NVarChar).Value = _DienThoaiA;
                zCommand.Parameters.Add("@EmailA", SqlDbType.NVarChar).Value = _EmailA;
                zCommand.Parameters.Add("@NgayCapA", SqlDbType.NVarChar).Value = _NgayCapA;
                zCommand.Parameters.Add("@CustomerKeyB", SqlDbType.NVarChar).Value = _CustomerKeyB;
                zCommand.Parameters.Add("@HotenB", SqlDbType.NVarChar).Value = _HotenB;
                zCommand.Parameters.Add("@NgaySinhB", SqlDbType.NVarChar).Value = _NgaySinhB;
                zCommand.Parameters.Add("@CMNDB", SqlDbType.NVarChar).Value = _CMNDB;
                zCommand.Parameters.Add("@NoiCapB", SqlDbType.NVarChar).Value = _NoiCapB;
                zCommand.Parameters.Add("@DiaChiLienHeB", SqlDbType.NVarChar).Value = _DiaChiLienHeB;
                zCommand.Parameters.Add("@DiaChiLienLacB", SqlDbType.NVarChar).Value = _DiaChiLienLacB;
                zCommand.Parameters.Add("@DienThoaiB", SqlDbType.NVarChar).Value = _DienThoaiB;
                zCommand.Parameters.Add("@EmailB", SqlDbType.NVarChar).Value = _EmailB;
                zCommand.Parameters.Add("@NgayCapB", SqlDbType.NVarChar).Value = _NgayCapB;
                zCommand.Parameters.Add("@Duan", SqlDbType.NVarChar).Value = _Duan;
                zCommand.Parameters.Add("@MaCan", SqlDbType.NVarChar).Value = _MaCan;
                zCommand.Parameters.Add("@LoaiCan", SqlDbType.NVarChar).Value = _LoaiCan;
                zCommand.Parameters.Add("@DienTichTimTuong", SqlDbType.NVarChar).Value = _DienTichTimTuong;
                zCommand.Parameters.Add("@DienTichThongThuy", SqlDbType.NVarChar).Value = _DienTichThongThuy;
                zCommand.Parameters.Add("@GiaChoThue", SqlDbType.NVarChar).Value = _GiaChoThue;
                zCommand.Parameters.Add("@GiaChoThueBangChu", SqlDbType.NVarChar).Value = _GiaChoThueBangChu;
                zCommand.Parameters.Add("@ThueBaoGom", SqlDbType.NVarChar).Value = _ThueBaoGom;
                zCommand.Parameters.Add("@ChuTaiKhoan", SqlDbType.NVarChar).Value = _ChuTaiKhoan;
                zCommand.Parameters.Add("@SoTaiKhoan", SqlDbType.NVarChar).Value = _SoTaiKhoan;
                zCommand.Parameters.Add("@NganHang", SqlDbType.NVarChar).Value = _NganHang;
                zCommand.Parameters.Add("@ChiNhanh", SqlDbType.NVarChar).Value = _ChiNhanh;
                zCommand.Parameters.Add("@BThanhToanThueA", SqlDbType.NVarChar).Value = _BThanhToanThueA;
                zCommand.Parameters.Add("@SoLan", SqlDbType.NVarChar).Value = _SoLan;
                zCommand.Parameters.Add("@TuNgay", SqlDbType.NVarChar).Value = _TuNgay;
                zCommand.Parameters.Add("@DenNgay", SqlDbType.NVarChar).Value = _DenNgay;
                zCommand.Parameters.Add("@DauMoi", SqlDbType.NVarChar).Value = _DauMoi;
                zCommand.Parameters.Add("@Thang", SqlDbType.NVarChar).Value = _Thang;
                zCommand.Parameters.Add("@ThoiGianThue", SqlDbType.NVarChar).Value = _ThoiGianThue;
                zCommand.Parameters.Add("@ThoiGianBanGiao", SqlDbType.NVarChar).Value = _ThoiGianBanGiao;
                zCommand.Parameters.Add("@ThoiGianTinhTien", SqlDbType.NVarChar).Value = _ThoiGianTinhTien;
                zCommand.Parameters.Add("@ThoaThuanThueKhac", SqlDbType.NVarChar).Value = _ThoaThuanThueKhac;
                zCommand.Parameters.Add("@GiaBan", SqlDbType.NVarChar).Value = _GiaBan;
                zCommand.Parameters.Add("@GiaBanBangChu", SqlDbType.NVarChar).Value = _GiaBanBangChu;
                zCommand.Parameters.Add("@BanBaoGom", SqlDbType.NVarChar).Value = _BanBaoGom;
                zCommand.Parameters.Add("@BThanhToanBanA", SqlDbType.NVarChar).Value = _BThanhToanBanA;
                zCommand.Parameters.Add("@BThanhToanBanABangChu", SqlDbType.NVarChar).Value = _BThanhToanBanABangChu;
                zCommand.Parameters.Add("@BDongChuDauTu", SqlDbType.NVarChar).Value = _BDongChuDauTu;
                zCommand.Parameters.Add("@BDongChuDauTuBangChu", SqlDbType.NVarChar).Value = _BDongChuDauTuBangChu;
                zCommand.Parameters.Add("@PhiDichVuC", SqlDbType.NVarChar).Value = _PhiDichVuC;
                zCommand.Parameters.Add("@PhiDichVuCBangChu", SqlDbType.NVarChar).Value = _PhiDichVuCBangChu;
                zCommand.Parameters.Add("@NgayCongChung1", SqlDbType.NVarChar).Value = _NgayCongChung1;
                zCommand.Parameters.Add("@NgayCongChung2", SqlDbType.NVarChar).Value = _NgayCongChung2;
                zCommand.Parameters.Add("@GiaCongChung", SqlDbType.NVarChar).Value = _GiaCongChung;
                zCommand.Parameters.Add("@ThoaThuanBanKhac", SqlDbType.NVarChar).Value = _ThoaThuanBanKhac;
                zCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = _Status;
                zCommand.Parameters.Add("@StatusDate", SqlDbType.NVarChar).Value = _StatusDate;
                zCommand.Parameters.Add("@TicketID", SqlDbType.NVarChar).Value = _TicketID;
                zCommand.Parameters.Add("@CodeDuAn", SqlDbType.NVarChar).Value = _CodeDuAn;
                zCommand.Parameters.Add("@CodeCan", SqlDbType.NVarChar).Value = _CodeCan;
                zCommand.Parameters.Add("@CongTy", SqlDbType.NVarChar).Value = _CongTy;
                zCommand.Parameters.Add("@MaSoThue", SqlDbType.NVarChar).Value = _MaSoThue;
                zCommand.Parameters.Add("@DaiDien", SqlDbType.NVarChar).Value = _DaiDien;
                zCommand.Parameters.Add("@ChucVu", SqlDbType.NVarChar).Value = _ChucVu;
                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;
                zCommand.Parameters.Add("@Unit", SqlDbType.NVarChar).Value = _Unit;
                zCommand.Parameters.Add("@ThoiGianKyHD", SqlDbType.NVarChar).Value = _ThoiGianKyHD;
                zCommand.Parameters.Add("@NgayThueThangDau", SqlDbType.NVarChar).Value = _NgayThueThangDau;
                zCommand.Parameters.Add("@TienThueThangDau", SqlDbType.NVarChar).Value = _TienThueThangDau;
                zCommand.Parameters.Add("@SoTienThueThangDau", SqlDbType.NVarChar).Value = _SoTienThueThangDau;
                zCommand.Parameters.Add("@SoTienThueThangBangChu", SqlDbType.NVarChar).Value = _SoTienThueThangBangChu;
                zCommand.Parameters.Add("@SoThangThueDau", SqlDbType.NVarChar).Value = _SoThangThueDau;
                zCommand.Parameters.Add("@ThueTuNgay", SqlDbType.NVarChar).Value = _ThueTuNgay;
                zCommand.Parameters.Add("@ThueDenNgay", SqlDbType.NVarChar).Value = _ThueDenNgay;
                zCommand.Parameters.Add("@HotenC", SqlDbType.NVarChar).Value = _HotenC;
                zCommand.Parameters.Add("@MaNhanVienC", SqlDbType.NVarChar).Value = _MaNhanVienC;
                zCommand.Parameters.Add("@ChucVuC", SqlDbType.NVarChar).Value = _ChucVuC;
                zCommand.Parameters.Add("@DiaChiC", SqlDbType.NVarChar).Value = _DiaChiC;
                zCommand.Parameters.Add("@DiaChiLienHeC", SqlDbType.NVarChar).Value = _DiaChiLienHeC;
                zCommand.Parameters.Add("@GiayUyQuyenC", SqlDbType.NVarChar).Value = _GiayUyQuyenC;             
              
             
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;             
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_TicketKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Ticket WHERE TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Ticket SET Approved = 1, ApprovedBy = @ApprovedBy, ApprovedDate = GETDATE() WHERE TicketKey = @TicketKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TicketKey", SqlDbType.Int).Value = _TicketKey;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
