﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.HRM
{
    public class Articles_Info
    {
        #region [ Field Name ]
        private int _ArticleKey = 0;
        private string _ArticleID = "";
        private DateTime _ArticleDate;
        private string _ArticleName = "";
        private string _Summarize = "";
        private string _ArticleContent = "";
        private int _CategoryKey = 0;
        private string _Url = "";
        private string _ImageSmall = "";
        private string _ImageLarge = "";
        private bool _Publish;
        private bool _Hot;
        private int _Rank = 0;
        private int _Hits = 0;
        private int _CompanyKey = 0;
        private int _ArticleTyle = 0;
        private DateTime _CreatedDateTime;
        private string _CreatedBy = "";
        private DateTime _ModifiedDateTime;
        private string _ModifiedBy = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int ArticleKey
        {
            get { return _ArticleKey; }
            set { _ArticleKey = value; }
        }
        public string ArticleID
        {
            get { return _ArticleID; }
            set { _ArticleID = value; }
        }
        public DateTime ArticleDate
        {
            get { return _ArticleDate; }
            set { _ArticleDate = value; }
        }
        public string ArticleName
        {
            get { return _ArticleName; }
            set { _ArticleName = value; }
        }
        public string Summarize
        {
            get { return _Summarize; }
            set { _Summarize = value; }
        }
        public string ArticleContent
        {
            get { return _ArticleContent; }
            set { _ArticleContent = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
        public string ImageSmall
        {
            get { return _ImageSmall; }
            set { _ImageSmall = value; }
        }
        public string ImageLarge
        {
            get { return _ImageLarge; }
            set { _ImageLarge = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public bool Hot
        {
            get { return _Hot; }
            set { _Hot = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Hits
        {
            get { return _Hits; }
            set { _Hits = value; }
        }
        public int CompanyKey
        {
            get { return _CompanyKey; }
            set { _CompanyKey = value; }
        }
        public int ArticleTyle
        {
            get { return _ArticleTyle; }
            set { _ArticleTyle = value; }
        }
        public DateTime CreatedDateTime
        {
            get { return _CreatedDateTime; }
            set { _CreatedDateTime = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime ModifiedDateTime
        {
            get { return _ModifiedDateTime; }
            set { _ModifiedDateTime = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Articles_Info()
        {
        }
        public Articles_Info(int ArticleKey)
        {
            string zSQL = "SELECT * FROM HRM_Articles WHERE ArticleKey = @ArticleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.Int).Value = ArticleKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ArticleKey"] != DBNull.Value)
                        _ArticleKey = int.Parse(zReader["ArticleKey"].ToString());
                    _ArticleID = zReader["ArticleID"].ToString();
                    if (zReader["ArticleDate"] != DBNull.Value)
                        _ArticleDate = (DateTime)zReader["ArticleDate"];
                    _ArticleName = zReader["ArticleName"].ToString();
                    _Summarize = zReader["Summarize"].ToString();
                    _ArticleContent = zReader["ArticleContent"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _Url = zReader["Url"].ToString();
                    _ImageSmall = zReader["ImageSmall"].ToString();
                    _ImageLarge = zReader["ImageLarge"].ToString();
                    if (zReader["Publish"] != DBNull.Value)
                        _Publish = (bool)zReader["Publish"];
                    if (zReader["Hot"] != DBNull.Value)
                        _Hot = (bool)zReader["Hot"];
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Hits"] != DBNull.Value)
                        _Hits = int.Parse(zReader["Hits"].ToString());
                    if (zReader["CompanyKey"] != DBNull.Value)
                        _CompanyKey = int.Parse(zReader["CompanyKey"].ToString());
                    if (zReader["ArticleTyle"] != DBNull.Value)
                        _ArticleTyle = int.Parse(zReader["ArticleTyle"].ToString());
                    if (zReader["CreatedDateTime"] != DBNull.Value)
                        _CreatedDateTime = (DateTime)zReader["CreatedDateTime"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["ModifiedDateTime"] != DBNull.Value)
                        _ModifiedDateTime = (DateTime)zReader["ModifiedDateTime"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Articles ("
        + " ArticleID ,ArticleDate ,ArticleName ,Summarize ,ArticleContent ,CategoryKey ,Url ,ImageSmall ,ImageLarge ,Publish ,Hot ,Rank ,Hits ,CompanyKey ,ArticleTyle ,CreatedDateTime ,CreatedBy ,ModifiedDateTime ,ModifiedBy ) "
         + " VALUES ( "
         + "@ArticleID ,@ArticleDate ,@ArticleName ,@Summarize ,@ArticleContent ,@CategoryKey ,@Url ,@ImageSmall ,@ImageLarge ,@Publish ,@Hot ,@Rank ,@Hits ,@CompanyKey ,@ArticleTyle ,@CreatedDateTime ,@CreatedBy ,@ModifiedDateTime ,@ModifiedBy ) "
         + " SELECT ArticleKey FROM HRM_Articles WHERE ArticleKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.Int).Value = _ArticleKey;
                zCommand.Parameters.Add("@ArticleID", SqlDbType.NChar).Value = _ArticleID;
                if (_ArticleDate.Year == 0001)
                    zCommand.Parameters.Add("@ArticleDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ArticleDate", SqlDbType.DateTime).Value = _ArticleDate;
                zCommand.Parameters.Add("@ArticleName", SqlDbType.NVarChar).Value = _ArticleName;
                zCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = _Summarize;
                zCommand.Parameters.Add("@ArticleContent", SqlDbType.NVarChar).Value = _ArticleContent;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Url", SqlDbType.NVarChar).Value = _Url;
                zCommand.Parameters.Add("@ImageSmall", SqlDbType.NVarChar).Value = _ImageSmall;
                zCommand.Parameters.Add("@ImageLarge", SqlDbType.NVarChar).Value = _ImageLarge;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = _Publish;
                zCommand.Parameters.Add("@Hot", SqlDbType.Bit).Value = _Hot;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Hits", SqlDbType.Int).Value = _Hits;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.Int).Value = _CompanyKey;
                zCommand.Parameters.Add("@ArticleTyle", SqlDbType.Int).Value = _ArticleTyle;
                if (_CreatedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = _CreatedDateTime;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                if (_ModifiedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = _ModifiedDateTime;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                _ArticleKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE HRM_Articles SET "
                        + " ArticleID = @ArticleID,"
                        + " ArticleDate = @ArticleDate,"
                        + " ArticleName = @ArticleName,"
                        + " Summarize = @Summarize,"
                        + " ArticleContent = @ArticleContent,"
                        + " CategoryKey = @CategoryKey,"
                        + " Url = @Url,"
                        + " ImageSmall = @ImageSmall,"
                        + " ImageLarge = @ImageLarge,"
                        + " Publish = @Publish,"
                        + " Hot = @Hot,"
                        + " Rank = @Rank,"
                        + " Hits = @Hits,"
                        + " CompanyKey = @CompanyKey,"
                        + " ArticleTyle = @ArticleTyle,"
                        + " CreatedDateTime = @CreatedDateTime,"
                        + " CreatedBy = @CreatedBy,"
                        + " ModifiedDateTime = @ModifiedDateTime,"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE ArticleKey = @ArticleKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.Int).Value = _ArticleKey;
                zCommand.Parameters.Add("@ArticleID", SqlDbType.NChar).Value = _ArticleID;
                if (_ArticleDate.Year == 0001)
                    zCommand.Parameters.Add("@ArticleDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ArticleDate", SqlDbType.DateTime).Value = _ArticleDate;
                zCommand.Parameters.Add("@ArticleName", SqlDbType.NVarChar).Value = _ArticleName;
                zCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = _Summarize;
                zCommand.Parameters.Add("@ArticleContent", SqlDbType.NVarChar).Value = _ArticleContent;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Url", SqlDbType.NVarChar).Value = _Url;
                zCommand.Parameters.Add("@ImageSmall", SqlDbType.NVarChar).Value = _ImageSmall;
                zCommand.Parameters.Add("@ImageLarge", SqlDbType.NVarChar).Value = _ImageLarge;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = _Publish;
                zCommand.Parameters.Add("@Hot", SqlDbType.Bit).Value = _Hot;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Hits", SqlDbType.Int).Value = _Hits;
                zCommand.Parameters.Add("@CompanyKey", SqlDbType.Int).Value = _CompanyKey;
                zCommand.Parameters.Add("@ArticleTyle", SqlDbType.Int).Value = _ArticleTyle;
                if (_CreatedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = _CreatedDateTime;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                if (_ModifiedDateTime.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = _ModifiedDateTime;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_ArticleKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Articles WHERE ArticleKey = @ArticleKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.Int).Value = _ArticleKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
