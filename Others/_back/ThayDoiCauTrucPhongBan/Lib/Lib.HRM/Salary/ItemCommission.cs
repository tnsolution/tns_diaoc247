﻿namespace Lib.HRM
{
    public class ItemCommission
    {
        public int AutoKey { get; set; } = 0;
        public int CommisionKey { get; set; } = 0;
        public int PositionKey { get; set; } = 0;
        public int TradeCategory { get; set; } = 0;
        public string Money { get; set; } = "0";
        public string Number { get; set; } = "0";
        public int TradeType { get; set; } = 0;
        public int Method { get; set; } = 0;
        public string Description { get; set; } = "";
        public string Name { get; set; } = "";
        public int EmployeeKey { get; set; } = 0;
    }
}
