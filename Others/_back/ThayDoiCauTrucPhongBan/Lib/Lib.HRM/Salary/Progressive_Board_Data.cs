﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Progressive_Board_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Progressive_Board ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Key)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Progressive_Board WHERE EmployeeKey = @Key ORDER BY ToMoney";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = Key;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Key, int Category)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM HRM_Progressive_Board WHERE EmployeeKey = @Key AND Category = @Category ORDER BY ToMoney";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = Key;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }        
    }
}
