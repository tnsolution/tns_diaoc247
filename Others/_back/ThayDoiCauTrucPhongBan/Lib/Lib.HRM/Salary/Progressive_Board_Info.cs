﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.HRM
{
    public class Progressive_Board_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _EmployeeKey = 0;
        private double _FromMoney = 0;
        private double _ToMoney = 0;
        private int _Rate = 0;
        private int _Rank = 0;
        private string _Description = "";
        private int _Category = 0;
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Progressive_Board_Info()
        {
        }
        public Progressive_Board_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Progressive_Board WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["FromMoney"] != DBNull.Value)
                        _FromMoney = double.Parse(zReader["FromMoney"].ToString());
                    if (zReader["ToMoney"] != DBNull.Value)
                        _ToMoney = double.Parse(zReader["ToMoney"].ToString());
                    if (zReader["Rate"] != DBNull.Value)
                        _Rate = int.Parse(zReader["Rate"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public double FromMoney
        {
            get { return _FromMoney; }
            set { _FromMoney = value; }
        }
        public double ToMoney
        {
            get { return _ToMoney; }
            set { _ToMoney = value; }
        }
        public int Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int Category
        {
            get
            {
                return _Category;
            }

            set
            {
                _Category = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Progressive_Board ("
        + " EmployeeKey ,FromMoney ,ToMoney ,Rate ,Rank ,Description,Category ,ModifiedDate ,ModifiedBy ) "
         + " VALUES ( "
         + "@EmployeeKey ,@FromMoney ,@ToMoney ,@Rate ,@Rank ,@Description,@Category ,@ModifiedDate ,@ModifiedBy ) "
         + " SELECT AutoKey FROM HRM_Progressive_Board WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@FromMoney", SqlDbType.Money).Value = _FromMoney;
                zCommand.Parameters.Add("@ToMoney", SqlDbType.Money).Value = _ToMoney;
                zCommand.Parameters.Add("@Rate", SqlDbType.Int).Value = _Rate;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE HRM_Progressive_Board SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " FromMoney = @FromMoney,"
                        + " ToMoney = @ToMoney,"
                        + " Rate = @Rate,"
                        + " Rank = @Rank,"
                        + " Description = @Description, Category =@Category,"
                        + " ModifiedDate = @ModifiedDate,"
                        + " ModifiedBy = @ModifiedBy"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@FromMoney", SqlDbType.Money).Value = _FromMoney;
                zCommand.Parameters.Add("@ToMoney", SqlDbType.Money).Value = _ToMoney;
                zCommand.Parameters.Add("@Rate", SqlDbType.Int).Value = _Rate;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Progressive_Board WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
