﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.HRM
{
    public class Salary_Data
    {
        public static double SumSalary(int Employee, int Year)
        {
            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return zSql.GetObject(@"SELECT ISNULL(SUM(A.Param10),0) FROM HRM_Salary A WHERE EmployeeKey = @Employee AND YEAR(A.FromDate) = @Year").ToDouble();
        }        

        public static double SumSalary(int Employee, DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            string SQL = "SELECT ISNULL(SUM(A.Param10),0) FROM HRM_Salary A WHERE 1 = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                SQL += " AND A.FromDate BETWEEN @FromDate AND @ToDate";
            if (Employee != 0)
                SQL += " AND A.EmployeeKey = @Employee";
            return zSql.GetObject(SQL).ToDouble();
        }
        public static double SumSalary(int Employee, int Department, DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSql = new SqlContext();
            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            zSql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            string SQL = "SELECT ISNULL(SUM(A.Param10),0) FROM HRM_Salary A WHERE 1 = 1";
            if (Department != 0)
                SQL += " AND A.DepartmentKey=@Department";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                SQL += " AND A.FromDate BETWEEN @FromDate AND @ToDate";
            if (Employee != 0)
                SQL += " AND A.EmployeeKey = @Employee";
            return zSql.GetObject(SQL).ToDouble();
        }
               
        public static DataTable CommissionShortTerm1(DateTime FromDate, DateTime ToDate, int DepartmentKey, int EmployeeKey)
        {
            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SQL.CMD.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
            SQL.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            return SQL.GetData(@"
SELECT B.TransactionKey, B.AssetID, B.Income, 'HT' AS [Type], 0 AS InternalCost, B.Source 
FROM FNC_Trade_Temp A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey = B.TransactionKey
WHERE B.Source = 1 
AND B.IsApproved = 1 
AND B.IsFinish = 1 
AND B.EmployeeKey <> @EmployeeKey
AND B.DepartmentKey = @DepartmentKey
AND B.FinishDate BETWEEN @FromDate AND @ToDate ");
        }

        public static DataTable CommissionShortTerm2(DateTime FromDate, DateTime ToDate, int DepartmentKey, int EmployeeKey)
        {
            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SQL.CMD.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
            SQL.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            return SQL.GetData(@"
SELECT B.TransactionKey, B.AssetID, B.Income, 'HT' AS [Type], 0 AS InternalCost, B.Source 
FROM FNC_Trade_Temp A 
LEFT JOIN FNC_Transaction B ON A.TransactionKey = B.TransactionKey
WHERE B.Source = 2
AND B.IsApproved = 1 
AND B.IsFinish = 1
AND B.EmployeeKey <> @EmployeeKey
AND B.DepartmentKey = @DepartmentKey
AND B.FinishDate BETWEEN @FromDate AND @ToDate ");
        }
        public static DataTable List(DateTime FromDate, DateTime ToDate, int EmployeeKey, int DepartmentKey)
        {
            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SQL.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            SQL.CMD.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;

            string Query = @"
SELECT A.AutoKey, A.EmployeeKey, A.DepartmentKey, 
A.FromDate, A.ToDate, A.Param11, A.Param12, 
A.Param11 + A.Param12 AS FINAL,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS DepartmentName,
CASE dbo.FNC_CheckSentSalary(A.EmployeeKey,A.AutoKey)
WHEN 0 THEN 0
ELSE 1 END AS [Status] ,
dbo.FNC_GetStepSalary(A.AutoKey) AS [Current],
dbo.FNC_GetStepSalaryStatus(A.AutoKey) AS [CurrentStatus]
FROM HRM_Salary A
WHERE 1=1 ";
            if (DepartmentKey != 0)
                Query += " AND A.DepartmentKey=@DepartmentKey";
            if (EmployeeKey != 0)
                Query += " AND A.EmployeeKey=@EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                Query += " AND A.FromDate BETWEEN @FromDate AND @ToDate";
            Query += " ORDER BY DepartmentKey";
            return SQL.GetData(Query);
        }
        public static DataTable ListSent(DateTime FromDate, DateTime ToDate, int Employee)
        {
            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SQL.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;

            string Query = @"
SELECT A.AutoKey, A.EmployeeKey, A.DepartmentKey, 
A.FromDate, A.ToDate, A.Param11, A.Param12, 
A.Param11 + A.Param12 AS FINAL,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS DepartmentName,
dbo.FNC_GetStepSalary(A.AutoKey) AS [Current],
dbo.FNC_GetStepSalaryStatus(A.AutoKey) AS [CurrentStatus]
FROM HRM_Salary A
WHERE dbo.FNC_CheckSentSalary(A.EmployeeKey,A.AutoKey) = 1";
            if (Employee != 0)
                Query += " AND A.EmployeeKey=@EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                Query += " AND A.FromDate BETWEEN @FromDate AND @ToDate";
            Query += " ORDER BY DepartmentKey";
            return SQL.GetData(Query);
        }

        public static DataTable List(DateTime FromDate, DateTime ToDate, int EmployeeKey)
        {
            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SQL.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;

            string Query = @"SELECT A.AutoKey, A.EmployeeKey, A.DepartmentKey, 
A.FromDate, A.ToDate, A.Param11, A.Param12, 
A.Param11 + A.Param12 AS FINAL,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS DepartmentName 
FROM HRM_Salary A
WHERE 1=1 ";
            if (EmployeeKey != 0)
                Query += " AND A.EmployeeKey=@EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                Query += " AND A.FromDate BETWEEN @FromDate AND @ToDate";
            Query += " ORDER BY DepartmentKey";
            return SQL.GetData(Query);
        }
        public static DataTable List(DateTime FromDate, DateTime ToDate)
        {
            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

            return SQL.GetData(@"
SELECT A.AutoKey, A.EmployeeKey, A.DepartmentKey, 
A.FromDate, A.ToDate, A.Param11, A.Param12, 
A.Param11 + A.Param12 AS FINAL,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetDepartmentName(A.DepartmentKey) AS DepartmentName, 
CASE dbo.FNC_CheckSentSalary(A.EmployeeKey,A.AutoKey)
WHEN 0 THEN 0
ELSE 1 END AS [Status] ,
dbo.FNC_GetStepSalary(A.AutoKey) AS [Current],
dbo.FNC_GetStepSalaryStatus(A.AutoKey) AS [CurrentStatus]
FROM HRM_Salary A
WHERE A.FromDate BETWEEN @FromDate AND @ToDate
ORDER BY DepartmentKey");
        }

        public static DataTable GetTable(int ParentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Salary_Detail WHERE ParentKey=@ParentKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = ParentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //lay giao dich dể tinh doanh thu và hoa hong ho tro (phi hoa hong khong tinh ngan hạn)
        public static DataTable GetCommission(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionKey, A.AssetID, A.Income, 0 AS [Money], 'TT' AS [Type], A.InternalCost, A.Source, '' AS CommissionName
FROM FNC_Transaction A 
WHERE A.DepartmentKey = @DepartmentKey
AND A.EmployeeKey = @EmployeeKey
AND A.IsApproved = 1 
AND A.IsFinish = 1 
AND A.FinishDate BETWEEN @FromDate AND @ToDate
UNION ALL
SELECT B.TransactionKey, B.AssetID, B.Income, A.[Money], 'HT' AS [Type], 0 AS InternalCost, B.Source, dbo.FNC_GetNameCommission(A.CommisionKey) AS CommissionName
FROM FNC_Transaction_Commision A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.DepartmentKey = @DepartmentKey
AND A.EmployeeKey = @EmployeeKey
AND B.ShortTerm <> 1
AND B.IsApproved = 1 
AND B.IsFinish = 1 
AND B.FinishDate BETWEEN @FromDate AND @ToDate ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable GetCommissionTrade(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.TransactionKey, A.AssetID, A.Income, 0 AS [Money], 'TT' AS [Type], A.InternalCost, A.Source
FROM FNC_Transaction A 
WHERE A.DepartmentKey = @DepartmentKey
AND A.EmployeeKey = @EmployeeKey
AND A.IsApproved = 1 
AND A.IsFinish = 1 
AND A.FinishDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetCommissionSupport(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT B.TransactionKey, B.AssetID, B.Income, A.[Money], 'HT' AS [Type], 0 AS InternalCost, B.Source
FROM FNC_Transaction_Commision A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.DepartmentKey = @DepartmentKey
AND A.EmployeeKey = @EmployeeKey
AND B.ShortTerm <> 1
AND B.IsApproved = 1 
AND B.IsFinish = 1 
AND B.FinishDate BETWEEN @FromDate AND @ToDate ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}

/*
 SELECT A.TransactionKey, A.AssetID, A.Income, 0 AS [Money], 'TT' AS [Type], A.InternalCost, A.Source
FROM FNC_Transaction A 
WHERE A.DepartmentKey = @DepartmentKey
AND A.EmployeeKey = @EmployeeKey
AND IsApproved = 1 AND IsFinish = 1 
AND A.FinishDate BETWEEN @FromDate AND @ToDate
UNION ALL
SELECT B.TransactionKey, B.AssetID, B.Income, A.[Money], 'HT' AS [Type], 0 AS InternalCost, B.Source
FROM FNC_Transaction_Commision A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.DepartmentKey = @DepartmentKey
AND B.ShortTerm <> 1
AND A.EmployeeKey = @EmployeeKey
AND B.IsApproved = 1 AND B.IsFinish = 1 
AND B.FinishDate BETWEEN @FromDate AND @ToDate 
     */
