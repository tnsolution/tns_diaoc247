﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;

namespace Lib.HRM
{
    public class Salary_Info
    {
        #region [ Field Name ]
        private string _EmployeeName = "";
        private double _ParamTotal1 = 0;
        private double _ParamTotal2 = 0;
        private double _ParamTotal3 = 0;
        private double _ParamTotal4 = 0;
        private double _ParamTotal5 = 0;
        private double _ParamTotal6 = 0;
        private int _AutoKey = 0;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private string _Description = "";
        private string _Step = "";
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private double _Param1 = 0;
        private double _Param2 = 0;
        private double _Param3 = 0;
        private double _Param4 = 0;
        private float _Param5 = 0;
        private double _Param51 = 0;
        private double _Param6 = 0;
        private double _Param7 = 0;
        private double _Param8 = 0;
        private double _Param9 = 0;
        private double _Param10 = 0;
        private double _Param11 = 0;
        private double _Param12 = 0;
        private double _Param13 = 0;
        private double _Param14 = 0;
        private double _Param15 = 0;
        private double _Param16 = 0;

        private string _Note1 = "";
        private string _Note2 = "";
        private int _Tag = 0;// dung set cho chuyển lương lần 1 lần 2

        private int _Approved = 0;
        private int _ApprovedBy = 0;
        private DateTime _ApprovedDate;
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public double Param1
        {
            get { return _Param1; }
            set { _Param1 = value; }
        }
        public double Param2
        {
            get { return _Param2; }
            set { _Param2 = value; }
        }
        public double Param3
        {
            get { return _Param3; }
            set { _Param3 = value; }
        }
        public double Param4
        {
            get { return _Param4; }
            set { _Param4 = value; }
        }
        public float Param5
        {
            get { return _Param5; }
            set { _Param5 = value; }
        }
        public double Param51
        {
            get { return _Param51; }
            set { _Param51 = value; }
        }
        public double Param6
        {
            get { return _Param6; }
            set { _Param6 = value; }
        }
        public double Param7
        {
            get { return _Param7; }
            set { _Param7 = value; }
        }
        public double Param8
        {
            get { return _Param8; }
            set { _Param8 = value; }
        }
        public double Param9
        {
            get { return _Param9; }
            set { _Param9 = value; }
        }
        public double Param10
        {
            get { return _Param10; }
            set { _Param10 = value; }
        }
        public double Param11
        {
            get { return _Param11; }
            set { _Param11 = value; }
        }
        public double Param12
        {
            get { return _Param12; }
            set { _Param12 = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public double ParamTotal1
        {
            get
            {
                return _ParamTotal1;
            }

            set
            {
                _ParamTotal1 = value;
            }
        }

        public double ParamTotal2
        {
            get
            {
                return _ParamTotal2;
            }

            set
            {
                _ParamTotal2 = value;
            }
        }

        public double ParamTotal3
        {
            get
            {
                return _ParamTotal3;
            }

            set
            {
                _ParamTotal3 = value;
            }
        }

        public double ParamTotal4
        {
            get
            {
                return _ParamTotal4;
            }

            set
            {
                _ParamTotal4 = value;
            }
        }

        public double ParamTotal5
        {
            get
            {
                return _ParamTotal5;
            }

            set
            {
                _ParamTotal5 = value;
            }
        }

        public double ParamTotal6
        {
            get
            {
                return _ParamTotal6;
            }

            set
            {
                _ParamTotal6 = value;
            }
        }

        public DateTime ApprovedDate { get => _ApprovedDate; set => _ApprovedDate = value; }
        public int ApprovedBy { get => _ApprovedBy; set => _ApprovedBy = value; }
        public int Approved { get => _Approved; set => _Approved = value; }
        public string Step { get => _Step; set => _Step = value; }
        public string EmployeeName { get => _EmployeeName; set => _EmployeeName = value; }
        public double Param13 { get => Param131; set => Param131 = value; }
        public double Param14 { get => Param141; set => Param141 = value; }
        public double Param15 { get => _Param15; set => _Param15 = value; }
        public double Param16 { get => _Param16; set => _Param16 = value; }
        public double Param131 { get => _Param13; set => _Param13 = value; }
        public double Param141 { get => _Param14; set => _Param14 = value; }
        public int Tag { get => _Tag; set => _Tag = value; }
        public string Note1 { get => _Note1; set => _Note1 = value; }
        public string Note2 { get => _Note2; set => _Note2 = value; }
        #endregion

        #region [ Constructor Get Information ]
        public Salary_Info()
        {
        }
        public Salary_Info(int AutoKey)
        {
            string zSQL = "SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM HRM_Salary WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _Note2 = zReader["Note2"].ToString();
                    _Note1 = zReader["Note1"].ToString();
                    if (zReader["Tag"] != DBNull.Value)
                        _Tag = zReader["Tag"].ToInt();
                    if (zReader["Param13"] != DBNull.Value)
                        _Param13 = double.Parse(zReader["Param13"].ToString());
                    if (zReader["Param14"] != DBNull.Value)
                        _Param14 = double.Parse(zReader["Param14"].ToString());
                    if (zReader["ParamTotal1"] != DBNull.Value)
                        _ParamTotal1 = double.Parse(zReader["ParamTotal1"].ToString());
                    if (zReader["ParamTotal2"] != DBNull.Value)
                        _ParamTotal2 = double.Parse(zReader["ParamTotal2"].ToString());
                    if (zReader["ParamTotal3"] != DBNull.Value)
                        _ParamTotal3 = double.Parse(zReader["ParamTotal3"].ToString());
                    if (zReader["ParamTotal2"] != DBNull.Value)
                        _ParamTotal2 = double.Parse(zReader["ParamTotal2"].ToString());
                    if (zReader["ParamTotal5"] != DBNull.Value)
                        _ParamTotal5 = double.Parse(zReader["ParamTotal5"].ToString());
                    if (zReader["ParamTotal6"] != DBNull.Value)
                        _ParamTotal6 = double.Parse(zReader["ParamTotal6"].ToString());
                    if (zReader["Param1"] != DBNull.Value)
                        _Param1 = double.Parse(zReader["Param1"].ToString());
                    if (zReader["Param2"] != DBNull.Value)
                        _Param2 = double.Parse(zReader["Param2"].ToString());
                    if (zReader["Param3"] != DBNull.Value)
                        _Param3 = double.Parse(zReader["Param3"].ToString());
                    if (zReader["Param4"] != DBNull.Value)
                        _Param4 = double.Parse(zReader["Param4"].ToString());
                    if (zReader["Param5"] != DBNull.Value)
                        _Param5 = float.Parse(zReader["Param5"].ToString());
                    if (zReader["Param51"] != DBNull.Value)
                        _Param51 = double.Parse(zReader["Param51"].ToString());
                    if (zReader["Param6"] != DBNull.Value)
                        _Param6 = double.Parse(zReader["Param6"].ToString());
                    if (zReader["Param7"] != DBNull.Value)
                        _Param7 = double.Parse(zReader["Param7"].ToString());
                    if (zReader["Param8"] != DBNull.Value)
                        _Param8 = double.Parse(zReader["Param8"].ToString());
                    if (zReader["Param9"] != DBNull.Value)
                        _Param9 = double.Parse(zReader["Param9"].ToString());
                    if (zReader["Param10"] != DBNull.Value)
                        _Param10 = double.Parse(zReader["Param10"].ToString());
                    if (zReader["Param11"] != DBNull.Value)
                        _Param11 = double.Parse(zReader["Param11"].ToString());
                    if (zReader["Param12"] != DBNull.Value)
                        _Param12 = double.Parse(zReader["Param12"].ToString());
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();

                    _Step = zReader["Step"].ToString();
                    if (zReader["Approved"] != DBNull.Value)
                        _Approved = int.Parse(zReader["Approved"].ToString());
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["ApprovedDate"] != DBNull.Value)
                        _ApprovedDate = (DateTime)zReader["ApprovedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Salary_Info(int Employee, int Month, int Year)
        {
            string zSQL = "SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM HRM_Salary WHERE EmployeeKey = @EmployeeKey AND MONTH(FromDate)=@Month AND YEAR(ToDate)=@Year";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeName = zReader["EmployeeName"].ToString();

                    if (zReader["ParamTotal1"] != DBNull.Value)
                        _ParamTotal1 = double.Parse(zReader["ParamTotal1"].ToString());
                    if (zReader["ParamTotal2"] != DBNull.Value)
                        _ParamTotal2 = double.Parse(zReader["ParamTotal2"].ToString());
                    if (zReader["ParamTotal3"] != DBNull.Value)
                        _ParamTotal3 = double.Parse(zReader["ParamTotal3"].ToString());
                    if (zReader["ParamTotal2"] != DBNull.Value)
                        _ParamTotal2 = double.Parse(zReader["ParamTotal2"].ToString());
                    if (zReader["ParamTotal5"] != DBNull.Value)
                        _ParamTotal5 = double.Parse(zReader["ParamTotal5"].ToString());
                    if (zReader["ParamTotal6"] != DBNull.Value)
                        _ParamTotal6 = double.Parse(zReader["ParamTotal6"].ToString());

                    if (zReader["Param1"] != DBNull.Value)
                        _Param1 = double.Parse(zReader["Param1"].ToString());
                    if (zReader["Param2"] != DBNull.Value)
                        _Param2 = double.Parse(zReader["Param2"].ToString());
                    if (zReader["Param3"] != DBNull.Value)
                        _Param3 = double.Parse(zReader["Param3"].ToString());
                    if (zReader["Param4"] != DBNull.Value)
                        _Param4 = double.Parse(zReader["Param4"].ToString());
                    if (zReader["Param5"] != DBNull.Value)
                        _Param5 = float.Parse(zReader["Param5"].ToString());
                    if (zReader["Param51"] != DBNull.Value)
                        _Param51 = double.Parse(zReader["Param51"].ToString());
                    if (zReader["Param6"] != DBNull.Value)
                        _Param6 = double.Parse(zReader["Param6"].ToString());
                    if (zReader["Param7"] != DBNull.Value)
                        _Param7 = double.Parse(zReader["Param7"].ToString());
                    if (zReader["Param8"] != DBNull.Value)
                        _Param8 = double.Parse(zReader["Param8"].ToString());
                    if (zReader["Param9"] != DBNull.Value)
                        _Param9 = double.Parse(zReader["Param9"].ToString());
                    if (zReader["Param10"] != DBNull.Value)
                        _Param10 = double.Parse(zReader["Param10"].ToString());
                    if (zReader["Param11"] != DBNull.Value)
                        _Param11 = double.Parse(zReader["Param11"].ToString());
                    if (zReader["Param12"] != DBNull.Value)
                        _Param12 = double.Parse(zReader["Param12"].ToString());
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"
INSERT INTO HRM_Salary (
EmployeeKey ,DepartmentKey ,FromDate ,ToDate ,Description ,
Param1 ,Param2 ,Param3 ,Param4 ,Param5 ,Param51 ,Param6 ,Param7 ,Param8 ,Param9 ,Param10 ,Param11 ,Param12, Param13,Param14,Param15,Param16,
ParamTotal1 ,ParamTotal2 ,ParamTotal3 ,ParamTotal4 ,ParamTotal5 ,ParamTotal6, Note1, Note2, Tag,
Approved, ApprovedBy,ApprovedDate,Step,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName )
VALUES (
@EmployeeKey ,@DepartmentKey ,@FromDate ,@ToDate ,@Description,
@Param1 ,@Param2 ,@Param3 ,@Param4 ,@Param5 ,@Param51 ,@Param6 ,@Param7 ,@Param8 ,@Param9 ,@Param10 ,@Param11 ,@Param12 , @Param13,@Param14,@Param15,@Param16,
@ParamTotal1 ,@ParamTotal2 ,@ParamTotal3 ,@ParamTotal4 ,@ParamTotal5 ,@ParamTotal6, @Note1, @Note2, @Tag,
@Approved, @ApprovedBy,@ApprovedDate,@Step,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName )
SELECT AutoKey FROM HRM_Salary WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@Step", SqlDbType.NVarChar).Value = _Step;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;

                zCommand.Parameters.Add("@Param13", SqlDbType.Decimal).Value = Param131;
                zCommand.Parameters.Add("@Param14", SqlDbType.Decimal).Value = Param141;
                zCommand.Parameters.Add("@Param15", SqlDbType.Decimal).Value = _Param15;
                zCommand.Parameters.Add("@Param16", SqlDbType.Decimal).Value = _Param16;
                zCommand.Parameters.Add("@Note1", SqlDbType.NVarChar).Value = _Note1;
                zCommand.Parameters.Add("@Note2", SqlDbType.NVarChar).Value = _Note2;
                zCommand.Parameters.Add("@Tag", SqlDbType.Int).Value = Tag;

                zCommand.Parameters.Add("@ParamTotal1", SqlDbType.Decimal).Value = _ParamTotal1;
                zCommand.Parameters.Add("@ParamTotal2", SqlDbType.Decimal).Value = _ParamTotal2;
                zCommand.Parameters.Add("@ParamTotal3", SqlDbType.Decimal).Value = _ParamTotal3;
                zCommand.Parameters.Add("@ParamTotal4", SqlDbType.Decimal).Value = _ParamTotal4;
                zCommand.Parameters.Add("@ParamTotal5", SqlDbType.Decimal).Value = _ParamTotal5;
                zCommand.Parameters.Add("@ParamTotal6", SqlDbType.Decimal).Value = _ParamTotal6;

                zCommand.Parameters.Add("@Param1", SqlDbType.Decimal).Value = _Param1;
                zCommand.Parameters.Add("@Param2", SqlDbType.Decimal).Value = _Param2;
                zCommand.Parameters.Add("@Param3", SqlDbType.Decimal).Value = _Param3;
                zCommand.Parameters.Add("@Param4", SqlDbType.Decimal).Value = _Param4;
                zCommand.Parameters.Add("@Param5", SqlDbType.Decimal).Value = _Param5;
                zCommand.Parameters.Add("@Param51", SqlDbType.Decimal).Value = _Param51;
                zCommand.Parameters.Add("@Param6", SqlDbType.Decimal).Value = _Param6;
                zCommand.Parameters.Add("@Param7", SqlDbType.Decimal).Value = _Param7;
                zCommand.Parameters.Add("@Param8", SqlDbType.Decimal).Value = _Param8;
                zCommand.Parameters.Add("@Param9", SqlDbType.Decimal).Value = _Param9;
                zCommand.Parameters.Add("@Param10", SqlDbType.Decimal).Value = _Param10;
                zCommand.Parameters.Add("@Param11", SqlDbType.Decimal).Value = _Param11;
                zCommand.Parameters.Add("@Param12", SqlDbType.Decimal).Value = _Param12;

                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                if (_FromDate.Year == 0001)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate.Year == 0001)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Salary SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " FromDate = @FromDate,"
                        + " ToDate = @ToDate,"
                        + " Description = @Description, Approved = @Approved, ApprovedBy =@ApprovedBy, ApprovedDate = @ApprovedDate, Step = @Step, Tag=@Tag,"
                        + " Param1 = @Param1,"
                        + " Param2 = @Param2,"
                        + " Param3 = @Param3,"
                        + " Param4 = @Param4,"
                        + " Param5 = @Param5,"
                        + " Param51 = @Param51,"
                        + " Param6 = @Param6,"
                        + " Param7 = @Param7,"
                        + " Param8 = @Param8,"
                        + " Param9 = @Param9,"
                        + " Param10 = @Param10,"
                        + " Param11 = @Param11, Param13 = @Param13, Param14 = @Param14,Param15 = @Param15,Param16 = @Param16, Note1 =@Note1, Note2 =@Note2,"
                        + " Param12 = @Param12, ParamTotal1=@ParamTotal1, ParamTotal2=@ParamTotal2, ParamTotal3=@ParamTotal3, ParamTotal4=@ParamTotal4, ParamTotal5=@ParamTotal5, ParamTotal6=@ParamTotal6,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Param13", SqlDbType.Decimal).Value = Param131;
                zCommand.Parameters.Add("@Param14", SqlDbType.Decimal).Value = Param141;
                zCommand.Parameters.Add("@Param15", SqlDbType.Decimal).Value = _Param15;
                zCommand.Parameters.Add("@Param16", SqlDbType.Decimal).Value = _Param16;
                zCommand.Parameters.Add("@Note1", SqlDbType.NVarChar).Value = _Note1;
                zCommand.Parameters.Add("@Note2", SqlDbType.NVarChar).Value = _Note2;
                zCommand.Parameters.Add("@Tag", SqlDbType.Int).Value = Tag;

                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@Step", SqlDbType.NVarChar).Value = _Step;
                if (_ApprovedDate.Year == 0001)
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ApprovedDate", SqlDbType.DateTime).Value = _ApprovedDate;

                zCommand.Parameters.Add("@ParamTotal1", SqlDbType.Decimal).Value = _ParamTotal1;
                zCommand.Parameters.Add("@ParamTotal2", SqlDbType.Decimal).Value = _ParamTotal2;
                zCommand.Parameters.Add("@ParamTotal3", SqlDbType.Decimal).Value = _ParamTotal3;
                zCommand.Parameters.Add("@ParamTotal4", SqlDbType.Decimal).Value = _ParamTotal4;
                zCommand.Parameters.Add("@ParamTotal5", SqlDbType.Decimal).Value = _ParamTotal5;
                zCommand.Parameters.Add("@ParamTotal6", SqlDbType.Decimal).Value = _ParamTotal6;

                zCommand.Parameters.Add("@Param1", SqlDbType.Decimal).Value = _Param1;
                zCommand.Parameters.Add("@Param2", SqlDbType.Decimal).Value = _Param2;
                zCommand.Parameters.Add("@Param3", SqlDbType.Decimal).Value = _Param3;
                zCommand.Parameters.Add("@Param4", SqlDbType.Decimal).Value = _Param4;
                zCommand.Parameters.Add("@Param5", SqlDbType.Decimal).Value = _Param5;
                zCommand.Parameters.Add("@Param51", SqlDbType.Decimal).Value = _Param51;
                zCommand.Parameters.Add("@Param6", SqlDbType.Decimal).Value = _Param6;
                zCommand.Parameters.Add("@Param7", SqlDbType.Decimal).Value = _Param7;
                zCommand.Parameters.Add("@Param8", SqlDbType.Decimal).Value = _Param8;
                zCommand.Parameters.Add("@Param9", SqlDbType.Decimal).Value = _Param9;
                zCommand.Parameters.Add("@Param10", SqlDbType.Decimal).Value = _Param10;
                zCommand.Parameters.Add("@Param11", SqlDbType.Decimal).Value = _Param11;
                zCommand.Parameters.Add("@Param12", SqlDbType.Decimal).Value = _Param12;

                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                if (_FromDate.Year == 0001)
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate.Year == 0001)
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = _ToDate;

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM HRM_Salary WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateApprove()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Salary SET Approved = @Approved, ApprovedBy = @ApprovedBy, ApprovedDate = GETDATE() WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Approved", SqlDbType.Int).Value = _Approved;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
