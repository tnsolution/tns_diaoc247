﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.KPI
{
    public class KPI_Assets_Data
{
public static DataTable List()
{
DataTable zTable = new DataTable();
string zSQL = "SELECT  * FROM KPI_Assets " ;
string zConnectionString = ConnectDataBase.ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close();
}
catch (Exception ex)
{
string zstrMessage = ex.ToString();
}
return zTable;
}
public static DataTable List(int Amount)
{
DataTable zTable = new DataTable();
string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM KPI_Assets ";
string zConnectionString = ConnectDataBase.ConnectionString;
try
{
SqlConnection zConnect = new SqlConnection(zConnectionString);
zConnect.Open();
SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
zAdapter.Fill(zTable);
zCommand.Dispose();
zConnect.Close();
}
catch (Exception ex)
{
string zstrMessage = ex.ToString();
}
return zTable;
}
}
}
