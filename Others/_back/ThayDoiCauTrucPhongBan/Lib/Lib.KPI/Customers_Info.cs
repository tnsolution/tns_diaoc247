﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.KPI
{
    public class KPI_Customers_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ParentKey = 0;
        private int _CustomerKey = 0;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _CategoryKey = 0;
        private int _CategoryPlan = 0;
        private DateTime _CreatedDate;
        private string _Description = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public KPI_Customers_Info()
        {
        }
        public KPI_Customers_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM KPI_Customers WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ParentKey"] != DBNull.Value)
                        _ParentKey = int.Parse(zReader["ParentKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public KPI_Customers_Info(int CustomerKey, int EmployeeKey, DateTime CreatedDate)
        {
            string zSQL = "SELECT * FROM KPI_Customers WHERE CustomerKey = @CustomerKey AND EmployeeKey = @EmployeeKey AND (CreatedDate BETWEEN @FromDate AND @ToDate)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                DateTime FromDate = new DateTime(CreatedDate.Year, CreatedDate.Month, CreatedDate.Day, 0, 0, 0);
                DateTime ToDate = new DateTime(CreatedDate.Year, CreatedDate.Month, CreatedDate.Day, 23, 59, 59);

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ParentKey"] != DBNull.Value)
                        _ParentKey = int.Parse(zReader["ParentKey"].ToString());
                    if (zReader["CustomerKey"] != DBNull.Value)
                        _CustomerKey = int.Parse(zReader["CustomerKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public int CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int DepartmentKey { get => _DepartmentKey; set => _DepartmentKey = value; }
        public int CategoryPlan { get => _CategoryPlan; set => _CategoryPlan = value; }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO KPI_Customers ("
        + " ParentKey ,CustomerKey ,EmployeeKey, DepartmentKey ,CategoryKey, CategoryPlan ,CreatedDate ,Description ) "
         + " VALUES ( "
         + "@ParentKey ,@CustomerKey ,@EmployeeKey, @DepartmentKey ,@CategoryKey, @CategoryPlan ,GETDATE() ,@Description ) "
         + " SELECT AutoKey FROM KPI_Customers WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = _ParentKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryPlan", SqlDbType.Int).Value = _CategoryPlan;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE KPI_Customers SET "
                        + " ParentKey = @ParentKey,"
                        + " CustomerKey = @CustomerKey,"
                        + " EmployeeKey = @EmployeeKey, DepartmentKey = @DepartmentKey,"
                        + " CategoryKey = @CategoryKey, CategoryPlan = @CategoryPlan,"
                        + " CreatedDate = GETDATE(),"
                        + " Description = @Description"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = _ParentKey;
                zCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = _CustomerKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryPlan", SqlDbType.Int).Value = _CategoryPlan;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM KPI_Customers WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
