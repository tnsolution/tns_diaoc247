﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ItemPlan
    {
        private string _Message = "";
        private string _ID = "0";
        private string _PlanKey = "0";
        private string _CategoryKey = "0";
        private string _CategoryName = "0";
        private string _Value = "0";
        private string _ValueKPI = "0";

        public string ID
        {
            get
            {
                return _ID;
            }

            set
            {
                _ID = value;
            }
        }

        public string PlanKey
        {
            get
            {
                return _PlanKey;
            }

            set
            {
                _PlanKey = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public string CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public string Value
        {
            get
            {
                return _Value;
            }

            set
            {
                _Value = value;
            }
        }

        public string ValueKPI { get => _ValueKPI; set => _ValueKPI = value; }
    }
}
