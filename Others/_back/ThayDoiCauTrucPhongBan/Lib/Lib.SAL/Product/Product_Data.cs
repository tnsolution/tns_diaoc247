﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SAL
{
    public class Product_Data
    {
        public static List<Product_Item> Search_ADV1(int Project, int Category, string ID1, string ID2, string ID3, string Bed, string Name, string Status, string Purpose, int Personal, int Employee, string View, string Door, string Customer, string Phone)
        {
            Project_Info zProject = new Project_Info(Project);
            List<Product_Item> zList = new List<Product_Item>();
            string zSQL = "";
            switch (zProject.ShortTable)
            {
                case "ResaleHouse":
                    #region [MyRegion]
                    zSQL += @"
SELECT 'ResaleHouse' AS AssetType, A.AssetID, A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, A.Hot,
A.DepartmentKey, A.EmployeeKey, A.Description, A.NumberRoom AS Room,
A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, A.PriceRent, A.Price, A.PricePurchase,
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure, A.NumberRoom,
A.PurposeSearch, A.PurposeShow AS Purpose, A.Web, A.WebPublic, A.Title, A.DateContractEnd,
A.CreatedBy, A.CreatedName, A.CreatedDate, 
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath,
dbo.FNC_GetOwnerPhone(A.AssetKey,'ResaleHouse') Phone,
dbo.FNC_GetOwnerName(A.AssetKey,'ResaleHouse') CustomerName,
dbo.FNC_SysCategoryName(A.CategoryInside) AS InSide,
ISNULL(A.WebPublic,0) AS Web, A.Area AS Area
FROM PUL_Resale_House A 
WHERE IsResale = 1";

                    #region [filter]
                    if (View != "0")
                    {
                        zSQL += " AND A.DirectionView LIKE @View";
                    }
                    if (Door != "0")
                    {
                        zSQL += " AND A.DirectionDoor LIKE @Door";
                    }
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberRoom LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.HouseStatus IN (" + Status + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                    }
                    if (Customer != string.Empty)
                    {
                        zSQL += " AND dbo.FNC_GetOwnerName(A.AssetKey,'ResaleHouse') LIKE @CustomerName";
                    }
                    if (Phone != string.Empty)
                    {
                        zSQL += " AND dbo.FNC_GetOwnerPhone(A.AssetKey,'ResaleHouse') LIKE @Phone";
                    }
                    #endregion

                    zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                    DataTable zTable = new DataTable();
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                        zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + Customer + "%";
                        zCommand.Parameters.Add("@Door", SqlDbType.NVarChar).Value = "%" + Door + "%";
                        zCommand.Parameters.Add("@View", SqlDbType.NVarChar).Value = "%" + View + "%";

                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;
                case "ResaleApartment":
                    #region [MyRegion]
                    zSQL += @"
SELECT 'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, 
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room,
A.ApartmentStatus AS StatusKey,		
A.WallArea AS Area, A.PriceRent, A.Price, A.PricePurchase,
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.CreatedName, A.Hot,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath,
dbo.FNC_GetOwnerPhone(A.AssetKey,'ResaleApartment') Phone,
dbo.FNC_GetOwnerName(A.AssetKey,'ResaleApartment') CustomerName,
dbo.FNC_SysCategoryName(A.CategoryInside) AS InSide,
ISNULL(A.WebPublic,0) AS Web
FROM PUL_Resale_Apartment A 
WHERE A.IsResale = 1";

                    #region [filter]
                    if (View != "0")
                    {
                        zSQL += " AND A.DirectionView LIKE @View";
                    }
                    if (Door != "0")
                    {
                        zSQL += " AND A.DirectionDoor LIKE @Door";
                    }
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberBed LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    if (Customer != string.Empty)
                    {
                        zSQL += " AND dbo.FNC_GetOwnerName(A.AssetKey,'ResaleApartment') LIKE @CustomerName";
                    }
                    if (Phone != string.Empty)
                    {
                        zSQL += " AND dbo.FNC_GetOwnerPhone(A.AssetKey,'ResaleApartment') LIKE @Phone";
                    }
                    #endregion

                    zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                    zTable = new DataTable();
                    zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%";
                        zCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + Customer + "%";
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        zCommand.Parameters.Add("@Door", SqlDbType.NVarChar).Value = "%" + Door + "%";
                        zCommand.Parameters.Add("@View", SqlDbType.NVarChar).Value = "%" + View + "%";
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;
            }
            return zList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Project"></param>
        /// <param name="Category"></param>
        /// <param name="ID1"></param>
        /// <param name="ID2"></param>
        /// <param name="ID3"></param>
        /// <param name="Bed"></param>
        /// <param name="Name"></param>
        /// <param name="Status"></param>
        /// <param name="Purpose"></param>
        /// <param name="Personal"></param>
        /// <param name="Employee"></param>
        /// <returns></returns>
        public static List<ItemAsset> Search(int Project, int Category, string ID1, string ID2, string ID3, string Bed, string Name, string Status, string Purpose, int Personal, int Employee)
        {
            Project_Info zProject = new Project_Info(Project);
            List<ItemAsset> zList = new List<ItemAsset>();
            string zSQL = "";
            string zSQLUnion = "";
            string ZSQLExe = "";
            switch (zProject.ShortTable)
            {
                case "ResaleHouse":
                    #region [MyRegion]
                    zSQL += @"
SELECT 'ResaleHouse' AS AssetType, A.AssetID, A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, A.Hot,
A.DepartmentKey, A.EmployeeKey, A.Description, A.NumberRoom AS Room,
A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, A.PriceRent, A.Price, A.PricePurchase,
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure, A.NumberRoom,
A.PurposeSearch, A.PurposeShow AS Purpose, A.Web, A.WebPublic, A.Title, A.DateContractEnd,
A.CreatedBy, A.CreatedName, A.CreatedDate, 
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath
FROM PUL_Resale_House A WHERE IsResale = 1";

                    #region [filter]
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberRoom LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.HouseStatus IN (" + Status + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT 'ResaleHouse' AS AssetType, A.AssetID, A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, A.Hot,
                        //A.DepartmentKey, A.EmployeeKey, A.Description, 
                        //A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
                        //A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, A.PriceRent, A.Price, A.PricePurchase,
                        //A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
                        //A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure, A.NumberRoom,
                        //A.PurposeSearch, A.PurposeShow AS Purpose, A.Web, A.WebPublic, A.Title, A.DateContractEnd,
                        //A.CreatedBy, A.CreatedName, A.CreatedDate, 
                        //A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
                        //dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
                        //dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
                        //dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_House A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleHouse'
                        //AND B.EmployeeKey =  @Employee AND B.IsResale = 1";

                        //                        #region [fitler]
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberRoom LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.HouseStatus IN (" + Status + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;

                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    DataTable zTable = new DataTable();
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<ItemAsset>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;
                case "ResaleApartment":
                    #region [MyRegion]
                    zSQL += @"
SELECT 'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, 
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room,
A.ApartmentStatus AS StatusKey,		
A.WallArea AS Area, A.PriceRent, A.Price, A.PricePurchase,
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.CreatedName, A.Hot,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
FROM PUL_Resale_Apartment A WHERE A.IsResale = 1";

                    #region [filter]
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberBed LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    //tim cá nhân và chia se dau tien la select where employee = ca nhan va bảng chi se
                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT 'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID,
                        //A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
                        //A.ApartmentStatus AS StatusKey, A.NumberBed AS Room,		
                        //A.WallArea AS Area, A.PriceRent, A.Price, A.PricePurchase,
                        //A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD, 
                        //A.CreatedName, A.Hot,
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
                        //dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
                        //dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_Apartment A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleApartment'
                        //AND B.EmployeeKey = @Employee AND A.IsResale = 1";

                        //                        #region [fitler]
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberBed LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;

                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }

                    zTable = new DataTable();
                    zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<ItemAsset>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;
            }
            return zList;
        }

        /// <summary>
        /// Search in ProductView
        /// </summary>
        /// <param name="Project"></param>
        /// <param name="Category"></param>
        /// <param name="ID1"></param>
        /// <param name="ID2"></param>
        /// <param name="ID3"></param>
        /// <param name="Bed"></param>
        /// <param name="Name"></param>
        /// <param name="Status"></param>
        /// <param name="Purpose"></param>
        /// <param name="Personal"></param>
        /// <param name="Employee"></param>
        /// <param name="Amount"></param>
        /// <returns></returns>
        public static List<Product_Item> Search(int Project, int Category, string ID1, string ID2, string ID3, string Bed, string Name, string Status, string Purpose, int Personal, int Employee, int Amount)
        {
            Project_Info zProject = new Project_Info(Project);
            List<Product_Item> zList = new List<Product_Item>();
            string zSQL = "";
            string zSQLUnion = "";
            string ZSQLExe = "";
            if (Amount != 0)
                zSQL += "SELECT TOP " + Amount;
            else
                zSQL += "SELECT ";
            switch (zProject.ShortTable)
            {
                case "ResaleHouse":
                    #region [MyRegion]
                    zSQL += @"
'ResaleHouse' AS AssetType, A.AssetID, A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, 
A.DepartmentKey, A.EmployeeKey, A.Description, A.NumberRoom AS Room,
A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, A.PriceRent_VND, A.Price_VND,
A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure,
A.Hot, A.PurposeSearch, A.PurposeShow AS Purpose, A.Web, A.WebPublic, A.Title, A.DateContractEnd,
A.CreatedBy, A.CreatedName, A.CreatedDate, 
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath
FROM PUL_Resale_House A WHERE A.IsResale = 1";

                    #region [filter]
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberRoom LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.HouseStatus IN (" + Status + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT TOP " + Amount + @" 'ResaleHouse' AS AssetType, A.AssetID, A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, 
                        //A.DepartmentKey, A.EmployeeKey, A.Description, 
                        //A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
                        //A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, A.Price, A.PriceRent,
                        //A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure, A.NumberRoom,
                        //A.Hot, A.PurposeSearch, A.PurposeShow, A.Web, A.WebPublic, A.Title, A.DateContractEnd,
                        //A.CreatedBy, A.CreatedName, A.CreatedDate, 
                        //A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
                        //dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
                        //dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
                        //dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_House A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleHouse'
                        //AND B.EmployeeKey =  @Employee AND B.IsResale = 1";

                        //                        #region [fitler]
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberRoom LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.HouseStatus IN (" + Status + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    DataTable zTable = new DataTable();
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;

                case "ResaleApartment":
                    #region [MyRegion]
                    zSQL += @"
'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room,
A.ApartmentStatus AS StatusKey,		
A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName, 	
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
FROM PUL_Resale_Apartment A WHERE A.IsResale = 1";

                    #region [filter]
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberBed LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    //tim cá nhân và chia se dau tien la select where employee = ca nhan va bảng chi se
                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT 'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
                        //A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
                        //A.ApartmentStatus AS StatusKey, A.NumberBed AS Room,		
                        //A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName, 	
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
                        //dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
                        //dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_Apartment A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleApartment'
                        //AND B.EmployeeKey =  @Employee AND IsResale = 1";

                        //                        #region [fitler]
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberBed LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;

                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }

                    zTable = new DataTable();
                    zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;

                default:
                    #region [MyRegion]
                    zSQL += @"
'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room,
A.ApartmentStatus AS StatusKey,		
A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName, 	
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
FROM PUL_Resale_Apartment A WHERE A.IsResale = 1";

                    #region [filter]
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberBed LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    //tim cá nhân và chia se dau tien la select where employee = ca nhan va bảng chi se
                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT 'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
                        //A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
                        //A.ApartmentStatus AS StatusKey, A.NumberBed AS Room,		
                        //A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName, 	
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
                        //dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
                        //dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_Apartment A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleApartment'
                        //AND B.EmployeeKey =  @Employee AND IsResale = 1";

                        //                        #region [fitler]
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberBed LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }

                    zTable = new DataTable();
                    zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;
            }

            return zList;
        }

        // Lib.SAL.Product_Data recovery
        public static List<Product_ItemMobile> SearchMobile(int Project, int Category, string ID1, string ID2, string ID3, string Bed,
            string Name, string Status, string Purpose, int Personal, int Employee, int Index, int Show)
        {
            Project_Info project_Info = new Project_Info(Project);
            List<Product_ItemMobile> result = new List<Product_ItemMobile>();
            string text = "";
            string shortTable = project_Info.ShortTable;

            switch (shortTable)
            {
                case "ResaleHouse":
                    #region MyRegion
                    text += @"
SELECT ROW_NUMBER() OVER (ORDER BY Hot DESC, ID1, ID2, ID3 ) AS RowNum, 
'ResaleHouse' AS AssetType, A.AssetID, A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, A.Hot,
A.DepartmentKey, A.EmployeeKey, A.Description,
A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, A.PriceRent, A.Price, A.PricePurchase,
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure, A.NumberRoom AS Room,
A.PurposeSearch, A.PurposeShow AS Purpose, A.DateContractEnd,
A.CreatedBy, A.CreatedName, A.CreatedDate, 
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath,
B.CustomerName, B.Phone1 AS CustomerPhone
FROM PUL_Resale_House A 
OUTER APPLY dbo.GetProductCustomer(A.AssetKey,'ResaleHouse') B
WHERE IsResale = 1";

                    bool flag = Personal == 1;
                    if (flag)
                    {
                        text += " AND A.EmployeeKey = @Employee";
                    }
                    bool flag2 = Project != 0;
                    if (flag2)
                    {
                        text = text + " AND A.ProjectKey =" + Project;
                    }
                    bool flag3 = Category != 0;
                    if (flag3)
                    {
                        text = text + " AND A.CategoryKey =" + Category;
                    }
                    bool flag4 = ID1 != string.Empty;
                    if (flag4)
                    {
                        text += " AND A.ID1 LIKE @ID1";
                    }
                    bool flag5 = ID2 != string.Empty;
                    if (flag5)
                    {
                        text += " AND A.ID2 LIKE @ID2";
                    }
                    bool flag6 = ID3 != string.Empty;
                    if (flag6)
                    {
                        text += " AND A.ID3 LIKE @ID3";
                    }
                    bool flag7 = Bed.ToInt() != 0;
                    if (flag7)
                    {
                        text += " AND A.NumberRoom LIKE @Room";
                    }
                    bool flag8 = Name != string.Empty;
                    if (flag8)
                    {
                        text += " AND A.AssetID LIKE @Name";
                    }
                    bool flag9 = Status != string.Empty;
                    if (flag9)
                    {
                        text = text + " AND A.HouseStatus IN (" + Status + ")";
                    }
                    bool flag10 = Purpose != string.Empty;
                    if (flag10)
                    {
                        text = text + " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                    }
                    string cmdText = "SELECT * FROM (" + text + ") Y WHERE Y.RowNum BETWEEN (@Index -1) * @Show + 1 AND (((@Index -1) * @Show + 1) + @Show) - 1";
                    DataTable dataTable = new DataTable();
                    string connectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection sqlConnection = new SqlConnection(connectionString);
                        sqlConnection.Open();
                        SqlCommand sqlCommand = new SqlCommand(cmdText, sqlConnection);
                        sqlCommand.Parameters.Add("@Index", SqlDbType.Int).Value = Index;
                        sqlCommand.Parameters.Add("@Show", SqlDbType.Int).Value = Show;
                        sqlCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        sqlCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        sqlCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        sqlCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        sqlCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        sqlCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        sqlDataAdapter.Fill(dataTable);
                        sqlCommand.Dispose();
                        sqlConnection.Close();
                        result = dataTable.DataTableToList<Product_ItemMobile>();
                    }
                    catch (Exception ex)
                    {
                        string text2 = ex.ToString();
                    }
                    #endregion
                    break;

                case "ResaleApartment":
                    #region MyRegion
                    text += @"
SELECT ROW_NUMBER() OVER (ORDER BY Hot DESC, ID1, ID2, ID3 ) AS RowNum, 
'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room, A.ApartmentStatus AS StatusKey, A.EmployeeKey,
A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath,
B.CustomerName, B.Phone1 AS CustomerPhone
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetProductCustomer(A.AssetKey,'ResaleApartment') B
WHERE A.IsResale = 1";
                    bool flag11 = Personal == 1;
                    if (flag11)
                    {
                        text += " AND A.EmployeeKey = @Employee";
                    }
                    bool flag12 = Project != 0;
                    if (flag12)
                    {
                        text = text + " AND A.ProjectKey =" + Project;
                    }
                    bool flag13 = Category != 0;
                    if (flag13)
                    {
                        text = text + " AND A.CategoryKey =" + Category;
                    }
                    bool flag14 = ID1 != string.Empty;
                    if (flag14)
                    {
                        text += " AND A.ID1 LIKE @ID1";
                    }
                    bool flag15 = ID2 != string.Empty;
                    if (flag15)
                    {
                        text += " AND A.ID2 LIKE @ID2";
                    }
                    bool flag16 = ID3 != string.Empty;
                    if (flag16)
                    {
                        text += " AND A.ID3 LIKE @ID3";
                    }
                    bool flag17 = Bed.ToInt() != 0;
                    if (flag17)
                    {
                        text += " AND A.NumberBed LIKE @Room";
                    }
                    bool flag18 = Name != string.Empty;
                    if (flag18)
                    {
                        text += " AND A.AssetID LIKE @Name";
                    }
                    bool flag19 = Status != string.Empty;
                    if (flag19)
                    {
                        text = text + " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    bool flag20 = Purpose != string.Empty;
                    if (flag20)
                    {
                        text = text + " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    cmdText = "SELECT * FROM (" + text + ") Y WHERE Y.RowNum BETWEEN (@Index -1) * @Show + 1 AND (((@Index -1) * @Show + 1) + @Show) - 1";
                    dataTable = new DataTable();
                    connectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection sqlConnection2 = new SqlConnection(connectionString);
                        sqlConnection2.Open();
                        SqlCommand sqlCommand2 = new SqlCommand(cmdText, sqlConnection2);
                        sqlCommand2.Parameters.Add("@Index", SqlDbType.Int).Value = Index;
                        sqlCommand2.Parameters.Add("@Show", SqlDbType.Int).Value = Show;
                        sqlCommand2.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        sqlCommand2.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        sqlCommand2.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        sqlCommand2.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        sqlCommand2.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        sqlCommand2.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter sqlDataAdapter2 = new SqlDataAdapter(sqlCommand2);
                        sqlDataAdapter2.Fill(dataTable);
                        sqlCommand2.Dispose();
                        sqlConnection2.Close();
                        result = dataTable.DataTableToList<Product_ItemMobile>();
                    }
                    catch (Exception ex2)
                    {
                        string text3 = ex2.ToString();
                    }
                    #endregion
                    break;

                default:
                    #region MyRegion
                    text += @"
SELECT ROW_NUMBER() OVER (ORDER BY Hot DESC, ID1, ID2, ID3 ) AS RowNum, 
'ResaleApartment' AS AssetType, ID1,ID2,ID3, A.AssetID, A.Hot,
A.AssetKey, A.CategoryKey, A.PurposeShow AS Purpose, A.ProjectKey,
A.NumberBed AS Room, A.ApartmentStatus AS StatusKey, A.EmployeeKey,
A.WallArea AS Area, A.PriceRent_VND, A.Price_VND, A.CreatedName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath,
B.CustomerName, B.Phone1 AS CustomerPhone
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetProductCustomer(A.AssetKey,'ResaleApartment') B
WHERE A.IsResale = 1";
                    flag11 = Personal == 1;
                    if (flag11)
                    {
                        text += " AND A.EmployeeKey = @Employee";
                    }
                    flag12 = Project != 0;
                    if (flag12)
                    {
                        text = text + " AND A.ProjectKey =" + Project;
                    }
                    flag13 = Category != 0;
                    if (flag13)
                    {
                        text = text + " AND A.CategoryKey =" + Category;
                    }
                    flag14 = ID1 != string.Empty;
                    if (flag14)
                    {
                        text += " AND A.ID1 LIKE @ID1";
                    }
                    flag15 = ID2 != string.Empty;
                    if (flag15)
                    {
                        text += " AND A.ID2 LIKE @ID2";
                    }
                    flag16 = ID3 != string.Empty;
                    if (flag16)
                    {
                        text += " AND A.ID3 LIKE @ID3";
                    }
                    flag17 = Bed.ToInt() != 0;
                    if (flag17)
                    {
                        text += " AND A.NumberBed LIKE @Room";
                    }
                    flag18 = Name != string.Empty;
                    if (flag18)
                    {
                        text += " AND A.AssetID LIKE @Name";
                    }
                    flag19 = Status != string.Empty;
                    if (flag19)
                    {
                        text = text + " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    flag20 = Purpose != string.Empty;
                    if (flag20)
                    {
                        text = text + " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    cmdText = "SELECT * FROM (" + text + ") Y WHERE Y.RowNum BETWEEN (@Index -1) * @Show + 1 AND (((@Index -1) * @Show + 1) + @Show) - 1";
                    dataTable = new DataTable();
                    connectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection sqlConnection2 = new SqlConnection(connectionString);
                        sqlConnection2.Open();
                        SqlCommand sqlCommand2 = new SqlCommand(cmdText, sqlConnection2);
                        sqlCommand2.Parameters.Add("@Index", SqlDbType.Int).Value = Index;
                        sqlCommand2.Parameters.Add("@Show", SqlDbType.Int).Value = Show;
                        sqlCommand2.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        sqlCommand2.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        sqlCommand2.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        sqlCommand2.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        sqlCommand2.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        sqlCommand2.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter sqlDataAdapter2 = new SqlDataAdapter(sqlCommand2);
                        sqlDataAdapter2.Fill(dataTable);
                        sqlCommand2.Dispose();
                        sqlConnection2.Close();
                        result = dataTable.DataTableToList<Product_ItemMobile>();
                    }
                    catch (Exception ex2)
                    {
                        string text3 = ex2.ToString();
                    }
                    #endregion
                    break;
            }

            return result;
        }

        /// <summary>
        /// Search in ProductList Page
        /// </summary>
        /// <param name="Project"></param>
        /// <param name="Category"></param>
        /// <param name="ID1"></param>
        /// <param name="ID2"></param>
        /// <param name="ID3"></param>
        /// <param name="Bed"></param>
        /// <param name="Name"></param>
        /// <param name="Status"></param>
        /// <param name="Purpose"></param>
        /// <param name="Personal"></param>
        /// <param name="Employee"></param>
        /// <param name="View"></param>
        /// <param name="Door"></param>
        /// <returns></returns>
        public static List<Product_Item> Search(int Project, int Category, string ID1, string ID2, string ID3, string Bed, string Name, string Status, string Purpose, int Personal, int Employee, string View, string Door)
        {
            string zSQL = "";
            string zSQLUnion = "";
            string ZSQLExe = "";
            List<Product_Item> zList = new List<Product_Item>();
            Project_Info zProject = new Project_Info(Project);
            switch (zProject.ShortTable)
            {
                case "ResaleApartment":
                    #region [MyRegion]
                    zSQL += @"
SELECT 'ResaleApartment' AS AssetType, 
ID1, ID2, ID3, A.AssetID, 
A.AssetKey, A.CategoryKey, 
A.PurposeShow AS Purpose, A.NumberBed AS Room, 
A.ApartmentStatus AS StatusKey, A.WallArea AS Area, 
A.PriceRent, A.Price, A.PricePurchase, 
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.CreatedName, A.Hot, A.ProjectKey,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
FROM PUL_Resale_Apartment A 
WHERE A.IsResale = 1";

                    #region [filter]
                    if (View != "0")
                    {
                        zSQL += " AND A.DirectionView LIKE @View";
                    }
                    if (Door != "0")
                    {
                        zSQL += " AND A.DirectionDoor LIKE @Door";
                    }
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberBed LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    //tim cá nhân và chia se dau tien la select where employee = ca nhan va bảng chi se
                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT 'ResaleApartment' AS AssetType, 
                        //ID1, ID2, ID3, A.AssetID, 
                        //A.AssetKey, A.CategoryKey, 
                        //A.PurposeShow AS Purpose, A.NumberBed AS Room, 
                        //A.ApartmentStatus AS StatusKey, A.WallArea AS Area, 
                        //A.PriceRent, A.Price, A.PricePurchase, 
                        //A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
                        //A.CreatedName, A.Hot, A.ProjectKey,
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
                        //dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
                        //dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleApartment') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_Apartment A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleApartment'
                        //AND B.EmployeeKey = @Employee AND A.IsResale = 1";

                        //                        #region [fitler]
                        //                        if (View != "0")
                        //                        {
                        //                            zSQL += " AND A.DirectionView LIKE @View";
                        //                        }
                        //                        if (Door != "0")
                        //                        {
                        //                            zSQL += " AND A.DirectionDoor LIKE @Door";
                        //                        }
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberBed LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ApartmentStatus IN (" + Status.Trim() + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose.Trim() + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;

                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }

                    DataTable zTable = new DataTable();
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@View", SqlDbType.NVarChar).Value = "%" + View + "%";
                        zCommand.Parameters.Add("@Door", SqlDbType.NVarChar).Value = "%" + Door + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;
                case "ResaleHouse":
                    #region [MyRegion]
                    zSQL += @"
SELECT 'ResaleHouse' AS AssetType, A.AssetID, 
A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, A.Hot,
A.DepartmentKey, A.EmployeeKey, A.Description, A.NumberRoom AS Room,
A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, 
A.PriceRent, A.Price, A.PricePurchase,
A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure,
A.PurposeSearch, A.PurposeShow AS Purpose, A.DateContractEnd,
A.CreatedBy, A.CreatedName, A.CreatedDate, 
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath
FROM PUL_Resale_House A WHERE IsResale = 1";

                    #region [filter]
                    if (Personal == 1) //tim cá nhân và chia se
                    {
                        zSQL += " AND A.EmployeeKey = @Employee";
                    }
                    if (Project != 0)
                    {
                        zSQL += " AND A.ProjectKey =" + Project;
                    }
                    if (Category != 0)
                    {
                        zSQL += " AND A.CategoryKey =" + Category;
                    }
                    if (ID1 != string.Empty)
                    {
                        zSQL += " AND A.ID1 LIKE @ID1";
                    }
                    if (ID2 != string.Empty)
                    {
                        zSQL += " AND A.ID2 LIKE @ID2";
                    }
                    if (ID3 != string.Empty)
                    {
                        zSQL += " AND A.ID3 LIKE @ID3";
                    }
                    if (Bed != string.Empty)
                    {
                        zSQL += " AND A.NumberRoom LIKE @Room";
                    }
                    if (Name != string.Empty)
                    {
                        zSQL += " AND A.AssetID LIKE @Name";
                    }
                    if (Status != string.Empty)
                    {
                        zSQL += " AND A.HouseStatus IN (" + Status + ")";
                    }
                    if (Purpose != string.Empty)
                    {
                        zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                    }
                    //if (Department != 0)
                    //{
                    //    zSQL += " AND A.DepartmentKey = " + Department;
                    //}
                    #endregion

                    if (Personal == 1)
                    {
                        //                        zSQL += " UNION ";
                        //                        zSQL += @"
                        //SELECT 'ResaleHouse' AS AssetType, A.AssetID, 
                        //A.ID1, A.ID2, A.ID3, A.AssetKey, A.ProductKey, A.Hot,
                        //A.DepartmentKey, A.EmployeeKey, A.Description, A.NumberRoom AS Room,
                        //A.CategoryKey, A.Legal AS LegalKey, A.GroundArea ,A.GroundTotal, 
                        //A.MainDoor AS Door, A.CategoryInside AS FurnitureKey, 
                        //A.PriceRent, A.Price, A.PricePurchase,
                        //A.PriceRent_VND, A.Price_VND, A.PriceRent_USD, A.Price_USD,
                        //A.Web, A.WebPublic, A.Title, A.HouseStatus AS StatusKey, A.Structure, 
                        //A.PurposeSearch, A.PurposeShow AS Purpose, A.DateContractEnd,
                        //A.CreatedBy, A.CreatedName, A.CreatedDate, 
                        //A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
                        //dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
                        //dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
                        //dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
                        //dbo.FNC_AssetCategoryName(A.CategoryKey) CategoryName,
                        //dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
                        //dbo.FNC_SysCategoryName(A.Legal) AS [Legal],
                        //dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
                        //dbo.FNC_GetImage(A.AssetKey, 'ResaleHouse') ImagePath
                        //FROM PUL_SharePermition B 
                        //LEFT JOIN PUL_Resale_House A ON A.AssetKey = B.AssetKey 
                        //WHERE B.ObjectTable = 'ResaleHouse'
                        //AND B.EmployeeKey =  @Employee AND A.IsResale = 1";

                        //                        #region [fitler]
                        //                        if (Project != 0)
                        //                        {
                        //                            zSQL += " AND A.ProjectKey =" + Project;
                        //                        }
                        //                        if (Category != 0)
                        //                        {
                        //                            zSQL += " AND A.CategoryKey =" + Category;
                        //                        }
                        //                        if (ID1 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID1 LIKE @ID1";
                        //                        }
                        //                        if (ID2 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID2 LIKE @ID2";
                        //                        }
                        //                        if (ID3 != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.ID3 LIKE @ID3";
                        //                        }
                        //                        if (Bed != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.NumberRoom LIKE @Room";
                        //                        }
                        //                        if (Name != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.AssetID LIKE @Name";
                        //                        }
                        //                        if (Status != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.HouseStatus IN (" + Status + ")";
                        //                        }
                        //                        if (Purpose != string.Empty)
                        //                        {
                        //                            zSQL += " AND A.PurposeSearch LIKE '%" + Purpose + "%'";
                        //                        }
                        //                        //if (Department != 0)
                        //                        //{
                        //                        //    zSQL += " AND A.DepartmentKey = " + Department;
                        //                        //}
                        //                        #endregion

                        //                        zSQLUnion = @"WITH X AS(" + zSQL + ") SELECT * FROM X ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        //                        ZSQLExe = zSQLUnion;
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    else
                    {
                        zSQL += " ORDER BY Hot DESC, Status, ProjectName, ID1, ID2, ID3";
                        ZSQLExe = zSQL;
                    }
                    zTable = new DataTable();
                    zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(ZSQLExe, zConnect);
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = "%" + ID1 + "%";
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = "%" + ID2 + "%";
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = "%" + ID3 + "%";
                        zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                        zCommand.Parameters.Add("@Room", SqlDbType.NVarChar).Value = Bed;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();

                        zList = zTable.DataTableToList<Product_Item>();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    #endregion
                    break;

                default:
                    break;
            }
            return zList;
        }
        public static DataTable ManagerProduct(int Project, int Employee1, int Employee2, string AssetID, int Number)
        {
            string Amount = "";
            if (Number != 0)
                Amount = " TOP " + Number;
            Project_Info zProject = new Project_Info(Project);
            DataTable zTable = new DataTable();
            string zSQL = "";
            switch (zProject.ShortTable)
            {
                case "ResaleApartment":
                    zSQL += @"
SELECT " + Amount + @" 'ResaleApartment' AS AssetType, A.AssetID, 
A.AssetKey, A.CreatedName, A.ProjectKey, A.EmployeeKey,
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName, 
DATEDIFF(DAY,A.ModifiedDate, GETDATE()) AS NumberOfDay, B.ConfirmDate, B.Confirmed,
dbo.FNC_GetNameEmployee(ConfirmBy) AS ConfirmByName, 
dbo.FNC_GetNameEmployee(NewStaff) AS NewStaffName
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetTransfer(A.AssetKey, 'ResaleApartment') B 
WHERE A.IsResale = 1";

                    if (Employee1 != 0)
                        zSQL += " AND A.EmployeeKey = @Employee1";
                    if (Employee2 != 0)
                        zSQL += " AND B.NewStaff = @Employee2";
                    if (Project != 0)
                        zSQL += " AND A.ProjectKey = @Project";
                    if (AssetID != string.Empty)
                        zSQL += " AND A.AssetID LIKE @AssetID";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    try
                    {
                        SqlConnection zConnect = new SqlConnection(zConnectionString);
                        zConnect.Open();
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@Project", SqlDbType.Int).Value = Project;
                        zCommand.Parameters.Add("@Employee1", SqlDbType.Int).Value = Employee1;
                        zCommand.Parameters.Add("@Employee2", SqlDbType.Int).Value = Employee2;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = "%" + AssetID + "%";
                        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                        zAdapter.Fill(zTable);
                        zCommand.Dispose();
                        zConnect.Close();
                    }
                    catch (Exception ex)
                    {
                        string zstrMessage = ex.ToString();
                    }
                    return zTable;
                case "ResaleHouse":
                    return zTable;
                default:
                    return zTable;
            }
        }
        public static DataTable ManagerProduct_Sent(int DepartmentKey, int EmployeeKey)
        {
            string SQL = "SELECT *, dbo.FNC_GetNameEmployee(OldStaff) AS EmployeeName, dbo.FNC_GetNameEmployee(NewStaff) AS NewStaffName FROM SYS_LogTranfer WHERE Confirmed = 0";
            if (EmployeeKey != 0)
                SQL += " AND NewStaff = " + EmployeeKey;
            if (DepartmentKey != 0)
                SQL += " AND DepartmentKey = " + DepartmentKey;
            //SQL += " AND dbo.FNC_GetDepartment(" + EmployeeKey + ")=" + DepartmentKey;
            SqlContext Sql = new SqlContext();
            return Sql.GetData(SQL);
        }

        //----------------------------------             
        public static int CheckTransfer(int AssetKey,int EmployeeKey)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetObject("SELECT ObjectID FROM SYS_LogTranfer WHERE ObjectTable = 'ResaleApartment' AND ObjectID=" + AssetKey + " AND NewStaff = " + EmployeeKey + "  AND Confirmed =0").ToInt();
        }
        public static int CheckReminder(int AssetKey)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetObject("SELECT ID FROM SYS_Notification WHERE ObjectTableName = 'PUL_Resale_Apartment' AND ObjectTable ='" + AssetKey + "' AND IsRead =0").ToInt();
        }
        public static DataTable GetPrefix_AssetName(int ProjectKey, string Name)
        {
            SqlContext Sql = new SqlContext();
            return Sql.GetData("SELECT AssetKey, AssetID FROM PUL_Resale_Apartment A WHERE ProjectKey = " + ProjectKey + " AND AssetID LIKE N'%" + Name + "%'");
        }
        public static DataTable GetFloor(int ProjectKey, string Tower, string Floor)
        {
            string SQL = "SELECT DISTINCT ID2 FROM PUL_Resale_Apartment A WHERE ProjectKey = " + ProjectKey + " AND ID1 = '" + Tower + "' ";
            if (Floor != "0")
                SQL += " AND ID2 = '" + Floor + "'";
            SQL += " ORDER BY ID2 DESC";
            SqlContext Sql = new SqlContext();
            return Sql.GetData(SQL);
        }
        public static DataTable GetAsset(int ProjectKey, string Tower, string Floor, string AssetNo)
        {
            string SQL = @"
SELECT A.AssetKey, A.AssetID, ID1, ID2, ID3, A.IsResale, 
NumberBed AS Room, ApartmentStatus AS [Status], 
WallArea AS AreaWall, A.PurposeSearch AS Want, 
dbo.FNC_AssetCategoryName(A.CategoryKey) AS Category, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) AS EmployeeTaskName,
A.EmployeeKey AS EmployeeManager, 
B.StatusKey, B.[Status] AS StatusTask, B.AutoKey, B.InfoKey, 
B.EmployeeKey AS EmployeeTask, 
B.DateCreated, B.DateReplied,
C.CustomerName, C.Phone1, C.Phone2
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetTask(A.ProjectKey, RTRIM(A.AssetID)) B
OUTER APPLY dbo.GetOwner(A.AssetKey) C
WHERE ProjectKey = " + ProjectKey + " AND ID1 = '" + Tower + "'";
            if (AssetNo != string.Empty)
                SQL += " AND ID3='" + AssetNo + "'";
            if (Floor != "0")
                SQL += " AND ID2 = '" + Floor + "'";
            SQL += " ORDER BY ID3";
            SqlContext Sql = new SqlContext();
            return Sql.GetData(SQL);
        }
    }
}