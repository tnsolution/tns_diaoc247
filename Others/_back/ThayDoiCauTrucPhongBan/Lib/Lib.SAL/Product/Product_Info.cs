﻿using Lib.Config;
using Lib.SYS;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SAL
{
    public class Product_Info
    {
        ItemAsset _ItemAsset = new ItemAsset();
        public ItemAsset ItemAsset
        {
            get
            {
                return _ItemAsset;
            }

            set
            {
                _ItemAsset = value;
            }
        }
        public Product_Info()
        {

        }
        public Product_Info(int Key, string Type, string Project)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.ApartmentStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, A.PurposeSearch, 
A.PurposeShow, A.WallArea, A.WaterArea,
A.DirectionDoor AS Door,
A.DirectionView AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberBed AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetKey = @AssetKey AND A.ProjectKey = @ProjectKey";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project.ToInt();
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();

                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                case "ResaleHouse":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.HouseStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, 
A.PurposeSearch, 
A.PurposeShow, 
Area AS WallArea, '' AS WaterArea,
A.[MainDoor] AS Door, '' AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberRoom AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_House A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetKey = @AssetKey AND A.ProjectKey = @ProjectKey";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project.ToInt();
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();
                            //_ItemAsset.IsShare = zReader["IsShare"].ToString();
                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                default:
                    break;
            }
        }
        public Product_Info(string AssetID, string Type, string Project)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.ApartmentStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, A.PurposeSearch, 
A.PurposeShow, A.WallArea, A.WaterArea,
A.DirectionDoor AS Door,
A.DirectionView AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase, 
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberBed AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.AutoKey, B.Address1, B.Address2, B.Email AS Email1
FROM PUL_Resale_Apartment A
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetID = @AssetID AND A.ProjectKey = @ProjectKey";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = AssetID;
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project.ToInt();
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.Price = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();

                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Email1 = zReader["Email1"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                case "ResaleHouse":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.HouseStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, 
A.PurposeSearch, 
A.PurposeShow, 
Area AS WallArea, '' AS WaterArea,
A.[MainDoor] AS Door, '' AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberRoom AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_House A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetID = @AssetID AND A.ProjectKey = @ProjectKey";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = AssetID;
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project.ToInt();
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();
                            //_ItemAsset.IsShare = zReader["IsShare"].ToString();
                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;
            }
        }
        public Product_Info(string AssetID, string Type)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.ApartmentStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, A.PurposeSearch, 
A.PurposeShow, A.WallArea, A.WaterArea,
A.DirectionDoor AS Door,
A.DirectionView AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase, 
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberBed AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.AutoKey, B.Address1, B.Address2, B.Email AS Email1
FROM PUL_Resale_Apartment A
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetID = @AssetID";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = AssetID;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.Price = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();

                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Email1 = zReader["Email1"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                case "ResaleHouse":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.HouseStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, 
A.PurposeSearch, 
A.PurposeShow, 
Area AS WallArea, '' AS WaterArea,
A.[MainDoor] AS Door, '' AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberRoom AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_House A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetID = @AssetID";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = AssetID;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();
                            //_ItemAsset.IsShare = zReader["IsShare"].ToString();
                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;
            }
        }
        public Product_Info(int Key, string Type)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.ApartmentStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, A.PurposeSearch, 
A.PurposeShow, A.WallArea, A.WaterArea,
A.DirectionDoor AS Door,
A.DirectionView AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberBed AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetKey = @AssetKey";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();

                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                case "ResaleHouse":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.HouseStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, 
A.PurposeSearch, 
A.PurposeShow, 
Area AS WallArea, '' AS WaterArea,
A.[MainDoor] AS Door, '' AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberRoom AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_House A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetKey = @AssetKey";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();
                            //_ItemAsset.IsShare = zReader["IsShare"].ToString();
                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// lay thong tin san pham
        /// </summary>
        /// <param name="Key">ma</param>
        /// <param name="Type">loai</param>
        /// <param name="EmployeeKey">key người xem kiem, dung kiem tra có được phép chỉnh sửa hay ko</param>
        public Product_Info(int Key, string Type, int EmployeeKey)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.ApartmentStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, 
A.PurposeSearch, 
A.PurposeShow, A.WallArea, A.WaterArea,
A.DirectionDoor AS Door,
A.DirectionView AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberBed AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.ApartmentStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetPermition(A.AssetKey, 'ResaleApartment', @Employee) AS IsShare,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_Apartment A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetKey = @AssetKey";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = EmployeeKey;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();
                            _ItemAsset.IsShare = zReader["IsShare"].ToString();
                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                case "ResaleHouse":
                    #region [Get Resale Apartment]
                    zSQL = @"
SELECT A.AssetKey, A.AssetID, A.ID1, A.ID2, A.ID3,
A.DepartmentKey, A.EmployeeKey, 
A.HouseStatus AS [StatusKey],
A.Legal AS LegalKey, A.ProjectKey,
A.CategoryInside AS FurnitureKey,
A.CategoryKey, 
A.PurposeSearch, 
A.PurposeShow, 
Area AS WallArea, '' AS WaterArea,
A.[MainDoor] AS Door,
'' AS [View], 
A.Description, A.DateContractEnd,
A.PricePurchase,
A.PricePurchase_VND, A.PricePurchase_USD,
A.Price_VND, A.PriceRent_VND, 
A.Price_USD, A.PriceRent_USD, 
A.Hot, A.NumberRoom AS Room,
A.Title, A.Web, A.WebPublic, A.IsResale,
A.CreatedBy, A.CreatedDate, A.CreatedName,
A.ModifiedBy, A.ModifiedDate, A.ModifiedName,
dbo.FNC_GetEmployeePhone(A.EmployeeKey) AS EmployeePhone,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetProjectName (A.ProjectKey) AS ProjectName,
dbo.FNC_GetAddressProject(A.ProjectKey) AS [Address],
dbo.FNC_AssetCategoryName(A.CategoryKey) AS CategoryName,
dbo.FNC_SysCategoryName(A.HouseStatus) AS [Status],
dbo.FNC_SysCategoryName(A.Legal) AS Legal,
dbo.FNC_SysCategoryName(A.CategoryInside) AS Furniture,
dbo.FNC_GetPermition(A.AssetKey, 'ResaleHouse', @Employee) AS IsShare,
B.CustomerName, B.Phone1, B.Phone2, B.Address1, B.Address2, B.AutoKey, B.Email
FROM PUL_Resale_House A 
OUTER APPLY dbo.GetOwner(A.AssetKey) B
WHERE A.AssetKey = @AssetKey";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = Key;
                        zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = EmployeeKey;
                        SqlDataReader zReader = zCommand.ExecuteReader();
                        if (zReader.HasRows)
                        {
                            zReader.Read();
                            _ItemAsset.AssetKey = zReader["AssetKey"].ToString();
                            _ItemAsset.ID1 = zReader["ID1"].ToString();
                            _ItemAsset.ID2 = zReader["ID2"].ToString();
                            _ItemAsset.ID3 = zReader["ID3"].ToString();
                            _ItemAsset.AssetID = zReader["AssetID"].ToString();
                            _ItemAsset.StatusKey = zReader["StatusKey"].ToString();
                            _ItemAsset.Status = zReader["Status"].ToString();
                            _ItemAsset.LegalKey = zReader["LegalKey"].ToString();
                            _ItemAsset.Legal = zReader["Legal"].ToString();
                            _ItemAsset.AssetType = Type;
                            _ItemAsset.AreaWall = zReader["WallArea"].ToString();
                            _ItemAsset.AreaWater = zReader["WaterArea"].ToString();
                            _ItemAsset.ProjectKey = zReader["ProjectKey"].ToString();
                            _ItemAsset.ProjectName = zReader["ProjectName"].ToString();
                            _ItemAsset.Address = zReader["Address"].ToString();
                            _ItemAsset.CategoryKey = zReader["CategoryKey"].ToString();
                            _ItemAsset.CategoryName = zReader["CategoryName"].ToString();
                            _ItemAsset.DepartmentKey = zReader["DepartmentKey"].ToString();
                            _ItemAsset.EmployeeKey = zReader["EmployeeKey"].ToString();
                            _ItemAsset.EmployeeName = zReader["EmployeeName"].ToString();
                            _ItemAsset.EmployeePhone = zReader["EmployeePhone"].ToString();
                            _ItemAsset.FurnitureKey = zReader["FurnitureKey"].ToString();
                            _ItemAsset.Furniture = zReader["Furniture"].ToString();
                            _ItemAsset.Description = zReader["Description"].ToString();
                            _ItemAsset.View = zReader["View"].ToString();
                            _ItemAsset.Door = zReader["Door"].ToString();
                            _ItemAsset.Room = zReader["Room"].ToString();
                            _ItemAsset.PricePurchase = zReader["PricePurchase"].ToDoubleString();
                            _ItemAsset.PricePurchase_USD = zReader["PricePurchase_USD"].ToDoubleString();
                            _ItemAsset.PricePurchase_VND = zReader["PricePurchase_VND"].ToDoubleString();
                            _ItemAsset.PriceRent_USD = zReader["PriceRent_USD"].ToDoubleString();
                            _ItemAsset.Price_USD = zReader["Price_USD"].ToDoubleString();
                            _ItemAsset.PriceRent_VND = zReader["PriceRent_VND"].ToDoubleString();
                            _ItemAsset.Price_VND = zReader["Price_VND"].ToDoubleString();
                            _ItemAsset.WebTitle = zReader["Title"].ToString();
                            _ItemAsset.WebPublished = zReader["WebPublic"].ToString();
                            _ItemAsset.WebContent = zReader["Web"].ToString();
                            _ItemAsset.Hot = zReader["Hot"].ToString();
                            _ItemAsset.PurposeKey = zReader["PurposeSearch"].ToString();
                            _ItemAsset.Purpose = zReader["PurposeShow"].ToString();
                            _ItemAsset.IsResale = zReader["IsResale"].ToString();
                            _ItemAsset.IsShare = zReader["IsShare"].ToString();
                            _ItemAsset.CustomerName = zReader["CustomerName"].ToString();
                            _ItemAsset.Phone1 = zReader["Phone1"].ToString();
                            _ItemAsset.Phone2 = zReader["Phone2"].ToString();
                            _ItemAsset.Address1 = zReader["Address1"].ToString();
                            _ItemAsset.Address2 = zReader["Address2"].ToString();
                            _ItemAsset.AutoKey = zReader["AutoKey"].ToString();
                            _ItemAsset.Email1 = zReader["Email"].ToString();

                            if (zReader["DateContractEnd"] != DBNull.Value)
                                _ItemAsset.DateContractEnd = Convert.ToDateTime(zReader["DateContractEnd"]).ToString("dd/MM/yyyy");
                            if (zReader["CreatedDate"] != DBNull.Value)
                                _ItemAsset.CreatedDate = Convert.ToDateTime(zReader["CreatedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.CreatedBy = zReader["CreatedBy"].ToString();
                            _ItemAsset.CreatedName = zReader["CreatedName"].ToString();
                            if (zReader["ModifiedDate"] != DBNull.Value)
                                _ItemAsset.ModifiedDate = Convert.ToDateTime(zReader["ModifiedDate"]).ToString("dd/MM/yyyy");
                            _ItemAsset.ModifiedBy = zReader["ModifiedBy"].ToString();
                            _ItemAsset.ModifiedName = zReader["ModifiedName"].ToString();
                        }
                        zReader.Close();
                        zCommand.Dispose();
                    }
                    catch (Exception Err) { _ItemAsset.Message = Err.ToString(); }
                    finally { zConnect.Close(); }
                    #endregion
                    break;

                default:
                    break;
            }
        }
        public string Delete(string Type)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    zSQL = "DELETE FROM PUL_Resale_Apartment WHERE AssetKey = @AssetKey";
                    break;

                case "ResaleHouse":
                    zSQL = "DELETE FROM PUL_Resale_House WHERE AssetKey = @AssetKey";
                    break;

            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = ItemAsset.AssetKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                ItemAsset.Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save(string Type)
        {
            if (_ItemAsset.AssetKey.ToInt() > 0)
                return Update(Type);
            else
                return Create(Type);
        }
        public string Create(string Type)
        {
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region ResaleApartment
                    zSQL = @"
INSERT INTO PUL_Resale_Apartment (
ProjectKey ,CategoryKey ,ID1 ,ID2 ,ID3 ,WallArea ,WaterArea ,AssetID ,Legal ,
ApartmentStatus ,DepartmentKey ,EmployeeKey ,
PurposeSearch ,PurposeShow ,DirectionView ,DirectionDoor ,NumberBed ,CategoryInside ,Description ,
PricePurchase, PricePurchase_VND, PricePurchase_USD, PriceRent_USD, Price_USD , PriceRent_VND, Price_VND,
DateContractEnd ,Title ,Web ,WebPublic ,Hot, IsResale,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) 
VALUES (
@ProjectKey ,@CategoryKey ,@ID1 ,@ID2 ,@ID3 ,@WallArea ,@WaterArea ,@AssetID ,@Legal ,
@ApartmentStatus ,@DepartmentKey ,@EmployeeKey,
@PurposeSearch ,@PurposeShow ,@DirectionView ,@DirectionDoor ,@NumberBed ,@CategoryInside ,@Description ,
@PricePurchase, @PricePurchase_VND, @PricePurchase_USD,@PriceRent_USD, @Price_USD , @PriceRent_VND, @Price_VND,
@DateContractEnd ,@Title ,@Web ,@WebPublic ,@Hot ,@IsResale,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName )
SELECT AssetKey FROM PUL_Resale_Apartment WHERE AssetKey = SCOPE_IDENTITY() ";

                    string zResult = "";
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        #region Paramater
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ItemAsset.ProjectKey.ToInt();
                        zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _ItemAsset.CategoryKey.ToInt();
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ItemAsset.ID1;
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ItemAsset.ID2;
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ItemAsset.ID3;
                        zCommand.Parameters.Add("@WallArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWall;
                        zCommand.Parameters.Add("@WaterArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWater;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _ItemAsset.AssetID;
                        zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _ItemAsset.LegalKey.ToInt();
                        zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.NVarChar).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _ItemAsset.DepartmentKey.ToInt();
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _ItemAsset.EmployeeKey.ToInt();
                        zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _ItemAsset.PurposeKey.Trim();
                        zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _ItemAsset.Purpose.Trim();
                        zCommand.Parameters.Add("@DirectionView", SqlDbType.NVarChar).Value = _ItemAsset.View;
                        zCommand.Parameters.Add("@DirectionDoor", SqlDbType.NVarChar).Value = _ItemAsset.Door;
                        zCommand.Parameters.Add("@NumberBed", SqlDbType.Int).Value = _ItemAsset.Room.ToInt();
                        zCommand.Parameters.Add("@CategoryInside", SqlDbType.Int).Value = _ItemAsset.FurnitureKey.ToInt();
                        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _ItemAsset.Description;

                        double PricePurchase = 0;
                        double.TryParse(_ItemAsset.PricePurchase, out PricePurchase);
                        double Price_USD = 0;
                        double.TryParse(_ItemAsset.Price_USD, out Price_USD);
                        double PriceRent_USD = 0;
                        double.TryParse(_ItemAsset.PriceRent_USD, out PriceRent_USD);
                        double Price_VND = 0;
                        double.TryParse(_ItemAsset.Price_VND, out Price_VND);
                        double PriceRent_VND = 0;
                        double.TryParse(_ItemAsset.PriceRent_VND, out PriceRent_VND);
                        double PricePurchase_USD = 0;
                        double.TryParse(_ItemAsset.PricePurchase_USD, out PricePurchase_USD);
                        double PricePurchase_VND = 0;
                        double.TryParse(_ItemAsset.PricePurchase_VND, out PricePurchase_VND);

                        zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = PricePurchase;
                        zCommand.Parameters.Add("@PricePurchase_USD", SqlDbType.Money).Value = PricePurchase_USD;
                        zCommand.Parameters.Add("@PricePurchase_VND", SqlDbType.Money).Value = PricePurchase_VND;
                        zCommand.Parameters.Add("@PriceRent_USD", SqlDbType.Money).Value = PriceRent_USD;
                        zCommand.Parameters.Add("@Price_USD", SqlDbType.Money).Value = Price_USD;
                        zCommand.Parameters.Add("@PriceRent_VND", SqlDbType.Money).Value = PriceRent_VND;
                        zCommand.Parameters.Add("@Price_VND", SqlDbType.Money).Value = Price_VND;

                        if (_ItemAsset.DateContractEnd == string.Empty)
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                        else
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = Tools.ConvertToDate(_ItemAsset.DateContractEnd, string.Empty);

                        zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _ItemAsset.WebTitle;
                        zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _ItemAsset.WebContent;
                        zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _ItemAsset.WebPublished.ToInt();
                        zCommand.Parameters.Add("@Hot", SqlDbType.Int).Value = _ItemAsset.Hot.ToInt();
                        zCommand.Parameters.Add("@IsResale", SqlDbType.Int).Value = _ItemAsset.IsResale.ToInt();

                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _ItemAsset.CreatedBy;
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _ItemAsset.CreatedName;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        _ItemAsset.AssetKey = zCommand.ExecuteScalar().ToString();
                        #endregion                        
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;
                case "ResaleHouse":
                    #region ResaleHouse
                    zSQL = @"
INSERT INTO PUL_Resale_House (
ProjectKey ,ID1 ,ID2 ,ID3 ,GroundSize ,GroundArea ,AssetID,
HouseStatus ,Legal ,DepartmentKey ,EmployeeKey ,CategoryInside,PurposeSearch ,PurposeShow ,
MainDoor ,NumberRoom ,Description ,DateContractEnd ,
Web ,Title ,WebPublic ,Hot ,
IsResale ,PriceRent_USD ,Price_USD ,Price_VND ,PriceRent_VND ,PricePurchase_VND ,PricePurchase_USD ,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName )
VALUES (
@ProjectKey ,@ID1 ,@ID2 ,@ID3 ,@WaterArea ,@WallArea,@AssetID,
@ApartmentStatus ,@Legal ,@DepartmentKey ,@EmployeeKey ,@CategoryInside ,@PurposeSearch ,@PurposeShow ,
@DirectionDoor  ,@NumberBed ,@Description  ,@DateContractEnd ,
@Web ,@Title ,@WebPublic ,@Hot ,
@IsResale ,@PriceRent_USD ,@Price_USD ,@Price_VND ,@PriceRent_VND ,@PricePurchase_VND ,@PricePurchase_USD ,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName )
SELECT AssetKey FROM PUL_Resale_House WHERE AssetKey = SCOPE_IDENTITY() ";

                    zResult = "";
                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey.ToInt();
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ItemAsset.ProjectKey.ToInt();
                        zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _ItemAsset.CategoryKey.ToInt();
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ItemAsset.ID1;
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ItemAsset.ID2;
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ItemAsset.ID3;
                        zCommand.Parameters.Add("@WallArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWall;
                        zCommand.Parameters.Add("@WaterArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWater;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _ItemAsset.AssetID;
                        zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _ItemAsset.LegalKey.ToInt();
                        zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.NVarChar).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _ItemAsset.DepartmentKey.ToInt();
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _ItemAsset.EmployeeKey.ToInt();
                        zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _ItemAsset.PurposeKey.Trim();
                        zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _ItemAsset.Purpose.Trim();
                        zCommand.Parameters.Add("@DirectionDoor", SqlDbType.NVarChar).Value = _ItemAsset.Door;
                        zCommand.Parameters.Add("@NumberBed", SqlDbType.Int).Value = _ItemAsset.Room.ToInt();
                        zCommand.Parameters.Add("@CategoryInside", SqlDbType.Int).Value = _ItemAsset.FurnitureKey.ToInt();
                        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _ItemAsset.Description;

                        double PricePurchase = 0;
                        double.TryParse(_ItemAsset.PricePurchase, out PricePurchase);
                        double Price_USD = 0;
                        double.TryParse(_ItemAsset.Price_USD, out Price_USD);
                        double PriceRent_USD = 0;
                        double.TryParse(_ItemAsset.PriceRent_USD, out PriceRent_USD);
                        double Price_VND = 0;
                        double.TryParse(_ItemAsset.Price_VND, out Price_VND);
                        double PriceRent_VND = 0;
                        double.TryParse(_ItemAsset.PriceRent_VND, out PriceRent_VND);
                        double PricePurchase_USD = 0;
                        double.TryParse(_ItemAsset.PricePurchase_USD, out PricePurchase_USD);
                        double PricePurchase_VND = 0;
                        double.TryParse(_ItemAsset.PricePurchase_VND, out PricePurchase_VND);

                        zCommand.Parameters.Add("@PricePurchase_USD", SqlDbType.Money).Value = PricePurchase_USD;
                        zCommand.Parameters.Add("@PricePurchase_VND", SqlDbType.Money).Value = PricePurchase_VND;
                        zCommand.Parameters.Add("@PriceRent_USD", SqlDbType.Money).Value = PriceRent_USD;
                        zCommand.Parameters.Add("@PriceRent_VND", SqlDbType.Money).Value = PriceRent_VND;
                        zCommand.Parameters.Add("@Price_USD", SqlDbType.Money).Value = Price_USD;
                        zCommand.Parameters.Add("@Price_VND", SqlDbType.Money).Value = Price_VND;

                        if (_ItemAsset.DateContractEnd == string.Empty)
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                        else
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = Tools.ConvertToDate(_ItemAsset.DateContractEnd, string.Empty);

                        zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _ItemAsset.WebTitle;
                        zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _ItemAsset.WebContent;
                        zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _ItemAsset.WebPublished.ToInt();
                        zCommand.Parameters.Add("@Hot", SqlDbType.Int).Value = _ItemAsset.Hot.ToInt();
                        zCommand.Parameters.Add("@IsResale", SqlDbType.Int).Value = _ItemAsset.IsResale.ToInt();

                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _ItemAsset.CreatedBy;
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _ItemAsset.CreatedName;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        _ItemAsset.AssetKey = zCommand.ExecuteScalar().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion                    
                    return zResult;

                default:
                    return string.Empty;
            }
        }
        public string UpdateMobile(string Type)
        {
            string zResult = "";
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region MyRegion
                    zSQL = @"
UPDATE PUL_Resale_Apartment SET 
ApartmentStatus = @ApartmentStatus , 
Description = @Description,
ModifiedName = @ModifiedName, 
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy,
WHERE AssetKey = @AssetKey";
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey;
                        zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.Int).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _ItemAsset.Description;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                case "ResaleHouse":
                    #region MyRegion
                    zSQL = @"
UPDATE PUL_Resale_House SET 
HouseStatus = @HouseStatus , 
Description =@Description,
ModifiedName = @ModifiedName, 
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy,
WHERE AssetKey = @AssetKey";
                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey;
                        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _ItemAsset.Description;
                        zCommand.Parameters.Add("@HouseStatus", SqlDbType.Int).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                default:
                    return string.Empty;
            }
        }
        public string Update(string Type)
        {
            string zResult = "";
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region MyRegion
                    zSQL = @"
UPDATE PUL_Resale_Apartment SET 
ProjectKey = @ProjectKey,
CategoryKey = @CategoryKey,
ID1 = @ID1, 
ID2 = @ID2, 
ID3 = @ID3,
WallArea = @WallArea,
WaterArea = @WaterArea, 
AssetID = @AssetID,
DepartmentKey = @DepartmentKey, 
Legal = @Legal,
ApartmentStatus = @ApartmentStatus,                                       
PurposeSearch = @PurposeSearch,
PurposeShow = @PurposeShow,
DirectionView = @DirectionView,
DirectionDoor = @DirectionDoor,
NumberBed = @NumberBed,
CategoryInside = @CategoryInside,
Description = @Description,

PriceRent_USD = @PriceRent_USD, PricePurchase_VND =@PricePurchase_VND, PricePurchase_USD=@PricePurchase_USD,
Price_USD = @Price_USD,
PriceRent_VND = @PriceRent_VND,
Price_VND = @Price_VND,
DateContractEnd = @DateContractEnd,
Web = @Web, 
WebPublic = @WebPublic, 
Title = @Title,
Hot = @Hot, IsResale = @IsResale,
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy,
ModifiedName = @ModifiedName
WHERE AssetKey = @AssetKey";
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey.ToInt();
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ItemAsset.ProjectKey.ToInt();
                        zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _ItemAsset.CategoryKey.ToInt();
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ItemAsset.ID1;
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ItemAsset.ID2;
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ItemAsset.ID3;
                        zCommand.Parameters.Add("@WallArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWall;
                        zCommand.Parameters.Add("@WaterArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWater;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _ItemAsset.AssetID;
                        zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _ItemAsset.LegalKey.ToInt();
                        zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.NVarChar).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _ItemAsset.DepartmentKey.ToInt();
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _ItemAsset.EmployeeKey.ToInt();
                        zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _ItemAsset.PurposeKey.Trim();
                        zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _ItemAsset.Purpose.Trim();
                        zCommand.Parameters.Add("@DirectionView", SqlDbType.NVarChar).Value = _ItemAsset.View;
                        zCommand.Parameters.Add("@DirectionDoor", SqlDbType.NVarChar).Value = _ItemAsset.Door;
                        zCommand.Parameters.Add("@NumberBed", SqlDbType.Int).Value = _ItemAsset.Room.ToInt();
                        zCommand.Parameters.Add("@CategoryInside", SqlDbType.Int).Value = _ItemAsset.FurnitureKey.ToInt();
                        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _ItemAsset.Description;

                        double PricePurchase = 0;
                        double.TryParse(_ItemAsset.PricePurchase, out PricePurchase);
                        double Price_USD = 0;
                        double.TryParse(_ItemAsset.Price_USD, out Price_USD);
                        double PriceRent_USD = 0;
                        double.TryParse(_ItemAsset.PriceRent_USD, out PriceRent_USD);
                        double Price_VND = 0;
                        double.TryParse(_ItemAsset.Price_VND, out Price_VND);
                        double PriceRent_VND = 0;
                        double.TryParse(_ItemAsset.PriceRent_VND, out PriceRent_VND);
                        double PricePurchase_USD = 0;
                        double.TryParse(_ItemAsset.PricePurchase_USD, out PricePurchase_USD);
                        double PricePurchase_VND = 0;
                        double.TryParse(_ItemAsset.PricePurchase_VND, out PricePurchase_VND);

                        zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = PricePurchase;
                        zCommand.Parameters.Add("@PricePurchase_USD", SqlDbType.Money).Value = PricePurchase_USD;
                        zCommand.Parameters.Add("@PricePurchase_VND", SqlDbType.Money).Value = PricePurchase_VND;
                        zCommand.Parameters.Add("@PriceRent_USD", SqlDbType.Money).Value = PriceRent_USD;
                        zCommand.Parameters.Add("@Price_USD", SqlDbType.Money).Value = Price_USD;
                        zCommand.Parameters.Add("@PriceRent_VND", SqlDbType.Money).Value = PriceRent_VND;
                        zCommand.Parameters.Add("@Price_VND", SqlDbType.Money).Value = Price_VND;

                        if (_ItemAsset.DateContractEnd == string.Empty)
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                        else
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = Tools.ConvertToDate(_ItemAsset.DateContractEnd, string.Empty);

                        zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _ItemAsset.WebTitle;
                        zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _ItemAsset.WebContent;
                        zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _ItemAsset.WebPublished.ToInt();
                        zCommand.Parameters.Add("@Hot", SqlDbType.Int).Value = _ItemAsset.Hot.ToInt();
                        zCommand.Parameters.Add("@IsResale", SqlDbType.Int).Value = _ItemAsset.IsResale.ToInt();

                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                case "ResaleHouse":
                    #region MyRegion
                    zSQL = @"
UPDATE PUL_Resale_House SET 
ProjectKey = @ProjectKey,
CategoryKey = @CategoryKey,
ID1 = @ID1, 
ID2 = @ID2, 
ID3 = @ID3,
AssetID = @AssetID,
DepartmentKey = @DepartmentKey,
Legal = @Legal,
HouseStatus = @ApartmentStatus,                                       
PurposeSearch = @PurposeSearch,
PurposeShow = @PurposeShow,
MainDoor = @DirectionDoor,
NumberRoom = @NumberBed,
CategoryInside = @CategoryInside,
Description = @Description,

PriceRent_USD = @PriceRent_USD, PricePurchase_VND =@PricePurchase_VND, PricePurchase_USD=@PricePurchase_USD,
Price_USD = @Price_USD, PriceRent_VND = @PriceRent_VND, Price_VND = @Price_VND,
DateContractEnd = @DateContractEnd,
Web = @Web, 
WebPublic = @WebPublic, 
Title = @Title,
Hot = @Hot, IsResale = @IsResale,
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy,
ModifiedName = @ModifiedName
WHERE AssetKey = @AssetKey";
                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey.ToInt();
                        zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ItemAsset.ProjectKey.ToInt();
                        zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _ItemAsset.CategoryKey.ToInt();
                        zCommand.Parameters.Add("@ID1", SqlDbType.NVarChar).Value = _ItemAsset.ID1;
                        zCommand.Parameters.Add("@ID2", SqlDbType.NVarChar).Value = _ItemAsset.ID2;
                        zCommand.Parameters.Add("@ID3", SqlDbType.NVarChar).Value = _ItemAsset.ID3;
                        zCommand.Parameters.Add("@WallArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWall;
                        zCommand.Parameters.Add("@WaterArea", SqlDbType.NVarChar).Value = _ItemAsset.AreaWater;
                        zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _ItemAsset.AssetID;
                        zCommand.Parameters.Add("@Legal", SqlDbType.Int).Value = _ItemAsset.LegalKey.ToInt();
                        zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.NVarChar).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _ItemAsset.DepartmentKey.ToInt();
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _ItemAsset.EmployeeKey.ToInt();
                        zCommand.Parameters.Add("@PurposeSearch", SqlDbType.NVarChar).Value = _ItemAsset.PurposeKey.Trim();
                        zCommand.Parameters.Add("@PurposeShow", SqlDbType.NVarChar).Value = _ItemAsset.Purpose.Trim();
                        zCommand.Parameters.Add("@DirectionView", SqlDbType.NVarChar).Value = _ItemAsset.View;
                        zCommand.Parameters.Add("@DirectionDoor", SqlDbType.NVarChar).Value = _ItemAsset.Door;
                        zCommand.Parameters.Add("@NumberBed", SqlDbType.Int).Value = _ItemAsset.Room.ToInt();
                        zCommand.Parameters.Add("@CategoryInside", SqlDbType.Int).Value = _ItemAsset.FurnitureKey.ToInt();
                        zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _ItemAsset.Description;

                        double PricePurchase = 0;
                        double.TryParse(_ItemAsset.PricePurchase, out PricePurchase);
                        double Price_USD = 0;
                        double.TryParse(_ItemAsset.Price_USD, out Price_USD);
                        double PriceRent_USD = 0;
                        double.TryParse(_ItemAsset.PriceRent_USD, out PriceRent_USD);
                        double Price_VND = 0;
                        double.TryParse(_ItemAsset.Price_VND, out Price_VND);
                        double PriceRent_VND = 0;
                        double.TryParse(_ItemAsset.PriceRent_VND, out PriceRent_VND);
                        double PricePurchase_USD = 0;
                        double.TryParse(_ItemAsset.PricePurchase_USD, out PricePurchase_USD);
                        double PricePurchase_VND = 0;
                        double.TryParse(_ItemAsset.PricePurchase_VND, out PricePurchase_VND);

                        zCommand.Parameters.Add("@PricePurchase", SqlDbType.Money).Value = PricePurchase;
                        zCommand.Parameters.Add("@PricePurchase_USD", SqlDbType.Money).Value = PricePurchase_USD;
                        zCommand.Parameters.Add("@PricePurchase_VND", SqlDbType.Money).Value = PricePurchase_VND;
                        zCommand.Parameters.Add("@PriceRent_USD", SqlDbType.Money).Value = PriceRent_USD;
                        zCommand.Parameters.Add("@Price_USD", SqlDbType.Money).Value = Price_USD;
                        zCommand.Parameters.Add("@PriceRent_VND", SqlDbType.Money).Value = PriceRent_VND;
                        zCommand.Parameters.Add("@Price_VND", SqlDbType.Money).Value = Price_VND;

                        if (_ItemAsset.DateContractEnd == string.Empty)
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                        else
                            zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = Tools.ConvertToDate(_ItemAsset.DateContractEnd, string.Empty);

                        zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _ItemAsset.WebTitle;
                        zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _ItemAsset.WebContent;
                        zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _ItemAsset.WebPublished.ToInt();
                        zCommand.Parameters.Add("@Hot", SqlDbType.Int).Value = _ItemAsset.Hot.ToInt();
                        zCommand.Parameters.Add("@IsResale", SqlDbType.Int).Value = _ItemAsset.IsResale.ToInt();

                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return string.Empty;

                default:
                    return string.Empty;
            }
        }
        public string UpdateManagerment(string Type)
        {
            string zResult = "";
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region [MyRegion]
                    zSQL = @"
UPDATE PUL_Resale_Apartment SET 
DepartmentKey = @DepartmentKey,
EmployeeKey = @EmployeeKey,
CreatedBy = @CreatedBy,
CreatedDate = GETDATE(),
CreatedName = @CreatedName
WHERE AssetKey = @AssetKey";

                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey.ToInt();
                        zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _ItemAsset.DepartmentKey.ToInt();
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _ItemAsset.EmployeeKey.ToInt();
                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                case "ResaleHouse":
                    #region [MyRegion]
                    zSQL = @"
UPDATE PUL_Resale_House SET 
DepartmentKey = @DepartmentKey,
EmployeeKey = @EmployeeKey,
CreatedBy = @CreatedBy,
CreatedDate = GETDATE(),
CreatedName = @CreatedName
WHERE AssetKey = @AssetKey";

                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.Text;
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey.ToInt();
                        zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _ItemAsset.DepartmentKey.ToInt();
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _ItemAsset.EmployeeKey.ToInt();
                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;
                default:
                    return string.Empty;
            }
        }
        public string UpdateStatus(string Type)
        {
            string zResult = "";
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region MyRegion
                    zSQL = @"
UPDATE PUL_Resale_Apartment SET 
ApartmentStatus = @ApartmentStatus ,
ModifiedName = @ModifiedName, 
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy,
WHERE AssetKey = @AssetKey";
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey;
                        zCommand.Parameters.Add("@ApartmentStatus", SqlDbType.Int).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                case "ResaleHouse":
                    #region MyRegion
                    zSQL = @"
UPDATE PUL_Resale_House SET 
HouseStatus = @HouseStatus ,
ModifiedName = @ModifiedName, 
ModifiedDate = GETDATE(),
ModifiedBy = @ModifiedBy,
WHERE AssetKey = @AssetKey";
                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey;
                        zCommand.Parameters.Add("@HouseStatus", SqlDbType.Int).Value = _ItemAsset.StatusKey;
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedBy;
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ItemAsset.ModifiedName;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                default:
                    return string.Empty;
            }
        }
        public string PublishWeb(string Type)
        {
            string zResult = "";
            string zSQL = "";
            switch (Type)
            {
                case "ResaleApartment":
                    #region MyRegion
                    zSQL += @"
UPDATE PUL_Resale_Apartment SET 
WebPublic = @WebPublic, 
Web = @Web, 
Title = @Title 
WHERE AssetKey = @AssetKey";
                    string zConnectionString = ConnectDataBase.ConnectionString;
                    SqlConnection zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey;
                        zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _ItemAsset.WebPublished;
                        zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _ItemAsset.WebContent;
                        zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _ItemAsset.WebTitle;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                case "ResaleHouse":
                    #region MyRegion
                    zSQL += @"
UPDATE PUL_Resale_House SET 
WebPublic = @WebPublic, 
Web = @Web, 
Title = @Title 
WHERE AssetKey = @AssetKey";
                    zConnectionString = ConnectDataBase.ConnectionString;
                    zConnect = new SqlConnection(zConnectionString);
                    zConnect.Open();
                    try
                    {
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                        zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _ItemAsset.AssetKey;
                        zCommand.Parameters.Add("@WebPublic", SqlDbType.Int).Value = _ItemAsset.WebPublished;
                        zCommand.Parameters.Add("@Web", SqlDbType.NVarChar).Value = _ItemAsset.WebContent;
                        zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _ItemAsset.WebTitle;
                        zResult = zCommand.ExecuteNonQuery().ToString();
                        zCommand.Dispose();
                    }
                    catch (Exception Err)
                    {
                        _ItemAsset.Message = Err.ToString();
                    }
                    finally
                    {
                        zConnect.Close();
                    }
                    #endregion
                    return zResult;

                default:
                    return string.Empty;
            }
        }
    }
}
