﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SAL
{
    public class Project_Assets_Data
    {
        public static DataTable List(int ProjectKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.Bed AS Room, B.CategoryName 
FROM PUL_Project_Assets A 
LEFT JOIN PUL_Category B ON A.CategoryKey = B.CategoryKey 
WHERE ProjectKey = @ProjectKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int ProjectKey, int Category, int Room, double FromPrice, double ToPrice)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.Bed AS Room, B.CategoryName 
FROM PUL_Project_Assets A 
LEFT JOIN PUL_Category B ON A.CategoryKey = B.CategoryKey 
WHERE ProjectKey = @ProjectKey";

            if (FromPrice > 0 && ToPrice > 0)
                zSQL += " AND A.Money BETWEEN " + FromPrice + " AND " + ToPrice;
            if (FromPrice > 0 && ToPrice == 0)
                zSQL += " AND A.Money <= " + ToPrice;
            if (FromPrice == 0 && ToPrice > 0)
                zSQL += " AND A.Money >= " + ToPrice;

            if (Category != 0)
                zSQL += " AND A.CategoryKey = @CategoryKey";
            if (Room != 0)
                zSQL += " AND A.Bed = @Room";
            zSQL += " ORDER BY Bed DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Room", SqlDbType.Int).Value = Room;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }              

        public static DataTable List(int Category, int Room, double FromPrice, double ToPrice)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.Bed AS Room, B.CategoryName 
FROM PUL_Project_Assets A 
LEFT JOIN PUL_Category B ON A.CategoryKey = B.CategoryKey 
WHERE 1=1";

            if (FromPrice > 0 && ToPrice > 0)
                zSQL += " AND A.Money BETWEEN " + FromPrice + " AND " + ToPrice;
            if (FromPrice > 0 && ToPrice == 0)
                zSQL += " AND A.Money <= " + ToPrice;
            if (FromPrice == 0 && ToPrice > 0)
                zSQL += " AND A.Money >= " + ToPrice;

            if (Category != 0)
                zSQL += " AND A.CategoryKey = @CategoryKey";
            if (Room != 0)
                zSQL += " AND A.Bed = @Room";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Room", SqlDbType.Int).Value = Room;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(int Category, int Room, double FromPrice, double ToPrice, int LoaiGia)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, A.Bed AS Room, B.CategoryName 
FROM PUL_Project_Assets A 
LEFT JOIN PUL_Category B ON A.CategoryKey = B.CategoryKey 
WHERE 1=1";
            if (LoaiGia == 1)
            {
                if (FromPrice > 0 && ToPrice > 0)
                    zSQL += " AND A.Money BETWEEN " + FromPrice + " AND " + ToPrice;
                if (FromPrice > 0 && ToPrice == 0)
                    zSQL += " AND A.Money <= " + ToPrice;
                if (FromPrice == 0 && ToPrice > 0)
                    zSQL += " AND A.Money >= " + ToPrice;
            }
            else
            {
                if (FromPrice > 0 && ToPrice > 0)
                    zSQL += " AND A.Money2 BETWEEN " + FromPrice + " AND " + ToPrice;
                if (FromPrice > 0 && ToPrice == 0)
                    zSQL += " AND A.Money2 <= " + ToPrice;
                if (FromPrice == 0 && ToPrice > 0)
                    zSQL += " AND A.Money2 >= " + ToPrice;
            }

            if (Category != 0)
                zSQL += " AND A.CategoryKey = @CategoryKey";
            if (Room != 0)
                zSQL += " AND A.Bed = @Room";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@Room", SqlDbType.Int).Value = Room;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
