﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Project_Assets_Info
    {

        #region [ Field Name ]
        private int _AssetKey = 0;
        private string _AssetID = "";
        private int _CategoryKey = 0;
        private string _CategoryID = "";
        private string _CategoryName = "";
        private int _ProjectKey = 0;
        private int _Bed = 0;
        private string _Name = "";
        private float _Area;
        private double _Money = 0;
        private double _Money2 = 0;
        private string _Description = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Project_Assets_Info()
        {
        }
        public Project_Assets_Info(int AssetKey)
        {
            string zSQL = "SELECT * FROM PUL_Project_Assets WHERE AssetKey = @AssetKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = AssetKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    _AssetID = zReader["AssetID"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryID = zReader["CategoryID"].ToString();
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    if (zReader["Bed"] != DBNull.Value)
                        _Bed = int.Parse(zReader["Bed"].ToString());
                    _Name = zReader["Name"].ToString();
                    if (zReader["Area"] != DBNull.Value)
                        _Area = float.Parse(zReader["Area"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Money2"] != DBNull.Value)
                        _Money2 = double.Parse(zReader["Money2"].ToString());
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }     
        #endregion

        #region [ Properties ]
        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public string AssetID
        {
            get { return _AssetID; }
            set { _AssetID = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public int Bed
        {
            get { return _Bed; }
            set { _Bed = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public float Area
        {
            get { return _Area; }
            set { _Area = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public double Money2
        {
            get
            {
                return _Money2;
            }

            set
            {
                _Money2 = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PUL_Project_Assets ("
        + " AssetID ,CategoryKey ,CategoryID ,ProjectKey ,Bed ,Name ,Area ,Money, Money2 ,Description ) "
         + " VALUES ( "
         + "@AssetID ,@CategoryKey ,@CategoryID ,@ProjectKey ,@Bed ,@Name ,@Area ,@Money, @Money2 ,@Description ) "
         + " SELECT AssetKey FROM PUL_Project_Assets WHERE AssetKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Char).Value = _CategoryID;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Bed", SqlDbType.Int).Value = _Bed;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Money2", SqlDbType.Money).Value = _Money2;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                _AssetKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE PUL_Project_Assets SET "
                        + " AssetID = @AssetID,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryID = @CategoryID,"
                        + " ProjectKey = @ProjectKey,"
                        + " Bed = @Bed,"
                        + " Name = @Name,"
                        + " Area = @Area,"
                        + " Money = @Money, Money2 = @Money2,"
                        + " Description = @Description"
                       + " WHERE AssetKey = @AssetKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.Char).Value = _CategoryID;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Bed", SqlDbType.Int).Value = _Bed;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Money2", SqlDbType.Money).Value = _Money2;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_AssetKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Project_Assets WHERE AssetKey = @AssetKey AND ProjectKey = @ProjectKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
