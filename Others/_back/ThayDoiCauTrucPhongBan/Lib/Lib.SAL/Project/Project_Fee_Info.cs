﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Project_Fee_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ProjectKey = 0;
        private string _Name = "";
        private double _Money = 0;
        private double _Money2 = 0;
        private string _Description = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Project_Fee_Info()
        {
        }
        public Project_Fee_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM PUL_Project_Fee WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    _Name = zReader["Name"].ToString();
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Money2"] != DBNull.Value)
                        _Money2 = double.Parse(zReader["Money2"].ToString());
                    _Description = zReader["Description"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }       
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public double Money2
        {
            get
            {
                return _Money2;
            }

            set
            {
                _Money2 = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PUL_Project_Fee ("
        + " ProjectKey ,Name ,Money, Money2 ,Description ) "
         + " VALUES ( "
         + "@ProjectKey ,@Name ,@Money, @Money ,@Description ) "
         + " SELECT AutoKey FROM PUL_Project_Fee WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Money2", SqlDbType.Money).Value = _Money2;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE PUL_Project_Fee SET "
                        + " ProjectKey = @ProjectKey,"
                        + " Name = @Name,"
                        + " Money = @Money, Money2 = @Money2"
                        + " Description = @Description"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = _Name;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Money2", SqlDbType.Money).Value = _Money2;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PUL_Project_Fee WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
