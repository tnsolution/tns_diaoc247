﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SAL
{
    public class ScheduleItem
    {
        public int ParentID { get; set; }
        public int ID { get; set; }
        public string Title { get; set; }
        public int EmployeeKey { get; set; }
        public int DepartmentKey { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public bool AllDay { get; set; }
    }
}
