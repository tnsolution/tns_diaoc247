﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SAL
{
    public class Schedule_Info
    {
        #region [ Field Name ]
        private int _TimeKey = 0;
        private int _EmployeeKey = 0;
        private string _EmployeeName = "";
        private int _DepartmentKey = 0;
        private string _UnitID = "";
        private DateTime _ScheduleDate;
        private string _CreatedBy = "";
        private DateTime _CreatedDate;
        private string _Title = "";
        private string _Description = "";
        private string _Start = "";
        private string _End = "";
        private bool _AllDay;
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _TimeKey; }
            set { _TimeKey = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string UnitID
        {
            get { return _UnitID; }
            set { _UnitID = value; }
        }
        public DateTime ScheduleDate
        {
            get { return _ScheduleDate; }
            set { _ScheduleDate = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string Title
        {
            get
            {
                return _Title;
            }

            set
            {
                _Title = value;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }

        public string Start
        {
            get
            {
                return _Start;
            }

            set
            {
                _Start = value;
            }
        }

        public string End
        {
            get
            {
                return _End;
            }

            set
            {
                _End = value;
            }
        }

        public bool AllDay
        {
            get
            {
                return _AllDay;
            }

            set
            {
                _AllDay = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Schedule_Info()
        {
        }
        public Schedule_Info(int EmployeeKey, DateTime ReportDate)
        {
            int Day = ReportDate.Day;
            int Month = ReportDate.Month;
            int Year = ReportDate.Year;

            string zSQL = "SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM HRM_Schedule WHERE DAY([Start]) = @Day AND MONTH([Start]) = @Month AND YEAR([Start]) = @Year AND EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@Day", SqlDbType.Int).Value = Day;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Title = zReader["Title"].ToString();
                    _Start = zReader["Start"].ToString();
                    _End = zReader["End"].ToString();
                    if (zReader["AllDay"] != DBNull.Value)
                        _AllDay = (bool)zReader["AllDay"];
                    _Description = zReader["Description"].ToString();

                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());

                    if (zReader["ScheduleDate"] != DBNull.Value)
                        _ScheduleDate = (DateTime)zReader["ScheduleDate"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Schedule_Info(int TimeKey)
        {
            string zSQL = "SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM HRM_Schedule WHERE TimeKey = @TimeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = TimeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Title = zReader["Title"].ToString();
                    _Start = zReader["Start"].ToString();
                    _End = zReader["End"].ToString();
                    if (zReader["AllDay"] != DBNull.Value)
                        _AllDay = (bool)zReader["AllDay"];
                    _Description = zReader["Description"].ToString();

                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());

                    if (zReader["ScheduleDate"] != DBNull.Value)
                        _ScheduleDate = (DateTime)zReader["ScheduleDate"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Schedule_Info(int Employee, bool isTop)
        {
            string zSQL = "SELECT TOP 1 *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM HRM_Schedule WHERE EmployeeKey = @Employee ORDER BY TimeKey DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["ScheduleDate"] != DBNull.Value)
                        _ScheduleDate = (DateTime)zReader["ScheduleDate"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Schedule ("
        + " EmployeeKey ,DepartmentKey ,Description ,ScheduleDate,Title,Start,[End],AllDay ,CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@EmployeeKey ,@DepartmentKey ,@Description ,@ScheduleDate,@Title,@Start,@End,@AllDay ,GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName ) "
         + " SELECT TimeKey FROM HRM_Schedule WHERE TimeKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                zCommand.Parameters.Add("@Start", SqlDbType.NVarChar).Value = _Start;
                zCommand.Parameters.Add("@End", SqlDbType.NVarChar).Value = _End;
                zCommand.Parameters.Add("@AllDay", SqlDbType.Bit).Value = _AllDay;

                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_ScheduleDate.Year == 0001)
                    zCommand.Parameters.Add("@ScheduleDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ScheduleDate", SqlDbType.DateTime).Value = _ScheduleDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _TimeKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Schedule SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " Description = @Description, [Start] = @Start, [End] = @End, AllDay = @AllDay,"
                        + " Title = @Title,"
                        + " ScheduleDate = @ScheduleDate,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE TimeKey = @TimeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Title", SqlDbType.NVarChar).Value = _Title;
                zCommand.Parameters.Add("@Start", SqlDbType.NVarChar).Value = _Start;
                zCommand.Parameters.Add("@End", SqlDbType.NVarChar).Value = _End;
                zCommand.Parameters.Add("@AllDay", SqlDbType.Bit).Value = _AllDay;

                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                if (_ScheduleDate.Year == 0001)
                    zCommand.Parameters.Add("@ScheduleDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ScheduleDate", SqlDbType.DateTime).Value = _ScheduleDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_TimeKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM HRM_Schedule WHERE TimeKey = @TimeKey
DELETE FROM HRM_ScheduleDetail WHERE TimeKey = @TimeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
