﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.SAL
{
    public class Trade_Info
    {
        #region [ Field Name ]
        private int _ShortTerm = 0;
        private int _HireMonth = 0;
        private double _InternalCost = 0;
        private int _Support = 0;
        private int _SupportBy = 0;
        private int _TransactionKey = 0;
        private DateTime _TransactionDate;
        private string _TransactionID = "";
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private int _TransactionCategory = 0;
        private int _TransactionType = 0;
        private DateTime _DateDeposit;
        private DateTime _DateContract;
        private DateTime _DateContractEnd;
        private DateTime _DateApprove;
        private int _ApprovedBy = 0;
        private int _IsApproved = 0;
        private int _ProjectKey = 0;
        private int _ProjectCategory = 0;
        private int _AssetKey = 0;
        private string _AssetID = "";
        private int _AssetCategory = 0;
        private string _AssetType = "";
        private float _Area;
        private int _ProvinceKey = 0;
        private int _DistrictKey = 0;
        private string _Street = "";
        private string _Address = "";
        private double _Amount = 0;
        private double _AmountVAT = 0;
        private int _VAT = 0;
        private double _TransactionFee = 0;
        private double _OtherFee = 0;
        private double _Income = 0;
        private string _Description = "";
        private int _IsDraft = 0;
        private int _IsDevined = 0;
        private int _IsFinish = 0;
        private int _FinishBy = 0;
        private DateTime _FinishDate;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private string _AddressProject = "";
        private string _ProjectName = "";
        private string _AssetCategoryName = "";
        private string _TradeCategoryName = "";
        private string _EmployeeName = "";
        private string _EmployeeApproved = "";
        private string _EmployeeFinished = "";
        private int _Source = 0;
        #endregion
        #region [ Properties ]
        public int Key
        {
            get { return _TransactionKey; }
            set { _TransactionKey = value; }
        }
        public DateTime TransactionDate
        {
            get { return _TransactionDate; }
            set { _TransactionDate = value; }
        }
        public string TransactionID
        {
            get { return _TransactionID; }
            set { _TransactionID = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int TradeCategoryKey
        {
            get { return _TransactionCategory; }
            set { _TransactionCategory = value; }
        }
        public int TransactionType
        {
            get { return _TransactionType; }
            set { _TransactionType = value; }
        }
        public DateTime DateDeposit
        {
            get { return _DateDeposit; }
            set { _DateDeposit = value; }
        }
        public DateTime DateContract
        {
            get { return _DateContract; }
            set { _DateContract = value; }
        }
        public DateTime DateContractEnd
        {
            get { return _DateContractEnd; }
            set { _DateContractEnd = value; }
        }
        public DateTime DateApprove
        {
            get { return _DateApprove; }
            set { _DateApprove = value; }
        }
        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        public int IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }
        public int ProjectKey
        {
            get { return _ProjectKey; }
            set { _ProjectKey = value; }
        }
        public int ProjectCategory
        {
            get { return _ProjectCategory; }
            set { _ProjectCategory = value; }
        }
        public int AssetKey
        {
            get { return _AssetKey; }
            set { _AssetKey = value; }
        }
        public string AssetID
        {
            get { return _AssetID; }
            set { _AssetID = value; }
        }
        public int AssetCategory
        {
            get { return _AssetCategory; }
            set { _AssetCategory = value; }
        }
        public string AssetType
        {
            get { return _AssetType; }
            set { _AssetType = value; }
        }
        public float Area
        {
            get { return _Area; }
            set { _Area = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public int DistrictKey
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public double AmountVAT
        {
            get { return _AmountVAT; }
            set { _AmountVAT = value; }
        }
        public int VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public double TransactionFee
        {
            get { return _TransactionFee; }
            set { _TransactionFee = value; }
        }
        public double OtherFee
        {
            get { return _OtherFee; }
            set { _OtherFee = value; }
        }
        public double Income
        {
            get { return _Income; }
            set { _Income = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int IsDraft
        {
            get { return _IsDraft; }
            set { _IsDraft = value; }
        }
        public int IsDevined
        {
            get { return _IsDevined; }
            set { _IsDevined = value; }
        }
        public int IsFinish
        {
            get { return _IsFinish; }
            set { _IsFinish = value; }
        }
        public int FinishBy
        {
            get { return _FinishBy; }
            set { _FinishBy = value; }
        }
        public DateTime FinishDate
        {
            get { return _FinishDate; }
            set { _FinishDate = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeFinished
        {
            get
            {
                return _EmployeeFinished;
            }

            set
            {
                _EmployeeFinished = value;
            }
        }

        public string EmployeeApproved
        {
            get
            {
                return _EmployeeApproved;
            }

            set
            {
                _EmployeeApproved = value;
            }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string TradeCategoryName
        {
            get
            {
                return _TradeCategoryName;
            }

            set
            {
                _TradeCategoryName = value;
            }
        }

        public string AssetCategoryName
        {
            get
            {
                return _AssetCategoryName;
            }

            set
            {
                _AssetCategoryName = value;
            }
        }

        public string ProjectName
        {
            get
            {
                return _ProjectName;
            }

            set
            {
                _ProjectName = value;
            }
        }

        public string AddressProject
        {
            get
            {
                return _AddressProject;
            }

            set
            {
                _AddressProject = value;
            }
        }

        public int SupportBy
        {
            get
            {
                return _SupportBy;
            }

            set
            {
                _SupportBy = value;
            }
        }

        public int Support
        {
            get
            {
                return _Support;
            }

            set
            {
                _Support = value;
            }
        }

        public int Source
        {
            get
            {
                return _Source;
            }

            set
            {
                _Source = value;
            }
        }

        public int HireMonth
        {
            get
            {
                return _HireMonth;
            }

            set
            {
                _HireMonth = value;
            }
        }

        public double InternalCost
        {
            get
            {
                return _InternalCost;
            }

            set
            {
                _InternalCost = value;
            }
        }

        public int ShortTerm
        {
            get
            {
                return _ShortTerm;
            }

            set
            {
                _ShortTerm = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Trade_Info()
        {
        }
        public Trade_Info(int InKey)
        {
            string zSQL = @"SELECT A.*, 
dbo.FNC_GetAddressProject(A.ProjectKey) AS [AddressProject],
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.FNC_AssetCategoryName(A.AssetCategory) AS AssetCategoryName,
dbo.FNC_SysCategoryName(A.TransactionCategory) AS TradeCategoryName,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName,
dbo.FNC_GetNameEmployee (A.ApprovedBy) AS EmployeeApproved,
dbo.FNC_GetNameEmployee (A.FinishBy) AS EmployeeFinished
FROM FNC_Transaction A 
WHERE A.TransactionKey = @TransactionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = InKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ShortTerm"] != DBNull.Value)
                        _ShortTerm = int.Parse(zReader["ShortTerm"].ToString());
                    if (zReader["HireMonth"] != DBNull.Value)
                        _HireMonth = int.Parse(zReader["HireMonth"].ToString());
                    if (zReader["InternalCost"] != DBNull.Value)
                        _InternalCost = double.Parse(zReader["InternalCost"].ToString());

                    if (zReader["Support"] != DBNull.Value)
                        _Support = int.Parse(zReader["Support"].ToString());
                    if (zReader["SupportBy"] != DBNull.Value)
                        _SupportBy = int.Parse(zReader["SupportBy"].ToString());
                    if (zReader["TransactionKey"] != DBNull.Value)
                        _TransactionKey = int.Parse(zReader["TransactionKey"].ToString());
                    if (zReader["TransactionDate"] != DBNull.Value)
                        _TransactionDate = (DateTime)zReader["TransactionDate"];
                    _TransactionID = zReader["TransactionID"].ToString();
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TransactionCategory"] != DBNull.Value)
                        _TransactionCategory = int.Parse(zReader["TransactionCategory"].ToString());
                    if (zReader["TransactionType"] != DBNull.Value)
                        _TransactionType = int.Parse(zReader["TransactionType"].ToString());
                    if (zReader["DateDeposit"] != DBNull.Value)
                        _DateDeposit = (DateTime)zReader["DateDeposit"];
                    if (zReader["DateContract"] != DBNull.Value)
                        _DateContract = (DateTime)zReader["DateContract"];
                    if (zReader["DateContractEnd"] != DBNull.Value)
                        _DateContractEnd = (DateTime)zReader["DateContractEnd"];
                    if (zReader["DateApprove"] != DBNull.Value)
                        _DateApprove = (DateTime)zReader["DateApprove"];
                    if (zReader["ApprovedBy"] != DBNull.Value)
                        _ApprovedBy = int.Parse(zReader["ApprovedBy"].ToString());
                    if (zReader["IsApproved"] != DBNull.Value)
                        _IsApproved = int.Parse(zReader["IsApproved"].ToString());
                    if (zReader["ProjectKey"] != DBNull.Value)
                        _ProjectKey = int.Parse(zReader["ProjectKey"].ToString());
                    if (zReader["ProjectCategory"] != DBNull.Value)
                        _ProjectCategory = int.Parse(zReader["ProjectCategory"].ToString());
                    if (zReader["AssetKey"] != DBNull.Value)
                        _AssetKey = int.Parse(zReader["AssetKey"].ToString());
                    _AssetID = zReader["AssetID"].ToString();
                    if (zReader["AssetCategory"] != DBNull.Value)
                        _AssetCategory = int.Parse(zReader["AssetCategory"].ToString());
                    _AssetType = zReader["AssetType"].ToString();
                    if (zReader["Area"] != DBNull.Value)
                        _Area = float.Parse(zReader["Area"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    _Street = zReader["Street"].ToString();
                    _Address = zReader["Address"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["AmountVAT"] != DBNull.Value)
                        _AmountVAT = double.Parse(zReader["AmountVAT"].ToString());
                    if (zReader["VAT"] != DBNull.Value)
                        _VAT = int.Parse(zReader["VAT"].ToString());
                    if (zReader["TransactionFee"] != DBNull.Value)
                        _TransactionFee = double.Parse(zReader["TransactionFee"].ToString());
                    if (zReader["OtherFee"] != DBNull.Value)
                        _OtherFee = double.Parse(zReader["OtherFee"].ToString());
                    if (zReader["Income"] != DBNull.Value)
                        _Income = double.Parse(zReader["Income"].ToString());
                    _Description = zReader["Description"].ToString();
                    if (zReader["IsDraft"] != DBNull.Value)
                        _IsDraft = int.Parse(zReader["IsDraft"].ToString());
                    if (zReader["IsDevined"] != DBNull.Value)
                        _IsDevined = int.Parse(zReader["IsDevined"].ToString());
                    if (zReader["IsFinish"] != DBNull.Value)
                        _IsFinish = int.Parse(zReader["IsFinish"].ToString());
                    if (zReader["FinishBy"] != DBNull.Value)
                        _FinishBy = int.Parse(zReader["FinishBy"].ToString());
                    if (zReader["FinishDate"] != DBNull.Value)
                        _FinishDate = (DateTime)zReader["FinishDate"];
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                    if (zReader["Source"] != DBNull.Value)
                        _Source = int.Parse(zReader["Source"].ToString());
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();

                    _ProjectName = zReader["ProjectName"].ToString();
                    _AddressProject = zReader["AddressProject"].ToString();
                    _AssetCategoryName = zReader["AssetCategoryName"].ToString();
                    _TradeCategoryName = zReader["TradeCategoryName"].ToString();
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _EmployeeApproved = zReader["EmployeeApproved"].ToString();
                    _EmployeeFinished = zReader["EmployeeFinished"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Save()
        {
            string zResult;
            if (_TransactionKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO FNC_Transaction (
TransactionDate ,TransactionID ,EmployeeKey ,DepartmentKey ,TransactionCategory ,TransactionType ,DateDeposit ,DateContract ,DateContractEnd ,DateApprove 
,ApprovedBy ,IsApproved ,ProjectKey ,ProjectCategory ,AssetKey ,AssetID ,AssetCategory ,AssetType ,Area ,ProvinceKey ,DistrictKey ,Street ,Address ,Amount ,AmountVAT ,VAT ,
TransactionFee ,OtherFee ,Income ,Description ,IsDraft ,IsDevined ,IsFinish ,FinishBy ,FinishDate ,Support, SupportBy, Source,HireMonth,InternalCost,ShortTerm,
CreatedDate ,CreatedBy ,CreatedName ,ModifiedDate ,ModifiedBy ,ModifiedName )
VALUES (
@TransactionDate ,@TransactionID ,@EmployeeKey ,@DepartmentKey ,@TransactionCategory ,@TransactionType ,
@DateDeposit ,@DateContract ,@DateContractEnd ,@DateApprove ,@ApprovedBy ,@IsApproved ,@ProjectKey ,@ProjectCategory ,
@AssetKey ,@AssetID ,@AssetCategory ,@AssetType ,@Area ,@ProvinceKey ,@DistrictKey ,@Street ,@Address ,@Amount ,@AmountVAT ,@VAT ,
@TransactionFee ,@OtherFee ,@Income ,@Description ,@IsDraft ,@IsDevined ,@IsFinish ,@FinishBy ,@FinishDate ,@Support, @SupportBy, @Source,@HireMonth,@InternalCost,@ShortTerm,
GETDATE() ,@CreatedBy ,@CreatedName ,GETDATE() ,@ModifiedBy ,@ModifiedName )
SELECT TransactionKey FROM FNC_Transaction WHERE TransactionKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@ShortTerm", SqlDbType.Int).Value = _ShortTerm;
                zCommand.Parameters.Add("@HireMonth", SqlDbType.Int).Value = _HireMonth;
                zCommand.Parameters.Add("@InternalCost", SqlDbType.Int).Value = _InternalCost;

                zCommand.Parameters.Add("@Source", SqlDbType.Int).Value = _Source;
                zCommand.Parameters.Add("@SupportBy", SqlDbType.Int).Value = _SupportBy;
                zCommand.Parameters.Add("@Support", SqlDbType.Int).Value = _Support;

                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;
                if (_TransactionDate.Year == 0001)
                    zCommand.Parameters.Add("@TransactionDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TransactionDate", SqlDbType.DateTime).Value = _TransactionDate;
                zCommand.Parameters.Add("@TransactionID", SqlDbType.NVarChar).Value = _TransactionID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TransactionCategory", SqlDbType.Int).Value = _TransactionCategory;
                zCommand.Parameters.Add("@TransactionType", SqlDbType.Int).Value = _TransactionType;
                if (_DateDeposit.Year == 0001)
                    zCommand.Parameters.Add("@DateDeposit", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateDeposit", SqlDbType.DateTime).Value = _DateDeposit;
                if (_DateContract.Year == 0001)
                    zCommand.Parameters.Add("@DateContract", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContract", SqlDbType.DateTime).Value = _DateContract;
                if (_DateContractEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = _DateContractEnd;
                if (_DateApprove.Year == 0001)
                    zCommand.Parameters.Add("@DateApprove", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateApprove", SqlDbType.DateTime).Value = _DateApprove;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectCategory", SqlDbType.Int).Value = _ProjectCategory;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = _AssetCategory;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = _AmountVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Int).Value = _VAT;
                zCommand.Parameters.Add("@TransactionFee", SqlDbType.Money).Value = _TransactionFee;
                zCommand.Parameters.Add("@OtherFee", SqlDbType.Money).Value = _OtherFee;
                zCommand.Parameters.Add("@Income", SqlDbType.Money).Value = _Income;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@IsDevined", SqlDbType.Int).Value = _IsDevined;
                zCommand.Parameters.Add("@IsFinish", SqlDbType.Int).Value = _IsFinish;
                zCommand.Parameters.Add("@FinishBy", SqlDbType.Int).Value = _FinishBy;
                if (_FinishDate.Year == 0001)
                    zCommand.Parameters.Add("@FinishDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FinishDate", SqlDbType.DateTime).Value = _FinishDate;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _TransactionKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FNC_Transaction SET "
                        + " TransactionDate = @TransactionDate,"
                        + " TransactionID = @TransactionID,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " DepartmentKey = @DepartmentKey,"
                        + " TransactionCategory = @TransactionCategory,"
                        + " TransactionType = @TransactionType,"
                        + " DateDeposit = @DateDeposit,"
                        + " DateContract = @DateContract,"
                        + " DateContractEnd = @DateContractEnd,"
                        + " DateApprove = @DateApprove,"
                        + " ApprovedBy = @ApprovedBy,"
                        + " IsApproved = @IsApproved,"
                        + " ProjectKey = @ProjectKey,"
                        + " ProjectCategory = @ProjectCategory,"
                        + " AssetKey = @AssetKey,"
                        + " AssetID = @AssetID,"
                        + " AssetCategory = @AssetCategory,"
                        + " AssetType = @AssetType,"
                        + " Area = @Area,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " DistrictKey = @DistrictKey,"
                        + " Street = @Street,"
                        + " Address = @Address,"
                        + " Amount = @Amount,"
                        + " AmountVAT = @AmountVAT,"
                        + " VAT = @VAT,"
                        + " TransactionFee = @TransactionFee,"
                        + " OtherFee = @OtherFee,"
                        + " Income = @Income,"
                        + " Description = @Description,"
                        + " IsDraft = @IsDraft,"
                        + " IsDevined = @IsDevined,"
                        + " IsFinish = @IsFinish,"
                        + " FinishBy = @FinishBy,"
                        + " FinishDate = @FinishDate, Support = @Support, SupportBy = @SupportBy, Source =@Source, HireMonth=@HireMonth, InternalCost=@InternalCost, ShortTerm =@ShortTerm,"
                        + " ModifiedDate = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE TransactionKey = @TransactionKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ShortTerm", SqlDbType.Int).Value = _ShortTerm;
                zCommand.Parameters.Add("@HireMonth", SqlDbType.Int).Value = _HireMonth;
                zCommand.Parameters.Add("@InternalCost", SqlDbType.Int).Value = _InternalCost;
                zCommand.Parameters.Add("@Source", SqlDbType.Int).Value = _Source;
                zCommand.Parameters.Add("@SupportBy", SqlDbType.Int).Value = _SupportBy;
                zCommand.Parameters.Add("@Support", SqlDbType.Int).Value = _Support;

                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;
                if (_TransactionDate.Year == 0001)
                    zCommand.Parameters.Add("@TransactionDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@TransactionDate", SqlDbType.DateTime).Value = _TransactionDate;
                zCommand.Parameters.Add("@TransactionID", SqlDbType.NVarChar).Value = _TransactionID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TransactionCategory", SqlDbType.Int).Value = _TransactionCategory;
                zCommand.Parameters.Add("@TransactionType", SqlDbType.Int).Value = _TransactionType;
                if (_DateDeposit.Year == 0001)
                    zCommand.Parameters.Add("@DateDeposit", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateDeposit", SqlDbType.DateTime).Value = _DateDeposit;
                if (_DateContract.Year == 0001)
                    zCommand.Parameters.Add("@DateContract", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContract", SqlDbType.DateTime).Value = _DateContract;
                if (_DateContractEnd.Year == 0001)
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateContractEnd", SqlDbType.DateTime).Value = _DateContractEnd;
                if (_DateApprove.Year == 0001)
                    zCommand.Parameters.Add("@DateApprove", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateApprove", SqlDbType.DateTime).Value = _DateApprove;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@ProjectCategory", SqlDbType.Int).Value = _ProjectCategory;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.Int).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.Int).Value = _AssetCategory;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@Area", SqlDbType.Float).Value = _Area;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@Street", SqlDbType.NVarChar).Value = _Street;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = _AmountVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Int).Value = _VAT;
                zCommand.Parameters.Add("@TransactionFee", SqlDbType.Money).Value = _TransactionFee;
                zCommand.Parameters.Add("@OtherFee", SqlDbType.Money).Value = _OtherFee;
                zCommand.Parameters.Add("@Income", SqlDbType.Money).Value = _Income;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@IsDraft", SqlDbType.Int).Value = _IsDraft;
                zCommand.Parameters.Add("@IsDevined", SqlDbType.Int).Value = _IsDevined;
                zCommand.Parameters.Add("@IsFinish", SqlDbType.Int).Value = _IsFinish;
                zCommand.Parameters.Add("@FinishBy", SqlDbType.Int).Value = _FinishBy;
                if (_FinishDate.Year == 0001)
                    zCommand.Parameters.Add("@FinishDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FinishDate", SqlDbType.DateTime).Value = _FinishDate;

                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM FNC_Transaction_Customer WHERE TransactionKey = @TransactionKey
DELETE FROM FNC_Transaction WHERE TransactionKey = @TransactionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateProduct()
        {
            string zSQL = "UPDATE FNC_Transaction SET "
                        + " ProjectKey = @ProjectKey,"
                        + " AssetKey = @AssetKey,"
                        + " Area =@Area,"
                        + " AssetCategory= @AssetCategory, AssetType =@AssetType,"
                        + " AssetID = @AssetID,"
                        + " Address = @Address"
                        + " WHERE TransactionKey = @TransactionKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;
                zCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = _ProjectKey;
                zCommand.Parameters.Add("@AssetID", SqlDbType.NVarChar).Value = _AssetID;
                zCommand.Parameters.Add("@AssetKey", SqlDbType.NVarChar).Value = _AssetKey;
                zCommand.Parameters.Add("@AssetType", SqlDbType.NVarChar).Value = _AssetType;
                zCommand.Parameters.Add("@Area", SqlDbType.NVarChar).Value = _Area;
                zCommand.Parameters.Add("@AssetCategory", SqlDbType.NVarChar).Value = _AssetCategory;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Approve()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Transaction SET ApprovedBy = @ApprovedBy, IsApproved = @IsApproved, DateApprove = @DateApprove WHERE TransactionKey = @TransactionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (_DateApprove.Year == 0001)
                    zCommand.Parameters.Add("@DateApprove", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateApprove", SqlDbType.DateTime).Value = _DateApprove;
                zCommand.Parameters.Add("@ApprovedBy", SqlDbType.Int).Value = _ApprovedBy;
                zCommand.Parameters.Add("@IsApproved", SqlDbType.Int).Value = _IsApproved;
                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Finish()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Transaction SET FinishBy = @FinishBy, IsFinish = @IsFinish, FinishDate = @FinishDate WHERE TransactionKey = @TransactionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (_DateApprove.Year == 0001)
                    zCommand.Parameters.Add("@FinishDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@FinishDate", SqlDbType.DateTime).Value = _FinishDate;

                zCommand.Parameters.Add("@FinishBy", SqlDbType.Int).Value = FinishBy;
                zCommand.Parameters.Add("@IsFinish", SqlDbType.Int).Value = IsFinish;
                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string UpdateSupport()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Transaction SET SupportBy = @SupportBy WHERE TransactionKey = @TransactionKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@SupportBy", SqlDbType.Int).Value = FinishBy;
                zCommand.Parameters.Add("@TransactionKey", SqlDbType.Int).Value = _TransactionKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
