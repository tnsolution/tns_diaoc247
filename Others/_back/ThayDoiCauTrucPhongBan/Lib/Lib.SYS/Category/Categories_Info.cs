﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SYS
{
    public class Categories_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ProductID = "";
        private string _Product = "";
        private int _Category = 0;
        private string _CategoryName = "";
        private string _Value = "";
        private int _Type = 0;
        private int _Rank = 0;
        private string _Description = "";
        private string _Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Categories_Info()
        {
        }
        public Categories_Info(int AutoKey)
        {
            string zSQL = "SELECT A.*, B.Product AS CategoryName FROM SYS_Categories A LEFT JOIN dbo.SYS_Categories B ON A.Category = B.AutoKey WHERE A.AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _ProductID = zReader["ProductID"].ToString();
                    _Product = zReader["Product"].ToString();
                    _Value = zReader["Value"].ToString();

                    if (zReader["Category"] != DBNull.Value)
                        _Category = int.Parse(zReader["Category"].ToString());

                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());

                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());

                    _Description = zReader["Description"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Categories_Info(string ProductID)
        {
            string zSQL = "SELECT A.*, B.Product AS CategoryName FROM SYS_Categories A LEFT JOIN dbo.SYS_Categories B ON A.Category = B.AutoKey WHERE A.ProductID = @ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = ProductID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _ProductID = zReader["ProductID"].ToString();
                    _Product = zReader["Product"].ToString();
                    if (zReader["Category"] != DBNull.Value)
                        _Category = int.Parse(zReader["Category"].ToString());
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    _Description = zReader["Description"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Properties ]

        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string Product
        {
            get { return _Product; }
            set { _Product = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int Rank
        {
            get
            {
                return _Rank;
            }

            set
            {
                _Rank = value;
            }
        }

        public string Value
        {
            get
            {
                return _Value;
            }

            set
            {
                _Value = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Categories ("
        + " ProductID ,Product ,Type ,Description, Category, Value) "
         + " VALUES ( "
         + "@ProductID ,@Product ,@Type ,@Description, @Category,@Value ) "
         + " SELECT AutoKey FROM SYS_Categories WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = _ProductID;
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = _Product;
                zCommand.Parameters.Add("@Value", SqlDbType.NVarChar).Value = Value;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                _AutoKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Categories SET "
                        + " ProductID = @ProductID, Category = @Category,"
                        + " Product = @Product, Value= @Value,"
                        + " Type = @Type,"
                        + " Description = @Description"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.Char).Value = _ProductID;
                zCommand.Parameters.Add("@Product", SqlDbType.NVarChar).Value = _Product;
                zCommand.Parameters.Add("@Value", SqlDbType.NVarChar).Value = Value;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = _Category;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Categories WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
