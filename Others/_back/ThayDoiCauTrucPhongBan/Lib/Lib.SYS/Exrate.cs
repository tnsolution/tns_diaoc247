﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.SYS
{
    public class Exrate
    {
        private string _CurrencyCode = string.Empty;

        public string CurrencyCode
        {
            get { return _CurrencyCode; }
            set { _CurrencyCode = value; }
        }
        private string _CurrencyName = string.Empty;

        public string CurrencyName
        {
            get { return _CurrencyName; }
            set { _CurrencyName = value; }
        }
        private float _Buy = 0;

        public float Buy
        {
            get { return _Buy; }
            set { _Buy = value; }
        }
        private float _Transfer = 0;

        public float Transfer
        {
            get { return _Transfer; }
            set { _Transfer = value; }
        }
        private float _Sell = 0;

        public float Sell
        {
            get { return _Sell; }
            set { _Sell = value; }
        }
    }
}
