﻿using Lib.Config;
using Lib.SYS.Report;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Lib.SYS
{
    public class HomePage
    {
        public static int Count_Require_Trade(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int Result = 0;
            string zSQL = @"
	SELECT SUM(A.Value) AS Require 
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN HRM_Employees C ON B.EmployeeKey = C.EmployeeKey
    WHERE A.CategoryKey = 298 AND (B.PlanDate BETWEEN @FromDate AND @ToDate)";
            if (Employee != 0)
                zSQL += " AND B.EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                Result = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return Result;
        }
        public static int Count_Success_Trade(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            string zSQL = @"
SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE (A.IsApproved = 1) AND A.DateApprove BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND (A.EmployeeKey = @EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }

        public static int Count_Success_Sale(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            string zSQL = @"
SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE (A.IsApproved = 1) 
AND A.TransactionCategory <> 227
AND A.DateApprove BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND (A.EmployeeKey = @EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }               

        public static int Count_Success_ShortTerm_1(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            string zSQL = @"
SELECT ISNULL(SUM(dbo.FNC_SumShortTerm(A.TransactionKey)),0) AS COUNT
FROM dbo.FNC_Transaction A
WHERE (A.IsApproved = 1) 
AND A.TransactionCategory = 227 
AND A.Source = 1
AND A.ShortTerm = 1
AND A.DateApprove BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND (A.EmployeeKey = @EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();

                return zResult / 6;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
            //
        }
        public static int Count_Success_ShortTerm_2(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            string zSQL = @"
SELECT ISNULL(SUM(dbo.FNC_SumShortTerm(A.TransactionKey)),0) AS COUNT
FROM dbo.FNC_Transaction A
WHERE (A.IsApproved = 1) 
AND A.TransactionCategory = 227 
AND A.Source = 2
AND A.ShortTerm = 1
AND A.DateApprove BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND (A.EmployeeKey = @EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();

                return zResult / 12;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
            //
        }

        /// <summary>
        /// Dem giao dich cho thue thang dự trên cột HireCounted (update tự động mỗi khi có giao dịch cho thuê phát sinh), /12 để tinh ra 1 giao dich, 24 2 giao dịch
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="Department"></param>
        /// <param name="Employee"></param>
        /// <returns></returns>
        public static int Count_Success_Hire(DateTime FromDate, DateTime ToDate, int Department, int Employee)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            string zSQL = @"
SELECT ISNULL(SUM(A.HireMonth),0) AS COUNT
FROM dbo.FNC_Transaction A
WHERE (A.IsApproved = 1) 
AND A.TransactionCategory = 227 
AND A.ShortTerm = 2
AND A.DateApprove BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND (A.EmployeeKey = @EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Char).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();

                return zResult / 12;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }       
        //
        public static double Sum_Capital(DateTime FromDate, DateTime ToDate)
        {
            double first = RptHelper.TotalBalanceCapital(FromDate.AddMonths(-1), ToDate.AddMonths(-1));
            double pay = Sum_CapitalOut(FromDate, ToDate);
            double receipt = Sum_CapitalIn(FromDate, ToDate);

            return first + receipt - pay;
        }
        public static double Sum_CapitalIn(DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSql = new SqlContext();
            double sum = zSql.GetObject(@"SELECT SUM(Amount) FROM FNC_Capital_Input A WHERE IsApproved = 1 AND CapitalDate BETWEEN '" + FromDate + "' AND '" + ToDate + "'").ToDouble();
            return sum;
        }
        public static double Sum_CapitalOut(DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSql = new SqlContext();
            double sum = zSql.GetObject(@"SELECT SUM(Amount) FROM FNC_Capital_Output A WHERE IsApproved = 1 AND CapitalDate BETWEEN '" + FromDate + "' AND '" + ToDate + "'").ToDouble();
            return sum;
        }
        //

        public static double Sum_Receipt(DateTime FromDate, DateTime ToDate, int Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND ReceiptDate BETWEEN @FromDate AND @ToDate AND IsApproved=1";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSqlContext.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }
        public static double Sum_Payment(DateTime FromDate, DateTime ToDate, int Department)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND PaymentDate BETWEEN @FromDate AND @ToDate AND IsApproved=1 AND IsApproved2=1";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            zSqlContext.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }
        public static double Sum_Fee(DateTime FromDate, DateTime ToDate, int Department)
        {
            double first = RptHelper.PreviousMonth(FromDate.Month, ToDate.Year, Department);
            double receipt = Sum_Receipt(FromDate, ToDate, Department);
            double pay = Sum_Payment(FromDate, ToDate, Department);

            return first + receipt - pay;
        }

        //
        public static double Sum_Payment2(DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSql = new SqlContext();
            double sum = zSql.GetObject(@"SELECT SUM(Amount) FROM FNC_Payment_Detail A WHERE A.CategoryKey = 2 AND PaymentDate BETWEEN '" + FromDate + "' AND '" + ToDate + "'").ToDouble();
            return sum;
        }
        public static double Sum_Receipt2(DateTime FromDate, DateTime ToDate)
        {
            SqlContext zSql = new SqlContext();
            double sum = zSql.GetObject(@"SELECT SUM(Amount) FROM FNC_Receipt_Detail A WHERE A.CategoryKey = 2 AND ReceiptDate BETWEEN '" + FromDate + "' AND '" + ToDate + "'").ToDouble();
            return sum;
        }
        public static double Sum_FeePlay(DateTime FromDate, DateTime ToDate)
        {
            double first = RptHelper.PreviousMonth_Play(FromDate.Month, FromDate.Year);
            double pay = Sum_Payment2(FromDate, ToDate);
            double receipt = Sum_Receipt2(FromDate, ToDate);

            return first + receipt - pay;
        }
        //

        public static int Count_Trade_NeedApproved(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"
SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE A.IsApproved = 0";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static int Count_Trade_NeedCollect(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(A.TransactionKey) AS COUNT
FROM dbo.FNC_Transaction A
WHERE A.IsFinish = 0";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.CreatedDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static double Sum_Trade(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT SUM(A.Income) AS COUNT
FROM FNC_Transaction A
WHERE A.IsApproved = 1 AND A.IsFinish = 1 AND (A.FinishDate BETWEEN @FromDate AND @ToDate)";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {


                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }


        public static DataTable Get_Annou(int Department)
        {
            DataTable zTable = new DataTable();
            string zSQL = "";
            if (Department != 0)
                zSQL = @"SELECT A.* FROM HRM_Announce A 
LEFT JOIN PUL_SharePermition B ON A.ID = B.AssetKey 
WHERE A.IsDelete = 0 AND 
B.EmployeeKey = @DepartmentKey 
ORDER BY A.CreatedDate DESC";
            else
                zSQL = "SELECT A.* FROM HRM_Announce A WHERE A.IsDelete = 0 ORDER BY CreatedDate DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_Schedule(int EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.* 
FROM HRM_ScheduleDetail A 
WHERE CONVERT(DATE,[Start],120) = CONVERT(DATE,GETDATE(),120)
AND EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Get_ListStaff(DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT ROW_NUMBER() OVER (ORDER BY INCOME DESC) AS RowNumber , ISNULL(Income,0) AS INCOME, Name_Employee
FROM (
	SELECT A.LastName + ' ' + A.FirstName AS Name_Employee, 
    dbo.FNC_GetIncome(A.EmployeeKey,@FromDate,@ToDate) AS INCOME 
    FROM HRM_Employees A LEFT JOIN HRM_Positions B ON A.PositionKey = B.PositionKey
    WHERE A.IsWorking = 2 AND B.Major = N'Sales'
) X";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
