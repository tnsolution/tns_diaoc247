﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Lib.SYS
{
    public class MyCryptography
    {
        public static string HashPass(string nPass)
        {
            HashAlgorithm Hash = HashAlgorithm.Create("SHA1");
            byte[] pwordData = Encoding.Default.GetBytes(nPass);
            byte[] bHash = Hash.ComputeHash(pwordData);
            return Convert.ToBase64String(bHash);
        }

        public static Boolean VerifyHash(string NewPass, string OldPass)
        {
            string HashNewPass = HashPass(NewPass);
            return (OldPass == HashNewPass);
        }

        public static string Encrypt(string Text)
        {
            string EncryptionKey = "HHT2015";
            byte[] clearBytes = Encoding.Unicode.GetBytes(Text);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    Text = Convert.ToBase64String(ms.ToArray());
                }
            }
            return Text;
        }

        public static string Decrypt(string Text)
        {
            string EncryptionKey = "HHT2015";
            byte[] cipherBytes = Convert.FromBase64String(Text);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    Text = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return Text;
        }
    }
}
