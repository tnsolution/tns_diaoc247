﻿using Lib.Config;
using System;
using System.Data;
using System.Data.SqlClient;
namespace Lib.SYS
{
    public class Notification_Data
    {
        public static int GetMessageSalary(int AutoKey, int EmployeeKey)
        {
            string Sql = @"
SELECT AutoKey FROM SYS_Message 
WHERE ObjectTable = N'HRM_Salary'
AND ObjectKey = @AutoKey
AND EmployeeKey = @EmployeeKey";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            zSqlContext.CMD.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
            return zSqlContext.GetObject(Sql).ToInt();
        }

        public static int Check_Salary(int EmployeeKey, int AutoKey)
        {
            string Sql = @"
SELECT Accept FROM SYS_Message 
WHERE ObjectTable = N'HRM_Salary'
AND ObjectKey = @AutoKey
AND EmployeeKey = @EmployeeKey";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            zSqlContext.CMD.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
            return zSqlContext.GetObject(Sql).ToInt();
        }

        public static int Count_Salary(int EmployeeKey)
        {
            string Sql = @"
SELECT COUNT(AutoKey) FROM SYS_Message 
WHERE ObjectTable = N'HRM_Salary'
AND EmployeeKey = @EmployeeKey
AND Readed=0";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            return zSqlContext.GetObject(Sql).ToInt();
        }

        public static int Get_MessageKey(int TicketKey, int EmployeeKey)
        {
            string Sql = @"
SELECT AutoKey FROM SYS_Message 
WHERE ObjectTable = N'HRM_TicketOff'
AND ObjectKey = @TicketKey
AND EmployeeKey = @EmployeeKey";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            zSqlContext.CMD.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
            return zSqlContext.GetObject(Sql).ToInt();
        }
        public static DataTable MeesageTicketOff(int TicketKey)
        {
            string Sql = @"
SELECT *, dbo.FNC_GetNameEmployee(EmployeeKey) AS EmployeeName FROM SYS_Message 
WHERE ObjectTable = N'HRM_TicketOff'
AND ObjectKey = @ObjectKey";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@ObjectKey", SqlDbType.Int).Value = TicketKey;
            return zSqlContext.GetData(Sql);
        }

        public static int Count_TicketOff(int EmployeeKey)
        {
            string Sql = @"
SELECT COUNT(*) FROM SYS_Message 
WHERE ObjectTable = N'HRM_TicketOff'
AND EmployeeKey = @EmployeeKey 
AND Readed = 0";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Check_TicketOff(int EmployeeKey, int TicketKey)
        {
            string Sql = @"
SELECT Accept FROM SYS_Message 
WHERE ObjectTable = N'HRM_TicketOff'
AND ObjectKey = @TicketKey
AND EmployeeKey = @EmployeeKey";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            zSqlContext.CMD.Parameters.Add("@TicketKey", SqlDbType.Int).Value = TicketKey;
            return zSqlContext.GetObject(Sql).ToInt();
        }

        public static int Count_FeePlay(int EmployeeKey)
        {
            string Sql = @"SELECT COUNT(AutoKey) FROM SYS_Message WHERE Readed = 0 
AND EmployeeKey = @EmployeeKey 
AND (ObjectTable LIKE '%FNC_Receipt%' OR ObjectTable LIKE '%FNC_Payment%' )";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }

        public static int Count_Capital(int Year, int Month)
        {
            return Count_InCapital(Year, Month) + Count_OutCapital(Year, Month);
        }
        public static int Count_InCapital(int Year, int Month)
        {
            string Sql = "SELECT COUNT(AutoKey) FROM FNC_Capital_Input WHERE IsApproved = 0 AND MONTH(CreatedDate) = @Month AND YEAR(CreatedDate)=@Year ";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_OutCapital(int Year, int Month)
        {
            string Sql = "SELECT COUNT(AutoKey) FROM FNC_Capital_Output WHERE IsApproved = 0 AND MONTH(CreatedDate) = @Month AND YEAR(CreatedDate)=@Year ";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Capital()
        {
            return Count_InCapital() + Count_OutCapital();
        }
        public static int Count_InCapital()
        {
            string Sql = "SELECT COUNT(AutoKey) FROM FNC_Capital_Input WHERE IsApproved = 0 ";
            SqlContext zSqlContext = new SqlContext();

            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_OutCapital()
        {
            string Sql = "SELECT COUNT(AutoKey) FROM FNC_Capital_Output WHERE IsApproved = 0 ";
            SqlContext zSqlContext = new SqlContext();

            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }

        public static int Count_Transfer(int EmployeeKey)
        {
            string Sql = "SELECT COUNT(*) FROM SYS_LogTranfer WHERE Confirmed = 0 AND NewStaff = @Employee";
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = EmployeeKey;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Transfer(int EmployeeKey, int DepartmentKey)
        {
            string Sql = "SELECT COUNT(*) FROM SYS_LogTranfer A WHERE Confirmed = 0";
            if (EmployeeKey != 0)
                Sql += " AND NewStaff = @Employee";
            if (DepartmentKey != 0)
                Sql += " AND dbo.FNC_GetDepartment(NewStaff) =" + DepartmentKey;
            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = EmployeeKey;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Receipt(int Year, int Month, int Department)
        {
            string Sql = "SELECT COUNT(Amount) FROM FNC_Receipt_Detail WHERE MONTH(ReceiptDate) = @Month AND YEAR(ReceiptDate)=@Year AND IsApproved = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Payment(int Year, int Month, int Department)
        {
            string Sql = "SELECT COUNT(Amount) FROM FNC_Payment_Detail WHERE MONTH(PaymentDate) = @Month AND YEAR(PaymentDate)=@Year AND IsApproved2 = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Payment(int Department, int Category)
        {
            string Sql = "SELECT COUNT(Amount) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND IsApproved2 = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Payment(int Year, int Month, int Department, int Category)
        {
            string Sql = "SELECT COUNT(Amount) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND MONTH(PaymentDate) = @Month AND YEAR(PaymentDate)=@Year AND IsApproved2 = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }
        public static int Count_Receipt(int Year, int Month, int Department, int Category)
        {
            string Sql = "SELECT COUNT(Amount) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND MONTH(ReceiptDate) = @Month AND YEAR(ReceiptDate)=@Year AND IsApproved = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }

        public static int Count_Receipt(int Department, int Category)
        {
            string Sql = "SELECT COUNT(Amount) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND IsApproved = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToInt32(zSqlContext.GetObject(Sql));
        }

        public static double Sum_Receipt(int Year, int Month, int Department, int Category)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Receipt_Detail WHERE CategoryKey = 1 AND MONTH(ReceiptDate) = @Month AND YEAR(ReceiptDate)=@Year AND IsApproved = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }
        public static double Sum_Payment(int Year, int Month, int Department, int Category)
        {
            string Sql = "SELECT ISNULL(SUM(Amount),0) FROM FNC_Payment_Detail WHERE CategoryKey = 1 AND MONTH(PaymentDate) = @Month AND YEAR(PaymentDate)=@Year AND IsApproved2 = 0";
            if (Department != 0)
                Sql += " AND DepartmentKey = @Department";

            SqlContext zSqlContext = new SqlContext();
            zSqlContext.CMD.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
            zSqlContext.CMD.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
            zSqlContext.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            return Convert.ToDouble(zSqlContext.GetObject(Sql));
        }


        public static int Count_Notification(int Type, int EmployeeKey, int DepartmentKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT ISNULL(COUNT(A.ID),0) FROM SYS_Notification A WHERE A.[TYPE] = @Type AND IsRead = 0";

            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @Key";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartKey", SqlDbType.Int).Value = DepartmentKey;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                zResult = -1;
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static int Count_Task(int EmployeeKey, int DepartmentKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @" 
SELECT COUNT(A.InfoKey) 
FROM TASK_Note_Detail A 
LEFT JOIN TASK_Note B ON A.NoteKey = B.NoteKey 
LEFT JOIN HRM_Employees C ON C.EmployeeKey = B.SendTo
WHERE StatusInfo = 6";

            if (DepartmentKey != 0)
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND B.SendTo = @EmployeeKey";

            // lay thong tin chua thuc hien va chua lien lac duoc
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zResult = Convert.ToInt32(zCommand.ExecuteScalar());
                zCommand.Dispose();
                return zResult;
            }
            catch (Exception Err)
            {
                return 0;
            }
            finally
            {
                zConnect.Close();
            }
        }
        public static DataTable List(int Type, int DepartmentKey, int EmployeeKey)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT * FROM SYS_Notification A WHERE A.[TYPE] = @Type AND IsShow = 1 AND IsRead = 0";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @Key ";
            zSQL += " ORDER BY A.CreatedDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            DataTable zTable = new DataTable();
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zCommand.Parameters.Add("@Key", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception Err)
            {
                zResult = -1;
            }
            finally
            {
                zConnect.Close();
            }
            return zTable;
        }
        public static DataTable Customer()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, B.LastName + ' ' + B.FirstName AS EmployeeName FROM CRM_Customer A LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey
WHERE 
CONVERT(VARCHAR(10), DATEPART(mm, DATEADD(DAY, 1, GETDATE()))) + CONVERT(VARCHAR(10), DATEPART(dd, DATEADD(DAY, 1, GETDATE()))) =
CONVERT(VARCHAR(10), DATEPART(mm, A.Birthday)) + CONVERT(VARCHAR(10), DATEPART(dd, A.Birthday))";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Trade()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM FNC_Transaction WHERE CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 30, GETDATE()), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Building()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, C.ProjectName, B.LastName + ' ' + B.FirstName AS EmployeeName, B.DepartmentKey 
FROM PUL_Resale_Apartment A 
LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN PUL_Project C ON C.ProjectKey = A.ProjectKey
WHERE A. DateContractEnd IS NOT NULL AND 
CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 7, GETDATE()), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //----------auto noti
        public static DataTable GetDataCustomer()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, dbo.GetEmployeeName(A.EmployeeKey) AS EmployeeName FROM CRM_Customer A
WHERE 
CONVERT(VARCHAR(10), DATEPART(mm, DATEADD(DAY, 1, GETDATE()))) + CONVERT(VARCHAR(10), DATEPART(dd, DATEADD(DAY, 1, GETDATE()))) =
CONVERT(VARCHAR(10), DATEPART(mm, A.Birthday)) + CONVERT(VARCHAR(10), DATEPART(dd, A.Birthday))";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetDataTransaction()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM FNC_Transaction 
WHERE CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 30, GETDATE()), 112))
OR CONVERT(VARCHAR(10), DateContractEnd, 112) = CONVERT(VARCHAR(10), GETDATE(), 112)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetDataApartment()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.AssetKey, A.AssetID, DepartmentKey, EmployeeKey, DateContractEnd,
dbo.FNC_GetProjectName(A.ProjectKey) AS ProjectName,
dbo.GetEmployeeName(A.EmployeeKey) AS EmployeeName, 
dbo.FNC_GetDepartment(A.EmployeeKey) AS DepartmentName
FROM PUL_Resale_Apartment A 
WHERE A.DepartmentKey IS NOT NULL 
AND A.EmployeeKey IS NOT NULL 
AND A. DateContractEnd IS NOT NULL 
AND (CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 7, GETDATE()), 112)) OR 
CONVERT(VARCHAR(10), DateContractEnd, 112) = CONVERT(VARCHAR(10), GETDATE(), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetDataHouse()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,  C.ProjectName, B.LastName + ' ' + B.FirstName AS EmployeeName, B.DepartmentKey 
FROM PUL_Resale_House A 
LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN PUL_Project C ON C.ProjectKey = A.ProjectKey
WHERE A.DepartmentKey IS NOT NULL 
AND A.EmployeeKey IS NOT NULL 
AND A. DateContractEnd IS NOT NULL 
AND CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 7, GETDATE()), 112)) OR 
CONVERT(VARCHAR(10), DateContractEnd, 112) = CONVERT(VARCHAR(10), GETDATE(), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetDataGround()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,  C.ProjectName, B.LastName + ' ' + B.FirstName AS EmployeeName, B.DepartmentKey 
FROM PUL_Resale_Ground A 
LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN PUL_Project C ON C.ProjectKey = A.ProjectKey
WHERE A. DateContractEnd IS NOT NULL AND 
CONVERT(VARCHAR(10), DateContractEnd, 112) = (SELECT CONVERT(VARCHAR(10), DATEADD (DAY, 7, GETDATE()), 112))";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //----------- COUNT KH VA BC
        public static int CountSchedule(int Employee)
        {
            SqlContext zSql = new SqlContext();
            return zSql.GetObject(@"SELECT COUNT(*) FROM HRM_Schedule A
WHERE DAY(A.CreatedDate) = DAY(GETDATE())
AND MONTH(A.CreatedDate) = MONTH(GETDATE())
AND YEAR(A.CreatedDate) = YEAR(GETDATE())
AND A.EmployeeKey = " + Employee).ToInt();
        }

        public static int CountReport(int Employee)
        {
            SqlContext zSql = new SqlContext();
            return zSql.GetObject(@"SELECT COUNT(*) FROM TASK_Report A
WHERE DAY(A.CreatedDate) = DAY(GETDATE())
AND MONTH(A.CreatedDate) = MONTH(GETDATE())
AND YEAR(A.CreatedDate) = YEAR(GETDATE())
AND A.EmployeeKey = " + Employee).ToInt();
        }
    }
}
