﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.SYS
{
    public class User_Info
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private int _CategoryKey = 0;
        private string _UserName = "";
        private string _Password = "";
        private string _Description = "";
        private int _Activated = 0;
        private DateTime _ExpireDate;
        private DateTime _LastLoginDate;
        private int _FailedPasswordAttemptCount = 0;
        private int _UnitLevel = 0;
        private int _EmployeeKey = 0;
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";
        private int _ManagerKey = 0;
        private string _EmployeeName = "";
        private string _ImageThumb = "";
        private string _CreatedBy = "";
        private DateTime _CreatedDate;
        private string _ModifiedBy = "";
        private DateTime _ModifiedDate;
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public string Key
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string Name
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Activated
        {
            get { return _Activated; }
            set { _Activated = value; }
        }
        public DateTime ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        public DateTime LastLoginDate
        {
            get { return _LastLoginDate; }
            set { _LastLoginDate = value; }
        }
        public int FailedPasswordAttemptCount
        {
            get { return _FailedPasswordAttemptCount; }
            set { _FailedPasswordAttemptCount = value; }
        }
        public int EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public int DepartmentKey
        {
            get
            {
                return _DepartmentKey;
            }

            set
            {
                _DepartmentKey = value;
            }
        }

        public int ManagerKey
        {
            get
            {
                return _ManagerKey;
            }

            set
            {
                _ManagerKey = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return _DepartmentName;
            }

            set
            {
                _DepartmentName = value;
            }
        }

        public string ImageThumb
        {
            get
            {
                return _ImageThumb;
            }

            set
            {
                _ImageThumb = value;
            }
        }

        public int UnitLevel
        {
            get
            {
                return _UnitLevel;
            }

            set
            {
                _UnitLevel = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public User_Info()
        { }
        public User_Info(string UserKey)
        {
            string zSQL = "SELECT * FROM SYS_Users WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _UserName = zReader["UserName"].ToString();
                    _Password = zReader["Password"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["Activated"] != DBNull.Value)
                        _Activated = int.Parse(zReader["Activated"].ToString());
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    if (zReader["FailedPasswordAttemptCount"] != DBNull.Value)
                        _FailedPasswordAttemptCount = int.Parse(zReader["FailedPasswordAttemptCount"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public User_Info(int EmployeeKey)
        {
            string zSQL = "SELECT * FROM SYS_Users WHERE EmployeeKey = @EmployeeKey AND Activated = 1";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _UserKey = zReader["UserKey"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _UserName = zReader["UserName"].ToString();
                    _Password = zReader["Password"].ToString();
                    _Description = zReader["Description"].ToString();
                    if (zReader["Activated"] != DBNull.Value)
                        _Activated = int.Parse(zReader["Activated"].ToString());
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    if (zReader["FailedPasswordAttemptCount"] != DBNull.Value)
                        _FailedPasswordAttemptCount = int.Parse(zReader["FailedPasswordAttemptCount"].ToString());
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public User_Info(string UserName, bool CheckUserName)
        {
            string zSQL = @"
SELECT A.*, C.DepartmentKey, C.DepartmentName, B.ManagerKey, B.ImageThumb, B.UnitLevel,
dbo.FNC_GetNameEmployee(A.EmployeeKey) AS EmployeeName 
FROM SYS_Users A
LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey
LEFT JOIN HRM_Departments C ON C.DepartmentKey = B.DepartmentKey
WHERE UserName = @UserName";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["UnitLevel"] != DBNull.Value)
                        _UnitLevel = int.Parse(zReader["UnitLevel"].ToString());
                    _UserKey = zReader["UserKey"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _UserName = zReader["UserName"].ToString();
                    _Password = zReader["Password"].ToString();
                    _Description = zReader["Description"].ToString();
                    _ImageThumb = zReader["ImageThumb"].ToString();
                    if (zReader["ExpireDate"] != DBNull.Value)
                        _ExpireDate = (DateTime)zReader["ExpireDate"];
                    if (zReader["LastLoginDate"] != DBNull.Value)
                        _LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    if (zReader["FailedPasswordAttemptCount"] != DBNull.Value)
                        _FailedPasswordAttemptCount = int.Parse(zReader["FailedPasswordAttemptCount"].ToString());
                    if (zReader["Activated"] != DBNull.Value)
                        _Activated = int.Parse(zReader["Activated"].ToString());
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["EmployeeKey"] != DBNull.Value)
                        _EmployeeKey = int.Parse(zReader["EmployeeKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["ManagerKey"] != DBNull.Value)
                        _ManagerKey = int.Parse(zReader["ManagerKey"].ToString());
                    _DepartmentName = zReader["DepartmentName"].ToString();
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    if (zReader["CreatedDate"] != DBNull.Value)
                        _CreatedDate = (DateTime)zReader["CreatedDate"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    if (zReader["ModifiedDate"] != DBNull.Value)
                        _ModifiedDate = (DateTime)zReader["ModifiedDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Users ("
        + "CategoryKey ,UserName ,Password ,Description ,Activated ,ExpireDate ,LastLoginDate ,FailedPasswordAttemptCount ,EmployeeKey ,CreatedBy ,CreatedDate ,ModifiedBy ,ModifiedDate ) "
         + " VALUES ( "
         + "@CategoryKey ,@UserName ,@Password ,@Description ,@Activated ,@ExpireDate ,@LastLoginDate ,@FailedPasswordAttemptCount ,@EmployeeKey ,@CreatedBy ,@CreatedDate ,@ModifiedBy ,@ModifiedDate ) "
         + " SELECT TOP 1 UserKey FROM SYS_Users ORDER BY CreatedDate DESC";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description;
                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = _Activated;
                if (_ExpireDate.Year == 0001)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                if (_LastLoginDate.Year == 0001)
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = _LastLoginDate;
                zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                if (_CreatedDate.Year == 0001)
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = _CreatedDate;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                if (_ModifiedDate.Year == 0001)
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedDate", SqlDbType.DateTime).Value = _ModifiedDate;
                _UserKey = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Users SET "
                        + " CategoryKey = @CategoryKey,"
                        + " UserName = @UserName,"
                        + " Description = @Description,"
                        + " Activated = @Activated,"
                        + " ExpireDate = @ExpireDate,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedDate = GETDATE()"
                       + " WHERE UserKey = @UserKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = _UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description;
                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = _Activated;
                if (_ExpireDate.Year == 0001)
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = _ExpireDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = _EmployeeKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_UserKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"
DELETE FROM SYS_Users WHERE UserKey = @UserKey
DELETE FROM SYS_User_Roles WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdatePass()
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Users SET "
                        + " Password=@Password, "
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedDate = GETDATE()"
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = _Password;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;

        }
        public string UpdateFailedPass()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Users SET "
                        + " FailedPasswordAttemptCount = FailedPasswordAttemptCount + 1 "
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                // zCommand.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = _FailedPasswordAttemptCount;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateDateLogin()
        {

            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Users SET "
                        + " LastLoginDate = GETDATE() "
                        + " WHERE UserKey = @UserKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;

                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                string nzMessage = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetActivated()
        {
            string zSQL = " UPDATE SYS_Users SET Activated = CASE Activated WHEN 0 THEN 1 ELSE 0 END, ModifiedBy = @ModifiedBy WHERE UserKey = @UserKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = _UserKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return zResult;
        }
        #endregion
    }
}
