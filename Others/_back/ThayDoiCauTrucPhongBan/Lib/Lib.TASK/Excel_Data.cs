﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Excel_Data
    {
        public static DataTable GetInfo(string AssetID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT TOP 1  A.*, C.SendTo FROM TASK_Excel_Detail A 
LEFT JOIN TASK_Note_Detail B ON A.InfoKey = B.InfoKey
LEFT JOIN TASK_Note C ON C.NoteKey = B.NoteKey 
WHERE A.Asset LIKE @Name ORDER BY InfoDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + AssetID + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM TASK_Excel ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Amount)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP " + Amount.ToString() + " * FROM TASK_Excel ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
