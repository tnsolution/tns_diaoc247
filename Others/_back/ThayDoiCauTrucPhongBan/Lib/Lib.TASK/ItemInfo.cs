﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.TASK
{
    public class ItemInfo
    {
        #region [ Field Name ]
        private int _InfoKey = 0;
        private int _ExcelKey = 0;
        private DateTime _InfoDate;
        private string _SirName = "";
        private string _LastName = "";
        private string _FirstName = "";
        private string _Phone1 = "";
        private string _Phone2 = "";
        private string _Email1 = "";
        private string _Email2 = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _ProductID = "";
        private string _Product = "";
        private string _Asset = "";
        private string _Description = "";
        private int _Status = 0;
        private DateTime _CreatedDate;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedDate;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int InfoKey
        {
            get { return _InfoKey; }
            set { _InfoKey = value; }
        }
        public int ExcelKey
        {
            get { return _ExcelKey; }
            set { _ExcelKey = value; }
        }
        public DateTime InfoDate
        {
            get { return _InfoDate; }
            set { _InfoDate = value; }
        }
        public string SirName
        {
            get { return _SirName; }
            set { _SirName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }
        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }
        public string Email1
        {
            get { return _Email1; }
            set { _Email1 = value; }
        }
        public string Email2
        {
            get { return _Email2; }
            set { _Email2 = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string Product
        {
            get { return _Product; }
            set { _Product = value; }
        }
        public string Asset
        {
            get { return _Asset; }
            set { _Asset = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
    }
}
