﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
namespace Lib.TASK
{
    public class Note_Detail_Info
    {

        #region [ Field Name ]
        private int _DetailKey = 0;
        private int _NoteKey = 0;
        private int _InfoKey = 0;
        private DateTime _DateRespone;
        private int _StatusRespone = 0;
        private int _StatusInfo = 0;
        private string _Description = "";
        private string _Message = "";
        private int _ObjectKey = 0;
        private string _ObjectTable = "";
        #endregion

        #region [ Constructor Get Information ]
        public Note_Detail_Info()
        {
        }
        public Note_Detail_Info(int DetailKey)
        {
            string zSQL = "SELECT * FROM TASK_Note_Detail WHERE DetailKey = @DetailKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = DetailKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["DetailKey"] != DBNull.Value)
                        _DetailKey = int.Parse(zReader["DetailKey"].ToString());
                    if (zReader["NoteKey"] != DBNull.Value)
                        _NoteKey = int.Parse(zReader["NoteKey"].ToString());
                    if (zReader["InfoKey"] != DBNull.Value)
                        _InfoKey = int.Parse(zReader["InfoKey"].ToString());
                    if (zReader["DateRespone"] != DBNull.Value)
                        _DateRespone = (DateTime)zReader["DateRespone"];
                    if (zReader["StatusRespone"] != DBNull.Value)
                        _StatusRespone = int.Parse(zReader["StatusRespone"].ToString());
                    if (zReader["StatusInfo"] != DBNull.Value)
                        _StatusInfo = int.Parse(zReader["StatusInfo"].ToString());
                    _Description = zReader["Description"].ToString();
                } zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }        
        #endregion

        #region [ Properties ]
        public int DetailKey
        {
            get { return _DetailKey; }
            set { _DetailKey = value; }
        }
        public int NoteKey
        {
            get { return _NoteKey; }
            set { _NoteKey = value; }
        }
        public int InfoKey
        {
            get { return _InfoKey; }
            set { _InfoKey = value; }
        }
        public DateTime DateRespone
        {
            get { return _DateRespone; }
            set { _DateRespone = value; }
        }
        public int StatusRespone
        {
            get { return _StatusRespone; }
            set { _StatusRespone = value; }
        }
        public int StatusInfo
        {
            get { return _StatusInfo; }
            set { _StatusInfo = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string ObjectTable
        {
            get
            {
                return _ObjectTable;
            }

            set
            {
                _ObjectTable = value;
            }
        }

        public int ObjectKey
        {
            get
            {
                return _ObjectKey;
            }

            set
            {
                _ObjectKey = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO TASK_Note_Detail ("
        + " NoteKey ,InfoKey ,DateRespone ,StatusRespone ,StatusInfo ,Description, ObjectKey, ObjectTable ) "
         + " VALUES ( "
         + "@NoteKey ,@InfoKey ,@DateRespone ,@StatusRespone ,@StatusInfo ,@Description ) "
         + " SELECT DetailKey FROM TASK_Note_Detail WHERE DetailKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = _DetailKey;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _InfoKey;
                if (_DateRespone.Year == 0001)
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = _DateRespone;
                zCommand.Parameters.Add("@StatusRespone", SqlDbType.Int).Value = _StatusRespone;
                zCommand.Parameters.Add("@StatusInfo", SqlDbType.Int).Value = _StatusInfo;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.Int).Value = _ObjectKey;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = _ObjectTable;
                _DetailKey = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Update()
        {
            string zSQL = "UPDATE TASK_Note_Detail SET "
                        + " NoteKey = @NoteKey,"
                        + " InfoKey = @InfoKey,"
                        + " DateRespone = @DateRespone,"
                        + " StatusRespone = @StatusRespone,"
                        + " StatusInfo = @StatusInfo,"
                        + " Description = @Description, ObjectKey = @ObjectKey, ObjectTable = @ObjectTable"
                       + " WHERE DetailKey = @DetailKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = _DetailKey;
                zCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = _NoteKey;
                zCommand.Parameters.Add("@InfoKey", SqlDbType.Int).Value = _InfoKey;
                if (_DateRespone.Year == 0001)
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = _DateRespone;
                zCommand.Parameters.Add("@StatusRespone", SqlDbType.Int).Value = _StatusRespone;
                zCommand.Parameters.Add("@StatusInfo", SqlDbType.Int).Value = _StatusInfo;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.Int).Value = _ObjectKey;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = _ObjectTable;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }        
        public string Save()
        {
            string zResult;
            if (_DetailKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM TASK_Note_Detail WHERE DetailKey = @DetailKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = _DetailKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateStatus()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE TASK_Note_Detail SET StatusInfo = @StatusInfo, DateRespone = @DateRespone, ObjectKey = @ObjectKey, ObjectTable = @ObjectTable WHERE DetailKey = @DetailKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@DetailKey", SqlDbType.Int).Value = _DetailKey;
                zCommand.Parameters.Add("@StatusInfo", SqlDbType.Int).Value = _StatusInfo;
                zCommand.Parameters.Add("@DateRespone", SqlDbType.DateTime).Value = _DateRespone;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.Int).Value = _ObjectKey;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = _ObjectTable;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string CustomSQL(string SQL)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = SQL;
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
