﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.TASK
{
    public class ReportItem
    {
        public int ReportKey { get; set; }
        public int EmployeeKey { get; set; }
        public int DepartmentKey { get; set; }
        public DateTime ReportDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public bool AllDay { get; set; }
    }
}
