﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Lib.Config;
using Lib.SYS;

namespace Lib.TASK
{
    public class Report_Data
    {
        public static DataTable ChartReport_Project(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT SUM(A.Income) AS INCOME, B.ProjectName
FROM FNC_Transaction A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
WHERE A.IsApproved = 1 AND IsFinish = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " GROUP BY B.ProjectName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChartReport_Department(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT SUM(A.Income) AS INCOME, C.DepartmentName
FROM FNC_Transaction A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN dbo.HRM_Departments C ON A.DepartmentKey = C.DepartmentKey
WHERE A.IsApproved = 1 AND IsFinish = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " GROUP BY C.DepartmentName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChartReport_Employee(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT SUM(A.Income) AS INCOME, C.LastName + ' ' + C.FirstName AS EmployeeName
FROM FNC_Transaction A
LEFT JOIN dbo.PUL_Project B ON A.ProjectKey = B.ProjectKey
LEFT JOIN dbo.HRM_Employees C ON C.EmployeeKey = A.EmployeeKey
WHERE A.IsApproved = 1 AND IsFinish = 1";
            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                zSQL += " AND A.FinishDate BETWEEN @FromDate AND @ToDate";
            if (EmployeeKey != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (DepartmentKey != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " GROUP BY C.LastName, C.FirstName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Exits(int Employee)
        {
            SqlContext sqlContext = new SqlContext();
            return sqlContext.GetObject(@"
SELECT ReportKey FROM TASK_Report A
WHERE DAY(A.CreatedDate) = DAY(GETDATE())
AND MONTH(A.CreatedDate) = MONTH(GETDATE())
AND YEAR(A.CreatedDate) = YEAR(GETDATE())
AND A.EmployeeKey = " + Employee).ToInt();
        }

        //---------------
        public static DataTable TableReport_Mobi(int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            int Month = ToDate.Month;

            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT A.CategoryKey, A.CategoryName, '' Result, SUM(A.Value) AS Require, C.ProductID
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey 
    AND MONTH(B.PlanDate) = @Month
    GROUP BY CategoryKey, ProductID, CategoryName
	UNION ALL
	SELECT A.CategoryKey, A.CategoryName, SUM(A.Result) AS Result ,''Require, C.ProductID
	FROM TASK_Report_Detail A 
	LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey
    AND B.CreatedDate BETWEEN @FromDate AND @ToDate
	GROUP BY CategoryKey, ProductID, CategoryName
) 
SELECT CAST( ProductID AS INT) AS ID, CategoryName, SUM(Result) AS Result, SUM(Require) AS Require FROM X
GROUP BY ProductID, CategoryName
ORDER BY ID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static List<ReportItem> GetReport(DateTime Start, DateTime End, int Department, int Employee)
        {
            string zQuery = "SELECT ReportKey, DepartmentKey, EmployeeKey, [Title], [Description], [Start], [End], AllDay FROM TASK_Report WHERE [Start]>=@start AND [End]<=@end";
            if (Employee != 0)
                zQuery += " AND EmployeeKey = @Employee";
            if (Department != 0)
                zQuery += " AND DepartmentKey = @Department";

            SqlContext zSql = new SqlContext();

            zSql.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            zSql.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            zSql.CMD.Parameters.Add("@start", SqlDbType.DateTime).Value = Start;
            zSql.CMD.Parameters.Add("@end", SqlDbType.DateTime).Value = End;

            return zSql.GetData(zQuery).DataTableToList<ReportItem>();
        }

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, C.DepartmentName, B.LastName + ' ' + B.FirstName AS EmployeeName
FROM TASK_Report A 
LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey 
LEFT JOIN HRM_Departments C ON C.DepartmentKey = B.DepartmentKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int Department, int Employee, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*, C.DepartmentName, B.LastName + ' ' + B.FirstName AS EmployeeName
FROM TASK_Report A 
LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey 
LEFT JOIN HRM_Departments C ON A.DepartmentKey = C.DepartmentKey
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @Department";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @Employee";
            zSQL += " ORDER BY A.ReportDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable GetResult(int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {           
            int Month = FromDate.Month;
            int Year = FromDate.Year;

            DataTable zTable = new DataTable();
            string zSQL = @"
WITH X AS
(
	SELECT A.CategoryKey, A.CategoryName, '' Result, SUM(A.Value) AS Require, C.ProductID
	FROM HRM_Plan_Detail A 
	LEFT JOIN HRM_Plan B ON A.PlanKey = B.PlanKey 
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey 
    AND MONTH(B.PlanDate) = @Month
    AND YEAR(B.PlanDate)= @Year
    GROUP BY CategoryKey, ProductID, CategoryName
	UNION
	SELECT A.CategoryKey, A.CategoryName, SUM(A.Result) AS Result ,''Require, C.ProductID
	FROM TASK_Report_Detail A 
	LEFT JOIN TASK_Report B ON A.ReportKey = B.ReportKey
    LEFT JOIN SYS_Categories C ON A.CategoryKey = C.AutoKey
	WHERE B.EmployeeKey = @EmployeeKey
    AND B.CreatedDate BETWEEN @FromDate AND @ToDate
	GROUP BY CategoryKey, ProductID, CategoryName
) 
SELECT CAST( ProductID AS INT) AS ID, CategoryName, SUM(Result) AS Result, SUM(Require) AS Require FROM X
GROUP BY ProductID, CategoryName
ORDER BY ID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Month", SqlDbType.Int).Value = Month;
                zCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
