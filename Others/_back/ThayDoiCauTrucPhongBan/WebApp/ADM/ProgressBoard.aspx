﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProgressBoard.aspx.cs" Inherits="WebApp.ADM.ProgressBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #tblDataEmployee .highlight {
            background-color: lightblue;
        }

        .td15 {
            width: 15%;
        }

        #tblDataBoard td:last-child {
            text-align: right;
        }
    </style>
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Cài đặt bảng lũy tiến
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <asp:DropDownList ID="DDLDepartment" runat="server" class="select2" AppendDataBoundItems="true" CssClass="form-control select2">
                    <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <div class="hr-2 hr-double"></div>
                <asp:Literal ID="LitEmployee" runat="server"></asp:Literal>
            </div>
            <div class="col-xs-9" id="panelright" style="display: none;">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <select class="select2" id="OpSelect" style="width: 100%">
                                <option value="-1" disabled="disabled" selected="selected">Chọn cách tính</option>
                                <option value="0">Gộp nguồn giao dịch</option>
                                <option value="1">Phân biệt nguồn giao dịch</option>
                            </select>
                        </div>
                        <div class="radio" style="display: none">
                            <label style="display: none">
                                <input name="form-field-radio" class="ace ace-checkbox-2" type="radio" value="1" />
                                <span class="lbl">Giao dịch trực tiếp</span>
                            </label>
                            <label style="display: none">
                                <input name="form-field-radio" class="ace ace-checkbox-2" type="radio" value="2" />
                                <span class="lbl">Giao dịch liên kết</span>
                            </label>
                            <label style="display: none">
                                <input name="form-field-radio" class="ace ace-checkbox-2" type="radio" value="3" />
                                <span class="lbl">Cả 2</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-9">
                        <span id="diengiai"></span>
                        <a class="btn btn-sm btn-info btn-round" id="btnUpdate">Cập nhật cách tính</a>
                    </div>
                </div>
                <div class="row" id="tabledetail">
                    <asp:Literal ID="LitTable1" runat="server"></asp:Literal>
                </div>
                <div class="form-action">
                    <div class="hr-2 hr-double"></div>
                    <p>
                        Chọn nhân viên cần copy bảng tính trực tiếp và liên kết. 
                        <span class="text-danger">Bạn phải chọn nhân viên đã có bảng lũy tiến liên kết hoặc trực tiếp trước đó hoặc phải nhập và lưu bảng trước khi copy, 
                            nếu nhân viên được copy đã có thông tin sẽ bị xóa để áp dụng bảng mới !.</span>
                    </p>
                    <asp:DropDownList ID="DDLEmployee" runat="server" AppendDataBoundItems="true" CssClass="select2">
                        <asp:ListItem Value="0" Text="Chọn nhân viên"></asp:ListItem>
                    </asp:DropDownList>
                    <button class="btn btn-info btn-sm" id="btnCopy" type="button">
                        <i class="ace-icon fa fa-copy"></i>&nbsp;Copy
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Employee" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="/template/table-edits.min.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $('#OpSelect').on('change', function () {
                if (this.value == 1) {
                    $("tr[select]").show();
                    $("#diengiai").text("Tách nguồn giao dịch trực tiếp và liên kết ra 2 bảng liên kết khách nhau");
                    $('input:radio[name=form-field-radio]').filter('[value=1]').prop('checked', true);
                }
                else {
                    $("tr[select]").hide();
                    $("tr[select=0]").show();
                    $("#diengiai").text("Tính tổng doanh thu nguồn giao dịch trực tiếp và liên kết");
                    $('input:radio[name=form-field-radio]').filter('[value=3]').prop('checked', true);
                }
            })

            $(".select2").select2();
            $("#btnCopy").click(function () {
                var employee1 = $("[id$=HID_Employee]").val();
                var employee2 = $("[id$=DDLEmployee]").val();
                if (employee2 == null || employee2 == 0) {
                    alert("Bạn phải chọn nhân viên cần copy");
                    return;
                }
                console.log(employee1);
                console.log(employee2);
                Copy(employee1, employee2);
            });
            $("#btnUpdate").click(function () {
                var employee = $("[id$=HID_Employee]").val();
                var method = $("input[name='form-field-radio']:checked").val();
                UpdateMethod(employee, method);
            });
            $("#tblDataEmployee tbody").on("click", "tr", function (e) {
                var selected = $(this).hasClass("highlight");
                $("#tblDataEmployee tr").removeClass("highlight");
                if (!selected)
                    $(this).addClass("highlight");
                var trid = $(this).closest('tr').attr('id');
                $("[id$=HID_Employee]").val(trid);
                SelectTable(trid);
            });
            $("[id$=DDLDepartment]").on('change', function (e) {
                var valueSelected = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        "DepartmentKey": valueSelected,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("#tblDataEmployee tbody");
                        District.empty();
                        $.each(msg.d, function () {
                            var object = this;
                            if (object !== '') {
                                District.append("<tr id='" + object.Value + "'><td>" + object.Text + "</td></tr>");
                            }
                        });
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("[id$=DDLMethod]").on('change', function (e) {
                var valueSelected = $(this).val();
                if (valueSelected == 1)
                    $("#diengiai").text("Tính lương hiệu quả trên từng giao dịch");
                if (valueSelected == 2)
                    $("#diengiai").text("Tính lương hiệu quả trên % tổng các giao dịch");
                if (valueSelected == 3)
                    $("#diengiai").text("Tính lương hiệu quả bằng cả 2 cách tính");
            });
        });

        function UpdateMethod(employee, method) {
            console.log(method);
            $.ajax({
                type: "POST",
                url: "/ADM/ProgressBoard.aspx/UpdateMethod",
                data: JSON.stringify({
                    "Employee": employee,
                    "Method": method,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d != '')
                        Page.showNotiMessageError("Thông báo", msg.d);
                    else
                        Page.showNotiMessageInfo("Thông báo", "Đã cập nhật cách tính");
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function SelectTable(id) {
            $.ajax({
                type: "POST",
                url: "/ADM/ProgressBoard.aspx/LoadBoard",
                data: JSON.stringify({
                    "Employee": id,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    $("#panelright").show();
                    $("#tabledetail").empty();
                    $("#tabledetail").append($(msg.d.Result));

                    var $radios = $('input:radio[name=form-field-radio]');
                    $radios.filter('[value=' + msg.d.Result2 + ']').prop('checked', true);
                    $('#OpSelect').val(msg.d.Result2).trigger('change');
                    console.log(msg.d.Result2);
                    InitTable();
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function InitTable() {
            var tableid = "tblDataBoard";
            $("#" + tableid).on("click", function () {
                $("#" + tableid + " tr").editable({
                    keyboard: true,
                    dblclick: true,
                    button: true,
                    buttonSelector: ".edit",
                    dropdowns: {},
                    maintainWidth: true,
                    edit: function (values) {
                        $(".edit i", this)
                        .removeClass('fa-pencil')
                        .addClass('fa-save')
                        .attr('title', 'Save');

                        $("td[data-field=FromMoney] input", this).number(true, 0);
                        $("td[data-field=ToMoney] input", this).number(true, 0);
                        $("td[data-field=Rate] input", this).number(true, 0);
                    },
                    save: function (values) {
                        $(".edit i", this)
                        .removeClass('fa-save')
                        .addClass('fa-pencil')
                        .attr('title', 'Edit');

                        var category = $(this).attr("category");
                        var employee = $("[id$=HID_Employee]").val();
                        var id = $(this).data('id');
                        var frommoney = Page.FormatMoney(values['FromMoney']);
                        var tomoney = Page.FormatMoney(values['ToMoney']);

                        $("td[data-field=FromMoney]", this).text(frommoney);
                        $("td[data-field=ToMoney]", this).text(tomoney);

                        console.log(category);
                        Save(employee, id, category, values);
                    },
                    cancel: function (values) { }
                });
            });
            $("#" + tableid).on("click", "a[btn='delete']", function (e) {
                var Key = $(this).closest('tr').attr('data-id');
                var employee = $("[id$=HID_Employee]").val();
                if (confirm("Bạn có chắc xóa")) {
                    Delete(employee, Key);
                    $(this).closest('tr').remove();
                }
            });
        }
        function Save(employee, autokey, category, object) {
            $.ajax({
                type: "POST",
                url: "/ADM/ProgressBoard.aspx/SaveRow",
                data: JSON.stringify({
                    "Employee": employee,
                    "AutoKey": autokey,
                    "Category": category,
                    "Object": object
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (msg) {
                    if (msg.d != '')
                        Page.showNotiMessageError("Thông báo", msg.d);
                    else
                        Page.showNotiMessageInfo("Thông báo", "Đã cập nhật");
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function Delete(employee, autokey) {
            $.ajax({
                type: "POST",
                url: "/ADM/ProgressBoard.aspx/DeleteRow",
                data: JSON.stringify({
                    "Employee": employee,
                    "AutoKey": autokey,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (msg) {
                    if (msg.d != '')
                        Page.showNotiMessageError("Thông báo", msg.d);
                    else
                        Page.showNotiMessageInfo("Thông báo", "Đã xóa");
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function Copy(employee1, employee2) {
            $.ajax({
                type: "POST",
                url: "/ADM/ProgressBoard.aspx/CopyRow",
                data: JSON.stringify({
                    "Employee1": employee1,
                    "Employee2": employee2,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "OK") {
                        Page.showNotiMessageError("Thông báo", msg.d.Message);
                        return;
                    }
                    else {
                        var Name = $("[id$=DDLEmployee] option:selected").text();
                        Page.showNotiMessageInfo("Thông báo", "Đã copy thành công cho " + Name);
                        $("#right").empty();
                        $("#right").append($(msg.d.Result));
                        InitTable();
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function AutoId() {
            return $("#tblDataBoard tr").length;
        }
        function AddRowTrucTiep() {
            var id = AutoId();
            var row = '<tr data-id=' + id + ' category="1">';
            row += '<td data-field="FromMoney">0</td>';
            row += '<td data-field="ToMoney">0</td>';
            row += '<td data-field="Rate">0</td>';
            row += '<td></td>';
            row += '<td><a class="btn btn-sm btn-white btn-default btn-round edit"><i class="fa fa-pencil"></i></a>&nbsp;<a class="btn btn-sm btn-white btn-default btn-round delete" btn="delete"><i class="fa fa-trash"></i></a></td>';
            row += '</tr>';
            $("#tblDataBoard tr[category='1']:last").after(row);
        }
        function AddRowLienKet() {
            var id = AutoId();
            var row = '<tr data-id=' + id + ' category="2">';
            row += '<td data-field="FromMoney">0</td>';
            row += '<td data-field="ToMoney">0</td>';
            row += '<td data-field="Rate">0</td>';
            row += '<td></td>';
            row += '<td><a class="btn btn-sm btn-white btn-default btn-round edit"><i class="fa fa-pencil"></i></a>&nbsp;<a class="btn btn-sm btn-white btn-default btn-round delete" btn="delete"><i class="fa fa-trash"></i></a></td>';
            row += '</tr>';
            $("#tblDataBoard tr[category='2']:last").after(row);
        }
    </script>
</asp:Content>
