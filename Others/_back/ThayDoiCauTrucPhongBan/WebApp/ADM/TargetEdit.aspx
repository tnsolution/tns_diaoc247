﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TargetEdit.aspx.cs" Inherits="WebApp.ADM.TargetEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
       <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Mục tiêu</label>
                        <div class="col-sm-5">
                            <input type="text" runat="server" class="form-control" id="txt_Target" placeholder="..." required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ghi chú</label>
                        <div class="col-sm-5">
                            <input type="text" runat="server" class="form-control" id="txt_Description" placeholder="..." />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự sắp xếp</label>
                        <div class="col-sm-5">
                            <input type="text" runat="server" class="form-control" id="txt_Rank" placeholder="..." />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TargetKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
        <script>
            var TargetKey = $("[id$=HID_TargetKey]").val();
        $(function () {
            $("#btnSave").on("click", function () {
                var Target = $('[id$=txt_Target]').val();
                var Description = $('[id$=txt_Description]').val();
                var Rank = $('[id$=txt_Rank]').val();
                $.ajax({
                    type: "POST",
                    url: "/ADM/TargetEdit.aspx/SaveTarget",
                    data: JSON.stringify({
                        "TargetKey": TargetKey,
                        "TargetName": Target,
                        "Description": Description,
                        "Rank": Rank,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.href = "/ADM/TargetList.aspx";
                        }
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnDelete").on("click", function () {
                if (TargetKey > 0) {
                    if (confirm("Bạn có chắc xóa")) {
                        $.ajax({
                            type: "POST",
                            url: "/ADM/TargetEdit.aspx/DeleteTarget",
                            data: JSON.stringify({
                                "TargetKey": TargetKey,
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $(".se-pre-con").fadeIn("slow");
                            },
                            success: function (msg) {
                                if (msg.d.Message != "") {
                                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                                }
                                else {
                                    window.location = "/ADM/TargetList.aspx";
                                }
                            },
                            complete: function () {
                                $('.se-pre-con').fadeOut('slow');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr.status);
                                console.log(xhr.responseText);
                                console.log(thrownError);
                            }
                        });
                    }
                }
                else {
                    Page.showNotiMessageInfo("Thông báo !", "Không có thông tin để xóa.");
                }
            });
        });
    </script>
</asp:Content>
