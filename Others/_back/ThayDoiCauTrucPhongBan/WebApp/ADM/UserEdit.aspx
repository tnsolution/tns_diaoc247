﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="UserEdit.aspx.cs" Inherits="WebApp.ADM.UserEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        table .checkbox {
            margin: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnReset">
                        <i class="ace-icon fa fa-refresh info"></i>
                        Reset mật khẩu
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="window.history.go(-1)">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                Thông tin
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#roles">
                                <i class="blue ace-icon fa fa-user bigger-110"></i>
                                Phân quyền
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane in active">
                            <div class="col-xs-12">
                                <div class="form-horizontal" id="validation-form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nhân viên</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="form-control select2" required>
                                                <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tài khoản</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" name="UserName" class="form-control" id="txt_UserName" placeholder="..." required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ghi chú</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Description" placeholder="nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tình trạng</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Status" runat="server" CssClass="form-control select2" required>
                                                <asp:ListItem Value="-1" Text="--Chọn tình trạng--" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="--Kích hoạt--"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="--Không kích hoạt--"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ngày hết hạn</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right" data-date-format="dd-mm-yyyy"
                                                    id="txt_ExpireDate" placeholder="Chọn ngày tháng năm" name="ExpireDate" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="roles" class="tab-pane">
                            <div class="col-xs-12">
                                <div class="row">
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mEditRoles" data-toggle="modal">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Thêm quyền
                                    </a>
                                </div>
                                <div class="hr hr-double"></div>
                                <asp:Literal ID="Lit_TableRole" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mEditRoles">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Quyền</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:DropDownList ID="DDL_Roles" CssClass="select2 form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input name="read" type="checkbox" class="ace" /><span class="lbl">Xem</span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="add" type="checkbox" class="ace" /><span class="lbl">Thêm</span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="edit" type="checkbox" class="ace" /><span class="lbl">Sửa</span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="del" type="checkbox" class="ace" /><span class="lbl">Xóa</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-info2 pull-right" data-dismiss="modal" id="btnUpdateRoles">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_UserKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_UserRoles" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        var UserKey = $("[id$=HID_UserKey]").val();
        $(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }

            $("#tblrole tbody").on("click", "tr", function () {
                var RoleKey = $(this).closest('tr').attr('rolekey');
                var UserKey = $("[id$=HID_UserKey]").val();
                $("[id$=DDL_Roles]").val(RoleKey);
                $.ajax({
                    type: "POST",
                    url: "/ADM/UserEdit.aspx/GetRole",
                    data: JSON.stringify({
                        "UserKey": UserKey,
                        "RoleKey": RoleKey,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $("#mEditRoles").modal("show");

                        var chkRead = $("#mEditRoles").find('input[name="read"]');
                        var chkAdd = $("#mEditRoles").find('input[name="add"]');
                        var chkEdit = $("#mEditRoles").find('input[name="edit"]');
                        var chkDelete = $("#mEditRoles").find('input[name="del"]');

                        $("[id$=DDL_Roles]").val(RoleKey).trigger("change");
                        if (msg.d.Result == 1) {
                            $(chkRead).prop('checked', true);
                        }
                        if (msg.d.Result2 == 1) {
                            $(chkAdd).prop('checked', true);
                        }
                        if (msg.d.Result3 == 1) {
                            $(chkEdit).prop('checked', true);
                        }
                        if (msg.d.Result4 == 1) {
                            $(chkDelete).prop('checked', true);
                        }
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnReset").click(function () {
                if (UserKey.length > 2) {
                    $.ajax({
                        type: "POST",
                        url: "/ADM/UserEdit.aspx/ResetPass",
                        data: JSON.stringify({
                            "UserKey": UserKey,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#btnReset").attr("disabled", "disabled");
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d.Message != "") {
                                Page.showPopupMessage("Lỗi !", msg.d.Message);
                            }
                            else {
                                Page.showNotiMessageInfo("Thông báo !", "Mật khẩu mới là 123456.");
                            }
                        },
                        complete: function () {
                            $("#btnReset").removeAttr("disabled");
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                } else {
                    Page.showNotiMessageInfo("Thông báo !", "Không có thông tin để đổi mật khẩu.");
                }
            });
            $("#btnDelete").click(function () {
                if (UserKey.length > 2) {
                    if (confirm("Bạn có chắc xóa thông tin")) {
                        $.ajax({
                            type: "POST",
                            url: "/ADM/UserEdit.aspx/DeleteUser",
                            data: JSON.stringify({
                                "UserKey": UserKey,
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#btnDelete").attr("disabled", "disabled");
                                $('.se-pre-con').fadeIn('slow');
                            },
                            success: function (msg) {
                                if (msg.d.Message != "") {
                                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                                }
                                else {
                                    window.location = "UserList.aspx";
                                    //Page.showNotiMessageInfo("Thông báo !", "Đã xóa thông tin thành công.");
                                    //$("input, select").val("");
                                }
                            },
                            complete: function () {
                                $("#btnDelete").removeAttr("disabled");
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr.status);
                                console.log(xhr.responseText);
                                console.log(thrownError);
                            }
                        });
                    }
                }
                else {
                    Page.showNotiMessageInfo("Thông báo !", "Không có thông tin để xóa.");
                }
            });
            $("#btnUpdateRoles").click(function () {
                var UserKey = $("[id$=HID_UserKey]").val();
                var RoleKey = $("[id$=DDL_Roles]").val();
                var isRead = 0, isAdd = 0, isEdit = 0, isDel = 0;

                var chkRead = $("#mEditRoles").find('input[name="read"]');
                var chkAdd = $("#mEditRoles").find('input[name="add"]');
                var chkEdit = $("#mEditRoles").find('input[name="edit"]');
                var chkDelete = $("#mEditRoles").find('input[name="del"]');

                if ($(chkRead).prop('checked'))
                    isRead = 1;
                if ($(chkAdd).prop('checked'))
                    isAdd = 1;
                if ($(chkEdit).prop('checked'))
                    isEdit = 1;
                if ($(chkDelete).prop('checked'))
                    isDel = 1;

                $.ajax({
                    type: "POST",
                    url: "/ADM/UserEdit.aspx/SaveRole",
                    data: JSON.stringify({
                        "UserKey": UserKey,
                        "RoleKey": RoleKey,
                        "Read": isRead,
                        "Add": isAdd,
                        "Edit": isEdit,
                        "Del": isDel,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showNotiMessageError("Lỗi !", msg.d.Message);
                        }
                    },
                    complete: function () {
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSave").click(function () {
                var valid = validate();
                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/ADM/UserEdit.aspx/SaveUser",
                        data: JSON.stringify({
                            "UserKey": UserKey,
                            "UserName": $("[id$=txt_UserName]").val(),
                            "Description": $("[id$=Description]").val(),
                            "ExpireDate": $("[id$=ExpireDate]").val(),
                            "Employee": $("[id$=Employee]").val(),
                            "Status": $("[id$=Status]").val(),
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $("#btnSave").attr("disabled", "disabled");
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d.Message != "") {
                                Page.showPopupMessage("Lỗi !", msg.d.Message);
                            }
                            else {
                                if (UserKey == "0")
                                    Page.showNotiMessageInfo("Thông báo !", "Tạo mới User thành công, mật khẩu mặc định là 123456.");
                                else
                                    Page.showNotiMessageInfo("Thông báo !", "Đã Sửa thông tin thông tin thành công.");

                                UserKey = msg.d.Result;
                            }
                        },
                        complete: function () {
                            $("#btnSave").removeAttr("disabled");
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $(".select2").select2({ width: "100%" });
        });

        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelectUser($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
