﻿

using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;

namespace WebApp
{
    public partial class Ajax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string ChangePass(string OldPass, string NewPass)
        {
            string UserName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["UserName"]);
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            ItemUser zResult = User_Data.CheckUser(UserName, OldPass);

            User_Info zUser = new User_Info();
            zUser.Key = UserKey;
            zUser.Name = UserName;
            zUser.Password = MyCryptography.HashPass(NewPass);
            zUser.ModifiedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zUser.UpdatePass();

            if (zUser.Message != string.Empty)
                return "ERROR";
            else
                return "OK";
        }
        [WebMethod]
        public static List<ItemWeb> GetDistricts(int ProvinceKey)
        {
            List<ItemWeb> zDistrict = Tools.GetDistricts(ProvinceKey);
            return zDistrict;
        }
        [WebMethod]
        public static List<ItemWeb> GetEmployees(int DepartmentKey)
        {
            List<ItemWeb> zDistrict = Tools.GetEmployee(DepartmentKey);
            return zDistrict;
        }
        //
        [WebMethod]
        public static List<ItemWeb> GetManager(int Department)
        {
            List<ItemWeb> zDistrict = Tools.GetEmployee(Department, 1);
            return zDistrict;
        }
        [WebMethod]
        public static List<ItemWeb> GetProject(int Department)
        {
            List<ItemWeb> zDistrict = Tools.GetProject(Department);
            return zDistrict;
        }
        [WebMethod]
        public static ItemWeb[] GetCategoryAsset(int ProjectKey)
        {
            List<ItemWeb> zList = new List<ItemWeb>();
            DataTable zTable = Project_Data.Category_Product(ProjectKey);
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb item = new ItemWeb();
                item.Text = zTable.Rows[i]["CategoryName"].ToString();
                item.Value = zTable.Rows[i]["CategoryKey"].ToString();
                zList.Add(item);
            }
            return zList.ToArray();
        }
        [WebMethod]
        public static string DeleteInput_Capital(int AutoKey)
        {
            Capital_Input_Info zInfo = new Capital_Input_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();
            if (zInfo.Message == string.Empty)
                return "OK";
            return zInfo.Message;
        }
        [WebMethod]
        public static string DeleteOutput_Capital(int AutoKey)
        {
            Capital_Output_Info zInfo = new Capital_Output_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();
            if (zInfo.Message == string.Empty)
                return "OK";
            return zInfo.Message;
        }
        [WebMethod]
        public static string DeleteReceipt(int AutoKey)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();
            if (zInfo.Message == string.Empty)
                return "OK";
            return zInfo.Message;
        }
        [WebMethod]
        public static string DeletePayment(int AutoKey)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();
            if (zInfo.Message == string.Empty)
                return "OK";
            return zInfo.Message;
        }

        [WebMethod]
        public static string Auto_Alert(int Employee)
        {
            try
            {
                string Mess = "";
                if (Notification_Data.CountReport(Employee) == 0)
                    Mess += "<div>Bạn chưa lập báo cáo ngày !. <a href='/SAL/ReportList.aspx'>Lập báo cáo</a></div>";
                if (Notification_Data.CountSchedule(Employee) == 0)
                    Mess += "<div>Bạn chưa lập kế hoạch !. <a href='/SAL/ScheduleList.aspx'>Lập kế hoạch</a></div>";

                return Mess;
            }
            catch (Exception ex)
            {
                Notification_Info zInfo = new Notification_Info();
                string strLog = "INSERT INTO SYS_Log_Notification ([Datetime],ERROR ) VALUES ('" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + "',N'Lỗi tự động nhắc nhở báo cáo, kế hoạch: " + ex.ToString() + "')";
                zInfo.InsertAuto(strLog); return string.Empty;
            }
        }

        [WebMethod]
        public static string Auto_Notification()
        {
            try
            {
                //sinh nhật khách
                #region MyRegion
                DataTable zCustomer = Notification_Data.GetDataCustomer();
                string zSQL = "";
                if (zCustomer.Rows.Count > 0)
                {
                    for (int t = 0; t < zCustomer.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zCustomer.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zCustomer.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zCustomer.Rows[t]["EmployeeName"].ToString();
                        int CustomerKey = Convert.ToInt32(zCustomer.Rows[t]["CustomerKey"]);
                        string BirthDay = Convert.ToDateTime(zCustomer.Rows[t]["Birthday"]).ToString("dd/MM/yyyy");
                        string Name = zCustomer.Rows[t]["CustomerName"].ToString();
                        string Phone = zCustomer.Rows[t]["Phone1"].ToString();
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectName, ObjectProduct, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Khách sinh nhật','" + BirthDay + "',N'" + Name + "','" + Phone + "',N'" + EmployeeName + "',1,'CRM_Customer'," + CustomerKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                //căn hộ hết hạn
                #region MyRegion
                DataTable zProduct = Notification_Data.GetDataApartment();
                if (zProduct.Rows.Count > 0)
                {
                    for (int t = 0; t < zProduct.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zProduct.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zProduct.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zProduct.Rows[t]["EmployeeName"].ToString();
                        int AssetKey = Convert.ToInt32(zProduct.Rows[t]["AssetKey"]);
                        string ProjectName = zProduct.Rows[t]["ProjectName"].ToString();
                        string DateContractEnd = Convert.ToDateTime(zProduct.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                        string Name = zProduct.Rows[t]["ApartmentName"].ToString();
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectProduct, ObjectName, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Căn hộ','" + DateContractEnd + "',N'" + ProjectName + "',N'" + Name + "',N'" + EmployeeName + "',3,'PUL_Resale_Apartment'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                //nhà phố hết hạn
                #region MyRegion
                DataTable zHouse = Notification_Data.GetDataHouse();
                if (zHouse.Rows.Count > 0)
                {
                    for (int t = 0; t < zHouse.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zHouse.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zHouse.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zHouse.Rows[t]["EmployeeName"].ToString();
                        int AssetKey = Convert.ToInt32(zHouse.Rows[t]["AssetKey"]);
                        string ProjectName = zHouse.Rows[t]["ProjectName"].ToString();
                        string DateContractEnd = Convert.ToDateTime(zHouse.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                        string Name = zHouse.Rows[t]["ApartmentName"].ToString();
                        //chen cho admin
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectProduct, ObjectName, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Nhà phố','" + DateContractEnd + "',N'" + ProjectName + "',N'" + Name + "',N'" + EmployeeName + "',3,'PUL_Resale_House'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                //đất nền hết hạn
                #region MyRegion
                //                DataTable zGround = Notification_Data.GetDataGround();
                //                if (zHouse.Rows.Count > 0)
                //                {
                //                    for (int t = 0; t < zHouse.Rows.Count; t++)
                //                    {
                //                        int DepartmentKey = Convert.ToInt32(zGround.Rows[t]["DepartmentKey"]);
                //                        int EmployeeKey = Convert.ToInt32(zGround.Rows[t]["EmployeeKey"]);
                //                        string EmployeeName = zGround.Rows[t]["EmployeeName"].ToString();
                //                        int AssetKey = Convert.ToInt32(zGround.Rows[t]["AssetKey"]);
                //                        string ProjectName = zGround.Rows[t]["ProjectName"].ToString();
                //                        string DateContractEnd = Convert.ToDateTime(zGround.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                //                        string Name = zGround.Rows[t]["ApartmentName"].ToString();
                //                        zSQL += @"
                //INSERT INTO [dbo].[SYS_Notification]
                //(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectProduct, ObjectName, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
                //Values
                //(" + EmployeeKey + "," + DepartmentKey + ",N'Đất nền','" + DateContractEnd + "',N'" + ProjectName + "',N'" + Name + "',N'" + EmployeeName + "',3,'PUL_Resale_Ground'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                //                    }
                //                }
                #endregion

                //giao dich hết hạn.
                #region MyRegion
                DataTable zTransaction = Notification_Data.GetDataTransaction();
                if (zTransaction.Rows.Count > 0)
                {
                    for (int t = 0; t < zTransaction.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zTransaction.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zTransaction.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zTransaction.Rows[t]["EmployeeName"].ToString();
                        int AssetKey = Convert.ToInt32(zTransaction.Rows[t]["TransactionKey"]);
                        string Product = zTransaction.Rows[t]["ProjectKey"].ToString();
                        string DateContractEnd = Convert.ToDateTime(zTransaction.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                        string Name = zTransaction.Rows[t]["AssetID"].ToString();
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectName, ObjectProduct, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Căn hộ','" + DateContractEnd + "',N'" + Name + "',N'" + Product + "',N'" + EmployeeName + "',3,'PUL_Resale_Apartment'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                Notification_Info zInfo = new Notification_Info();
                string strLog = @"
INSERT INTO SYS_Log_Notification ([Datetime],RecordCustomer,RecordBuilding,RecordHouse,RecordGround,RecordTrade ) VALUES ('"
+ DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + "',"
+ zCustomer.Rows.Count + ","
+ zProduct.Rows.Count + ","
+ zHouse.Rows.Count + ",0,"
+ zTransaction.Rows.Count + ")";

                zInfo.InsertAuto(strLog);
                if (zSQL != string.Empty)
                {
                    zInfo.InsertAuto(zSQL);
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Notification_Info zInfo = new Notification_Info();
                string strLog = "INSERT INTO SYS_Log_Notification ([Datetime],ERROR ) VALUES ('" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + "',N'" + ex.ToString() + "')";
                zInfo.InsertAuto(strLog);

                return ex.ToString();
            }
        }
    }
}