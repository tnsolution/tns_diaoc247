﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class AutoComplete_Asset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = "{\"result\":\"ok\"}";
            try
            {
                string temp = "";
                DataTable zTable = Product_Data.GetAsset_Auto(Request["ProjectKey"].ToInt(), Request["query"].ToString());
                foreach (DataRow r in zTable.Rows)
                {
                    temp += "\"" + r["AssetID"].ToString().Trim() + "\",";
                }
                json = "[" + temp.Remove(temp.LastIndexOf(','), 1) + "]";
            }
            catch (Exception ex)
            {
                json = "{\"result\":\"fail\"}";
            }

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(json);
            Response.End();
        }
    }
}