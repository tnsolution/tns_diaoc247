﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp
{
    public partial class Auto_Notification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //sinh nhật khách
                #region MyRegion
                DataTable zCustomer = Notification_Data.GetDataCustomer();
                string zSQL = "";
                if (zCustomer.Rows.Count > 0)
                {
                    for (int t = 0; t < zCustomer.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zCustomer.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zCustomer.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zCustomer.Rows[t]["EmployeeName"].ToString();
                        int CustomerKey = Convert.ToInt32(zCustomer.Rows[t]["CustomerKey"]);
                        string BirthDay = Convert.ToDateTime(zCustomer.Rows[t]["Birthday"]).ToString("dd/MM/yyyy");
                        string Name = zCustomer.Rows[t]["CustomerName"].ToString();
                        string Phone = zCustomer.Rows[t]["Phone1"].ToString();
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectName, ObjectProduct, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Khách sinh nhật','" + BirthDay + "',N'" + Name + "','" + Phone + "',N'" + EmployeeName + "',1,'CRM_Customer'," + CustomerKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                //căn hộ hết hạn
                #region MyRegion
                DataTable zProduct = Notification_Data.GetDataApartment();
                if (zProduct.Rows.Count > 0)
                {
                    for (int t = 0; t < zProduct.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zProduct.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zProduct.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zProduct.Rows[t]["EmployeeName"].ToString();
                        int AssetKey = Convert.ToInt32(zProduct.Rows[t]["AssetKey"]);
                        string ProjectName = zProduct.Rows[t]["ProjectName"].ToString();
                        string DateContractEnd = Convert.ToDateTime(zProduct.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                        string Name = zProduct.Rows[t]["ApartmentName"].ToString();
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectProduct, ObjectName, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Căn hộ','" + DateContractEnd + "',N'" + ProjectName + "',N'" + Name + "',N'" + EmployeeName + "',3,'PUL_Resale_Apartment'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                //nhà phố hết hạn
                #region MyRegion
                DataTable zHouse = Notification_Data.GetDataHouse();
                if (zHouse.Rows.Count > 0)
                {
                    for (int t = 0; t < zHouse.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zHouse.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zHouse.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zHouse.Rows[t]["EmployeeName"].ToString();
                        int AssetKey = Convert.ToInt32(zHouse.Rows[t]["AssetKey"]);
                        string ProjectName = zHouse.Rows[t]["ProjectName"].ToString();
                        string DateContractEnd = Convert.ToDateTime(zHouse.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                        string Name = zHouse.Rows[t]["ApartmentName"].ToString();
                        //chen cho admin
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectProduct, ObjectName, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Nhà phố','" + DateContractEnd + "',N'" + ProjectName + "',N'" + Name + "',N'" + EmployeeName + "',3,'PUL_Resale_House'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                //đất nền hết hạn
                #region MyRegion
                //                DataTable zGround = Notification_Data.GetDataGround();
                //                if (zHouse.Rows.Count > 0)
                //                {
                //                    for (int t = 0; t < zHouse.Rows.Count; t++)
                //                    {
                //                        int DepartmentKey = Convert.ToInt32(zGround.Rows[t]["DepartmentKey"]);
                //                        int EmployeeKey = Convert.ToInt32(zGround.Rows[t]["EmployeeKey"]);
                //                        string EmployeeName = zGround.Rows[t]["EmployeeName"].ToString();
                //                        int AssetKey = Convert.ToInt32(zGround.Rows[t]["AssetKey"]);
                //                        string ProjectName = zGround.Rows[t]["ProjectName"].ToString();
                //                        string DateContractEnd = Convert.ToDateTime(zGround.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                //                        string Name = zGround.Rows[t]["ApartmentName"].ToString();
                //                        zSQL += @"
                //INSERT INTO [dbo].[SYS_Notification]
                //(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectProduct, ObjectName, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
                //Values
                //(" + EmployeeKey + "," + DepartmentKey + ",N'Đất nền','" + DateContractEnd + "',N'" + ProjectName + "',N'" + Name + "',N'" + EmployeeName + "',3,'PUL_Resale_Ground'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                //                    }
                //                }
                #endregion

                //giao dich hết hạn.
                #region MyRegion
                DataTable zTransaction = Notification_Data.GetDataTransaction();
                if (zTransaction.Rows.Count > 0)
                {
                    for (int t = 0; t < zTransaction.Rows.Count; t++)
                    {
                        int DepartmentKey = Convert.ToInt32(zTransaction.Rows[t]["DepartmentKey"]);
                        int EmployeeKey = Convert.ToInt32(zTransaction.Rows[t]["EmployeeKey"]);
                        string EmployeeName = zTransaction.Rows[t]["EmployeeName"].ToString();
                        int AssetKey = Convert.ToInt32(zTransaction.Rows[t]["TransactionKey"]);
                        string Product = zTransaction.Rows[t]["ProjectKey"].ToString();
                        string DateContractEnd = Convert.ToDateTime(zTransaction.Rows[t]["DateContractEnd"]).ToString("dd/MM/yyyy");
                        string Name = zTransaction.Rows[t]["AssetID"].ToString();
                        zSQL += @"
INSERT INTO [dbo].[SYS_Notification]
(EmployeeKey, DepartmentKey, [Message], ObjectDate, ObjectName, ObjectProduct, EmployeeName, Type, ObjectTableName, ObjectTable , IsRead, ReadDate, CreatedDate, IsShow)
Values
(" + EmployeeKey + "," + DepartmentKey + ",N'Căn hộ','" + DateContractEnd + "',N'" + Name + "',N'" + Product + "',N'" + EmployeeName + "',3,'PUL_Resale_Apartment'," + AssetKey + ",0,NULL,GETDATE(),1) ";
                    }
                }
                #endregion

                Notification_Info zInfo = new Notification_Info();
                string strLog = @"
INSERT INTO SYS_Log_Notification ([Datetime],RecordCustomer,RecordBuilding,RecordHouse,RecordGround,RecordTrade ) VALUES ('"
+ DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + "',"
+ zCustomer.Rows.Count + ","
+ zProduct.Rows.Count + ","
+ zHouse.Rows.Count + ",0,"
+ zTransaction.Rows.Count + ")";

                zInfo.InsertAuto(strLog);
                if (zSQL != string.Empty)
                {
                    zInfo.InsertAuto(zSQL);                    
                }                
            }
            catch (Exception ex)
            {
                Notification_Info zInfo = new Notification_Info();
                string strLog = "INSERT INTO SYS_Log_Notification ([Datetime],ERROR ) VALUES ('" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + "',N'" + ex.ToString() + "')";
                zInfo.InsertAuto(strLog);              
            }
        }
    }
}