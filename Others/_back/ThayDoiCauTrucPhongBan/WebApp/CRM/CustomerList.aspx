﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CustomerList.aspx.cs" Inherits="WebApp.CRM.CustomerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .table-detail {
            max-height: 240px;
            overflow: scroll;
        }

        .cuonchamsoc {
            max-height: 430px;
            overflow-y: auto;
        }

        #divcare {
            height: 230px;
            overflow-y: auto;
        }

        sup {
            vertical-align: sup;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Danh sách khách hàng
                <span class="tools pull-right">
                    <a href="#mInitCustomer" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm khách
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Loại khách hàng</label>
                        <asp:DropDownList ID="DDL_Category" CssClass="select2" runat="server">
                            <asp:ListItem Value="0" Text="Tất cả" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Quan tâm"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Đã giao dịch"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Ký gữi đã giao dịch"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="quantam">
                        <label>Dự án quan tâm/ hoặc đã giao dịch</label>
                        <asp:DropDownList ID="DDL_Project" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Chọn dự án--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_AssetCategory" runat="server" CssClass="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Chọn loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Thông tin khách</label>
                        <input type="text" id="txtName" class="form-control" placeholder="Họ và tên khách" />
                        <input type="text" id="txtPhone" class="form-control" placeholder="Số điện thoại" />
                    </div>
                    <div class="form-group">
                        <div id="giaodich">
                            <label>Loại giao dịch</label>
                            <div class="radio">
                                <label>
                                    <input type="checkbox" class="ace" value="228" name="chkTradeType" />
                                    <span class="lbl">Chuyển nhượng</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="checkbox" class="ace" value="230" name="chkTradeType" />
                                    <span class="lbl">Bán mới</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="checkbox" class="ace" value="0" name="chkTradeType" />
                                    <span class="lbl">Cho thuê</span>
                                </label>
                            </div>
                        </div>
                        <div id="tintrang">
                            <label>Tình trạng</label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="ace" value="68" name="chkStatus" />
                                    <span class="lbl">Rất tiềm năng</span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="ace" value="69" name="chkStatus" />
                                    <span class="lbl">Tiềm năng</span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="ace" value="70" name="chkStatus" />
                                    <span class="lbl">Không tiềm năng</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="quanly">
                        <label>Phòng, nhân viên</label>
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Employee" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade modal-default" id="mInitCustomer" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Thêm thông tin khách hàng</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" class="ace" value="0" name="rdoCustomerType" />
                                <span class="lbl">Cá nhân</span>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" class="ace" value="1" name="rdoCustomerType" />
                                <span class="lbl">Doanh nghiệp</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" class="ace" value="0" name="rdoCustomerTrade" />
                                <span class="lbl">Quan tâm</span>
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" class="ace" value="1" name="rdoCustomerTrade" />
                                <span class="lbl">Đã giao dịch</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" id="txtCustomerName" class="form-control" placeholder="Họ và tên khách" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnInitCustomer">Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Manager" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/CRM/CustomerList.js"></script>
</asp:Content>
