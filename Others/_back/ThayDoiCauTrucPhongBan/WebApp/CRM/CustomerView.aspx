﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CustomerView.aspx.cs" Inherits="WebApp.CRM.CustomerView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .cuonchamsoc {
            max-height: 430px;
            overflow-y: auto;
        }

        #divcare {
            height: 230px;
            overflow-y: auto;
        }

        .indam {
            font-weight: bolder;
        }

        .profile-user-info {
            width: 100% !important;
        }

        .profile-info-name {
            width: 150px !important;
        }

        #scroll table th {
            padding: 0px !important;
        }

        #scroll table td {
            padding-left: 4px !important;
            padding-right: 4px !important;
        }

        #scroll table .td1 {
            width: 10%;
        }

        #scroll table .td2 {
            width: 15%;
        }

        #scroll table .td20 {
            width: 20%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <span id="tieude">
                    <asp:Literal ID="txt_Name" runat="server"></asp:Literal></span>
                <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                <a class="btn btn-white btn-info btn-bold" id="btnExeReminder" style="display: none">
                    <i class="ace-icon fa fa-adjust blue"></i>
                    Xử lý nhắc nhở
                </a>
                <a href="#" class="btn btn-warning btn-white btn-bold" id="btnExpand">Mở rộng</a>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:goBackWithRefresh();">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trờ về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6" id="leftbox">
                <table id="tblMain" class="table table-bordered table-hover">
                    <tbody>
                        <tr class="action indam">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin làm việc</td>
                            <td style="width: 1%">
                                <a href="#" tabindex="-1" class="btn btn-link" id="GetInfoCustomer" isedit><i class='ace-icon fa fa-pencil-square-o orange bigger-130'></i></a>
                            </td>
                        </tr>
                        <tr class="detail-row open">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="thongtincoban">
                                            <asp:Literal ID="Lit_InfoBasic" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Quan tâm & chăm sóc</td>
                            <td style="width: 1%">
                                <a href="#" tabindex="-1" class="btn btn-link" isedit data-toggle="modal" data-target="#mWant"><i class='ace-icon fa fa-pencil-square-o orange bigger-130'></i></a>
                            </td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row" id="quantam">
                                        <asp:Literal ID="Lit_InfoConsent" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Chi tiết liên lạc</td>
                            <td style="width: 1%"><a href="#" tabindex="-1" class="btn btn-link" id="GetInfoAddress" isedit><i class='ace-icon fa fa-pencil-square-o orange bigger-130'></i></a></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="chitietlienlac">
                                            <asp:Literal ID="Lit_InfoDetail" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Thông tin quản lý</td>
                            <td style="width: 1%"><a href="#" tabindex="-1" class="btn btn-link" id="GetInfoShare" isedit><i class="ace-icon fa fa-pencil-square-o orange bigger-130"></i></a></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row">
                                        <div class="col-xs-12" id="thongtinquanly">
                                            <asp:Literal ID="Lit_Manager" runat="server"></asp:Literal>
                                            <asp:Literal ID="Lit_Allow" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="action">
                            <td style="width: 1%"><a href="#" class="green bigger-130 show-details-btn" title="Show Details">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span class="sr-only">Details</span>
                            </a></td>
                            <td>Giao dịch</td>
                            <td style="width: 1%; text-align: center"><span id="tonggiaodich">
                                <asp:Literal ID="txt_TotalTrade" runat="server"></asp:Literal></span></td>
                        </tr>
                        <tr class="detail-row">
                            <td colspan="3">
                                <div class="table-detail">
                                    <div class="row" id="giaodich">
                                        <asp:Literal ID="Lit_InfoTrade" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-6">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Khách hàng</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div id="scroll">
                                <asp:Literal ID="Lit_RightTable" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mShare">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chia sẽ thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal' id='btnSaveShare'>
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="mAddress">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin liên lạc</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ngày sinh</label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_Birthday" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CMND/Passport</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CardID" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ngày cấp</label>
                                    <div class="col-sm-9">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_CardDate" placeholder="Chọn ngày tháng năm" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nơi cấp</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CardPlace" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group" custype="1">
                                    <label class="col-sm-3 control-label">Tên doanh nghiệp</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CompanyName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group" custype="1">
                                    <label class="col-sm-3 control-label">Mã số thuế</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Tax" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group" custype="0">
                                    <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address1" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group" custype="0">
                                    <label class="col-sm-3 control-label">Địa chỉ liên lạc</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group" custype="1">
                                    <label class="col-sm-3 control-label">Địa chỉ</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address3" placeholder="Nhập text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal' id='btnSaveAddress'>
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="mInfo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin khách</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" custype="0">Họ tên</label>
                                    <label class="col-sm-3 control-label" custype="1">Đại diện doanh nghiệp</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT1</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="txt_Phone1" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT2</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nguồn khách</label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="DDL_Source" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" required>
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="txt_SourceNote" placeholder="Chi tiết nguồn" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mức độ tiềm năng</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="DDL_Status" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" required>
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ghi chú</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Description" placeholder="..." />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal' id='btnSaveInfo'>
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="mCare" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Nội dung chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <asp:DropDownList ID="DDL_Status2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                        <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                    </asp:DropDownList>
                    <hr />
                    <textarea id="txt_Care" rows="5" cols="20" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveCare" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mWant" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin quan tâm/ chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nhu cầu</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Want" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn nhu cầu--"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án</label>
                            <div class="col-sm-9">
                                <asp:ListBox ID="LB_Project" runat="server" SelectionMode="Multiple" CssClass="select2" data-placeholder="--Chọn--"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án khác</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại sản phẩm</label>
                            <div class="col-sm-9">
                                <asp:ListBox ID="LB_Category" runat="server" SelectionMode="Multiple" CssClass="form-control select2" data-placeholder="--Chọn--" Style="width: 100%"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Giá (VNĐ)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Price" placeholder="Nhập số" moneyinput />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Diện tích (m<sup>2</sup>)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Area" placeholder="Nhập số" areainput />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số PN</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Bed" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1PN"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2PN"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3PN"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4PN"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5PN"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nội thất</label>
                            <div class="col-sm-9">
                                <asp:ListBox ID="LB_Furniture" runat="server" SelectionMode="Multiple" CssClass="form-control select2" data-placeholder="--Chọn--" Style="width: 100%"></asp:ListBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Quan tâm khác
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 60px;" placeholder="...."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveWant" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Thêm
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_CustomerType" runat="server" Value="0" />
    <asp:HiddenField ID="HID_ConsentKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_RemindKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/tableHeadFixer.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script src="CustomerView.js"></script>
    <script>
        $(document).ready(function () {
            $('input[moneyinput]').number(true, 0);
            $('input[areainput]').number(true, 2);
        });
    </script>
</asp:Content>
