﻿using Lib.SYS;
using System;
using System.Web;

namespace WebApp.Controls
{
    public partial class TopLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                int NumCapital = 0;
                int NumBirthday = 0;
                int NumTrade = 0;
                int NumAsset = 0;
                int NumTask = 0;
                int NumReceipt = 0;
                int NumPayment = 0;
                int NumTransfer = 0;
                int NumTicketOff = Notification_Data.Count_TicketOff(EmployeeKey);
                int Salary = Notification_Data.Count_Salary(EmployeeKey);

                switch (UnitLevel)
                {
                    case 0:
                    case 1:
                        NumTransfer = Notification_Data.Count_Transfer(0, 0);
                        NumBirthday = Notification_Data.Count_Notification(1, 0, 0);
                        NumTrade = Notification_Data.Count_Notification(2, 0, 0);
                        NumAsset = Notification_Data.Count_Notification(3, 0, 0);
                        NumTask = Notification_Data.Count_Task(0, 0);
                        NumReceipt = Notification_Data.Count_Receipt(0, 1);
                        NumPayment = Notification_Data.Count_Payment(0, 1);
                        NumCapital = Notification_Data.Count_Capital();
                        break;

                    case 2:
                        NumTransfer = Notification_Data.Count_Transfer(0, DepartmentKey);
                        NumBirthday = Notification_Data.Count_Notification(1, 0, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, 0, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, 0, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(0, DepartmentKey);
                        NumReceipt = Notification_Data.Count_Receipt(DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DepartmentKey, 1);
                        break;

                    case 3:
                        NumBirthday = Notification_Data.Count_Notification(1, EmployeeKey, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, EmployeeKey, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, EmployeeKey, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(EmployeeKey, DepartmentKey);
                        NumReceipt = Notification_Data.Count_Receipt(DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DepartmentKey, 1);
                        NumTransfer = Notification_Data.Count_Transfer(EmployeeKey, DepartmentKey);
                        break;

                    default:
                        NumBirthday = Notification_Data.Count_Notification(1, EmployeeKey, DepartmentKey);
                        NumTrade = Notification_Data.Count_Notification(2, EmployeeKey, DepartmentKey);
                        NumAsset = Notification_Data.Count_Notification(3, EmployeeKey, DepartmentKey);
                        NumTask = Notification_Data.Count_Task(EmployeeKey, DepartmentKey);
                        NumReceipt = Notification_Data.Count_Receipt(DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DepartmentKey, 1);
                        NumTransfer = Notification_Data.Count_Transfer(EmployeeKey, DepartmentKey);
                        break;
                }

                Lit_TicketOff.Text = NumTicketOff.ToString();
                Lit_Transfer.Text = NumTransfer.ToString();
                Lit_Birthday.Text = NumBirthday.ToString();
                Lit_TradeExpired.Text = NumTrade.ToString();
                Lit_AssetExpired.Text = NumAsset.ToString();
                Lit_Task.Text = NumTask.ToString();
                Lit_TotalRemind.Text = (NumBirthday + NumTrade + NumAsset).ToString();
                Lit_TotalTask.Text = (NumTask + NumTransfer).ToString();
                Lit_FeePlay.Text = Notification_Data.Count_FeePlay(EmployeeKey).ToString();

              
                MenuFNC(NumPayment.ToString(), NumReceipt.ToString(), NumCapital.ToString(), (NumPayment + NumCapital + NumReceipt).ToString(), Salary.ToString());
            }
        }

        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void MenuFNC(string PayCount, string ReceiptCount, string CapitalCount, string TotalCount, string Salary)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "ACC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');

            if (result[0].ToInt() == 0)
                return;
            if (result[3].ToInt() == 1)
            {
                #region [html menu fnc]
                LitMenuFNC.Text = @"
    <li id='menuFNC'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-usd bigger-110'></i>&nbsp;Thu - Chi
            <span class='badge badge-warning icon-animated-vertical'>" + TotalCount + @"</span>&nbsp;<i class='ace-icon fa fa-angle-down bigger-110'></i>
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>
            <li><a href='../FNC/Capital.aspx'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Quỹ công ty <span class='badge badge-warning'>" + CapitalCount + @"</span></a></li>         
            <li><a href='../FNC/Receipt.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Thu <span class='badge badge-warning'>" + ReceiptCount + @"</span></a></li>
            <li><a href='../FNC/Payment.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Chi <span class='badge badge-warning'>" + PayCount + @"</span></a></li>
            <li id='subFNC'><a href='../FNC/FeeCheck.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Bảng cân đối</a></li>
        </ul>
    </li>";
                #endregion
            }
            else
            {
                #region [html menu fnc]
                LitMenuFNC.Text = @"
    <li id='menuFNC'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-usd bigger-110'></i>&nbsp;Thu - Chi
            <span class='badge badge-warning icon-animated-vertical'>" + TotalCount + @"</span>&nbsp;<i class='ace-icon fa fa-angle-down bigger-110'></i>
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>          
            <li><a href='../FNC/Receipt.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Thu <span class='badge badge-warning'>" + ReceiptCount + @"</span></a></li>
            <li><a href='../FNC/Payment.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Chi <span class='badge badge-warning'>" + PayCount + @"</span></a></li>
            <li id='subFNC'><a href='../FNC/FeeCheck.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Bảng cân đối</a></li>
        </ul>
    </li>";
                #endregion                
            }
            RolePage = "ACC01";
            result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            string bag = "";
            if (Salary != "0")
            {
                bag = @"<span class='badge badge-warning bigger-110'>" + Salary + "</span>";
            }
            if (result[3].ToInt() == 1)
            {
                #region [html salary]
                LitMenuSalary.Text = @"
    <li>
        <a href='#' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-envelope bigger-110'></i>&nbsp;Lương&nbsp;" + bag + @"
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>
            <li>
                <a href='../HRM/SalaryList2.aspx'>
                    <i class='ace-icon fa fa-info bigger-110 blue'></i>
                    Bảng lương              
                </a>
            </li>
            <li>
                <a href='../HRM/SalaryEdit.aspx'>
                    <i class='ace-icon fa fa-info bigger-110 blue'></i>
                    Làm lương                 
                </a>
            </li>
        </ul>
    </li>";
                #endregion
            }
            else
            {                
                LitMenuSalary.Text = @"<li><a href='../HRM/SalaryList1.aspx'><i class='ace-icon fa fa-envelope bigger-110'></i>&nbsp;Lương&nbsp;" + bag + "</a></li>";
            }
        }
    }
}