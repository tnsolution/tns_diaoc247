﻿using Lib.FNC;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class CapitalInput : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL_Month(DDL_Month);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                LoadData();
            }
        }
        void LoadData()
        {
            int CurrentUnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int CurrentDepartment = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            DataTable zTable = Capital_Input_Data.List();
            StringBuilder zSb = new StringBuilder();

            #region [Thu Quỹ]
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblCapital'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Ngày thu</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th>Người giao tiền</th>");
            zSb.AppendLine("        <th>Người nhận tiền</th>");
            zSb.AppendLine("        <th>Số tiền thu</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    string DisableRow = "";
                    string Jquery = "";
                    string Status = "";

                    if (CurrentUnitLevel <= 1)
                        Jquery = "style='cursor:pointer;float:right' onclick='ApproveCheck(" + r["AutoKey"].ToString() + ")'";
                    else
                        Jquery = "style='float:right'";

                    if (r["IsApproved"].ToInt() == 1)
                    {
                        DisableRow = "class=\"disable\"";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        DisableRow = "'";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' " + DisableRow + ">");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>Quỹ</td>");
                    zSb.AppendLine("               <td>" + r["CapitalDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Note"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CapitalByName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + Status + r["CapitalFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='6' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='7'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            Literal_Table.Text = zSb.ToString();
        }
        [WebMethod]
        public static ItemReceipt GetCapital(int AutoKey)
        {
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.CapitalDate.ToDateString();
            zItem.Contents = zInfo.Note;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.FromEmployee.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static ItemResult CapitalStatus(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string SaveCapital(int AutoKey, string Employee1, string Employee2, string Date, string Contents, string Description, string Amount)
        {
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            zInfo.CapitalDate = Tools.ConvertToDate(Date);
            zInfo.FromEmployee = Employee1.ToInt();
            zInfo.ToEmployee = Employee2.ToInt();
            zInfo.Note = Contents.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.Description = Description.Trim();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }
    }
}