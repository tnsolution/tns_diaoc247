﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class CapitalOutput : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                LoadData();
            }
        }
        void LoadData()
        {
            DataTable zTable = Capital_Output_Data.List();
            int CurrentDepartment = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int CurrentUnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblCapital'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Ngày chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th>Người chi tiền</th>");
            zSb.AppendLine("        <th>Người nhận tiền</th>");
            zSb.AppendLine("        <th>Số tiền chi</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    string DisableRow = "";
                    string Jquery = "";
                    string Status = "";
                    if (CurrentUnitLevel <= 1)
                        Jquery = "style='cursor:pointer;float:right' onclick='ApproveCheck(" + r["AutoKey"].ToString() + ")'";
                    else
                        Jquery = "style='float:right'";
                    if (r["IsApproved"].ToInt() == 1)
                    {
                        DisableRow = "class=\"disable\"";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        DisableRow = "'";
                        Status = "<img width=20 " + Jquery + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' " + DisableRow + ">");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CapitalDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["FromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='noclick'>" + Status + r["ToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='6' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='7'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        [WebMethod]
        public static ItemReceipt GetCapital(int AutoKey)
        {
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.CapitalDate.ToString("dd/MM/yyyy");
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.ToEmployee.ToString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static ItemResult CapitalStatus(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();

            #region [auto create phieu thu]
            Receipt_Detail_Info zReceipt = new Receipt_Detail_Info(zInfo.AutoKey, true);
            zReceipt.ReceiptDate = zInfo.CapitalDate;
            zReceipt.ReceiptFrom = zInfo.ToEmployee;
            zReceipt.ReceiptBy = zInfo.FromEmployee;
            zReceipt.Contents = zInfo.Contents.Trim();
            zReceipt.Description = zInfo.Description.Trim();
            zReceipt.Amount = zInfo.Amount;
            zReceipt.CategoryKey = 1;
            zReceipt.DepartmentKey = zInfo.DepartmentKey;
            zReceipt.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zReceipt.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zReceipt.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zReceipt.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zReceipt.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            if (zReceipt.AutoKey != 0)
                zReceipt.Update();
            else
            {
                zReceipt.FromCapital = zInfo.AutoKey;
                zReceipt.Create();
            }
            #endregion

            if (zInfo.Message != string.Empty)
                zResult.Message = "Lỗi duyệt chi quỹ: " + zInfo.Message;
            if (zReceipt.Message != string.Empty)
                zReceipt.Message = "Lỗi tạo phiếu thu" + zReceipt.Message;
            return zResult;
        }
        [WebMethod]
        public static string SaveCapital(int AutoKey, int Department, int Employee1, string Date, string Contents, string Description, string Amount)
        {
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            zInfo.CapitalDate = Tools.ConvertToDate(Date);
            zInfo.FromEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = Department;
            zInfo.ToEmployee = Employee1;
            zInfo.Contents = Contents.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.Description = Description.Trim();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }
    }
}