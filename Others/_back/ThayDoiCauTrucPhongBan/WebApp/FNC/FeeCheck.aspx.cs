﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using Lib.SYS.Report;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.FNC
{
    public partial class FeeCheck : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["Category"] != null)
                    HID_Category.Value = Request["Category"];

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");

                CheckRole();
                LoadData(FromDate, ToDate);
            }
        }
        void LoadData(DateTime FromDate, DateTime ToDate)
        {
            Lit_Month.Text = "Tháng: " + ToDate.Month;

            DataTable zTable = new DataTable();
            StringBuilder zSb = new StringBuilder();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Month = ToDate.Month;
            int Year = ToDate.Year;
            string htmlTotal = "";
            string htmlTransfer = "";
            double TotalPay = 0, TotalReceipt = 0, PreviousMonth = 0, AmountRemain = 0;
            double CapitalPrevious = Capital_Output_Data.GetPreviousMoney(ToDate.Month, ToDate.Year),
                CapitalInput = Capital_Input_Data.SumMoney(ToDate.Month, ToDate.Year),
                CapitalOutput = Capital_Output_Data.SumMoney(ToDate.Month, ToDate.Year),
                CapitalRemain = CapitalInput - CapitalOutput;

            if (UnitLevel <= 1)
            {
                #region [Quỹ]
                Capital_CloseMonth_Info zCapitalCloseMonth = new Capital_CloseMonth_Info(FromDate.Month, FromDate.Year);
                if (UnitLevel <= 2 && zCapitalCloseMonth.CloseFinish == 0)
                    htmlTransfer = "<a class='orange' id='btncloseMonth' depart=0><i class='ace-icon fa fa-bolt bigger-110'></i>&nbsp;Chuyển tháng&nbsp;<i class='ace-icon fa fa-arrow-right icon-on-right'></i></a>";

                htmlTotal = @"
                    <table class='table table-striped table-bordered'>
                        <tr>
                            <td class='td1'>#</td>
                            <td>Quỹ CTY</td>
                            <td class='tdr' style='width:text-align:right'><b>Tổng</b></td>
                            <td class='giatien td15'>Tháng trước: " + CapitalPrevious.ToDoubleString() + @"</td>
                            <td class='giatien td15'>Nhập quỹ: " + CapitalInput.ToDoubleString() + @"</td>
                            <td class='giatien td15'>Chi quỹ: " + CapitalOutput.ToDoubleString() + @"</td>
                            <td class='giatien td15' amount='" + CapitalRemain + "'>Còn: " + CapitalRemain.ToDoubleString() + @"</td>
                            <td style='width: 10%' class='action-buttons center noprint'>" + htmlTransfer + @"</td>
                        </tr>
                    </table>";
                zSb.AppendLine("<div class='panel panel-default' id='0'>");
                zSb.AppendLine("    <div class='panel-heading'>");
                zSb.AppendLine("        <a style='padding:0px; !important' href='#faq0' data-parent='#faq-list-1' data-toggle='collapse' class='accordion-toggle collapsed' aria-expanded='false'>");
                zSb.AppendLine(htmlTotal);
                zSb.AppendLine("        </a>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("    <div class='panel-collapse collapse' id='faq0' aria-expanded='false' style='height: 0px;'>");
                zSb.AppendLine("        <div class='panel-body'>");
                zSb.AppendLine("            <div department='0' class='row'>");
                zSb.AppendLine("            </div>");
                zSb.AppendLine("        </div>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("</div>");
                LitTable_Capital.Text = zSb.ToString();
                #endregion
                htmlTotal = "";
            }

            #region [Tổng]
            switch (UnitLevel)
            {
                case 1:
                case 0:
                    zTable = Departments_Data.List(Month, Year, 0);
                    TotalReceipt = RptHelper.TotalReceipt(Month, Year, 0);
                    TotalPay = RptHelper.TotalPayment(Month, Year, 0);
                    PreviousMonth = RptHelper.PreviousMonth(Month, Year, 0);
                    AmountRemain = PreviousMonth + TotalReceipt - TotalPay;
                    break;

                default:
                    zTable = Departments_Data.List(Month, Year, DepartmentKey);
                    TotalReceipt = RptHelper.TotalReceipt(Month, Year, DepartmentKey);
                    TotalPay = RptHelper.TotalPayment(Month, Year, DepartmentKey);
                    PreviousMonth = RptHelper.PreviousMonth(Month, Year, DepartmentKey);
                    AmountRemain = PreviousMonth + TotalReceipt - TotalPay;
                    break;
            }
            #endregion

            #region [Nút kết chuyển]
            CloseMonth_Info zInfo;
            //    = new CloseMonth_Info(FromDate.Month, FromDate.Year, DepartmentKey, HID_Category.Value.ToInt());
            //if (UnitLevel <= 2 && zInfo.CloseFinish == 0)
            //    htmlTransfer = "<a class='orange' id='btncloseMonth' depart=" + DepartmentKey + "><i class='ace-icon fa fa-bolt bigger-110'></i>&nbsp;Chuyển tháng&nbsp;<i class='ace-icon fa fa-arrow-right icon-on-right'></i></a>";

            htmlTotal = @"
                    <table class='table table-striped table-bordered'>
                        <tr>
                            <td class='td1'>#</td>
                            <td></td>
                            <td class='tdr' style='width:text-align:right'><b>Tổng</b></td>
                            <td class='giatien td15'>Tháng trước: " + PreviousMonth.ToDoubleString() + @"</td>
                            <td class='giatien td15'>Chi: " + TotalReceipt.ToDoubleString() + @"</td>
                            <td class='giatien td15'>Thực chi: " + TotalPay.ToDoubleString() + @"</td>
                            <td class='giatien td15' amount='" + AmountRemain + "'>Còn: " + AmountRemain.ToDoubleString() + @"</td>
                            <td style='width: 10%' class='action-buttons center'></td>
                        </tr>
                    </table>";
            LitTotal.Text = htmlTotal;
            #endregion

            zSb = new StringBuilder();
            #region [Bảng]

            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    DataRow r = zTable.Rows[i];
                    htmlTransfer = "";
                    zInfo = new CloseMonth_Info(FromDate.Month, FromDate.Year, r["DepartmentKey"].ToInt(), HID_Category.Value.ToInt());
                    if (UnitLevel <= 2 && zInfo.CloseFinish == 0)
                        htmlTransfer = "<a class='orange' id='btncloseMonth' depart=" + r["DepartmentKey"].ToString() + "><i class='ace-icon fa fa-bolt bigger-110'></i>&nbsp;Chuyển tháng&nbsp;<i class='ace-icon fa fa-arrow-right icon-on-right'></i></a>";

                    AmountRemain = r["PreviousMonth"].ToDouble() + r["ReceiptMonth"].ToDouble() - r["PaymentMonth"].ToDouble();
                    htmlTotal = @"
                    <table class='table table-striped table-bordered'>
                        <tr>
                            <td class='td1'><i class='ace-icon fa fa-info bigger-130'></i></td>
                            <td>" + r["DepartmentName"].ToString() + @"</td>
                            <td class='tdr' style='width:text-align:right'><b>Tổng</b></td>
                            <td class='giatien td15'>Tháng trước: " + r["PreviousMonth"].ToDoubleString() + @"</td>
                            <td class='giatien td15'>Thu: " + r["ReceiptMonth"].ToDoubleString() + @"</td>
                            <td class='giatien td15'>Chi: " + r["PaymentMonth"].ToDoubleString() + @"</td>
                            <td class='giatien td15' amount='" + AmountRemain + "'>Còn: " + AmountRemain.ToDoubleString() + @"</td>
                            <td style='width: 10%' class='action-buttons center'>" + htmlTransfer + @"</td>
                        </tr>
                    </table>";

                    zSb.AppendLine("<div class='panel panel-default' id='" + r["DepartmentKey"].ToString() + "'>");
                    zSb.AppendLine("    <div class='panel-heading'>");
                    zSb.AppendLine("        <a style='padding:0px; !important' href='#faq" + No + "' data-parent='#faq-list-1' data-toggle='collapse' class='accordion-toggle collapsed' aria-expanded='false'>");
                    zSb.AppendLine(htmlTotal);
                    zSb.AppendLine("        </a>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("    <div class='panel-collapse collapse' id='faq" + No + "' aria-expanded='false' style='height: 0px;'>");
                    zSb.AppendLine("        <div class='panel-body'>");
                    zSb.AppendLine("            <div department='" + r["DepartmentKey"].ToString() + "' class='row'>");
                    zSb.AppendLine("            </div>");
                    zSb.AppendLine("        </div>");
                    zSb.AppendLine("    </div>");
                    zSb.AppendLine("</div>");
                    No++;
                }
            }

            LitTable_Data.Text = zSb.ToString();
            #endregion
        }
        [WebMethod]
        public static ItemReturn SaveCloseMonth(string Date, string Amount, int Department, int Category)
        {
            DateTime zDate = Tools.ConvertToDate(Date);
            ItemReturn zResult = new ItemReturn();
            if (Department == 0)
            {
                Capital_CloseMonth_Info zCapital = new Capital_CloseMonth_Info(zDate.Month, zDate.Year);
                if (zCapital.CloseFinish > 0)
                {
                    zResult.Result = "OK";
                    zResult.Message = "Tháng này đã kết chuyển rồi vui lòng chọn lại !.";
                }
                else
                {
                    zCapital.Amount = double.Parse(Amount);
                    zCapital.CloseDate = zDate;
                    zCapital.CloseFinish = 1;
                    zCapital.CloseBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    zCapital.Create();
                    if (zCapital.Message != string.Empty)
                    {
                        zResult.Result = "ERROR";
                        zResult.Message = zCapital.Message;
                    }
                    else
                    {
                        zResult.Result = "OK";
                        zResult.Message = "Đã chuyển thành công !.";
                    }
                }
            }
            else
            {
                CloseMonth_Info zInfo = new CloseMonth_Info(zDate.Month, zDate.Year, Department, Category);
                if (zInfo.CloseFinish > 0)
                {
                    zResult.Result = "OK";
                    zResult.Message = "Tháng này đã kết chuyển rồi vui lòng chọn lại !.";
                }
                else
                {
                    zInfo.Amount = double.Parse(Amount);
                    zInfo.CloseDate = zDate;
                    zInfo.CloseFinish = 1;
                    zInfo.CategoryKey = 1;
                    zInfo.DepartmentKey = Department;
                    zInfo.CloseBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    zInfo.Create();
                    if (zInfo.Message != string.Empty)
                    {
                        zResult.Result = "ERROR";
                        zResult.Message = zInfo.Message;
                    }
                    else
                    {
                        zResult.Result = "OK";
                        zResult.Message = "Đã chuyển thành công !.";
                    }
                }
            }

            return zResult;
        }
        [WebMethod]
        public static string GetMoreDetail(int Key, string FromDate, string ToDate)
        {
            DateTime zDate = Tools.ConvertToDate(ToDate);
            int Month = zDate.Month;
            int Year = zDate.Year;
            StringBuilder zSb = new StringBuilder();

            if (Key == 0)
            {
                DataTable zTable = Capital_Input_Data.List(Month, Year, 1);

                double TongThu = 0;
                double TongChi = 0;

                #region [THU]
                zSb.AppendLine("<div class='col-xs-6'>");
                zSb.AppendLine("<table class='table table-striped table-bordered' id='tblReceipt'>");
                zSb.AppendLine("   <thead class=''>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th class='td1'>STT</th>");
                zSb.AppendLine("        <th class='td15'>Ngày nhập</th>");
                zSb.AppendLine("        <th>Nội dung</th>");
                zSb.AppendLine("        <th class='td15'>Số tiền</th>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");
                if (zTable.Rows.Count > 0)
                {
                    int No = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                        zSb.AppendLine("               <td>" + (No++) + "</td>");
                        zSb.AppendLine("               <td>" + r["CapitalDate"].ToDateString() + "</td>");
                        zSb.AppendLine("               <td>" + r["Note"].ToString() + "</td>");
                        zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                        zSb.AppendLine("            </tr>");

                        TongThu += double.Parse(r["Amount"].ToString());
                    }
                }
                else { zSb.AppendLine("<tr><td></td><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
                zSb.AppendLine("</div>");
                #endregion

                #region [CHI]
                zTable = Capital_Output_Data.List(Month, Year, 1);
                zSb.AppendLine("<div class='col-xs-6'>");
                zSb.AppendLine("<table class='table table-striped table-bordered' id='tblReceipt'>");
                zSb.AppendLine("   <thead class=''>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th class='td1'>STT</th>");
                zSb.AppendLine("        <th class='td15'>Ngày chi</th>");
                zSb.AppendLine("        <th>Nội dung</th>");
                zSb.AppendLine("        <th class='td15'>Số tiền</th>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");
                if (zTable.Rows.Count > 0)
                {
                    int No = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                        zSb.AppendLine("               <td>" + (No++) + "</td>");
                        zSb.AppendLine("               <td>" + r["CapitalDate"].ToDateString() + "</td>");
                        zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                        zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                        zSb.AppendLine("            </tr>");

                        TongChi += double.Parse(r["Amount"].ToString());
                    }
                }
                else { zSb.AppendLine("<tr><td></td><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
                zSb.AppendLine("</div>");
                #endregion
            }
            else
            {
                double TongThu = 0;
                double TongChi = 0;

                #region [THU]
                DataTable zTable = Receipt_Detail_Data.List(Month, Year, Key, 1, 1);
                zSb.AppendLine("<div class='col-xs-6'>");
                zSb.AppendLine("<table class='table table-striped table-bordered' id='tblReceipt'>");
                zSb.AppendLine("   <thead class=''>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th class='td1'>STT</th>");
                zSb.AppendLine("        <th class='td15'>Ngày thu</th>");
                zSb.AppendLine("        <th>Nội dung</th>");
                zSb.AppendLine("        <th class='td15'>Số tiền</th>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");
                if (zTable.Rows.Count > 0)
                {
                    int No = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                        zSb.AppendLine("               <td>" + (No++) + "</td>");
                        zSb.AppendLine("               <td>" + r["ReceiptDate"].ToDateString() + "</td>");
                        zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                        zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                        zSb.AppendLine("            </tr>");

                        TongThu += double.Parse(r["Amount"].ToString());
                    }
                }
                else { zSb.AppendLine("<tr><td></td><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
                zSb.AppendLine("</div>");
                #endregion

                #region [CHI]
                zTable = Payment_Detail_Data.List(Month, Year, Key, 1, 1);
                zSb.AppendLine("<div class='col-xs-6'>");
                zSb.AppendLine("<table class='table table-striped table-bordered' id='tblReceipt'>");
                zSb.AppendLine("   <thead class=''>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th class='td1'>STT</th>");
                zSb.AppendLine("        <th class='td15'>Ngày chi</th>");
                zSb.AppendLine("        <th>Nội dung</th>");
                zSb.AppendLine("        <th class='td15'>Số tiền</th>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");
                if (zTable.Rows.Count > 0)
                {
                    int No = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                        zSb.AppendLine("               <td>" + (No++) + "</td>");
                        zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                        zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                        zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                        zSb.AppendLine("            </tr>");

                        TongChi += double.Parse(r["Amount"].ToString());
                    }
                }
                else { zSb.AppendLine("<tr><td></td><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
                zSb.AppendLine("</div>");
                #endregion
            }

            return zSb.ToString();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            Lit_Month.Text = ToDate.Month.ToString();

            LoadData(FromDate, ToDate);
        }

        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "ACC";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
        }
        #endregion

        void ExportExcel()
        {
            DateTime FromDate = Tools.ConvertToDate(HID_FromDate.Value);
            DateTime ToDate = Tools.ConvertToDate(HID_ToDate.Value);

            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Category = HID_Category.Value.ToInt();

            zTable = Departments_Data.List(FromDate.Month, FromDate.Year, 0);

            DataTable dtExcel = new DataTable();
            dtExcel.Columns.Add("Phòng", typeof(string));
            dtExcel.Columns.Add("Tháng trước", typeof(string));
            dtExcel.Columns.Add("Thu/ nhập", typeof(string));
            dtExcel.Columns.Add("Chi/ thực chi", typeof(string));
            dtExcel.Columns.Add("Còn lại", typeof(string));

            double tong1 = 0, tong2 = 0, tong3 = 0, tong4 = 0, conlai=0;
            double CapitalPrevious = Capital_Output_Data.GetPreviousMoney(ToDate.Month, ToDate.Year),
                CapitalInput = Capital_Input_Data.SumMoney(ToDate.Month, ToDate.Year),
                CapitalOutput = Capital_Output_Data.SumMoney(ToDate.Month, ToDate.Year),
                CapitalRemain = CapitalInput - CapitalOutput;

            DataRow rExcel = dtExcel.NewRow();
            rExcel[0] = "Quỹ CTY";
            rExcel[1] = CapitalPrevious.ToString("n0");
            rExcel[2] = CapitalInput.ToString("n0");
            rExcel[3] = CapitalOutput.ToString("n0");
            rExcel[4] = CapitalRemain.ToString("n0");
            dtExcel.Rows.Add(rExcel);

            foreach (DataRow r in zTable.Rows)
            {
                conlai = r["PreviousMonth"].ToDouble() + r["ReceiptMonth"].ToDouble() - r["PaymentMonth"].ToDouble();
                rExcel = dtExcel.NewRow();
                rExcel[0] = r["DepartmentName"].ToString();
                rExcel[1] = r["PreviousMonth"].ToDoubleString();
                rExcel[2] = r["PaymentMonth"].ToDoubleString();
                rExcel[3] = r["ReceiptMonth"].ToDoubleString();
                rExcel[4] = conlai;
                dtExcel.Rows.Add(rExcel);

                tong1 += r["PreviousMonth"].ToDouble();
                tong2 += r["PaymentMonth"].ToDouble();
                tong3 += r["ReceiptMonth"].ToDouble();
                tong4 += conlai;
            }

            DataRow rTong = dtExcel.NewRow();
            rTong[0] = "Tổng cộng";
            rTong[1] = tong1.ToString("n0");
            rTong[2] = tong2.ToString("n0");
            rTong[3] = tong3.ToString("n0");
            rTong[4] = tong4.ToString("n0");
            dtExcel.Rows.Add(rTong);
            DownloadExcel(dtExcel);
        }
        void DownloadExcel(DataTable dtExcel)
        {
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView GridView1 = new GridView();
                GridView1.AllowPaging = false;
                GridView1.DataSource = dtExcel;
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = Color.WhiteSmoke;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = Color.LemonChiffon;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Chi.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }
    }
}