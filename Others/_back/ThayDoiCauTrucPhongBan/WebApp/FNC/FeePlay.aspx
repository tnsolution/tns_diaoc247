﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="FeePlay.aspx.cs" Inherits="WebApp.FNC.FeePlay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="LitPageHead" runat="server"></asp:Literal>
                <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                <span class="pull-right action-buttons noprint">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <asp:Literal ID="LitTable1" runat="server"></asp:Literal>
            </div>
            <div class="col-xs-6">
                <asp:Literal ID="LitTable2" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mReceipt" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin thu</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label">Phòng</label>
                                <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Thu tiền từ</label>
                                <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ngày thu</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_ReceiptDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung</label>
                                <input type="text" class="form-control" id="txt_ReceiptContents" placeholder="Nhập text" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <input type="text" class="form-control" id="txt_ReceiptAmount" placeholder="Nhập số" moneyinput="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú</label>
                                <textarea class="form-control" id="txt_ReceiptDescription" placeholder="Nhập text" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input name="form-field-radio" value="1" type="radio" class="ace" />
                                        <span class="lbl">Đã thu</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input name="form-field-radio" value="0" type="radio" class="ace" />
                                        <span class="lbl">Chưa thu</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveReceipt" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mPayment" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông cần tin chi</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label">Phòng</label>
                                <asp:DropDownList ID="DDL_Department2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Người cần chi</label>
                                <asp:DropDownList ID="DDL_Employee2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ngày chi</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_PaymentDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung</label>
                                <input type="text" class="form-control" id="txt_PaymentContents" placeholder="Nhập text" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <input type="text" class="form-control" id="txt_PaymentAmount" placeholder="Nhập số" moneyinput="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú</label>
                                <textarea class="form-control" id="txt_PaymentDescription" placeholder="Nhập text" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSavePayment" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Category" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <asp:Literal ID="LitAllow" runat="server"></asp:Literal>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            $('#closeLeft').trigger("click");
        });
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });
            $("input[moneyinput]").number(true, 0);
            $("#thuquy").click(function () {
                $('[id$=HID_Category]').val(1);
                $("[id$=HID_AutoKey]").val(0);
                $('#mReceipt').modal('show');
            });
            $("#chiquy").click(function () {
                $('[id$=HID_Category]').val(1);
                $("[id$=HID_AutoKey]").val(0);
                $('#mPayment').modal('show');
            });

            $('#txt_ReceiptDate').val(Page.getCurrentDate());
            $('#txt_PaymentDate').val(Page.getCurrentDate());

            $('#tblReceipt').on("click", "tr td:not(:last-child)", function () {
                var trid = $(this).closest('tr').attr('id');
                $('#mReceipt').modal('show');
                $.ajax({
                    type: 'POST',
                    url: '/FNC/FeePlay.aspx/GetReceipt',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                        $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                        $('[id$=DDL_Employee1]').val(msg.d.ReceiptFrom).trigger("change");
                        $('[id$=txt_ReceiptDate]').val(msg.d.ReceiptDate);
                        $('[id$=txt_ReceiptContents]').val(msg.d.Contents);
                        $('[id$=txt_ReceiptAmount]').val(msg.d.Amount);
                        $('[id$=txt_ReceiptDescription]').val(msg.d.Description);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $('#tblPayment').on("click", "tr td:not(:last-child)", function () {
                var trid = $(this).closest('tr').attr('id');
                $('#mPayment').modal('show');
                $.ajax({
                    type: 'POST',
                    url: '/FNC/FeePlay.aspx/GetPayment',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                        $('[id$=DDL_Department2]').val(msg.d.DepartmentKey).trigger("change");
                        $('[id$=DDL_Employee2]').val(msg.d.PaymentTo).trigger("change");
                        $('[id$=txt_PaymentDate]').val(msg.d.ReceiptDate);
                        $('[id$=txt_PaymentContents]').val(msg.d.Contents);
                        $('[id$=txt_PaymentAmount]').val(msg.d.Amount);
                        $('[id$=txt_PaymentDescription]').val(msg.d.Description);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $('#btnSaveReceipt').on('click', function () {
                var status = 0;
                status = $("input[name='form-field-radio']:checked").val();
                $.ajax({
                    type: 'POST',
                    url: '/FNC/FeePlay.aspx/SaveReceipt',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Department': $('[id$=DDL_Department1]').val(),
                        'Employee': $('[id$=DDL_Employee1]').val(),
                        'Category': $('[id$=HID_Category]').val(),
                        'Date': $('[id$=txt_ReceiptDate]').val(),
                        'Contents': $('[id$=txt_ReceiptContents]').val(),
                        'Description': $('[id$=txt_ReceiptDescription]').val(),
                        'Amount': $('[id$=txt_ReceiptAmount]').val(),
                        'Status': status
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            location.href = '/FNC/FeePlay.aspx';
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $('#btnSavePayment').on('click', function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/FeePlay.aspx/SavePayment',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Department': $('[id$=DDL_Department2]').val(),
                        'Employee': $('[id$=DDL_Employee2]').val(),
                        'Category': $('[id$=HID_Category]').val(),
                        'Date': $('[id$=txt_PaymentDate]').val(),
                        'Contents': $('[id$=txt_PaymentContents]').val(),
                        'Description': $('[id$=txt_PaymentDescription]').val(),
                        'Amount': $('[id$=txt_PaymentAmount]').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            location.href = '/FNC/FeePlay.aspx';
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btncloseMonth").click(function () {
                var amount = $("span[amount]").attr("amount");
                if (confirm("Bạn đồng ý kết chuyển số tiền còn lại sang tháng sau ?")) {
                    $.ajax({
                        type: "POST",
                        url: "/FNC/FeePlay.aspx/SaveCloseMonth",
                        data: JSON.stringify({
                            'Date': $("[id$=HID_ToDate]").val(),
                            'Amount': amount,
                            'Category': 2
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (r) {
                            if (r.d.Result == 'ERROR') {
                                $('.se-pre-con').fadeOut('slow');
                                Page.showNotiMessageError("Lỗi xin liên hệ Admin", r.d.Message);
                            }
                            else {
                                $("#btncloseMonth").hide();
                                Page.showNotiMessageInfo("..", r.d.Message);
                                window.location = '/FNC/FeePlay.aspx';
                            }
                        },
                        complete: function () { },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
