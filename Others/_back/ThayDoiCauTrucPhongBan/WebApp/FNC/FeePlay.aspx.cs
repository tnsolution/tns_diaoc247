﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using Lib.SYS.Report;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

//2 là loại phí public quỹ ăn chơi.
namespace WebApp.FNC
{
    public partial class FeePlay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department2, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);

                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();

                CheckRoles();
                LoadData();

                SetReadedMessage();
            }
        }

        void LoadData()
        {
            StringBuilder zSb = new StringBuilder();
            #region [thu]
            double TotalThu = 0;
            DataTable zTable = Receipt_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, 0, 2);
            zSb.AppendLine("<table class='table table-hover table-striped' id='tblReceipt'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Người đóng quỹ</th>");
            zSb.AppendLine("        <th>Ngày thu</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền thu</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            #region quy thang truoc
            double thangtruoc = RptHelper.PreviousMonth_Play(DateTime.Now.Month, DateTime.Now.Year);
            int Month = 0;
            if ((DateTime.Now.Month - 1) == 0)
                Month = 12;
            else
                Month = DateTime.Now.Month - 1;
            zSb.AppendLine("            <tr id='0'>");
            zSb.AppendLine("               <td>1</td>");
            zSb.AppendLine("               <td>Tự động</td>");
            zSb.AppendLine("               <td>tháng: " + Month + "</td>");
            zSb.AppendLine("               <td>Quỹ tháng trước</td>");
            zSb.AppendLine("               <td class='giatien'>" + thangtruoc.ToString("n0") + "</td>");
            zSb.AppendLine("            </tr>");
            #endregion

            if (zTable.Rows.Count > 0)
            {
                int No = 2;
                foreach (DataRow r in zTable.Rows)
                {
                    string Status = "(Chưa thu)";
                    int IsApprove = r["IsApproved"].ToInt();
                    if (IsApprove == 1)
                    {
                        Status = "<img width='20' class='noprint' alt='' src='../template/custom-image/true.png'>&nbsp;";
                        TotalThu += r["Amount"].ToDouble();
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + Status + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='3' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + (thangtruoc + TotalThu).ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='4'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable1.Text = zSb.ToString();
            zSb = new StringBuilder();
            #region [chi]
            double TotalChi = 0;
            zTable = Payment_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, 0, 2);
            zSb.AppendLine("<table class='table table-hover table-striped' id='tblPayment'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Người chi quỹ</th>");
            zSb.AppendLine("        <th>Ngày chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền chi</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {

                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");

                    TotalChi += r["Amount"].ToDouble();
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='3' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + TotalChi.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='4'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable2.Text = zSb.ToString();

            LitPageHead.Text = "Quỹ tháng " + DateTime.Now.Month + " | " + (TotalThu - TotalChi + RptHelper.PreviousMonth_Play(DateTime.Now.Month, DateTime.Now.Year)).ToString("n0");
        }

        [WebMethod]
        public static string SaveReceipt(int AutoKey, string Department, string Employee, string Category, string Date, string Contents, string Description, string Amount, int Status)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            zInfo.ReceiptDate = Tools.ConvertToDateTime(Date);
            zInfo.ReceiptFrom = Employee.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.CategoryKey = 2;
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.IsApproved = Status;
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (AutoKey == 0)
                AutoMessage1(zInfo.AutoKey);

            if (zInfo.Message == string.Empty)
            {
                return "OK";
            }
            else
            {
                return "Lỗi không lưu được";
            }
        }
        [WebMethod]
        public static string SavePayment(int AutoKey, string Department, string Employee, string Category, string Date, string Contents, string Description, string Amount)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            zInfo.PaymentDate = Tools.ConvertToDateTime(Date);
            zInfo.PaymentTo = Employee.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.CategoryKey = 2;
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (AutoKey == 0)
                AutoMessage2(zInfo.AutoKey);

            if (zInfo.Message == string.Empty)
            {
                return "OK";
            }
            else
            {
                return "Lỗi không lưu được";
            }
        }

        static void AutoMessage2(int Key)
        {
            string SQL = "";
            DataTable zTable = Employees_Data.ListWorking();
            foreach (DataRow r in zTable.Rows)
            {
                SQL += @" INSERT INTO SYS_Message ([ObjectTable], [ObjectKey], [EmployeeKey], [Readed],[Description]) VALUES (N'FNC_Payment_Detail'," + Key + "," + r["EmployeeKey"].ToInt() + ",0,N'XapXinhChi')";
            }

            CustomInsert.Exe(SQL);
        }
        static void AutoMessage1(int Key)
        {
            string SQL = "";
            DataTable zTable = Employees_Data.ListWorking();
            foreach (DataRow r in zTable.Rows)
            {
                SQL += @" INSERT INTO SYS_Message ([ObjectTable], [ObjectKey], [EmployeeKey], [Readed],[Description]) VALUES (N'FNC_Receipt_Detail'," + Key + "," + r["EmployeeKey"].ToInt() + ",0,N'XapXinhThu')";
            }

            CustomInsert.Exe(SQL);
        }

        [WebMethod]
        public static ItemReturn SaveCloseMonth(string Date, string Amount, int Category)
        {
            DateTime zDate = Convert.ToDateTime(Date);
            ItemReturn zResult = new ItemReturn();
            CloseMonth_Info zInfo = new CloseMonth_Info(zDate.Month, zDate.Year, HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt(), Category);
            if (zInfo.CloseFinish > 0)
            {
                zResult.Result = "OK";
                zResult.Message = "Tháng này đã kết chuyển rồi vui lòng chọn lại !.";
            }
            else
            {
                zInfo.Amount = double.Parse(Amount);
                zInfo.CloseDate = zDate;
                zInfo.CloseFinish = 1;
                zInfo.CategoryKey = 2;
                zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
                zInfo.CloseBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.Create();
                if (zInfo.Message != string.Empty)
                {
                    zResult.Result = "ERROR";
                    zResult.Message = zInfo.Message;
                }
                else
                {
                    zResult.Result = "OK";
                    zResult.Message = "Đã chuyển thành công !.";
                }
            }

            return zResult;
        }

        [WebMethod]
        public static ItemReceipt GetReceipt(int AutoKey)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.ReceiptDate.ToDateString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.ReceiptFrom.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }

        [WebMethod]
        public static ItemPayment GetPayment(int AutoKey)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            ItemPayment zItem = new ItemPayment();
            zItem.VAT = zInfo.VAT.ToString();
            zItem.IsHasVAT = zInfo.IsHasVAT.ToString();
            zItem.Money = zInfo.Money.ToDoubleString();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.PaymentDate = zInfo.PaymentDate.ToDateString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.PaymentTo = zInfo.PaymentTo.ToString();
            if (zInfo.IsApproved == 1 || zInfo.IsApproved == -1 ||
                zInfo.IsApproved2 == 1 || zInfo.IsApproved2 == -1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Convert.ToDateTime(HID_FromDate.Value);
                ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }
            if (ViewTime == -1)
            {
                FromDate = Convert.ToDateTime(HID_FromDate.Value);
                ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }

            LoadData(FromDate, ToDate);
        }

        void LoadData(DateTime FromDate, DateTime ToDate)
        {
            StringBuilder zSb = new StringBuilder();
            #region thu
            double TotalThu = 0;
            DataTable zTable = Receipt_Detail_Data.List(FromDate.Month, FromDate.Year, 0, 2);
            zSb.AppendLine("<table class='table table-hover table-striped' id='tblReceipt'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Người đóng quỹ</th>");
            zSb.AppendLine("        <th>Ngày thu</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền thu</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            #region quy thang truoc
            double thangtruoc = RptHelper.PreviousMonth_Play(FromDate.Month, FromDate.Year);
            int Month = 0;
            if ((FromDate.Month - 1) == 0)
                Month = 12;
            else
                Month = FromDate.Month - 1;

            zSb.AppendLine("            <tr id='0'>");
            zSb.AppendLine("               <td>1</td>");
            zSb.AppendLine("               <td>Tự động</td>");
            zSb.AppendLine("               <td>tháng: " + Month + "</td>");
            zSb.AppendLine("               <td>Quỹ tháng trước</td>");
            zSb.AppendLine("               <td class='giatien'>" + thangtruoc.ToString("n0") + "</td>");
            zSb.AppendLine("            </tr>");
            #endregion

            if (zTable.Rows.Count > 0)
            {

                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    string Status = "(Chưa thu)";
                    int IsApprove = r["IsApproved"].ToInt();
                    if (IsApprove == 1)
                    {
                        Status = "<img width='20' class='noprint' alt='' src='../template/custom-image/true.png'>&nbsp;";
                        TotalThu += r["Amount"].ToDouble();
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ReceiptDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + Status + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='3' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + (thangtruoc + TotalThu).ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='4'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable1.Text = zSb.ToString();
            zSb = new StringBuilder();
            #region chi
            double TotalChi = 0;
            zTable = Payment_Detail_Data.List(FromDate.Month, FromDate.Year, 0, 2);
            zSb.AppendLine("<table class='table table-hover table-striped' id='tblPayment'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Người chi quỹ</th>");
            zSb.AppendLine("        <th>Ngày chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền chi</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {

                int No = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (No++) + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");

                    TotalChi += r["Amount"].ToDouble();
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='3' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + TotalChi.ToDoubleString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='4'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion
            LitTable2.Text = zSb.ToString();
            double total = TotalThu - TotalChi + RptHelper.PreviousMonth_Play(FromDate.Month, FromDate.Year);
            LitPageHead.Text = "Quỹ tháng " + FromDate.Month + " | <span amount='" + total + "'>" + total.ToString("n0") + "</span>";
        }

        void SetReadedMessage()
        {
            int Key = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string SQL = " UPDATE SYS_Message SET Readed = 1, ReadedDate = GETDATE() WHERE EmployeeKey= " + Key + " AND Readed = 0 AND (ObjectTable LIKE '%FNC_Receipt%' OR ObjectTable LIKE '%FNC_Payment%')";
            CustomInsert.Exe(SQL);
        }

        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        bool CheckRoles()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "ACC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');

            if (result[3].ToInt() == 1)
            {
                LitAllow.Text = @"<script>
$('#mReceipt :input').prop('disabled', false); 
$('#mPayment :input').prop('disabled', false);
$('#btnSaveReceipt').prop('disabled', false);
$('#btnSavePayment').prop('disabled', false);</script>";
                LitButton.Text = @"
                <span class='tools pull-right'><div class='btn-group'><button data-toggle='dropdown' class='btn btn-info btn-white dropdown-toggle' aria-expanded='false'><span class='ace-icon fa fa-caret-down icon-only'></span>&nbsp;Thêm mới</button>
                    <ul class='dropdown-menu dropdown-info dropdown-menu-right'>
						<li><a href = '#' class='' id='thuquy'><i class='ace-icon fa fa-plus blue'></i>&nbsp;Thu</a></li>
            			<li><a href = '#' class='' id='chiquy'><i class='ace-icon fa fa-plus blue'></i>&nbsp;Chi</a></li>
                        <li><a href = '#' class='' id='btncloseMonth'><i class='ace-icon fa fa-share orange'></i>&nbsp;Chuyển số dư</a></li></ul></div></span>";
                return true;
            }
            LitAllow.Text = @"<script>
$('#mReceipt :input').prop('disabled', true); 
$('#mPayment :input').prop('disabled', true);
$('#btnSaveReceipt').prop('disabled', true);
$('#btnSavePayment').prop('disabled', true);</script>";
            //mac dinh khoa
            return false;
        }
        #endregion
    }
}