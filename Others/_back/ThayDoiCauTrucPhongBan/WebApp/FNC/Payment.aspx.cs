﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.FNC
{
    public partial class Payment : System.Web.UI.Page
    {
        static bool _IsDelete = false;
        //static bool _IsApprove = false;
        static string _UserKey = "";
        static int _UnitLevel = -1;
        static int _Department = -1;
        static int _Employee = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRoles();

                if (Request["Category"] != null)
                    HID_Category.Value = Request["Category"];

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");

                LoadData();
            }
        }
        void LoadData(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            int Category = HID_Category.Value.ToInt();
            switch (_UnitLevel)
            {
                case 1:
                case 0:
                    zTable = Payment_Detail_Data.List(FromDate, ToDate, 0, Category);
                    break;

                default:
                    zTable = Payment_Detail_Data.List(FromDate, ToDate, _Department, Category);
                    break;
            }
            lbltime.Text = "Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy");
            Literal_Table.Text = HtmlData(zTable);
        }
        void LoadData()
        {
            DataTable zTable = new DataTable();
            int Category = HID_Category.Value.ToInt();
            switch (_UnitLevel)
            {
                case 1:
                case 0:
                    zTable = Payment_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, 0, Category);
                    break;

                default:
                    zTable = Payment_Detail_Data.List(DateTime.Now.Month, DateTime.Now.Year, _Department, Category);
                    break;
            }
            lbltime.Text = "Tháng: " + DateTime.Now.Month;
            Literal_Table.Text = HtmlData(zTable);
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            LoadData(FromDate, ToDate);
        }
        #region [Ajax]
        [WebMethod]
        public static ItemReturn Search(string FromDate, string ToDate, string Department, string Category)
        {
            DateTime dtFromDate = Tools.ConvertToDate(FromDate);
            DateTime dtToDate = Tools.ConvertToDate(ToDate);
            dtFromDate = new DateTime(dtFromDate.Year, dtFromDate.Month, dtFromDate.Day, 0, 0, 0);
            dtToDate = new DateTime(dtToDate.Year, dtToDate.Month, dtToDate.Day, 23, 59, 59);

            ItemReturn zResult = new ItemReturn();
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Payment_Detail_Data.List(dtFromDate, dtToDate, Department.ToInt(), Category.ToInt());
            if (zTable.Rows.Count > 0)
            {
                int No = 1;
                double Total1 = 0, Total2 = 0, Total3 = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    //"<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["AutoKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                    double MoneyVat = (r["Money"].ToDouble() * r["VAT"].ToInt()) / 100;
                    string DeleteButton = "";
                    string ClassGachNgang = "";
                    string BackColor = "";
                    #region [Kiểm tra tình trạng]
                    int Duyet1 = r["IsApproved"].ToInt();
                    int Duyet2 = r["IsApproved2"].ToInt();

                    string Jquery1 = "style='float:right' float:right' onclick='UnApproveCheck(" + r["AutoKey"].ToString() + ",1)'";
                    string Jquery2 = "style='float:right' float:right' onclick='UnApproveCheck(" + r["AutoKey"].ToString() + ",2)'";

                    string Status1 = "<img width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    string Status2 = "<img width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";

                    if (Duyet1 == 0)
                        Jquery1 = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",1)'";
                    else
                    {
                        if (Duyet1 == 1 && Duyet2 == 0)
                        {
                            Jquery2 = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",2)'";
                        }
                    }

                    switch (Duyet1)
                    {
                        case -1:
                            Status1 = "<img width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status1 = "<img width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;
                        case 1:
                            Status1 = "<img width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["ApprovedName"].ToString();
                            break;
                    }

                    switch (Duyet2)
                    {
                        case -1:
                            Status2 = "<img width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["Approved2Name"].ToString();
                            break;
                        case 0:
                            Status2 = "<img width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;
                        case 1:
                            Status2 = "<img width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["Approved2Name"].ToString();
                            break;
                    }

                    if (Duyet1 == -1 || Duyet2 == -1)
                        ClassGachNgang = "gachngang";
                    if (Duyet1 == 0 || Duyet2 == 0)
                        BackColor = "style=background-color:#c5d0dc";
                    if (Duyet1 == 0 && Duyet2 == 0)
                        DeleteButton = "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteOutput(" + r["AutoKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                    #endregion

                    zSb.AppendLine("            <tr " + BackColor + " id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td class='noclick'>" + (No++) + "</td>");
                    zSb.AppendLine("               <td class='noclick'><div class='action-buttons pull-right'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["PaymentToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + MoneyVat.ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + ClassGachNgang + "'>" + r["Amount"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='td5'>" + DeleteButton + "</td>");
                    zSb.AppendLine("            </tr>");
                    zSb.AppendLine("            <tr class='detail-row'>");
                    zSb.AppendLine("                <td colspan='9' class='noclick'>");
                    zSb.AppendLine("                    <div class='table-detail'>");
                    zSb.AppendLine("                        <table class='table table-bordered'>");
                    zSb.AppendLine("                            <tr>");
                    zSb.AppendLine("                                <td class='td20 noclick'>Xác nhận: " + Status1 + "</td>");
                    zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote"].ToString() + "</td>");
                    zSb.AppendLine("                                <td class='td20 noclick'>Duyệt: " + Status2 + "</td>");
                    zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote2"].ToString() + "</td>");
                    zSb.AppendLine("                                <td class='noclick'>Ghi chú: " + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("                            </tr>");
                    zSb.AppendLine("                        </table>");
                    zSb.AppendLine("                    </div>");
                    zSb.AppendLine("                </td>");
                    zSb.AppendLine("            </tr>");

                    if (Duyet2 == 1)
                    {
                        Total1 += r["Money"].ToDouble();
                        Total2 += MoneyVat;
                        Total3 += r["Amount"].ToDouble();
                    }
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='4' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total1.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total2.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total3.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("            </tr>");
            }

            zResult.Result2 = zSb.ToString();
            return zResult;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AutoKey"></param>
        /// <param name="Approve"></param>
        /// <param name="Note"></param>
        /// <param name="Level">1 -duyệt, 2- là xác nhận</param>
        /// <returns></returns>
        [WebMethod]
        public static ItemReturn PaymentStatus(int AutoKey, int Approve, string Note, int Level)
        {
            /*
             duyệt và gỡ duyệt theo lần 1 xác nhan, 2 duyet
             gỡ duyệt thi không luu ghi chú
             truong phòng chỉ xác nhận chi
             chỉ admin, gd mới duyệt
             */

            ItemReturn zResult = new ItemReturn();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel > 2)
            {
                zResult.Message = "Bạn chưa có quyền duyệt thông tin này xin vui lòng liên hệ quản lý !.";
                return zResult;
            }

            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            if (Level == 1)
            {
                zInfo.IsApproved = Approve;
                if (Approve != 0)
                    zInfo.ApproveNote = Note.Trim() + "\r\n";
                zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SetStatus();
            }
            else
            {
                if (UnitLevel <= 1)
                {
                    zInfo.IsApproved2 = Approve;
                    if (Approve != 0)
                        zInfo.ApproveNote2 = Note.Trim() + "\r\n";
                    zInfo.Approved2By = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    zInfo.SetStatus2();
                }
                else
                {
                    zResult.Message = "Bạn chưa có quyền xác nhận thông tin này xin vui lòng liên hệ quản lý !.";
                    return zResult;
                }
            }
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemPayment GetPayment(int AutoKey)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            ItemPayment zItem = new ItemPayment();
            zItem.VAT = zInfo.VAT.ToString();
            zItem.IsHasVAT = zInfo.IsHasVAT.ToString();
            zItem.Money = zInfo.Money.ToDoubleString();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.PaymentDate = zInfo.PaymentDate.ToDateString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.PaymentTo = zInfo.PaymentTo.ToString();
            if (zInfo.IsApproved == 1 || zInfo.IsApproved == -1 ||
                zInfo.IsApproved2 == 1 || zInfo.IsApproved2 == -1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static string SavePayment(int AutoKey, string Department, string Employee, string Category,
            string Date, string Contents, string Description, string Money, string VAT, string Amount, int Check)
        {
            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            zInfo.PaymentDate = Tools.ConvertToDate(Date);
            zInfo.PaymentTo = Employee.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.VAT = VAT.ToInt();
            zInfo.Amount = double.Parse(Amount);
            zInfo.Money = double.Parse(Money);
            zInfo.IsHasVAT = Check;
            zInfo.CategoryKey = Category.ToInt();
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (AutoKey != 0)
                SendMail(Contents + " " + Description);
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }

        #endregion
        static string HtmlData(DataTable TableIn)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblPayment'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Ngày chi</th>");
            zSb.AppendLine("        <th>Người chi</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Số tiền</th>");
            zSb.AppendLine("        <th>VAT</th>");
            zSb.AppendLine("        <th>Thành tiền</th>");

            zSb.AppendLine("        <th isdelete class='td5 noprint'>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (TableIn.Rows.Count > 0)
            {
                int No = 1;
                double Total1 = 0, Total2 = 0, Total3 = 0;
                foreach (DataRow r in TableIn.Rows)
                {
                    double zMoneyVat = (r["Money"].ToDouble() * r["VAT"].ToInt()) / 100;
                    string zDeleteButton = "";
                    string zClassGachNgang = "";
                    string zBackColor = "";
                    string zDisableClick = "";

                    #region [Kiểm tra tình trạng]
                    int Duyet1 = r["IsApproved"].ToInt();
                    int Duyet2 = r["IsApproved2"].ToInt();

                    string Jquery1 = "style='float:right' float:right' onclick='UnApproveCheck(" + r["AutoKey"].ToString() + ",1)'";
                    string Jquery2 = "style='float:right' float:right' onclick='UnApproveCheck(" + r["AutoKey"].ToString() + ",2)'";

                    string Status1 = "<img class='noprint' width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    string Status2 = "<img class='noprint' width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";

                    if (Duyet1 == 0)
                        Jquery1 = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",1)'";
                    else
                    {
                        if (Duyet1 == 1 && Duyet2 == 0)
                        {
                            Jquery2 = "style='cursor:pointer; float:right' onclick='PopupCheck(" + r["AutoKey"].ToString() + ",2)'";
                        }
                    }

                    switch (Duyet1)
                    {
                        case -1:
                            Status1 = "<img class='noprint' width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status1 = "<img class='noprint' width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;
                        case 1:
                            Status1 = "<img class='noprint' width=20 " + Jquery1 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["ApprovedName"].ToString();
                            break;
                    }

                    switch (Duyet2)
                    {
                        case -1:
                            Status2 = "<img class='noprint' width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/cross.png' />" + r["Approved2Name"].ToString();
                            break;
                        case 0:
                            Status2 = "<img class='noprint' width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                            break;
                        case 1:
                            Status2 = "<img class='noprint' width=20 " + Jquery2 + " id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved2"].ToInt() + "' alt='' src='../template/custom-image/true.png' />" + r["Approved2Name"].ToString();
                            break;
                    }

                    if (Duyet1 == -1 || Duyet2 == -1)
                        zClassGachNgang = "gachngang";
                    if (Duyet1 == 0 || Duyet2 == 0)
                        zBackColor = "style=background-color:#c5d0dc";
                    if (Duyet1 == 0 && Duyet2 == 0)
                        zDeleteButton = "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteOutput(" + r["AutoKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                    if (Duyet1 == 1 && Duyet2 == 1)
                        zDisableClick = "noclick";
                    #endregion

                    zSb.AppendLine("            <tr " + zBackColor + " id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td class='noclick'>" + (No++) + "</td>");
                    zSb.AppendLine("               <td class='noclick'><div class='action-buttons pull-right noprint'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["PaymentDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["PaymentToName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + zClassGachNgang + "'>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + zClassGachNgang + "'>" + zMoneyVat.ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td class='giatien td10 " + zClassGachNgang + "'>" + r["Amount"].ToDoubleString() + "</td>");

                    if (_IsDelete)
                        zSb.AppendLine("               <td class='td5 noprint'>" + zDeleteButton + "</td>");

                    zSb.AppendLine("            </tr>");

                    #region chi tiet row
                    zSb.AppendLine("            <tr class='detail-row'>");
                    zSb.AppendLine("                <td colspan='9' class='noclick'>");
                    zSb.AppendLine("                    <div class='table-detail'>");
                    zSb.AppendLine("                        <table class='table table-bordered'>");
                    zSb.AppendLine("                            <tr>");
                    zSb.AppendLine("                                <td class='td20 noclick'>Xác nhận: " + Status1 + "</td>");

                    if (r["ApproveNote"].ToString().Trim() != string.Empty)
                        zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote"].ToString() + "</td>");

                    zSb.AppendLine("                                <td class='td20 noclick'>Duyệt: " + Status2 + "</td>");

                    if (r["ApproveNote2"].ToString().Trim() != string.Empty)
                        zSb.AppendLine("                                <td class='noclick'>Lý do: " + r["ApproveNote2"].ToString() + "</td>");

                    zSb.AppendLine("                                <td class='noclick'>Ghi chú: " + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("                            </tr>");
                    zSb.AppendLine("                        </table>");
                    zSb.AppendLine("                    </div>");
                    zSb.AppendLine("                </td>");
                    zSb.AppendLine("            </tr>");
                    #endregion

                    Total1 += r["Money"].ToDouble();
                    Total2 += zMoneyVat;
                    Total3 += r["Amount"].ToDouble();
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='4' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total1.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total2.ToDoubleString() + "</td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total3.ToDoubleString() + "</td>");
                if (_IsDelete)
                    zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='8'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }
        static void AutoMessage(int Key)
        {
            string SQL = "";
            DataTable zTable = Employees_Data.ListWorking();
            foreach (DataRow r in zTable.Rows)
            {
                SQL += @" INSERT INTO SYS_Message [ObjectTable], [ObjectKey], [EmployeeKey], [Readed],[Description]) VALUES (N'FNC_Payment_Detail'," + Key + "," + r["EmployeeKey"].ToInt() + ",0,N'XapXinhChi')";
            }

            CustomInsert.Exe(SQL);
        }
        #region [Send Mail]
        [WebMethod]
        public static void SendMail(string noidung)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            smtpClient.Credentials = new System.Net.NetworkCredential("247autocrm@gmail.com", "auto247crm");
            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.Body = "Có thông tin chi $$ đã chỉnh sửa" + noidung + " !. Vui lòng không phản hồi mail này";
            mail.From = new MailAddress("247autocrm@gmail.com", "CRM.DIAOC247.VN");
            mail.To.Add(new MailAddress("troly.diaoc247@gmail.com"));

            smtpClient.Send(mail);
        }
        #endregion
        #region [Excel]
        void ExportExcel()
        {
            DateTime FromDate = Tools.ConvertToDate(HID_FromDate.Value);
            DateTime ToDate = Tools.ConvertToDate(HID_ToDate.Value);

            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Category = HID_Category.Value.ToInt();
            switch (UnitLevel)
            {
                case 1:
                case 0:
                    Department = HID_Department.Value.ToInt();
                    zTable = Payment_Detail_Data.List(FromDate, ToDate, Department, Category);
                    break;

                default:
                    zTable = Payment_Detail_Data.List(FromDate, ToDate, Department, Category);
                    break;
            }

            DataTable dtExcel = new DataTable();
            dtExcel.Columns.Add("Phòng", typeof(string));
            dtExcel.Columns.Add("Ngày chi", typeof(string));
            dtExcel.Columns.Add("Người chi", typeof(string));
            dtExcel.Columns.Add("Nội dung", typeof(string));
            dtExcel.Columns.Add("Số tiền", typeof(string));
            dtExcel.Columns.Add("VAT", typeof(string));
            dtExcel.Columns.Add("Thành tiền", typeof(string));

            double tong1 = 0, tong2 = 0, tong3 = 0;
            foreach (DataRow r in zTable.Rows)
            {
                string VAT = ((r["Money"].ToDouble() * r["VAT"].ToInt()) / 100).ToDoubleString();

                DataRow rExcel = dtExcel.NewRow();
                rExcel[0] = r["Department"].ToString();
                rExcel[1] = r["PaymentDate"].ToDateString();
                rExcel[2] = r["PaymentToName"].ToString();
                rExcel[3] = r["Contents"].ToString();
                rExcel[4] = r["Money"].ToDoubleString();
                rExcel[5] = VAT;
                rExcel[6] = r["Amount"].ToDoubleString();
                dtExcel.Rows.Add(rExcel);

                tong1 += r["Money"].ToDouble();
                tong2 += VAT.ToDouble();
                tong3 += r["Amount"].ToDouble();
            }
            DataRow rTong = dtExcel.NewRow();
            rTong[0] = "Tổng cộng";
            rTong[4] = tong1.ToString("n0");
            rTong[5] = tong2.ToString("n0");
            rTong[6] = tong3.ToString("n0");
            dtExcel.Rows.Add(rTong);
            DownloadExcel(dtExcel);
        }

        void DownloadExcel(DataTable dtExcel)
        {
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView GridView1 = new GridView();
                GridView1.AllowPaging = false;
                GridView1.DataSource = dtExcel;
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = Color.WhiteSmoke;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = Color.LemonChiffon;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Chi.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }
        #endregion
        #region [CheckRoles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void CheckRoles()
        {
            _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();

            string zRolePage = "ACC";
            string[] zPermitsion = User_Data.RolesCheck(_UserKey, zRolePage).Split(',');
            string zScript = "<script>";

            if (_UnitLevel <= 1)
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                DDL_Department.Visible = true;
            }
            else
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey=" + _Department + " ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments DepartmentKey=" + _Department + "ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE DepartmentKey=" + _Department + " AND IsWorking = 2 ORDER BY LastName", false);
                DDL_Department.Visible = false;
                DDL_Department1.SelectedValue = _Department.ToString();
                DDL_Department.SelectedValue = _Department.ToString();
            }

            if (zPermitsion[2].ToInt() == 1)
            {
                //_IsApprove = true;
                #region [XacNhan]
                zScript += @"
            function PopupCheck(id, lvl) {
                $('[id$= HID_AutoKey]').val(id);
                $('[id$=HID_Level]').val(lvl);
                if (lvl == 2){
                    $('#pophead').text('Bạn có muốn xác nhận thông tin !.');
                    $('#btnOK').text('Xác nhận');
                }
                $('#mApprove').modal('show');
            }
            function ApproveCheck(approve) {
                if (confirm('Bạn có chắc duyệt thông tin !.Sau khi duyệt thông tin sẽ không thay đổi được')) {
                    $.ajax({
                        type: 'POST',
                        url: '/FNC/Payment.aspx/PaymentStatus',
                        data: JSON.stringify({
                        'AutoKey': $('[id$=HID_AutoKey]').val(),
                        'Approve': approve,
                        'Note': $('#txt_Note').val(),
                        'Level': $('[id$=HID_Level]').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function() {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function(msg) {
                        if (msg.d.Message != ''){
                            Page.showNotiMessageError('Thông báo !', msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else{
                            location.reload();
                        }
                    },
                    complete: function() {},
                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }";

                #endregion
            }
            if (zPermitsion[3].ToInt() == 1)
            {
                _IsDelete = true;
                #region [xóa, gỡ duyệt]
                zScript += @"
        function UnApproveCheck(key, lvl) {
            if (confirm('Bạn có chắc gỡ duyệt thông tin !.')) {
            $.ajax({
            type: 'POST',
            url: '/FNC/Payment.aspx/PaymentStatus',
            data: JSON.stringify({
                'AutoKey': key,
                'Approve': 0,
                'Note': $('#txt_Note').val(),
                'Level': lvl,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function() {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function(msg) {
                if (msg.d.Message != '') {
                    Page.showNotiMessageError('Thông báo !', msg.d.Message);
                    $('.se-pre-con').fadeOut('slow'); }
                else{                    
                    location.reload();                                             
                }
            },
            complete: function() {},
            error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
            }
        }

        function deleteOutput(id) {
        if (confirm('Bạn có chắc xóa thông tin')) {
        $.ajax({
                type: 'POST',
            url: '/Ajax.aspx/DeletePayment',
            data: JSON.stringify({
                        'AutoKey': id,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function() {
                $('.se-pre-con').fadeIn('slow');
                    },
            success: function(r) {
                        if (r.d == 'OK')
                            location.reload();
                    },
            complete: function() {

                    },
            error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }";
                #endregion
            }

            LitScript.Text = zScript + "</script>";
        }
        #endregion
    }
}