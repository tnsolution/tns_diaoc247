﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TicketCreate.aspx.cs" Inherits="WebApp.FNC.TicketCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" cssclass="form-control">
    <title></title>
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <script src="/template/ace-master/assets/js/jquery-2.1.4.min.js"></script>
    <script src="/template/ace-master/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="/template/ace-master/assets/js/bootstrap.min.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <style>
        .td5 {
            width: 5%;
        }

        .td10 {
            width: 10%;
        }

        .td15 {
            max-width: 15%;
        }    

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            border-top: 0px !important;
            vertical-align: middle !important;
            padding: 4px !important;
        }

        .hr:not(.hr-dotted) {
            background-color: black !important;
        }

        .hr {
            margin: 0px !important;
            height: 1px !important;
        }

        .table {
            margin-bottom: 0px;
            border: 1px solid black !important;
        }

      
    </style>
</head>
<body>
    <form id="form1" runat="server" cssclass="form-control">
        <div class="page-content">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
        <asp:HiddenField ID="HID_TicketKey" runat="server" />
    </form>
</body>
</html>
