﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketList.aspx.cs" Inherits="WebApp.FNC.TicketList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Các phiếu bán hàng
                <span class="tools pull-right">
                    <a href="TicketEdit2.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới (Chuyển nhượng)
                    </a>
                    <a href="TicketEdit1.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới (Cá nhân thuê)
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12" id="divtable">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ngày tháng năm</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtFromDate" placeholder="Từ " />
                        </div>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtToDate" placeholder="Đến " />
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="txtName" placeholder="Mã căn" />
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Project" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Dự án--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="quanly" style="display: none;">
                        <label>Phòng, nhân viên</label>
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Employee" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div style="display: none;">
        <asp:Label ID="CookEmployee" runat="server" Text=""></asp:Label>
        <asp:Label ID="CookDepartment" runat="server" Text=""></asp:Label>
        <asp:Label ID="CookUnitLevel" runat="server" Text=""></asp:Label>
        <asp:Label ID="CookUserKey" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txtFromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txtToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        jQuery(function ($) {
            var UnitLevel = getUnitLevel();
            if (UnitLevel >= 3) {
                $("#quanly").hide();
            }
            else {
                $("#quanly").show();
            }

            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            Date.prototype.ddmmyyyy = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
            };

            $("[id$=txtFromDate]").val(firstDay.ddmmyyyy());
            $("[id$=txtToDate]").val(lastDay.ddmmyyyy());

            $('.modal.aside').ace_aside();
            $(".select2").select2({ width: "100%" });
            $("[id$=DDL_Department]").change(function () {
                var Key = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        DepartmentKey: $(this).val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee]");
                        District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                        $.each(msg.d, function () {
                            District.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $("#tableTicketTrade tbody tr").on("click", "td:last-child", function (e) {
                e.stopPropagation();
            });
            $('#tableTicketTrade tbody tr').on('click', function () {
                var trid = $(this).closest('tr').attr('id');
                var category = $(this).closest('tr').attr('category');
                if (category == 1)
                    window.location = 'TicketEdit1.aspx?ID=' + trid;
                else
                    window.location = 'TicketEdit2.aspx?ID=' + trid;
            });

            $("#btnInitTicket").click(function () {
                if (!validate()) {
                    return false;
                }

                var department = $("[id$=DDL_Department]").val();
                var employee = $("[id$=DDL_Employee]").val();
                var category = $("[id$=DDL_Category]").val();
                var DateCreate = $("[id$=txt_DateSign]").val();

                $.ajax({
                    type: 'POST',
                    url: '/FNC/TicketEdit.aspx/InitTicket',
                    data: JSON.stringify({
                        "Category": category,
                        "DateCreate": DateCreate,
                        "Employee": employee,
                        "Department": department,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            window.location = "/FNC/TicketEdit.aspx?ID=" + r.d.Result;
                        else
                            Page.showPopupMessage("Lỗi khởi tạo thông tin", r.d.Message);
                    },
                    error: function () {
                        alert('Lỗi khởi tạo dữ liệu !');
                        $('.se-pre-con').fadeOut('slow');
                    },
                    complete: function () {

                    }
                });
            });
            $("#btnSearch").click(function () {
                var employee = $("[id$=DDL_Employee]").val();
                var department = $("[id$=DDL_Department]").val();
                var fromdate = $("[id$=txtFromDate]").val();
                var todate = $("[id$=txtToDate]").val();
                var assetid = $("[id$=txtName]").val();
                var project = $("[id$=DDL_Project]").val();
                if (department == null)
                    department = getDepartmentKey();
                $.ajax({
                    type: "POST",
                    url: "/FNC/TicketList.aspx/Search",
                    data: JSON.stringify({
                        "Department": department,
                        "Employee": employee,
                        "FromDate": fromdate,
                        "ToDate": todate,
                        "Project": project,
                        "AssetID": assetid,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#tableTicketTrade tbody').empty();
                        $('#tableTicketTrade tbody').append($(msg.d));
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
        function deleteInput(id) {
            if (confirm("Bạn có chắc xóa ?")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TicketList.aspx/Delete',
                    data: JSON.stringify({
                        'Key': id,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d == '')
                            $("#tableTicketTrade tr[id='" + id + "']").remove();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
