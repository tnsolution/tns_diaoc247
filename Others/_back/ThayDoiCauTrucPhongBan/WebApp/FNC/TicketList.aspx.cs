﻿using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TicketList : System.Web.UI.Page
    {
        //static int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
        //static int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
        //static int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
        //static string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            CookEmployee.Text = _Employee.ToString();
            CookDepartment.Text = _Department.ToString();
            CookUnitLevel.Text = _UnitLevel.ToString();
            CookUserKey.Text = _UserKey.ToString();

            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(_UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey =" + _Department + " ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey =" + _Department + " ORDER BY LastName", false);
                    DDL_Department.SelectedValue = _Department.ToString();
                    DDL_Employee.SelectedValue = _Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey =" + _Department + " ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + _Employee + "ORDER BY LastName", false);
                    DDL_Employee.SelectedValue = _Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                //Tools.DropDown_DDL(DDL_Category, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 29", false);
                LoadData();
            }
        }
        void LoadData()
        {
            int _UnitLevel = CookUnitLevel.Text.ToInt();
            int _Department = CookDepartment.Text.ToInt();
            int _Employee = CookEmployee.Text.ToInt();

            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = new DataTable();
            if (Session["SearchTicket"] != null)
            {
                Item_Trade_Search zSession = (Item_Trade_Search)Session["SearchTicket"];
                zTable = Ticket_Data.List(zSession.Employee, zSession.Department, zSession.AssetID, zSession.Project, Tools.ConvertToDate(zSession.FromDate), Tools.ConvertToDate(zSession.ToDate));
            }
            else
            {
                switch (_UnitLevel)
                {
                    case 0:
                    case 1:
                        zTable = Ticket_Data.List(0, 0, string.Empty, 0, startDate, endDate);
                        break;

                    case 2:
                        zTable = Ticket_Data.List(0, _Department, string.Empty, 0, startDate, endDate);
                        break;

                    default:
                        zTable = Ticket_Data.List(_Employee, _Department, string.Empty, 0, startDate, endDate);
                        break;
                }
            }

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tableTicketTrade'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>Loại</th>");
            zSb.AppendLine("        <th>Ngày cọc</th>");
            zSb.AppendLine("        <th>Giá trị</th>");
            if (_Permitsion[3].ToInt() == 1)
                zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TicketKey"].ToString() + "' category=" + r["Category"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["NgayCoc"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["GiaTriHopDong"].ToDoubleString() + "</td>");
                    if (_Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TicketKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else { zSb.AppendLine("<tr><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static string Delete(int Key)
        {
            Ticket_Info zInfo = new Ticket_Info();
            zInfo.TicketKey = Key;
            zInfo.Delete();
            return zInfo.Message;
        }
        [WebMethod]
        public static string Search(int Department, int Employee, string FromDate, string ToDate, int Project, string AssetID)
        {
            DataTable Table = Ticket_Data.List(Employee, Department, AssetID, Project, Tools.ConvertToDate(FromDate), Tools.ConvertToDate(ToDate));
            Item_Trade_Search ISearch = new Item_Trade_Search();
            ISearch.Department = Department;
            ISearch.Employee = Employee;
            ISearch.FromDate = FromDate;
            ISearch.ToDate = ToDate;
            ISearch.Project = Project;
            ISearch.AssetID = AssetID;
            HttpContext.Current.Session.Add("SearchTicket", ISearch);
            return ViewHtml(Table);
        }
        static string ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();
            if (Table.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TicketKey"].ToString() + "' category=" + r["Category"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["NgayCoc"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["GiaTriHopDong"].ToDoubleString() + "</td>");
                    if (_Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TicketKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else { zSb.AppendLine("<tr><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
            return zSb.ToString();
        }
    }
}