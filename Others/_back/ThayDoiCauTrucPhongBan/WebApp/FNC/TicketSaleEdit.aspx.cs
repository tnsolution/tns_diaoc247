﻿using FindAndReplace;
using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.IO;
using System.Web;

namespace WebApp.FNC
{
    public partial class TicketSaleEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDLNhanVien, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDLTruongPhong, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDLPhong, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDLDuAn, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDLLoaiCanHo, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);

                lblNgayLap.Text = DateTime.Now.ToString("dd/MM/yyyy");
                LitFile.Text = "Sau khi điền đủ thông tin, chọn file mẫu cần xử lý, click tải file để tải file word đã xử lý !.";

                LoadInfo();
            }
        }

        protected void btnWord_Click(object sender, EventArgs e)
        {
            SaveInfo();
            GenerateAll();
        }

        void GenerateAll()
        {
            Project_Info zProject = new Project_Info(DDLDuAn.SelectedValue.ToInt());

            string zList = "...";
            string[] row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                foreach (string item in row)
                {
                    string data = "";
                    string[] col = item.Split(':');
                    double money = col[2].Trim().ToDouble();
                    data = col[0] + " ngày " + col[1] + " số tiền " + money.ToString("n0") + " bằng chữ " + Utils.NumberToWordsVN(money) + col[3];
                    zList += data;
                }
            }

            #region [xử lý download]
            string FileName = DDLFile.SelectedValue; //"004MauThoaThuanCanHo.docx"; //DDLFile.SelectedValue;
            string FileNameOutput = FileName.Split('.')[0] + "_Output.docx";
            string PathIn = @"\Upload\File\HD\" + FileName;
            string PathOut = @"\Upload\File\HD\" + FileNameOutput;

            string InputFile = HttpContext.Current.Server.MapPath(@"\") + PathIn;
            string OutputFile = HttpContext.Current.Server.MapPath(@"\") + PathOut;

            File.Copy(InputFile, OutputFile, true);
            string phibangchu = Utils.ConvertDecimalToStringVN(txtPhiDichVuC.Text.ToDouble());

            var stream = new MemoryStream();
            using (var fileStream = File.OpenRead(OutputFile))
            {
                fileStream.CopyTo(stream);

                using (var flatDocument = new FlatDocument(stream))
                {
                    #region [tieu de]
                    flatDocument.FindAndReplace("#txt_ngaycoc", Tools.ConvertToDate(txtNgayCoc.Text).Day.ToString());
                    flatDocument.FindAndReplace("#txt_thangcoc ", Tools.ConvertToDate(txtNgayCoc.Text).Month.ToString());
                    flatDocument.FindAndReplace("#txt_namcoc", Tools.ConvertToDate(txtNgayCoc.Text).Year.ToString());
                    flatDocument.FindAndReplace("#txt_duan", DDLDuAn.SelectedItem.Text);
                    flatDocument.FindAndReplace("#txt_diachiduan", zProject.Address);
                    #endregion

                    #region [--A--]
                    flatDocument.FindAndReplace("#txt_hotenA", txtHotenA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_cmndA", txtCMNDA.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycapA", txtNgayCapA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_noicapA", txtNoiCapA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachiA", txtDiaChiThuongTruA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dienthoaiA", txtDienThoaiA.Text.Trim());
                    #endregion

                    #region [--B--]
                    flatDocument.FindAndReplace("#txt_hotenB", txtHotenB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_cmndB", txtCMNDB.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycapB", txtNgayCapB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_noicapB", txtNoiCapB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachiB", txtDiaChiThuongTruB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dienthoaiB", txtDienThoaiB.Text.Trim());
                    #endregion

                    flatDocument.FindAndReplace("#txt_taikhoanA", txtSoTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_nganhangA", txtNganHang.Text.Trim());

                    flatDocument.FindAndReplace("#txt_nguoilap", DDLTruongPhong.SelectedItem.Text);
                    flatDocument.FindAndReplace("#txt_macan", txtMaCan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dientichtimtuong", txtDienTichTimTuong.Text.Trim());
                    flatDocument.FindAndReplace("#txt_tonggiatrichuyennhuong", txtGiaBan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_thanhtoannhansohong", txtBTraChuDauTu.Text.Trim());
                    flatDocument.FindAndReplace("#txt_benbthanhtoanbena", txtBTraA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_giacongchung", txtGiaCongChung.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycongchung", txtNgayCongChung.Text.Trim());
                    flatDocument.FindAndReplace("#txt_phimoigioibangchu", phibangchu);
                    flatDocument.FindAndReplace("#txt_phimoigioi", txtPhiDichVuC.Text.Trim());
                    flatDocument.FindAndReplace("#tblphuongthucthanhtoan", zList);
                }
            }

            File.WriteAllBytes(OutputFile, stream.ToArray());
            FileInfo file = new FileInfo(OutputFile);
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();
            }
            else
            {
                Response.Write("This file does not exist.");
            }
            #endregion
        }
        void SaveInfo()
        {
            int Key = HID_TicketKey.Value.ToInt();
            #region MyRegion
            Ticket_Info zInfo = new Ticket_Info(Key);
            zInfo.DuAn = DDLDuAn.SelectedItem.Text;
            zInfo.CodeDuAn = DDLDuAn.SelectedValue;
            zInfo.Category = 1;
            zInfo.NgayCoc = Tools.ConvertToDate(txtNgayCoc.Text.Trim());

            zInfo.DepartmentKey = DDLPhong.SelectedValue;
            zInfo.Department = DDLPhong.SelectedItem.Text;
            zInfo.EmployeeKey = DDLNhanVien.SelectedValue;
            zInfo.EmployeeName = DDLNhanVien.SelectedItem.Text;
            zInfo.ManagerKey = DDLTruongPhong.SelectedValue;
            zInfo.ManagerName = DDLTruongPhong.SelectedItem.Text;

            zInfo.HotenA = txtHotenA.Text.Trim();
            zInfo.NgaySinhA = txtNgaySinhA.Text.Trim();
            zInfo.CMNDA = txtCMNDA.Text.Trim();
            zInfo.NgayCapA = txtNgayCapA.Text.Trim();
            zInfo.NoiCapA = txtNoiCapA.Text.Trim();
            zInfo.DiaChiLienHeA = txtDiaChiLienHeA.Text.Trim();
            zInfo.DiaChiLienLacA = txtDiaChiThuongTruA.Text.Trim();
            zInfo.DienThoaiA = txtDienThoaiA.Text.Trim();
            zInfo.EmailA = txtEmailA.Text.Trim();

            zInfo.HotenB = txtHotenB.Text.Trim();
            zInfo.NgaySinhB = txtNgaySinhB.Text.Trim();
            zInfo.CMNDB = txtCMNDB.Text.Trim();
            zInfo.NgayCapB = txtNgayCapB.Text.Trim();
            zInfo.NoiCapB = txtNoiCapB.Text.Trim();
            zInfo.DiaChiLienHeB = txtDiaChiLienHeB.Text.Trim();
            zInfo.DiaChiLienLacB = txtDiaChiThuongTruB.Text.Trim();
            zInfo.DienThoaiB = txtDienThoaiB.Text.Trim();
            zInfo.EmailB = txtEmailB.Text.Trim();

            zInfo.MaCan = txtMaCan.Text.Trim();
            zInfo.LoaiCan = DDLLoaiCanHo.SelectedItem.Text;
            zInfo.DienTichTimTuong = txtDienTichTimTuong.Text;
            zInfo.DienTichThongThuy = txtDienTichThongThuy.Text;
            zInfo.GiaBan = txtGiaBan.Text.Trim();
            zInfo.GiaBanBangChu = Utils.ConvertDecimalToStringVN(txtGiaBan.Text.ToDouble());
            zInfo.BanBaoGom = txtBaoGom.Text.Trim();
            zInfo.BThanhToanBanA = txtBTraA.Text.Trim();
            zInfo.BThanhToanBanABangChu = Utils.ConvertDecimalToStringVN(txtBTraA.Text.ToDouble());

            zInfo.ChuTaiKhoan = txtChuTaiKhoan.Text.Trim();
            zInfo.SoTaiKhoan = txtSoTaiKhoan.Text.Trim();
            zInfo.NganHang = txtNganHang.Text.Trim();

            zInfo.PhiDichVuC = txtPhiDichVuC.Text.Trim();
            zInfo.NgayCongChung = txtNgayCongChung.Text.Trim();
            zInfo.GiaCongChung = txtGiaCongChung.Text.Trim();

            zInfo.ThoaThuanBanKhac = txtThoaThuanKhac.Text.Trim();

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.Save();
            HID_TicketKey.Value = zInfo.TicketKey.ToString();
            if (zInfo.Message != string.Empty)
            {
                LitMessage.Text = "Lỗi liên hệ Admin " + zInfo.Message;
                return;
            }
            #endregion

            string[] row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                string SQL = " DELETE FNC_TicketPayment WHERE TicketKey =" + zInfo.TicketKey;
                foreach (string item in row)
                {
                    //string data = "";
                    string[] col = item.Split(':');
                    double money = col[2].Trim().ToDouble();
                    //data = col[0] + " ngày " + col[1] + " số tiền " + money.ToString("n0") + " bằng chữ " + Utils.ConvertDecimalToString(money) + col[3];

                    SQL += @"
INSERT INTO FNC_TicketPayment(Title ,TimePayment ,Description ,Amount, AmountInWord ,CreatedDate ,ModifiedDate ,TicketKey )
VALUES (N'" + col[0] + "' ,N'" + col[1] + "' ,'" + col[3] + "' ,N' số tiền " + money.ToString("n0") + "',N'" + Utils.ConvertDecimalToStringVN(money) + "' ,GETDATE() ,GETDATE()," + zInfo.TicketKey + " ) ";
                }

                string Message = CustomInsert.Exe(SQL).Message;
                if (Message != string.Empty)
                    LitMessage.Text = "Lỗi liên hệ Admin " + Message;
            }
        }
        void LoadInfo()
        {
            if (Request["ID"] != null)
            {
                #region MyRegion
                HID_TicketKey.Value = Request["ID"];
                int Key = HID_TicketKey.Value.ToInt();
                Ticket_Info zInfo = new Ticket_Info(Key);
                DDLDuAn.SelectedValue = zInfo.CodeDuAn;
                txtNgayCoc.Text = zInfo.NgayCoc.ToString("dd/MM/yyyy");
                DDLPhong.SelectedValue = zInfo.DepartmentKey.ToString();
                DDLNhanVien.SelectedValue = zInfo.EmployeeKey.ToString();
                DDLTruongPhong.SelectedValue = zInfo.ManagerKey.ToString();

                txtHotenA.Text = zInfo.HotenA;
                txtNgaySinhA.Text = zInfo.NgaySinhA;
                txtCMNDA.Text = zInfo.CMNDA;
                txtNgayCapA.Text = zInfo.NgayCapA;
                txtNoiCapA.Text = zInfo.NoiCapA;
                txtDiaChiLienHeA.Text = zInfo.DiaChiLienHeA;
                txtDiaChiThuongTruA.Text = zInfo.DiaChiLienLacA;
                txtDienThoaiA.Text = zInfo.DienThoaiA;
                txtEmailA.Text = zInfo.EmailA;

                txtHotenB.Text = zInfo.HotenB;
                txtNgaySinhB.Text = zInfo.NgaySinhB;
                txtCMNDB.Text = zInfo.CMNDB;
                txtNgayCapB.Text = zInfo.NgayCapB;
                txtNoiCapB.Text = zInfo.NoiCapB;
                txtDiaChiLienHeB.Text = zInfo.DiaChiLienHeB;
                txtDiaChiThuongTruB.Text = zInfo.DiaChiLienLacB;
                txtDienThoaiB.Text = zInfo.DienThoaiB;
                txtEmailB.Text = zInfo.EmailB;

                txtMaCan.Text = zInfo.MaCan;
                DDLLoaiCanHo.Text = zInfo.LoaiCan;
                txtDienTichTimTuong.Text = zInfo.DienTichTimTuong;
                txtDienTichThongThuy.Text = zInfo.DienTichThongThuy;
                txtGiaBan.Text = zInfo.GiaBan;

                txtBaoGom.Text = zInfo.BanBaoGom;
                txtBTraA.Text = zInfo.BThanhToanBanA;

                txtChuTaiKhoan.Text = zInfo.ChuTaiKhoan;
                txtSoTaiKhoan.Text = zInfo.SoTaiKhoan;
                txtNganHang.Text = zInfo.NganHang;

                txtPhiDichVuC.Text = zInfo.PhiDichVuC;
                txtNgayCongChung.Text = zInfo.NgayCongChung;
                txtGiaCongChung.Text = zInfo.GiaCongChung;

                txtThoaThuanKhac.Text = zInfo.ThoaThuanBanKhac;
                #endregion                
            }
            DataTable zTable = TicketPayment_Data.List(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                string table = @"<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Đợt</th>
                                        <th class='td10'>Ngày</th>
                                        <th class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th>Nội dung</th>
                                        <th class='noprint'>#</th>
                                    </tr>
                                </thead>
                                <tbody>";
                foreach (DataRow r in zTable.Rows)
                {
                    table += @"<tr id='" + r["AutoKey"].ToString() + "'><td>" + r["Title"].ToString() + "</td><td tabindex = '1'>" + r["TimePayment"].ToString() + "</td><td tabindex='1'>" + r["Amount"].ToDoubleString() + "</td><td tabindex = '1'>" + r["AmountInWord"].ToString() + "</td><td tabindex='1'>" + r["Description"].ToString() + "</td><td class='edit-disabled noprint'><span btn='btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td></tr> ";
                }

                table += "</tbody></table>";
                LitBangThanhToan.Text = table;
            }
            else
            {
                LitBangThanhToan.Text = @"
<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Đợt</th>
                                        <th class='td10'>Ngày</th>
                                        <th class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th>Nội dung</th>
                                        <th class='noprint'>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Đợt 1</td>
                                        <td tabindex = '1' ></td>
                                        <td tabindex='1'></td>
                                        <td tabindex = '1' ></td>
                                        <td tabindex='1'></td>
                                        <td class='edit-disabled noprint'><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td>
                                    </tr>
                                    <tr>
                                        <td>Đợt 2</td>
                                        <td tabindex = '1' ></td>
                                        <td tabindex='1'></td>
                                        <td tabindex = '1' ></td>
                                        <td tabindex='1'></td>
                                        <td class='edit-disabled noprint'><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td>
                                    </tr>
                                </tbody>
                            </table>";

            }
        }
    }
}