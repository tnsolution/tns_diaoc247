﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketSalesView.aspx.cs" Inherits="WebApp.FNC.TicketSalesView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Phiếu bán hàng</h1>
        </div>
        <div class="col-md-7">
            <table class="table-bordered table">
                <tr>
                    <th colspan="6">Dự án: Lexington</th>
                </tr>
                <tr>
                    <th>NV bán hàng:</th>
                    <td colspan="2">Nguyễn Thị Ngọc Trinh</td>
                    <th>Ngày lập:</th>
                    <td colspan="2">01/11/2017</td>
                </tr>
                <tr>
                    <th>Phòng:</th>
                    <td colspan="2">Phú Nhuận</td>
                    <th>Quản lý: </th>
                    <td colspan="2">Nguyễn Thị Thanh Thủy</td>
                </tr>
                <tr>
                    <th colspan="6">Bên A (Bên Chuyển Nhượng)</th>
                </tr>
                <tr>
                    <th>Họ tên:</th>
                    <td colspan="5">Trương Quang Hữu Phước</td>
                </tr>
                <tr>
                    <th>CMND/Passport:</th>
                    <td>025862953</td>
                    <th>Ngày cấp: </th>
                    <td>27/11/2013</td>
                    <th>Nơi cấp: </th>
                    <td>TP.HCM </td>
                </tr>
                <tr>
                    <th>Địa chỉ thường trú:</th>
                    <td colspan="5"></td>

                </tr>
                <tr>
                    <th>Địa chỉ liên hệ:</th>
                    <td colspan="5"></td>

                </tr>
                <tr>
                    <th colspan="6">Bên B (Bên Nhận Chuyển Nhượng)</th>
                </tr>
                <tr>
                    <th>Họ tên:</th>
                    <td colspan="5">Trương Quang Hữu Phước</td>
                </tr>
                <tr>
                    <th>CMND/Passport:</th>
                    <td>025862953</td>
                    <th>Ngày cấp: </th>
                    <td>27/11/2013</td>
                    <th>Nơi cấp: </th>
                    <td>TP.HCM </td>
                </tr>
                <tr>
                    <th>Địa chỉ thường trú:</th>
                    <td colspan="5"></td>

                </tr>
                <tr>
                    <th>Địa chỉ liên hệ:</th>
                    <td colspan="5"></td>

                </tr>
                <tr>
                    <th colspan="6">Sản phẩm</th>
                </tr>
                <tr>
                    <th>Mã căn:</th>
                    <td>GG-21.12</td>
                    <th>Loại:</th>
                    <td>Căn hộ</td>
                    <th>Diện tích m2</th>
                    <td>40.9</td>
                </tr>
                <tr>
                    <th>Giá trị thực</th>
                    <td>3,600,000,000 </td>
                    <th>Đặt cọc</th>
                    <td>50,000,000</td>

                    <th>Ngày</th>
                    <td>03/11/2017</td>
                </tr>
                <tr>
                    <th>Môi giới/CTV</th>
                    <td colspan="2">TP Bank</td>
                    <th>Phí môi giới CTV</th>
                    <td colspan="2">5%</td>
                </tr>
                <tr>
                    <th colspan="6">Thỏa thuận khác</th>
                </tr>
                <tr>
                    <td colspan="6">Bên A sẽ thanh toán cho Bên C “phí dịch vụ” tương đương số tiền: 36,000,000 VNĐ (Ba mươi sáu triệu đồng)<br />
                        Ngày công chứng thực tế có thể sớm hoặc muộn hơn 5 ngày tùy theo thỏa thuận của 2 Bên nhưng không quá ngày 07/07/2017</td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                    <th colspan="4">Hình thức thanh toán</th>
                </tr>
                <tr>
                    <th>Đợt 1</th>
                    <td>03/11/2017</td>
                    <td>200,000,000 VNĐ</td>
                    <td>Bên B thanh toán cho Bên A số tiền đặt cọc</td>
                </tr>
                <tr>
                    <th>Đợt 2</th>
                    <td>04/11/2017</td>
                    <td>3,300,000,000 VNĐ</td>
                    <td>hai Bên ra Công chứng chuyển nhượng hợp đồng mua bán</td>
                </tr>
                <tr>
                    <th>Đợt 1</th>
                    <td>11/11/2017</td>
                    <td>70,000,000 VNĐ</td>
                    <td>Bên A hoàn tất nghĩa vụ thuế. Bên B thanh toán cho Bên A số còn lại</td>
                </tr>
            </table>
        </div>
        <%--<div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fa fa-rss orange"></i>
                                Tình trạng
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">1. Duyệt bán hàng</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Nguyễn Thị Thanh Thủy</span>
                                            <span>01/11/2017 14:31</span>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">2. Xác nhận giao dịch</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Nguyễn Thị Thanh Thủy</span>
                                            <span>03/11/2017 14:31</span>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">3. Duyệt hợp đồng</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Nguyễn Thị Thanh Thủy</span>
                                            <span>04/11/2017 14:31</span>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">4. Hoàn tất giao dịch</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Trần Phi Hùng</span>
                                            <span>30/11/2017 14:31</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fa fa-rss orange"></i>
                                Hiện trạng xử lý
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div id="profile-feed-1" class="profile-feed ace-scroll">
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đang soạn phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        8 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đã hoàn tất phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đã gửi trình duyệt phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thanh Thủy</a>
                                            Đã duyệt phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đã gửi thông tin phiếu bán hàng để lập hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thùy Giang</a>
                                            Đang lập hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thùy Giang</a>
                                            Đã in hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thùy Giang</a>
                                            Đã gửi hợp đồng cho sales
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thanh Thủy</a>
                                            Hợp đồng sai yêu cầu làm lại<br />
                                            Nội dung cần sửa: Sai nội dung sản phẩm.
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thanh Thủy</a>
                                            Đã duyệt hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
