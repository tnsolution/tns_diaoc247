﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeEdit.aspx.cs" Inherits="WebApp.FNC.TradeEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
	<link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
	<style>
		sup {
			vertical-align: sup;
			font-size: smaller;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
	<div class="page-content">
		<div class="page-header">
			<h1>Cập nhật giao dịch
                <asp:Literal ID="txt_Name" runat="server"></asp:Literal>
				<span class="tools pull-right">
					<asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
					<a type="button" class="btn btn-white btn-default btn-bold" href="TradeList.aspx">
						<i class="ace-icon fa fa-reply blue"></i>
						Về danh sách
					</a>
				</span>
			</h1>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="tabbable tabs-left">
					<ul class="nav nav-tabs">
						<li class="active">
							<a data-toggle="tab" href="#info">
								<i class="pink ace-icon fa fa-tachometer bigger-110"></i>
								Thông tin giao dịch
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#file">
								<i class="blue ace-icon fa fa-folder bigger-110"></i>
								Tập tin văn bản
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#picture">
								<i class="blue ace-icon fa fa-image bigger-110"></i>
								Hình ảnh
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="info" class="tab-pane active">
							<div class="form-horizontal">
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">Người lập</label>
									<div class="col-sm-2">
										<asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
											<asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
										</asp:DropDownList>
									</div>
									<label for="" class="col-sm-2 control-label">Nguồn giao dịch</label>
									<div class="col-sm-2">
										<asp:DropDownList ID="DDL_Source" runat="server" class="form-control select2">
											<asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
											<asp:ListItem Value="1" Text="Trực tiếp"></asp:ListItem>
											<asp:ListItem Value="2" Text="Liên kết"></asp:ListItem>
										</asp:DropDownList>
									</div>
									<label for="" class="col-sm-2 control-label">Ngày ký hợp đồng</label>
									<div class="col-sm-2">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" runat="server" role="datepicker" class="form-control pull-right"
												id="txt_DateSign" placeholder="Chọn ngày tháng năm" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">Loại giao dịch</label>
									<div class="col-sm-2">
										<asp:DropDownList ID="DDL_Category" runat="server" class="form-control select2" AppendDataBoundItems="true">
											<asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
										</asp:DropDownList>
									</div>
									<label for="" class="col-sm-2 control-label">Ngày cọc</label>
									<div class="col-sm-2">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" runat="server" role="datepicker" class="form-control pull-right"
												id="txt_DatePreOrder" placeholder="Chọn ngày tháng năm" />
										</div>
									</div>
									<label for="" class="col-sm-2 control-label" type="divExpire">Ngày hết hợp đồng <sup>*</sup></label>
									<div class="col-sm-2" type="divExpire">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" runat="server" role="datepicker" class="form-control pull-right"
												id="txt_Expired" placeholder="Chọn ngày tháng năm" />
										</div>
									</div>
								</div>
								<div class="form-group" type="divExpire">
									<label for="" class="col-sm-2 control-label">Thuê ngắn/ dài hạn</label>
									<div class="col-sm-2">
										<asp:DropDownList ID="DDL_Shorterm" CssClass="form-control" runat="server">
											<asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
											<asp:ListItem Value="1" Text="Ngắn hạn"></asp:ListItem>
											<asp:ListItem Value="2" Text="Dài hạn"></asp:ListItem>
										</asp:DropDownList>
									</div>
									<label for="" class="col-sm-2 control-label">Số tháng thuê</label>
									<div class="col-sm-2">
										<input type="text" runat="server" class="form-control pull-right"
											id="txt_Month" placeholder="Số tháng thuê" />
									</div>
								</div>
							</div>
							<div class="space"></div>
							<div class="widget-box transparent">
								<div class="widget-header widget-header-small">
									<h4 class="widget-title blue smaller">
										<i class="ace-icon fa fa-product-hunt orange"></i>
										Thông tin sản phẩm giao dịch
									</h4>
									<div class="widget-toolbar action-buttons">
										<a href="#mAssetFilter" class="pink" data-toggle="modal">
											<i class="ace-icon fa fa-search bigger-125"></i>
											Chọn sản phẩm
										</a>
									</div>
								</div>
								<div class="widget-body">
									<div class="widget-main padding-8">
										<asp:Literal ID="Lit_TableProduct" runat="server"></asp:Literal>
									</div>
								</div>
							</div>
							<div class="space"></div>
							<div class="widget-box transparent">
								<div class="widget-header widget-header-small">
									<h4 class="widget-title blue smaller">
										<i class="ace-icon fa fa-rss orange"></i>
										Thông tin chủ nhà, khách.
									</h4>
									<div class="widget-toolbar action-buttons">
										<a id="btnAddGuest" class="pink" href="#">
											<i class="ace-icon fa fa-plus bigger-125"></i>
											Thêm khách mới
										</a>
										<a href="#mGuestFilter" class="pink" data-toggle="modal">
											<i class="ace-icon fa fa-search bigger-125"></i>
											Chọn khách
										</a>
									</div>
								</div>
								<div class="widget-body">
									<div class="widget-main padding-8">
										<asp:Literal ID="Lit_TableCustomer" runat="server"></asp:Literal>
									</div>
								</div>
							</div>
							<div class="space"></div>
							<div class="widget-box transparent">
								<div class="widget-header widget-header-small">
									<h4 class="widget-title blue smaller">
										<i class="ace-icon fa fa-dollar orange"></i>
										Chi phí.                                               
									</h4>
									<div class="widget-toolbar no-border">
										<label>
											<input name="form-field-checkbox" class="ace" type="checkbox" runat="server" id="chkDevined" />
											<span class="lbl">&nbsp Chia/1.1</span>
										</label>
									</div>
								</div>
								<div class="widget-body">
									<div class="widget-main padding-8">
										<div class="form-horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label">Giá có VAT</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_AmountVAT" placeholder="Nhập số" moneyinput calcmoney="yes" />
												</div>
												<label class="col-sm-2 control-label">Giá chưa VAT</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_Amount" placeholder="Tự động" moneyinput readonly="true" />
												</div>
												<label class="col-sm-2 control-label">Phí giảm trừ</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_OtherFee" placeholder="Nhập số" moneyinput calcmoney="yes" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">VAT %</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_VAT" placeholder="% VAT" moneyinput calcmoney="yes" data='money' maxlength="2" />
												</div>
												<label class="col-sm-2 control-label">Phí hoa đồng</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_Commision" placeholder="Nhập số" moneyinput calcmoney="yes" />
												</div>
												<label class="col-sm-2 control-label">Doanh thu</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_Income" placeholder="Tự động" moneyinput style="border-color: red" readonly="true" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="space" hoahong="true" style="display: none"></div>
							<div class="widget-box transparent" hoahong="true" style="display: none">
								<div class="widget-header widget-header-small">
									<h4 class="widget-title blue smaller">
										<i class="ace-icon fa fa-dollar orange"></i>
										Hoa hồng hổ trợ                                      
									</h4>
									<div class="widget-toolbar action-buttons">
										<a id="btnRefeshSupport" href="#" class="pink">
											<i class="ace-icon fa fa-refresh bigger-125"></i>
											Làm mới
										</a>
										<a id="btnAddSupport" href="#" class="pink">
											<i class="ace-icon fa fa-plus bigger-125"></i>
											Thêm
										</a>
									</div>
								</div>
								<div class="widget-body">
									<div class="widget-main padding-8">
										<asp:Literal ID="LitTableCommistion" runat="server"></asp:Literal>
										<div class="hr hr-double hr-dotted hr-2"></div>
										<div class="form-horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label">Chi phí nội bộ</label>
												<div class="col-sm-2">
													<input type="text" runat="server" class="form-control" id="txt_Fee" placeholder="Nhập số" moneyinput />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="space"></div>
							<div class="widget-box transparent">
								<div class="widget-header widget-header-small">
									<h4 class="widget-title blue smaller">
										<i class="ace-icon fa fa-info orange"></i>
										Ghi chú
									</h4>
								</div>
								<div class="widget-body">
									<div class="widget-main padding-8">
										<textarea class="form-control limited" id="txt_Description" maxlength="500" style="margin-top: 0px; margin-bottom: 0px; height: 60px;" runat="server"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div id="file" class="tab-pane">
							<div class="row">
								<div class="col-xs-12">
									<ul class="text-danger list-unstyled spaced">
										<li><i class="ace-icon fa fa-exclamation-triangle"></i>Các tập tin của dự án, khi tải tập tin vui lòng chọn mục tập tin (giới thiệu, pháp lý, ...) cần tải lên, chỉ cho phép các tập tin văn bản</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-2">
									<input type="file" id="id-input-file-1" name="id-input-file-1" multiple="" />
								</div>
								<div class="col-xs-3">
									<div class="form-group">
										<label class="">Diễn giải tập tin</label>
										<asp:TextBox ID="txt_Description1" runat="server" CssClass="form-control" placeholder="Diễn giải tập tin, ..."></asp:TextBox>
									</div>
								</div>
								<div class="col-xs-2">
									<a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadFile">
										<i class="ace-icon fa fa-plus"></i>
										Upload
									</a>
								</div>
							</div>
							<div class="space-6"></div>
							<div class="row">
								<div class="col-xs-12">
									<div id="accordion" class="accordion-style1 panel-group">
										<asp:Literal ID="Lit_Folder" runat="server"></asp:Literal>
									</div>
								</div>
							</div>
						</div>
						<div id="picture" class="tab-pane">
							<div class="row">
								<div class="col-xs-12">
									<ul class="text-warning list-unstyled spaced">
										<li><i class="ace-icon fa fa-exclamation-triangle"></i>Các tập tin của dự án, khi tải tập tin vui lòng chọn mục tập tin (giới thiệu, pháp lý, ...) cần tải lên, chỉ cho phép các tập tin văn bản</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-2">
									<input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
								</div>
								<div class="col-xs-3">
									<div class="form-group">
										<label class="">Diễn giải tập tin</label>
										<asp:TextBox ID="txt_Description2" runat="server" CssClass="form-control" placeholder="Diễn giải hình ảnh, ..."></asp:TextBox>
									</div>
								</div>
								<div class="col-xs-2">
									<a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
										<i class="ace-icon fa fa-plus"></i>
										Upload
									</a>
								</div>
							</div>
							<div class="space-6"></div>
							<div class="row">
								<div class="col-xs-12">
									<div id="accordion2" class="accordion-style1 panel-group">
										<asp:Literal ID="Lit_ListPicture" runat="server"></asp:Literal>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Search -->
	<div class="modal fade" id="mAssetFilter">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Chọn nguồn sản phẩm</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-10">
							<div class="form-group">
								<div class="row">
									<div class="col-xs-5">
										<asp:DropDownList ID="DDL_AssetProject" class="select2" runat="server" AppendDataBoundItems="true">
											<asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
										</asp:DropDownList>
									</div>
									<div class="col-xs-4">
										<asp:DropDownList ID="DDL_AssetCategory" runat="server" CssClass="select2" AppendDataBoundItems="true">
											<asp:ListItem Value="0" Text="--Loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
										</asp:DropDownList>
									</div>
									<div class="col-xs-3">
										<input type="text" class="form-control" id="txt_AssetID" placeholder="Mã" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-2">
							<button id="btnAssetSearch" type="button" class="btn btn-sm btn-purple btn-block pull-right">Tìm</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" id="table">
							<asp:Literal ID="Lit_Asset" runat="server"></asp:Literal>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
						Đóng</button>
					<button type="button" class="btn btn-primary" id="btnAssetSelect" data-dismiss="modal">
						Chọn</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="mGuestFilter">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Chọn nguồn khách hàng</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<input type="text" class="form-control" id="txt_Search_CustomerID" placeholder="Tìm SĐT" />
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<input type="text" class="form-control" id="txt_Search_CustomerName" placeholder="Tìm tên khách hàng" />
							</div>
						</div>
						<div class="col-md-2 pull-right">
							<button id="btnGuestSearch" type="button" class="btn btn-block btn-info">Tìm</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<asp:Literal ID="Lit_Guest" runat="server"></asp:Literal>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
						Đóng</button>
				</div>
			</div>
		</div>
	</div>
	<!---guest--->
	<div class="modal fade" id="mOwner">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Thông tin khách/ chủ nhà</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-4 control-label">Họ và tên<sup>*</sup></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Ngày sinh<sup>*</sup></label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" role="datepicker" class="form-control pull-right"
												id="txt_Birthday" placeholder="Chọn ngày tháng năm" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">CMND<sup>*</sup></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_CardID" placeholder="Nhập text" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Ngày cấp<sup>*</sup></label>
									<div class="col-sm-8">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" role="datepicker" class="form-control pull-right"
												id="txt_CardDate" placeholder="Chọn ngày tháng năm" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Nơi cấp<sup>*</sup></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_CardPlace" placeholder="Nhập text" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">SĐT1<sup>*</sup></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_Phone1" placeholder="Nhập text" role="phone" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">SĐT2</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" role="phone" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Địa chỉ thường trú(*)</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_Address1" placeholder="Nhập text" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Địa chỉ liên hệ</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<div class="radio">
											<label>
												<input type="radio" class="ace" value="1" name="chkPurpose" id="chkIsOwner1" />
												<span class="lbl">Chủ nhà</span>
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="checkbox">
											<label>
												<input type="radio" class="ace" value="2" name="chkPurpose" id="chkIsOwner2" />
												<span class="lbl">Khách thuê</span>
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="radio">
											<label>
												<input type="radio" class="ace" value="3" name="chkPurpose" id="chkIsOwner3" />
												<span class="lbl">Khách mua</span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
						Đóng</button>
					<button type="button" class="btn btn-primary" id="btnSaveCustomer">
						Lưu</button>
				</div>
			</div>
		</div>
	</div>
	<!---guest list --->
	<div class="modal fade" id="mOwnerList">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Chọn chủ nhà</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<h5 id="note"></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-bordered" id="tblOwnerList">
								<thead>
									<tr>
										<th>#</th>
										<th>Tên khách hàng</th>
										<th>SĐT</th>
										<th>Ngày cập nhật</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="3">Chưa có data</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
						Đóng</button>
					<button type="button" class="btn btn-primary" id="btnSelectCustomer" data-dismiss="modal">
						Chọn</button>
				</div>
			</div>
		</div>
	</div>
	<!---support list --->
	<div class="modal fade" id="mSupport">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Chỉnh sửa</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-3 control-label">Loại phí</label>
									<div class="col-sm-9">
										<asp:DropDownList ID="DDL_Fee" runat="server" class="form-control select2" AppendDataBoundItems="true">
											<asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
										</asp:DropDownList>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Nhân viên hổ trợ</label>
									<div class="col-sm-9">
										<asp:DropDownList ID="DDL_StaffSupport" runat="server" class="form-control select2" AppendDataBoundItems="true">
											<asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
										</asp:DropDownList>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Số tiền</label>
									<div class="col-sm-9">
										<input id="txtMoney" type="text" class="form-control" placeholder="Nhập số tiền" moneyinput="true" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<textarea id="txtDescription" class="form-control" placeholder="Ghi chú"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
						Đóng</button>
					<button type='button' class='btn btn-primary' data-dismiss='modal' id='btnSave'>
						Cập nhật</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	<asp:HiddenField ID="HID_SupportKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_EmployeeKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_OwnerKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
	<asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_TradeKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_CustomerKey" runat="server" Value="0" />
	<asp:HiddenField ID="HID_CustomerEdit" runat="server" Value="0" />
	<!--kiem tra tình trang xử lý thao tac trên form 1 sửa,2 thêm mới -->
	<asp:Button ID="btnUploadFile" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadFile_Click" />
	<asp:Button ID="btnUploadImg" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadImg_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
	<script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
	<script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
	<script type="text/javascript" src="/template/jquery.number.min.js"></script>
	<script src="/FNC/TradeEdit.js"></script>
	<script>
		$(function () {
			$("[id$=DDL_Shorterm]").change(function () {
				if (this.value == 1) {
					$("[hoahong]").hide();
				}
			});
			$("input[moneyinput]").number(true, 0);
			$("#tblCommision > tbody").on("click", "tr td:not(:last-child)", function () {
				var id = $(this).closest('tr').attr('id');
				$("[id$=HID_SupportKey]").val(id);
				OpenSupport(id);
			});
			$("#tblCommision > tbody").on("click", "a[btn='btnDel']", function (e) {
				e.preventDefault();
				var key = $(this).closest('tr').attr('id');
				DeleteSupport(key);
			});
			$("#btnSave").click(function () {
				SaveSupport();
			});
			$("#btnRefeshSupport").click(function (e) {
				e.preventDefault();
				RefeshSupport();
			});
			$("#btnAddSupport").click(function (e) {
				e.preventDefault();
				AddSupport();
			});
			var unit = getUnitLevel();			if (unit >= 2) {
				$("[hoahong]").hide();
			}
			else {
				$("[hoahong]").show();
			}
		});
		function RefeshSupport() {
			var source = $("[id$=DDL_Source]").val();
			var category = $("[id$=DDL_Category]").val();
			var staff = $("[id$=DDL_Employee]").val();

			if (staff == null || staff == undefined) {
				alert("Bạn phải chọn nhân viên");
				return;
			}
			if (category == null || category == undefined) {
				alert("Bạn phải chọn loại giao dịch");
				return;
			}
			if (source == null || source == undefined) {
				alert("Bạn phải chọn nguồn giao dịch");
				return;
			}
			if (confirm("Thao tác này sẽ xóa và tạo mới lại phí hổ trợ, bạn có muốn tiếp tục")) {
				$.ajax({
					type: 'POST',
					url: '/FNC/TradeEdit.aspx/RefeshSupport',
					data: JSON.stringify({
						'TradeKey': $("[id$=HID_TradeKey]").val(),
						'TradeType': source,
						'TradeCategory': category,
						'EmployeeKey': staff,
					}),
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					beforeSend: function () {
						$('.se-pre-con').fadeIn('slow');
					},
					success: function (msg) {
						$('#tblCommision > tbody').empty();
						$('#tblCommision > tbody').append($(msg.d));
					},
					complete: function () {
						$('.se-pre-con').fadeOut('slow');
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}
		}
		function OpenSupport(id) {
			$.ajax({
				type: 'POST',
				url: '/FNC/TradeEdit.aspx/GetSupport',
				data: JSON.stringify({
					'AutoKey': id,
				}),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				beforeSend: function () {
					$(".se-pre-con").fadeIn("slow");
				},
				success: function (r) {
					$("[id$=HID_SupportKey]").val(r.d.AutoKey);
					$("[id$=DDL_StaffSupport]").val(r.d.EmployeeKey).trigger("change");
					$("[id$=DDL_Fee]").val(r.d.CommissionKey).trigger("change");
					$("#txtMoney").val(r.d.Money);
					$("#txtDescription").val(r.d.Description);

					$('#mSupport').modal({
						backdrop: true,
						show: true
					});
				},
				complete: function () {
					$(".se-pre-con").fadeOut("slow");
				},
				error: function (xhr, ajaxOptions, thrownError) {
					Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
		function AddSupport() {
			$('[id$=HID_SupportKey]').val(0);
			$('#mSupport').modal({
				backdrop: true,
				show: true
			});
		}
		function SaveSupport() {
			var AutoKey = $('[id$=HID_SupportKey]').val();
			var TradeKey = $('[id$=HID_TradeKey]').val();
			var EmployeeKey = $('[id$=DDL_StaffSupport]').val();
			var Money = $('[id$=txtMoney]').val();
			var Description = $('[id$=txtDescription]').val();
			var CommissionKey = $('[id$=DDL_Fee]').val();
			$.ajax({
				type: 'POST',
				url: '/FNC/TradeEdit.aspx/SaveSupport',
				data: JSON.stringify({
					'AutoKey': AutoKey,
					'CommissionKey': CommissionKey,
					'TradeKey': TradeKey,
					'EmployeeKey': EmployeeKey,
					'Money': Money,
					'Description': Description,
				}),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				beforeSend: function () {
					$('.se-pre-con').fadeIn('slow');
				},
				success: function (msg) {
					$("[id$=HID_SupportKey]").val(0);
					if (msg.d == "OK") {
						LoadSupport();
					}
					else {
						Page.showNotiMessageError(msg.d);
					}
				},
				complete: function () {
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
		function LoadSupport() {
			$.ajax({
				type: 'POST',
				url: '/FNC/TradeEdit.aspx/LoadSupport',
				data: JSON.stringify({
					'TradeKey': $("[id$=HID_TradeKey]").val(),
				}),
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				beforeSend: function () {
				},
				success: function (msg) {
					$('#tblCommision > tbody').empty();
					$('#tblCommision > tbody').append($(msg.d));
				},
				complete: function () {
					$('.se-pre-con').fadeOut('slow');
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
					console.log(thrownError);
				}
			});
		}
		function DeleteSupport(id) {
			if (confirm("Bạn có chắc xóa thông tin hoa hồng này !.")) {
				$.ajax({
					type: "POST",
					url: "/FNC/TradeEdit.aspx/DeleteSupport",
					data: JSON.stringify({
						'AutoKey': id,
					}),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					beforeSend: function () {

					},
					success: function (msg) {
						if (msg.d == "") {
							$('#tblCommision > tbody > tr[id=' + id + ']').remove();
						}
						else {
							Page.showPopupMessage("Lỗi xóa thông tin !.", msg.d);
						}
					},
					complete: function () {

					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
						console.log(thrownError);
					}
				});
			}
		}
	</script>
</asp:Content>
