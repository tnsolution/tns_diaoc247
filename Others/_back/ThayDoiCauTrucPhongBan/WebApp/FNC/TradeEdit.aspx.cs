﻿using Lib.CRM;
using Lib.FNC;
using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

/*
//nguon giao dich
//1 truc tiep, 2 lien ket
//giao dịch ngan han luu thêm for số tháng lưu từng dòng vào 1 bảng tạm
 */

namespace WebApp.FNC
{
    public partial class TradeEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_TradeKey.Value = Request["ID"];

                Tools.DropDown_DDL(DDL_StaffSupport, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 29", false);
                Tools.DropDown_DDL(DDL_AssetProject, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                CheckRole();
                LoadData();
                LoadCustomer();
                LoadFolder();
                LoadPicture();
                LoadCommistion();
                InitSearchAsset();
                InitSearchGuest();
            }
        }

        #region [Chức năng]
        void LoadData()
        {
            Trade_Info zInfo = new Trade_Info(HID_TradeKey.Value.ToInt());
            Tools.DropDown_DDL(DDL_Fee, @"SELECT AutoKey, Name FROM HRM_Commission_Support WHERE TradeCategory IN (0, " + zInfo.TradeCategoryKey + ") AND TradeType IN (0," + zInfo.Source + ") ", false);

            DDL_Shorterm.SelectedValue = zInfo.ShortTerm.ToString();
            DDL_Category.SelectedValue = zInfo.TradeCategoryKey.ToString();
            DDL_Employee.SelectedValue = zInfo.EmployeeKey.ToString();
            DDL_Source.SelectedValue = zInfo.Source.ToString();

            if (zInfo.IsDevined == 1)
                chkDevined.Checked = true;
            else
                chkDevined.Checked = false;

            txt_DatePreOrder.Value = zInfo.DateDeposit.ToString("dd/MM/yyyy");
            txt_DateSign.Value = zInfo.DateContract.ToString("dd/MM/yyyy");
            txt_Expired.Value = zInfo.DateContractEnd.ToString("dd/MM/yyyy");
            txt_AmountVAT.Value = zInfo.AmountVAT.ToDoubleString();
            txt_Amount.Value = zInfo.AmountVAT.ToDoubleString();
            txt_VAT.Value = zInfo.VAT.ToString();
            txt_OtherFee.Value = zInfo.OtherFee.ToDoubleString();
            txt_Commision.Value = zInfo.TransactionFee.ToDoubleString();
            txt_Income.Value = zInfo.Income.ToDoubleString();
            txt_Description.Value = zInfo.Description;
            txt_Fee.Value = zInfo.InternalCost.ToDoubleString();
            txt_Month.Value = zInfo.HireMonth.ToString();

            #region [html sản phẩm]
            string Asset = "";
            if (zInfo.AssetKey != 0)
            {
                Asset = @"  <tr id=" + zInfo.AssetKey + " type=" + zInfo.AssetType + @">
                                                <td>" + 1 + @"</td>
                                                <td>" + zInfo.ProjectName + @"</td>
                                                <td>" + zInfo.AssetID + @"</td>
                                                <td>" + zInfo.AssetCategoryName + @"</td>
                                                <td>" + zInfo.Area + @"</td>
                                                <td>" + zInfo.AddressProject + @"</td>
                                                <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDelAsset' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>
                                            </tr>";
            }

            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine(@"<table class='table' id='tblAsset'>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Dự án</th>
                                                <th>Mã sản phẩm</th>
                                                <th>Loại sản phẩm</th>
                                                <th>Diện tích m<sup>2</sup></th>
                                                <th>Địa chỉ</th>
                                                <th>...</th>
                                            </tr>
                                        </thead>
                                        <tbody>" + Asset + "</tbody></table>");
            #endregion

            Lit_TableProduct.Text = zSb.ToString();
            HID_TradeKey.Value = zInfo.Key.ToString();
            HID_AssetKey.Value = zInfo.AssetKey.ToString();
            HID_AssetType.Value = zInfo.AssetType;
            HID_EmployeeKey.Value = zInfo.EmployeeKey.ToString();
        }
        void LoadCustomer()
        {
            DataTable zTable = Trade_Data.GetCustomer(HID_TradeKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table' id='tblCustomer'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th>#</th>");
            zSb.AppendLine("            <th>Họ tên</th>");
            zSb.AppendLine("            <th>SĐT</th>");
            zSb.AppendLine("            <th>Ngày sinh</th>");
            zSb.AppendLine("            <th>CMND</th>");
            zSb.AppendLine("            <th>Địa chỉ</th>");
            zSb.AppendLine("            <th>...</th>");
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int o = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("        <tr id='" + r["CustomerKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (o++) + "</td>");
                    zSb.AppendLine("            <td>" + r["CustomerName"].ToString() + "<br/>" + r["Owner"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Phone1"].ToString() + " <br/>" + r["Phone2"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Birthday"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>CMND: " + r["CardID"].ToString() + "<br/>Nơi cấp: " + r["CardPlace"].ToString() + " </td>");
                    zSb.AppendLine("            <td>Thường trú: " + r["Address1"].ToString() + "<br/> Liên hệ:" + r["Address2"].ToString() + "</td>");
                    zSb.AppendLine("            <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDelCustomer' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("        </tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("              <td>#</td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_TableCustomer.Text = zSb.ToString();
        }
        void LoadPicture()
        {
            List<ItemDocument> zList = Document_Data.List(130, HID_TradeKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<ul class='ace-thumbnails clearfix'>");
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<li id='" + item.FileKey + "'>");
                zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageUrl + "' />");
                zSb.AppendLine("            <div class='text'>");
                zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                zSb.AppendLine("            </div>");
                zSb.AppendLine("    </a>");
                zSb.AppendLine("    <div class='tools tools-bottom'><a href='#' btn='btnDeleteImg'><i class='ace-icon fa fa-times red'></i>Xóa</a></div>");
                zSb.AppendLine("</li>");
            }
            zSb.AppendLine(" </ul>");
            Lit_ListPicture.Text = zSb.ToString();
        }
        void LoadFolder()
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemDocument> zList = Document_Data.List(135, HID_TradeKey.Value.ToInt(), "Transaction");
            zSb.AppendLine("<table class='table table-hover' id='tblFile'>");
            zSb.AppendLine("  <thead>");
            zSb.AppendLine("     <tr>");
            zSb.AppendLine("         <th>STT</th>");
            zSb.AppendLine("         <th>Tập tin</th>");
            zSb.AppendLine("         <th>Diễn giải</th>");
            zSb.AppendLine("         <th>Ngày cập nhật</th>");
            zSb.AppendLine("         <th>Người cập nhật</th>");
            zSb.AppendLine("         <th>...</th>");
            zSb.AppendLine("     </tr>");
            zSb.AppendLine(" </thead>");
            zSb.AppendLine(" <tbody>");
            int i = 1;
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td>" + item.Description + "</td><td>" + item.ModifiedDate + "</td><td>" + item.ModifiedName + "</td><td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteFile' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td></tr>");
            }
            zSb.AppendLine(" </tbody>");
            zSb.AppendLine(" </table>");

            Lit_Folder.Text = zSb.ToString();
        }
        //-----
        void InitSearchAsset()
        {
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            List<Product_Item> zList = new List<Product_Item>();
            zList = Product_Data.Search(DDL_AssetProject.SelectedValue.ToInt(), DDL_AssetCategory.SelectedValue.ToInt(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 1, CurrentAgent, 100);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblAssetSearch'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Mã sản phẩm</th>");
            zSb.AppendLine("        <th>Loại sản phẩm</th>");
            zSb.AppendLine("        <th>Diện tích m<sup>2</sup></th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int i = 1;
            foreach (Product_Item r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.AssetKey + "' type='" + r.AssetType + "'>");
                zSb.AppendLine("                <td>" + i++ + "</td>");
                zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                zSb.AppendLine("                <td>" + r.Area + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Asset.Text = zSb.ToString();
        }
        void InitSearchGuest()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            List<ItemCustomer> zList = Customer_Data.Get(Department, Employee);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblGuestSearch'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Tên khách hàng</th>");
            zSb.AppendLine("        <th>SĐT</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            int no = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "'>");
                zSb.AppendLine("               <td>" + (no++) + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br/>" + r.Phone2 + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Guest.Text = zSb.ToString();
        }
        #endregion

        #region [Action giao dich]
        [WebMethod]
        public static ItemReturn DeleteTrade(int TradeKey)
        {
            Trade_Info zInfo = new Trade_Info();
            zInfo.Key = TradeKey;
            zInfo.Delete();

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn InitTrade(int TradeCategory, string DateSign, string DateExpired, string DatePre, string Description, int Employee, int Source)
        {
            Trade_Info zInfo = new Trade_Info();
            zInfo.TradeCategoryKey = TradeCategory;
            zInfo.DateContract = Tools.ConvertToDate(DateSign);
            zInfo.DateDeposit = Tools.ConvertToDate(DatePre);
            zInfo.DateContractEnd = Tools.ConvertToDate(DateExpired);
            zInfo.Description = Description;
            zInfo.TransactionDate = DateTime.Now;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(Employee);
            zInfo.ApprovedBy = Employees_Data.GetManager(Employee);
            zInfo.EmployeeKey = Employee;
            zInfo.Source = Source;

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Create();

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemReturn SaveTrade(
            int TradeKey, int TradeCategory, string DateSign, string DateExpired, string DatePre,
            string Description, int Employee, string AmountVAT, string Amount, string OtherFee,
            string VAT, string Commision, string Income, string IsDevined, string Support, string Source,
            string Cost, string Month, string Shorterm)
        {
            Trade_Info zInfo = new Trade_Info(TradeKey);
            zInfo.TradeCategoryKey = TradeCategory;
            zInfo.DateContract = Tools.ConvertToDate(DateSign);
            zInfo.DateDeposit = Tools.ConvertToDate(DatePre);
            zInfo.DateContractEnd = Tools.ConvertToDate(DateExpired);
            zInfo.Description = Description;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(Employee);
            zInfo.ApprovedBy = Employees_Data.GetManager(Employee);
            zInfo.EmployeeKey = Employee;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Source = Source.ToInt();
            zInfo.Amount = double.Parse(Amount);
            zInfo.AmountVAT = double.Parse(AmountVAT);
            zInfo.VAT = VAT.ToInt();
            zInfo.TransactionFee = double.Parse(Commision);
            zInfo.OtherFee = double.Parse(OtherFee);
            zInfo.Income = double.Parse(Income);
            zInfo.IsDevined = IsDevined.ToInt();
            zInfo.InternalCost = double.Parse(Cost);
            zInfo.HireMonth = Month.ToInt();
            zInfo.ShortTerm = Shorterm.ToInt();
            zInfo.Update();

            #region [Auto Sum Month]
            string ExtraMessage = "";
            if (Shorterm.ToInt() == 1)
            {
                string SQL = " DELETE FNC_Trade_Temp WHERE TransactionKey = " + zInfo.Key;
                for (int i = 1; i <= Month.ToInt(); i++)
                {
                    SQL += " INSERT INTO FNC_Trade_Temp (TransactionKey, Month, Passed) VALUES (" + zInfo.Key + "," + i + ",0)";
                }
                ExtraMessage = CustomInsert.Exe(SQL).Message;
            }
            #endregion

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message + ExtraMessage;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        //-----
        [WebMethod]
        public static ItemAsset[] AssetSearch(int Project, int Category, string AssetID)
        {
            List<ItemAsset> zList = new List<ItemAsset>();
            zList = Product_Data.Search(Project, Category, string.Empty, string.Empty, string.Empty, string.Empty, AssetID, string.Empty, string.Empty, 0, 0);
            return zList.ToArray();
        }
        [WebMethod]
        public static ItemAsset AssetSelect(int Key, int AssetKey, string Type)
        {
            Trade_Info zTrade = new Trade_Info(Key);
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            //giao dịch cho thuê update => sp lai thanh đa cho thue, nguoc lai la đã bán.
            if (zTrade.TradeCategoryKey == 227)
                zInfo.ItemAsset.StatusKey = "30";
            else
                zInfo.ItemAsset.StatusKey = "29";
            zInfo.UpdateStatus(Type);

            zTrade.AssetType = zInfo.ItemAsset.AssetType;
            zTrade.AssetKey = zInfo.ItemAsset.AssetKey.ToInt();
            zTrade.ProjectKey = zInfo.ItemAsset.ProjectKey.ToInt();
            zTrade.Address = zInfo.ItemAsset.Address;
            float area = 0;
            float.TryParse(zInfo.ItemAsset.AreaWall, out area);
            zTrade.Area = area;
            zTrade.AssetID = zInfo.ItemAsset.AssetID;
            zTrade.AssetCategoryName = zInfo.ItemAsset.CategoryName;
            zTrade.AssetCategory = zInfo.ItemAsset.CategoryKey.ToInt();
            zTrade.Key = Key;
            zTrade.UpdateProduct();

            return zInfo.ItemAsset;
        }
        [WebMethod]
        public static ItemReturn AssetDelete(int Key)
        {
            Trade_Info zTrade = new Trade_Info(Key);

            //xoa sp ra khỏi giao dịch thì update lai là chưa giao dich
            Product_Info zInfo = new Product_Info(zTrade.AssetKey, zTrade.AssetType);
            zInfo.ItemAsset.StatusKey = "31";
            zInfo.UpdateStatus(zTrade.AssetType);

            zTrade.AssetType = "";
            zTrade.AssetKey = 0;
            zTrade.ProjectKey = 0;
            zTrade.Address = "";
            zTrade.Area = 0;
            zTrade.AssetID = "";
            zTrade.AssetCategoryName = "";
            zTrade.AssetCategory = 0;
            zTrade.Key = Key;
            zTrade.UpdateProduct();
            ItemReturn zResult = new ItemReturn();
            zResult.Message = zTrade.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetOwner(int AssetKey, string Type)
        {
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            List<ItemCustomer> zList = zTable.DataTableToList<ItemCustomer>();

            foreach (ItemCustomer item in zList)
            {
                string temp = "";
                if (item.Phone1.Contains("/"))
                    temp = item.Phone1.Split('/')[0];
                else
                    temp = item.Phone1;
                temp = temp.Trim().Replace(" ", "");
                Customer_Info zCustomer = new Customer_Info(temp);
                if (zCustomer.Key > 0)
                {
                    item.CustomerKey = zCustomer.Key.ToString();
                    item.CustomerName = zCustomer.CustomerName;
                    item.Phone1 = zCustomer.Phone1;
                    item.Phone2 = zCustomer.Phone2;
                    item.Address1 = zCustomer.Address1;
                    item.Address2 = zCustomer.Address2;
                    item.CardDate = zCustomer.CardDate.ToDateString();
                    item.CardID = zCustomer.CardID;
                    item.CardPlace = zCustomer.CardPlace;
                    item.Birthday = zCustomer.Birthday.ToDateString();
                }
            }

            return zList.ToArray();
        }
        [WebMethod]
        public static ItemCustomer OneOwner(int OwnerKey, string Type)
        {
            Owner_Info zOwn = new Owner_Info(Type, OwnerKey);
            ItemCustomer zResult = new ItemCustomer();
            Customer_Info zCustomer = new Customer_Info(zOwn.Phone1);
            if (zCustomer.Key > 0)
            {
                zResult.CustomerKey = zCustomer.Key.ToString();
                zResult.CustomerName = zCustomer.CustomerName;
                zResult.Phone1 = zCustomer.Phone1;
                zResult.Phone2 = zCustomer.Phone2;
                zResult.Address1 = zCustomer.Address1;
                zResult.Address2 = zCustomer.Address2;
                zResult.CardDate = zCustomer.CardDate.ToDateString();
                zResult.CardID = zCustomer.CardID;
                zResult.CardPlace = zCustomer.CardPlace;
                zResult.Birthday = zCustomer.Birthday.ToDateString();
            }
            else
            {
                zResult.CustomerName = zCustomer.CustomerName;
                zResult.Phone1 = zOwn.Phone1;
                zResult.Phone2 = zOwn.Phone2;
                zResult.Address1 = zOwn.Address1;
                zResult.Address2 = zOwn.Address2;
            }

            return zResult;
        }
        [WebMethod]
        public static ItemReturn SaveGuest(
            int TradeKey, int OwnerKey, int CustomerKey, int AssetKey, string AssetType,
            string CustomerName, string Phone, string Phone2, string Address, 
            string Address2, string CardDate, string CardID,
            string CardPlace, string Birthday, int EmployeeKey, int IsOwner)
        {
            int Status = 0;// tinh trang khách 277 đã giao dich, 279 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 277;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemReturn zResult = new ItemReturn();

            //luu thong tin khach
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.ID = Phone.Replace(" ", "");
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone.Replace(" ", "");
            zInfo.Phone2 = Phone2.Replace(" ", "");
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.EmployeeKey = EmployeeKey.ToInt();
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.CreatedDate = DateTime.Now;
            zInfo.ModifiedDate = DateTime.Now;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();

            //luu khach giao dich
            Trade_Customer_Info zCustomerTrade = new Trade_Customer_Info(TradeKey, zInfo.Key);
            zCustomerTrade.CustomerKey = zInfo.Key;
            zCustomerTrade.Key = TradeKey;
            zCustomerTrade.IsOwner = IsOwner;
            zCustomerTrade.Save();

            //luu tinh trang quan tam
            Consents_Info zConsent = new Consents_Info(zInfo.Key, true);
            zConsent.CategoryConsent = "277"; //đã giao dịch
            zConsent.Name_Consent = "Đã giao dịch";
            if (zConsent.CustomerKey == 0)
            {
                zConsent.CustomerKey = zInfo.Key;
                zConsent.Create();
            }
            else
            {
                zConsent.Update(zInfo.Key);
            }

            //fixed thêm swtich khi cap nhật/lưu giao dịch thuê
            //trường hợp lập giao dịch thuê - khi cập nhật thì ở trong ký gửi sản phẩm đó bị cập nhật khách thuê thành chủ nhà luôn và mất thông tin chủ nhà
            switch (IsOwner)
            {
                case 3://khachmua thi cap nhat thanh chu nha
                    Owner_Info zOwner = new Owner_Info(AssetType, OwnerKey);
                    zOwner.CustomerName = CustomerName;
                    zOwner.Phone1 = Phone.Replace(" ", "");
                    zOwner.Phone2 = Phone2.Replace(" ", "");
                    zOwner.Address1 = Address;
                    zOwner.Address2 = Address2;
                    zOwner.Update(AssetType);
                    break;

                default:
                    break;
            }
            //cap nhat thong tin chu nha

            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetGuest(int TradeKey)
        {
            DataTable zTable = Trade_Data.GetCustomer(TradeKey);
            List<ItemCustomer> zList = zTable.DataTableToList<ItemCustomer>();
            return zList.ToArray();
        }
        [WebMethod]
        public static ItemReturn GuestDelete(int TradeKey, int CustomerKey)
        {
            ItemReturn zResult = new ItemReturn();
            Trade_Customer_Info zInfo = new Trade_Customer_Info();

            //trả tình trạng khách khi xóa ra khỏi giao dịch
            Consents_Info zConsent = new Consents_Info(CustomerKey, true);
            zConsent.CategoryConsent = "";
            zConsent.Name_Consent = "";
            zConsent.Update(CustomerKey);

            zInfo.Delete(CustomerKey, TradeKey);
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer GetCustomer(int TradeKey, int CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            Trade_Customer_Info zTrade = new Trade_Customer_Info(TradeKey, CustomerKey);
            ItemCustomer zResult = new ItemCustomer();

            zResult.CustomerKey = zInfo.Key.ToString();
            zResult.CustomerName = zInfo.CustomerName;
            zResult.Birthday = zInfo.Birthday.ToDateString();
            zResult.Phone1 = zInfo.Phone1;
            zResult.Phone2 = zInfo.Phone2;
            zResult.Address1 = zInfo.Address1;
            zResult.Address2 = zInfo.Address2;
            zResult.CardID = zInfo.CardID;
            zResult.CardDate = zInfo.CardDate.ToDateString();
            zResult.CardPlace = zInfo.CardPlace;
            zResult.IsOwner = zTrade.IsOwner.ToString();
            zResult.Owner = zTrade.Owner;
            return zResult;
        }

        //************ cap nhat tinh trang khách hàng khi lập giao dịch, chọn hoặc thêm mới ngang, 
        //nếu là chủ nhà => cập nhật tình trang khách là ký gửi đã giao dịch, 
        //nếu là khách mua => cập nhật tình trang khách là đã giao dịch và là chủ nhà mới của căn giao dịch
        //nếu là khách thuê => cập nhật tình trang khách là đã giao dịch 
        [WebMethod]
        public static ItemReturn SaveCustomer(int CustomerKey, int AssetKey, string AssetType, string CustomerName,
            string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int IsOwner)
        {
            int Status = 0;// tinh trang khách 277 đã giao dich, 279 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 277;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemReturn zResult = new ItemReturn();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        //*************
        [WebMethod]
        public static ItemReturn SaveNewCustomer(int TradeKey, int AssetKey, string AssetType, string CustomerName,
            string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int EmployeeKey, int IsOwner)
        {
            int Status = 0;// tinh trang khách 227 đã giao dich, 229 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 279;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemReturn zResult = new ItemReturn();
            Customer_Info zInfo = new Customer_Info();
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            //luu khach giao dich
            Trade_Customer_Info zCustomerTrade = new Trade_Customer_Info(TradeKey, zInfo.Key);
            zCustomerTrade.CustomerKey = zInfo.Key;
            zCustomerTrade.Key = TradeKey;
            zCustomerTrade.IsOwner = IsOwner;
            zCustomerTrade.Save();

            zInfo.Save();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GuestSearch(string Phone, string Name)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
            {
                Employee = 0;
                Department = 0;
            }
            List<ItemCustomer> zList = Customer_Data.Get(string.Empty, string.Empty, string.Empty, Name, Phone, string.Empty, Department, Employee);
            return zList.ToArray();
        }

        [WebMethod]
        public static ItemCustomer CheckPhone(string Phone)
        {
            Customer_Info zInfo = new Customer_Info(Phone);
            ItemCustomer zCustomer = new ItemCustomer();
            zCustomer.CustomerName = zInfo.CustomerName;
            zCustomer.Address2 = zInfo.Address2;
            zCustomer.Address1 = zInfo.Address1;
            zCustomer.Phone1 = zInfo.Phone1;
            zCustomer.Phone2 = zInfo.Phone2;
            zCustomer.Birthday = zInfo.Birthday.ToDateString();
            zCustomer.CardID = zInfo.CardID;
            zCustomer.CardPlace = zInfo.CardPlace;
            zCustomer.CardDate = zInfo.CardDate.ToDateString();
            return zCustomer;
        }
        #endregion

        #region [File & IMG]
        [WebMethod]
        public static ItemReturn DeleteFile(int FileKey)
        {
            ItemReturn zResult = new ItemReturn();
            Document_Info zInfo = new Document_Info();
            zInfo.FileKey = FileKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;

            if (zInfo.Message == string.Empty)
            {
                if (File.Exists(zInfo.ImagePath))
                    File.Delete(zInfo.ImagePath);
            }
            return zResult;
        }
        void UploadFile()
        {
            int zCategoryKey = 135;
            int zProductKey = HID_TradeKey.Value.ToInt();
            string zSQL = "";

            string zPath = "~/Upload/File/Transaction/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileName(zInputFile.FileName);
                    string zFilePath = Server.MapPath(Path.Combine(zPath, zFileName));
                    string zFileUrl = (zPath + zFileName).Substring(1, zPath.Length + zFileName.Length - 1);

                    if (File.Exists(zFilePath))
                        File.Delete(zFilePath);
                    zInputFile.SaveAs(zFilePath);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, Description ,ImagePath, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('Transaction','" + zProductKey + "','" + zCategoryKey + "',N'" + txt_Description1.Text + "',N'" + zFilePath + "',N'" + zFileUrl + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadFolder();
                }
            }
        }
        void UploadImg()
        {
            int zCategoryKey = 130;
            int zProductKey = HID_TradeKey.Value.ToInt();
            string zSQL = "";

            string zPath = "~/Upload/File/Transaction/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                    string zFileThumb = zFileName + "_thumb." + Path.GetExtension(zInputFile.FileName);
                    string zFileLarge = Path.GetFileName(zInputFile.FileName);

                    string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                    string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                    string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                    string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                    if (File.Exists(zFilePathLarge))
                        File.Delete(zFilePathLarge);
                    zInputFile.SaveAs(zFilePathLarge);

                    Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                    System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(150, 150));
                    objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, Description, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('Transaction','" + zProductKey + "','" + zCategoryKey + "', N'" + txt_Description2.Text + "', N'" + zFileLarge + "', N'" + zFileUrlThumb + "', N'" + zFileUrlLarge + "', N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadPicture();
                }
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            UploadFile();
        }
        protected void btnUploadImg_Click(object sender, EventArgs e)
        {
            UploadImg();
        }
        #endregion

        #region [Commission]
        static DataRow AddInManager(DataRow rObject, DataTable IntblSchema, DataTable IntblFee, int PositionKey, int DepartmentKey, int EmployeeKey, string EmployeeName)
        {
            foreach (DataRow rFee in IntblFee.Rows)
            {
                if (PositionKey == rFee["PositionKey"].ToInt())
                {
                    rObject = IntblSchema.NewRow();
                    rObject["Name"] = rFee["Name"];
                    rObject["CommisionKey"] = rFee["AutoKey"];
                    rObject["Money"] = rFee["Money"];
                    rObject["DepartmentKey"] = DepartmentKey;
                    rObject["EmployeeKey"] = EmployeeKey;
                    rObject["EmployeeName"] = EmployeeName;
                    rObject["TradeCategory"] = rFee["TradeCategory"];
                    rObject["TradeType"] = rFee["TradeType"];
                    rObject["Number"] = rFee["Number"];
                    rObject["Method"] = rFee["Method"];
                    return rObject;
                }
            }

            return null;
        }
        static DataRow AddInSupport(DataRow rObject, DataTable IntblSchema, DataRow rFee, int PositionKey, int DepartmentKey, int EmployeeKey, string EmployeeName)
        {
            rObject = IntblSchema.NewRow();
            rObject["Name"] = rFee["Name"].ToString();
            rObject["CommisionKey"] = rFee["AutoKey"];
            rObject["Money"] = rFee["Money"];
            rObject["DepartmentKey"] = DepartmentKey;
            rObject["EmployeeKey"] = EmployeeKey;
            rObject["EmployeeName"] = EmployeeName;
            rObject["TradeCategory"] = rFee["TradeCategory"];
            rObject["TradeType"] = rFee["TradeType"];
            rObject["Number"] = rFee["Number"];
            rObject["Method"] = rFee["Method"];
            return rObject;
        }

        void LoadCommistion()
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string _EmployeeName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            DataTable zTable = Trade_Data.GetSupportCommission(HID_TradeKey.Value.ToInt());
            if (zTable.Rows.Count <= 0)
            {
                #region [InitTable]
                DataTable ztblObject = new DataTable();
                ztblObject.Columns.Add("AutoKey");
                ztblObject.Columns.Add("Name");
                ztblObject.Columns.Add("CommisionKey");
                ztblObject.Columns.Add("EmployeeKey");
                ztblObject.Columns.Add("DepartmentKey");
                ztblObject.Columns.Add("EmployeeName");
                ztblObject.Columns.Add("Money");
                ztblObject.Columns.Add("Number");
                ztblObject.Columns.Add("TradeCategory");
                ztblObject.Columns.Add("Method");
                ztblObject.Columns.Add("TradeType");
                #endregion
                DataRow rObject = null;
                DataTable ztblFee = Commission_Support_Data.Default(DDL_Category.SelectedValue.ToInt(), DDL_Source.SelectedValue.ToInt());

                #region [Cấp quản lý]
                DataTable ztblManager = Employees_Data.GetAllManager(HID_EmployeeKey.Value.ToInt());
                foreach (DataRow r in ztblManager.Rows)
                {
                    int EmployeeKey = r["EmployeeKey"].ToInt();
                    int DepartmentKey = r["DepartmentKey"].ToInt();
                    int ManagerKey = r["ManagerKey"].ToInt();
                    int PositionKey = r["PositionKey"].ToInt();
                    string Name = r["EmployeeName"].ToString();

                    if (EmployeeKey != HID_EmployeeKey.Value.ToInt())
                    {
                        rObject = AddInManager(rObject, ztblObject, ztblFee, PositionKey, DepartmentKey, EmployeeKey, Name);
                        if (rObject != null)
                            ztblObject.Rows.Add(rObject);
                    }
                }
                #endregion

                #region [Phòng hổ trợ chỉ định]
                DataTable ztblSupport = Employees_Data.StaffSupport();
                DataRow rFee = ztblFee.Select("[DepartmentKey]=13")[0];
                foreach (DataRow r in ztblSupport.Rows)
                {
                    int EmployeeKey = r["EmployeeKey"].ToInt();
                    int DepartmentKey = r["DepartmentKey"].ToInt();
                    int ManagerKey = r["ManagerKey"].ToInt();
                    int PositionKey = r["PositionKey"].ToInt();
                    string Name = r["EmployeeName"].ToString();

                    rObject = AddInSupport(rObject, ztblObject, rFee, PositionKey, DepartmentKey, EmployeeKey, Name);
                    if (rObject != null)
                        ztblObject.Rows.Add(rObject);
                }
                #endregion

                #region [AutoInsert]
                string SQL = "";
                foreach (DataRow rInsert in ztblObject.Rows)
                {
                    SQL += @"
INSERT INTO FNC_Transaction_Commision (
TradeKey,CommisionKey,EmployeeKey,DepartmentKey,Money,Number,TradeType,TradeCategory,Method,
CreatedDate,CreatedBy,CreatedName,ModifiedDate,ModifiedBy,ModifiedName)
VALUES ("
+ HID_TradeKey.Value + ","
+ rInsert["CommisionKey"].ToString() + ","
+ rInsert["EmployeeKey"].ToString() + ","
+ rInsert["DepartmentKey"].ToString() + ","
+ rInsert["Money"].ToString() + ","
+ rInsert["Number"].ToString() + ","
+ rInsert["TradeType"].ToString() + ","
+ rInsert["TradeCategory"].ToString() + ","
+ rInsert["Method"].ToString() + " ,GETDATE(),"
+ _Employee + ",N'"
+ _EmployeeName + "',GETDATE(),N'"
+ _EmployeeName + "',"
+ _Employee + ")";
                }

                string Message = CustomInsert.Exe(SQL).Message;
                if (Message != string.Empty)
                {
                    LitTableCommistion.Text = "Lỗi khởi tạo thông tin hoa hồng !." + Message;
                    return;
                }
                #endregion

                zTable = Trade_Data.GetSupportCommission(HID_TradeKey.Value.ToInt());
            }

            LitTableCommistion.Text = HtmlCommistion(zTable);
        }
        string HtmlCommistion(DataTable TableIn)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-bordered table-hover' id='tblCommision'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên phí</th>");
            zSb.AppendLine("        <th>Số tiền</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th></th>");//<a href='#mSupport' data-toggle='modal' id='btnAdd'><i class='ace-icon fa fa-plus green'></i>Thêm</a>
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (TableIn.Rows.Count > 0)
            {
                int n = 1;
                for (int i = 0; i < TableIn.Rows.Count; i++)
                {
                    DataRow r = TableIn.Rows[i];

                    zSb.AppendLine("<tr id=" + r["AutoKey"].ToString() + ">");
                    zSb.AppendLine("<td>" + (n++) + "</td>");
                    zSb.AppendLine("<td>" + r["Name"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("<td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("<td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDel' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-120 orange'></i>&nbsp;Xóa</a></div></td>");
                    zSb.AppendLine("</tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }
        [WebMethod]
        public static string RefeshSupport(int TradeKey, int TradeType, int TradeCategory, int EmployeeKey)
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string _EmployeeName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            Trade_Commision_Info zInfo = new Trade_Commision_Info();
            zInfo.TradeKey = TradeKey;
            zInfo.DeleteSupport();

            #region [InitTable]
            DataTable ztblObject = new DataTable();
            ztblObject.Columns.Add("AutoKey");
            ztblObject.Columns.Add("Name");
            ztblObject.Columns.Add("CommisionKey");
            ztblObject.Columns.Add("EmployeeKey");
            ztblObject.Columns.Add("DepartmentKey");
            ztblObject.Columns.Add("EmployeeName");
            ztblObject.Columns.Add("Money");
            ztblObject.Columns.Add("Number");
            ztblObject.Columns.Add("TradeCategory");
            ztblObject.Columns.Add("Method");
            ztblObject.Columns.Add("TradeType");
            #endregion
            DataRow rObject = null;
            DataTable ztblFee = Commission_Support_Data.Default(TradeCategory, TradeType);

            #region [Cấp quản lý]
            DataTable ztblManager = Employees_Data.GetAllManager(EmployeeKey);
            foreach (DataRow r in ztblManager.Rows)
            {
                int zEmployeeKey = r["EmployeeKey"].ToInt();
                int zDepartmentKey = r["DepartmentKey"].ToInt();
                int zManagerKey = r["ManagerKey"].ToInt();
                int zPositionKey = r["PositionKey"].ToInt();
                string Name = r["EmployeeName"].ToString();

                if (zEmployeeKey != EmployeeKey)
                {
                    rObject = AddInManager(rObject, ztblObject, ztblFee, zPositionKey, zDepartmentKey, zEmployeeKey, Name);
                    if (rObject != null)
                        ztblObject.Rows.Add(rObject);
                }
            }
            #endregion

            #region [Phòng hổ trợ chỉ định]
            DataTable ztblSupport = Employees_Data.StaffSupport();
            DataRow rFee = ztblFee.Select("[DepartmentKey]=13")[0];
            foreach (DataRow r in ztblSupport.Rows)
            {
                int zEmployeeKey = r["EmployeeKey"].ToInt();
                int zDepartmentKey = r["DepartmentKey"].ToInt();
                int zManagerKey = r["ManagerKey"].ToInt();
                int zPositionKey = r["PositionKey"].ToInt();
                string Name = r["EmployeeName"].ToString();

                rObject = AddInSupport(rObject, ztblObject, rFee, zPositionKey, zDepartmentKey, zEmployeeKey, Name);
                if (rObject != null)
                    ztblObject.Rows.Add(rObject);
            }
            #endregion

            #region [AutoInsert]
            string SQL = "";
            foreach (DataRow rInsert in ztblObject.Rows)
            {
                SQL += @"
INSERT INTO FNC_Transaction_Commision (
TradeKey,CommisionKey,EmployeeKey,DepartmentKey,Money,Number,TradeType,TradeCategory,Method,
CreatedDate,CreatedBy,CreatedName,ModifiedDate,ModifiedBy,ModifiedName)
VALUES ("
+ TradeKey + ","
+ rInsert["CommisionKey"].ToString() + ","
+ rInsert["EmployeeKey"].ToString() + ","
+ rInsert["DepartmentKey"].ToString() + ","
+ rInsert["Money"].ToString() + ","
+ rInsert["Number"].ToString() + ","
+ rInsert["TradeType"].ToString() + ","
+ rInsert["TradeCategory"].ToString() + ","
+ rInsert["Method"].ToString() + " ,GETDATE(),"
+ _Employee + ",N'"
+ _EmployeeName + "',GETDATE(),N'"
+ _EmployeeName + "',"
+ _Employee + ")";
            }

            string Message = CustomInsert.Exe(SQL).Message;
            if (Message != string.Empty)
            {
                return "Lỗi khởi tạo thông tin hoa hồng !." + Message;
            }
            #endregion

            DataTable zTable = Trade_Data.GetSupportCommission(TradeKey);
            StringBuilder zSb = new StringBuilder();
            int n = 1;
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow r = zTable.Rows[i];

                zSb.AppendLine("<tr id=" + r["AutoKey"].ToString() + ">");
                zSb.AppendLine("<td>" + (n++) + "</td>");
                zSb.AppendLine("<td>" + r["Name"].ToString() + "</td>");
                zSb.AppendLine("<td>" + r["Money"].ToDoubleString() + "</td>");
                zSb.AppendLine("<td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("<td>" + r["Description"].ToString() + "</td>");
                zSb.AppendLine("<td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDel' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-120 orange'></i>&nbsp;Xóa</a></div></td>");
                zSb.AppendLine("</tr>");
            }

            return zSb.ToString();
        }
        [WebMethod]
        public static string SaveSupport(int AutoKey, int CommissionKey, int TradeKey, int EmployeeKey, string Money, string Description)
        {
            Trade_Commision_Info zInfo = new Trade_Commision_Info(AutoKey);
            zInfo.TradeKey = TradeKey;
            zInfo.CommisionKey = CommissionKey;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.Money = Money.ToDouble();
            zInfo.Description = Description;
            zInfo.Save();

            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return zInfo.Message;
        }
        [WebMethod]
        public static ItemCommission GetSupport(int AutoKey)
        {
            Trade_Commision_Info zInfo = new Trade_Commision_Info(AutoKey);
            ItemCommission zItem = new ItemCommission();
            zItem.CommisionKey = zInfo.CommisionKey;
            zItem.EmployeeKey = zInfo.EmployeeKey;
            zItem.AutoKey = zInfo.AutoKey;
            zItem.Description = zInfo.Description;
            zItem.Money = zInfo.Money.ToString("n0");

            return zItem;
        }

        [WebMethod]
        public static string LoadSupport(int TradeKey)
        {
            DataTable zTable = Trade_Data.GetSupportCommission(TradeKey);
            StringBuilder zSb = new StringBuilder();
            int n = 1;
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow r = zTable.Rows[i];

                zSb.AppendLine("<tr id=" + r["AutoKey"].ToString() + ">");
                zSb.AppendLine("<td>" + (n++) + "</td>");
                zSb.AppendLine("<td>" + r["Name"].ToString() + "</td>");
                zSb.AppendLine("<td>" + r["Money"].ToDoubleString() + "</td>");
                zSb.AppendLine("<td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("<td>" + r["Description"].ToString() + "</td>");
                zSb.AppendLine("<td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDel' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-120 orange'></i>&nbsp;Xóa</a></div></td>");
                zSb.AppendLine("</tr>");
            }

            return zSb.ToString();
        }

        [WebMethod]
        public static string DeleteSupport(int AutoKey)
        {
            Trade_Commision_Info zInfo = new Trade_Commision_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                return zInfo.Message;
            else
                return string.Empty;
        }
        #endregion

        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string _EmployeeName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(_UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey =" + _Department + " ORDER BY LastName", false);
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + _Employee + "ORDER BY LastName", false);
                    break;
            }

            if (_Permitsion[2] == "1")
            {
                Lit_Button.Text = "<a class='btn btn-white btn-info btn-bold' id='btnEdit' href='#'><i class='ace-icon fa fa-save blue'></i>Cập nhật</a>";
            }
            if (_Permitsion[3] == "1")
            {
                Lit_Button.Text += " <a class='btn btn-white btn-warning btn-bold' id='btnDel' href='#'><i class='ace-icon fa fa-trash-o bigger-120 orange'></i>Xóa</a>";
            }
        }
        #endregion
    }
}