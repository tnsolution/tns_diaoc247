﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeList.aspx.cs" Inherits="WebApp.FNC.TradeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <span id="tit">
                    <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal></span><asp:Label ID="lbltime" runat="server" Text="..."></asp:Label>
                <span class="tools pull-right">
                    <a href="#mInitTrade" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
                <span class="pull-right action-buttons">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12" id="divtable">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <!---->
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm giao dịch
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ngày tháng năm</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtFromDate" placeholder="Từ " />
                        </div>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtToDate" placeholder="Đến " />
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="txtName" placeholder="Mã căn" />
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Project" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Dự án--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_CategoryTrade" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Giao dịch--" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="227" Text="--Cho thuê--"></asp:ListItem>
                            <asp:ListItem Value="228" Text="--Chuyển nhượng--"></asp:ListItem>
                            <asp:ListItem Value="230" Text="--Bán mới--"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Status" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tình trạng--" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="--Chưa duyệt--"></asp:ListItem>
                            <asp:ListItem Value="2" Text="--Chưa thanh toán--"></asp:ListItem>
                            <asp:ListItem Value="3" Text="--Đã thu--"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="quanly" style="display: none;">
                        <label>Phòng, nhân viên</label>
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Employee2" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade modal-default" id="mInitTrade" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Thêm thông tin giao dịch</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Người lập giao dịch</label>
                        <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true" required>
                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Loại giao dịch</label>
                        <select id="DDL_Category" class="form-control select2">
                            <option value="0" disabled="disabled" selected="selected">--Chọn--</option>
                            <option value="227">Cho thuê</option>
                            <option value="228">Chuyển nhượng</option>
                            <option value="230">Bán mới</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nguồn giao dịch</label>
                        <select id="DDL_Source" class="form-control select2" required>
                            <option value="0" disabled="disabled" selected="selected">--Chọn--</option>
                            <option value="1">Trực tiếp</option>
                            <option value="2">Liên kết</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ngày ký hợp đồng</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_DateSign" placeholder="Chọn ngày tháng năm" required />
                        </div>
                    </div>
                    <div class="form-group" type="divExpire">
                        <label class="control-label">Ngày hết hợp đồng</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_Expired" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ngày đặt cọc</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" role="datepicker" class="form-control pull-right"
                                id="txt_DatePreOrder" placeholder="Chọn ngày tháng năm" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea rows="7" class="form-control" id="txt_Description" placeholder="Ghi chú"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnInitTrade">Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:HiddenField ID="HID_TradeType" runat="server" Value="3" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txtFromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txtToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            $('.modal.aside').ace_aside();

            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            Date.prototype.ddmmyyyy = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
            };

            $("[id$=txtFromDate]").val(firstDay.ddmmyyyy());
            $("[id$=txtToDate]").val(lastDay.ddmmyyyy());
        });
        jQuery(function ($) {
            var UnitLevel = getUnitLevel();
            if (UnitLevel >= 3) {
                $("#quanly").hide();
            }
            else {
                $("#quanly").show();
            }
            $("#btnSearch").click(function () {
                var status = $("[id$=DDL_Status]").val();
                if (status == 1)
                    $("#tit").text("Giao dịch chưa duyệt");
                if (status == 2)
                    $("#tit").text("Giao dịch chưa thanh toán");
                if (status == 3)
                    $("#tit").text("Giao dịch đã thanh toán");

                var employee = $("[id$=DDL_Employee2]").val();
                var department = $("[id$=DDL_Department]").val();
                var fromdate = $("[id$=txtFromDate]").val();
                var todate = $("[id$=txtToDate]").val();
                var assetid = $("[id$=txtName]").val();
                if (department == null)
                    department = getDepartmentKey();
                $.ajax({
                    type: "POST",
                    url: "/FNC/TradeList.aspx/Search",
                    data: JSON.stringify({
                        "Department": department,
                        "Employee": employee,
                        "FromDate": fromdate,
                        "ToDate": todate,
                        "Project": $("[id$=DDL_Project]").val(),
                        "CategoryTrade": $("[id$=DDL_CategoryTrade]").val(),
                        "TradeType": $("[id$=DDL_Status]").val(),
                        "AssetID": assetid,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#tblData tbody').empty();
                        $('#tblData tbody').append($(msg.d));
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $(".select2").select2({ width: "100%" });
            $("#DDL_Category").on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected == 227)
                    $("[type='divExpire']").show();
                else
                    $("[type='divExpire']").hide();
            });
            $("#tblData tbody tr").on("click", "td:last-child", function (e) {
                e.stopPropagation();
            });
            $("#tblData tbody").on("click", "tr", function () {
                $('.se-pre-con').fadeIn('slow');
                var trid = $(this).closest('tr').attr('id');
                window.location = "/FNC/TradeView.aspx?ID=" + trid;
            });
            $("#btnInitTrade").click(function () {
                if (!validate()) {
                    return false;
                }

                var tran_source = $("[id$=DDL_Source]").val();
                var tran_employee = $("[id$=DDL_Employee]").val();
                var tran_category = $("[id$=DDL_Category]").val();
                var tran_datesign = $("[id$=txt_DateSign]").val();
                var tran_expired = $("[id$=txt_Expired]").val();
                var tran_datepre = $("[id$=txt_DatePreOrder]").val();
                var tran_description = $("[id$=txt_Description]").val();
                if (tran_employee == null)
                    tran_employee = getEmployeeKey();
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeEdit.aspx/InitTrade',
                    data: JSON.stringify({
                        "TradeCategory": tran_category,
                        "DateSign": tran_datesign,
                        "DateExpired": tran_expired,
                        "DatePre": tran_datepre,
                        "Description": tran_description,
                        "Employee": tran_employee,
                        "Source": tran_source,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            window.location = "/FNC/TradeEdit.aspx?ID=" + r.d.Result;
                        else {
                            $('.se-pre-con').fadeIn('slow');
                            Page.showPopupMessage("Lỗi khởi tạo thông tin", r.d.Message);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                        //alert('Lỗi khởi tạo dữ liệu !');
                        //$('.se-pre-con').fadeOut('slow');
                    },
                    complete: function () {

                    }
                });
            });
            $("#ViewPrevious").click(function () {
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
        });
        function validate() {
            $('input[required]').each(function (idx, item) {
                Page.checkError($(item));
            });
            $('select[required]').each(function (idx, item) {
                Page.checkSelect($(item));
            });
            if ($('div.error').length > 0) {
                return false;
            }
            return true;
        }
        function getDepartmentKey() {
            cookieList = document.cookie.split('; ');
            cookies = {};
            for (i = cookieList.length - 1; i >= 0; i--) {
                var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
                if (cName == 'UserLog') {
                    var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                    cookies = val.split('&')[1].split('=');
                    return cookies[1];
                }
            }
        }
        function deleteInput(id) {
            if (confirm("Bạn có chắc xóa ?")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TradeList.aspx/Delete',
                    data: JSON.stringify({
                        'Key': id,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d == '')
                            $("#tblData tbody tr[id='" + id + "']").remove();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
