﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TradeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                if (Request["Type"] != null)
                    HID_TradeType.Value = Request["Type"];

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");

                CheckRole();
                LoadData();
            }
        }
        void LoadData()
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            DataTable zTable = new DataTable();
            if (Session["SearchTrade"] != null)
            {
                Item_Trade_Search zSession = (Item_Trade_Search)Session["SearchTrade"];
                zTable = Trade_Data.Get(zSession.Department, zSession.Employee, zSession.TradeType,
                    Tools.ConvertToDate(zSession.FromDate), Tools.ConvertToDate(zSession.ToDate),
                    zSession.Project, zSession.CategoryTrade, zSession.AssetID);
            }
            else
            {
                DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
                DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

                int TradeType = HID_TradeType.Value.ToInt();

                if (TradeType == 1)
                {
                    Lit_TitlePage.Text = "Các giao dịch chưa duyệt";
                }
                if (TradeType == 2)
                {
                    Lit_TitlePage.Text = "Các giao dịch chưa thanh toán";
                }
                if (TradeType == 3 ||
                    TradeType == 0)
                {
                    Lit_TitlePage.Text = "Các giao dịch";
                }
                DDL_Status.SelectedValue = TradeType.ToString();
                switch (_UnitLevel)
                {
                    case 0:
                    case 1:
                        zTable = Trade_Data.Get(0, 0, TradeType, startDate, endDate);
                        break;

                    case 2:
                        zTable = Trade_Data.Get(_Department, 0, TradeType, startDate, endDate);
                        break;

                    default:
                        zTable = Trade_Data.Get(_Department, _Employee, TradeType, startDate, endDate);
                        break;
                }
            }
            ViewHtml(zTable);
        }
        void LoadData(DateTime startDate, DateTime endDate)
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            int TradeType = HID_TradeType.Value.ToInt();

            if (TradeType == 1)
            {
                Lit_TitlePage.Text = "Các giao dịch chưa duyệt";
            }
            if (TradeType == 2)
            {
                Lit_TitlePage.Text = "Các giao dịch chưa thanh toán";
            }
            if (TradeType == 3 || TradeType == 0)
            {
                Lit_TitlePage.Text = "Các giao dịch";
            }
            lbltime.Text = " Từ ngày: " + startDate.ToString("dd/MM/yyyy") + " đến ngày: " + endDate.ToString("dd/MM/yyyy");
            DDL_Status.SelectedValue = TradeType.ToString();
            DataTable zTable = new DataTable();
            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    zTable = Trade_Data.Get(0, 0, TradeType, startDate, endDate);
                    break;

                case 2:
                    zTable = Trade_Data.Get(_Department, 0, TradeType, startDate, endDate);
                    break;

                default:
                    zTable = Trade_Data.Get(_Department, _Employee, TradeType, startDate, endDate);
                    break;
            }

            ViewHtml(zTable);
        }
        [WebMethod(EnableSession = true)]
        public static string Search(int Department, int Employee, string FromDate, string ToDate, int Project, int CategoryTrade, int TradeType, string AssetID)
        {
            DataTable Table = Trade_Data.Get(Department, Employee, TradeType, Tools.ConvertToDate(FromDate), Tools.ConvertToDate(ToDate), Project, CategoryTrade, AssetID);
            StringBuilder zSb = new StringBuilder();

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    if (r["IsApproved"].ToInt() == 0 && _Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TransactionKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    else
                        zSb.AppendLine("               <td class='notclick'></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td>");
                zSb.AppendLine("            <td colspan='11'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            Item_Trade_Search ISearch = new Item_Trade_Search();
            ISearch.Department = Department;
            ISearch.Employee = Employee;
            ISearch.FromDate = FromDate;
            ISearch.ToDate = ToDate;
            ISearch.Project = Project;
            ISearch.CategoryTrade = CategoryTrade;
            ISearch.TradeType = TradeType;
            ISearch.AssetID = AssetID;
            HttpContext.Current.Session.Add("SearchTrade", ISearch);

            return zSb.ToString();
        }
        void ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày lập GD</th>");
            zSb.AppendLine("        <th>Ngày ký hợp đồng</th>");
            zSb.AppendLine("        <th>Tên dự án</th>");
            zSb.AppendLine("        <th>Mã căn hộ</th>");
            zSb.AppendLine("        <th>Loại căn hộ</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Giao dịch</th>");
            zSb.AppendLine("        <th>Doanh thu</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    if (r["IsApproved"].ToInt() == 0 && _Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TransactionKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    else
                        zSb.AppendLine("               <td></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td>");
                zSb.AppendLine("            <td colspan='11'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            LoadData(FromDate, ToDate);
        }

        [WebMethod]
        public static string Delete(int Key)
        {
            Trade_Info zInfo = new Trade_Info();
            zInfo.Key = Key;
            zInfo.Delete();
            return zInfo.Message;
        }

        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(_UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Employee2.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + _Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + _Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + _Department + " ORDER BY DepartmentName", false);

                    DDL_Department.SelectedValue = _Department.ToString();
                    DDL_Employee.SelectedValue = _Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Employee2.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + _Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + _Employee + " AND IsWorking=2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = _Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    DDL_Employee2.Visible = true;
                    break;
            }
        }
    }
}