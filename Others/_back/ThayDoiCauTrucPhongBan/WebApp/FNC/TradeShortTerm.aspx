﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeShortTerm.aspx.cs" Inherits="WebApp.FNC.TradeShortTerm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Các giao dịch ngắn hạn đạt chỉ số chưa cài đặt hoa hồng
                <span class="tools pull-right">
                    <a href="#mInitTrade" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Hoa hồng
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12" id="divtable">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm giao dịch
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ngày tháng năm</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtFromDate" placeholder="Từ " />
                        </div>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtToDate" placeholder="Đến " />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Project" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Dự án--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="quanly">
                        <label>Phòng, nhân viên</label>
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Employee2" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade modal-default" id="mInitTrade" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Cài đặt hoa hồng giao dịch ngắn hạn cho nhân viên ?.</h4>
                </div>
                <div class="modal-body">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fa fa-dollar orange"></i>
                                Hoa hồng trực tiếp
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn nhân viên--"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                id="txt_DateSetup" placeholder="Chọn ngày tính hoa hồng" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fa fa-dollar orange"></i>
                                Hoa hồng hổ trợ                                      
                            </h4>
                            <div class="widget-toolbar action-buttons">
                                <a id="btnRefeshSupport" href="#" class="pink">
                                    <i class="ace-icon fa fa-refresh bigger-125"></i>
                                    Làm mới
                                </a>
                                <a id="btnAddSupport" href="#" class="pink">
                                    <i class="ace-icon fa fa-plus bigger-125"></i>
                                    Thêm
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div class="row">
                                    <asp:Literal ID="LitTableCommistion" runat="server"></asp:Literal>
                                    <div class="col-sm-4">
                                        <input type="text" runat="server" class="form-control" id="txt_Fee" placeholder="Chi phí nội bộ" moneyinput />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control limited" id="txt_Description" placeholder="Ghi chú" maxlength="500" style="margin-top: 0px; margin-bottom: 0px; height: 60px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnInitTrade">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txtFromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txtToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            $('.modal.aside').ace_aside();

            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            Date.prototype.ddmmyyyy = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
            };

            $("[id$=txtFromDate]").val(firstDay.ddmmyyyy());
            $("[id$=txtToDate]").val(lastDay.ddmmyyyy());

            $("#btnSearch").click(function () {
                var employee = $("[id$=DDL_Employee2]").val();
                var department = $("[id$=DDL_Department]").val();
                var fromdate = $("[id$=txtFromDate]").val();
                var todate = $("[id$=txtToDate]").val();
                var assetid = $("[id$=txtName]").val();
                if (department == null)
                    department = getDepartmentKey();

                $.ajax({
                    type: "POST",
                    url: "/FNC/TradeShortTerm.aspx/Search",
                    data: JSON.stringify({
                        "Department": department,
                        "Employee": employee,
                        "FromDate": fromdate,
                        "ToDate": todate,
                        "Project": $("[id$=DDL_Project]").val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#tblData').empty();
                        $('#tblData').append($(msg.d));
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $(".select2").select2({ width: "100%" });
        });

    </script>
</asp:Content>
