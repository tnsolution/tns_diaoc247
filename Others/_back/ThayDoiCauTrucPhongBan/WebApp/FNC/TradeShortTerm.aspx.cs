﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TradeShortTerm : System.Web.UI.Page
    {
        static int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
        static int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
        static int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
        static string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);            
            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);

            CheckRole();
            LoadData();
        }

        void LoadData()
        {
            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            DataTable zTable = new DataTable();
            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    zTable = Trade_Data.GetShortTerm(0, 0, 0, startDate, endDate);
                    break;

                case 2:
                    zTable = Trade_Data.GetShortTerm(_Department, 0, 0, startDate, endDate);
                    break;

                default:
                    zTable = Trade_Data.GetShortTerm(_Department, _Employee, 0, startDate, endDate);
                    break;
            }

            Literal_Table.Text = ViewHtml(zTable);
        }
        [WebMethod]
        public static string Search(int Department, int Employee, string FromDate, string ToDate, int Project)
        {
            DataTable Table = Trade_Data.GetShortTerm(Department, Employee, 0, Tools.ConvertToDate(FromDate), Tools.ConvertToDate(ToDate), Project, 0, "");
            StringBuilder zSb = new StringBuilder();

            if (Table.Rows.Count > 0)
            {
                return ViewHtml(Table);
            }         

            return zSb.ToString();
        }
        static string ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày lập GD</th>");
            zSb.AppendLine("        <th>Ngày ký hợp đồng</th>");
            zSb.AppendLine("        <th>Tên dự án</th>");
            zSb.AppendLine("        <th>Mã căn hộ</th>");
            zSb.AppendLine("        <th>Loại căn hộ</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Giao dịch</th>");
            zSb.AppendLine("        <th>Doanh thu</th>");
            zSb.AppendLine("        <th>Số tháng</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["HireMonth"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }           

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {

        }
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(_UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee2.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + _Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + _Department + " ORDER BY DepartmentName", false);

                    DDL_Department.SelectedValue = _Department.ToString();

                    DDL_Employee2.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:

                    Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + _Employee + " AND IsWorking=2 ORDER BY LastName", false);

                    DDL_Department.Visible = false;
                    DDL_Employee2.Visible = true;
                    break;
            }
        }
    }
}