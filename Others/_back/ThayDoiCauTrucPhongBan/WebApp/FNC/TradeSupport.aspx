﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TradeSupport.aspx.cs" Inherits="WebApp.FNC.TradeSupport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Hổ trợ giao dịch
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6" id="divtable">
                <asp:Literal ID="LitTableTrade" runat="server"></asp:Literal>
            </div>
            <div class="col-xs-6" id="right">           
                <asp:Literal ID="LitTableSupport" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
