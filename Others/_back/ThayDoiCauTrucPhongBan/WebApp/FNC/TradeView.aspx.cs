﻿using FindAndReplace;
using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

//KPI tham số categoryplan = 298 là duyet giao dich, 295 là thu tiền giao dich
//category cap nhat san pham la 307

namespace WebApp.FNC
{
    public partial class TradeView : System.Web.UI.Page
    {
        const int _Category1 = 298;//duyet giao dich
        const int _Category2 = 295;//hoan tat giao dich
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_TradeKey.Value = Request["ID"];

                LoadData();
                LoadCustomer();
                LoadFile();
                LoadImage();
                CheckRole();
            }
        }

        void LoadData()
        {
            Trade_Info zInfo = new Trade_Info(HID_TradeKey.Value.ToInt());
            txt_CreatedBy.Text = zInfo.EmployeeName;
            txt_TradeCategory.Text = zInfo.TradeCategoryName;
            txt_DatePreOrder.Text = zInfo.DateDeposit.ToDateString();
            txt_DateTrade.Text = zInfo.TransactionDate.ToDateString();
            txt_DateSign.Text = zInfo.DateContract.ToDateString();
            txt_DateExpired.Text = zInfo.DateContractEnd.ToDateString();
            txt_PriceVAT.Text = zInfo.AmountVAT.ToDoubleString();
            txt_NotVAT.Text = zInfo.AmountVAT.ToDoubleString();
            txt_Vat.Text = zInfo.VAT.ToString();
            txt_OtherFee.Text = zInfo.OtherFee.ToDoubleString();
            txt_Commision.Text = zInfo.TransactionFee.ToDoubleString();
            txt_Income.Text = zInfo.Income.ToDoubleString();
            txt_Description.Text = zInfo.Description;

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            StringBuilder zSb = new StringBuilder();
            if (zInfo.IsApproved == 1)
            {
                txt_Approve.Text = "Đã duyệt." + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a class='red' href='#' id='cancelApprove'><i class='ace-icon fa fa-trash-o bigger-130'></i>Hủy</a></div>";
                txt_ApproveBy.Text = zInfo.EmployeeApproved;
                txt_ApproveDate.Text = zInfo.DateApprove.ToDateTimeString();

                if (zInfo.IsFinish == 1)
                {
                    txt_Finish.Text = "Đã hoàn thành." + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a class='red' href='#' id='cancelFinish'><i class='ace-icon fa fa-trash-o bigger-130'></i>Hủy</a></div>";
                    txt_FinishBy.Text = zInfo.EmployeeFinished;
                    txt_FinishDate.Text = zInfo.FinishDate.ToDateTimeString();
                }
                else
                {
                    txt_Finish.Text = "Chưa hoàn thành.";
                    txt_FinishBy.Text = "";
                    txt_FinishDate.Text = "";
                    if (UnitLevel <= 1)
                        zSb.AppendLine("<a href='#mFinish' data-toggle='modal' class='btn btn-white btn-default btn-bold'><i class='ace-icon fa fa-dollar blue'></i>Duyệt hoàn thành</a>");
                }
            }
            else
            {
                txt_Approve.Text = "Chưa duyệt.";
                txt_ApproveBy.Text = "";
                txt_ApproveDate.Text = "";
                if (UnitLevel <= 2)
                    zSb.AppendLine("<a href='#mApprove' data-toggle='modal' class='btn btn-white btn-default btn-bold'><i class='ace-icon fa fa-check blue'></i>Duyệt giao dịch</a>");
            }
            Lit_Button.Text = zSb.ToString();

            zSb = new StringBuilder();
            zSb.AppendLine(@"<table class='table' id='tblProduct'><thead><tr><th>#</th><th>Dự án</th><th>Mã sản phẩm</th><th>Loại sản phẩm</th><th>Diện tích m<sup>2</sup></th><th>Địa chỉ</th></tr></thead><tbody><tr><td>" + 1 + @"</td><td>" + zInfo.ProjectName + @"</td><td>" + zInfo.AssetID + @"</td><td>" + zInfo.AssetCategoryName + @"</td><td>" + zInfo.Area + @"</td><td>" + zInfo.AddressProject + @"</td></tr></tbody></table>");

            Lit_TableProduct.Text = zSb.ToString();
        }
        void LoadCustomer()
        {
            DataTable zTable = Trade_Data.GetCustomer(HID_TradeKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table' id='tblCustomer'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th>#</th>");
            zSb.AppendLine("            <th>Họ tên</th>");
            zSb.AppendLine("            <th>SĐT</th>");
            zSb.AppendLine("            <th>Ngày sinh</th>");
            zSb.AppendLine("            <th>CMND</th>");
            zSb.AppendLine("            <th>Địa chỉ</th>");
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int o = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("        <tr id='" + r["CustomerKey"].ToString() + "' style='cursor:pointer'>");
                    zSb.AppendLine("            <td>" + (o++) + "</td>");
                    zSb.AppendLine("            <td>" + r["CustomerName"].ToString() + "<br/>" + r["Owner"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Phone1"].ToString() + " <br />" + r["Phone2"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Birthday"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>CMND: " + r["CardID"].ToString() + "<br />Nơi cấp: " + r["CardPlace"].ToString() + " </td>");
                    zSb.AppendLine("            <td>Liên hệ:" + r["Address1"].ToString() + "<br />Thường trú: " + r["Address2"].ToString() + "</td>");
                    zSb.AppendLine("        </tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("              <td>#</td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_TableCustomer.Text = zSb.ToString();
        }
        void LoadFile()
        {
            List<ItemDocument> zList = Document_Data.List(135, HID_TradeKey.Value.ToInt(), "Transaction");
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblFile'>");
            zSb.AppendLine("  <thead>");
            zSb.AppendLine("     <tr>");
            zSb.AppendLine("         <th>#</th>");
            zSb.AppendLine("         <th>Tập tin</th>");
            zSb.AppendLine("         <th>Diễn giải</th>");
            zSb.AppendLine("         <th>Ngày cập nhật</th>");
            zSb.AppendLine("         <th>Người cập nhật</th>");
            zSb.AppendLine("     </tr>");
            zSb.AppendLine(" </thead>");
            zSb.AppendLine(" <tbody>");
            int i = 1;
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td>" + item.Description + "</td><td>" + item.ModifiedDate.ToDateTimeString() + "</td><td>" + item.ModifiedName + "</td></tr>");
            }
            zSb.AppendLine(" </tbody>");
            zSb.AppendLine(" </table>");
            Lit_ListFolder.Text = zSb.ToString();
        }
        void LoadImage()
        {
            List<ItemDocument> zList = Document_Data.List(130, HID_TradeKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine(@"<a class='' href='" + item.ImageUrl + "' title=''><img width=100 height=70 src='" + item.ImageThumb.ToThumb() + "'></a>");

                    //zSb.AppendLine("<li id='" + item.FileKey + "'>");
                    //zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                    //zSb.AppendLine("        <img width='150' alt='150x150' src='" + item.ImageThumb.ToThumb() + "' />");
                    //zSb.AppendLine("            <div class='text'>");
                    //zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                    //zSb.AppendLine("            </div>");
                    //zSb.AppendLine("    </a>");
                    //zSb.AppendLine("</li>");
                }
            }
            Lit_ListImage.Text = zSb.ToString();
        }

        [WebMethod]
        public static string SaveApprove(int TradeKey, string DateApprove)
        {
            int IsApproved;
            Trade_Info zInfo = new Trade_Info(TradeKey);
            if (zInfo.IsApproved == 0)
                IsApproved = 1;
            else
                IsApproved = 0;

            zInfo.IsApproved = IsApproved;
            zInfo.DateApprove = Tools.ConvertToDateTime(DateApprove);
            zInfo.ApprovedBy = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);
            zInfo.Approve();

            if (zInfo.Message == string.Empty)
            {
                int EmployeeKey = zInfo.EmployeeKey;
                int DepartmentKey = zInfo.DepartmentKey;
                string Mess = SaveKPI(TradeKey, EmployeeKey, DepartmentKey, "Duyệt giao dịch: " + DateApprove, _Category1);

                return zInfo.Key.ToString();
            }
            else
                return "Lỗi không lưu được ! " + zInfo.Message;
        }
        [WebMethod]
        public static string SaveFinish(int TradeKey, string DateFinish)
        {
            int IsFinish;
            Trade_Info zInfo = new Trade_Info(TradeKey);
            if (zInfo.IsFinish == 0)
                IsFinish = 1;
            else
                IsFinish = 0;

            zInfo.IsFinish = IsFinish;
            zInfo.FinishDate = Tools.ConvertToDateTime(DateFinish);
            zInfo.FinishBy = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);
            zInfo.Finish();

            if (zInfo.Message == string.Empty)
            {
                int EmployeeKey = zInfo.EmployeeKey;
                int DepartmentKey = zInfo.DepartmentKey;
                string Mess = SaveKPI(TradeKey, EmployeeKey, DepartmentKey, "Hoàn tất giao dịch: " + DateFinish, _Category2);

                return zInfo.Key.ToString();
            }
            else
                return "Lỗi không lưu được ! " + zInfo.Message;
        }

        #region [KPI]
        public static string SaveKPI(int TradeKey, int EmployeeKey, int DepartmentKey, string Description, int Category)
        {
            string MessageKPI = Helper.KPI(TradeKey, EmployeeKey, DepartmentKey, DateTime.Now, Description, 0, Category, 3);
            return MessageKPI;
        }
        #endregion


        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            StringBuilder zSb = new StringBuilder();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                zSb.AppendLine("<a href='/FNC/TradeEdit.aspx?ID=" + HID_TradeKey.Value + @"' class='btn btn-white btn-warning btn-bold' id='btnEdit'><i class='ace-icon fa fa-pencil orange'></i>Chỉnh sửa</a>");
            }

            Lit_Button.Text += zSb.ToString();
        }

        #region [Process file]
        void ProcessFile()
        {
            Trade_Info zInfo = new Trade_Info(HID_TradeKey.Value.ToInt());
            DataTable zTable = Trade_Data.GetCustomer(HID_TradeKey.Value.ToInt());
            ItemCustomer zCustomerA = new ItemCustomer();
            ItemCustomer zCustomerB = new ItemCustomer();
            foreach (DataRow r in zTable.Rows)
            {
                if (r["IsOwner"].ToInt() == 1)
                {
                    zCustomerA.CustomerName = r["CustomerName"].ToString();
                    zCustomerA.CardID = r["CardID"].ToString();
                    zCustomerA.CardDate = r["CardDate"].ToString();
                    zCustomerA.Birthday = r["Birthday"].ToString();
                    zCustomerA.CardPlace = r["CardPlace"].ToString();
                    zCustomerA.Address1 = r["Address1"].ToString();
                    zCustomerA.Phone1 = r["Phone1"].ToString();
                }
                else
                {
                    zCustomerB.CustomerName = r["CustomerName"].ToString();
                    zCustomerB.CardID = r["CardID"].ToString();
                    zCustomerB.CardDate = r["CardDate"].ToString();
                    zCustomerB.Birthday = r["Birthday"].ToString();
                    zCustomerB.CardPlace = r["CardPlace"].ToString();
                    zCustomerB.Address1 = r["Address1"].ToString();
                    zCustomerB.Phone1 = r["Phone1"].ToString();
                }
            }

            string ngaycoc = zInfo.DateDeposit.Day.ToString();
            string thangcoc = zInfo.DateDeposit.Month.ToString();
            string namcoc = zInfo.DateDeposit.Year.ToString();

            string inputpath = Server.MapPath("") + "\\BM004 Input.docx";
            string outputpath = Server.MapPath("") + "\\BM004 Output.docx";

            //// Copy Word document.
            File.Copy(inputpath, outputpath, true);

            // Open copied document.
            using (var flatDocument = new FlatDocument(outputpath))
            {
                // Search and replace document's text content.
                flatDocument.FindAndReplace("#ngaycoc", ngaycoc);
                flatDocument.FindAndReplace("#thangcoc", thangcoc);
                flatDocument.FindAndReplace("#namcoc", namcoc);

                flatDocument.FindAndReplace("#hotenA", zCustomerA.CustomerName);
                flatDocument.FindAndReplace("#cmndA", zCustomerA.CardID);
                flatDocument.FindAndReplace("#ngaycapA", zCustomerA.CardDate.ToDateString());
                flatDocument.FindAndReplace("#noicapA", zCustomerA.CardPlace);
                flatDocument.FindAndReplace("#diachiA", zCustomerA.Address1);
                flatDocument.FindAndReplace("#dienthoaiA", zCustomerA.Phone1);

                flatDocument.FindAndReplace("#hotenB", zCustomerB.CustomerName);
                flatDocument.FindAndReplace("#cmndB", zCustomerB.CardID);
                flatDocument.FindAndReplace("#ngaycapB", zCustomerB.CardDate.ToDateString());
                flatDocument.FindAndReplace("#noicapB", zCustomerB.CardPlace);
                flatDocument.FindAndReplace("#diachiB", zCustomerB.Address1);
                flatDocument.FindAndReplace("#dienthoaiB", zCustomerB.Phone1);

                // Save document on Dispose.
            }

            //Process.Start(outputpath);

            //DirectoryInfo directory = new DirectoryInfo(Server.MapPath("/Upload/"));
            //int counter = 0;
            //foreach (FileInfo file in directory.GetFiles())
            //{
            //    HyperLink link = new HyperLink();
            //    link.ID = "Link" + counter++;
            //    link.Text = file.Name;
            //    link.NavigateUrl = "Default2.aspx?name=" + file.Name;

            //    Page.Controls.Add(link);
            //    Page.Controls.Add(new LiteralControl("<br/>"));
            //}
        }
        #endregion

        protected void btnXuly_Click(object sender, EventArgs e)
        {
            ProcessFile();
        }
    }
}