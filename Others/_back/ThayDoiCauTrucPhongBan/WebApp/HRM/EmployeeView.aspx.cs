﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class EmployeeView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckRoles();

            if (Request["ID"] != null)
                HID_EmployeeKey.Value = Request["ID"];

            if (HID_EmployeeKey.Value != string.Empty &&
                HID_EmployeeKey.Value != "0")
            {
                Employees_Info zInfo = new Employees_Info(HID_EmployeeKey.Value.ToInt());
                txt_EmployeeName.Text = zInfo.LastName + " " + zInfo.FirstName;
                DDL_Position.Text = zInfo.Position;
                DDL_Department.Text = zInfo.DepartmentName;
                DDL_Manager.Text = zInfo.ManagerName;
                DDL_IsWorking.Text = zInfo.IsWorking == 2 ? "Đang làm việc" : "Đã nghĩ việc";
                DDL_Gender.Text = zInfo.Gender == 1 ? "Nam" : "Nữ";
                txt_Unit.Text = zInfo.UnitDescription;
                txt_Class.Text = zInfo.Class;
                txt_Birthday.Text = zInfo.Birthday.ToString("dd/MM/yyyy");
                txt_IDCard.Text = zInfo.IDCard;
                txt_IDDate.Text = zInfo.IDDate.ToString("dd/MM/yyyy");
                txt_IDPlace.Text = zInfo.IDPlace;
                txt_TaxNumber.Text = zInfo.TaxNumber;
                txt_DateEnd.Text = zInfo.DateEnd.ToString("dd/MM/yyyy");
                txt_DateStart.Text = zInfo.DateStart.ToString("dd/MM/yyyy");
                txt_Address1.Text = zInfo.Address1;
                txt_Address2.Text = zInfo.Address2;
                txt_CarNumber.Text = zInfo.CarNumber;
                txt_Phone1.Text = zInfo.Phone1;
                txt_Email1.Text = zInfo.Email1;
                User_Info zUser = new User_Info(zInfo.Key);
                txt_User.Text = zUser.Name;
                txt_Pass.Text = "**********";

                if (zInfo.ImageThumb.Length > 0)
                {
                    Lit_Image.Text = @"
                                                <label class='ace-file-input ace-file-multiple'>
                                                    <input type='file' id='myFileImage' name='myFileImage' accept='image/*' />
                                                        <span class='ace-file-container' data-title='Change avatar' style='display: none'>
                                                            <span class='ace-file-name' data-title='No File ...'><i class=' ace-icon ace-icon fa fa-picture-o'></i></span>
                                                        </span>
                                                    <a class='remove' href='#'><i class=' ace-icon fa fa-times'></i></a>                  
                                                    <img id='preview' src='" + zInfo.ImageThumb + @"' alt='your image' style='display: block; cursor:pointer' />                              
                                                </label>";
                }
                else
                {
                    Lit_Image.Text = @"
                                                <label class='ace-file-input ace-file-multiple'>
                                                    <input type='file' id='myFileImage' name='myFileImage' accept='image/*' />
                                                        <span class='ace-file-container' data-title='Change avatar'>
                                                            <span class='ace-file-name' data-title='No File ...'><i class=' ace-icon ace-icon fa fa-picture-o'></i></span>
                                                        </span>
                                                    <a class='remove' href='#'><i class=' ace-icon fa fa-times'></i></a>       
                                                    <img id='preview' src='#' alt='your image' style='display: none' style='cursor:pointer' />                                             
                                                </label>";
                }

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();
            }
        }

        protected void btnUploadImage_Click(object sender, EventArgs e)
        {
            string zPath = "~/Upload/File/Employee/" + HID_EmployeeKey.Value.ToInt() + "/";

            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    try
                    {
                        string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                        string zFileThumb = zFileName + "_thumb" + Path.GetExtension(zInputFile.FileName);
                        string zFileLarge = Path.GetFileName(zInputFile.FileName);

                        string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                        string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                        string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                        string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                        if (System.IO.File.Exists(zFilePathLarge))
                            System.IO.File.Delete(zFilePathLarge);
                        zInputFile.SaveAs(zFilePathLarge);

                        Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                        System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(50, 50));
                        objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                        Employees_Info zEmployee = new Employees_Info();
                        zEmployee.Key = HID_EmployeeKey.Value.ToInt();
                        zEmployee.ImageLarge = zFileUrlLarge;
                        zEmployee.ImageThumb = zFileUrlThumb;

                        zEmployee.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                        zEmployee.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

                        zEmployee.UpdateImage();

                        Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Chỉnh sửa
                        </button>";
            }
        }
        #endregion                
    }
}