﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfoWebEdit.aspx.cs" Inherits="WebApp.HRM.InfoWebEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sàn</label>
                        <div class="col-sm-5">
                            <asp:DropDownList ID="DDLSan" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tiêu đề</label>
                        <div class="col-sm-5">
                            <input type="text" runat="server" class="form-control" id="txt_ArticleName" placeholder="..." required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-5">
                            <input type="text" runat="server" class="form-control" id="txt_Url" placeholder="..." />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">HotLine</label>
                        <div class="col-sm-5">
                            <input type="text" runat="server" class="form-control" id="txtHotLine" placeholder="..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(function () {
            $("#btnSave").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/HRM/InfoWebEdit.aspx/Save",
                    data: JSON.stringify({
                        "AutoKey": $('[id$=HID_AutoKey]').val(),
                        "CategoryKey": $('[id$=DDLSan]').val(),
                        "ArticleName": $('[id$=txt_ArticleName]').val(),
                        "Url": $('[id$=txt_Url]').val(),
                        "HotLine": $("[id$=txtHotLine]").val()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#btnSave").attr("disabled", "disabled");
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d != "") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            window.location.href = document.referrer;
                        }
                    },
                    complete: function () {
                        $("#btnSave").removeAttr("disabled");
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>
</asp:Content>
