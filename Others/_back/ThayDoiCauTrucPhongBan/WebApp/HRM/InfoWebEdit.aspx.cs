﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Web.Services;

namespace WebApp.HRM
{
    public partial class InfoWebEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDLSan, "SELECT CategoryKey, CategoryName FROM HRM_Articles_Categories", false);

                if (Request["ID"] != null)
                    HID_AutoKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Articles_Info zInfo = new Articles_Info(HID_AutoKey.Value.ToInt());
            txt_ArticleName.Value = zInfo.ArticleName;
            txt_Url.Value = zInfo.Url;
            DDLSan.SelectedValue = zInfo.CategoryKey.ToString();
            txtHotLine.Value = zInfo.ArticleID.Trim();
        }

        [WebMethod]
        public static string Save(int AutoKey, int CategoryKey, string ArticleName, string Url, string HotLine)
        {
            Articles_Info zInfo = new Articles_Info(AutoKey);
            zInfo.ArticleName = ArticleName.Trim();
            zInfo.CategoryKey = CategoryKey;
            zInfo.Url = Url.Trim();
            zInfo.ArticleID = HotLine;
            zInfo.Save();

            return zInfo.Message;
        }
    }
}