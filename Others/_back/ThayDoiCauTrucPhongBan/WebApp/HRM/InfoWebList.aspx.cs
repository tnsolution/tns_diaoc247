﻿using Lib.HRM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class InfoWebList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Articles_Data.List();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Sàn</th>");
            zSb.AppendLine("        <th>Dự án</th>");
            zSb.AppendLine("        <th>Link</th>");
            //zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["ArticleKey"].ToString() + "'>");
                    zSb.AppendLine("                <td>" + (i++) + "</td>");
                    zSb.AppendLine("                <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["ArticleName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Url"].ToString() + "</td>");
                    //zSb.AppendLine("                <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='delete(" + r["ArticleKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td></td><td colspan='4'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
    }
}