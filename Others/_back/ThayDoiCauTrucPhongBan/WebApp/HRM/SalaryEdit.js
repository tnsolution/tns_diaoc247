﻿
$(function () {
    $("[id$=DDL_Employee]").change(function () {
        var d = new Date(),
            n = d.getMonth() + 1,
            y = d.getFullYear();
        var key = this.value;
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/CheckEmployee',
            data: JSON.stringify({
                "sEmployee": getEmployeeKey(),
                "Month": n,
                "Year": y,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                //$('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d != "0") {
                    if (confirm("Nhân viên này đã có bảng lương rồi !."))
                        window.location.href = "/HRM/SalaryEdit.aspx?id=" + r.d;
                }
                //else {
                //    Page.showNotiMessageError("Thông báo", r.d);
                //}
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            complete: function () {
                //$('.se-pre-con').fadeOut('slow');
            }
        });
    });
    $("img").click(function () {
        var id = $(this).attr('id');
        var status = $(this).attr('action');
        if (status != 1 || status != 2) {
            if (id == getEmployeeKey()) {
                Approve(id, status);
            }
        }
    });
    $("#btnOK").click(function () {
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/Accepted',
            data: JSON.stringify({
                "sEmployee": getEmployeeKey(),
                "sAutoKey": $("[id$=HID_AutoKey]").val()
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d == "") {
                    window.location.href = "/HRM/SalaryList.aspx";
                }
                else {
                    Page.showNotiMessageError("Thông báo", r.d);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });
    $("#btnProcess").click(function () {
        var employee = $("[id$=DDL_Employee]").val();
        var fromdate = $("[id$=txtFromDate]").val();
        var todate = $("[id$=txtToDate]").val();
        var thuong = $("[id$=txt13]").val();
        var lan = $("[id$=DDLTag]").val();
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/Process',
            data: JSON.stringify({
                "sEmployee": employee,
                "sFromDate": fromdate,
                "sToDate": todate,
                "sthuong": thuong,
                "slan": lan
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d.Message != "") {
                    Page.showNotiMessageInfo("Thông báo", r.d.Message);
                }
                if (r.d.AutoKey != 0) {
                    Page.showNotiMessageInfo("Thông báo", "Đã tính lương cho thông tin này rồi !.");
                }
                $("#bangtinh").empty();
                $("#bangtinh").append($(r.d.TableDetail));
                $("[id$=txt1]").val(r.d.Param1);
                $("[id$=txt2]").val(r.d.Param2);
                $("[id$=txt3]").val(r.d.Param3);
                $("[id$=txt4]").val(r.d.Param4);
                $("[id$=txt51]").val(r.d.Param51);
                $("[id$=txt6]").val(r.d.Param6);
                $("[id$=txt7]").val(r.d.Param7);
                $("[id$=txt8]").val(r.d.Param8);
                $("[id$=txt9]").val(r.d.Param9);
                $("[id$=txt10]").val(r.d.Param10);
                $("[id$=txt11]").val(r.d.Param11);
                $("[id$=txt12]").val(r.d.Param12);
                $("[id$=txt14]").val(r.d.Param14);
                $("[id$=HID_Tong]").val(r.d.Note1 + r.d.Note2 + r.d.Note3);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });
    $("#btnSave").click(function () {
        var employee = $("[id$=DDL_Employee]").val();
        var fromdate = $("[id$=txtFromDate]").val();
        var todate = $("[id$=txtToDate]").val();
        var description = $("[id$=txtdescription]").val();
        var param1 = $("[id$=txt1]").val();
        var param2 = $("[id$=txt2]").val();
        var param3 = $("[id$=txt3]").val();
        var param4 = $("[id$=txt4]").val();
        var param5 = $("[id$=txt5]").val();
        var param51 = $("[id$=txt51]").val();
        var param6 = $("[id$=txt6]").val();
        var param7 = $("[id$=txt7]").val();
        var param8 = $("[id$=txt8]").val();
        var param9 = $("[id$=txt9]").val();
        var param10 = $("[id$=txt10]").val();
        var param11 = $("[id$=txt11]").val();
        var param12 = $("[id$=txt12]").val();
        var param13 = $("[id$=txt13]").val();
        var param14 = $("[id$=txt14]").val();
        var note = $("[id$=txtNote1]").val();

        var total = $('#tblCommission tbody tr:not(.tong)').length;
        var row = "";
        var num = 0;
        $('#tblCommission tbody tr:not(.tong)').each(function () {
            num++;
            if (num < total)
                row +=
                    $(this).find('td:eq(0)').text() + ":"
                    + $(this).find('td:eq(1)').text() + ":"
                    + $(this).find('td:eq(2)').text() + ":"
                    + $(this).find('td:eq(3)').text() + ":"
                    + $(this).find('td:eq(4)').text() + ":"
                    + $(this).find('td:eq(5)').text() + ":"
                    + $(this).find('td:eq(6)').text() + ":"
                    + $(this).find('td:eq(7)').text() + ":"
                    + $(this).attr("tradekey") + ":"
                    + $(this).attr("type") + ":"
                    + $(this).attr("source") + ";";
            else
                row +=
                    $(this).find('td:eq(0)').text() + ":"
                    + $(this).find('td:eq(1)').text() + ":"
                    + $(this).find('td:eq(2)').text() + ":"
                    + $(this).find('td:eq(3)').text() + ":"
                    + $(this).find('td:eq(4)').text() + ":"
                    + $(this).find('td:eq(5)').text() + ":"
                    + $(this).find('td:eq(6)').text() + ":"
                    + $(this).find('td:eq(7)').text() + ":"
                    + $(this).attr("tradekey") + ":"
                    + $(this).attr("type") + ":"
                    + $(this).attr("source");
        });

        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/Save',
            data: JSON.stringify({
                "sEmployee": employee,
                "sFromDate": fromdate,
                "sToDate": todate,
                "sDescription": description,
                "sTable": row,
                "Param1": param1,
                "Param2": param2,
                "Param3": param3,
                "Param4": param4,
                "Param5": param5,
                "Param51": param51,
                "Param6": param6,
                "Param7": param7,
                "Param8": param8,
                "Param9": param9,
                "Param10": param10,
                "Param11": param11,
                "Param12": param12,
                "Param13": param13,
                "Param14": param14,
                "Tag": $("[id$=DDLTag]").val(),
                "NoteBonus": note
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (isNaN(r.d)) {
                    Page.showNotiMessageError("Lỗi", r.d);
                } else {
                    //Page.showNotiMessageInfo("Thông báo", "Đã tính lương thành công");
                    location.href = "/HRM/SalaryEdit.aspx?ID=" + r.d;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
                $('.se-pre-con').fadeOut('slow');
            },
            complete: function () {
            }
        });
    });
    $("#btnSend").click(function () {
        var key = $("[id$=HID_AutoKey]").val();
        if (key != 0) {
            $.ajax({
                type: 'POST',
                url: '/HRM/SalaryEdit.aspx/SendMessage',
                data: JSON.stringify({
                    "sAutoKey": $("[id$=HID_AutoKey]").val()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (r) {
                    if (r.d == "") {
                        window.location.href = "/HRM/SalaryList.aspx";
                    }
                    else {
                        Page.showNotiMessageError("Thông báo", r.d);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                }
            });
        }
        else {
            Page.showNotiMessageInfo("Không có thông tin để gửi !.");
        }
    });
    $("input[moneyinput]").number(true, 0);
    $(".select2").select2({ width: "100%" });
    $("[id$=DDLTag]").change(function () {
        NgayCongThucTe();
        //BaoHiem();
        LuongCBThucNhan();
        LuongNhanSauThue();
    });
    $("input[isCal]").on('blur', function (e) {
        var giatri = $(this).val();
        if (giatri.length <= 0) {
            $(this).val(0);
        }

        NgayCongThucTe();
        //BaoHiem();
        LuongCBThucNhan();
        LuongNhanSauThue();
    });
    $("[id$=txt8]").val($("#tblCommission #hieuqua").text());
});
function Approve(id, status) {
    $.ajax({
        type: "POST",
        url: "/HRM/SalaryEdit.aspx/Approve",
        data: JSON.stringify({
            "AutoKey": $('[id$=HID_AutoKey]').val(),
            "Employee": id,
            "Action": status,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (msg) {
            if (msg.d.Result == "ERROR") {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
            }
            else {
                location.reload();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function NgayCongThucTe() {
    var ngaycongchuan = parseFloat($("[id$=txt2]").val());
    var ngaynghi = parseFloat($("[id$=txt3]").val());
    var ngaycongthucte = ngaycongchuan - ngaynghi;
    $("[id$=txt4]").val(ngaycongthucte);
}
function BaoHiem() {
    var phantram = parseFloat($("[id$=txt5]").val());
    var luongcoban = parseFloat(Page.RemoveComma(($("[id$=txt1]").val())));
    var baohiem = (phantram * luongcoban) / 100;
    var kq = Page.FormatMoney(baohiem);
    $("[id$=txt51]").val(kq);
}
function LuongCBThucNhan() {
    //var phantram = parseFloat($("[id$=txt5]").val());
    var luongcoban = parseFloat(Page.RemoveComma(($("[id$=txt1]").val())));
    var baohiem = $("[id$=txt51]").val();//(phantram * luongcoban) / 100;
    var phucaptrachnhiem = parseFloat(Page.RemoveComma(($("[id$=txt6]").val())));
    var phucap = parseFloat(Page.RemoveComma(($("[id$=txt14]").val())));
    var ngaycongchuan = parseFloat($("[id$=txt2]").val());
    var ngaynghi = parseFloat($("[id$=txt3]").val());
    var ngaycongthucte = ngaycongchuan - ngaynghi;
    var temp = (((luongcoban + phucap + phucaptrachnhiem) / ngaycongchuan) * ngaycongthucte) - baohiem;
    var kq = Page.FormatMoney(temp);
    $("[id$=txt7]").val(kq);
}
function LuongNhanSauThue() {
    //var phantram = parseFloat($("[id$=txt5]").val());
    var luongcoban = parseFloat(Page.RemoveComma(($("[id$=txt1]").val())));
    var baohiem = $("[id$=txt51]").val();// (phantram * luongcoban) / 100;
    var phucaptrachnhiem = parseFloat(Page.RemoveComma(($("[id$=txt6]").val())));
    var phucap = parseFloat(Page.RemoveComma(($("[id$=txt14]").val())));
    var ngaycongchuan = parseFloat($("[id$=txt2]").val());
    var ngaynghi = parseFloat($("[id$=txt3]").val());
    var ngaycongthucte = ngaycongchuan - ngaynghi;
    var luongcobanthucnhan = (((luongcoban + phucap + phucaptrachnhiem) / ngaycongchuan) * ngaycongthucte) - baohiem;
    var hieuqua = parseFloat(Page.RemoveComma(($("[id$=txt8]").val())));
    var thue = parseFloat(Page.RemoveComma(($("[id$=txt9]").val())));
    var thuong = parseFloat(Page.RemoveComma(($("[id$=txt13]").val())));
    var temp = luongcobanthucnhan + hieuqua - thue + thuong;//luong nhan sau thue   
    var kq = Page.FormatMoney(temp);

    $("[id$=txt10]").val(kq);
    var lan = $("[id$=DDLTag]").val();
    if (lan == 1) {
        var temp1 = luongcobanthucnhan + thuong;
        var temp2 = temp - temp1;
        var kq2 = Page.FormatMoney(temp2);
        
        //$("[id$=txt11]").val(temp1);
        $("[id$=txt12]").val(kq2);
    }
    else {
        var temp1 = luongcobanthucnhan
        var temp2 = temp - temp1;
        var kq2 = Page.FormatMoney(temp2);

        //$("[id$=txt11]").val(temp1);
        $("[id$=txt12]").val(kq2);
    }
}