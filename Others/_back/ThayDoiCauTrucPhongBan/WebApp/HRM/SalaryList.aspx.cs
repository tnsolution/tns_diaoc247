﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebApp.HRM
{
    public partial class SalaryList : System.Web.UI.Page
    {
        static int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
        static int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
        static int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
        static string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
        static bool _IsManager = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                LoadData(FromDate, ToDate);

                CheckRoles();
                LoadData(FromDate, ToDate);
            }
        }
        void LoadData(DateTime FromDate, DateTime ToDate)
        {
           
            DataTable zTable = new DataTable();
            if (_IsManager)
            {
                lbltime.Text = "Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy");
                LitButton.Text = @" 
                <span class='pull-right action-buttons'>
                    <a href='#' id='ViewPrevious'>← Tháng trước</a> |
                      <a href='#' id='ViewNext'>Tháng sau →</a>
                </span>";
                zTable = Salary_Data.List(FromDate, ToDate);
            }
            else
            {
                FromDate = new DateTime(FromDate.Year, 1, 1, 0, 0, 0);
                ToDate = new DateTime(ToDate.Year, 12, 31, 23, 59, 59);
                lbltime.Text = "Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy");
                LitButton.Text = @" 
                <span class='pull-right action-buttons'>
                    <a href='#' id='ViewPrevious'>← Năm trước</a> |
                      <a href='#' id='ViewNext'>Năm sau →</a>
                </span>";
                zTable = Salary_Data.List(FromDate, ToDate, _Employee);
            }
            HtmlView(zTable);
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();

            if (_IsManager)
            {
                int ViewTime = HID_NextPrev.Value.ToInt();
                if (ViewTime == 1)
                {
                    FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                    ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                    FromDate = FromDate.AddMonths(1);
                    ToDate = ToDate.AddMonths(1).AddDays(-1);

                    HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                    HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                }
                if (ViewTime == -1)
                {
                    FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                    ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                    FromDate = FromDate.AddMonths(-1);
                    ToDate = FromDate.AddMonths(1).AddDays(-1);

                    HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                    HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                }
            }
            else
            {
                int ViewTime = HID_NextPrev.Value.ToInt();
                if (ViewTime == 1)
                {
                    FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                    ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                    FromDate = FromDate.AddYears(1);
                    ToDate = FromDate;

                    HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                    HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                }
                if (ViewTime == -1)
                {
                    FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                    ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                    FromDate = FromDate.AddYears(-1);
                    ToDate = FromDate;

                    HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                    HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                }
            }


            LoadData(FromDate, ToDate);
        }
        void CheckRoles()
        {
            switch (_UnitLevel)
            {
                case 0:
                case 1:
                    string RolePage = "ACC";
                    string[] result = User_Data.RolesCheck(_UserKey, RolePage).Split(',');
                    if (result[3].ToInt() == 1)
                    {
                        _IsManager = true;
                    }
                    else
                    {
                        _IsManager = false;
                    }
                    break;

                default:
                    _IsManager = false;
                    break;
            }
        }
        void HtmlView(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Từ ngày</th>");
            zSb.AppendLine("        <th>Đến ngày</th>");
            zSb.AppendLine("        <th>Lần 1</th>");
            zSb.AppendLine("        <th>Lần 2</th>");
            zSb.AppendLine("        <th>Tổng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (i++) + "</td>");
                    zSb.AppendLine("            <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FromDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["ToDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param11"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param12"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FINAL"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='8'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal1.Text = zSb.ToString();
        }
    }
}