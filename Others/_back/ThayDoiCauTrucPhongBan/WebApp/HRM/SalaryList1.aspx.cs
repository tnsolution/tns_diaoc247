﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebApp.HRM
{
    public partial class SalaryList1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                LoadData(FromDate, ToDate);
            }
        }
        void LoadData(DateTime FromDate, DateTime ToDate)
        {
            int _Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int _Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int _UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string _UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            DataTable zTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, 1, 1, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, 12, 31, 23, 59, 59);
            lbltime.Text = "Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy") + " tổng <b style=style='color: #c74444'>" + Salary_Data.SumSalary(_Employee, FromDate.Year).ToString("n0") + "</b>";
            LitButton.Text = @" 
                <span class='pull-right action-buttons'>
                    <a href='#' id='ViewPrevious'>← Năm trước</a> |
                    <a href='#' id='ViewNext'>Năm sau →</a>
                </span>";
            zTable = Salary_Data.ListSent(FromDate, ToDate, _Employee);
            HtmlView(zTable);
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();

            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = FromDate;

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate;

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            LoadData(FromDate, ToDate);
        }

        void HtmlView(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Từ ngày</th>");
            zSb.AppendLine("        <th>Đến ngày</th>");
            zSb.AppendLine("        <th>Lần 1</th>");
            zSb.AppendLine("        <th>Lần 2</th>");
            zSb.AppendLine("        <th>Tổng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    string Style = "";
                    int Key = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    if (r["Current"].ToInt() == Key && r["CurrentStatus"].ToInt() == 0)
                    {
                        Style = "style='Background:#ff00004f'";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' " + Style + ">");
                    zSb.AppendLine("            <td>" + (i++) + "</td>");
                    zSb.AppendLine("            <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FromDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["ToDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param11"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param12"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FINAL"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='8'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal1.Text = zSb.ToString();
        }
    }
}