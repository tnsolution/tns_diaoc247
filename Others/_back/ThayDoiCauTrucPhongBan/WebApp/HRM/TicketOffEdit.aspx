﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketOffEdit.aspx.cs" Inherits="WebApp.HRM.TicketOffEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết
                <span class="tools pull-right">
                    <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                    <a href="/HRM/TicketOffList.aspx" class="btn btn-white btn-default btn-bold">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-9">
                <table class="table table-bordered" id="tblInfo">
                    <tr>
                        <td rowspan="3" class="text-center">
                            <img src="http://diaoc247.vn/wp-content/themes/central/images/logo.png" />
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lblHoten" runat="server" Text="Họ tên:"></asp:Label></td>
                        <td colspan="2">Chức vụ:
                            <asp:Label ID="lblChucvu" runat="server" Text=".."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblPhong" runat="server" Text="Phòng: Hỗ trợ kinh doanh"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblQuanly" runat="server" Text="Quản lý trực tiếp:"></asp:Label></td>
                    </tr>
                    <tr>
                        <th style="width: 150px">Loại phép</th>
                        <th>Ngày xin</th>
                        <th>Số ngày nghỉ</th>
                        <th>Nghỉ từ ngày</th>
                        <th>Ngày vào làm lại</th>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="LitSelect" runat="server"></asp:Literal>
                        </td>
                        <td>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtNgayXin" runat="server" CssClass="form-control" role='datepicker'></asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSoNgayNghi" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                        </td>
                        <td>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtTuNgay" runat="server" CssClass="form-control" role='datepicker' placeholder="Từ ngày"></asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <asp:TextBox ID="txtDenNgay" runat="server" CssClass="form-control" role='datepicker' placeholder="Đến ngày"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:TextBox ID="txtNoiDung" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Lý do"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td colspan="5">
                            <asp:TextBox ID="txtGhiChu" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Dành cho quản lý"></asp:TextBox></td>
                    </tr>
                </table>
                <div class="space-6"></div>
                <div class="row">
                    <asp:Literal ID="LiteralSign" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main" style="padding: 6px;">
                            <asp:Literal ID="LitInfo" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $("#btnDelete").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/HRM/TicketOffEdit.aspx/Delete",
                    data: JSON.stringify({
                        "TicketKey": $('[id$=HID_TicketKey]').val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d.Result == "ERROR") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSave").click(function () {
                var Key = $('[id$=HID_TicketKey]').val();
                var LoaiPhep = $('[id$=DDLLoaiPhep]').val();
                var NoiDung = $('[id$=txtNoiDung]').val();
                var NgayXin = $("[id$=txtNgayXin]").val();
                var TuNgay = $('[id$=txtTuNgay]').val();
                var DenNgay = $('[id$=txtDenNgay]').val();
                var SoNgay = $("[id$=txtSoNgayNghi]").val();
                $.ajax({
                    type: "POST",
                    url: "/HRM/TicketOffEdit.aspx/SaveInfo",
                    data: JSON.stringify({
                        "TicketKey": Key,
                        "LoaiPhep": LoaiPhep,
                        "NgayXin": NgayXin,
                        "TuNgay": TuNgay,
                        "DenNgay": DenNgay,
                        "NoiDung": NoiDung,
                        "SoNgay": SoNgay
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d.Result == "ERROR") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            //Page.showPopupMessage("Thông báo !", msg.d.Message);
                            window.location.href = msg.d.Result;
                        }
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("img").click(function () {
                var id = $(this).attr('id');
                var status = $(this).attr('action');
                if (status == 3) {
                    Confirm();
                    return;
                }
                if (status != 1 || status != 2) {
                    if (id == getEmployeeKey()) {
                        Approve(id, status);
                    }
                }
            });
            $("[id$=txtSoNgayNghi]").keypress(function () {
                var val = $(this).val();
                if (isNaN(val)) {
                    val = val.replace(/[^0-9\.]/g, '');
                    if (val.split('.').length > 2)
                        val = val.replace(/\.+$/, "");
                }
                $(this).val(val);
            });
        });
        function Approve(id, status) {
            $.ajax({
                type: "POST",
                url: "/HRM/TicketOffEdit.aspx/Approve",
                data: JSON.stringify({
                    "TicketKey": $('[id$=HID_TicketKey]').val(),
                    "CurrentEmployee": id,
                    "Action": status,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Result == "ERROR") {
                        Page.showPopupMessage("Lỗi !", msg.d.Message);
                    }
                    else {
                        location.reload();
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function Confirm() {
            $.ajax({
                type: "POST",
                url: "/HRM/TicketOffEdit.aspx/Confirm",
                data: JSON.stringify({
                    "TicketKey": $('[id$=HID_TicketKey]').val(),
                    "CurrentEmployee": getEmployeeKey(),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Result == "ERROR") {
                        Page.showPopupMessage("Lỗi !", msg.d.Message);
                    }
                    else {
                        window.location.href = "/HRM/TicketOffList.aspx";
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    </script>
</asp:Content>
