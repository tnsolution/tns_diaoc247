﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.HRM
{
    public partial class TicketOffList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");

                CheckRole();
                LoadData();
            }
        }

        void LoadPrevious_Data()
        {
            DateTime FromDate;
            DateTime ToDate;
            int Employee = 0, Department = 0, UnitLevel = 0;

            ItemOffSearch zSession = (ItemOffSearch)Session["SearchOff"];

            FromDate = Tools.ConvertToDate(zSession.FromDate);
            ToDate = Tools.ConvertToDate(zSession.ToDate);
            Employee = zSession.Employee.ToInt();
            Department = zSession.Department.ToInt();
            UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            DataTable zTable = TicketOff_Data.List(Department, Employee, FromDate, ToDate);
            Literal_Table.Text = Html(zTable, string.Empty);
        }
        void LoadFix_Data()
        {
            DateTime FromDate;
            DateTime ToDate;
            int Employee = 0, Department = 0, UnitLevel = 0;

            FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            DataTable zTable = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = TicketOff_Data.List(0, 0, DateTime.MinValue, DateTime.MinValue, 0);
                    break;

                case 2:
                    zTable = TicketOff_Data.List(Department, 0, DateTime.MinValue, DateTime.MinValue, 0);
                    break;

                default:
                    zTable = TicketOff_Data.List(Department, Employee, DateTime.MinValue, DateTime.MinValue, 0);
                    break;
            }
            Literal_Table2.Text = Html(zTable, "1");
        }
        void LoadData()
        {
            DateTime FromDate;
            DateTime ToDate;

            if (Session["SearchOff"] != null)
            {
                LoadPrevious_Data();
            }
            else
            {
                FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                ToDate = FromDate.AddMonths(1).AddDays(-1);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                LoadData(FromDate, ToDate);
            }

            LoadFix_Data();
        }
        void LoadData(DateTime FromDate, DateTime ToDate)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            DataTable zTable = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = TicketOff_Data.List(0, 0, FromDate, ToDate, 3);
                    break;

                case 2:
                    zTable = TicketOff_Data.List(Department, 0, FromDate, ToDate, 3);
                    break;

                default:
                    zTable = TicketOff_Data.List(Department, Employee, FromDate, ToDate, 3);
                    break;
            }

            Literal_Table.Text = Html(zTable, string.Empty);
        }
        string Html(DataTable zTable, string param)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData" + param + "'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th class='td5'>STT</th>");
            zSb.AppendLine("        <th class='td15'>Phòng</th>");
            zSb.AppendLine("        <th class='td15'>Họ tên</th>");
            zSb.AppendLine("        <th>Ngày gửi</th>");
            zSb.AppendLine("        <th>Từ ngày</th>");
            zSb.AppendLine("        <th>Đến ngày</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                int Key = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                foreach (DataRow r in zTable.Rows)
                {
                    string Style = "";

                    if (r["Current"].ToInt() == Key && r["CurrentStatus"].ToInt() == 0)
                    {
                        Style = "style='Background:#ff00004f'";
                    }

                    zSb.AppendLine("            <tr id='" + r["TicketKey"].ToString() + "' " + Style + ">");
                    zSb.AppendLine("                <td>" + (i++) + "</td>");
                    zSb.AppendLine("                <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["TicketDate"].ToDateString() + "</td>");
                    zSb.AppendLine("                <td>" + r["FromDate"].ToDateString() + "</td>");
                    zSb.AppendLine("                <td>" + r["ToDate"].ToDateString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Status"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td></td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }

        [WebMethod(EnableSession = true)]
        public static string Search(string Department, string Employee, string FromDate, string ToDate, int Status)
        {
            ItemOffSearch ISearch = new ItemOffSearch();
            ISearch.Department = Department;
            ISearch.Employee = Employee;
            ISearch.FromDate = FromDate;
            ISearch.ToDate = ToDate;
            ISearch.Status = Status.ToString();
            HttpContext.Current.Session.Add("SearchOff", ISearch);

            DateTime zFromDate = Tools.ConvertToDate(FromDate);
            DateTime zToDate = Tools.ConvertToDate(ToDate);

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            if (Status == -1)
                zTable = TicketOff_Data.List(Department.ToInt(), Employee.ToInt(), zFromDate, zToDate);
            else
                zTable = TicketOff_Data.List(Department.ToInt(), Employee.ToInt(), zFromDate, zToDate, Status);

            StringBuilder zSb = new StringBuilder();
            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                int Key = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                foreach (DataRow r in zTable.Rows)
                {
                    string Style = "";

                    if (r["Current"].ToInt() == Key && r["CurrentStatus"].ToInt() == 0)
                    {
                        Style = "style='Background:#ff00004f'";
                    }

                    zSb.AppendLine("            <tr id='" + r["TicketKey"].ToString() + "' " + Style + ">");
                    zSb.AppendLine("                <td>" + (i++) + "</td>");
                    zSb.AppendLine("                <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["TicketDate"].ToDateString() + "</td>");
                    zSb.AppendLine("                <td>" + r["FromDate"].ToDateString() + "</td>");
                    zSb.AppendLine("                <td>" + r["ToDate"].ToDateString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Status"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td></td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }

        [WebMethod]
        public static ItemReturn Approve(int TicketKey, int EmployeeKey)
        {
            ItemReturn zResult = new ItemReturn();
            try
            {
                int Manager = 0;
                Manager = Employees_Data.GetManager(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
                #region send for quan ly truc tiep
                int phong = Employees_Data.GetDepartment(Manager.ToInt());
                int tructiep = Employees_Data.GetLevel(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
                string SQL = " UPDATE SYS_Message SET Readed = 1, ReadedDate = GETDATE() WHERE ObjectKey = " + TicketKey + " AND EmployeeKey = " + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];

                if (tructiep != 1)
                {
                    SQL += @" INSERT INTO SYS_Message 
(Date, ObjectTable, ObjectKey, EmployeeKey, DepartmentKey, Readed, Description, Orders) 
VALUES (GETDATE(), N'HRM_TicketOff'," + TicketKey + "," + Manager + "," + phong + ",0,N'Tình Trạng đơn phép'," + tructiep + ")";
                    zResult.Message = "Phép đã được chuyển lên " + Employees_Data.GetManagerName(Manager);
                }
                #endregion
                zResult = CustomInsert.Exe(SQL);
            }
            catch (Exception ex)
            {
                zResult.Result = "ERROR";
                throw;
            }

            return zResult;
        }

        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + DepartmentKey + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + DepartmentKey + " ORDER BY DepartmentName", false);

                    DDL_Department.SelectedValue = DepartmentKey.ToString();
                    DDL_Employee.SelectedValue = EmployeeKey.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + EmployeeKey + " AND IsWorking=2 ORDER BY LastName", false);

                    DDL_Employee.SelectedValue = EmployeeKey.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            LoadData(FromDate, ToDate);
        }
    }
}