﻿using Lib.Config;
using Lib.KPI;
using Lib.SYS;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace WebApp
{
    public static class Helper
    {
        public static string CreateEmailBody(string title, string message)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Email/AlertEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Title}", title);
            body = body.Replace("{Message}", message);
            return body;
        }
        /// <summary>
        /// Lấy tên bảng 
        /// </summary>
        /// <param name="TableName">PUL_Resale_Apartment, PUL_Resale_Ground,PUL_Resale_House</param>
        /// <returns>ResaleApartment, ResaleGround, ResaleHouse</returns>
        public static string ShortTable(string TableName)
        {
            switch (TableName.Trim())
            {
                case "PUL_Resale_Apartment":
                    return "ResaleApartment";

                case "PUL_Resale_Ground":
                    return "ResaleGround";

                case "PUL_Resale_House":
                    return "ResaleHouse";

                default:
                    return "ResaleApartment";
            }
        }

        /// <summary>
        /// Phân loai KPI
        /// </summary>
        /// <param name="ObjectKey"></param>
        /// <param name="EmployeeKey"></param>
        /// <param name="CreatedDate"></param>
        /// <param name="Param">1 khách, 2 san pham, 3 giao dich</param>
        /// <returns></returns>
        public static string KPI(int ObjectKey, int EmployeeKey, int DepartmentKey, DateTime CreatedDate, string Description, int CategoryKey, int CategoryPlan, int Param)
        {
            switch (Param)
            {
                case 1:
                    KPI_Customers_Info KpiCustomer = new KPI_Customers_Info(ObjectKey, EmployeeKey, CreatedDate);
                    KpiCustomer.CustomerKey = ObjectKey;
                    KpiCustomer.EmployeeKey = EmployeeKey;
                    KpiCustomer.DepartmentKey = DepartmentKey;
                    KpiCustomer.CreatedDate = CreatedDate;
                    KpiCustomer.Description = Description;
                    KpiCustomer.CategoryKey = CategoryKey;
                    KpiCustomer.CategoryPlan = CategoryPlan;
                    KpiCustomer.Save();
                    return KpiCustomer.Message;

                case 3:
                    KPI_Trades_Info KpiTrade = new KPI_Trades_Info(ObjectKey, EmployeeKey, CreatedDate);
                    KpiTrade.TradeKey = ObjectKey;
                    KpiTrade.EmployeeKey = EmployeeKey;
                    KpiTrade.DepartmentKey = DepartmentKey;
                    KpiTrade.CreatedDate = CreatedDate;
                    KpiTrade.Description = Description;
                    KpiTrade.CategoryKey = CategoryKey;
                    KpiTrade.CategoryPlan = CategoryPlan;
                    KpiTrade.Save();
                    return KpiTrade.Message;

                default:
                    return string.Empty;
            }
        }
        public static string KPI_Asset(int ObjectKey, string AssetID, string AssetType, int ProjectKey, int EmployeeKey, int DepartmentKey, DateTime CreatedDate, string Description, int CategoryKey, int CategoryPlan)
        {
            KPI_Assets_Info KpiAsset = new KPI_Assets_Info(ObjectKey, EmployeeKey, CreatedDate);
            KpiAsset.AssetKey = ObjectKey;
            KpiAsset.AssetID = AssetID;
            KpiAsset.EmployeeKey = EmployeeKey;
            KpiAsset.DepartmentKey = DepartmentKey;
            KpiAsset.CreatedDate = CreatedDate;
            KpiAsset.Description = Description;
            KpiAsset.CategoryKey = CategoryKey;
            KpiAsset.CategoryPlan = CategoryPlan;
            KpiAsset.AssetType = AssetType;
            KpiAsset.ProjectKey = ProjectKey;
            KpiAsset.Save();
            return KpiAsset.Message;
        }
        public static string KPI_Phone(int NoteKey, int DetailKey, int InfoKey, int EmployeeKey, int DepartmentKey, int Status, string Description, DateTime CreatedDate)
        {
            try
            {
                KPI_Phone_Info KpiPhone = new KPI_Phone_Info(InfoKey, EmployeeKey, CreatedDate);
                KpiPhone.InfoKey = InfoKey;
                KpiPhone.DetailKey = DetailKey;
                KpiPhone.NoteKey = NoteKey;
                KpiPhone.EmployeeKey = EmployeeKey;
                KpiPhone.DepartmentKey = DepartmentKey;
                KpiPhone.CreatedDate = CreatedDate;
                KpiPhone.Description = Description;
                KpiPhone.Status = Status;
                KpiPhone.Save();
                return KpiPhone.Message;
            }
            catch (Exception ex)
            {
                Notification_Info zInfo = new Notification_Info();
                string strLog = "INSERT INTO SYS_Log_Notification ([Datetime],ERROR ) VALUES ('" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + "',N'KPI PHONE InfoKey" + InfoKey + "| EmployeeKey" + EmployeeKey + "|DetailKey" + DetailKey + "|NoteKey" + NoteKey + "|Status" + Status + "|Description" + Description + "|" + ex.ToString() + "')";
                zInfo.InsertAuto(strLog);

                return string.Empty;
            }
        }

        public static DataTable GetKPI(int Employee, int Department, DateTime FromDate, DateTime ToDate, DateTime BeginDate, DateTime EndDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"
SELECT AutoKey, Product AS [Name],
dbo.KPI_RequireNumber(@Department, @Employee,  AutoKey, @FromDate, @ToDate) AS RequireNumber,
dbo.KPI_RequirePercent(@Department, @Employee, AutoKey, @FromDate, @ToDate) AS RequirePercent,
dbo.KPI_DoingNumber( @Department, @Employee, AutoKey, @FromDate, @ToDate) AS DoingNumberTotal,
dbo.KPI_DoingNumber(@Department, @Employee, AutoKey, @BeginDate, @EndDate) AS DoingNumberCurrent
FROM SYS_Categories 
WHERE [TYPE] = 34 AND [VALUE] = 1 
ORDER BY [RANK]";

            SqlContext SQL = new SqlContext();
            SQL.CMD.Parameters.Add("@Employee", SqlDbType.Int).Value = Employee;
            SQL.CMD.Parameters.Add("@Department", SqlDbType.Int).Value = Department;
            SQL.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            SQL.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            SQL.CMD.Parameters.Add("@BeginDate", SqlDbType.DateTime).Value = BeginDate;
            SQL.CMD.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDate;
            return SQL.GetData(zSQL);
        }
        public static DataTable GetKPI_Phone(int Employee, int Department, int Category, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"
SELECT A.[Status], B.*,
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo,
dbo.FNC_GetStatusInfoData(A.[Status]) [StatusName]
FROM KPI_Phone A 
LEFT JOIN TASK_Excel_Detail B ON A.InfoKey = B.InfoKey
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " ORDER BY A.[Status]";
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetKPI_Trade(int Employee, int Department, int Category, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"
SELECT A.[Description], B.Income, B.AssetID, 
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetProjectName(B.ProjectKey) ProjectName
FROM KPI_Trades A 
LEFT JOIN FNC_Transaction B ON A.TradeKey = B.TransactionKey
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate AND CategoryPlan = @Category";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetKPI_Customer(int Employee, int Department, int Category, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"SELECT A.[Description], 
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo, 
dbo.FNC_GetNameEmployee(B.EmployeeKey) EmployeeBelong, 
B.CustomerName, B.Phone1, B.Email1
FROM KPI_Customers A 
LEFT JOIN CRM_Customer B ON A.CustomerKey = B.CustomerKey
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate AND CategoryPlan = @Category";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetKPI_Asset(int Employee, int Department, int Category, DateTime FromDate, DateTime ToDate)
        {
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"SELECT A.[Description], 
dbo.FNC_SysCategoryName(A.CategoryKey) [Status],
dbo.FNC_GetNameEmployee(A.EmployeeKey) EmployeeDo,
dbo.FNC_GetProjectName(A.ProjectKey) ProjectName,
A.AssetType, A.AssetID, EmployeeKey, DepartmentKey, CategoryKey, CategoryPlan
FROM KPI_Assets A 
WHERE A.CreatedDate BETWEEN @FromDate AND @ToDate";
            if (Employee != 0)
                zSQL += " AND A.EmployeeKey = @EmployeeKey";
            if (Department != 0)
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            zSQL += " ORDER BY ProjectName DESC";

            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Category;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}