﻿jQuery(function ($) {    
    $(".select2").select2({ width: "100%" });

    $('.comments').ace_scroll({
        size: 450
    });
    $('#BotRight').ace_scroll({
        size: 300
    });
    $('#BotLeft').ace_scroll({
        size: 300
    });
    $('#BotRight').height($('#BotLeft').height());

    $("#ViewPrevious").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=HID_NextPrev]").val(-1);
        $("[id$=btnView]").trigger("click");
    });
    $("#ViewNext").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=HID_NextPrev]").val(1);
        $("[id$=btnView]").trigger("click");
    });

    $('a[btn="btnDeleteMessage"]').click(function () {
        var trid = $(this).attr('key');
        $.ajax({
            type: 'POST',
            url: 'Home.aspx/DeleteMessage',
            data: JSON.stringify({
                'id': trid,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d == 'OK') {
                    $('div[key=' + trid + ']').remove();
                }
                else {
                    alert(msg.d);
                }
            },
            error: function () {
                alert('Lỗi xin liên hệ Admin !');
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });
    $('a[btn="btnEditMessage"]').click(function () {
        var key = $(this).attr("key");
        $("[id$=HID_Meesage]").val(key);
        $("[id$=txt_Contents]").val($('div[key=' + key + ']').find($('div[class="text"]')).text());
        $('#NewMessage').modal({
            backdrop: true,
            show: true
        });
    });
});