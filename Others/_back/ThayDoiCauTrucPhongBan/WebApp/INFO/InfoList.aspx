﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfoList.aspx.cs" Inherits="WebApp.INFO.InfoList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        .form-group {
            margin-bottom: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Nguồn tin<small>cần khai tác, đã nhập, đã gửi, đang xử lý,...</small>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Lit_Message" runat="server"></asp:Literal>
            <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <!--Search-->
    <div class="modal fade" id="mSearch" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Tìm kiếm, xử lý thông tin công việc</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Thông tin</label>
                                <asp:DropDownList ID="DDL_Type" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn đã gửi, chưa gửi--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="--Đã gửi--" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="--Chưa gửi--"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Status" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn tình trạng--" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:DropDownList ID="DDL_Project" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%">
                                    <asp:ListItem Value="0" Text="--Chọn dự án--" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txt_Name" runat="server" CssClass="col-md-6" Text="" placeholder="-- Mã sản phẩm--"></asp:TextBox>
                                <asp:TextBox ID="txt_Room" runat="server" CssClass="col-md-6" Text="" placeholder="-- Số phòng ngủ--"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txt_GuestName" runat="server" CssClass="form-control" Text="" placeholder="--Tên chủ nhà--"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txt_FromDate" runat="server" CssClass="form-control" Text="" placeholder="--Chọn từ ngày--" role='datepicker'></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txt_ToDate" runat="server" CssClass="form-control" Text="" placeholder="--Chọn đến ngày--" role='datepicker'></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSearch" runat="server" onserverclick="btnSearch_ServerClick">
                        <i class="ace-icon fa fa-search-plus"></i>
                        Tìm
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mSend" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Gửi thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:Literal ID="Lit_mTitle" runat="server"></asp:Literal>
                            <div class="space-6"></div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Chọn dự án cần gửi</label>
                                <asp:DropDownList ID="DDL_ProjectToSend" runat="server" class="form-control select2" Style="width: 100%" required>
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Nhập số thông tin cần gửi</label>
                                <asp:TextBox ID="txt_InfoNo" runat="server" CssClass="form-control" Text="" placeholder="..." required></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Nhân viên cần gửi đến</label>
                                <asp:DropDownList ID="DDL_ToEmployee" runat="server" class="form-control select2" AppendDataBoundItems="true" Style="width: 100%" required>
                                    <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label class="control-label no-padding-right" for="form-field-1">Ghi chú</label>
                                <asp:TextBox ID="txt_Description" runat="server" CssClass="form-control" Text="" placeholder="..."></asp:TextBox>
                            </div>
                            <div class="form-group" style="display: none">
                                <div class="checkbox">
                                    <label>
                                        <input name="form-field-checkbox" type="checkbox" class="ace" id="chkCut" runat="server" />
                                        <span class="lbl">Cắt hết công việc của người trước đó</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSend" runat="server" onserverclick="btnSend_ServerClick">
                        <i class="ace-icon fa fa-send-o"></i>
                        Gửi
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mExcel" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Chọn tập tin Excel</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="exampleInputFile">Tập tin excel</label>
                                <div class="row">
                                    <div class="col-md-8">
                                        <asp:FileUpload ID="FU" runat="server" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                    </div>
                                </div>
                                <p class="help-block">
                                    Tập tin excel bị trùng sẽ tự động xóa.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <asp:DropDownList ID="DDL_ProjectInput" runat="server" class="form-control select2" required>
                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnImport" runat="server" onserverclick="btnImport_Click">
                        <i class="ace-icon fa fa-upload"></i>
                        Tải tập tin
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mProcess" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" id="PageHead">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="PageInfo">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header widget-header-blue widget-header-flat">
                                    <h5 class="widget-title lighter red"><span id="ReplyInfoEarlier">...</span>&nbsp;<span id="msgAction">...</span></h5>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-0">
                                        <div class="space-6"></div>
                                        <div class="row">
                                            <div id="kygui" style="display: none">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Tình trạng</label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="DDL_StatusAsset" CssClass="select2" runat="server" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                                        <div class="col-sm-3">
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input type="text" role="datepicker" class="form-control pull-right"
                                                                    id="txt_DateContractEnd" placeholder="dd/MM/yyyy" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Nội thất</label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="DDL_Furnitur" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-sm-2 control-label"><span id="giaban">Giá bán VNĐ</span><sup>*</sup></label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="txt_PriceVND" placeholder="Nhập số" moneyonly value="0" />
                                                            <input type="text" class="form-control" id="txt_PriceUSD" placeholder="Nhập số" moneyonly value="0" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input name="switch-field-1" class="ace ace-switch" type="checkbox" id="Usdswitch1" />
                                                            <span class="lbl" data-lbl="USD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VNĐ"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Pháp lý</label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="DDL_Legal" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-sm-2 control-label"><span id="giathue">Giá thuê VNĐ</span><sup>*</sup></label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="txt_PriceRentVND" placeholder="Nhập số" moneyonly value="0" />
                                                            <input type="text" class="form-control" id="txt_PriceRentUSD" placeholder="Nhập số" moneyonly value="0" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input name="switch-field-1" class="ace ace-switch" type="checkbox" id="Usdswitch2" />
                                                            <span class="lbl" data-lbl="USD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VNĐ"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Nhu cầu</label>
                                                        <div class="col-sm-3">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="ace" value="CN" name="chkPurpose" id="chkCN" />
                                                                    <span class="lbl">Chuyển nhượng</span>
                                                                </label>
                                                            </div>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="ace" value="CT" name="chkPurpose" id="chkCT" />
                                                                    <span class="lbl">Cho thuê</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Ghi chú</label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control" id="txt_AssetDescription" placeholder="..." rows="4"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="chunha" style="display: none">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Họ và tên(*)</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoName" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">SĐT1</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoPhone1" placeholder="Nhập text" role="phone" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">SĐT2</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoPhone2" placeholder="Nhập text" role="phone" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoEmail" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoAddress1" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Địa chỉ liên hệ</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" id="txt_InfoAddress2" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="khachhang" style="display: none">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Họ tên</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">SĐT1</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" id="txt_Phone1" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">SĐT2</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Nguồn khách</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="DDL_Source" runat="server" CssClass="select2" AppendDataBoundItems="true" required>
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" runat="server" class="form-control" id="txt_SourceNote" placeholder="Chi tiết nguồn" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Mức độ tiềm năng</label>
                                                        <div class="col-sm-4">
                                                            <asp:DropDownList ID="DDL_" runat="server" CssClass="select2" AppendDataBoundItems="true" required>
                                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" disabled></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Nhu cầu</label>
                                                        <div class="col-sm-4">
                                                            <asp:ListBox ID="LB_Need" runat="server" SelectionMode="Multiple" CssClass="select2" data-placeholder="--Chọn--" required></asp:ListBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Ghi chú</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="txt_CustomerDescription" placeholder="..." />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnProcess">
                        OK</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_RelateKey" runat="server" />
    <asp:HiddenField ID="HID_TaskKey" runat="server" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" />
    <asp:HiddenField ID="HID_InfoKey" runat="server" />
    <asp:HiddenField ID="HID_InfoType" runat="server" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script src="/INFO/InfoList.js"></script>
</asp:Content>
