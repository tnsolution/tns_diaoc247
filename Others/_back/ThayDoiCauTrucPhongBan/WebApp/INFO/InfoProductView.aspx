﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfoProductView.aspx.cs" Inherits="WebApp.INFO.InfoProductView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết sản phẩm
                <span id="tieudetrang">
                    <asp:Literal ID="txt_Asset" runat="server"></asp:Literal>
                </span>
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a href="javascript:void(0)" class="btn btn-white btn-warning btn-bold toggle-accordion" accordion-id="#accordion">
                        <i class="ace-icon fa fa-expand orange"></i>Mở rộng 
                    </a>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnExeReminder">
                        <i class="ace-icon fa fa-adjust blue"></i>
                        Xử lý nhắc nhở
                    </button>
                    <a class="btn btn-white btn-default btn-bold" href="InfoList.aspx">
                        <i class="ace-icon fa fa-reply info"></i>
                        Trở về
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div id="accordion" class="accordion-style1 panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Thông tin làm việc
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseOne" aria-expanded="true">
                            <div class="panel-body" id="body1">
                                <asp:Literal ID="Lit_InfoAsset" runat="server"></asp:Literal>
                                <asp:Literal ID="Lit_InfoOwner" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Thông tin cơ bản
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseTwo">
                            <div class="panel-body">
                                <asp:Literal ID="Lit_ExtraAsset" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Hình ảnh
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseThree">
                            <div class="panel-body">
                                <ul class="ace-thumbnails clearfix" id="hinhanh">
                                    <asp:Literal ID="Lit_File" runat="server"></asp:Literal>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Chủ nhà
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse4">
                            <div class="panel-body" id="chunha">
                                <asp:Literal ID="Lit_Owner" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                    &nbsp;Tin đăng 247
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse5">
                            <div class="panel-body">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Tiêu đề
                                        </div>
                                        <div class="profile-info-value" id="tieude">
                                            <asp:Literal ID="txt_WebTitle" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Nội dung tin
                                        </div>
                                        <div class="profile-info-value" id="noidung">
                                            <asp:Literal ID="txt_WebContent" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            Xuất bản
                                        </div>
                                        <div class="profile-info-value" id="xuatban">
                                            <asp:Literal ID="txt_WebPublish" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Các sản phẩm cùng dự án:
                            <asp:Literal ID="Lit_RightTitle" runat="server"></asp:Literal></h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" id="txt_Name" class="col-xs-2 input-sm" placeholder="Mã căn" />
                                    <input type="text" id="txt_Bed" class="col-xs-2 input-sm" placeholder="PN" />
                                    <asp:DropDownList ID="DDL_Category" runat="server" CssClass="col-xs-2 input-sm" AppendDataBoundItems="true">
                                        <asp:ListItem Value="0" Text="Loại sản phẩm" disabled="disabled" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="DDL_Purpose" runat="server" CssClass="col-xs-2 input-sm" AppendDataBoundItems="true">
                                        <asp:ListItem Value="0" Text="Nhu cầu" disabled="disabled" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="CN" Text="Chuyển nhượng"></asp:ListItem>
                                        <asp:ListItem Value="CT" Text="Cho thuê"></asp:ListItem>
                                        <asp:ListItem Value="KT" Text="Kinh doanh"></asp:ListItem>
                                    </asp:DropDownList>
                                    <a href="#" class="pull-right btn btn-sm btn-purple" id="btnSearch">
                                        <i class="ace-icon fa fa-search"></i>Lọc
                                    </a>
                                </div>
                            </div>
                            <div class="space-2"></div>
                            <div id="scroll">
                                <asp:Literal ID="Lit_RightTable" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
    <asp:HiddenField ID="HID_RemindKey" runat="server" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" />
    <asp:HiddenField ID="HID_EmployeeKey" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/SAL/ProductView.js"></script>
</asp:Content>
