﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.INFO
{
    public partial class InfoProductView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AssetKey.Value = Request["ID"];
                if (Request["Type"] != null)
                    HID_AssetType.Value = Request["Type"];

                LoadAsset();
                LoadFile();
                CheckRoles();
            }
        }

        void LoadAsset()
        {
            Product_Info zAsset = new Product_Info(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);

            LoadOwner(zAsset.ItemAsset.EmployeeKey, zAsset.ItemAsset.EmployeeName);
            LoadData(zAsset.ItemAsset);
            HID_ProjectKey.Value = zAsset.ItemAsset.ProjectKey;
            HID_EmployeeKey.Value = zAsset.ItemAsset.EmployeeKey;

            Tools.DropDown_DDL(DDL_Category, "SELECT A.CategoryKey, C.CategoryName FROM PUL_Project_Category A LEFT JOIN PUL_Category C ON C.CategoryKey = A.CategoryKey WHERE A.ProjectKey=" + HID_ProjectKey.Value, false);

            txt_Asset.Text = zAsset.ItemAsset.AssetID;
            txt_WebTitle.Text = zAsset.ItemAsset.WebTitle;
            txt_WebPublish.Text = zAsset.ItemAsset.WebPublished.ToInt() == 0 ? "Chưa đăng tin" : "Đã đăng tin";
            txt_WebContent.Text = zAsset.ItemAsset.WebContent;

            #region [view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "  <input name='switch-field-1' class='ace ace-switch' type='checkbox' id='chkPriceType' />";
            ViewMoney += "  <span class='lbl'>&nbsp USD</span>";
            ViewMoney += "  </div>";
            #endregion

            #region [Asset Info]
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang'>" + zAsset.ItemAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe'>" + zAsset.ItemAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán</div>");
            zSb.AppendLine("        <div class='profile-info-value giatien'><div view=usd class='pull-left' id='giaban1'>" + zAsset.ItemAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left' id='giaban2'>" + zAsset.ItemAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê</div>");
            zSb.AppendLine("        <div class='profile-info-value giatien'><div view=usd class='pull-left' id='giathue1'>" + zAsset.ItemAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left' id='giathue2'>" + zAsset.ItemAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua</div>");
            zSb.AppendLine("        <div class='profile-info-value giatien' id='giamua'>" + zAsset.ItemAsset.PricePurchase + " (VND)</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat'>" + zAsset.ItemAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau'>" + zAsset.ItemAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ghichu'>" + zAsset.ItemAsset.Description + "</div>");
            zSb.AppendLine("    </div>");

            if (!CheckViewPhone(
                zAsset.ItemAsset.EmployeeKey.ToInt(),
                zAsset.ItemAsset.AssetKey.ToInt(),
                zAsset.ItemAsset.AssetType))
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.EmployeeName + "</div>");
                zSb.AppendLine("    </div>");
            }
            else
            {

            }

            zSb.AppendLine("</div>");
            Lit_InfoAsset.Text = zSb.ToString();
            #endregion

            #region [Extra Info]
            zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Dự án</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='duan'>" + zAsset.ItemAsset.ProjectName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã sản phẩm</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='masp'>" + zAsset.ItemAsset.AssetID + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Loại sản phẩm</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='loaisp'>" + zAsset.ItemAsset.CategoryName + "</div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Số phòng ngủ</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='sophong'>" + zAsset.ItemAsset.Room + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dttimtuong'>" + zAsset.ItemAsset.AreaWall + " </div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích thông thủy</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dtthongthuy'>" + zAsset.ItemAsset.AreaWater + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Hướng view</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='huongview'>" + zAsset.ItemAsset.View + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Hướng cửa</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='huongcua'>" + zAsset.ItemAsset.Door + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");

            Lit_ExtraAsset.Text = zSb.ToString();
            #endregion                     
        }
        void LoadOwner(string EmployeeKey, string EmployeeName)
        {
            StringBuilder zSb = new StringBuilder();
            StringBuilder Owner = new StringBuilder();
            DataTable zTable = Owner_Data.List(HID_AssetType.Value, HID_AssetKey.Value.ToInt());
            string color = "border: 1px solid #7f209c;";
            if (zTable.Rows.Count > 0)
            {
                bool IsAllow = CheckViewPhone(EmployeeKey.ToInt(), HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
                string Phone = "Bạn chưa được <b>" + EmployeeName + "</b> cho xem thông tin này";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    if (i != 0)
                        color = "";

                    DataRow r = zTable.Rows[i];
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' style='" + color + "'>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    if (IsAllow)
                        zSb.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                    else
                        zSb.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Email</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["Email1"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Địa chỉ</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>Liên lạc: " + r["Address1"].ToString() + "<br/> Thường trú: " + r["Address2"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Ngày cập nhật</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CreatedDate"].ToDateString() + "</div>");
                    zSb.AppendLine(" </div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='space-6'></div>");

                    if (i == 0)
                    {
                        Owner.AppendLine("<div class='profile-user-info profile-user-info-striped' id='chunha1'>");
                        Owner.AppendLine("<div class='profile-info-row'>");
                        Owner.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                        Owner.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                        Owner.AppendLine("</div>");
                        Owner.AppendLine("<div class='profile-info-row'>");
                        Owner.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                        if (IsAllow)
                            Owner.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                        else
                            Owner.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                        Owner.AppendLine("</div>");
                        Owner.AppendLine("</div>");
                    }
                }
            }
            Lit_InfoOwner.Text = Owner.ToString();
            Lit_Owner.Text = zSb.ToString();
        }
        void LoadFile()
        {
            List<ItemDocument> zList = Document_Data.List(135, HID_AssetKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine("<li id='" + item.FileKey + "'>");
                    zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                    zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageThumb + "' />");
                    zSb.AppendLine("            <div class='text'>");
                    zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                    zSb.AppendLine("            </div>");
                    zSb.AppendLine("    </a>");
                    zSb.AppendLine("</li>");
                }
            }
            Lit_File.Text = zSb.ToString();
        }
        void LoadData(ItemAsset Item)
        {
            Lit_RightTitle.Text = Item.ProjectName;

            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            List<ItemAsset> zList = new List<ItemAsset>();

            zList = Product_Data.Search(Item.ProjectKey.ToInt(), 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0);

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblExtraData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Mã sản phẩm</th>");
            zSb.AppendLine("        <th>Số phòng</th>");
            zSb.AppendLine("        <th>Loại</th>");
            zSb.AppendLine("        <th>Nhu cầu</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zList.Count > 0)
            {
                int i = 1;
                foreach (ItemAsset r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_RightTable.Text = zSb.ToString();
        }

        //kiem tra nhac nhở san phẩm
        [WebMethod]
        public static ItemResult CheckReminder(int AssetKey)
        {
            ItemResult zResult = new ItemResult();
            int Exists = Product_Data.CheckReminder(AssetKey);
            if (Exists > 0)
            {
                zResult.Message = "Sản phẩm này đã đến hẹn, chưa được xử lý, vui lòng kiểm tra ngày liên hệ !";
                zResult.Result = Exists.ToString();
            }
            return zResult;
        }
        [WebMethod]
        public static ItemResult ExeReminder(int RemindKey)
        {
            ItemResult zResult = new ItemResult();
            Notification_Info zInfo = new Notification_Info();
            zInfo.ID = RemindKey;
            zInfo.IsRead = 1;
            zInfo.ReadDate = DateTime.Now;
            zInfo.UpdateReaded();
            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
                zResult.Result = "0";
            }
            else
            {
                zResult.Message = "Đã xử lý thành công !";
                zResult.Result = "1";
            }
            return zResult;
        }

        //get 1 thong tin khi click ben phai
        [WebMethod]
        public static ItemAsset GetAsset(int AssetKey, string Type)
        {
            string CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            if (zInfo.ItemAsset.EmployeeKey == CurrentAgent)
                zInfo.ItemAsset.Edit = "1";
            return zInfo.ItemAsset;
        }
        [WebMethod]
        public static ItemResult GetOwner(int AssetKey, string Type, int EmployeeKey, string EmployeeName)
        {
            ItemResult Result = new ItemResult();
            StringBuilder zSb = new StringBuilder();
            StringBuilder zTop = new StringBuilder();
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            if (zTable.Rows.Count > 0)
            {
                bool IsAllow = CheckViewPhone(EmployeeKey, AssetKey, Type);
                string Phone = "Bạn chưa được <b>" + EmployeeName + "</b> cho xem thông tin này";
                string color = "border: 1px solid #7f209c;";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    if (i != 0)
                        color = "";
                    DataRow r = zTable.Rows[i];
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' style='" + color + "'>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    if (IsAllow)
                        zSb.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                    else
                        zSb.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Email</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["Email1"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Địa chỉ</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>Liên lạc: " + r["Address1"].ToString() + "<br/> Thường trú: " + r["Address2"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Ngày cập nhật</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CreatedDate"].ToDateString() + "</div>");
                    zSb.AppendLine(" </div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='space-6'></div>");

                    #region [get 1 chu nha]
                    if (i == 0)
                    {
                        zTop.AppendLine("<div class='profile-info-row'>");
                        zTop.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                        zTop.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                        zTop.AppendLine("</div>");
                        zTop.AppendLine("<div class='profile-info-row'>");
                        zTop.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                        if (IsAllow)
                            zTop.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                        else
                            zTop.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                        zTop.AppendLine("</div>");
                    }
                    #endregion
                }

                Result.Result = zSb.ToString();
                Result.Result2 = zTop.ToString();
            }

            return Result;
        }
        [WebMethod]
        public static string GetFile(int AssetKey)
        {
            List<ItemDocument> zList = Document_Data.List(135, AssetKey);
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine("<li id='" + item.FileKey + "'>");
                    zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                    zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageThumb + "' />");
                    zSb.AppendLine("            <div class='text'>");
                    zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                    zSb.AppendLine("            </div>");
                    zSb.AppendLine("    </a>");
                    zSb.AppendLine("</li>");
                }
            }
            return zSb.ToString();
        }

        //search
        [WebMethod(EnableSession = true)]
        public static string SearchAsset(int Project, string AssetID, string Room, string Category, string Purpose)
        {
            List<ItemAsset> zList = new List<ItemAsset>();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            zList = Product_Data.Search(Project, Category.ToInt(), string.Empty, string.Empty, string.Empty, Room, AssetID, string.Empty, Purpose, 0, 0);
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (ItemAsset r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }

        //kiem tra user hien tai co duoc phep xem so dt cua thong tin sản phẩm dang xem
        static bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            if (UnitLevel == 0 || UnitLevel == 1 || CurrentAgent == OwnerAgent)
            {
                return true;
            }

            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }
        //loc lay gia va nhu cau
        static string GetPrice(ItemAsset Item)
        {
            string PurposeShow = "";
            if (Item.Purpose.Contains(","))
            {
                string[] temp = Item.Purpose.Split(',');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "Chuyển nhượng":
                                PurposeShow += "Chuyển nhượng - <span class='giatien'>" + Item.Price_VND.ToDoubleString() + "</span><br/>";
                                break;

                            case "Cho thuê":
                                PurposeShow += "Cho thuê - <span class='giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</span><br/>";
                                break;

                            case "Kinh doanh":
                                PurposeShow += "Kinh doanh<br/>";
                                break;
                        }
                    }
                }
            }
            else
            {
                string s = Item.Purpose;
                switch (s.Trim())
                {
                    case "Chuyển nhượng":
                        PurposeShow += "Chuyển nhượng - <span class='giatien'>" + Item.Price_VND.ToDoubleString() + "</span>";
                        break;

                    case "Cho thuê":
                        PurposeShow += "Cho thuê - <span class='giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</span>";
                        break;

                    case "Kinh doanh":
                        PurposeShow += "Kinh doanh";
                        break;
                }
            }

            return PurposeShow;
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Lit_Button.Text = @"
                        <a href='#' class='btn btn-white btn-warning btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil orange'></i>
                            Điều chỉnh
                        </a>";
                    break;

                default:
                    if (_Permitsion[2].ToInt() == 1 &&
                        CurrentEmployee == HID_EmployeeKey.Value.ToInt())
                    {
                        Lit_Button.Text = @"
                        <a href='#' class='btn btn-white btn-warning btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil orange'></i>
                            Điều chỉnh
                        </a>";
                    }
                    break;
            }
        }
        #endregion
    }
}