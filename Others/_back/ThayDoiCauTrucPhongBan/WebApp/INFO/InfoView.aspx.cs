﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

//Type loại tin, chưa gữi đã gửi.
namespace WebApp.INFO
{
    public partial class InfoView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRoles();

                if (Request["ID"] != null)
                    HID_Key.Value = Request["ID"];

                if (Request["Type"] != null)
                    HID_Type.Value = Request["Type"];

                Lit_Dropdowns.Text = Tools.Html_Dropdown_StatusTask("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent <> 0 ORDER BY ID");
                //Tools.DropDown_DDL(DDL_Table, "SELECT TableName, CategoryName FROM SYS_Table WHERE TYPE = 2 ORDER BY Rank", false);
                //Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                //Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);

                LoadInfo();
                LoadRelate();
                //LoadExtraData();
            }
        }

        void LoadInfo()
        {
            if (HID_Key.Value != string.Empty &&
                HID_Key.Value != "0")
            {
                //HID_Type: 1 truy xuất data đã gửi, 2 data chưa gửi
                Excel_Detail_Info zInfo = new Excel_Detail_Info(HID_Key.Value.ToInt(), HID_Type.Value.ToInt());
                // tức là thông tin đã được xử lý => không hiển thị nút xử lý
                // thong tin đã xử lý link liên kết qua luôn bảng record thực tế
                if (zInfo.StatusKey == 6)
                {
                    //Response.Redirect("/INFO/InfoProductView.aspx?ID=Type=");
                    Lit_Button.Text += @"<button type='button' class='btn btn-white btn-info btn-bold' id='btnProcessInfo'><i class='ace-icon fa fa-floppy-o blue'></i>Xử lý tin</button>&nbsp";
                }

                HID_KeyInfo.Value = zInfo.Key.ToString();

                txt_ProjectName.Text = zInfo.Product;
                txt_AssetName.Text = zInfo.Asset;
                txt_CustomerName.Text = zInfo.SirName + " " + zInfo.LastName + " " + zInfo.FirstName;
                txt_Address1.Text = zInfo.Address1;
                txt_Address2.Text = zInfo.Address2;
                txt_Phone.Text = "<span id='txtPhone1'>" + zInfo.Phone1 + "</span><span id='txtPhone2'>" + zInfo.Phone2 + "</span>";
                txt_Status.Text = zInfo.Status.ToString();
                txt_EmployeeName.Text = zInfo.EmployeeName;

                txt_InfoName.Value = zInfo.SirName + " " + zInfo.LastName + " " + zInfo.FirstName;
                txt_InfoAddress1.Value = zInfo.Address1;
                txt_InfoAddress2.Value = zInfo.Address2;
                txt_InfoPhone1.Value = zInfo.Phone1;
                txt_InfoPhone2.Value = zInfo.Phone2;
                txt_InfoEmail.Value = zInfo.Email1;

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();

                #region [----]
                txt_EditCustomerName.Value = zInfo.LastName + " " + zInfo.FirstName;
                txt_EditEmail1.Value = zInfo.Email1;
                txt_EditPhone1.Value = zInfo.Phone1;
                txt_EditPhone2.Value = zInfo.Phone2;
                txt_EditAddress2.Value = zInfo.Address2;
                txt_EditAddress1.Value = zInfo.Address1;
                #endregion                
            }
        }
        void LoadRelate()
        {
            DataTable zTable = Product_Data.CheckExsist(txt_AssetName.Text);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<tr id='" + r["AssetKey"].ToString() + "' type='" + r["Type"].ToString() + "'><td><a href='#'>Dự án: " + r["ProjectName"].ToString() + "<br/> Sản phẩm: " + r["AssetID"].ToString() + "<br/>Chủ nhà: " + r["OwnerInfo"].ToString() + "<br/><strong class='lighter red'>Tình trạng: " + r["Status"].ToString() + "</strong></a></td></tr>");
                    HID_AssetKey.Value = r["AssetKey"].ToString();
                    HID_AssetType.Value = r["Type"].ToString();
                }
                if (zTable.Rows.Count >= 2)
                {
                    Lit_Button.Text = "Xin liên hệ Admin phần mềm để giải quyết tình huống này !.";
                }
                else
                {
                    Lit_Button.Text += @"<a class='btn btn-white btn-info btn-bold' data-toggle='modal' href='#mUpdateAsset'><i class='ace-icon fa fa-adjust info'></i>Cập nhật chủ nhà</a>";
                }
            }
            else
            {
                zSb.AppendLine("<tr><td>Chưa có thông tin</td></tr>");
            }
            zSb.AppendLine("</table>");
           // txt_RelateInfo.Text = zTable.Rows.Count.ToString();
           // Lit_Relate.Text = zSb.ToString();
        }
        void LoadExtraData()
        {
            int Status = 6;
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            if (UnitLevel <= 1)
                Employee = 0;

            DataTable zTable = Note_Data.ListInfo_Sent(string.Empty, string.Empty, string.Empty, Employee, Status);
            View_HtmlTable(zTable);
        }

        void View_HtmlTable(DataTable TableSearch)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Thông tin</th>");
            zSb.AppendLine("        <th>Sản phẩm</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (TableSearch.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in TableSearch.Rows)
                {
                    string phone1 = r["Phone1"].ToString() == string.Empty ? string.Empty : r["Phone1"].ToString() + "<br />";
                    string phone2 = r["Phone2"].ToString() == string.Empty ? string.Empty : r["Phone2"].ToString() + "<br />";
                    string mail1 = r["Email1"].ToString() == string.Empty ? string.Empty : r["Email1"].ToString() + "<br />";
                    string mail2 = r["Email2"].ToString() == string.Empty ? string.Empty : r["Email2"].ToString();
                    string status = r["Status"].ToString() == string.Empty ? string.Empty : "Xử lý: " + r["Status"].ToString() + "<br/>";
                    string employee = r["EmployeeName"].ToString() == string.Empty ? string.Empty : r["EmployeeName"].ToString();

                    zSb.AppendLine("            <tr id=" + r["Key"].ToString() + " relatekey=" + r["RelateKey"] + " type=" + r["Type"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>Họ tên: " + r["CustomerName"].ToString() + "<br/>Điện thoại: " + phone1 + phone2 + mail1 + mail2 + "</td>");
                    zSb.AppendLine("               <td>" + r["Product"].ToString() + "<br/>" + r["Asset"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + status + employee + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_Info.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemResult SaveProduct(int Key, int Type, int Status, string InTable, int Project, int Category)
        {
            bool Success = true;
            ItemResult zResult = new ItemResult();
            //Không liên lạc, đã bán, không quan tâm
            if (Status >= 3)
            {
                #region [<cap nhat xử lý tình trang tin>]
                Note_Detail_Info zNote = new Note_Detail_Info();
                zNote.DetailKey = Key;
                zNote.StatusInfo = Status;
                zNote.DateRespone = DateTime.Now;
                zNote.UpdateStatus();
                if (zNote.Message != string.Empty)
                {
                    zResult.Message = zNote.Message;
                    zResult.Result = "Error-1";
                }
                else
                {
                    zResult.Message = "Đã cập nhật thông tin";
                    zResult.Result = "OK-1";
                }
                #endregion

                return zResult;
            }
            //Khách quan tâm
            if (Status == 2)
            {
                #region [<cap nhật tin và lưu khách>]
                Note_Detail_Info zNote = new Note_Detail_Info(Key);
                zNote.StatusInfo = Status;
                zNote.DateRespone = DateTime.Now;
                zNote.UpdateStatus();
                //
                Excel_Detail_Info zExcel = new Excel_Detail_Info(zNote.InfoKey);
                Customer_Info zCustomer = new Customer_Info();
                zCustomer.CategoryKey = 0;
                zCustomer.FirstName = zExcel.FirstName.Trim();
                zCustomer.LastName = zExcel.LastName.Trim();
                zCustomer.CustomerName = zExcel.LastName.Trim() + " " + zExcel.FirstName.Trim();
                zCustomer.Email1 = zExcel.Email1.Trim();
                zCustomer.Email2 = zExcel.Email2.Trim();
                zCustomer.Phone1 = zExcel.Phone1.Trim();
                zCustomer.Phone2 = zExcel.Phone2.Trim();
                zCustomer.Address1 = zExcel.Address1.Trim();
                zCustomer.Address2 = zExcel.Address2.Trim();
                zCustomer.ID = Customer_Data.AutoID();
                zCustomer.EmployeeKey = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);
                zCustomer.DepartmentKey = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"]);
                zCustomer.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zCustomer.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zCustomer.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zCustomer.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zCustomer.Save();

                if (zNote.Message != string.Empty)
                {
                    zResult.Message = zNote.Message;
                    zResult.Result = "Error-2";
                }
                else
                {
                    zResult.Message = "Đã cập nhật thông tin thông tin khách";
                    zResult.Result = "OK-2";
                }
                #endregion

                return zResult;
            }

            Excel_Detail_Info zInfo = new Excel_Detail_Info(Key, Type);
            ItemAsset zAsset = new ItemAsset();
            string idenTable = "";
            if (InTable == "PUL_Resale_Apartment")
                idenTable = "ResaleApartment";
            if (InTable == "PUL_Resale_House")
                idenTable = "ResaleHouse";

            zAsset.ProjectKey = Project.ToString();
            zAsset.CategoryKey = Category.ToString();
            zAsset.AssetID = zInfo.Asset;
            zAsset.ID1 = zInfo.ID1;
            zAsset.ID2 = zInfo.ID2;
            zAsset.ID3 = zInfo.ID3;
            zAsset.AreaWall = zInfo.AreaWall;
            zAsset.AreaWater = zInfo.AreaWater;
            zAsset.PricePurchase = zInfo.Price;
            zAsset.Room = zInfo.Room;
            zAsset.View = zInfo.DirectionView;
            zAsset.Door = zInfo.DirectionDoor;
            zAsset.CategoryKey = zInfo.AssetCategory;
            zAsset.AssetType = idenTable;
            zAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
            zAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            Product_Info zProduct = new Product_Info(zAsset.AssetID, zAsset.AssetType);
            if (zProduct.ItemAsset.AssetKey.ToInt() > 0)
            {
                zResult.Message = "Đã có sản phẩm này bạn có muốn mở lại thông tin";
                zResult.Result = zProduct.ItemAsset.AssetKey;
                zResult.Result2 = zProduct.ItemAsset.AssetType;
            }
            else
            {
                zProduct = new Product_Info();
                zProduct.ItemAsset = zAsset;
                zProduct.Create(idenTable);

                Owner_Info zOwner = new Owner_Info();
                zOwner.CustomerName = zInfo.FullName;
                zOwner.Phone1 = zInfo.Phone1;
                zOwner.Phone2 = zInfo.Phone2;
                zOwner.Email = zInfo.Email1;
                zOwner.Address1 = zInfo.Address1;
                zOwner.Address2 = zInfo.Address2;
                zOwner.AssetKey = zProduct.ItemAsset.AssetKey.ToInt();
                zOwner.Create(idenTable);

                zResult.Message = "Đã cập nhật chủ sở hữu";
                zResult.Result = zProduct.ItemAsset.AssetKey;
                zResult.Result2 = idenTable;

                if (zAsset.Message != string.Empty ||
                    zOwner.Message != string.Empty)
                    Success = false;
            }

            if (Success)
            {
                Note_Detail_Info zNote_Detail = new Note_Detail_Info();
                zNote_Detail.DetailKey = zInfo.DetailKey;
                zNote_Detail.StatusInfo = Status;
                zNote_Detail.DateRespone = DateTime.Now;
                zNote_Detail.ObjectKey = zAsset.AssetKey.ToInt();
                zNote_Detail.ObjectTable = idenTable;
                zNote_Detail.UpdateStatus();
                return zResult;
            }
            else
            {
                zResult.Message = "Lỗi xử lý data";
                zResult.Result = "Error";
                return zResult;
            }
        }

        [WebMethod]
        public static ItemResult SaveInfo(int AutoKey, string CustomerName, string Phone, string Phone2, string Email, string Address, string Address2)
        {
            Excel_Detail_Info zExcel = new Excel_Detail_Info(AutoKey);
            ItemResult zResult = new ItemResult();
            zExcel.FullName = CustomerName;
            zExcel.Phone1 = Phone;
            zExcel.Phone2 = Phone2;
            zExcel.Email1 = Email;
            zExcel.Address1 = Address;
            zExcel.Address2 = Address2;
            zExcel.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zExcel.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zExcel.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zExcel.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zExcel.Update();
            zResult.Message = zExcel.Message;
            return zResult;
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS04";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <a type='button' class='btn btn-white btn-info btn-bold' id='btnEdit' href='#mUpdateInfo' data-toggle='modal'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Điều chỉnh
                        </a>&nbsp";
            }
        }
        #endregion
    }
}