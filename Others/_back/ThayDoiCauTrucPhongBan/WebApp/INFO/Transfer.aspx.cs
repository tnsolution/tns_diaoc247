﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebApp.INFO
{
    public partial class Transfer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }
        void LoadData()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int No = 1;
            StringBuilder zSb = new StringBuilder();
            DataTable zList = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zList = Product_Data.ManagerProduct_Sent(0, 0);
                    break;

                case 2:
                    zList = Product_Data.ManagerProduct_Sent(Department, 0);
                    break;

                default:
                    zList = Product_Data.ManagerProduct_Sent(Department, Employee);
                    break;
            }

            if (zList.Rows.Count > 0)
            {
                foreach (DataRow r in zList.Rows)
                {
                    string TinhTrang = "";
                    int XacNhan = r["Confirmed"] == DBNull.Value ? -1 : r["Confirmed"].ToInt();
                    if (XacNhan == 0)
                        TinhTrang = "Chờ xử lý: " + r["NewStaffName"].ToString();

                    zSb.AppendLine("            <tr>");
                    zSb.AppendLine("                <td>" + No++ + "</td>");
                    zSb.AppendLine("                <td><a href='/SAL/ProductView.aspx?ID=" + r["ObjectID"].ToString() + "&Type=" + r["ObjectTable"].ToString() + "'>" + r["AssetName"].ToString() + "</a></td>");
                    zSb.AppendLine("                <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + TinhTrang + "</td>");
                    zSb.AppendLine("            </tr>");
                }
                LitTable.Text = zSb.ToString();
                LitButton.Text = "<a href='#' class='btn btn-primary btn-white' data-toggle='modal' id='confirm'><i class='ace-icon fa fa-check'></i>&nbsp;Xử lý</a>"; ;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            DataTable zList = Product_Data.ManagerProduct_Sent(DepartmentKey, EmployeeKey);

            string SQL = "";
            foreach (DataRow r in zList.Rows)
            {
                SQL += @"UPDATE SYS_LogTranfer SET ConfirmBy=" + EmployeeKey + " , ConfirmDate = GETDATE(), Confirmed=1 WHERE NewStaff = " + EmployeeKey + " AND Confirmed = 0";
            }

            string Message = CustomInsert.Exe(SQL).Message;
            if (Message != string.Empty)
                LitMessage.Text = "<div class='alert alert-danger'>" + Message + "</div>";
            else
                LitMessage.Text = "<div class='alert alert-danger'>Đã xử lý thành công !.</div>";
        }
    }
}