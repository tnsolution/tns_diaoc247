﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.JOB
{
    public partial class WorkList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadData();
        }

        void LoadData()
        {
            StringBuilder zSbTable = new StringBuilder();
            DataTable zTable = new DataTable();

            zSbTable.AppendLine("<table class='table table-bordered table-hover' id='tableWork'>");
            zSbTable.AppendLine("   <thead>");
            zSbTable.AppendLine("       <tr>");
            zSbTable.AppendLine("           <th>No.</th>");
            zSbTable.AppendLine("           <th>Name</th>");
            zSbTable.AppendLine("           <th>Dealine</th>");
            zSbTable.AppendLine("           <th>CustomerName</th>");
            zSbTable.AppendLine("           <th>Status</th>");
            zSbTable.AppendLine("           <th>Action</th>");
            zSbTable.AppendLine("       </tr>");
            zSbTable.AppendLine("   </thead>");
            zSbTable.AppendLine("   <tbody>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow r = zTable.Rows[i];

                zSbTable.AppendLine("       <tr>");
                zSbTable.AppendLine("           <td data-title='No.'>" + i.ToString() + " </td>");
                zSbTable.AppendLine("           <td data-title='Name.'>" + r["ProjectName"].ToString() + " </td>");
                zSbTable.AppendLine("           <td data-title='Dealine.'>" + Utils.GetShortContent(r["ProjectContent"].ToString(), 20) + " </td>");
                zSbTable.AppendLine("           <td data-title='CustomerName.'>" + r["CustomerName"].ToString() + " </td>");
                zSbTable.AppendLine(@"        <td data-title='Status'>
                                            <button type='button' class='btn btn-xs btn-white btn-info' btnUpdate>
												<i class='ace-icon fa fa-pencil-square-o bigger-120 blue'></i>
												Processing
	                                        </button></td>");
                zSbTable.AppendLine(@"        <td data-title='Action'>                                           
                                            <button type='button' class='btn btn-xs btn-white btn-warning' btnEdit>
												<i class='ace-icon fa fa-pencil bigger-120 orange'></i>
												Edit
											</button>
                                            <button type='button' class='btn btn-xs btn-white btn-danger' btnDelete>
												<i class='ace-icon fa fa-trash-o bigger-120 red2'></i>
												Delete
											</button></td>");
                zSbTable.AppendLine("       </tr>");
            }
            zSbTable.AppendLine("   </tbody>");
            zSbTable.AppendLine("</table>");
            Literal_Table.Text = zSbTable.ToString();
        }
    }
}