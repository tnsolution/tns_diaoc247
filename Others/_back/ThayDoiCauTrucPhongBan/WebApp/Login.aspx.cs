﻿using Lib.SYS;
using System;
using System.Web;
using System.Web.Services;

namespace WebApp
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserLog"] != null)
            {
                HID_User.Value = Request.Cookies["UserLog"].Values["UsernameLogin"];
                HID_Pass.Value = Request.Cookies["UserLog"].Values["PasswordLogin"];

                //HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies["UserLog"];
                //HttpContext.Current.Response.Cookies.Remove("UserLog");
                //currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                //currentUserCookie.Value = null;
                //HttpContext.Current.Response.SetCookie(currentUserCookie);
            }           
        }

        [WebMethod]
        public static ItemReturn CheckLogin(string UserName, string Password, string Remember)
        {
            ItemUser zUser = User_Data.CheckUser(UserName, Password);
            ItemReturn zResult = new ItemReturn();
            if (zUser.Message == "ERR")
            {
                switch (zUser.MessageCode)
                {
                    case "CheckUser_Error01":
                        zResult.Message = "Vui lòng kiểm tra Username và Password";
                        return zResult;

                    case "CheckUser_Error02":
                        zResult.Message = "User này chưa kích hoạt, vui lòng liên hệ Administrator";
                        return zResult;

                    case "CheckUser_Error03":
                        zResult.Message = "User này đã hết hạn, vui lòng liên hệ Administrator";
                        return zResult;

                    default:
                        zResult.Message = "Lỗi đăng nhập !";
                        return zResult;
                }
            }
            else
            {
                HttpCookie zCook = new HttpCookie("UserLog");
                zCook.Values["UserKey"] = zUser.UserKey;
                zCook.Values["DepartmentKey"] = zUser.DepartmentKey;
                zCook.Values["EmployeeKey"] = zUser.EmployeeKey;
                zCook.Values["UnitLevel"] = zUser.UnitLevel;
                zCook.Values["UserName"] = HttpUtility.UrlEncode(zUser.UserName);
                zCook.Values["EmployeeName"] = HttpUtility.UrlEncode(zUser.EmployeeName);
                zCook.Values["Departmentname"] = HttpUtility.UrlEncode(zUser.DepartmentName);            
                zCook.Values["ImgThumb"] = HttpUtility.UrlDecode(zUser.ImageThumb);                
                if (Remember.ToInt() == 1)
                {
                    zCook.Values.Add("UsernameLogin", UserName);
                    zCook.Values.Add("PasswordLogin", Password);
                    zCook.Expires = DateTime.Now.AddDays(15);
                }
                else
                {
                    zCook.Values["UsernameLogin"] = "";
                    zCook.Values["PasswordLogin"] = "";
                    zCook.Expires = DateTime.Now.AddHours(12);
                }
                HttpContext.Current.Response.Cookies.Add(zCook);
                zResult.Message = "Success";
                zResult.Result = "1";
                return zResult;
            }
        }
    }
}