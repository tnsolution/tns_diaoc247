﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Remind.aspx.cs" Inherits="WebApp.Remind" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Nhắc nhở
                <small>
                    <asp:Label ID="lblTitle" runat="server" Text="..."></asp:Label></small>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <asp:HiddenField ID="HID_Remind" runat="server" Value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $('#tableData tbody').on('click', 'td:not(:last-child)', function () {
                var trid = $(this).closest('tr').attr('data-href');
                window.location = trid;
                //window.open(trid);
            });
        });
    </script>
</asp:Content>
