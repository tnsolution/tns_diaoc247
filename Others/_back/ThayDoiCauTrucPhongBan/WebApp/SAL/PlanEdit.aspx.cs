﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class PlanEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_PlanKey.Value = Request["ID"];

                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT AutoKey, Product FROM SYS_Categories WHERE TYPE = 34", false);

                LoadInfo();
                LoadTable();
            }
        }

        void LoadInfo()
        {
            Plan_Info zInfo = new Plan_Info(HID_PlanKey.Value.ToInt());
            Lit_TitlePage.Text = "Cài đặt chi tiết mục tiêu tháng " + zInfo.PlanDate.ToString("MM/yyyy");
            txt_Month.Value = zInfo.PlanDate.ToString("MM/yyyy");
            DDL_Department.SelectedValue = zInfo.DepartmentKey.ToString();
            DDL_Employee.SelectedValue = zInfo.EmployeeKey.ToString();
            txt_Description.Value = zInfo.Note;

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy") + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }
        void LoadTable()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Plan_Rec_Data.List(HID_PlanKey.Value.ToInt());

            zSb.AppendLine("<table class='table  table-bordered table-hover' id='tblPlanRecord'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Mục tiêu</th>");
            zSb.AppendLine("        <th>Yêu cầu/ tháng</th>");
            zSb.AppendLine("        <th>Tỷ lệ %</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                int tongYeuCau = 0;
                float tongKPI = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["ID"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Value"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ValueKPI"].ToString() + "</td>");
                    zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a class='red' btn='btnDeleteRec' href='#'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");

                    tongYeuCau += r["Value"].ToInt();
                    tongKPI += r["ValueKPI"].ToFLoat();
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>" + (i++) + "</td>");
                zSb.AppendLine("               <td>Tổng</td>");
                zSb.AppendLine("               <td>" + tongYeuCau + "</td>");
                zSb.AppendLine("               <td>" + tongKPI + "</td>");
                zSb.AppendLine("               <td></td>");
                zSb.AppendLine("            </tr>");
            }
            else
                zSb.AppendLine("<td></td><td colspan='3'></td>");

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemReturn DeletePlan(int Key)
        {
            ItemReturn zResult = new ItemReturn();
            Plan_Info zInfo = new Plan_Info();
            zInfo.Key = Key;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn SavePlan(int Key, string Department, string Employee, string Description, string Date)
        {
            int M = Date.Split('/')[0].ToInt();
            int Y = Date.Split('/')[1].ToInt();

            ItemReturn zResult = new ItemReturn();
            Plan_Info zInfo = new Plan_Info(Key);
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = Employee.ToInt();
            zInfo.PlanDate = new DateTime(Y, M, 1);
            zInfo.Note = Description;

            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemPlan[] GetListRecord(int Key)
        {
            DataTable zTable = Plan_Rec_Data.List(Key);
            List<ItemPlan> ListAsset = zTable.DataTableToList<ItemPlan>();
            return ListAsset.ToArray();
        }

        [WebMethod]
        public static ItemPlan GetRecord(int AutoKey)
        {
            ItemPlan zItem = new ItemPlan();
            Plan_Rec_Info zInfo = new Plan_Rec_Info(AutoKey);
            zItem.ID = zInfo.ID.ToString();
            zItem.CategoryKey = zInfo.CategoryKey.ToString();
            zItem.Value = zInfo.Value.ToString();
            zItem.ValueKPI = zInfo.ValueKPI.ToString();
            return zItem;
        }

        [WebMethod]
        public static ItemReturn SaveDetailRecord(int CategoryKey, int Key, string Value, string ValueKPI)
        {
            Plan_Rec_Info zInfo = new Plan_Rec_Info(Key, CategoryKey);
            ItemReturn zResult = new ItemReturn();
            zInfo.PlanKey = Key;
            zInfo.CategoryKey = CategoryKey;
            zInfo.Value = Value.ToInt();
            float temp = 0;
            float.TryParse(ValueKPI, out temp);
            zInfo.ValueKPI = temp;
            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            zResult.Result = zInfo.ID.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemReturn DeleteRecord(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Plan_Rec_Info zInfo = new Plan_Rec_Info(AutoKey);
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}