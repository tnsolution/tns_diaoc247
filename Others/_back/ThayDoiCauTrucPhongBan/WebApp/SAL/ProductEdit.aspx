﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductEdit.aspx.cs" Inherits="WebApp.SAL.ProductEdit" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        .datepicker {
            z-index: 9999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <a type="button" class="btn btn-white btn-default btn-bold" href="ProductList.aspx" onclick="$('.se-pre-con').fadeIn('slow');">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left" id="Tabs">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="green ace-icon fa fa-hand-o-right bigger-110"></i>
                                Thông tin làm việc
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info2">
                                <i class="pink ace-icon fa fa-briefcase bigger-110"></i>
                                Thông tin cơ bản
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#picture">
                                <i class="blue ace-icon fa fa-photo bigger-110"></i>
                                Hình ảnh
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#guest">
                                <i class="blue ace-icon fa fa-phone bigger-110"></i>
                                Chủ nhà
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#post">
                                <i class="blue ace-icon fa fa-archive bigger-110"></i>
                                Tin đăng
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#allow">
                                <i class="blue ace-icon fa fa-circle bigger-110"></i>
                                Chia thông tin
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane in active">
                            <div class="col-xs-12 no-padding">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Pháp lý</label>
                                        <div class="col-sm-2 no-padding">
                                            <asp:DropDownList ID="DDL_Legal" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">Nhu cầu</label>
                                        <div class="col-sm-2 no-padding">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="ace" value="CN" name="chkPurpose" id="chkCN" />
                                                    <span class="lbl">Chuyển nhượng</span>
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: none">
                                                <label>
                                                    <input type="checkbox" class="ace" value="KT" name="chkPurpose" />
                                                    <span class="lbl">Khai thác kinh doanh</span>
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="ace" value="CT" name="chkPurpose" id="chkCT" />
                                                    <span class="lbl">Cho thuê</span>
                                                </label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Ưu tiên</label>
                                        <div class="col-sm-2 no-padding">
                                            <input name="switch-field-1" class="ace ace-switch" type="checkbox" runat="server" id="chkHot" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tình trạng</label>
                                        <div class="col-sm-2 no-padding">
                                            <asp:DropDownList ID="DDL_Status" CssClass="select2" runat="server" AppendDataBoundItems="true" required="">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label" id="giaban">Giá bán VNĐ<sup>*</sup></label>
                                        <div class="col-sm-2 no-padding">
                                            <input type="text" runat="server" class="form-control" id="txt_PriceVND" placeholder="Nhập số" moneyonly value="0" />
                                            <input type="text" runat="server" class="form-control" id="txt_PriceUSD" placeholder="Nhập số" moneyonly value="0" />
                                        </div>
                                        <div class="col-sm-2 no-padding" style="margin-top: 5px">
                                            <input name="switch-field-1" class="ace ace-switch" type="checkbox" runat="server" id="Usdswitch1" />
                                            <span class="lbl" data-lbl="USD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VNĐ"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ngày liên hệ lại</label>
                                        <div class="col-sm-2 no-padding">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                    id="txt_DateContractEnd" placeholder="dd/MM/yyyy" />
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label" id="giathue">Giá thuê VNĐ<sup>*</sup></label>
                                        <div class="col-sm-2 no-padding">
                                            <input type="text" runat="server" class="form-control" id="txt_PriceRentVND" placeholder="Nhập số" moneyonly value="0" />
                                            <input type="text" runat="server" class="form-control" id="txt_PriceRentUSD" placeholder="Nhập số" moneyonly value="0" />
                                        </div>
                                        <div class="col-sm-2 no-padding" style="margin-top: 5px">
                                            <input name="switch-field-1" class="ace ace-switch" type="checkbox" runat="server" id="Usdswitch2" />
                                            <span class="lbl" data-lbl="USD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VNĐ"></span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nội thất</label>
                                        <div class="col-sm-2 no-padding">
                                            <asp:DropDownList ID="DDL_Furnitur" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label" id="giamua">Giá mua VNĐ<sup>*</sup></label>
                                        <div class="col-sm-2 no-padding">
                                            <input type="text" runat="server" class="form-control" id="txt_PricePurchase_VND" placeholder="Nhập số" moneyonly value="0" />
                                            <input type="text" runat="server" class="form-control" id="txt_PricePurchase_USD" placeholder="Nhập số" moneyonly value="0" />
                                        </div>
                                        <div class="col-sm-2 no-padding" style="margin-top: 5px">
                                            <input name="switch-field-1" class="ace ace-switch" type="checkbox" runat="server" id="Usdswitch3" />
                                            <span class="lbl" data-lbl="USD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VNĐ"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ghi chú</label>
                                        <div class="col-sm-10 no-padding">
                                            <textarea runat="server" class="form-control" id="txt_Description" placeholder="..." rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info2" class="tab-pane">
                            <div class="col-xs-12 no-padding">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Dự án<sup>*</sup></label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Project" CssClass="select2" runat="server" required AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Loại sản phẩm</label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="DDL_Category" runat="server" CssClass="select2" required AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Mã căn (mã SP)<sup>*</sup></label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_AssetID" runat="server" placeholder="Nhập text" CssClass="form-control" required></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_Id1" runat="server" placeholder="Tháp" CssClass="form-control" required></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_Id2" runat="server" placeholder="Tầng" CssClass="form-control" required></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_Id3" runat="server" placeholder="Căn" CssClass="form-control" required></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div type="ResaleApartment">
                                            <label class="col-sm-2 control-label">DT tim tường m<sup>2</sup></label>
                                            <div class="col-sm-2">
                                                <input id="txt_WallArea" runat="server" class="form-control" placeholder="Nhập số" type="text" required="" dientichonly />
                                            </div>
                                        </div>
                                        <div type="ResaleHouse">
                                            <label class="col-sm-2 control-label">DT đất m<sup>2</sup></label>
                                            <div class="col-sm-2">
                                                <input id="txt_GroundArea" runat="server" class="form-control" placeholder="Nhập số" type="text" dientichonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div type="ResaleApartment">
                                            <label class="col-sm-2 control-label">DT thông thủy m<sup>2</sup></label>
                                            <div class="col-sm-2">
                                                <input id="txt_WaterArea" runat="server" class="form-control" placeholder="Nhập số" type="text" required="" dientichonly />
                                            </div>
                                        </div>
                                        <div type="ResaleHouse">
                                            <label class="col-sm-2 control-label">Tổng DT XD</label>
                                            <div class="col-sm-2">
                                                <input id="txt_GroundTotal" runat="server" class="form-control" placeholder="Nhập số" type="text" dientichonly />
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Hướng cửa</label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="DDL_DirectionDoor" runat="server" CssClass="select2" required="">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="Đông" Text="Đông"></asp:ListItem>
                                                <asp:ListItem Value="Tây" Text="Tây"></asp:ListItem>
                                                <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                                                <asp:ListItem Value="Bắc" Text="Bắc"></asp:ListItem>
                                                <asp:ListItem Value="Đông Nam" Text="Đông Nam"></asp:ListItem>
                                                <asp:ListItem Value="Tây Bắc" Text="Tây Bắc"></asp:ListItem>
                                                <asp:ListItem Value="Đông Bắc" Text="Đông Bắc"></asp:ListItem>
                                                <asp:ListItem Value="Tây Nam" Text="Tây Nam"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Số phòng ngủ</label>
                                        <div class="col-sm-2">
                                            <input id="txt_Room" runat="server" class="form-control" placeholder="Nhập text" type="text" required="" dientichonly />
                                        </div>
                                        <div type="ResaleApartment">
                                            <label class="col-sm-2 control-label">Hướng view</label>
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="DDL_DirectionView" runat="server" CssClass="select2" required="">
                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="Đông" Text="Đông"></asp:ListItem>
                                                    <asp:ListItem Value="Tây" Text="Tây"></asp:ListItem>
                                                    <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                                                    <asp:ListItem Value="Bắc" Text="Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Đông Nam" Text="Đông Nam"></asp:ListItem>
                                                    <asp:ListItem Value="Tây Bắc" Text="Tây Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Đông Bắc" Text="Đông Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Tây Nam" Text="Tây Nam"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div type="ResaleHouse">
                                            <label class="col-sm-2 control-label">Cấu trúc</label>
                                            <div class="col-sm-2">
                                                <input id="txt_Sctruc" runat="server" class="form-control" placeholder="Nhập text" type="text" />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="picture" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Hình ảnh sản phẩm (bạn có thể chọn nhiều tập tin)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
                                </div>
                                <div class="col-xs-7">
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Upload
                                    </a>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="ace-thumbnails clearfix">
                                        <asp:Literal ID="Lit_ListPicture" runat="server"></asp:Literal>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="guest" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Chủ nhà của sản phẩm này
                                              <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mGuest" data-toggle="modal">
                                                  <i class="ace-icon fa fa-plus"></i>
                                                  Add
                                              </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_Owner" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="post" class="tab-pane">
                            <div class="col-xs-12">
                                <ul class="text-warning list-unstyled spaced">
                                    <li><i class="ace-icon fa fa-exclamation-triangle"></i>Dùng để đăng tin sản phẩm                                           
                                    </li>
                                </ul>
                                <div class="space-6"></div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Post</label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="DDL_WebPublished" CssClass="form-control select2" runat="server">
                                                <asp:ListItem Value="0" Text="Không đăng tin"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Đăng tin"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tiêu đề tin</label>
                                        <div class="col-sm-10">
                                            <input type="text" runat="server" class="form-control" id="txt_WebTitle" placeholder="Nội dung tóm tắt" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <textarea runat="server" class="form-control" id="txt_WebContent"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="allow" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Cho phép nhân viên chia sẽ được cập nhật sản phẩm này</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-8">
                                            <button type='button' class='btn btn-white btn-default btn-round btn-sm' id='btnSaveShare'>
                                                <i class="ace-icon fa fa-plus"></i>Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_AllowSearch" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mGuest">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin liên hệ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Họ tên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT1</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SĐT2</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Email" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ liên lạc</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveGuest" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_GuestKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Purpose" runat="server" Value="0" />
    <asp:Button ID="btnUploadImg" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadImg_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/template/tinymce/tinymce.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <asp:Literal ID="Lit_USD" runat="server"></asp:Literal>
    <script src="/SAL/ProductEdit.js"></script>
</asp:Content>
