﻿$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(document).ready(function () {
    $(".modal.aside").ace_aside();
    $(".select2").select2({ width: "100%" });
    $("#txt_AssetID").autocomplete({
        minLength: 1,
        autoFill: true,
        source: function (request, response) {
            $.ajax({
                url: '/SAL/ProductList.aspx/GetAsset',
                data: "{ 'prefix': '" + request.term + "','project':'" + $("[id$=DDL_Project2]").val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split(';')[1],
                            val: item.split(';')[0]
                        }
                    }))
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("[id$=HID_Asset]").val(i.item.val);
            window.location = "ProductEdit.aspx?ID=" + i.item.val + "&Type=ResaleApartment";
        }
    });
    $("#txt_Name").on('keyup', function (e) {
        //if (e.keyCode == 13) {
        //    $("#btnSearch").trigger("click");
        //}
    });
    $("#viewExten").click(function () {
        //alert($("[id$=DDL_Project]").val());
        window.location = "/SAL/ProductGallery.aspx?ID=" + $("[id$=DDL_Project]").val();
    });
    //
    $("[id$=DDL_Employee]").on('change', function (e) {
        var val = this.value;
        if (val != 0)
            $('#chkPersonal').prop('checked', true);
        else
            $('#chkPersonal').prop('checked', false);
    });
    $("[id$=DDL_Department]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetProject",
                data: JSON.stringify({
                    "Department": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                success: function (msg) {
                    var District = $("[id$=DDL_Project]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn dự án--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });

    $("[id$=DDL_Project]").on('change', function (e) {
        if (this.value != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetCategoryAsset",
                data: JSON.stringify({
                    "ProjectKey": this.value,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () { },
                success: function (msg) {
                    var District = $("[id$=DDL_Category]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
            //
            $.ajax({
                type: "POST",
                url: "/SAL/ProductList.aspx/PrimaryProject",
                data: JSON.stringify({
                    "ProjectKey": this.value,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () { },
                success: function (msg) {
                    $("[id$=HID_View]").val(msg.d);
                },
                complete: function () { },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //
    $("#tblData tbody").on("click", "tr", function () {
        var trid = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('fid');
        $('.se-pre-con').fadeIn('slow');
        window.location = "ProductView.aspx?ID=" + trid + "&Type=" + type;
    });
    $("#tblData tbody").on('mouseover', "tr", function () {
        //var id = $(this).attr('id');
        //var fid = $(this).attr('fid');
        //setTimeout(function () {
        //    $.ajax({
        //        type: "POST",
        //        url: "/SAL/ProductList.aspx/GetRelate",
        //        data: JSON.stringify({
        //            "AssetKey": id,
        //            "AssetType": fid,
        //        }),
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        beforeSend: function () { },
        //        success: function (msg) {
        //            if (msg.d.length > 0) {
        //                $.gritter.add({
        //                    title: 'Đề xuất thông tin khách hàng!',
        //                    text: msg.d,
        //                    sticky: false,
        //                    time: '',
        //                    class_name: "gritter-info",
        //                    before_open: function () {
        //                        if ($('.gritter-item-wrapper').length >= 3) {
        //                            return false;
        //                        }
        //                    },
        //                    mouse_over: close,
        //                });
        //            }
        //        },
        //        complete: function () {
        //        },
        //        error: function (xhr, ajaxOptions, thrownError) {
        //            console.log(xhr.status);
        //            console.log(xhr.responseText);
        //            console.log(thrownError);
        //        }
        //    });
        //}, 3000);
    });
    $("#btnSearch").click(function (e) {
        if ($("[id$=HID_View]").val() == 1)
            $("#viewExten").show();
        else
            $("#viewExten").hide();
        //
        var view = $("[id$=DDL_View]").val();
        var door = $("[id$=DDL_Door]").val();
        var table = $("[id$=DDL_Department]").val();
        if (table <= 0) {
            Page.showNotiMessageInfo("...", "Bản phải chọn tìm sản phẩm là thuộc khu vực");
            return;
        }
        var project = $("[id$=DDL_Project]").val();
        var category = $("[id$=DDL_Category]").val();
        if (project == null || project == undefined || project == 0) {
            project = 0;
            alert("Vui lòng chọn 1 dự án trước khi tìm các thông tin khác ?");
            return;
        }
        if (category == null)
            category = 0;
        var id1 = $("#txt_ID1").val();
        var id2 = $("#txt_ID2").val();
        var id3 = $("#txt_ID3").val();
        var bed = $("#txt_Bed").val();
        var name = $("#txt_Name").val();
        var personal = 0;
        if ($('#chkPersonal').is(':checked'))
            personal = 1;
        var status = "", purpose = "";
        $("input[name='chkStatus']:checked").each(function (i) {
            status += $(this).val() + ",";
        });
        $("input[name='chkPurpose']:checked").each(function (i) {
            purpose += $(this).val() + "/ ";
        });
        var employee = $("[id$=DDL_Employee]").val();
        if (employee == null)
            employee = 0;
        $.ajax({
            type: "POST",
            url: "/SAL/ProductList.aspx/Search",
            data: JSON.stringify({
                "Department": table,
                "Project": project,
                "Category": category,
                "ID1": id1,
                "ID2": id2,
                "ID3": id3,
                "Bed": bed,
                "Name": name,
                "Personal": personal,
                "Status": status,
                "Purpose": purpose,
                "Employee": employee,
                "View": view,
                "Door": door,
                "Customer": $("#txt_Customer").val(),
                "Phone": $("#txt_Phone").val()
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $('#tblData > tbody').empty();
                $('#tblData > tbody:last').append($.parseHTML(msg.d));

                makeSelectFromColumn("duan", 2);
                makeSelectFromColumn("loaisp", 4);
            },
            complete: function () {
                $('#mSearch').modal('hide');
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnInitProduct").click(function () {
        var project = $('[id$=DDL_Project2]').val();
        var table = $("[id$=DDL_Table2]").val();
        var note = $("#txt_Asset_Description").val();
        var assetid = $("#txt_AssetID").val();
        if (table <= 0) {
            Page.showNotiMessageInfo("...", "Bản phải chọn sản phẩm là [chung cư, liền kế, đất nền, ... ]");
            return;
        }
        if (project <= 0) {
            Page.showNotiMessageInfo("...", "Bạn phải chọn dự án cho sản phẩm");
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/SAL/ProductEdit.aspx/InitProduct',
            data: JSON.stringify({
                "Type": table,
                "Project": project,
                "AssetID": assetid,
                "Note": note
            }),
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                var trid = r.d.Result;
                var type = r.d.Result2;
                if (r.d.Result3 != "") {
                    if (r.d.Result3 == "Duplicate") {
                        if (confirm(r.d.Message)) {
                            window.location = "ProductView.aspx?ID=" + trid + "&Type=" + type;
                        }
                    }
                    else if (r.d.Result3 == "NotAllow") {
                        alert(r.d.Message);
                        return;
                    }
                }
                else {
                    window.location = "ProductEdit.aspx?ID=" + trid + "&Type=" + type;
                }
            },
            error: function () {
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });

});