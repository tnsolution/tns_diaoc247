﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ProductView : System.Web.UI.Page
    {
        //KPI tham số categoryplan = 1 là từ công việc, 2 là từ cập nhật sản phẩm
        //category cap nhat san pham la 307

        const int _Category = 307;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AssetKey.Value = Request["ID"];
                if (Request["Type"] != null)
                    HID_AssetType.Value = Request["Type"];

                Tools.DropDown_DDL(DDL_Legal, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 6", false);
                Tools.DropDown_DDL(DDL_Furnitur2, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_StatusAsset2, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);

                CheckRoles();
                LoadAsset();
            }
        }

        #region [Xử lý sản phẩm chuyển giao]
        [WebMethod]
        public static ItemReturn CheckTransfer(int AssetKey)
        {
            ItemReturn zResult = new ItemReturn();
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Exists = Product_Data.CheckTransfer(AssetKey, EmployeeKey);
            if (Exists > 0)
            {
                zResult.Message = "Sản phẩm này đã chuyển cho bạn xử lý, vui lòng xác nhận !";
                zResult.Result = Exists.ToString();
            }
            return zResult;
        }
        [WebMethod]
        public static ItemReturn ExeTransfer(int AssetKey)
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            string SQL = @"UPDATE SYS_LogTranfer SET ConfirmBy=" + EmployeeKey + " , ConfirmDate = GETDATE(), Confirmed=1 WHERE NewStaff = " + EmployeeKey + " AND Confirmed = 0 AND ObjectID = " + AssetKey;

            string Message = CustomInsert.Exe(SQL).Message;
            ItemReturn zResult = new ItemReturn();
            if (Message != string.Empty)
                zResult.Message = Message;
            else
                zResult.Message = string.Empty;
            return zResult;
        }
        #endregion

        #region [Xử lý đăng ký nhận sản phẩm chưa được ký gửi]
        [WebMethod]
        public static ItemReturn AssetBookingEmployee(int AssetKey, string AssetType)
        {
            ItemReturn zResult = new ItemReturn();
            Product_Info zInfo = new Product_Info(AssetKey, AssetType);
            if (zInfo.ItemAsset.EmployeeKey.ToInt() == 0 &&
              zInfo.ItemAsset.DepartmentKey.ToInt() == 0 &&
              zInfo.ItemAsset.CreatedBy == string.Empty &&
              zInfo.ItemAsset.CreatedDate == string.Empty &&
              zInfo.ItemAsset.CreatedName == string.Empty)
            {
                zInfo.ItemAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zInfo.ItemAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.UpdateManagerment(AssetType);
            }
            zResult.Message = zInfo.ItemAsset.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn AssetCheckEmployee(int AssetKey, string AssetType)
        {
            ItemReturn zResult = new ItemReturn();
            Product_Info zInfo = new Product_Info(AssetKey, AssetType);
            if (zInfo.ItemAsset.EmployeeKey.ToInt() == 0 &&
              zInfo.ItemAsset.DepartmentKey.ToInt() == 0 &&
              zInfo.ItemAsset.CreatedBy == string.Empty &&
              zInfo.ItemAsset.CreatedDate == string.Empty &&
              zInfo.ItemAsset.CreatedName == string.Empty)
            {
                zResult.Message = "Sản phẩm này chưa được ký gửi, bạn có muốn đăng ký nhận xử lý thông tin ?.";
            }
            else
                zResult.Message = "";

            return zResult;
        }
        #endregion

        #region [Xử lý load data]
        void LoadAsset()
        {
            int AssetKey = HID_AssetKey.Value.ToInt();
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string AssetType = HID_AssetType.Value;

            StringBuilder Owner = new StringBuilder();
            StringBuilder zSb = new StringBuilder();
            Product_Info zInfo = new Product_Info(AssetKey, AssetType, CurrentEmployee);
            ItemAsset zAsset = zInfo.ItemAsset;

            HID_ProjectKey.Value = zAsset.ProjectKey;
            HID_EmployeeKey.Value = zAsset.EmployeeKey;
            txt_Asset.Text = zAsset.AssetID;

            zSb = new StringBuilder();
            #region [tindang]
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tiêu đề</div><div class='profile-info-value' id='tieude' value>" + zAsset.WebTitle + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội dung</div><div class='profile-info-value' id='noidung' value>" + zAsset.WebContent + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng tin</div><div class='profile-info-value' id='tinhtrang' value>" + (zAsset.WebPublished.ToInt() == 0 ? "Chưa đăng tin" : "Đã đăng tin") + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            Lit_InfoArticle.Text = zSb.ToString();
            #endregion            

            #region [html view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "  <input name='switch-field-1' class='ace ace-switch' type='checkbox' id='chkPriceType' />";
            ViewMoney += "  <span class='lbl'>&nbsp USD</span>";
            ViewMoney += "  </div>";
            #endregion

            zSb = new StringBuilder();
            #region [Asset Info]
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang' value>" + zAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Pháp lý</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='phaply' value>" + zAsset.Legal + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe' value>" + zAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán</div>");
            zSb.AppendLine("        <div class='profile-info-value'><div style='display:none' view=usd class='pull-left giatien' id='giaban1' value>" + zAsset.Price_USD + " (USD)</div><div style='display:none' view=vnd class='pull-left giatien' id='giaban2' value>" + zAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê</div>");
            zSb.AppendLine("        <div class='profile-info-value'><div style='display:none' view=usd class='pull-left giatien' id='giathue1' value>" + zAsset.PriceRent_USD + " (USD)</div><div style='display:none' view=vnd class='pull-left giatien' id='giathue2' value>" + zAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua</div>");
            zSb.AppendLine("        <div class='profile-info-value'><div style='display:none' view=usd class='pull-left giatien' id='giamua1' value>" + zAsset.PricePurchase_USD + " (USD)</div><div style='display:none' view=vnd class='pull-left giatien' id='giamua2' value>" + zAsset.PricePurchase_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat' value>" + zAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau' value>" + zAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");

            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.Description + "</div>");
            zSb.AppendLine("    </div>");

            if (UnitLevel <= 1)
                zAsset.Edit = 1;
            else
            {
                if (zAsset.EmployeeKey.ToInt() == CurrentEmployee ||
                    zAsset.IsShare.ToInt() == 1)
                {
                    zAsset.Edit = 1;
                    Owner.AppendLine("<div class='profile-user-info profile-user-info-striped' id='chunha1'>");
                    Owner.AppendLine("<div class='profile-info-row'>");
                    Owner.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    Owner.AppendLine("    <div class='profile-info-value' value id='tenchunha'>" + zAsset.CustomerName + "</div>");
                    Owner.AppendLine("</div>");
                    Owner.AppendLine("<div class='profile-info-row'>");
                    Owner.AppendLine("    <div class='profile-info-name'>Số điện thoại</div>");
                    Owner.AppendLine("    <div class='profile-info-value' value id='sdtchunha'>" + zAsset.Phone1.HtmlPhone() + zAsset.Phone2.HtmlPhone() + "</div>");
                    Owner.AppendLine("</div>");
                    Owner.AppendLine("</div>");
                }
                else
                {
                    zAsset.Edit = 0;

                    zSb.AppendLine("    <div class='profile-info-row'>");
                    zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                    zSb.AppendLine("        <div class='profile-info-value' id='sales' value>" + zAsset.EmployeeName + "</div>");
                    zSb.AppendLine("    </div>");
                }
            }

            zSb.AppendLine("</div>");
            Lit_InfoAsset.Text = zSb.ToString();
            Lit_InfoOwner.Text = Owner.ToString();
            #endregion

            zSb = new StringBuilder();
            #region [Extra Info]
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Dự án</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='duan' value>" + zAsset.ProjectName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã sản phẩm</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='masp' value>" + zAsset.AssetID + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Loại sản phẩm</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='loaisp' value>" + zAsset.CategoryName + "</div>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Số phòng ngủ</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='sophong' value>" + zAsset.Room + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dttimtuong' value>" + zAsset.AreaWall + " </div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích thông thủy</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dtthongthuy' value>" + zAsset.AreaWater + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Hướng view</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='huongview' value>" + zAsset.View + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Hướng cửa</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='huongcua' value>" + zAsset.Door + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");

            Lit_ExtraAsset.Text = zSb.ToString();
            #endregion

            zSb = new StringBuilder();
            #region [Mana Info]
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người khởi tạo</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoikhoitao' value>" + zAsset.CreatedName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày khởi tạo</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaykhoitao' value>" + zAsset.CreatedDate + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người cập nhật</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoicapnhat' value>" + zAsset.ModifiedName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày cập nhật</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaycapnhat' value>" + zAsset.ModifiedDate + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Người quản lý</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nguoiquanly' value>" + zAsset.EmployeeName + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("</div>");
            Lit_Info.Text = zSb.ToString();
            #endregion          

            HID_Edit.Value = zAsset.Edit.ToString();
            LoadData(zAsset);
            LoadFile(zAsset.AssetKey.ToInt(), AssetType);
            LoadOwner(zAsset.EmployeeKey, zAsset.EmployeeName, zAsset.Edit);
            LoadAllow();
        }
        void LoadAllow()
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblShare'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>Chia sản phẩm cho </div><div class='profile-info-value'>" + zTable.Rows[i]["EmployeeName"].ToString() + "</div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            Lit_AllowSearch.Text = zSb.ToString();
        }
        void LoadOwner(string EmployeeKey, string EmployeeName, int Edit)
        {
            string zTableSelect = HID_AssetType.Value;
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Owner_Data.List(HID_AssetType.Value, HID_AssetKey.Value.ToInt());
            string color = "border: 1px solid #7f209c;";
            if (zTable.Rows.Count > 0)
            {
                bool IsAllow = false;
                if (Edit == 1)
                    IsAllow = true;
                string Phone = "Bạn chưa được <b>" + EmployeeName + "</b> cho xem thông tin này";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    if (i != 0)
                        color = "";

                    DataRow r = zTable.Rows[i];
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' style='" + color + "'>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>" + r["CustomerName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a onclick='UpdateGuest(" + r["OwnerKey"] + ",\"" + zTableSelect + "\")' href='#' class='red' style='display:none'><i class='ace-icon fa fa-pencil-square-o bigger-130'></i>&nbsp;Sửa</a></div></div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    if (IsAllow)
                        zSb.AppendLine("    <div class='profile-info-value' value>" + r["Phone1"].HtmlPhone() + r["Phone2"].HtmlPhone() + "</div>");
                    else
                        zSb.AppendLine("    <div class='profile-info-value' value>" + Phone.HtmlPhone() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Email</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>" + r["Email1"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Địa chỉ</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>Liên lạc: " + r["Address1"].ToString() + "<br/> Thường trú: " + r["Address2"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Ngày cập nhật</div>");
                    zSb.AppendLine("    <div class='profile-info-value' value>" + r["CreatedDate"].ToDateString() + "</div>");
                    zSb.AppendLine(" </div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='space-6'></div>");
                }
            }
            Lit_Owner.Text = zSb.ToString();
        }
        void LoadFile(int Key,string AssetType)
        {
            List<ItemDocument> zList = Document_Data.List(135, Key, AssetType);
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine(@"<a class='' href='" + item.ImageUrl + "' title=''><img style='border:1px solid;' width=150 height=150 src='" + item.ImageThumb.ToThumb() + "'></a>");
                }
                Lit_File.Text = zSb.ToString();
            }
        }
        void LoadData(ItemAsset Item)
        {
            string DDL_TinhTrang = Tools.Html_Select("DDL_Status", "Tình trạng", "form-control input-sm", "SELECT Product, Product FROM dbo.SYS_Categories WHERE Type = 4");
            string DDL_Category = Tools.Html_Select("DDL_Category", "Loại SP", "form-control input-sm", "SELECT C.CategoryName, C.CategoryName FROM PUL_Project_Category A LEFT JOIN PUL_Category C ON C.CategoryKey = A.CategoryKey WHERE A.ProjectKey=" + HID_ProjectKey.Value);
            string DDL_Purpose = @"
<select name='DDL_Purpose' id='DDL_Purpose' class='form-control input-sm' onchange='myFunction(this,4);'>
	<option selected='selected' value='0' disabled='disabled'>Nhu cầu</option>
    <option value='Chuyển nhượng'></option>
	<option value='Chuyển nhượng'>Chuyển nhượng</option>
	<option value='Cho thuê'>Cho thuê</option>
</select>";
            string txt_Name = "<input type='text' id='txt_Name' class='form-control input-sm' placeholder='Mã căn' onkeyup='myFunction(this, 1)' />";
            string txt_Bed = @"
<select name='DDL_Room' id='DDL_Room' class='form-control input-sm' onchange='myFunction(this,2);'>
    <option selected='selected' value='0' disabled='disabled'>PN</option>
    <option value='1'></option>
	<option value='1'>1 PN</option>
	<option value='2'>2 PN</option>
	<option value='3'>3 PN</option>
    <option value='3'>4 PN</option>
    <option value='3'>5 PN</option>
</select>";
            Lit_RightTitle.Text = Item.ProjectName;
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;
            StringBuilder zSb = new StringBuilder();
            List<Product_Item> zList = new List<Product_Item>();
            zList = Product_Data.Search(Item.ProjectKey.ToInt(), 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0, 500);
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblExtraData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th colspan=2>" + txt_Name + "</th>");
            zSb.AppendLine("        <th>" + txt_Bed + "</th>");
            zSb.AppendLine("        <th>" + DDL_Category + "</th>");
            zSb.AppendLine("        <th>" + DDL_Purpose + "</th>");
            zSb.AppendLine("        <th>" + DDL_TinhTrang + "</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (Product_Item r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td style='width:5%'>" + i++ + "</td>");
                    zSb.AppendLine("                <td class='td2'>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td class='td2'>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td class='td5'>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_RightTable.Text = zSb.ToString();
        }
        #endregion

        #region [Xử lý nhắc nhở]
        [WebMethod]
        public static ItemReturn CheckReminder(int AssetKey)
        {
            ItemReturn zResult = new ItemReturn();
            int Exists = Product_Data.CheckReminder(AssetKey);
            if (Exists > 0)
            {
                zResult.Message = "Sản phẩm này đã đến hẹn, chưa được xử lý, vui lòng kiểm tra ngày liên hệ !";
                zResult.Result = Exists.ToString();
            }
            return zResult;
        }
        [WebMethod]
        public static ItemReturn ExeReminder(int RemindKey)
        {
            ItemReturn zResult = new ItemReturn();
            Notification_Info zInfo = new Notification_Info();
            zInfo.ID = RemindKey;
            zInfo.IsRead = 1;
            zInfo.ReadDate = DateTime.Now;
            zInfo.UpdateReaded();
            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
                zResult.Result = "0";
            }
            else
            {
                zResult.Message = "Đã xử lý thành công !";
                zResult.Result = "1";
            }
            return zResult;
        }
        #endregion

        #region [Xử lý box bên phải]
        //get 1 thong tin khi click ben phai
        [WebMethod]
        public static ItemAsset GetAsset(int AssetKey, string AssetType)
        {
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            Product_Info zInfo = new Product_Info(AssetKey, AssetType, CurrentEmployee);
            ItemAsset zAsset = zInfo.ItemAsset;
            zAsset.Edit = 0;
            if (UnitLevel <= 1)
                zAsset.Edit = 1;
            else
               if (zAsset.EmployeeKey.ToInt() == CurrentEmployee ||
                zAsset.IsShare.ToInt() == 1)
                zAsset.Edit = 1;

            return zAsset;
        }
        [WebMethod]
        public static ItemReturn GetOwner(int AssetKey, string AssetType, int Edit, int EmployeeKey, string EmployeeName)
        {
            ItemReturn Result = new ItemReturn();
            StringBuilder zSb = new StringBuilder();
            StringBuilder zTop = new StringBuilder();
            DataTable zTable = Owner_Data.List(AssetType, AssetKey);
            if (zTable.Rows.Count > 0)
            {
                bool IsAllow = false;
                if (Edit == 1)
                    IsAllow = true;
                string Phone = "Bạn chưa được <b>" + EmployeeName + "</b> cho xem thông tin này";
                //string color = "border: 1px solid #7f209c;";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    //if (i != 0)
                    //    color = "";
                    DataRow r = zTable.Rows[i];
                    zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' >");//style='" + color + "'
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a onclick='UpdateGuest(" + r["OwnerKey"] + ",\"ResaleApartment\")' href='#' class='red' style='display:none'><i class='ace-icon fa fa-pencil-square-o bigger-130'></i>&nbsp;Sửa</a></div></div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    if (IsAllow)
                        zSb.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                    else
                        zSb.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Email</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["Email1"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Địa chỉ</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>Liên lạc: " + r["Address1"].ToString() + "<br/> Thường trú: " + r["Address2"].ToString() + "</div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine(" <div class='profile-info-row'>");
                    zSb.AppendLine("    <div class='profile-info-name'>Ngày cập nhật</div>");
                    zSb.AppendLine("    <div class='profile-info-value'>" + r["CreatedDate"].ToDateString() + "</div>");
                    zSb.AppendLine(" </div>");
                    zSb.AppendLine("</div>");
                    zSb.AppendLine("<div class='space-6'></div>");

                    //#region [get 1 chu nha]
                    //if (i == 0)
                    //{
                    //zTop.AppendLine("<div class='profile-info-row'>");
                    //zTop.AppendLine("    <div class='profile-info-name'>Tên khách</div>");
                    //zTop.AppendLine("    <div class='profile-info-value'>" + r["CustomerName"].ToString() + "</div>");
                    //zTop.AppendLine("</div>");
                    //zTop.AppendLine("<div class='profile-info-row'>");
                    //zTop.AppendLine("    <div class='profile-info-name'>SĐT</div>");
                    //if (IsAllow)
                    //    zTop.AppendLine("    <div class='profile-info-value'>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</div>");
                    //else
                    //    zTop.AppendLine("    <div class='profile-info-value'>" + Phone + "</div>");
                    //zTop.AppendLine("</div>");
                    //}
                }

                Result.Result = zSb.ToString();
                //Result.Result2 = zTop.ToString();
            }

            return Result;
        }
        [WebMethod]
        public static string GetFile(int AssetKey)
        {
            List<ItemDocument> zList = Document_Data.List(135, AssetKey);
            StringBuilder zSb = new StringBuilder();
            if (zList.Count > 0)
            {
                foreach (ItemDocument item in zList)
                {
                    zSb.AppendLine("<a href='" + item.ImageUrl + "' title='" + item.ImageName + "'><img style='border:1px solid;' width=150 height=150 src='" + item.ImageThumb.ToThumb() + "'></a>");
                }
            }
            return zSb.ToString();
        }

        //search
        [WebMethod(EnableSession = true)]
        public static string SearchAsset(int Project, string AssetID, string Room, string Category, string Purpose)
        {
            List<Product_Item> zList = new List<Product_Item>();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            zList = Product_Data.Search(Project, Category.ToInt(), string.Empty, string.Empty, string.Empty, Room, AssetID, string.Empty, Purpose, 0, 0, 500);
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (Product_Item r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }
        //loc lay gia va nhu cau
        static string GetPrice(Product_Item Item)
        {
            string PurposeShow = "";
            if (Item.Purpose.Contains(","))
            {
                string[] temp = Item.Purpose.Split(',');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "Chuyển nhượng":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                                break;

                            case "Cho thuê":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                                break;
                        }
                    }
                }
            }
            else
            {
                string s = Item.Purpose;
                switch (s.Trim())
                {
                    case "Chuyển nhượng":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                        break;

                    case "Cho thuê":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                        break;
                }
            }

            return PurposeShow;
        }
        #endregion

        #region [Xử lý popup edit và lưu thông tin sản phẩm]
        [WebMethod]
        public static ItemAsset GetAssetEdit(int AssetKey, string AssetType)
        {
            Product_Info zAsset = new Product_Info(AssetKey, AssetType);
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }

        [WebMethod]
        public static ItemReturn SaveProduct(int AssetKey, string AssetType,
            string AssetStatus, string AssetRemind,
            string AssetPrice_VND, string AssetPriceRent_VND,
            string AssetPrice_USD, string AssetPriceRent_USD,
            string AssetPricePurchase_VND, string AssetPricePurchase_USD,
            string AssetFurnitur, string AssetPurpose,
            string AssetDescription, string AssetLegal, 
            int UsdMua, int UsdBan, int UsdThue)
        {
            ItemReturn zResult = new ItemReturn();
            Product_Info zProduct = new Product_Info(AssetKey, AssetType);
            ItemAsset zAsset = zProduct.ItemAsset;
            zAsset.AssetKey = AssetKey.ToString();
            zAsset.IsResale = "1";
            //if (AssetStatus == "313")//tinh trang dùng ở
            //    zAsset.IsResale = "0";
            zAsset.StatusKey = AssetStatus;
            zAsset.LegalKey = AssetLegal;

            #region [Asset SaleInfo]
            zAsset.Description = AssetDescription;
            zAsset.DateContractEnd = AssetRemind;

            double giabanUSD = 0;
            double.TryParse(AssetPrice_USD, out giabanUSD);
            double giabanVND = 0;
            double.TryParse(AssetPrice_VND, out giabanVND);
            double giathueUSD = 0;
            double.TryParse(AssetPriceRent_USD, out giathueUSD);
            double giathueVND = 0;
            double.TryParse(AssetPriceRent_VND, out giathueVND);
            double giamuaUSD = 0;
            double.TryParse(AssetPricePurchase_USD, out giamuaUSD);
            double giamuaVND = 0;
            double.TryParse(AssetPricePurchase_VND, out giamuaVND);

            if (UsdMua == 1)
            {
                zAsset.PricePurchase_USD = giamuaUSD.ToString();
                zAsset.PricePurchase_VND = (giamuaUSD * 22700).ToString();
            }
            else
            {
                zAsset.PricePurchase_USD = (giamuaVND / 22700).ToString();
                zAsset.PricePurchase_VND = giamuaVND.ToString();
            }

            if (UsdBan == 1)
            {
                zAsset.Price_USD = giabanUSD.ToString();
                zAsset.Price_VND = (giabanUSD * 22700).ToString();
            }
            else
            {
                zAsset.Price_USD = (giabanVND / 22700).ToString();
                zAsset.Price_VND = giabanVND.ToString();
            }

            if (UsdThue == 1)
            {
                zAsset.PriceRent_USD = giathueUSD.ToString();
                zAsset.PriceRent_VND = (giathueUSD * 22700).ToString();
            }
            else
            {
                zAsset.PriceRent_USD = (giathueVND / 22700).ToString();
                zAsset.PriceRent_VND = giathueVND.ToString();
            }

            zAsset.FurnitureKey = AssetFurnitur;
            zAsset.PurposeKey = AssetPurpose;
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }
            #endregion

            if (zAsset.EmployeeKey.ToInt() == 0 &&
              zAsset.DepartmentKey.ToInt() == 0 &&
              zAsset.CreatedBy == string.Empty &&
              zAsset.CreatedDate == string.Empty &&
              zAsset.CreatedName == string.Empty)
            {
                zAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zProduct.UpdateManagerment(AssetType);
            }

            zAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zProduct.Save(AssetType);
            if (zProduct.ItemAsset.Message != string.Empty)
            {
                zResult.Message = zProduct.ItemAsset.Message;
                zResult.Result = "Error";
                return zResult;
            }

            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            string Mess = SaveKPI(AssetKey, zAsset.AssetID, EmployeeKey, DepartmentKey, AssetType, zProduct.ItemAsset.ProjectKey.ToInt(), AssetDescription, _Category);

            zResult.Message = "OK";
            zResult.Result = zProduct.ItemAsset.AssetKey;
            return zResult;
        }
        #endregion

        #region [Xử lý popup edit và lưu thông tin chủ nhà]
        [WebMethod]
        public static ItemCustomer GetGuest(string Type, int AutoKey)
        {
            Type = "ResaleApartment";
            Owner_Info zInfo = new Owner_Info(Type, AutoKey);
            ItemCustomer zItem = new ItemCustomer();
            zItem.AssetKey = zInfo.AssetKey.ToString();
            zItem.OwnerKey = zInfo.OwnerKey.ToString();
            zItem.CustomerName = zInfo.CustomerName;
            zItem.Phone1 = zInfo.Phone1;
            zItem.Phone2 = zInfo.Phone2;
            zItem.Email1 = zInfo.Email;
            zItem.Address1 = zInfo.Address1;
            zItem.Address2 = zInfo.Address2;
            zItem.Type = zInfo.Type;
            return zItem;
        }

        [WebMethod]
        public static ItemReturn SaveGuest(int AutoKey, string AssetType, int AssetKey, string CustomerName, string Phone1, string Phone2, string Email, string Address1, string Address2)
        {
            ItemReturn zResult = new ItemReturn();
            Owner_Info zInfo = new Owner_Info(AssetType, AutoKey);
            zInfo.CustomerName = CustomerName;
            zInfo.Phone1 = Phone1;
            zInfo.Phone2 = Phone2;
            zInfo.Email = Email;
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.AssetKey = AssetKey;

            if (zInfo.OwnerKey != 0)
                zInfo.Update(AssetType);
            else
                zInfo.Create(AssetType);

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "OK";

            return zResult;
        }

        #endregion

        [WebMethod]
        public static ItemReturn SavePost(int AssetKey, string AssetType, string NoiDung, string TieuDe, string HienThi)
        {
            ItemReturn zResult = new ItemReturn();
            Product_Info zProduct = new Product_Info(AssetKey, AssetType);
            ItemAsset zAsset = zProduct.ItemAsset;
            zAsset.WebContent = NoiDung;
            zAsset.WebTitle = TieuDe;
            zAsset.WebPublished = HienThi;
            zProduct.PublishWeb(AssetType);
            if (zAsset.Message == string.Empty)
                zResult.Result = "OK";
            else
                zResult.Result = "ERROR";
            return zResult;
        }

        #region [Xử lý share]
        [WebMethod]
        public static ItemReturn SaveShare(int AssetKey, string AssetType, int EmployeeKey)
        {
            ItemReturn zResult = new ItemReturn();
            Share_Permition_Info zInfo = new Share_Permition_Info(AssetKey, EmployeeKey, AssetType);
            if (zInfo.AutoKey > 0)
            {
                zResult.Message = "Trùng thông tin, vui lòng chọn lại";
                return zResult;
            }

            zInfo.ObjectTable = AssetType;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.AssetKey = AssetKey;
            zInfo.Create();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemWeb[] GetShare(int AssetKey, string AssetType)
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(AssetKey, AssetType);
            List<ItemWeb> ListAsset = new List<ItemWeb>();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb iwe = new ItemWeb();
                iwe.Value = zTable.Rows[i]["AutoKey"].ToString();
                iwe.Text = zTable.Rows[i]["EmployeeName"].ToString();

                ListAsset.Add(iwe);
            }
            return ListAsset.ToArray();
        }
        #endregion

        #region [Xử lý tải file]
        void UploadImg()
        {
            int zCategoryKey = 135;
            int zProductKey = HID_AssetKey.Value.ToInt();
            string zSQL = "";
            string zPath = "~/Upload/File/" + HID_AssetType.Value + "/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                    string zFileThumb = zFileName + "_thumb" + Path.GetExtension(zInputFile.FileName);
                    string zFileLarge = Path.GetFileName(zInputFile.FileName);

                    string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                    string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                    string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                    string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                    if (File.Exists(zFilePathLarge))
                        File.Delete(zFilePathLarge);
                    zInputFile.SaveAs(zFilePathLarge);

                    Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                    System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(150, 150));
                    objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('" + HID_AssetType.Value + "','" + zProductKey + "','" + zCategoryKey + "',N'" + zFileLarge + "',N'" + zFileUrlThumb + "',N'" + zFileUrlLarge + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
            }
        }
        protected void btnUploadImg_Click(object sender, EventArgs e)
        {
            UploadImg();
            Response.Redirect("/SAL/ProductView.aspx?ID=" + HID_AssetKey.Value + "&Type=" + HID_AssetType.Value);
        }
        #endregion

        #region [KPI]
        public static string SaveKPI(int AssetKey, string AssetID, int EmployeeKey, int DepartmentKey, string AssetType, int ProjectKey, string Description, int Category)
        {
            string MessageKPI = Helper.KPI_Asset(AssetKey, AssetID, AssetType, ProjectKey, EmployeeKey, DepartmentKey, DateTime.Now, Description, Category, 1);
            return MessageKPI;
        }
        #endregion

        #region [Check Roles]
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL02";
            //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (UnitLevel <= 1)
                Lit_Button.Text = "<a href='#' class='btn btn-white btn-warning btn-bold' id='btnEdit' isedit><i class='ace-icon fa fa-pencil orange'></i>Chỉnh sửa</a>";
            else
            if (Permitsion[2] == "1")
                Lit_Button.Text = "<a href='#' class='btn btn-white btn-warning btn-bold' id='btnEdit' isedit><i class='ace-icon fa fa-pencil orange'></i>Chỉnh sửa</a>";
        }
        #endregion
    }
}