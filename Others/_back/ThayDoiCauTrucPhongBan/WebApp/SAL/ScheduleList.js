﻿var Employee;
var Department;

$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(document).ready(function () {
    Employee = getEmployeeKey();
    Department = getDepartmentKey();

    $("#tbldata tbody tr").on("click", "td:not(:last-child)", function () {
        var tmp = $(this).closest('tr').find("td:eq(1)").text();
        var id = $(this).closest('tr').attr("id");
        var td1 = tmp.split("-")[0];
        var td2 = tmp.split("-")[1];
        var td3 = $(this).closest('tr').find("td:eq(2)").text();

        $("#txt_AddFrom").val(td1);
        $("#txt_AddTo").val(td2);
        $("#txt_AddContent").val(td3);
        $("#childId").val(id);
        console.log(id);
    });
    $("#tbldata").on("click", "a[btn='del']", function (e) {
        var id = $(this).closest('tr').attr("id");
        if (confirm('Bạn có chắc xóa !.')) {
            $.ajax({
                type: "POST",
                url: "/SAL/ScheduleList.aspx/Delete",
                data: JSON.stringify({
                    'childKey': id,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d == 'OK') {
                        $("#" + id).remove();
                    } else {
                        Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d);
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    })
});

jQuery(function ($) {
    var unit = getUnitLevel();
    if (unit <= 2) {
        $(".modal.aside").ace_aside();
    }

    $(".select2").select2({ width: "100%" });
    $('#txt_AddFrom').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i',
        'show2400': true
    });
    $('#txt_AddTo').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i',
        'show2400': true
    });

    $('#datepairExample1').datepair({ 'defaultTimeDelta': 1800000 });
    $('#datepairExample2').datepair({ 'defaultTimeDelta': 1800000 });

    $("#btnSearch").click(function () {
        Employee = $('[id$=DDL_Employee]').val();
        Department = $('[id$=DDL_Department]').val();
        if (Employee == null) {
            alert("Bạn phải chọn nhân viên");
        }
        if (Department == null) {
            Department = getDepartmentKey();
        }

        $('#calendar').fullCalendar('destroy');
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: ''//month,agendaWeek,agendaDay
            },
            defaultView: 'month',
            eventClick: updateEvent,
            selectable: true,
            selectHelper: true,
            select: selectDate,
            editable: true,
            events: '/JS_Schedule.ashx?Employee=' + Employee + '&Department=' + Department,
            eventRender: function (event, element) {
                //alert(event.title);
                element.qtip({
                    content: {
                        text: qTipText(event.title),
                        title: '<strong>' + event.description + '</strong>'
                    },
                    position: {
                        my: 'bottom center',
                        at: 'top center'
                    },
                    style: { classes: 'qtip-shadow qtip-rounded tooltipfont' }
                });
            }
        });
    });
    $('[id$=DDL_Department]').change(function () {
        var Key = $(this).val();
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetEmployees",
            data: JSON.stringify({
                DepartmentKey: $(this).val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                var District = $("[id$=DDL_Employee]");
                District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                $.each(msg.d, function () {
                    District.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
});
jQuery(function ($) {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var options = {
        weekday: "long", year: "numeric", month: "short",
        day: "numeric", hour: "2-digit", minute: "2-digit"
    };

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: ''//month,agendaWeek,agendaDay
        },
        defaultView: 'month',
        eventClick: updateEvent,
        selectable: true,
        selectHelper: true,
        select: selectDate,
        editable: true,
        events: '/JS_Schedule.ashx?Employee=' + Employee + '&Department=' + Department,
        eventRender: function (event, element) {
            //alert(event.title);
            element.qtip({
                content: {
                    text: qTipText(event.title),
                    title: '<strong>' + event.description + '</strong>'
                },
                position: {
                    my: 'bottom center',
                    at: 'top center'
                },
                style: { classes: 'qtip-shadow qtip-rounded tooltipfont' }
            });
        }
    });
    $("#btnSave").click(function () {
        $('.se-pre-con').fadeIn('slow');
        location.reload();
    });
    $("#addRecord").click(function () {
        var start = '', end = '', allday = true;
        if (addStartDate != null)
            start = addStartDate.toJSON();
        if (addEndDate != null)
            end = addEndDate.toJSON();
        if (start != '' && end != '')
            allday = isAllDay(addStartDate, addEndDate);

        $.ajax({
            type: 'POST',
            url: '/SAL/ScheduleList.aspx/SaveRecord',
            data: JSON.stringify({
                TimeKey: $("#eventId").val(),
                childKey: $("#childId").val(),
                FromTime: $("#txt_AddFrom").val(),
                ToTime: $("#txt_AddTo").val(),
                Noidung: $("#txt_AddContent").val(),
                start: start,
                end: end,
                allDay: allday
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (r) {
                $("#tbldata tbody").empty();
                $("#tbldata tbody").append($(r.d.Result));
                $("#eventId").val(r.d.Result2);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Lỗi: " + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            }
        });
        $("#childId").val(0);
        $("#txt_AddContent").val('');
    });
});

var currentUpdateEvent;
var addStartDate;
var addEndDate;
var globalAllDay;

function selectDate(start, end, allDay) {
    if ($("[id$=DDL_Employee]").val() != null)
        Employee = $("[id$=DDL_Employee]").val();

    console.log('Employee' + Employee);

    addStartDate = start;
    addEndDate = end;
    globalAllDay = allDay;

    $("#eventId").val(0);
    $.ajax({
        type: 'POST',
        url: '/SAL/ScheduleList.aspx/GetRecord',
        data: JSON.stringify({
            ReportKey: 0,
            ReportDate: start,
            EmployeeKey: Employee
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            $("#eventStartDate").val(addStartDate.toJSON());
            $("#eventEndDate").val(addEndDate.toJSON());

            switch (r.d.Result)
            {
                case "0":
                    $("#title").text(r.d.Result2);
                    $("#input").hide();
                    $("#bang").hide();
                    $("#noidung").show();
                    $("#noidung").empty();
                    $("#noidung").append($(r.d.Result3));
                    break;

                case "2":
                    $("#title").text(r.d.Result2);
                    $("#btnSave").hide();
                    $("#input").hide();
                    $("#bang").show();
                    $("#noidung").empty();
                    $("#noidung").hide();
                    $("#tbldata tbody").empty();
                    $("#tbldata tbody").append($(r.d.Result3));
                    $("#eventId").val(r.d.Result4);
                    $('#tbldata tbody tr td:nth-child(4)').hide();
                    $('#tbldata thead tr th:nth-child(4)').hide();
                    break;

                default:
                    $("#title").text(r.d.Result2);
                    $("#btnSave").show();
                    $("#input").show();
                    $("#bang").show();
                    $("#noidung").empty();
                    $("#noidung").hide();
                    $("#tbldata tbody").empty();
                    $("#tbldata tbody").append($(r.d.Result3));
                    $("#eventId").val(r.d.Result4);
                    break;
            }          
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Lỗi: " + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        }
    });
    $('#mView').modal('show');
    console.log('new[' + $("#eventId").val() + ']' + addStartDate);
}
function updateEvent(event, element) {
    if ($("[id$=DDL_Employee]").val() != null)
        Employee = $("[id$=DDL_Employee]").val();

    console.log('Employee' + Employee);

    if ($(this).data("qtip")) $(this).qtip("destroy");
    currentUpdateEvent = event;
    $("#eventId").val(event.id);
    $.ajax({
        type: 'POST',
        url: '/SAL/ScheduleList.aspx/GetRecord',
        data: JSON.stringify({
            ReportKey: event.id,
            ReportDate: '',
            EmployeeKey: Employee
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            console.log(r.d.Result);

            switch (r.d.Result) {
                case "0":
                    $("#title").text(r.d.Result2);
                    $("#input").hide();
                    $("#bang").hide();
                    $("#noidung").show();
                    $("#noidung").empty();
                    $("#noidung").append($(r.d.Result3));
                    break;

                case "2":
                    $("#title").text(r.d.Result2);
                    $("#btnSave").hide();
                    $("#input").hide();
                    $("#bang").show();
                    $("#noidung").empty();
                    $("#noidung").hide();
                    $("#tbldata tbody").empty();
                    $("#tbldata tbody").append($(r.d.Result3));
                    $("#eventId").val(r.d.Result4);
                    $('#tbldata tbody tr td:nth-child(4)').hide();
                    $('#tbldata thead tr th:nth-child(4)').hide();
                    break;

                default:
                    $("#title").text(r.d.Result2);
                    $("#btnSave").show();
                    $("#input").show();
                    $("#bang").show();
                    $("#noidung").empty();
                    $("#noidung").hide();
                    $("#tbldata tbody").empty();
                    $("#tbldata tbody").append($(r.d.Result3));
                    $("#eventId").val(r.d.Result4);
                    break;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Lỗi: " + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        }
    });
    $('#mView').modal('show');
    console.log('xem[' + event.id + ']' + addStartDate);
    return false;
}

function isAllDay(startDate, endDate) {
    var allDay;

    if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
        allDay = true;
        globalAllDay = true;
    }
    else {
        allDay = false;
        globalAllDay = false;
    }

    return allDay;
}
function qTipText(description) {
    var text;

    //if (end !== null)
    //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") + "<br/><br/>" + description;
    //else
    //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><br/>" + description;

    return description;
}
//----------------
function getEmployeeKey() {
    cookieList = document.cookie.split('; ');
    cookies = {};
    for (i = cookieList.length - 1; i >= 0; i--) {
        var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
        if (cName == 'UserLog') {
            var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
            cookies = val.split('&')[2].split('=');
            return cookies[1];
        }
    }
}
function getDepartmentKey() {
    cookieList = document.cookie.split('; ');
    cookies = {};
    for (i = cookieList.length - 1; i >= 0; i--) {
        var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
        if (cName == 'UserLog') {
            var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
            cookies = val.split('&')[1].split('=');
            return cookies[1];
        }
    }
}

//var td1 = $("#txt_AddFrom").val();
//var td2 = $("#txt_AddTo").val();

//td1 = parseInt(td1.split(':')[0]) + 1;
//td2 = parseInt(td2.split(':')[0]) + 1;

//if (td1.length < 2)
//    td1 = "0" + td1 + ":00";
//if (td2.length < 2)
//    td2 = "0" + td2 + ":00";

//$("#txt_AddFrom").val();
//$("#txt_AddTo").val();