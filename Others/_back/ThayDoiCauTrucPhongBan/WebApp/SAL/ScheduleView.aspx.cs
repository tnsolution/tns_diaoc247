﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ScheduleView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ScheduleKey.Value = Request["ID"];
                CheckRoles();
                LoadInfo();
            }
        }

        void LoadInfo()
        {
            StringBuilder zSb = new StringBuilder();
            Schedule_Info zInfo = new Schedule_Info(HID_ScheduleKey.Value.ToInt());

            Lit_TitlePage.Text = "Chi tiết kế hoạch cho ngày " + zInfo.ScheduleDate.ToString("dd/MM/yyyy");

            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Nhân viên: " + zInfo.CreatedName);
            zSb.AppendLine("</li>");
            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Ngày hiển thị kế hoạch: " + zInfo.ScheduleDate.ToString("dd/MM/yyyy"));
            zSb.AppendLine("</li>");
            zSb.AppendLine("<li>");
            zSb.AppendLine("    <i class='ace-icon fa fa-caret-right blue'></i>Sơ lược kế hoạch: " + zInfo.Description);
            zSb.AppendLine("</li>");
            Lit_ScheduleInfo.Text = zSb.ToString();

            DataTable zChild = Schedule_Rec_Data.List(zInfo.Key);
            zSb = new StringBuilder();
            zSb.AppendLine("                        <div class='row'>");
            zSb.AppendLine("                            <div class='col-xs-12'>");
            zSb.AppendLine("                                <div class='profile-user-info profile-user-info-striped'>");

            for (int j = 0; j < zChild.Rows.Count; j++)
            {
                DataRow rChild = zChild.Rows[j];
                zSb.AppendLine("                                <div class='profile-info-row'>");
                zSb.AppendLine("                                    <div class='profile-info-name'>" + rChild["WorkTime"].ToString() + "</div>");
                zSb.AppendLine("                                        <div class='profile-info-value'>");
                zSb.AppendLine("                                            <span>" + rChild["WorkContent"].ToString() + "</span>");
                zSb.AppendLine("                                        </div>");
                zSb.AppendLine("                                    </div>");
            }

            zSb.AppendLine("                                </div>");
            zSb.AppendLine("                            </div>");
            zSb.AppendLine("                        </div>");

            Lit_Table.Text = zSb.ToString();

            zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy") + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL05";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Chỉnh sửa
                        </button>";
            }
        }
        #endregion
    }
}