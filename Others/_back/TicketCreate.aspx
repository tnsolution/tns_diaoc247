﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TicketCreate.aspx.cs" Inherits="WebApp.FNC.TicketCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" cssclass="form-control">
    <title></title>
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <script src="/template/ace-master/assets/js/jquery-2.1.4.min.js"></script>
    <script src="/template/ace-master/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="/template/ace-master/assets/js/bootstrap.min.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <style>
        .td5 {
            width: 5%;
        }

        .td10 {
            width: 10%;
        }

        .td15 {
            width: 15%;
        }

        .select2-container--default .select2-selection--single {
            border: 0px !important;
            border-radius: 0px !important;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            border-top: 0px !important;
            vertical-align: middle !important;
            padding: 4px !important;
        }

        .hr:not(.hr-dotted) {
            background-color: black !important;
        }

        .hr {
            margin: 0px !important;
            height: 1px !important;
        }

        .table {
            margin-bottom: 0px;
            border: 1px solid black !important;
        }

        input[readonly] {
            background: none !important;
        }

        .form-control[disabled] {
            background-color: none !important;
        }

        .form-control:not(textarea) {
            height: 24px !important;
        }

        .form-control {
            border: 0px !important;
            border-radius: 0px !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            -webkit-transition: none !important;
            -o-transition: none !important;
            transition: none !important;
        }

        input, select, textarea {
            background-color: transparent;
            border: 0px solid;
            height: 20px;
            width: 160px;
            color: #CCC;
        }

            input:focus, select:focus, textarea:focus {
                outline: none !important;
                border: 1px solid !important;
                box-shadow: 0 0 10px #719ECE !important;
            }
    </style>
    <script>
        $(document).ready(function () {
            $(".select2").select2({ width: "100%" });
        })
    </script>
</head>
<body>
    <form id="form1" runat="server" cssclass="form-control">
        <div class="page-content">
            <div class="row">
                <div class="col-md-9">
                    <table class="table">
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <img src="http://diaoc247.vn/wp-content/themes/central/images/logo.png" height="60" /></td>
                            <td class="center">
                                <b>CÔNG TY CỔ PHẦN ĐẦU TƯ ĐỊA ỐC 247</b>
                            </td>

                            <td class="center">
                                <b>PHIẾU BÁN HÀNG</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM</b>
                            </td>
                            <td class="center">
                                <b>Số:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>Điện thoại:</b> (028) 6685 1818 &nbsp;&nbsp;&nbsp;&nbsp; <b>Website</b>  : www.diaoc247.vn
                            </td>
                            <td class="center">
                                <b>Ngày lập:</b>
                            </td>
                        </tr>
                    </table>
                    <table class="table">
                        <tr>
                            <th>Dự án:</th>
                            <td colspan="2">
                                <asp:DropDownList ID="DDLDuAn" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th colspan="3">Loại giao dịch:</th>
                            <td>
                                <asp:DropDownList ID="DDLLoaiGiaoDich" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th colspan="3">Ngày cọc:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCoc" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Phòng:</th>
                            <td colspan="2">
                                <asp:DropDownList ID="DDLPhong" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th colspan="3">Trưởng phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLTruongPhong" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th colspan="3">Nhân viên:</th>
                            <td>
                                <asp:DropDownList ID="DDLNhanVien" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Bên A (Bên bán):</th>
                            <td colspan="6">
                                <asp:TextBox ID="txtHotenA" runat="server" CssClass="form-control" placeholder="Họ tên bên chuyển nhượng"></asp:TextBox>
                            </td>
                            <th colspan="2">Ngày sinh:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtNgaySinhA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtCMNDA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th colspan="3">Ngày cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                            </td>
                            <th colspan="2">Nơi cấp:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtNoiCapA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="6">
                                <asp:TextBox ID="txtDiaChiLienHeA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th colspan="2">Điện thoại:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtDienThoaiA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ thường trú:</th>
                            <td colspan="6">
                                <asp:TextBox ID="txtDiaChiThuongTruA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th colspan="2">Email:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtEmailA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <div class="hr hr-dotted hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Bên B (Bên mua):</th>
                            <td colspan="6">
                                <asp:TextBox ID="txtHotenB" runat="server" CssClass="form-control" placeholder="Họ tên bên nhận chuyển nhượng"></asp:TextBox>
                            </td>
                            <th colspan="2">Ngày sinh:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtNgaySinhB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtCMNDB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th colspan="3">Ngày cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                            </td>
                            <th colspan="2">Nơi cấp:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtNoiCapB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="6">
                                <asp:TextBox ID="txtDiaChiLienHeB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th colspan="2">Điện thoại:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtDienThoaiB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ thường trú:</th>
                            <td colspan="6">
                                <asp:TextBox ID="txtDiaChiThuongTruB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th colspan="2">Email:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtEmailB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Mã căn:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtMaCan" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <th>Diện tích tim tường:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtDienTichTimTuong" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </td>
                            <th colspan="3">Diện tích thông thủy:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichThongThuy" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Loại hình:</th>
                            <td colspan="3">
                                <asp:DropDownList ID="DDLLoaiCanHo" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Giá bán:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtGiaBan" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </td>
                            <th colspan="4">Bằng chữ:</th>
                        </tr>
                        <tr>
                            <th>Bao gồm:</th>
                            <td colspan="10">
                                <asp:TextBox ID="txtBaoGom" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3">Bên B thanh toán cho Bên A:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtBTraA" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </td>
                            <th colspan="5">Bằng chữ:</th>
                        </tr>
                        <tr>
                            <th colspan="3">Bên B giữ lại đóng cho Chủ Đầu Tư/ Nhận sổ hồng:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtBTraChuDauTu" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </td>
                            <th colspan="5">Bằng chữ:</th>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11"><b>Phương thức thanh toán:</b> Bằng tiền mặt hoặc chuyển khoản qua tài khoản ngân hàng theo lịch thanh toán như sau
                                    <span class="pull-right">
                                        <button class="btn btn-minier btn-white" type="button" id="AddRow"><i class="ace-icon fa fa-plus-circle blue"></i>Thêm dòng</button>
                                    </span>

                            </td>
                        </tr>
                        <tr>
                            <th>Chủ tài khoản
                            </th>
                            <td colspan="2">
                                <asp:TextBox ID="txtChuTaiKhoan" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                            <td colspan="3">Số tài khoản
                            </td>
                            <th>
                                <asp:TextBox ID="txtSoTaiKhoan" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </th>
                            <th>Ngân hàng</th>
                            <th colspan="3">
                                <asp:TextBox ID="txtNganHang" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <table class="table table-bordered" id="tblBangThanhToan">
                                    <tr>
                                        <th class="td5">
                                            <center>Đợt</center>
                                        </th>
                                        <th class="td10">
                                            <center>Ngày</center>
                                        </th>
                                        <th class="td15">
                                            <center>Số tiền</center>
                                        </th>
                                        <th>
                                            <center>Nội dung</center>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Đợt 1</td>
                                        <td></td>
                                        <td>3,400,000,000</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Đợt 2</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Phí dịch vụ của bên C:</th>
                            <td>
                                <asp:TextBox ID="txtPhiDichVuC" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                            </td>

                            <th colspan="4">Ngày công chứng thực tế không quá:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCongChung" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                            </td>
                            <th colspan="2">Giá công chứng:</th>
                            <td colspan="2">
                                <asp:TextBox ID="txtGiaCongChung" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Thỏa thuận khác:</th>
                            <td colspan="10">
                                <asp:TextBox ID="txtThoaThuanKhac" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="row">
                <asp:Button ID="btnWord" runat="server" CssClass="btn btn-default" Text="Word" OnClick="btnWord_Click" />
            </div>
        </div>
        <asp:HiddenField ID="HID_BangThanhToan" runat="server" />

        <script>
            $("[role='datepicker']").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                startDate: '01/01/1900',
                todayHighlight: true
            });
            $("[id$=DDLDuAn]").on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/Ajax.aspx/GetCategoryAsset",
                        data: JSON.stringify({
                            "ProjectKey": valueSelected,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $(".se-pre-con").fadeIn("slow");
                        },
                        success: function (msg) {
                            var District = $("[id$=DDLLoaiCanHo]");
                            District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                            $.each(msg.d, function () {
                                var object = this;
                                if (object !== '') {
                                    District.append($("<option></option>").val(object.Value).html(object.Text));
                                }
                            });
                        },
                        complete: function () {
                            $(".se-pre-con").fadeOut("slow");
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $("#AddRow").click(function () {
                var No = $("#tblBangThanhToan tr").length;
                var row = "<tr><td>Đợt " + No + "</td><td></td><td></td><td></td></tr>"
                $("#tblBangThanhToan").append(row);
            });
        </script>
    </form>
</body>
</html>
