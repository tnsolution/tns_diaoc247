﻿using FindAndReplace;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.FNC
{
    public partial class TicketCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.DropDown_DDL(DDLNhanVien, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
            Tools.DropDown_DDL(DDLTruongPhong, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
            Tools.DropDown_DDL(DDLPhong, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);          
            Tools.DropDown_DDL(DDLDuAn, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
            Tools.DropDown_DDL(DDLLoaiGiaoDich, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 29", false);
        }

        protected void btnWord_Click(object sender, EventArgs e)
        {
            GenerateAll(0);
        }

        void GenerateAll(int Key)
        {
            #region [xử lý download]
            string FileName = "004MauCanHo.docx"; //DDLFile.SelectedValue;

            string PathIn = @"\Upload\File\HD\" + FileName;
            string PathOut = @"\Upload\File\HD\" + FileName.Split('.')[0] + "_Output.docx";

            string InputFile = HttpContext.Current.Server.MapPath(@"\") + PathIn;
            string OutputFile = HttpContext.Current.Server.MapPath(@"\") + PathOut;

            File.Copy(InputFile, OutputFile, true);

            using (var flatDocument = new FlatDocument(OutputFile))
            {
                #region [tieu de]
                flatDocument.FindAndReplace("#ngaycoc",Tools.ConvertToDate( txtNgayCoc.Text).Day.ToString());
                flatDocument.FindAndReplace("#thangcoc ", Tools.ConvertToDate(txtNgayCoc.Text).Month.ToString());
                flatDocument.FindAndReplace("#namcoc", Tools.ConvertToDate(txtNgayCoc.Text).Year.ToString());
                flatDocument.FindAndReplace("#duan", DDLDuAn.SelectedItem.Text);
                flatDocument.FindAndReplace("#diachiduan", "12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM");
                #endregion

                #region [--A--]
                flatDocument.FindAndReplace("#hotenA", txtHotenA.Text.Trim());
                flatDocument.FindAndReplace("#cmndA", txtCMNDA.Text.Trim());
                flatDocument.FindAndReplace("#ngaycapA", txtNgayCapA.Text.Trim());
                flatDocument.FindAndReplace("#noicapA", txtNoiCapA.Text.Trim());
                flatDocument.FindAndReplace("#diachiA", txtDiaChiThuongTruA.Text.Trim());
                flatDocument.FindAndReplace("#dienthoaiA",txtDienThoaiA.Text.Trim());
                flatDocument.FindAndReplace("#taikhoanA", txtSoTaiKhoan.Text.Trim());
                flatDocument.FindAndReplace("#nganhangA", txtNganHang.Text.Trim());
                #endregion

                #region [--B--]
                flatDocument.FindAndReplace("#hotenB", txtHotenB.Text.Trim());
                flatDocument.FindAndReplace("#cmndB", txtCMNDB.Text.Trim());
                flatDocument.FindAndReplace("#ngaycapB", txtNgayCapB.Text.Trim());
                flatDocument.FindAndReplace("#noicapB", txtNoiCapB.Text.Trim());
                flatDocument.FindAndReplace("#diachiB", txtDiaChiThuongTruB.Text.Trim());
                flatDocument.FindAndReplace("#dienthoaiB", txtDienThoaiB.Text.Trim());
                #endregion

                flatDocument.FindAndReplace("#macan", txtMaCan.Text.Trim());
                flatDocument.FindAndReplace("#dientichtimtuong", txtDienTichTimTuong.Text.Trim());
                flatDocument.FindAndReplace("#tonggiatrichuyennhuong", txtGiaBan.Text.Trim());
                flatDocument.FindAndReplace("#thanhtoannhansohong", txtBTraChuDauTu.Text.Trim());
                flatDocument.FindAndReplace("#benbthanhtoanbena", txtBTraA.Text.Trim());
                flatDocument.FindAndReplace("#giacongchung", txtGiaCongChung.Text.Trim());
                flatDocument.FindAndReplace("#ngaycongchung", txtNgayCongChung.Text.Trim());
                flatDocument.FindAndReplace("#phimoigioibangchu", "tự động ghi chữ");
                flatDocument.FindAndReplace("#phimoigioi", txtPhiDichVuC.Text.Trim());
                flatDocument.FindAndReplace("#phuongthucthanhtoan", "tự động add paragath vào word");
            }
            #endregion
        }
    }
}