﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master"
    AutoEventWireup="true" CodeBehind="TicketEdit.aspx.cs"
    Inherits="WebApp.FNC.TicketEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td15 {
            width: 15%;
        }

        .td10 {
            width: 10%;
        }

        sup {
            vertical-align: sup;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Phiếu bán hàng
                <span class="tools pull-right">
                    <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                    <a type="button" class="btn btn-white btn-default btn-bold" href="TradeList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info1">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Đơn vị bán hàng
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info2">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Thông tin sản phẩm
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info3">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Thông tin khách
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info4">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Phí môi giới
                            </a>
                        </li>
                        <li type="chuyennhuong">
                            <a data-toggle="tab" href="#info5">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Chuyển nhượng
                            </a>
                        </li>
                        <li type="chothue">
                            <a data-toggle="tab" href="#info6">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Cho thuê
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info7">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Tải tập tin word
                            </a>
                        </li>
                        <li style="display: none;">
                            <a data-toggle="tab" href="#info8">
                                <i class="blue ace-icon fa fa-info bigger-110"></i>
                                Tải tập tin word  | Xem thông tin
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info1" class="tab-pane active">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Ngày lập</label>
                                    <div class="col-sm-4">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                id="txt_DateCreate" placeholder="Chọn ngày" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Loại giao dịch</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="DDL_Category" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class=" col-sm-3 control-label">Chi nhánh</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class=" col-sm-3 control-label">Người lập</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="DDL_Employee" runat="server" class=" form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info2" class="tab-pane">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        ...
                                    </h4>
                                    <div class="widget-toolbar action-buttons">
                                        <a href="#mAssetFilter" class="pink" data-toggle="modal">
                                            <i class="ace-icon fa fa-search bigger-125"></i>
                                            Chọn
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <asp:Literal ID="Lit_Product" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info3" class="tab-pane">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-rss orange"></i>
                                        ...
                                    </h4>
                                    <div class="widget-toolbar action-buttons">
                                        <a id="btnAddGuest" class="pink" href="#">
                                            <i class="ace-icon fa fa-plus bigger-125"></i>
                                            Thêm
                                        </a>
                                        <a href="#mGuestFilter" class="pink" data-toggle="modal">
                                            <i class="ace-icon fa fa-search bigger-125"></i>
                                            Chọn
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <asp:Literal ID="Lit_Customer" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info4" class="tab-pane">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Giá trị thực tế</label>
                                    <div class="col-sm-3">
                                        <input type="text" runat="server" class="form-control" id="txt_Amount" placeholder="Nhập số" moneyinput />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Đặt cọc</label>
                                    <div class="col-sm-3">
                                        <input type="text" runat="server" class="form-control" id="txt_PreOrderAmount" placeholder="Nhập số" moneyinput />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ngày cọc</label>
                                    <div class="col-sm-3">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                id="txt_DatePreOrder" placeholder="Chọn ngày" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Môi giới/ CTV</label>
                                    <div class="col-sm-9">
                                        <input type="text" runat="server" class="form-control" id="txt_ReferenceName" placeholder="Nhập đơn vị liên kết môi giới" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Phí môi giới/ CTV</label>
                                    <div class="col-sm-3">
                                        <input type="text" runat="server" class="form-control" id="txt_ReferencePercent" placeholder="%" maxlength="2" moneyinput />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info5" class="tab-pane">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Giá trị chuyển nhượng
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Tổng giá trị chuyển nhượng (VNĐ)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_TongGiaTriChuyenNhuong" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Thanh toán nhận sổ (VNĐ)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_ThanhToanNhanSo" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Bên B thanh toán cho Bên A (VNĐ)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_BthanhtoanA" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Phí dịch vụ cho Bên C (VNĐ)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_phidichvuC" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Giá công chứng (VNĐ)</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_giacongchung" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ngày công chứng</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_ngaycongchung" placeholder="Chọn ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Lịch thanh toán
                                    </h4>
                                    <div class="widget-toolbar action-buttons">
                                        <a id="btnAddPayment" class="pink" href="#mPayment" data-toggle="modal">
                                            <i class="ace-icon fa fa-plus bigger-125"></i>
                                            Thêm
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8" id="bangPayment">
                                        <asp:Literal ID="Lit_Payment" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Thỏa thuận khác
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <textarea class="form-control limited" id="txt_ThoaThuanChuyenNhuong" maxlength="500" style="margin-top: 0px; margin-bottom: 0px; height: 60px;" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info6" class="tab-pane">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Giá cho thuê
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Số tiền</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_SoTienThue" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Bằng chữ</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_SoTienThueBangChu" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Bao gồm</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_SoTienThueGom" placeholder="Nhập text" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Thời hạn và số tiền thanh toán
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Ngày cọc</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_NgayCocThue" placeholder="Chọn ngày" />
                                                    </div>
                                                </div>
                                                <label class="col-sm-3 control-label">Ngày thanh toán</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_NgayThanhToanThue" placeholder="Chọn ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Tiền cọc</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_TienCocThue" placeholder="Nhập số" moneyinput />
                                                </div>
                                                <label for="" class="col-sm-3 control-label">Tiền thanh toán</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_SoTienThanhToanThue" placeholder="Nhập số" moneyinput />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Thuê theo</label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="DDLThueTheo" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Thời gian chuyển tiền</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_ChuyenTienTuNgay" placeholder="Từ ngày" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_ChuyenTienDenNgay" placeholder="Đến ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Thời gian thuê</label>
                                                <div class="col-sm-3">
                                                    <input type="text" runat="server" class="form-control" id="txt_ThoiGianThue" placeholder="6 tháng/ 1 năm" />
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_ThueTuNgay" placeholder="Từ ngày" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_ThueDenNgay" placeholder="Đến ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Thời gian ký hợp đồng thuê</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_NgayKyHopDong" placeholder="Chọn ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Thời gian bàn giao</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_NgayBanGiao" placeholder="Chọn ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Thời gian bắt đầu tính tiền</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                                            id="txt_NgayTinhTien" placeholder="Chọn ngày" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-info orange"></i>
                                        Thỏa thuận khác
                                    </h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <textarea class="form-control limited" id="txt_ThoaThuanChoThue" maxlength="500" style="margin-top: 0px; margin-bottom: 0px; height: 60px;" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info7" class="tab-pane">
                            <%--<div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        Tạo tập tin word
                                    </h4>

                                    <div class="widget-toolbar action-buttons">
                                        <a href="#" class="pink" id="btnCollectInfo">
                                            <i class="ace-icon fa fa-word bigger-125"></i>
                                            Xử lý
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8" id="allinfo">
                                        <asp:Literal ID="Lit_AllInfo" runat="server"></asp:Literal>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="DDLFile" runat="server" CssClass="select2">
                                                <asp:ListItem Value="001 Hợp Đồng Thuê Căn Hộ Lexington (Tiếng Việt).docx" Text="001 Hợp Đồng Thuê Căn Hộ Lexington (Tiếng Việt).docx"></asp:ListItem>
                                                <asp:ListItem Value="002 Hợp Đồng Thuê Căn Hộ Lexington (Song Ngữ).doc" Text="002 Hợp Đồng Thuê Căn Hộ Lexington (Song Ngữ).doc"></asp:ListItem>
                                                <asp:ListItem Value="003 Thỏa Thuận Đặt Cọc Thuê Căn Hộ Lexington.docx" Text="003 Thỏa Thuận Đặt Cọc Thuê Căn Hộ Lexington.docx"></asp:ListItem>
                                                <asp:ListItem Value="004 Thỏa Thuận Đặt Cọc Chuyển Nhượng Căn Hộ Lexington.docx" Text="004 Thỏa Thuận Đặt Cọc Chuyển Nhượng Căn Hộ Lexington.docx"></asp:ListItem>
                                                <asp:ListItem Value="005 Phụ Lục Bàn Giao Căn Hộ Lexington.docx" Text="005 Phụ Lục Bàn Giao Căn Hộ Lexington.docx"></asp:ListItem>
                                                <asp:ListItem Value="006 Phụ lục bàn giao Căn Hộ Lexington (Song ngữ).docx" Text="006 Phụ lục bàn giao Căn Hộ Lexington (Song ngữ).docx"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-small">
                                    <h4 class="widget-title blue smaller">
                                        <i class="ace-icon fa fa-product-hunt orange"></i>
                                        Xem tổng hợp thông tin phiếu bán hàng
                                    </h4>

                                    <div class="widget-toolbar action-buttons">
                                        <a href="#" class="pink" id="btnCollectInfo">
                                            <i class="ace-icon fa fa-word bigger-125"></i>
                                            Tổng hợp
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main padding-8" id="allinfo">
                                        <asp:Literal ID="Lit_AllInfo" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info8" class="tab-pane">
                            <asp:Literal ID="Lit_File" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search -->
    <div class="modal fade" id="mAssetFilter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn nguồn sản phẩm</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-10">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <asp:DropDownList ID="DDL_AssetProject" CssClass="select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:DropDownList ID="DDL_AssetCategory" runat="server" CssClass="select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" id="txt_AssetID" placeholder="Mã" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <button id="btnAssetSearch" type="button" class="btn btn-sm btn-purple btn-block pull-right">Tìm</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12" id="table">
                            <asp:Literal ID="Lit_Asset" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnAssetSelect" data-dismiss="modal">
                        Chọn</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Guest -->
    <div class="modal fade" id="mGuestFilter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn nguồn khách hàng</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txt_Search_CustomerID" placeholder="Tìm SĐT" />
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txt_Search_CustomerName" placeholder="Tìm tên khách hàng" />
                            </div>
                        </div>
                        <div class="col-md-2 pull-right">
                            <button id="btnGuestSearch" type="button" class="btn btn-block btn-info">Tìm</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Literal ID="Lit_Guest" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <!--- guest --->
    <div class="modal fade" id="mOwner">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Thông tin khách/ chủ nhà</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Họ và tên<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_CustomerName" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày sinh<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_Birthday" placeholder="Chọn ngày" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">CMND<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_CardID" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ngày cấp<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" role="datepicker" class="form-control pull-right"
                                                id="txt_CardDate" placeholder="Chọn ngày" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nơi cấp<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_CardPlace" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT1<sup>*</sup></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Phone1" placeholder="Nhập text" role="phone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT2</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Phone2" placeholder="Nhập text" role="phone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ thường trú(*)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Address1" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ liên hệ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_Address2" placeholder="Nhập text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="ace" value="1" name="chkPurpose" id="chkIsOwner1" />
                                                <span class="lbl">Chủ nhà</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="radio" class="ace" value="2" name="chkPurpose" id="chkIsOwner2" />
                                                <span class="lbl">Khách thuê</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="ace" value="3" name="chkPurpose" id="chkIsOwner3" />
                                                <span class="lbl">Khách mua</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveCustomer">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <!--- payment --->
    <div class="modal fade" id="mPayment">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Lần thanh toán</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày</label>
                            <div class="col-sm-4">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" role="datepicker" class="form-control pull-right"
                                        id="txt_Ngay" placeholder="Chọn ngày" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tiêu đề</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txt_TieuDe" placeholder="Nhập text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Số tiền</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txt_Sotien" placeholder="Nhập số" moneyinput />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nội dung</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="txt_noidungDot" placeholder="Nhập text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSavePayment">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <!--- guest list --->
    <div class="modal fade" id="mOwnerList">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn chủ nhà</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 id="note"></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered" id="tblOwnerList">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tên khách hàng</th>
                                        <th>SĐT</th>
                                        <th>Ngày cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3">Chưa có data</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSelectCustomer" data-dismiss="modal">
                        Chọn</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_PaymentKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TicketCategory" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
    <asp:HiddenField ID="HID_OwnerKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_CustomerEdit" runat="server" Value="0" />
    <asp:Button ID="btnProcessFile" runat="server" Text="Button" Style="visibility: hidden; display: none" OnClick="btnProcessFile_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script src="/FNC/TicketEdit.js"></script>
</asp:Content>
