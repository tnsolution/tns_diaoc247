﻿using FindAndReplace;
using Lib.CRM;
using Lib.FNC;
using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TicketEdit : System.Web.UI.Page
    {
        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey =" + Department + " ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey =" + Department + " ORDER BY LastName", false);
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey =" + Department + " ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + "ORDER BY LastName", false);
                    break;
            }

            if (_Permitsion[2] == "1")
            {
                LitButton.Text = "<a class='btn btn-white btn-info btn-bold' id='btnEdit' href='#'><i class='ace-icon fa fa-save blue'></i>Cập nhật</a>";
            }
            if (_Permitsion[3] == "1")
            {
                LitButton.Text += " <a class='btn btn-white btn-warning btn-bold' id='btnDel' href='#'><i class='ace-icon fa fa-trash-o bigger-120 orange'></i>Xóa</a>";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();

                Tools.DropDown_DDL(DDL_Category, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 29", false);
                Tools.DropDown_DDL(DDL_AssetProject, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                if (Request["ID"] != null)
                    HID_TicketKey.Value = Request["ID"];

                LoadData();
                LoadCustomer();
                LoadPayment();

                InitSearchAsset();
                InitSearchGuest();

                //Lit_AllInfo.Text = "Sau khi cập nhật đủ thông tin click [Tổng hợp] để tập hợp thông tin phiếu bán hàng !.";
            }
        }

        void InitSearchAsset()
        {
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            List<Product_Item> zList = new List<Product_Item>();
            zList = Product_Data.Search(DDL_AssetProject.SelectedValue.ToInt(), DDL_AssetCategory.SelectedValue.ToInt(),
                string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 1, CurrentAgent, 100);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblAssetSearch'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Mã sản phẩm</th>");
            zSb.AppendLine("        <th>Loại sản phẩm</th>");
            zSb.AppendLine("        <th>Diện tích m<sup>2</sup></th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int i = 1;
            foreach (Product_Item r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.AssetKey + "' type='" + r.AssetType + "'>");
                zSb.AppendLine("                <td>" + i++ + "</td>");
                zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                zSb.AppendLine("                <td>" + r.Area + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Asset.Text = zSb.ToString();
        }
        void InitSearchGuest()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            List<ItemCustomer> zList = Customer_Data.Get(Department, Employee);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblGuestSearch'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Tên khách hàng</th>");
            zSb.AppendLine("        <th>SĐT</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            int no = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "'>");
                zSb.AppendLine("               <td>" + (no++) + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br/>" + r.Phone2 + "</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Guest.Text = zSb.ToString();
        }

        void LoadPayment()
        {
            DataTable zTable = TicketTrade_Data.GetPayment(HID_TicketKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table' id='tblPayment'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th>STT</th>");
            zSb.AppendLine("            <th>Đợt</th>");
            zSb.AppendLine("            <th>Ngày</th>");
            zSb.AppendLine("            <th>Nội dung</th>");
            zSb.AppendLine("            <th>...</th>");
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int o = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("        <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (o++) + "</td>");
                    zSb.AppendLine("            <td>" + r["Title"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["TimePayment"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("            <td><a btn='btnDelPayment' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></td>");
                    zSb.AppendLine("        </tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("              <td>#</td><td colspan='4'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_Payment.Text = zSb.ToString();
        }
        void LoadCustomer()
        {
            DataTable zTable = TicketTrade_Data.GetCustomer(HID_TicketKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table' id='tblCustomer'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th>#</th>");
            zSb.AppendLine("            <th>Họ tên</th>");
            zSb.AppendLine("            <th>SĐT</th>");
            zSb.AppendLine("            <th>Ngày sinh</th>");
            zSb.AppendLine("            <th>CMND</th>");
            zSb.AppendLine("            <th>Địa chỉ</th>");
            zSb.AppendLine("            <th>...</th>");
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int o = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("        <tr id='" + r["CustomerKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (o++) + "</td>");
                    zSb.AppendLine("            <td>" + r["CustomerName"].ToString() + "<br/>" + r["Owner"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Phone1"].ToString() + " <br/>" + r["Phone2"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Birthday"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>CMND: " + r["CardID"].ToString() + "<br/>Nơi cấp: " + r["CardPlace"].ToString() + " </td>");
                    zSb.AppendLine("            <td>Liên hệ:" + r["Address1"].ToString() + "<br/>Thường trú: " + r["Address2"].ToString() + "</td>");
                    zSb.AppendLine("            <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDelCustomer' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("        </tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("              <td>#</td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            Lit_Customer.Text = zSb.ToString();
        }
        void LoadData()
        {
            int zTicketKey = HID_TicketKey.Value.ToInt();
            TicketTrade_Info zInfo = new TicketTrade_Info(zTicketKey);
            Product_Info zProduct = new Product_Info(zInfo.AssetKey, zInfo.AssetType);

            DDL_Category.SelectedValue = zInfo.TicketCategory.ToString();
            DDL_Department.SelectedValue = zInfo.DepartmentKey.ToString();
            DDL_Employee.SelectedValue = zInfo.EmployeeKey.ToString();
            txt_DateCreate.Value = zInfo.TicketDate.ToString("dd/MM/yyyy");

            #region [Show Asset]
            string Asset = "";
            if (zInfo.AssetKey != 0)
            {
                ItemAsset zAsset = zProduct.ItemAsset;
                txt_Amount.Value = zAsset.Price_VND;
                Asset = @"  <tr id=" + zInfo.AssetKey + " type=" + zInfo.AssetType + @">
                                                <td>" + 1 + @"</td>
                                                <td>" + zAsset.ProjectName + @"</td>
                                                <td>" + zAsset.AssetID + @"</td>
                                                <td>" + zAsset.CategoryName + @"</td>
                                                <td>" + zAsset.Area + @"</td>
                                                <td>" + zAsset.Address + @"</td>
                                                <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDelAsset' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>
                                            </tr>";
            }

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine(@"<table class='table' id='tblAsset'>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class='td15'>Dự án</th>
                                                <th class='td10'>Mã căn</th>
                                                <th class='td10'>Loại căn</th>
                                                <th class='td10'>DT m<sup>2</sup></th>
                                                <th>Địa chỉ</th>
                                                <th>...</th>
                                            </tr>
                                        </thead>
                                        <tbody>" + Asset + "</tbody></table>");


            Lit_Product.Text = zSb.ToString();
            #endregion

            #region [thông tin chuyển nhượng]
            txt_PreOrderAmount.Value = zInfo.PreOrderAmount.ToString("n0");
            txt_ReferenceName.Value = zInfo.ReferenceName;
            txt_ReferencePercent.Value = zInfo.ReferencePercent.ToString();
            txt_DatePreOrder.Value = zInfo.DateOrder.ToString("dd/MM/yyyy");

            txt_TongGiaTriChuyenNhuong.Value = zInfo.TongGiaTriChuyenNhuong.ToString("n0");
            txt_ThanhToanNhanSo.Value = zInfo.ThanhToanNhanSoHong.ToString("n0");
            txt_BthanhtoanA.Value = zInfo.BenBThanhToanBenA.ToString("n0");
            txt_phidichvuC.Value = zInfo.PhiMoiGioiBenC.ToString("n0");
            txt_giacongchung.Value = zInfo.GiaCongChung.ToString("n0");
            txt_ngaycongchung.Value = zInfo.NgayCongChung.ToString("dd/MM/yyyy");
            #endregion

            zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người khởi tạo:</td><td>" + zInfo.CreatedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày khởi tạo:</td><td>" + zInfo.CreatedDate + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();

            HID_ProjectKey.Value = zInfo.ProjectKey.ToString();
            HID_TicketKey.Value = zInfo.TicketKey.ToString();
            HID_TicketCategory.Value = zInfo.TicketCategory.ToString();
            HID_AssetKey.Value = zInfo.AssetKey.ToString();
            HID_AssetType.Value = zInfo.AssetType.ToString();
        }

        [WebMethod]
        public static ItemReturn Save_ChuyenNhuong(
            int TicketKey, string Employee, string Department,
            string TicketDate, string Amount, string PreOrderAmount, string DateOrder,
            string ReferenceName, string ReferencePercent,
            string TongGiaTriChuyenNhuong, string ThanhToanNhanSoHong,
            string BenBThanhToanBenA, string GiaCongChung,
            string NgayCongChung, string PhiMoiGioiBenC,
            string ThoaThuanChuyenNhuong)
        {
            TicketTrade_Info zInfo = new TicketTrade_Info(TicketKey);
            zInfo.TicketDate = Tools.ConvertToDate(TicketDate);
            zInfo.EmployeeKey = int.Parse(Employee);
            zInfo.DepartmentKey = int.Parse(Department);

            zInfo.Amount = double.Parse(Amount);
            zInfo.PreOrderAmount = double.Parse(PreOrderAmount);
            zInfo.DateOrder = Tools.ConvertToDate(DateOrder);
            zInfo.ReferenceName = ReferenceName;
            zInfo.ReferencePercent = int.Parse(ReferencePercent);

            zInfo.TongGiaTriChuyenNhuong = double.Parse(TongGiaTriChuyenNhuong);
            zInfo.ThanhToanNhanSoHong = double.Parse(ThanhToanNhanSoHong);
            zInfo.BenBThanhToanBenA = double.Parse(BenBThanhToanBenA);
            zInfo.GiaCongChung = double.Parse(GiaCongChung);
            zInfo.NgayCongChung = Tools.ConvertToDate(NgayCongChung);
            zInfo.PhiMoiGioiBenC = double.Parse(PhiMoiGioiBenC);
            zInfo.ThoaThuanChuyenNhuong = ThoaThuanChuyenNhuong;

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.Save();

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.TicketKey.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemReturn Save_ChoThue(
            int TicketKey, string Employee, string Department,
            string TicketDate, string Amount, string PreOrderAmount, string DateOrder,
            string ReferenceName, string ReferencePercent,
            string SoTienThue, string SoTienThueBangChu,
            string SoTienThueGom, string NgayCocThue,
            string TienCocThue, string NgayThanhToanThue,
            string SoTienThanhToanThue, string ThueTheo,
            string ChuyenTienTuNgay, string ChuyenTienDenNgay,
            string ThoiGianThue, string ThueTuNgay, string ThueDenNgay,
            string NgayKyHopDong, string NgayBanGiao,
            string NgayTinhTien, string ThoaThuanChoThue)
        {
            TicketTrade_Info zInfo = new TicketTrade_Info(TicketKey);
            zInfo.TicketDate = Tools.ConvertToDate(TicketDate);
            zInfo.EmployeeKey = int.Parse(Employee);
            zInfo.DepartmentKey = int.Parse(Department);

            zInfo.Amount = double.Parse(Amount);
            zInfo.PreOrderAmount = double.Parse(PreOrderAmount);
            zInfo.DateOrder = Tools.ConvertToDate(DateOrder);
            zInfo.ReferenceName = ReferenceName;
            zInfo.ReferencePercent = int.Parse(ReferencePercent);

            zInfo.SoTienThue = double.Parse(SoTienThue);
            zInfo.SoTienThueBangChu = SoTienThueBangChu.Trim();
            zInfo.SoTienThueGom = SoTienThueGom.Trim();
            zInfo.NgayCocThue = Tools.ConvertToDate(NgayCocThue);
            zInfo.TienCocThue = double.Parse(TienCocThue);
            zInfo.NgayThanhToanThue = Tools.ConvertToDate(NgayThanhToanThue);
            zInfo.SoTienThanhToanThue = SoTienThanhToanThue.Trim();
            zInfo.ThueTheo = ThueTheo.Trim();
            zInfo.ChuyenTienTuNgay = Tools.ConvertToDate(ChuyenTienTuNgay);
            zInfo.ChuyenTienDenNgay = Tools.ConvertToDate(ChuyenTienDenNgay);
            zInfo.ThoiGianThue = ThoiGianThue.Trim();
            zInfo.ThueTuNgay = Tools.ConvertToDate(ThueTuNgay);
            zInfo.ThueDenNgay = Tools.ConvertToDate(ThueDenNgay);
            zInfo.NgayKyHopDong = Tools.ConvertToDate(NgayKyHopDong);
            zInfo.NgayBanGiao = Tools.ConvertToDate(NgayBanGiao);
            zInfo.NgayTinhTien = Tools.ConvertToDate(NgayTinhTien);
            zInfo.ThoaThuanChoThue = ThoaThuanChoThue.Trim();

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.Save();

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.TicketKey.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemCustomer OneOwner(int OwnerKey, string Type)
        {
            Owner_Info zOwn = new Owner_Info(Type, OwnerKey);
            ItemCustomer zResult = new ItemCustomer();
            Customer_Info zCustomer = new Customer_Info(zOwn.Phone1);
            if (zCustomer.Key > 0)
            {
                zResult.CustomerKey = zCustomer.Key.ToString();
                zResult.CustomerName = zCustomer.CustomerName;
                zResult.Phone1 = zCustomer.Phone1;
                zResult.Phone2 = zCustomer.Phone2;
                zResult.Address1 = zCustomer.Address1;
                zResult.Address2 = zCustomer.Address2;
                zResult.CardDate = zCustomer.CardDate.ToDateString();
                zResult.CardID = zCustomer.CardID;
                zResult.CardPlace = zCustomer.CardPlace;
                zResult.Birthday = zCustomer.Birthday.ToDateString();
            }
            else
            {
                zResult.CustomerName = zCustomer.CustomerName;
                zResult.Phone1 = zOwn.Phone1;
                zResult.Phone2 = zOwn.Phone2;
                zResult.Address1 = zOwn.Address1;
                zResult.Address2 = zOwn.Address2;
            }

            return zResult;
        }
        [WebMethod]
        public static ItemReturn AssetDelete(int Key)
        {
            TicketTrade_Info zTrade = new TicketTrade_Info();
            zTrade.AssetType = "";
            zTrade.AssetKey = 0;
            zTrade.ProjectKey = 0;
            zTrade.Address = "";
            zTrade.Area = 0;
            zTrade.AssetCategory = 0;
            zTrade.TicketKey = Key;
            zTrade.UpdateProduct();
            ItemReturn zResult = new ItemReturn();
            zResult.Message = zTrade.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemAsset AssetSelect(int Key, int AssetKey, string Type)
        {
            TicketTrade_Info zTicket = new TicketTrade_Info(Key);
            Product_Info zInfo = new Product_Info(AssetKey, Type);

            zTicket.AssetType = zInfo.ItemAsset.AssetType;
            zTicket.AssetKey = zInfo.ItemAsset.AssetKey.ToInt();
            zTicket.ProjectKey = zInfo.ItemAsset.ProjectKey.ToInt();
            zTicket.Address = zInfo.ItemAsset.Address;
            float area = 0;
            float.TryParse(zInfo.ItemAsset.AreaWall, out area);
            zTicket.Area = area;
            zTicket.AssetCategory = zInfo.ItemAsset.CategoryKey.ToInt();
            zTicket.Amount = zInfo.ItemAsset.Price_VND.ToDouble();
            zTicket.UpdateProduct();

            return zInfo.ItemAsset;
        }

        [WebMethod]
        public static ItemCustomer[] GetOwner(int AssetKey, string Type)
        {
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            List<ItemCustomer> zList = zTable.DataTableToList<ItemCustomer>();

            foreach (ItemCustomer item in zList)
            {
                string temp = "";
                if (item.Phone1.Contains("/"))
                    temp = item.Phone1.Split('/')[0];
                else
                    temp = item.Phone1;
                temp = temp.Trim().Replace(" ", "");
                Customer_Info zCustomer = new Customer_Info(temp);
                if (zCustomer.Key > 0)
                {
                    item.CustomerKey = zCustomer.Key.ToString();
                    item.CustomerName = zCustomer.CustomerName;
                    item.Phone1 = zCustomer.Phone1;
                    item.Phone2 = zCustomer.Phone2;
                    item.Address1 = zCustomer.Address1;
                    item.Address2 = zCustomer.Address2;
                    item.CardDate = zCustomer.CardDate.ToDateString();
                    item.CardID = zCustomer.CardID;
                    item.CardPlace = zCustomer.CardPlace;
                    item.Birthday = zCustomer.Birthday.ToDateString();
                }
            }

            return zList.ToArray();
        }

        [WebMethod]
        public static ItemAsset[] AssetSearch(int Project, int Category, string AssetID)
        {
            List<ItemAsset> zList = new List<ItemAsset>();
            zList = Product_Data.Search(Project, Category, string.Empty, string.Empty, string.Empty, string.Empty, AssetID, string.Empty, string.Empty, 0, 0);
            return zList.ToArray();
        }

        [WebMethod]
        public static ItemReturn InitTicket(int Category, string DateCreate, int Employee, int Department)
        {
            TicketTrade_Info zInfo = new TicketTrade_Info();
            zInfo.TicketCategory = Category;
            zInfo.TicketDate = Tools.ConvertToDate(DateCreate);
            zInfo.EmployeeKey = Employee;
            zInfo.DepartmentKey = Department;

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Create();

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.TicketKey.ToString();
            return zResult;
        }

        //------------------------
        [WebMethod]
        public static ItemCustomer GetCustomer(int TicketKey, int CustomerKey)
        {
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            TicketTrade_Customer_Info zTrade = new TicketTrade_Customer_Info(TicketKey, CustomerKey);
            ItemCustomer zResult = new ItemCustomer();

            zResult.CustomerKey = zInfo.Key.ToString();
            zResult.CustomerName = zInfo.CustomerName;
            zResult.Birthday = zInfo.Birthday.ToDateString();
            zResult.Phone1 = zInfo.Phone1;
            zResult.Phone2 = zInfo.Phone2;
            zResult.Address1 = zInfo.Address1;
            zResult.Address2 = zInfo.Address2;
            zResult.CardID = zInfo.CardID;
            zResult.CardDate = zInfo.CardDate.ToDateString();
            zResult.CardPlace = zInfo.CardPlace;
            zResult.IsOwner = zTrade.IsOwner.ToString();
            zResult.Owner = zTrade.Owner;
            return zResult;
        }

        [WebMethod]
        public static ItemCustomer[] GuestSearch(string Phone, string Name)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
            {
                Employee = 0;
                Department = 0;
            }
            List<ItemCustomer> zList = Customer_Data.Get(string.Empty, string.Empty, string.Empty, Name, Phone, string.Empty, Department, Employee);
            return zList.ToArray();
        }

        //------------------------
        [WebMethod]
        public static ItemReturn GuestDelete(int TicketKey, int CustomerKey)
        {
            TicketTrade_Customer_Info zInfo = new TicketTrade_Customer_Info();
            zInfo.Key = TicketKey;
            zInfo.CustomerKey = CustomerKey;
            zInfo.Delete(CustomerKey, TicketKey);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetGuest(int TicketKey)
        {
            DataTable zTable = TicketTrade_Data.GetCustomer(TicketKey);
            List<ItemCustomer> zList = zTable.DataTableToList<ItemCustomer>();
            return zList.ToArray();
        }

        [WebMethod]
        public static ItemReturn SaveGuest(int TicketKey, int OwnerKey, int CustomerKey, int AssetKey, string AssetType,
            string CustomerName, string Phone, string Phone2, string Address, string Address2,
            string CardDate, string CardID, string CardPlace, string Birthday, int EmployeeKey, int IsOwner)
        {
            int Status = 0;// tinh trang khách 277 đã giao dich, 279 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 277;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemReturn zResult = new ItemReturn();

            //luu thong tin khach
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.ID = Phone.Replace(" ", "");
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone.Replace(" ", "");
            zInfo.Phone2 = Phone2.Replace(" ", "");
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.EmployeeKey = EmployeeKey.ToInt();
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.CreatedDate = DateTime.Now;
            zInfo.ModifiedDate = DateTime.Now;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();

            //luu khach giao dich
            TicketTrade_Customer_Info zCustomerTrade = new TicketTrade_Customer_Info(TicketKey, zInfo.Key);
            zCustomerTrade.CustomerKey = zInfo.Key;
            zCustomerTrade.Key = TicketKey;
            zCustomerTrade.IsOwner = IsOwner;
            zCustomerTrade.Save();

            //luu tinh trang quan tam
            Consents_Info zConsent = new Consents_Info(zInfo.Key, true);
            zConsent.CategoryConsent = "277"; //đã giao dịch
            zConsent.Name_Consent = "Đã giao dịch";
            if (zConsent.CustomerKey == 0)
            {
                zConsent.CustomerKey = zInfo.Key;
                zConsent.Create();
            }
            else
            {
                zConsent.Update(zInfo.Key);
            }

            //cap nhat thong tin chu nha
            Owner_Info zOwner = new Owner_Info(AssetType, OwnerKey);
            zOwner.CustomerName = CustomerName;
            zOwner.Phone1 = Phone.Replace(" ", "");
            zOwner.Phone2 = Phone2.Replace(" ", "");
            zOwner.Address1 = Address;
            zOwner.Address2 = Address2;
            zOwner.Update(AssetType);

            return zResult;
        }

        [WebMethod]
        public static ItemReturn SaveNewCustomer(int TicketKey, int AssetKey, string AssetType, string CustomerName,
          string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int EmployeeKey, int IsOwner)
        {
            int Status = 0;// tinh trang khách 227 đã giao dich, 229 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 279;
                    break;
                case 3://khachmua
                    Status = 277;
                    Owner_Info zOwn = new Owner_Info();
                    zOwn.AssetKey = AssetKey;
                    zOwn.CustomerName = CustomerName;
                    zOwn.Phone1 = Phone;
                    zOwn.Phone2 = Phone2;
                    zOwn.Address1 = Address;
                    zOwn.Address2 = Address2;
                    zOwn.Type = AssetType;
                    zOwn.Create(AssetType);
                    break;
            }

            ItemReturn zResult = new ItemReturn();
            Customer_Info zInfo = new Customer_Info();
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.DepartmentKey = Employees_Data.GetDepartment(EmployeeKey);
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            //luu khach giao dich
            TicketTrade_Customer_Info zCustomerTrade = new TicketTrade_Customer_Info(TicketKey, zInfo.Key);
            zCustomerTrade.CustomerKey = zInfo.Key;
            zCustomerTrade.Key = TicketKey;
            zCustomerTrade.IsOwner = IsOwner;
            zCustomerTrade.Save();

            zInfo.Save();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemReturn SaveCustomer(int CustomerKey, int AssetKey, string AssetType, string CustomerName,
            string Phone, string Phone2, string Address, string Address2, string CardDate, string CardID, string CardPlace, string Birthday, int IsOwner)
        {
            int Status = 0;// tinh trang khách 277 đã giao dich, 279 ký gửi đã giao dịch, 
            switch (IsOwner)
            {
                case 1://chunha
                    Status = 279;
                    break;
                case 2://khachthue
                    Status = 277;
                    break;
                case 3://khachmua
                    Status = 277;
                    break;
            }

            ItemReturn zResult = new ItemReturn();
            Customer_Info zInfo = new Customer_Info(CustomerKey);
            zInfo.HasTrade = 1;
            zInfo.Status = Status;
            zInfo.CustomerName = CustomerName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.CardID = CardID;
            zInfo.CardDate = Tools.ConvertToDate(CardDate);
            zInfo.CardPlace = CardPlace;
            zInfo.Phone1 = Phone;
            zInfo.Phone2 = Phone2;
            zInfo.Address1 = Address;
            zInfo.Address2 = Address2;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemTicket_Payment GetPayment(int AutoKey)
        {
            ItemTicket_Payment zItem = new ItemTicket_Payment();
            TicketPayment_Info zInfo = new TicketPayment_Info(AutoKey);
            zItem.AutoKey = zInfo.AutoKey;
            zItem.Title = zInfo.Title;
            zItem.Description = zInfo.Description;
            zItem.TimePayment = zInfo.TimePayment;
            zItem.Amount = zInfo.Amount;
            return zItem;
        }
        [WebMethod]
        public static ItemReturn SavePayment(int AutoKey, int TicketKey, string Title, string TimePayment, string Description, string Amount)
        {
            TicketPayment_Info zInfo = new TicketPayment_Info(AutoKey);
            zInfo.TicketKey = TicketKey;
            zInfo.Title = Title;
            zInfo.TimePayment = Tools.ConvertToDate(TimePayment);
            zInfo.Description = Description;
            zInfo.Amount = Amount;
            zInfo.Save();

            DataTable zTable = TicketTrade_Data.GetPayment(TicketKey);
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table' id='tblPayment'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th>STT</th>");
            zSb.AppendLine("            <th>Đợt</th>");
            zSb.AppendLine("            <th>Ngày</th>");
            zSb.AppendLine("            <th>Nội dung</th>");
            zSb.AppendLine("            <th>...</th>");
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int o = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("        <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("            <td>" + (o++) + "</td>");
                    zSb.AppendLine("            <td>" + r["Title"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["TimePayment"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("            <td><a btn='btnDelPayment' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></td>");
                    zSb.AppendLine("        </tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("              <td>#</td><td colspan='4'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");

            ItemReturn zResult = new ItemReturn();
            zResult.Result = zSb.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemReturn DeletePayment(int AutoKey, int TicketKey)
        {
            TicketPayment_Info zInfo = new TicketPayment_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.TicketKey = TicketKey;
            zInfo.Delete(AutoKey, TicketKey);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        //[WebMethod]
        void GenerateAll(int Key)
        {
            TicketTrade_Info zInfo = new TicketTrade_Info(Key);
            ItemAsset zProduct = new Product_Info(zInfo.AssetKey, zInfo.AssetType).ItemAsset;
            DataTable zCustomer = TicketTrade_Data.GetCustomer(Key);
            DataTable zPayment = TicketTrade_Data.GetPayment(Key);

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table-bordered table'>");

            #region [tieude]
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th colspan='6'>Dự án: " + zProduct.ProjectName + "</th>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>NV bán hàng:</th>");
            zSb.AppendLine("                    <td colspan='2'>" + zInfo.EmployeeName + "</td>");
            zSb.AppendLine("                    <th>Ngày lập:</th>");
            zSb.AppendLine("                    <td colspan='2'>" + zInfo.TicketDate.ToString("dd/MM/yyyy") + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Phòng:</th>");
            zSb.AppendLine("                    <td colspan='2'>" + zInfo.DepartmentName + "</td>");
            zSb.AppendLine("                    <th>Quản lý: </th>");
            zSb.AppendLine("                    <td colspan='2'>" + zInfo.ManagerName + "</td>");
            zSb.AppendLine("                </tr>");
            #endregion

            DataRow[] resultA = zCustomer.Select("IsOwner = 1");
            #region [--A--]
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th colspan='6'>Bên A (Bên Chuyển Nhượng)</th>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Họ tên:</th>");
            zSb.AppendLine("                    <td colspan='3'>" + resultA[0]["Customername"].ToString() + "</td><th>Ngày sinh: </th><td>" + resultA[0]["Birthday"].ToDateString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>CMND/Passport:</th>");
            zSb.AppendLine("                    <td>" + resultA[0]["CardID"].ToString() + "</td>");
            zSb.AppendLine("                    <th>Ngày cấp: </th>");
            zSb.AppendLine("                    <td>" + resultA[0]["CardDate"].ToDateString() + "</td>");
            zSb.AppendLine("                    <th>Nơi cấp: </th>");
            zSb.AppendLine("                    <td>" + resultA[0]["CardPlace"].ToString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Địa chỉ thường trú:</th>");
            zSb.AppendLine("                    <td colspan='5'>" + resultA[0]["Address1"].ToString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Địa chỉ liên hệ:</th>");
            zSb.AppendLine("                    <td colspan='5'>" + resultA[0]["Address2"].ToString() + "</td>");
            zSb.AppendLine("                </tr>");
            #endregion

            DataRow[] resultB = zCustomer.Select("IsOwner <> 1");
            #region [--B--]
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th colspan='6'>Bên B (Bên Nhận Chuyển Nhượng)</th>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Họ tên:</th>");
            zSb.AppendLine("                    <td colspan='3'>" + resultB[0]["Customername"].ToString() + "</td><th>Ngày sinh: </th><td>" + resultB[0]["Birthday"].ToDateString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>CMND/Passport:</th>");
            zSb.AppendLine("                    <td>" + resultB[0]["CardID"].ToString() + "</td>");
            zSb.AppendLine("                    <th>Ngày cấp: </th>");
            zSb.AppendLine("                    <td>" + resultB[0]["CardDate"].ToDateString() + "</td>");
            zSb.AppendLine("                    <th>Nơi cấp: </th>");
            zSb.AppendLine("                    <td>" + resultB[0]["CardPlace"].ToString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Địa chỉ thường trú:</th>");
            zSb.AppendLine("                    <td colspan='5'>" + resultB[0]["Address1"].ToString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Địa chỉ liên hệ:</th>");
            zSb.AppendLine("                    <td colspan='5'>" + resultB[0]["Address2"].ToString() + "</td>");
            zSb.AppendLine("                </tr>");
            #endregion

            #region [San pham]
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th colspan='6'>Sản phẩm</th>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Mã căn:</th>");
            zSb.AppendLine("                    <td>" + zProduct.AssetID + "</td>");
            zSb.AppendLine("                    <th>Loại:</th>");
            zSb.AppendLine("                    <td>" + zProduct.CategoryName + "</td>");
            zSb.AppendLine("                    <th>Diện tích m2</th>");
            zSb.AppendLine("                    <td>" + zProduct.AreaWall + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Giá trị thực</th>");
            zSb.AppendLine("                    <td class='giatien'>" + zProduct.Price_VND + "</td>");
            zSb.AppendLine("                    <th>Đặt cọc</th>");
            zSb.AppendLine("                    <td class='giatien'>" + zInfo.PreOrderAmount.ToString("n0") + "</td>");
            zSb.AppendLine("                    <th>Ngày</th>");
            zSb.AppendLine("                    <td>" + zInfo.DateOrder.ToDateString() + "</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Môi giới/CTV</th>");
            zSb.AppendLine("                    <td colspan='2'>" + zInfo.ReferenceName + "</td>");
            zSb.AppendLine("                    <th>Phí môi giới CTV</th>");
            zSb.AppendLine("                    <td colspan='2'>" + zInfo.ReferencePercent + "%</td>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th colspan='6'>Thỏa thuận khác</th>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <td colspan='6'>" + zInfo.ThoaThuanChuyenNhuong + "</td>");
            zSb.AppendLine("                </tr>");
            #endregion

            zSb.AppendLine("</table>");

            #region [thanh toan]
            zSb.AppendLine("            <table class='table table-bordered'>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th colspan='4'>Hình thức thanh toán</th>");
            zSb.AppendLine("                </tr>");
            zSb.AppendLine("                <tr>");
            zSb.AppendLine("                    <th>Đợt</th>");
            zSb.AppendLine("                    <th>Ngày</th>");
            zSb.AppendLine("                    <th>Số tiền</th>");
            zSb.AppendLine("                    <th>Nội dung</th>");
            zSb.AppendLine("                </tr>");

            foreach (DataRow r in zPayment.Rows)
            {
                zSb.AppendLine("                <tr>");
                zSb.AppendLine("                    <th>" + r["Title"].ToString() + "</th>");
                zSb.AppendLine("                    <td>" + r["TimePayment"].ToDateString() + "</td>");
                zSb.AppendLine("                    <td class='giatien'>" + r["Amount"].ToDoubleString() + "</td>");
                zSb.AppendLine("                    <td>" + r["Description"].ToString() + "</td>");
                zSb.AppendLine("                </tr>");
            }

            zSb.AppendLine("            </table>");
            #endregion

            #region [xử lý download]
            StringBuilder zSbPayment = new StringBuilder();
            foreach (DataRow r in zPayment.Rows)
            {
                zSbPayment.AppendLine(r["Title"].ToString()
                    + " ngày " + r["TimePayment"].ToDateString()
                    + " số tiền " + r["Amount"].ToDoubleString()
                    + " bằng chữ " + Utils.ConvertDecimalToString(r["Amount"].ToDouble()) + " " + r["Description"].ToString() + "");
            }

            string FileName = "004MauCanHo.docx"; //DDLFile.SelectedValue;

            string PathIn = @"\Upload\File\HD\" + FileName;
            string PathOut = @"\Upload\File\HD\" + FileName.Split('.')[0] + "_Output.docx";

            string InputFile = HttpContext.Current.Server.MapPath(@"\") + PathIn;
            string OutputFile = HttpContext.Current.Server.MapPath(@"\") + PathOut;
                       
            File.Copy(InputFile, OutputFile, true);

            using (var flatDocument = new FlatDocument(OutputFile))
            {
                #region [tieu de]
                flatDocument.FindAndReplace("#ngaycoc", zInfo.TicketDate.Day.ToString());
                flatDocument.FindAndReplace("#thangcoc ", zInfo.TicketDate.Month.ToString());
                flatDocument.FindAndReplace("#namcoc", zInfo.TicketDate.Year.ToString());
                flatDocument.FindAndReplace("#duan", zProduct.ProjectName.Trim());
                flatDocument.FindAndReplace("#diachiduan", zProduct.Address.Trim());
                #endregion

                #region [--A--]
                flatDocument.FindAndReplace("#hotenA", resultA[0]["Customername"].ToString());
                flatDocument.FindAndReplace("#cmndA", resultA[0]["CardID"].ToString());
                flatDocument.FindAndReplace("#ngaycapA", resultA[0]["CardDate"].ToDateString());
                flatDocument.FindAndReplace("#noicapA", resultA[0]["CardPlace"].ToString());
                flatDocument.FindAndReplace("#diachiA", resultA[0]["Address1"].ToString());
                flatDocument.FindAndReplace("#dienthoaiA", resultA[0]["Phone1"].ToString());
                #endregion

                #region [--B--]
                flatDocument.FindAndReplace("#hotenB", resultB[0]["Customername"].ToString());
                flatDocument.FindAndReplace("#cmndB", resultB[0]["CardID"].ToString());
                flatDocument.FindAndReplace("#ngaycapB", resultB[0]["CardDate"].ToDateString());
                flatDocument.FindAndReplace("#noicapB", resultB[0]["CardPlace"].ToString());
                flatDocument.FindAndReplace("#diachiB", resultB[0]["Address1"].ToString());
                flatDocument.FindAndReplace("#dienthoaiB", resultB[0]["Phone1"].ToString());
                #endregion

                flatDocument.FindAndReplace("#macan", zProduct.AssetID);
                flatDocument.FindAndReplace("#dientichtimtuong", zProduct.AreaWall);
                flatDocument.FindAndReplace("#tonggiatrichuyennhuong", zInfo.TongGiaTriChuyenNhuong.ToString("n0"));
                flatDocument.FindAndReplace("#thanhtoannhansohong", zInfo.ThanhToanNhanSoHong.ToString("n0"));
                flatDocument.FindAndReplace("#benbthanhtoanbena", zInfo.BenBThanhToanBenA.ToString("n0"));
                flatDocument.FindAndReplace("#giacongchung", zInfo.GiaCongChung.ToString("n0"));
                flatDocument.FindAndReplace("#ngaycongchung", zInfo.NgayCongChung.ToDateString());
                flatDocument.FindAndReplace("#phimoigioibangchu", Utils.ConvertDecimalToString(zInfo.PhiMoiGioiBenC));
                flatDocument.FindAndReplace("#phimoigioi", zInfo.PhiMoiGioiBenC.ToString("n0"));
                flatDocument.FindAndReplace("#phuongthucthanhtoan", zSbPayment.ToString());
            }
            #endregion

            Lit_AllInfo.Text = zSb.ToString();
            zSb = new StringBuilder();
            System.Threading.Thread.Sleep(5000);
            zSb.AppendLine(@"<a href='" + PathOut.ToFullLink() + "' target='_blank'> Tải xuống !.</a> | <a href='" + PathOut.ToFullLink() + "'> Xem trực tiếp !.</a>");
            Lit_File.Text = zSb.ToString();
        }

        protected void btnProcessFile_Click(object sender, EventArgs e)
        {
            GenerateAll(HID_TicketKey.Value.ToInt());
        }
    }
}