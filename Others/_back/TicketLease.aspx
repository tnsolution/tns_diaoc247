﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketLease.aspx.cs" Inherits="WebApp.FNC.TicketLease" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Phiếu bán hàng</h1>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="blue"><span class="middle">Bên A (Bên Chuyển Nhượng)</span></h4>
                    <div class="profile-user-info">
                        <div class="profile-info-row">
                            <div class="profile-info-name">Họ tên</div>
                            <div class="profile-info-value">
                                <span>Trương Quang Hữu Phước</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">CMND/Passport</div>
                            <div class="profile-info-value">
                                <span>025862953</span>
                                <span>Ngày cấp: 27/11/2013</span>
                                <span>Nơi cấp: TP.HCM</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Địa chỉ</div>
                            <div class="profile-info-value">
                                <i class="fa fa-map-marker light-orange bigger-110"></i>
                                188/10 đường số 8, P.11, Q. Gò Vấp, Tp.HCM
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Điện thoại</div>
                            <div class="profile-info-value">
                                <span>0918150599</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Tài khoản số</div>
                            <div class="profile-info-value">
                                <span>111111222222</span>
                                <span>Ngân hàng thương mại cổ phần Kỹ Thương Việt Nam</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="blue"><span class="middle">Bên B (Bên Nhận Chuyển Nhượng)</span></h4>
                    <div class="profile-user-info">
                        <div class="profile-info-row">
                            <div class="profile-info-name">Họ tên</div>
                            <div class="profile-info-value">
                                <span>Nguyễn Thị Phương Hoa</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">CMND/Passport</div>
                            <div class="profile-info-value">
                                <span>025862953</span>
                                <span>Ngày cấp: 27/11/2013</span>
                                <span>Nơi cấp: TP.HCM</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Địa chỉ</div>
                            <div class="profile-info-value">
                                <i class="fa fa-map-marker light-orange bigger-110"></i>
                                188/10 đường số 8, P.11, Q. Gò Vấp, Tp.HCM
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="blue"><span class="middle">Sản phẩm</span></h4>
                    <div class="profile-user-info">
                        <div class="profile-info-row">
                            <div class="profile-info-name">Dự án</div>
                            <div class="profile-info-value">
                                <span>Lexington</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Mã căn</div>
                            <div class="profile-info-value">
                                <span>GG-21.12</span>
                                <span>Căn hộ</span>
                                <span>Diện tích: 12.95 m2</span>
                                <span>Giá trị thực: 1,430,000,000</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Địa chỉ</div>
                            <div class="profile-info-value">
                                <i class="fa fa-map-marker light-orange bigger-110"></i>
                                188/10 đường số 8, P.11, Q. Gò Vấp, Tp.HCM
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="blue"><span class="middle">Giá trị chuyển nhượng</span></h4>
                    <div class="profile-user-info">
                        <div class="profile-info-row">
                            <div class="profile-info-name">Tổng giá trị</div>
                            <div class="profile-info-value">
                                <span>3.600.000.000</span>
                                <span>VNĐ</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Thanh toán nhận sổ</div>
                            <div class="profile-info-value">
                                <span>30.000.000 </span>
                                <span>VNĐ</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">B thanh toán A</div>
                            <div class="profile-info-value">
                                <span>3.570.000.000 </span><span>VNĐ</span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">Giá công chứng</div>
                            <div class="profile-info-value">
                                <span>1.000.000.000 </span><span>VNĐ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="blue"><span class="middle">Phương thức thanh toán</span></h4>
                    <table class="table table-bordered">
                        <tr>
                            <td>Đợt 1</td>
                            <td>03/11/2017</td>
                            <td>200,000,000 VNĐ</td>
                        </tr>
                        <tr>
                            <td>Đợt 2</td>
                            <td>04/11/2017</td>
                            <td>3,300,000,000 VNĐ</td>
                        </tr>
                        <tr>
                            <td>Đợt 1</td>
                            <td>11/11/2017</td>
                            <td>70,000,000 VNĐ</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="blue"><span class="middle">Ghi chú điều khoản khác</span></h4>
                    <p>
                        Bên A sẽ thanh toán cho Bên C “phí dịch vụ” tương đương số tiền: 36,000,000 VNĐ (Ba mươi sáu triệu đồng)<br />
                        Ngày công chứng thực tế có thể sớm hoặc muộn hơn 5 ngày tùy theo thỏa thuận của 2 Bên nhưng không quá ngày 07/07/2017
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fa fa-rss orange"></i>
                                Tình trạng
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">1. Duyệt bán hàng</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Nguyễn Thị Thanh Thủy</span>
                                            <span>01/11/2017 14:31</span>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">2. Xác nhận giao dịch</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Nguyễn Thị Thanh Thủy</span>
                                            <span>03/11/2017 14:31</span>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">3. Duyệt hợp đồng</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Nguyễn Thị Thanh Thủy</span>
                                            <span>04/11/2017 14:31</span>
                                        </div>
                                    </div>
                                    <div class="profile-info-row">
                                        <div class="profile-info-name" style="text-align: left">4. Hoàn tất giao dịch</div>
                                        <div class="profile-info-value">
                                            <span>
                                                <img width="16" alt="" src="../template/custom-image/true.png" /></span>
                                            <span>Trần Phi Hùng</span>
                                            <span>30/11/2017 14:31</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fa fa-rss orange"></i>
                                Hiện trạng xử lý
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div id="profile-feed-1" class="profile-feed ace-scroll">
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đang soạn phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        8 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đã hoàn tất phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đã gửi trình duyệt phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thanh Thủy</a>
                                            Đã duyệt phiếu bán hàng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Ngọc Trinh</a>
                                            Đã gửi thông tin phiếu bán hàng để lập hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thùy Giang</a>
                                            Đang lập hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thùy Giang</a>
                                            Đã in hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thùy Giang</a>
                                            Đã gửi hợp đồng cho sales
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thanh Thủy</a>
                                            Hợp đồng sai yêu cầu làm lại<br />
                                            Nội dung cần sửa: Sai nội dung sản phẩm.
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                    <div class="profile-activity clearfix">
                                        <div>
                                            <a class="user" href="#">Nguyễn Thị Thanh Thủy</a>
                                            Đã duyệt hợp đồng
																	<div class="time">
                                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                        12 hours ago
                                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
