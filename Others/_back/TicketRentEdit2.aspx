﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketRentEdit2.aspx.cs" Inherits="WebApp.FNC.TicketRentEdit2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <link rel="stylesheet" href="../TicketCSS.css" />
    <style type="text/css">
        #discussion::-webkit-scrollbar-track {
            background-color: #F5F5F5;
        }

        #discussion::-webkit-scrollbar {
            width: 7px;
            background-color: #eeeeee;
        }

        #discussion::-webkit-scrollbar-thumb {
            background-color: #CCCCCC;
            -webkit-box-shadow: inset 0 0 0px rgba(0,0,0,0.3);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="row">
            <div class="col-lg-9" id="printable">
                <asp:Literal ID="LitMessage" runat="server"></asp:Literal>
                <div class="row" id="noidung">
                    <table class="table">
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <img src="http://diaoc247.vn/wp-content/themes/central/images/logo.png" height="60" /></td>
                            <td class="center">
                                <b>CÔNG TY CỔ PHẦN ĐẦU TƯ ĐỊA ỐC 247</b>
                            </td>

                            <td class="center">
                                <b>PHIẾU BÁN HÀNG</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM</b>
                            </td>
                            <td class="center">
                                <b>Số:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>Điện thoại:</b> (028) 6685 1818 &nbsp;&nbsp;&nbsp;&nbsp; <b>Website</b>  : www.diaoc247.vn
                            </td>
                            <td class="center">
                                <b>Ngày lập:<asp:Label ID="lblNgayLap" runat="server" Text=".."></asp:Label></b>
                            </td>
                        </tr>
                    </table>
                    <table class="table">
                        <tr>
                            <th class="td15">Dự án:</th>
                            <td>
                                <asp:DropDownList ID="DDLDuAn" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th class="td15">Giao dịch:</th>
                            <td>Cho thuê</td>
                            <th class="td15">Ngày cọc:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCoc" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLPhong" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Trưởng phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLTruongPhong" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Nhân viên:</th>
                            <td class="td200">
                                <asp:DropDownList ID="DDLNhanVien" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Bên A</b>
                                <small>(Cho thuê)</small>:</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtHotenA" runat="server" CssClass="form-control" placeholder="Họ tên bên chuyển nhượng" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày sinh:</th>
                            <td>
                                <asp:TextBox ID="txtNgaySinhA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td>
                                <asp:TextBox ID="txtCMNDA" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Nơi cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNoiCapA" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiLienHeA" runat="server" CssClass="form-control" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <th>Điện thoại:</th>
                            <td>
                                <asp:TextBox ID="txtDienThoaiA" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ thường trú:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiThuongTruA" runat="server" CssClass="form-control" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <th>Email:</th>
                            <td>
                                <asp:TextBox ID="txtEmailA" runat="server" CssClass="form-control noneed" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div class="hr hr-dotted hr1"></div>
                            </th>
                        </tr>
                        <tr>
                            <td><b>Bên B</b><small>(Bên thuê)</small>:</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtHotenB" runat="server" CssClass="form-control" placeholder="Họ tên bên nhận chuyển nhượng" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày sinh:</th>
                            <td>
                                <asp:TextBox ID="txtNgaySinhB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td>
                                <asp:TextBox ID="txtCMNDB" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Nơi cấp:</th>
                            <td>
                                <asp:TextBox ID="txtNoiCapB" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiLienHeB" runat="server" CssClass="form-control" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                            <th>Điện thoại:</th>
                            <td>
                                <asp:TextBox ID="txtDienThoaiB" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Công ty:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtCongTy" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Email:</th>
                            <td>
                                <asp:TextBox ID="txtEmailB" runat="server" CssClass="form-control noneed" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Mã số thuế:</th>
                            <td>
                                <asp:TextBox ID="txtMaSoThue" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Người đại điện</th>
                            <td>
                                <asp:TextBox ID="txtDaiDien" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>Chức vụ</th>
                            <td>
                                <asp:TextBox ID="txtChucVu" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </th>
                        </tr>
                        <tr>
                            <th>Mã căn:</th>
                            <td>
                                <asp:TextBox ID="txtMaCan" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>DT tim tường:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichTimTuong" runat="server" placeholder="Nhập số" require="true" Style="width: 50px"></asp:TextBox>m2
                            </td>
                            <th>DT thông thủy:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichThongThuy" runat="server" placeholder="Nhập số" require="true" Style="width: 50px"></asp:TextBox>m2
                            </td>
                        </tr>
                        <tr>
                            <th>Loại hình:</th>
                            <td>
                                <asp:DropDownList ID="DDLLoaiCanHo" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Giá cho thuê:</th>
                            <td>
                                <asp:TextBox ID="txtGiaThue" runat="server" placeholder="Nhập số" moneyinput require="true" Style="width: 70px"></asp:TextBox>
                                <asp:DropDownList ID="DDLDonVi" runat="server" Style="width: 50px; border: 0px;">
                                    <asp:ListItem Value="1" Text="VNĐ"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="USD"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="2"><b>Bằng chữ: </b>
                                <asp:Label ID="giathuebangchu" runat="server" Text="..."></asp:Label></td>
                        </tr>
                        <tr>
                            <th>Bao gồm:</th>
                            <td colspan="5">
                                <asp:TextBox ID="txtBaoGom" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <b>Phương thức thanh toán:</b> Bằng tiền mặt hoặc chuyển khoản qua tài khoản ngân hàng theo lịch thanh toán như sau
                                    <span class="pull-right noprint">
                                        <button class="btn btn-minier btn-white" type="button" id="AddRow"><i class="ace-icon fa fa-plus-circle blue"></i>Thêm dòng</button>
                                    </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <b>Chủ tài khoản:</b>
                                <asp:TextBox ID="txtChuTaiKhoan" runat="server" CssClass="" placeholder="Nhập text" require="true"></asp:TextBox>
                                <b>STK</b>
                                <asp:TextBox ID="txtSoTaiKhoan" runat="server" CssClass="" placeholder="Số tài khoản" require="true"></asp:TextBox>
                                <b>NH:</b><asp:TextBox ID="txtNganHang" runat="server" CssClass="" placeholder="Ngân hàng" require="true"></asp:TextBox>
                                <b>CN</b><asp:TextBox ID="txtChiNhanh" runat="server" CssClass="" placeholder="Chi nhánh" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:Literal ID="LitBangThanhToan" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <th>Các đợt tiếp theo:</th>
                            <td colspan="5">Bên B thanh toán bên A số tiền &nbsp;<asp:TextBox ID="txtSoTienThanhToan" runat="server" Style="width: 70px" placeholder="Nhập số" moneyinput></asp:TextBox>
                                <asp:Label ID="lblDonvi" runat="server" Text="VNĐ"></asp:Label>
                                , thanh toán
                                <asp:TextBox ID="txtLan" runat="server" Style="width: 15px" placeholder="1,2 lần" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                &nbsp;lần,
                            từ ngày&nbsp;<asp:TextBox ID="txtTuNgay" runat="server" Style="width: 15px" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                đến ngày&nbsp;<asp:TextBox ID="txtDenNgay" runat="server" Style="width: 15px" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                đầu mỗi
                            <asp:TextBox ID="txtMoiThang" runat="server" Style="width: 15px" MaxLength="2" moneyinput require="true"></asp:TextBox>
                                tháng</td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">Thời điểm giao nhận, cho thuê</th>
                        </tr>
                        <tr>
                            <td colspan="6"><b>Thời gian thuê:</b> từ ngày
                                <asp:TextBox ID="txtThueTuNgay" runat="server" placeholder="Nhập text" CssClass="" require="true" Style="width: 70px"></asp:TextBox>
                                đến ngày  
                                <asp:TextBox ID="txtThueDengnay" runat="server" placeholder="Nhập text" CssClass="" require="true" Style="width: 70px"></asp:TextBox><b>Thời gian bàn giao:</b>
                                <asp:TextBox ID="txtThoiGianBanGiao" runat="server" placeholder="Chọn" CssClass="" require="true" Style="width: 70px"></asp:TextBox><b>Thời gian tính tiền thuê:</b>
                                <asp:TextBox ID="txtThoiGianTinhTienThue" runat="server" placeholder="Chọn" CssClass="" require="true" Style="width: 70px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Thỏa thuận khác:</th>
                            <td colspan="5">
                                <asp:TextBox ID="txtThoaThuanKhac" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-3 noprint" id="right">
                <div class="row">
                    <div class="col-md-9">
                        <asp:DropDownList ID="DDLDaiDienC" runat="server" CssClass="select2" require="true" AppendDataBoundItems="true">
                            <asp:ListItem Value="--0--" Text="Chọn đại diện bên C" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-white dropdown-toggle">
                                Xử lý<i class="ace-icon fa fa-angle-down icon-on-right"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a id="btnSave" href="#"><i class="ace-icon fa fa-floppy-o"></i>&nbsp;Lưu</a>
                                </li>
                                <li style="display: none">
                                    <a id="btnSendApprove" href="#"><i class="ace-icon fa fa-send-o"></i>&nbsp;Gửi Duyệt</a>
                                </li>
                                <li>
                                    <a id="btnIn" href="javascript:window.print()"><i class="ace-icon fa fa-print"></i>&nbsp;In</a>
                                </li>
                                <li>
                                    <a id="btnSendTrade" href="#"><i class="ace-icon fa fa-send"></i>&nbsp;Lập giao dịch</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Thông tin</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <asp:Literal ID="LitInfo" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Xử lý file <small>Chọn file cần xuất thông tin</small></h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="DDLFile" runat="server" CssClass="select2" require="true">
                                                    <asp:ListItem Value="001MauHopDongChoThue.docx" Text="Hợp đồng cho thuê"></asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td class="text-right"><a id="btndownload" href="#"><i class="ace-icon fa fa-download"></i>&nbsp;Xuất file</a></td>
                                        </tr>
                                    </table>
                                    <div class="space-2"></div>
                                    <asp:Literal ID="Lit_ListFolder" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Lịch sử xử lý</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <asp:Literal ID="LitStatus" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Thảo luận &nbsp;<span id="titchat">0</span></h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="discussion" class="ace-scroll" style="height: 350px; overflow-y: scroll">
                                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                                        </asp:ScriptManager>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:HiddenField ID="HID_Change" runat="server" Value="0" />
                                                <asp:Label ID="lblDatetime" runat="server" Text="..." Visible="false"></asp:Label>
                                                <asp:Literal ID="Literal_Chat" runat="server"></asp:Literal>
                                                <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="2000" Enabled="true"></asp:Timer>
                                                <asp:Button ID="btnSend" runat="server" Text="Send" Style="display: none; visibility: hidden" OnClick="btnSend_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="input-group">
                                        <input type="text" name="message" placeholder="Type Message ..." class="form-control" id="txt_Message" runat="server" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-warning btn-sm" id="buttonSend" type="button">Send</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_LoaiCan" runat="server" Value="loaihinh" />
    <asp:HiddenField ID="HID_BangThanhToan" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TienThanhToan" runat="server" Value="0" />
    <asp:Button ID="btnTriggerSave" runat="server" Text="Save" OnClick="btnTriggerSave_Click" Style="display: none; visibility: hidden" />
    <asp:Button ID="btnWord" runat="server" Text="Save" OnClick="btnWord_Click" Style="display: none; visibility: hidden" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="/SoSangChu.js"></script>
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script>
        var isFormValid = true;
        var isUSD = 1;
        jQuery(function ($) {
            $("[id$=txt_Message]").keypress(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    $("#buttonSend").trigger("click");
                }
            });
            $('.iframe').colorbox({ iframe: true, width: "100%", height: "100%" });
        });
        $(document).ready(function () {
            $("[id$=DDLPhong]").on('change', function (e) {
                var valueSelected = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        "DepartmentKey": valueSelected,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDLNhanVien]");
                        var ddl = $("[id$=DDLDaiDienC]");

                        ddl.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                        $.each(msg.d, function () {
                            var object = this;
                            if (object !== '') {
                                ddl.append($("<option></option>").val(object.Value).html(object.Text));
                            }
                        });
                        District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                        $.each(msg.d, function () {
                            var object = this;
                            if (object !== '') {
                                District.append($("<option></option>").val(object.Value).html(object.Text));
                            }
                        });
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetManager",
                    data: JSON.stringify({
                        "Department": valueSelected,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDLTruongPhong]");
                        $.each(msg.d, function () {
                            var object = this;
                            if (object !== '') {
                                District.append($("<option></option>").val(object.Value).html(object.Text));
                            }
                        });
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("[id$=DDLDonVi]").change(function () {
                isUSD = this.value;
            });
            //--action--
            $("#btnSave").click(function () {
                $('.se-pre-con').fadeIn('slow');
                var total = $('#tblBangThanhToan tbody tr').length;
                var row = "";
                var num = 0;
                $('#tblBangThanhToan tbody tr').each(function () {
                    num++;
                    if (num < total)
                        row += ""
                            + $(this).find('td:eq(0)').text() + ":"
                            + $(this).find('td:eq(1)').text() + ":"
                            + $(this).find('td:eq(2)').text() + ":"
                            + $(this).find('td:eq(3)').text() + ":"
                            + $(this).find('td:eq(4)').text() + ":"
                            + $(this).find('td:eq(5)').text() + ":"
                            + $(this).find('td:eq(6)').text() + ";";
                    else
                        row += ""
                        + $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text();
                });
                $("[id$=HID_BangThanhToan]").val(row);
                $('#tblBangThanhToan tfoot tr').each(function () {
                    row = ""
                        + $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text();
                });
                $("[id$=HID_TienThanhToan]").val(row);
                $("[id$=btnTriggerSave]").trigger("click");
            });
            $("#btnIn").click(function () {
                $(':input').removeAttr('placeholder');
            });
            $("#btnApprove").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/FNC/TicketRentEdit2.aspx/Approve",
                    data: JSON.stringify({
                        "TicketKey": $("[id$=HID_TicketKey]").val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        location.reload();
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnNoCheck").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/FNC/TicketRentEdit2.aspx/NotApprove",
                    data: JSON.stringify({
                        "TicketKey": $("[id$=HID_TicketKey]").val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        location.reload();
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSendTrade").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/FNC/TicketRentEdit2.aspx/SendTrade",
                    data: JSON.stringify({
                        "TicketKey": $("[id$=HID_TicketKey]").val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        window.location = "/FNC/TradeEdit.aspx?ID=" + msg.d.Result;
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btndownload").click(function () {
                if ($("[id$=DDLFile]").val() == null) {
                    alert("Bạn phải chọn tập tin cần xử lý");
                    return false;
                }
                else {
                    $(".require").not(".noneed").each(function () {
                        isFormValid = true;
                        if ($.trim($(this).val()).length == 0) {
                            $(this).addClass("highlight");
                            isFormValid = false;
                            $(this).focus();
                            return false;
                        }
                        else {
                            $(this).removeClass("highlight");
                            isFormValid = true;
                        }
                    });
                    console.log(isFormValid);
                    if (!isFormValid) {
                        alert("Bạn phải nhập đủ các thông tin !.");
                        return false;
                    }
                    else {
                        $("[id$=btnWord]").trigger("click");
                    }
                }
            });
            $("#btnSendApprove").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/FNC/TicketRentEdit2.aspx/SendApprove",
                    data: JSON.stringify({
                        "TicketKey": $("[id$=HID_TicketKey]").val(),
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        location.reload();
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("[id$=btnWord]").click(function () {
                var total = $('#tblBangThanhToan tbody tr').length;
                var row = "";
                var num = 0;
                $('#tblBangThanhToan tbody tr').each(function () {
                    num++;
                    if (num < total)
                        row += ""
                            + $(this).find('td:eq(0)').text() + ":"
                            + $(this).find('td:eq(1)').text() + ":"
                            + $(this).find('td:eq(2)').text() + ":"
                            + $(this).find('td:eq(3)').text() + ":"
                            + $(this).find('td:eq(4)').text() + ":"
                            + $(this).find('td:eq(5)').text() + ":"
                            + $(this).find('td:eq(6)').text() + ";";
                    else
                        row += ""
                        + $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text();
                });
                $("[id$=HID_BangThanhToan]").val(row);
                $('#tblBangThanhToan tfoot tr').each(function () {
                    row = ""
                        + $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text();
                });

                $("[id$=HID_TienThanhToan]").val(row);
            });
            //----------

            $("input[moneyinput]").number(true, 0);
            $(".select2").select2({ width: "100%" });
            $("[role='datepicker']").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                startDate: '01/01/1900',
                todayHighlight: true
            });
            $("[id$=DDLLoaiCanHo]").on('change', function (e) {
                $("[id$=HID_LoaiCan]").val($("[id$=DDLLoaiCanHo] option:selected").text());
            });
            $("[id$=DDLDuAn]").on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/Ajax.aspx/GetCategoryAsset",
                        data: JSON.stringify({
                            "ProjectKey": valueSelected,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                        },
                        success: function (msg) {
                            var District = $("[id$=DDLLoaiCanHo]");
                            District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                            $.each(msg.d, function () {
                                var object = this;
                                if (object !== '') {
                                    District.append($("<option></option>").val(object.Value).html(object.Text));
                                }
                            });
                        },
                        complete: function () {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $("[id$=txtGiaThue]").on("blur", function () {
                var value = parseFloat($(this).val().replace(/,/g, ''));
                if (isUSD == 1)
                    $("[id$=giathuebangchu]").text(capitalize(DOCSO.doc(value)));
                else
                    $("[id$=giathuebangchu]").text(capitalize(toWords(value)) + "USD");
            });
            $("#AddRow").click(function () {
                var num = $('#tblBangThanhToan tbody tr').length;
                num = num + 1;
                var row = '<tr id=' + num + '><td tabindex=1>Đặt cọc đợt ' + (num) + '</td><td tabindex=1>&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1>1</td><td tabindex=1>tháng thuê nhà</td><td tabindex=1><span btn="btnDelete" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</span></td></tr>';
                $('#tblBangThanhToan tbody').append(row);
            });
            $("#tblBangThanhToan").on("click", "[btn='btnDelete']", function (e) {
                var Key = $(this).closest('tr').attr('id');
                $("tr[id=" + Key + "]").remove();
            });
            $("#tblBangThanhToan").editableTableWidget()
                .focus(function () {
                    $(this).select();
                });
            $("#tblBangThanhToan").on('change', 'td', function (evt, newValue) {
                if ($(this).index() == 2) {
                    var value = parseFloat(newValue.replace(/,/g, ''));
                    if (isUSD == 1) {
                        $(this).next('td').text('VNĐ');
                        $(this).next('td').next('td').
                            text(capitalize(DOCSO.doc(value)));
                    }
                    else {
                        $(this).next('td').text('USD');
                        $(this).next('td').next('td').
                            text(capitalize(toWords(value)) + "USD");
                    }

                    $(this).text(function (index, value) {
                        return value
                        .replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        ;
                    });
                }
            });
        });
        function textAreaAdjust(o) {
            o.style.height = "1px";
            o.style.height = (25 + o.scrollHeight) + "px";
        }
        function capitalize(s) {
            return s[0].toUpperCase() + s.slice(1);
        }
    </script>
    <script type="text/javascript">
        // American Numbering System
        var th = ['', 'thousand', 'million', 'billion', 'trillion'];

        var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

        var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

        var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        function toWords(s) {
            s = s.toString();
            s = s.replace(/[\, ]/g, '');
            if (s != parseFloat(s)) return 'not a number';
            var x = s.indexOf('.');
            if (x == -1) x = s.length;
            if (x > 15) return 'too big';
            var n = s.split('');
            var str = '';
            var sk = 0;
            for (var i = 0; i < x; i++) {
                if ((x - i) % 3 == 2) {
                    if (n[i] == '1') {
                        str += tn[Number(n[i + 1])] + ' ';
                        i++;
                        sk = 1;
                    } else if (n[i] != 0) {
                        str += tw[n[i] - 2] + ' ';
                        sk = 1;
                    }
                } else if (n[i] != 0) {
                    str += dg[n[i]] + ' ';
                    if ((x - i) % 3 == 0) str += 'hundred ';
                    sk = 1;
                }
                if ((x - i) % 3 == 1) {
                    if (sk) str += th[(x - i - 1) / 3] + ' ';
                    sk = 0;
                }
            }
            if (x != s.length) {
                var y = s.length;
                str += 'point ';
                for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
            }
            return str.replace(/\s+/g, ' ');

        }
    </script>
    <script type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(EndRequestHandler);
        var postBackElementID;
        function EndRequestHandler(sender, args) {
            var change = $("[id$=HID_Change]").val();
            //console.log(change);
            if (change > 0) {
                $("#titchat").text(change);
                var elem = document.getElementById('discussion');
                elem.scrollTop = elem.scrollHeight;
            }
        }

        $(function () {
            //setInterval(function () { alert("Hello"); }, 3000);


            var elem = document.getElementById('discussion');
            elem.scrollTop = elem.scrollHeight;

            //$('#discussion').ace_scroll({
            //    size: 350
            //});
            $("#buttonSend").click(function () {
                var txtval = $.trim($("[id$=txt_Message]").val());
                if (txtval.length != "") {
                    $("[id$=btnSend]").trigger("click");
                    $("[id$=txt_Message]").val("");
                }
            });
        });
    </script>
</asp:Content>
