﻿using FindAndReplace;
using Lib.CRM;
using Lib.FNC;
using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TicketRentEdit2 : System.Web.UI.Page
    {
        public static int _StatusNew = 0;
        public static int _StatusOld = 0;
        public static int _StatusCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRoles();

                Tools.DropDown_DDL(DDLNhanVien, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDLTruongPhong, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND UnitLevel = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDLPhong, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDLDuAn, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDLLoaiCanHo, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                Tools.DropDown_DDL(DDLDaiDienC, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);

                lblNgayLap.Text = DateTime.Now.ToString("dd/MM/yyyy");

                LoadInfo();
                LoadFile();
                LoadChat();
                TicketTracking();
            }
        }
        protected void btnWord_Click(object sender, EventArgs e)
        {
            GenerateAll();
            LoadFile();
        }
        protected void btnTriggerSave_Click(object sender, EventArgs e)
        {
            SaveInfo();
            TicketTracking();
        }
        void GenerateAll()
        {
            string giayuyquyen = "(Theo giấy ủy quyền của Tổng Giám Đốc số UQ-02/DO247/2017 ký ngày 01/01/2017)";
            string baogom = "";
            if (txtBaoGom.Text.Trim().Length == 0)
                baogom = "..";
            else
                baogom = txtBaoGom.Text.Trim();

            Project_Info zProject = new Project_Info(DDLDuAn.SelectedValue.ToInt());

            #region [lịch thanh toán]

            string zList = "";
            string[] row;
            if (HID_BangThanhToan.Value.Contains(";"))
            {
                row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                if (row.Length > 0)
                {
                    for (int i = 0; i < row.Length; i++)
                    {
                        string item = row[i];
                        string[] col = item.Split(':');

                        double money = col[2].Trim().ToDouble();
                        string data = "";
                        data = col[0].Trim() + " ngày " + col[1].Trim()
                            + " số tiền " + money.ToString("n0")
                            + " bằng chữ " + Utils.NumberToWordsVN(money)
                            + " tương đương " + col[5].Trim() + " " + col[6].Trim();

                        zList += data + ". ";
                    }
                }
            }
            #endregion

            #region [tien thanh toan]
            string ngaythuethangdau = "..";
            string sothangthue = "..";
            string sotienthue = "..";
            if (HID_TienThanhToan.Value.Contains(":"))
            {
                row = HID_TienThanhToan.Value.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                double moneyaddin = row[2].Trim().ToDouble();
                string addin = row[0].Trim() + " ngày " + row[1].Trim()
                           + " số tiền " + moneyaddin.ToString("n0")
                           + " bằng chữ " + Utils.NumberToWordsVN(moneyaddin)
                           + " tương đương " + row[5].Trim() + " " + row[6].Trim();
                ngaythuethangdau = row[1].Trim();
                sothangthue = row[5].Trim();
                sotienthue = moneyaddin.ToString("n0");
            }

            #endregion

            //TrackStatus(HID_TicketKey.Value.ToInt(),
            //  "Tải file !.",
            //  HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            string FileName = "001MauHopDongChoThue.docx"; //DDLFile.SelectedValue;

            string PathIn = @"/Upload/File/HD/" + FileName;
            string PathOut = @"/Upload/File/HD/" + FileName.Split('.')[0] + "_Output.docx";

            string InputFile = HttpContext.Current.Server.MapPath(@"/") + PathIn;
            string OutputFile = HttpContext.Current.Server.MapPath(@"/") + PathOut;

            File.Copy(InputFile, OutputFile, true);

            #region [xử lý file]
            var stream = new MemoryStream();
            using (var fileStream = File.OpenRead(OutputFile))
            {
                fileStream.CopyTo(stream);
                using (var flatDocument = new FlatDocument(stream))
                {
                    #region [tieu de]
                    flatDocument.FindAndReplace("#txt_ngaycoc", Tools.ConvertToDate(txtNgayCoc.Text).Day.ToString());
                    flatDocument.FindAndReplace("#txt_thangcoc", Tools.ConvertToDate(txtNgayCoc.Text).Month.ToString());
                    flatDocument.FindAndReplace("#txt_nam", Tools.ConvertToDate(txtNgayCoc.Text).Year.ToString());
                    flatDocument.FindAndReplace("#txt_duan", DDLDuAn.SelectedItem.Text);
                    flatDocument.FindAndReplace("#txt_diachiduan", zProject.Address);
                    #endregion

                    #region [--A--]
                    flatDocument.FindAndReplace("#txt_hotenA", txtHotenA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_cmndA", txtCMNDA.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycapA", txtNgayCapA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_noicapA", txtNoiCapA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachithuongtruA", txtDiaChiThuongTruA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachilienlacA", txtDiaChiLienHeA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dienthoaiA", txtDienThoaiA.Text.Trim());
                    #endregion

                    #region [--B--]
                    flatDocument.FindAndReplace("#txt_hotenB", txtHotenB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_cmndB", txtCMNDB.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycapB", txtNgayCapB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_noicapB", txtNoiCapB.Text.Trim());
                    //flatDocument.FindAndReplace("#txt_diachithuongtruB", txtDiaChiThuongTruB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachilienlacB", txtDiaChiLienHeB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dienthoaiB", txtDienThoaiB.Text.Trim());
                    #endregion

                    flatDocument.FindAndReplace("#txt_congty", txtCongTy.Text.Trim());
                    flatDocument.FindAndReplace("#txt_masothue", txtMaSoThue.Text.Trim());
                    flatDocument.FindAndReplace("#txt_daidien", txtDaiDien.Text.Trim());

                    flatDocument.FindAndReplace("#txt_chucvu", Employees_Data.GetPosition(DDLDaiDienC.SelectedValue.ToInt()));
                    flatDocument.FindAndReplace("#txt_nguoilap", DDLDaiDienC.SelectedItem.Text);
                    flatDocument.FindAndReplace("#txt_chutaikhoan", txtChuTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_sotaikhoan", txtSoTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_nganhang", txtNganHang.Text.Trim());

                    flatDocument.FindAndReplace("#txt_macan", txtMaCan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_Uloaican", HID_LoaiCan.Value.Trim().ToUpper());
                    flatDocument.FindAndReplace("#txt_loaican", HID_LoaiCan.Value.Trim());
                    flatDocument.FindAndReplace("#txt_dientich", txtDienTichTimTuong.Text.Trim());

                    string[] temp = zList.Split('.');
                    if (temp.Length > 5)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            flatDocument.FindAndReplace("#[" + (i + 1) + "]tblbangthanhtoan", temp[i]);
                        }
                    }
                    else
                    {
                        #region fill vào bảng
                        int count = 0;
                        for (int i = 0; i < temp.Length; i++)
                        {
                            flatDocument.FindAndReplace("#[" + (i + 1) + "]tblbangthanhtoan", temp[i]);
                            count = i;
                        }
                        #endregion

                        #region xoa dong trong
                        if (temp.Length < 5)
                        {
                            for (int i = count; i < 5; i++)
                            {
                                flatDocument.FindAndReplace("#[" + (i + 1) + "]tblbangthanhtoan", " ");
                                count = i;
                            }
                        }
                        #endregion
                    }

                    flatDocument.FindAndReplace("#txt_sotienthuebangchu", Utils.NumberToWordsVN(sotienthue.ToDouble()).FirstCharToUpper());
                    flatDocument.FindAndReplace("#txt_sotienthuethangdau", sotienthue);
                    flatDocument.FindAndReplace("#dt_ngaythuethangdau", ngaythuethangdau);
                    flatDocument.FindAndReplace("#txt_sothangthue", sothangthue);
                    flatDocument.FindAndReplace("#txt_tungay", txtTuNgay.Text.Trim());
                    flatDocument.FindAndReplace("#txt_denngay", txtDenNgay.Text.Trim());
                    flatDocument.FindAndReplace("#txt_thang", txtMoiThang.Text.Trim());

                    flatDocument.FindAndReplace("#txt_lan", txtLan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_sotienthue", txtGiaThue.Text.Trim().ToDoubleString());
                    flatDocument.FindAndReplace("#txt_baobom", baogom);

                    //flatDocument.FindAndReplace("#txt_thoigianthue", txtThoiGianThue.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaybangiao", txtThoiGianBanGiao.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaytinhtien", txtThoiGianTinhTienThue.Text.Trim());
                }
            }
            #endregion

            File.WriteAllBytes(OutputFile, stream.ToArray());
            FileInfo file = new FileInfo(OutputFile);
            if (file.Exists)
            {

                string zSQL = @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey ,ImagePath, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES (N'FNC_Ticket','" + HID_TicketKey.Value.ToInt() + "',N'" + OutputFile + "',N'" + PathOut + "',N'" + DDLFile.SelectedItem.Text + "',GETDATE(), GETDATE(),'"
                    + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                    + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                    + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                    + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";

                CustomInsert.Exe(zSQL);
            }

            //if (file.Exists)
            //{
            //    Response.Clear();
            //    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            //    Response.AddHeader("Content-Length", file.Length.ToString());
            //    Response.ContentType = "application/octet-stream";
            //    Response.WriteFile(file.FullName);
            //    Response.End();
            //}
            //else
            //{
            //    Response.Write("This file does not exist.");
            //}    
        }
        void SaveInfo()
        {
            int Key = HID_TicketKey.Value.ToInt();

            #region [thong tin phieu ban hang]
            Ticket_Info zInfo = new Ticket_Info(Key);
            zInfo.DuAn = DDLDuAn.SelectedItem.Text;
            zInfo.CodeDuAn = DDLDuAn.SelectedValue;
            zInfo.Category = 1;
            zInfo.NgayCoc = Tools.ConvertToDate(txtNgayCoc.Text.Trim());

            zInfo.DepartmentKey = DDLPhong.SelectedValue;
            zInfo.Department = DDLPhong.SelectedItem.Text;
            zInfo.EmployeeKey = DDLNhanVien.SelectedValue;
            zInfo.EmployeeName = DDLNhanVien.SelectedItem.Text;
            zInfo.ManagerKey = DDLTruongPhong.SelectedValue;
            zInfo.ManagerName = DDLTruongPhong.SelectedItem.Text;

            zInfo.HotenA = txtHotenA.Text.Trim();
            zInfo.NgaySinhA = txtNgaySinhA.Text.Trim();
            zInfo.CMNDA = txtCMNDA.Text.Trim();
            zInfo.NgayCapA = txtNgayCapA.Text.Trim();
            zInfo.NoiCapA = txtNoiCapA.Text.Trim();
            zInfo.DiaChiLienHeA = txtDiaChiLienHeA.Text.Trim();
            zInfo.DiaChiLienLacA = txtDiaChiThuongTruA.Text.Trim();
            zInfo.DienThoaiA = txtDienThoaiA.Text.Trim();
            zInfo.EmailA = txtEmailA.Text.Trim();

            zInfo.HotenB = txtHotenB.Text.Trim();
            zInfo.NgaySinhB = txtNgaySinhB.Text.Trim();
            zInfo.CMNDB = txtCMNDB.Text.Trim();
            zInfo.NgayCapB = txtNgayCapB.Text.Trim();
            zInfo.NoiCapB = txtNoiCapB.Text.Trim();
            zInfo.DiaChiLienHeB = txtDiaChiLienHeB.Text.Trim();
            //zInfo.DiaChiLienLacB = txtDiaChiThuongTruB.Text.Trim();
            zInfo.DienThoaiB = txtDienThoaiB.Text.Trim();
            zInfo.EmailB = txtEmailB.Text.Trim();

            zInfo.MaCan = txtMaCan.Text.Trim();
            zInfo.LoaiCan = DDLLoaiCanHo.SelectedItem.Text.Trim();
            zInfo.DienTichTimTuong = txtDienTichTimTuong.Text.Trim();
            zInfo.DienTichThongThuy = txtDienTichThongThuy.Text.Trim();
            zInfo.GiaChoThue = txtGiaThue.Text.Trim();
            zInfo.GiaChoThueBangChu = Utils.NumberToWordsVN(txtGiaThue.Text.ToDouble()).FirstCharToUpper();
            zInfo.ThueBaoGom = txtBaoGom.Text.Trim();

            zInfo.ChuTaiKhoan = txtChuTaiKhoan.Text.Trim();
            zInfo.SoTaiKhoan = txtSoTaiKhoan.Text.Trim();
            zInfo.NganHang = txtNganHang.Text.Trim();

            zInfo.BThanhToanThueA = txtSoTienThanhToan.Text.Trim();
            zInfo.SoLan = txtLan.Text.Trim();
            zInfo.TuNgay = txtTuNgay.Text.Trim();
            zInfo.DenNgay = txtDenNgay.Text.Trim();
            zInfo.DauMoi = txtMoiThang.Text.Trim();

            //zInfo.ThoiGianThue = txtThoiGianThue.Text;
            zInfo.ThueTuNgay = txtThueTuNgay.Text.Trim();
            zInfo.ThueDenNgay = txtThueDengnay.Text.Trim();
            zInfo.ThoiGianBanGiao = txtThoiGianBanGiao.Text.Trim();
            zInfo.ThoiGianTinhTien = txtThoiGianTinhTienThue.Text.Trim();

            zInfo.ThoaThuanThueKhac = txtThoaThuanKhac.Text.Trim();

            zInfo.ChucVu = txtChucVu.Text.Trim();
            zInfo.DaiDien = txtDaiDien.Text.Trim();
            zInfo.CongTy = txtCongTy.Text.Trim();
            zInfo.MaSoThue = txtMaSoThue.Text.Trim();
            zInfo.Unit = DDLDonVi.SelectedValue;
            zInfo.ChiNhanh = txtChiNhanh.Text.Trim();

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];

            string[] row = HID_TienThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                for (int i = 0; i < row.Length; i++)
                {
                    string[] coladdin = row[i].Split(':');
                    double moneyaddin = coladdin[2].Trim().ToDouble();
                    string addin = coladdin[0].Trim() + " ngày " + coladdin[1].Trim()
                               + " số tiền " + moneyaddin.ToString("n0")
                               + " bằng chữ " + Utils.NumberToWordsVN(moneyaddin)
                               + " tương đương " + coladdin[4].Trim()
                               + " " + coladdin[5].Trim();

                    zInfo.NgayThueThangDau = coladdin[1].Trim();
                    zInfo.TienThueThangDau = moneyaddin.ToString("n0");
                    zInfo.SoTienThueThangBangChu = Utils.NumberToWordsVN(moneyaddin).FirstCharToUpper();
                    zInfo.SoThangThueDau = coladdin[5].Trim();
                }
            }

            zInfo.Save();
            #endregion

            HID_TicketKey.Value = zInfo.TicketKey.ToString();
            if (zInfo.Message != string.Empty)
            {
                LitMessage.Text = "Lỗi liên hệ Admin " + zInfo.Message;
                return;
            }

            #region [thong tin bang thanh toan]
            row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                string SQL = " DELETE FNC_TicketPayment WHERE TicketKey =" + zInfo.TicketKey;
                foreach (string item in row)
                {
                    string[] col = item.Split(':');
                    double money = col[2].Trim().ToDouble();
                    string MoneyChu = "";
                    if (DDLDonVi.SelectedValue.ToString() == "1")
                        MoneyChu = Utils.NumberToWordsVN(money);
                    else
                        MoneyChu = Utils.NumberToWordsEN((long)money);

                    SQL += @"
INSERT INTO FNC_TicketPayment(Title ,TimePayment, Description ,Amount, AmountInWord, Unit,Number ,CreatedDate ,ModifiedDate ,TicketKey )
VALUES (N'" + col[0] + "' ,N'" + col[1] + "',N'" + col[5] + "' ,N'" + money.ToString("n0") + "',N'"
    + MoneyChu + "',"
    + DDLDonVi.SelectedItem.Text + " ,N'" + col[4]
    + "',GETDATE() ,GETDATE() ," + zInfo.TicketKey + " ) ";

                }

                string Message = CustomInsert.Exe(SQL).Message;
                if (Message != string.Empty)
                    LitMessage.Text = "Lỗi liên hệ Admin " + Message;
            }
            #endregion

            TrackStatus(HID_TicketKey.Value.ToInt(),
                "Lưu thông tin phiếu bán hàng !.",
                HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            //if (zInfo.Approved == 1)
            //    LitScript.Text = "<script>$(document).ready(function () {$('#btndownload').show();})</script>";
            //else
            //    LitScript.Text = "<script>$(document).ready(function () {$('#btndownload').hide();})</script>";

            string url = HttpContext.Current.Request.Url.ToString();
            if (!url.Contains("?"))
                Response.Redirect(url + "?id=" + zInfo.TicketKey, true);
            else
                Response.Redirect(url);
        }
        void LoadInfo()
        {
            StringBuilder zSb = new StringBuilder();

            string TienThueNha = "";
            string NgayThueNha = "";
            string DonViTien = "";
            string SoTienBangChu = "";
            string SoThang = "";

            if (Request["ID"] != null)
            {
                #region [MyRegion]
                HID_TicketKey.Value = Request["ID"];
                int Key = HID_TicketKey.Value.ToInt();
                Ticket_Info zInfo = new Ticket_Info(Key);
                DDLDuAn.SelectedValue = zInfo.CodeDuAn;
                txtNgayCoc.Text = zInfo.NgayCoc.ToString("dd/MM/yyyy");
                DDLPhong.SelectedValue = zInfo.DepartmentKey.ToString();
                DDLNhanVien.SelectedValue = zInfo.EmployeeKey.ToString();
                DDLTruongPhong.SelectedValue = zInfo.ManagerKey.ToString();
                txtHotenA.Text = zInfo.HotenA;
                txtNgaySinhA.Text = zInfo.NgaySinhA;
                txtCMNDA.Text = zInfo.CMNDA;
                txtNgayCapA.Text = zInfo.NgayCapA;
                txtNoiCapA.Text = zInfo.NoiCapA;
                txtDiaChiLienHeA.Text = zInfo.DiaChiLienHeA;
                txtDiaChiThuongTruA.Text = zInfo.DiaChiLienLacA;
                txtDienThoaiA.Text = zInfo.DienThoaiA;
                txtEmailA.Text = zInfo.EmailA;
                txtHotenB.Text = zInfo.HotenB;
                txtNgaySinhB.Text = zInfo.NgaySinhB;
                txtCMNDB.Text = zInfo.CMNDB;
                txtNgayCapB.Text = zInfo.NgayCapB;
                txtNoiCapB.Text = zInfo.NoiCapB;
                txtDiaChiLienHeB.Text = zInfo.DiaChiLienHeB;
                //txtDiaChiThuongTruB.Text = zInfo.DiaChiLienLacB;
                txtDienThoaiB.Text = zInfo.DienThoaiB;
                txtEmailB.Text = zInfo.EmailB;
                txtMaCan.Text = zInfo.MaCan;
                DDLLoaiCanHo.Text = zInfo.LoaiCan;
                txtDienTichTimTuong.Text = zInfo.DienTichTimTuong;
                txtDienTichThongThuy.Text = zInfo.DienTichThongThuy;
                txtGiaThue.Text = zInfo.GiaChoThue;
                giathuebangchu.Text = zInfo.GiaChoThueBangChu.FirstCharToUpper();
                txtBaoGom.Text = zInfo.ThueBaoGom;
                txtChuTaiKhoan.Text = zInfo.ChuTaiKhoan;
                txtSoTaiKhoan.Text = zInfo.SoTaiKhoan;
                txtNganHang.Text = zInfo.NganHang;
                txtSoTienThanhToan.Text = zInfo.BThanhToanThueA;
                txtLan.Text = zInfo.SoLan;
                txtTuNgay.Text = zInfo.TuNgay;
                txtDenNgay.Text = zInfo.DenNgay;
                txtMoiThang.Text = zInfo.DauMoi;
                //txtThoiGianThue.Text = zInfo.ThoiGianThue;

                txtThueDengnay.Text = zInfo.ThueDenNgay;
                txtThueTuNgay.Text = zInfo.ThueTuNgay;
                txtThoiGianBanGiao.Text = zInfo.ThoiGianBanGiao;
                txtThoiGianTinhTienThue.Text = zInfo.ThoiGianTinhTien;

                txtThoaThuanKhac.Text = zInfo.ThoaThuanThueKhac;
                txtThoaThuanKhac.Text = zInfo.ThoaThuanThueKhac;

                txtChucVu.Text = zInfo.ChucVu;
                txtCongTy.Text = zInfo.CongTy;
                txtDaiDien.Text = zInfo.DaiDien;
                txtMaSoThue.Text = zInfo.MaSoThue;

                DDLDonVi.SelectedValue = zInfo.Unit;
                #endregion

                TienThueNha = zInfo.TienThueThangDau;
                NgayThueNha = zInfo.NgayThueThangDau;
                if (zInfo.Unit == "1")
                    DonViTien = "VNĐ";
                else
                    DonViTien = "USD";
                SoTienBangChu = zInfo.SoTienThueThangBangChu.FirstCharToUpper();
                SoThang = zInfo.SoThangThueDau;

                HID_LoaiCan.Value = zInfo.LoaiCan;

                #region MyRegion
                zSb.AppendLine("<table class='table' style='border:0px !important'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người khởi tạo:</td><td>" + zInfo.CreatedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày khởi tạo:</td><td>" + zInfo.CreatedDate + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người duyệt:</td><td>" + zInfo.ApprovedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày duyệt:</td><td>" + zInfo.ApprovedDate + "</td></tr>");
                zSb.AppendLine("</table>");
                LitInfo.Text = zSb.ToString();
                #endregion                                

                //if (zInfo.Approved == 1)
                //    LitScript.Text = "<script>$(document).ready(function () {$('#btndownload').show();})</script>";
                //else
                //    LitScript.Text = "<script>$(document).ready(function () {$('#btndownload').hide();})</script>";
            }

            #region MyRegion
            DataTable zTable = TicketPayment_Data.List(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                #region bang data
                string table = @"<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Đợt</th>
                                        <th class='td10'>Ngày</th>
                                        <th colspan='2' class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th colspan='2'>Ghi chú (tương đương)</th>
                                        <th class='noprint'>#</th>
                                    </tr>
                                </thead>
                                <tbody>";
                foreach (DataRow r in zTable.Rows)
                {
                    table += @"<tr id='" + r["AutoKey"].ToString() + "'><td>" + r["Title"].ToString() + "</td><td tabindex = '1'>" + r["TimePayment"].ToString() + "</td><td tabindex='1'>" + r["Amount"].ToDoubleString() + "</td><td tabindex = '1'>" + r["Unit"].ToString() + "</td><td tabindex = '1'>" + r["AmountInWord"].ToString() + "</td><td tabindex='1'>" + r["Number"].ToString() + "</td><td tabindex='1' class='edit-disabled'>" + r["Description"].ToString() + "</td><td class='edit-disabled noprint'><span btn='btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td></tr> ";
                }

                table += "</tbody>";
                table += "<tfoot><tr><td>Tiền thuê nhà</td><td tabindex = '1'>"
                    + NgayThueNha + "</td><td tabindex='1'>"
                    + TienThueNha + "</td><td class='edit-disabled'>"
                    + DonViTien + "</td><td tabindex = '1'>"
                    + SoTienBangChu + "</td><td tabindex='1'>"
                    + SoThang + "</td><td tabindex='1' class='edit-disabled'>" + "tháng thuê nhà" + "</td><td class='edit-disabled noprint'>#</td></tr></tfoot>";
                table += "</table>";
                LitBangThanhToan.Text = table;
                #endregion               
            }
            else
            {
                #region bang trong
                LitBangThanhToan.Text = @"
<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Đợt</th>
                                        <th class='td10'>Ngày</th>
                                        <th colspan='2' class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th colspan='2'>Ghi chú (tương đương)</th>
                                        <th class='noprint'>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id=1>
                                        <td>Đặt cọc đợt 1</td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint'><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td>
                                    </tr>
                                    <tr id=2>
                                        <td>Đặt cọc đợt 2</td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint'><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td>
                                    </tr>
                                </tbody>
                                <tfoot><tr id=2>
                                        <td>Tiền thuê</td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint'>#</td>
                                    </tr></tfoot>
                            </table>";
                #endregion               
            }
            #endregion            
        }
        void LoadFile()
        {
            List<ItemDocument> zList = Document_Data.List(HID_TicketKey.Value.ToInt(), "FNC_Ticket");
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblFile'>");
            zSb.AppendLine("  <thead>");
            zSb.AppendLine("     <tr>");
            zSb.AppendLine("         <th>#</th>");
            zSb.AppendLine("         <th>Tập tin</th>");
            zSb.AppendLine("         <th>#</th>");
            zSb.AppendLine("     </tr>");
            zSb.AppendLine(" </thead>");
            zSb.AppendLine(" <tbody>");
            int i = 1;
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td style='text-align:right'><a target='_blank' href='" + item.ImageUrl + "'><i class='ace-icon fa fa-download'></i>&nbsp;Download</a></td></tr>");
            }
            zSb.AppendLine(" </tbody>");
            zSb.AppendLine(" </table>");
            Lit_ListFolder.Text = zSb.ToString();
        }
        void TicketTracking()
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<ul class='list-unstyled spaced'>");
            DataTable zTable = Ticket_Data.Tracking(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<li><i class='ace-icon fa fa-bell-o bigger-110 purple'></i>" + r["Date"].ToDateTimeString() + " | " + r["Title"].ToString() + " | " + r["Message"].ToString() + "</li>");
                }
            }
            zSb.AppendLine("</ul>");
            LitStatus.Text = zSb.ToString();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            //lblDatetime.Text = DateTime.Now.ToString("hh:mm:ss");
            LoadChat();
        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            SaveChat();
            LoadChat();
        }
        void LoadChat()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Ticket_Data.Tracking(HID_TicketKey.Value.ToInt(), "CHAT");

            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<div class='direct-chat-msg well well-sm'>");
                zSb.AppendLine("  <div class='direct-chat-info clearfix'>");
                zSb.AppendLine("   <span class='direct-chat-name pull-left'>" + r["Title"].ToString() + "</span>");
                zSb.AppendLine("   <span class='direct-chat-timestamp pull-right'>" + Convert.ToDateTime(r["Date"]).ToString("hh:mm") + "</span>");
                zSb.AppendLine("  </div>");
                zSb.AppendLine(" <div class='direct-chat-text'>" + r["Message"].ToString() + "</div>");
                zSb.AppendLine("</div>");
            }

            Literal_Chat.Text = zSb.ToString();
            _StatusNew = zTable.Rows.Count;
            if (_StatusOld == _StatusNew)
            {
                HID_Change.Value = "0";
            }
            else
            {
                _StatusCount++;
                _StatusOld = _StatusNew;
                HID_Change.Value = (_StatusCount).ToString();
            }
        }
        void LoadChatOnTime()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Ticket_Data.TrackingOnTime(HID_TicketKey.Value.ToInt(), "CHAT");

            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<div class='direct-chat-msg well well-sm'>");
                zSb.AppendLine("  <div class='direct-chat-info clearfix'>");
                zSb.AppendLine("   <span class='direct-chat-name pull-left'>" + r["Title"].ToString() + "</span>");
                zSb.AppendLine("   <span class='direct-chat-timestamp pull-right'>" + Convert.ToDateTime(r["Date"]).ToString("hh:mm") + "</span>");
                zSb.AppendLine("  </div>");
                zSb.AppendLine(" <div class='direct-chat-text'>" + r["Message"].ToString() + "</div>");
                zSb.AppendLine("</div>");
            }

            Literal_Chat.Text += zSb.ToString();
        }
        void SaveChat()
        {
            ChatStatus(HID_TicketKey.Value.ToInt(), txt_Message.Value.Trim(), HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            //Chat_Info Info = new Chat_Info();
            //Info.ContractKey = lblContractKey.Text.ToInt();
            //Info.Message = txt_Message.Value.Trim();
            //Info.CreatedDate = DateTime.Now;
            //Info.CreatedName = "...";
            //Info.Create();
        }

        #region [Ajax]
        [WebMethod]
        public static ItemReturn SendApprove(int TicketKey)
        {
            TrackStatus(TicketKey,
            "Gửi duyệt !.",
            HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = "OK";
            return zResult;
        }
        [WebMethod]
        public static ItemReturn Approve(int TicketKey)
        {
            Ticket_Info zTicket = new Ticket_Info();
            zTicket.TicketKey = TicketKey;
            zTicket.Approved = 1;
            zTicket.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zTicket.UpdateStatus();

            TrackStatus(TicketKey,
            "Đã duyệt !.",
            HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zTicket.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn NotApprove(int TicketKey)
        {
            Ticket_Info zTicket = new Ticket_Info();
            zTicket.TicketKey = TicketKey;
            zTicket.Approved = 0;
            zTicket.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zTicket.UpdateStatus();

            TrackStatus(TicketKey,
            "Không duyệt !.",
            HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zTicket.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn SendTrade(int TicketKey)
        {
            Ticket_Info zTicket = new Ticket_Info(TicketKey);

            #region check data
            int CustomerKeyA = Customer_Data.CheckExists(zTicket.DienThoaiA);
            int CustomerKeyB = Customer_Data.CheckExists(zTicket.DienThoaiB);
            int TradeKey = Trade_Data.CheckExists(TicketKey);
            #endregion

            #region pre init data
            Trade_Info zTrade = new Trade_Info(TradeKey);
            Project_Info zProject = new Project_Info(zTicket.CodeDuAn.ToInt());
            Product_Info zProduct = new Product_Info(zTicket.MaCan, zProject.ShortTable);
            ItemAsset zAsset = zProduct.ItemAsset;
            #endregion

            #region  thong tin giao dich
            zTrade.TransactionDate = DateTime.Now;
            zTrade.EmployeeKey = zTicket.EmployeeKey.ToInt();
            zTrade.DepartmentKey = zTicket.DepartmentKey.ToInt();
            zTrade.ProjectKey = zAsset.ProjectKey.ToInt();
            zTrade.AssetKey = zAsset.AssetKey.ToInt();
            zTrade.AssetID = zAsset.AssetID;
            zTrade.AssetType = zAsset.AssetType;
            float dientich = 0;
            float.TryParse(zAsset.AreaWall, out dientich);
            zTrade.Area = dientich;
            zTrade.AssetCategory = zAsset.CategoryKey.ToInt();

            if (zTicket.Category == 1)
                zTrade.TradeCategoryKey = 227;
            else
                zTrade.TradeCategoryKey = 228;

            zTrade.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zTrade.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zTrade.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zTrade.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];

            zTrade.IsDraft = zTicket.TicketKey; // dùng xác nhận giao dịch từ phiếu báng hàng
            zTrade.Save();
            #endregion

            #region thong tin khach a           
            Customer_Info zCustomer = new Customer_Info(CustomerKeyA);
            zCustomer.CustomerName = zTicket.HotenA;
            zCustomer.Phone1 = zTicket.DienThoaiA;
            zCustomer.CardID = zTicket.CMNDA;
            zCustomer.CardPlace = zTicket.NoiCapA;
            zCustomer.Birthday = Tools.ConvertToDate(zTicket.NgaySinhA);
            zCustomer.CardDate = Tools.ConvertToDate(zTicket.NgayCapA);
            zCustomer.Email1 = zTicket.EmailA;
            zCustomer.Address1 = zTicket.DiaChiLienHeA;
            zCustomer.Address2 = zTicket.DiaChiLienLacA;
            zCustomer.HasTrade = 1;
            zCustomer.Save();
            CustomerKeyA = zCustomer.Key;

            Trade_Customer_Info zTradeCustomer = new Trade_Customer_Info();
            zTradeCustomer.CustomerKey = CustomerKeyA;
            zTradeCustomer.Key = zTrade.Key;
            zTradeCustomer.Create();
            #endregion

            #region  thong tin khach b           
            zCustomer = new Customer_Info(CustomerKeyB);
            zCustomer.CustomerName = zTicket.HotenB;
            zCustomer.Phone1 = zTicket.DienThoaiB;
            zCustomer.CardID = zTicket.CMNDB;
            zCustomer.CardPlace = zTicket.NoiCapB;
            zCustomer.Birthday = Tools.ConvertToDate(zTicket.NgaySinhB);
            zCustomer.CardDate = Tools.ConvertToDate(zTicket.NgayCapB);
            zCustomer.Email1 = zTicket.EmailB;
            zCustomer.Address1 = zTicket.DiaChiLienHeB;
            zCustomer.Address2 = zTicket.DiaChiLienLacB;
            zCustomer.HasTrade = 1;
            zCustomer.CompanyName = zTicket.CongTy;
            zCustomer.Address3 = zTicket.DiaChiLienHeB;
            zCustomer.TaxCode = zTicket.MaSoThue;
            zCustomer.Description = "Đại diện " + zTicket.DaiDien;
            zCustomer.HasTrade = 1;
            zCustomer.Save();
            CustomerKeyB = zCustomer.Key;

            zTradeCustomer = new Trade_Customer_Info();
            zTradeCustomer.CustomerKey = CustomerKeyB;
            zTradeCustomer.Key = zTrade.Key;
            zTradeCustomer.Create();
            #endregion

            TrackStatus(TicketKey,
               "Đã gửi lập phiếu giao dịch !.",
               HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Result = zTrade.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static void TrackStatus(int TicketKey, string Message, string Employee)
        {
            string SQL = @"INSERT INTO SYS_Message 
(Message,Title,Date,ObjectTable,ObjectKey,EmployeeKey,DepartmentKey,Readed,Description) 
VALUES (N'" + Message + "',N'" + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode() + "', GETDATE(), N'FNC_Ticket',"
+ TicketKey + "," + Employee + "," + Employees_Data.GetDepartment(Employee.ToInt()) + ",0,N'Tình Trạng Ticket')";

            CustomInsert.Exe(SQL);
        }
        #endregion

        public static void ChatStatus(int TicketKey, string Message, string Employee)
        {
            string SQL = @"INSERT INTO SYS_Message 
(Message,Title,Date,ObjectTable,ObjectKey,EmployeeKey,DepartmentKey,Readed,Description) 
VALUES (N'" + Message + "',N'" + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode() + "', GETDATE(), N'CHAT',"
+ TicketKey + "," + Employee + "," + Employees_Data.GetDepartment(Employee.ToInt()) + ",0,N'Chat Ticket')";

            CustomInsert.Exe(SQL);
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            if (UnitLevel <= 2)
            {
                //                LitButton.Text = @"
                //<a id='btnApprove' href='#' class='btn btn-primary btn-white btn-sm'><i class='ace-icon fa fa-check'></i>&nbsp;Duyệt</a>
                //<a id='btnNoCheck' href='#' class='btn btn-primary btn-white btn-sm'><i class='ace-icon fa fa-backward'></i>&nbsp;Không Duyệt</a>";
            }
        }
        #endregion
    }
}