﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketSalesEdit.aspx.cs" Inherits="WebApp.FNC.TicketSalesEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Phiếu bán hàng</h1>
        </div>
        <div class="col-md-8">
            <table class="table-bordered table">
                <tr>
                    <th>Dự án:</th>
                    <td colspan="5">
                        <asp:DropDownList ID="DDLProject" runat="server" CssClass="form-control"></asp:DropDownList></td>
                </tr>
                <tr>
                    <th>NV bán hàng:</th>
                    <td colspan="2">Nguyễn Thị Ngọc Trinh</td>
                    <th>Ngày lập:</th>
                    <td colspan="2">01/11/2017</td>
                </tr>
                <tr>
                    <th>Phòng:</th>
                    <td colspan="2">Phú Nhuận</td>
                    <th>Quản lý: </th>
                    <td colspan="2">Nguyễn Thị Thanh Thủy</td>
                </tr>
                <tr>
                    <th colspan="6">Bên A (Bên Chuyển Nhượng)</th>
                </tr>
                <tr>
                    <th>Họ tên:</th>
                    <td colspan="5">
                        <input type="text" runat="server" class="form-control" id="txtCustomerNameA" placeholder="Nhập họ tên" /></td>
                </tr>
                <tr>
                    <th>SĐT:</th>
                    <td colspan="2">
                        <input type="text" runat="server" class="form-control" id="txt_PhoneA" placeholder="Nhập text" />
                    </td>
                    <th>Email:</th>
                    <td colspan="2">
                        <input type="text" runat="server" class="form-control" id="txt_EmailA" placeholder="Nhập text" />
                    </td>
                </tr>
                <tr>
                    <th>CMND/Passport:</th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_CardIDA" placeholder="Nhập text" /></td>
                    <th>Ngày cấp: </th>
                    <td>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                id="txt_CardDateA" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </td>
                    <th>Nơi cấp: </th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_CardPlaceA" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th>Địa chỉ thường trú:</th>
                    <td colspan="5">
                        <input type="text" runat="server" class="form-control" id="txt_AddressA1" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th>Địa chỉ liên hệ:</th>
                    <td colspan="5">
                        <input type="text" runat="server" class="form-control" id="txt_AddressA2" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th colspan="6">Bên B (Bên Nhận Chuyển Nhượng)</th>
                </tr>
                <tr>
                    <th>Họ tên:</th>
                    <td colspan="5">
                        <input type="text" runat="server" class="form-control" id="txtCustomerNameB" placeholder="Nhập họ tên" /></td>
                </tr>
                <tr>
                    <th>CMND/Passport:</th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_CardIDB" placeholder="Nhập text" /></td>
                    <th>Ngày cấp: </th>
                    <td>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                id="txt_CardDateB" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </td>
                    <th>Nơi cấp: </th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_CardPlaceB" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th>Địa chỉ thường trú:</th>
                    <td colspan="5">
                        <input type="text" runat="server" class="form-control" id="txt_AddressB1" placeholder="Nhập text" /></td>

                </tr>
                <tr>
                    <th>Địa chỉ liên hệ:</th>
                    <td colspan="5">
                        <input type="text" runat="server" class="form-control" id="txt_AddressB2" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th colspan="6">Sản phẩm</th>
                </tr>
                <tr>
                    <th>Mã căn:</th>
                    <td>
                        <asp:DropDownList ID="DDLAsset" runat="server" CssClass="form-control"></asp:DropDownList></td>
                    <th>Loại:</th>
                    <td>
                        <asp:DropDownList ID="DDLCategory" runat="server" CssClass="form-control"></asp:DropDownList></td>
                    <th>Diện tích m2</th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_Area" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th>Giá trị thực</th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_Amount" placeholder="Nhập text" /></td>
                    <th>Đặt cọc</th>
                    <td>
                        <input type="text" runat="server" class="form-control" id="txt_Deposit" placeholder="Nhập text" /></td>
                    <th>Ngày cọc</th>
                    <td>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                id="txt_DateConfirm" placeholder="Chọn ngày tháng năm" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Môi giới/CTV</th>
                    <td colspan="2">
                        <input type="text" runat="server" class="form-control" id="txt_Reference" placeholder="Nhập text" /></td>
                    <th>Phí môi giới CTV</th>
                    <td colspan="2">
                        <input type="text" runat="server" class="form-control" id="txt_ReferencePecent" placeholder="Nhập số" /></td>
                </tr>
                <tr>
                    <th colspan="6">Thỏa thuận khác</th>
                </tr>
                <tr>
                    <td colspan="6">
                        <textarea class="form-control" id="txt_Description" maxlength="500" style="margin-top: 0px; margin-bottom: 0px; height: 60px;" runat="server"></textarea></td>
                </tr>
                <tr>
                    <th colspan="6">Nội dung lập hợp đồng</th>
                </tr>
                <tr>
                    <th>Tổng giá trị</th>
                    <td><input type="text" runat="server" class="form-control" id="txt_AmountTotal" placeholder="Nhập text" /></td>
                    <th>Thanh toán nhận số hồng</th>
                    <td><input type="text" runat="server" class="form-control" id="txt_MoneyNote" placeholder="Nhập text" /></td>
                    <th>B thanh toán cho A</th>
                    <td><input type="text" runat="server" class="form-control" id="txt_BPayA" placeholder="Nhập text" /></td>
                </tr>
                <tr>
                    <th>Phí dịch vụ cho bên C</th>
                    <td colspan="5"></td>
                </tr>
            </table>

            <table class="table table-bordered">
                <tr>
                    <th colspan="4">Lịch thanh toán</th>
                </tr>
                <tr>
                    <th>Đợt 1</th>
                    <td>03/11/2017</td>
                    <td>200,000,000 VNĐ</td>
                    <td>Bên B thanh toán cho Bên A số tiền đặt cọc</td>
                </tr>
                <tr>
                    <th>Đợt 2</th>
                    <td>04/11/2017</td>
                    <td>3,300,000,000 VNĐ</td>
                    <td>hai Bên ra Công chứng chuyển nhượng hợp đồng mua bán</td>
                </tr>
                <tr>
                    <th>Đợt 1</th>
                    <td>11/11/2017</td>
                    <td>70,000,000 VNĐ</td>
                    <td>Bên A hoàn tất nghĩa vụ thuế. Bên B thanh toán cho Bên A số còn lại</td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
