﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="KPI2.aspx.cs" Inherits="WebApp.SAL.KPI2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
    <link rel="stylesheet" href="/SAL/KPI.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Thống kê
                    </div>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td style="width: 120px">
                                <div class="form-group">
                                    <label>Thời gian</label>
                                    <div class="radio">
                                        <label>
                                            <input class="ace" value="chonngay" name="rdo2" type="radio" checked="checked" />
                                            <span class="lbl">Ngày</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input class="ace" value="chonthang" name="rdo2" type="radio" />
                                            <span class="lbl">Tháng</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input class="ace" value="chonquy" name="rdo2" type="radio" />
                                            <span class="lbl">Quý</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input class="ace" value="chonnam" name="rdo2" type="radio" />
                                            <span class="lbl">Năm</span>
                                        </label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" id="chonngay" type="an2">
                                    <input type="text" class="form-control pull-right" style="display: none"
                                        id="txtCurrentDate" placeholder="Ngày hiện tại" />
                                </div>
                                <div class="form-group" id="chonthang" type="an2">
                                    <asp:DropDownList ID="DDLMonth" runat="server" class="select2">
                                        <asp:ListItem Value="0" Text="--Tháng--"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" id="chonquy" type="an2">
                                    <asp:DropDownList ID="DDLQuarter" runat="server" class="select2">
                                        <asp:ListItem Value="0" Text="--Quý--"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" id="chonnam" type="an2">
                                    <asp:DropDownList ID="DDLYear" runat="server" class="select2">
                                        <asp:ListItem Value="0" Text="--Năm--"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                        <tr id="tochuc">
                            <td>
                                <div class="form-group">
                                    <label>Tổ chức</label>
                                    <div class="radio" id="level1">
                                        <label>
                                            <input class="ace" value="choncongty" name="rdo1" type="radio" checked="checked" />
                                            <span class="lbl">Công ty</span>
                                        </label>
                                    </div>
                                    <div class="radio" id="level2">
                                        <label>
                                            <input class="ace" value="chonphong" name="rdo1" type="radio" />
                                            <span class="lbl">Phòng</span>
                                        </label>
                                    </div>
                                    <div class="radio" id="level3">
                                        <label>
                                            <input class="ace" value="chonnhanvien" name="rdo1" type="radio" />
                                            <span class="lbl">Nhân viên</span>
                                        </label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div id="chonphong" type="an">
                                    <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                </div>
                                <div id="chonnhanvien" type="an">
                                    <asp:DropDownList ID="DDL_Employee" runat="server" class="select2" AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="form-group">
                                    <label>Mục tiêu</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="ace" value="306" name="chkPurpose" checked="checked" />
                                            <span class="lbl">Chăm sóc khách hàng</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="ace" value="307" name="chkPurpose" checked="checked" />
                                            <span class="lbl">Cập nhật sản phẩm</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="ace" value="291" name="chkPurpose" checked="checked" />
                                            <span class="lbl">Khách phát sinh</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="ace" value="290" name="chkPurpose" checked="checked" />
                                            <span class="lbl">Khách dự án</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="ace" value="298" name="chkPurpose" checked="checked" />
                                            <span class="lbl">Cọc/ Ký HĐ</span>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="ace" value="295" name="chkPurpose" checked="checked" />
                                            <span class="lbl">Hoàn tất HĐ</span>
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <a id="btnSearch" class="btn btn-primary" data-dismiss="modal">
                            <i class="ace-icon fa fa-search"></i>
                            Xem
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="page-content">
        <div class="page-header">
            <h1><span id="title">Năng xuất làm việc trong ngày</span>
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-12" id="noidung">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
            <div class="col-sm-12">
                <h4 id="text"></h4>
                <table class='table table-bordered' id="noidung2">
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HIDFromDate" runat="server" />
    <asp:HiddenField ID="HIDToDate" runat="server" />
    <asp:HiddenField ID="HIDEmployee" runat="server" />
    <asp:HiddenField ID="HIDDepartment" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script src="/SAL/KPI.js"></script>
</asp:Content>
