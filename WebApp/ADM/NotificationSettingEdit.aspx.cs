﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.ADM
{
    public partial class NotificationSettingEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AutoKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_AutoKey.Value != "0")
            {
                Option_Info zInfo = new Option_Info(HID_AutoKey.Value.ToInt());
                txt_Value.Value = zInfo.Value.ToString();
                txt_Name.Value = zInfo.OptionName;
                txt_Description.Value = zInfo.OptionSummarize;

                Lit_TitlePage.Text = "Sửa thông tin " + zInfo.OptionName;
            }
        }

        [WebMethod]
        public static ItemReturn SaveOption(int AutoKey, string Name, string Description, int Value)
        {
            ItemReturn zResult = new ItemReturn();
            Option_Info zInfo = new Option_Info(AutoKey);
            zInfo.OptionName = Name;
            zInfo.OptionSummarize = Description;
            zInfo.Value = Value;
            zInfo.Update();
            zResult.Message = zInfo.Message;
            return zResult;
        }


    }
}