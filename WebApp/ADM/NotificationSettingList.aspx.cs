﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.ADM
{
    public partial class NotificationSettingList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoaData();
            }
        }

        void LoaData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Option_Data.List();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Mô tả</th>");
            zSb.AppendLine("        <th>Thứ tự</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["OptionKey"].ToString() + "'>");
                    zSb.AppendLine("                <td>" + (i++) + "</td>");
                    zSb.AppendLine("                <td>" + r["OptionName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["OptionSummarize"].ToString() + "</td>");
                    zSb.AppendLine("                <td><span class='editable editable-click'>" + r["Value"].ToString() + "</span></td>");
                    zSb.AppendLine("            </tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
    }
}