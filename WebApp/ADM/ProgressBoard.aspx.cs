﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web.Services;

/*
 category = 1 luy tien truc tiep
 2 luy tien % lien ket
     */
namespace WebApp.ADM
{
    public partial class ProgressBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDLDepartment, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);
                Tools.DropDown_DDL(DDLEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY DepartmentKey", false);
                LoadEmployee();              
            }
        }

        void LoadEmployee()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Employees_Data.ListWorking();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblDataEmployee'>");

            zSb.AppendLine("<thead><tr><th>Họ tên</th></tr></thead>");
            zSb.AppendLine("<tbody>");
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<tr id='" + r["EmployeeKey"].ToString() + "'><td>" + r["LastName"].ToString() + " " + r["FirstName"].ToString() + "</td></tr>");
            }
            zSb.AppendLine("</tbody>");
            zSb.AppendLine("</table>");
            LitEmployee.Text = zSb.ToString();           
        }      

        [WebMethod]
        public static ItemReturn LoadBoard(string Employee)
        {
            Employees_Info zInfo = new Employees_Info(Employee.ToInt());
          
            ItemReturn zResult = new ItemReturn();
            DataTable zTable = Progressive_Board_Data.List(Employee.ToInt());
            DataRow[] results = null;
            StringBuilder zSb = new StringBuilder();
            StringBuilder zSb2 = new StringBuilder();
            StringBuilder zSb3 = new StringBuilder();
            zSb.AppendLine("<table class='table table-bordered' id='tblDataBoard'>");
            zSb.AppendLine("    <thead><tr><th colspan=5>Bảng tính hiệu quả công việc</th></tr></thead>");
            zSb.AppendLine("    <tr select='0'>");
            zSb.AppendLine("        <th colspan=5>Giao dịch trực tiếp<a class='btn btn-sm btn-white btn-default btn-round pull-right' id='btnAdd1' onclick='AddRowTrucTiep()'>Thêm</a></th>");
            zSb.AppendLine("    </tr>");
            #region bang con
            zSb2.AppendLine("               <tr select='0'><th class='td15'>Từ (VNĐ)</th><th class='td15'>Đến (VNĐ)</th><th class='td15'>Tỷ lệ %</th><th>#</th><th>#</th></tr>");
            #region [truc tiep]
            results = zTable.Select("[Category]=1");
            if (results.Length > 0)
            {
                foreach (DataRow r in results)
                {
                    zSb2.AppendLine("<tr select='0' data-id='" + r["AutoKey"] + "' category='1'><td data-field='FromMoney'>"
                        + r["FromMoney"].ToDoubleString() + "</td><td data-field='ToMoney'>"
                        + r["ToMoney"].ToDoubleString() + "</td><td data-field='Rate'>"
                        + r["Rate"] + @" </td><td></td><td>                      
<a class='btn btn-sm btn-white btn-default btn-round edit'><i class='fa fa-pencil'></i></a>
<a class='btn btn-sm btn-white btn-default btn-round delete' btn='delete'><i class='fa fa-trash'></i></a></td></tr>");
                }
            }
            else
            {
                zSb2.AppendLine(@"<tr select='0' data-id='0' category='1'><td data-field='FromMoney'>0</td><td data-field='ToMoney'>0</td><td data-field='Rate'>0</td><td></td><td>
<a class='btn btn-sm btn-white btn-default btn-round edit'><i class='fa fa-pencil'></i></a>
<a class='btn btn-sm btn-white btn-default btn-round delete' btn='delete'><i class='fa fa-trash'></i></a></td></tr>");
            }
            #endregion
            zSb.AppendLine(zSb2.ToString());
            zSb.AppendLine("    <tr select='1'>");
            zSb.AppendLine("        <th colspan=5>Giao dịch liên kết<a class='btn btn-sm btn-white btn-default btn-round pull-right' id='btnAdd2' onclick='AddRowLienKet()'>Thêm</a></th></th>");
            zSb.AppendLine("    </tr>");
            #region [luy tien]
            results = zTable.Select("[Category]=2");
            if (results.Length > 0)
            {
                foreach (DataRow r in results)
                {
                    zSb3.AppendLine("<tr select='1' data-id='" + r["AutoKey"] + "' category='2'><td data-field='FromMoney'>"
                        + r["FromMoney"].ToDoubleString() + "</td><td data-field='ToMoney'>"
                        + r["ToMoney"].ToDoubleString() + "</td><td data-field='Rate'>"
                        + r["Rate"] + @" </td><td></td><td>                     
<a class='btn btn-sm btn-white btn-default btn-round edit'><i class='fa fa-pencil'></i></a>                        
<a class='btn btn-sm btn-white btn-default btn-round delete' btn='delete'><i class='fa fa-trash'></i></a></td></tr>");
                }
            }
            else
            {
                zSb3.AppendLine(@"<tr select='1' data-id='0' category='2'><td data-field='FromMoney'>0</td><td data-field='ToMoney'>0</td><td data-field='Rate'>0</td><td></td><td>
<a class='btn btn-sm btn-white btn-default btn-round edit'><i class='fa fa-pencil'></i></a>
<a class='btn btn-sm btn-white btn-default btn-round delete' btn='delete'><i class='fa fa-trash'></i></a></td></tr>");
            }
            #endregion
            zSb.AppendLine(zSb3.ToString());
            #endregion                        
            zSb.AppendLine("</table>");

            zResult.Result = zSb.ToString();
            zResult.Result2 = zInfo.Method.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemReturn CopyRow(string Employee1, string Employee2)
        {
            ItemReturn zResult = new ItemReturn();
            DataTable zTable = Progressive_Board_Data.List(Employee1.ToInt());

            if (zTable.Rows.Count > 0)
            {
                string SQL = " DELETE HRM_Progressive_Board WHERE EmployeeKey =" + Employee2;
                SQL += @" INSERT HRM_Progressive_Board (EmployeeKey, FromMoney, ToMoney, Rate, Category) 
SELECT " + Employee2 + " Employee, FromMoney, ToMoney, Rate, Category FROM HRM_Progressive_Board WHERE EmployeeKey=" + Employee1.ToInt();

                CustomInsert.Exe(SQL);
            }
            else
            {
                zResult.Message = "Không có dữ liệu để copy !.";
                return zResult;
            }

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblDataBoard'>");
            zSb.AppendLine("<tr><th class='td15'>Từ (VNĐ)</th><th class='td15'>Đến (VNĐ)</th><th class='td15'>Tỷ lệ %</th><th>#</th><th>#</th></tr>");

            zTable = Progressive_Board_Data.List(Employee2.ToInt());
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<tr data-id='" + r["AutoKey"] + "'><td data-field='FromMoney'>"
                + r["FromMoney"].ToDoubleString() + "</td><td data-field='ToMoney'>"
                + r["ToMoney"].ToDoubleString() + "</td data-field='Rate'><td>"
                + r["Rate"] + @" </td><td></td><td>
                    <a class='btn btn-sm btn-white btn-default btn-round edit'><i class='fa fa-pencil'></i></a>
                    <a class='btn btn-sm btn-white btn-default btn-round delete' btn='delete'><i class='fa fa-trash'></i></a></td></tr>");
                }
            }
            zSb.AppendLine("</table>");

            zResult.Message = "OK";
            zResult.Result = zSb.ToString();
            return zResult;
        }
        [WebMethod]
        public static string UpdateMethod(string Employee, string Method)
        {
            Employees_Info zInfo = new Employees_Info();
            zInfo.Key = Employee.ToInt();
            zInfo.Method = Method.ToInt();
            zInfo.UpdateMethod();

            return zInfo.Message;
        }
        [WebMethod]
        public static string DeleteRow(string Employee, string AutoKey)
        {
            Progressive_Board_Info zInfo = new Progressive_Board_Info();
            zInfo.EmployeeKey = Employee.ToInt();
            zInfo.AutoKey = AutoKey.ToInt();
            zInfo.Delete();

            return zInfo.Message;
        }
        [WebMethod]
        public static string SaveRow(string Employee, string AutoKey, string Category, ItemBoard Object)
        {
            Progressive_Board_Info zInfo = new Progressive_Board_Info(AutoKey.ToInt());
            zInfo.EmployeeKey = Employee.ToInt();
            zInfo.Category = Category.ToInt();
            zInfo.FromMoney = Object.FromMoney;
            zInfo.ToMoney = Object.ToMoney;
            zInfo.Rate = Object.Rate;
            zInfo.Save();

            return zInfo.Message;
        }
    }
}