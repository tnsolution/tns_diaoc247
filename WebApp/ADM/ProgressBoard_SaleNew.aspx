﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProgressBoard_SaleNew.aspx.cs" Inherits="WebApp.ADM.ProgressBoard_SaleNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .highlight {
            background-color: lightblue;
        }

        .td15 {
            width: 15%;
        }

        #tblDataBoard td:last-child {
            text-align: right;
        }
    </style>
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Cài đặt cách tính hoa hồng bán mới
                    <span class="tools pull-right">
                        <a href="#modal" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                            <i class="ace-icon fa fa-plus"></i>
                            Tạo mới
                        </a>
                    </span>
            </h1>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class='box box-primary'>
                    <div class='box-header with-border'>
                    </div>
                    <div class='box-body'>
                        <div class='col-xs-12 table-responsive'>
                            <table class='table table-hover table-bordered' id='tableProgressive'>
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên dự án</th>
                                        <th>Doanh thu hoặc Giá bán</th>
                                        <th>Phần trăm hoặc Lũy tiến</th>
                                        <th>Kích hoạt sử dụng</th>
                                        <th>Ngày cập nhật</th>
                                        <th>Ghi chú</th>
                                        <th class='thAction'>...</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Literal ID='Literal_Table' runat='server'></asp:Literal>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-default" id="modal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">CHI TIẾT</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>Chọn dự án (*)</td>
                                        <td>
                                            <asp:DropDownList ID="DDL_Project" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>Doanh thu hoặc Giá bán</td>
                                        <td>
                                            <asp:DropDownList ID="DDLMethod1" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Doanh thu"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Giá bán"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phần trăm hoặc Lũy tiến</td>
                                        <td>
                                            <asp:DropDownList ID="DDLMethod2" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="%"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Lũy tiến"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kích hoạt sử dụng</td>
                                        <td>
                                            <asp:DropDownList ID="DDLActive" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" disabled="disabled" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Có"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Không"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ghi chú</td>
                                        <td>
                                            <textarea id="txt_Description" cols="20" rows="4" class="form-control" placeholder="Ghi chú"></textarea></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnInitProduct">Tiếp tục</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Project" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $("#btnInitProduct").click(function () {
            var project = $("[id$=DDL_Project]").val();
            var projectname = $("[id$=DDL_Project] option:selected").text();
            if (project == null || project == undefined) {
                alert("Bạn phải chọn dự án !.");
                return false;
            }
            var method1 = $("[id$=DDLMethod1]").val();
            var method1name = $("[id$=DDLMethod1] option:selected").text();
            if (method1 == null || method1 == undefined) {
                alert("Doanh thu, giá bán !.");
                return false;
            }
            var method2 = $("[id$=DDLMethod2]").val();
            var method2name = $("[id$=DDLMethod2] option:selected").text();
            if (method2 == null || method2 == undefined) {
                alert("Phần tră, lũy tiến !.");
                return false;
            }
            var active = $("[id$=DDLActive]").val();
            if (active == null || active == undefined) {
                alert("Chọn sử dụng cach tích hoặc không !.");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "/ADM/ProgressBoard_SaleNew_Edit.aspx/InitNew",
                data: JSON.stringify({
                    "ProjectKey": project,
                    "ProjectName": projectname,
                    "Method1": method1,
                    "Method1Name": method1name,
                    "Method2": method2,
                    "Method2Name": method2name,
                    "Active": active,
                    "Description": $("#txt_Description").val(),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d != "ERROR")
                        $(location).attr('href', '/ADM/ProgressBoard_SaleNew_Edit.aspx?ID=' + msg.d);
                    else {
                        alert("Lỗi khởi tạo !.");
                        $('.se-pre-con').fadeOut('slow');
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        $("#tableProgressive tr").on("click", "td:not(:last-child)", function () {
            var id = $(this).closest('tr').attr('id');
            $(location).attr('href', '/ADM/ProgressBoard_SaleNew_Edit.aspx?ID=' + id);
        });
        $('#tableProgressive tr').on('click', 'a[btn=delete]', function () {
             var id = $(this).closest('tr').attr('id');
             if (confirm('Bạn có chắc xóa. Các thông tin kèm theo cũng sẽ mất !.')) {
                $.ajax({
                    type: 'POST',
                    url: 'ProgressBoard_SaleNew.aspx/DeleteParent',
                    data: JSON.stringify({
                        'AutoKey': id
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    success: function (msg) {
                       $(location).attr('href', '/ADM/ProgressBoard_SaleNew.aspx');
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        });
    </script>
</asp:Content>
