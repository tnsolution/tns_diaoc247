﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web.Services;

namespace WebApp.ADM
{
    /// <summary>
    /// DDLMethod1 1 doanh thu 2 gia bán, method2 1 %, 2 luy tiến
    /// </summary>
    public partial class ProgressBoard_SaleNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM dbo.PUL_Project", false);
                LoadData();
            }
        }

        protected void LoadData()
        {
            int no = 1;
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HRM_Progressive_Board_SaleNew_Data.List();
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                zSb.AppendLine("               <td>" + (no++) + "</td>");
                zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                zSb.AppendLine("               <td>" + r["Method1Name"].ToString() + "</td>");
                zSb.AppendLine("               <td>" + r["Method2Name"].ToString() + "</td>");
                zSb.AppendLine("               <td>" + (r["Active"].ToInt() == 1 ? "Có":"Không") + "</td>");
                zSb.AppendLine("               <td>" + r["ModifiedDate"].ToDateString() + "</td>");
                zSb.AppendLine("               <td>" + r["Description"].ToString() + "</td>");
                zSb.AppendLine("                   <td><a btn='delete' data-skin='skin-red' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i></a></td>");
                zSb.AppendLine("            </tr>");
            }

            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static string DeleteParent(int AutoKey)
        {
            Progressive_Board_SaleNew_Info zInfo = new Progressive_Board_SaleNew_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                return zInfo.Message;
            return "OK";
        }
    }
}