﻿using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.ADM
{
    public partial class ProgressBoard_SaleNew_Edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                {
                    HID_AutoKey.Value = Request["ID"];
                    int Key = HID_AutoKey.Value.ToInt();
                    Progressive_Board_SaleNew_Info zInfo = new Progressive_Board_SaleNew_Info(Key);

                    LitProject.Text = zInfo.ProjectName;
                    LitMethod1.Text = zInfo.Method1Name;
                    LitMethod2.Text = zInfo.Method2Name;
                    LitDescription.Text = zInfo.Description;
                    LitActive.Text = (zInfo.Active == 1 ? "Có" : "Không");

                    Literal_Table.Text = Html_Table(HRM_Progressive_Board_SaleNew_Detail_Data.List(Key));

                    HID_AutoKey.Value = zInfo.AutoKey.ToString();
                    HID_ProjectKey.Value = zInfo.ProjectKey.ToString();
                }
            }
        }

        public static string Html_Table(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + r["FromMoney"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td>" + r["ToMoney"].ToDoubleString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Rate"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("               <td><a btn='delete' data-skin='skin-red' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i></a></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else { zSb.AppendLine("<tr><td colspan='11'>Chưa có dữ liệu</td></tr>"); }
            return zSb.ToString();
        }

        [WebMethod]
        public static string InitNew(string ProjectKey, string ProjectName, string Method1, string Method1Name, string Method2, string Method2Name, string Active, string Description)
        {
            Project_Info zProject = new Project_Info(ProjectKey.ToInt());
            Progressive_Board_SaleNew_Info zInfo = new Progressive_Board_SaleNew_Info();
            zInfo.ProjectKey = ProjectKey.ToInt();
            zInfo.ProjectName = zProject.ProjectName;
            zInfo.MainTable = zProject.MainTable;
            zInfo.ShortTable = zProject.ShortTable;
            zInfo.Active = Active.ToInt();
            zInfo.Method1 = Method1.ToInt();
            zInfo.Method1Name = Method1Name;
            zInfo.Method2 = Method2.ToInt();
            zInfo.Method2Name = Method2Name;
            zInfo.Description = Description;

            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Create();
            if (zInfo.Message == string.Empty)
                return zInfo.AutoKey.ToString();
            else
                return "ERROR";
        }

        [WebMethod]
        public static string SaveDetail(int AutoKey, string ParentKey, string ProjectKey, string FromMoney, string ToMoney, string Rate, string Description)
        {
            Progressive_Board_SaleNew_Detail_Info zInfo = new Progressive_Board_SaleNew_Detail_Info(AutoKey);
            zInfo.ProjectKey = ProjectKey.ToInt();
            zInfo.FromMoney = FromMoney.ToDouble();
            zInfo.ToMoney = ToMoney.ToDouble();
            zInfo.Rate = Rate.ToFLoat();
            zInfo.ParentKey = ParentKey.ToInt();
            zInfo.Description = Description.Trim();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.Save();
            if (zInfo.Message == string.Empty)
                return zInfo.AutoKey.ToString();
            else
                return "Lỗi không lưu được";
        }

        [WebMethod]
        public static string LoadDetail(int AutoKey)
        {
            Progressive_Board_SaleNew_Detail_Info zInfo = new Progressive_Board_SaleNew_Detail_Info(AutoKey);
            Progressive_Board_SaleNew_Detail_Item zItem = new Progressive_Board_SaleNew_Detail_Item();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ProjectKey = zInfo.ProjectKey.ToString();
            zItem.FromMoney = zInfo.FromMoney.ToString();
            zItem.ToMoney = zInfo.ToMoney.ToString();
            zItem.ParentKey = zInfo.ParentKey.ToString();
            zItem.Rate = zInfo.Rate.ToString();
            var temp = JsonConvert.SerializeObject(zItem);
            return temp;
        }

        [WebMethod]
        public static string DeleteDetail(int AutoKey)
        {
            Progressive_Board_SaleNew_Detail_Info zInfo = new Progressive_Board_SaleNew_Detail_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không xóa được !";
        }
    }
}