﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="SupportFee.aspx.cs" Inherits="WebApp.ADM.SupportFee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Cài đặt phí hổ trợ
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="Lit_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mDetail">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chỉnh sửa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Chức vụ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="DDLPosition" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Tất cả--" Selected="True" disabled></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Loại giao dịch</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="DDLTradeCategory" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Tất cả--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nguồn giao dịch</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="DDLTradeType" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Tất cả--" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Trực tiếp"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Liên kết"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Cách tính</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="DDLMethod" runat="server" class="form-control select2" AppendDataBoundItems="true" require="true">
                                            <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Tính % doanh số"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Từng giao dịch"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tên phí</label>
                                    <div class="col-sm-9">
                                        <input id="txtName" type="text" class="form-control" placeholder="Nhập text" require="true" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Số tiền</label>
                                    <div class="col-sm-9">
                                        <input id="txtMoney" type="text" class="form-control" placeholder="Nhập số tiền" moneyinput="true" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Số %</label>
                                    <div class="col-sm-9">
                                        <input id="txtNumber" type="text" class="form-control" placeholder="Nhập số %" moneyinput="true" maxlength="5" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea id="txtDescription" class="form-control" placeholder="Ghi chú"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type='button' class='btn btn-primary' data-dismiss='modal' id='btnSave'>
                        Cập nhật</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="/template/table-edits.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {


            $(".select2").select2({ width: "100%" });
            $('input[moneyinput]').number(true, 2);
            $("#tblData tbody").on("click", "a[name=btnXoa]", function (e) {
                if (confirm("Bạn có chắc xóa thông tin !.")) {
                    var trid = $(this).closest('tr').attr('id');
                    $.ajax({
                        type: "POST",
                        url: "/ADM/SupportFee.aspx/DeleteRecord",
                        data: JSON.stringify({
                            "AutoKey": trid,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {

                        },
                        success: function (msg) {
                            if (msg.d === "OK")
                                $("tr#" + trid).remove();
                            else
                                alert(msg.d);
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $("#tblData tbody").on("click", "tr td:not(:last-child)", function (e) {
                var trid = $(this).closest('tr').attr('id');
                $.ajax({
                    type: "POST",
                    url: "/ADM/SupportFee.aspx/GetRecord",
                    data: JSON.stringify({
                        "AutoKey": trid,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#mDetail').modal({
                            backdrop: true,
                            show: true
                        });

                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                        $("[id$=DDLPosition]").val(msg.d.PositionKey);
                        $("[id$=DDLTradeCategory]").val(msg.d.TradeCategory).trigger("change");
                        $("[id$=DDLTradeType]").val(msg.d.TradeType).trigger("change");
                        $("[id$=DDLMethod]").val(msg.d.Method).trigger("change");
                        $("#txtMoney").val(msg.d.Money);
                        $("#txtDescription").val(msg.d.Description);
                        $("#txtNumber").val(msg.d.Number);
                        $("#txtName").val(msg.d.Name);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSave").click(function () {
                $(".require").each(function () {
                    if ($.trim(
                        $(this).val()).length == 0 ||
                        $(this).val() == 0) {
                        alert("Bạn phải nhập đủ thông tin");
                        $(this).focus();
                        return false;
                    }
                });

                var chucvu = $("[id$=DDLPosition]").val();
                var loaigiaodich = $("[id$=DDLTradeCategory]").val();
                var nguongiaodich = $("[id$=DDLTradeType]").val();
                var cachtinh = $("[id$=DDLMethod]").val();
                var sotien = $("#txtMoney").val();
                var ghichu = $("#txtDescription").val();
                var tenphi = $("#txtName").val();
                var sophamtram = $("#txtNumber").val();

                var Object = {
                    PositionKey: chucvu,
                    TradeCategory: loaigiaodich,
                    TradeType: nguongiaodich,
                    Method: cachtinh,
                    Money: sotien,
                    Number: sophamtram,
                    Description: ghichu,
                    Name: tenphi
                };

                $.ajax({
                    type: 'POST',
                    url: '/ADM/SupportFee.aspx/SaveRecord',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Object': Object
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Result == "") {
                            location.reload();
                        }
                        else {
                             $('.se-pre-con').fadeOut('slow');
                            Page.showNotiMessageError("Lỗi nhập dòng tin", msg.d.Message);
                        }
                    },
                    complete: function () {
                        $("[id$=HID_AutoKey]").val(0);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>

</asp:Content>
