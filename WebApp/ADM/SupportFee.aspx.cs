﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

/*
 * tradetype
 1 ho tro truc tiep
 2 ho tro lien ket

    method
    1 Tính % doanh số
    2 Từng giao dịch

     */

namespace WebApp.ADM
{
    public partial class SupportFee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDLPosition, "SELECT PositionKey, Position FROM HRM_Positions ORDER BY Rank", false);
                Tools.DropDown_DDL(DDLTradeCategory, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 29", false);

                TableHtml();
            }
        }

        void TableHtml()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Commission_Support_Data.List();

            zSb.AppendLine("<table class='table table-bordered table-hover' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>Chức vụ</th>");
            zSb.AppendLine("        <th>Tên phí</th>");
            zSb.AppendLine("        <th>Loại giao dịch</th>");
            zSb.AppendLine("        <th>Nguồn giao dịch</th>");
            zSb.AppendLine("        <th>Số tiền</th>");
            zSb.AppendLine("        <th>Số %</th>");
            zSb.AppendLine("        <th>Cách tính</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th><a href='#mDetail' data-toggle='modal' id='btnAdd'><i class='ace-icon fa fa-plus green'></i>Thêm</a></th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    DataRow r = zTable.Rows[i];

                    zSb.AppendLine("<tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("<td>" + r["Position"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Name"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Category"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Source"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("<td>" + r["Number"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Method"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("<td><a href='' class='btn btn-sm btn-danger' name='btnXoa'>Xóa</a></td>");
                    zSb.AppendLine("</tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemCommission GetRecord(int AutoKey)
        {
            ItemCommission zItem = new ItemCommission();
            Commission_Support_Info zInfo = new Commission_Support_Info(AutoKey);
            zItem.Name = zInfo.Name;
            zItem.TradeCategory = zInfo.TradeCategory;
            zItem.TradeType = zInfo.TradeType;
            zItem.Description = zInfo.Description;
            zItem.AutoKey = zInfo.AutoKey;
            zItem.Money = zInfo.Money.ToString();
            zItem.Number = zInfo.Number.ToString();
            zItem.Method = zInfo.Method;
           
            return zItem;
        }

        [WebMethod]
        public static ItemReturn SaveRecord(int AutoKey, ItemCommission Object)
        {
            Commission_Support_Info zInfo = new Commission_Support_Info(AutoKey);
            if (Object.PositionKey < 4)
            {
                zInfo.ID = "TP";
            }
            else
            {
                zInfo.ID = "NV";
            }

            zInfo.Active = 1;
            zInfo.PositionKey = Object.PositionKey;
            zInfo.TradeCategory = Object.TradeCategory;
            zInfo.TradeType = Object.TradeType;
            zInfo.Description = Object.Description;
            zInfo.Method = Object.Method;
            zInfo.Name = Object.Name;
            double SoTien = 0;
            double.TryParse(Object.Money, out SoTien);
            float SoPhanTram = 0;
            float.TryParse(Object.Number, out SoPhanTram);
            zInfo.Money = SoTien;
            zInfo.Number = SoPhanTram;
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["UserName"].ToUrlDecode();
            zInfo.Save();
            ItemReturn zResult = new ItemReturn();
            if (zInfo.Message != string.Empty)
            {
                zResult.Result = "ERROR";
                zResult.Message = zInfo.Message;
            }
            else
            {
                zResult.Result = "";
                zResult.Message = "";
            }
            return zResult;
        }

        [WebMethod]
        public static string DeleteRecord(int AutoKey)
        {
            try
            {
                ItemReturn zResult = new ItemReturn();
                Commission_Support_Info zInfo = new Commission_Support_Info(AutoKey);
                zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["UserName"].ToUrlDecode();
                zInfo.Delete();
                zResult.Message = zInfo.Message;
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}