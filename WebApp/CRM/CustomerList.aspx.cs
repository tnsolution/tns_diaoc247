﻿using Lib.CRM;
using Lib.SYS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.CRM
{
    public partial class CustomerList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Furnitur, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                InitView();
            }
        }
        protected void ViewHTML(List<ItemCustomer> List)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên khách hàng</th>");
            zSb.AppendLine("        <th>SĐT</th>");
            zSb.AppendLine("        <th>Email</th>");
            zSb.AppendLine("        <th>Dự án</th>");
            zSb.AppendLine("        <th>Sản phẩm</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("        <th>Ngày cập nhật</th>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            int n = 1;
            foreach (ItemCustomer r in List)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "' type='" + r.CategoryKey + "'>");
                zSb.AppendLine("               <td>" + (n++).ToString() + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br>" + r.Phone2 + "</td>");
                zSb.AppendLine("               <td>" + r.Email1 + "<br>" + r.Email2 + "</td>");
                zSb.AppendLine("               <td>" + r.ProjectName + "</td>");
                zSb.AppendLine("               <td>" + r.CategoryName + "</td>");
                zSb.AppendLine("               <td>" + r.Status + "</td>");
                zSb.AppendLine("               <td>" + r.ModifiedDate + "</td>");
                zSb.AppendLine("               <td class='notclick'><div class='action-buttons'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>");
                zSb.AppendLine("            </tr>");
                zSb.AppendLine("            <tr class='detail-row'>");
                zSb.AppendLine("                <td colspan='8'>");
                zSb.AppendLine("                    <div class='table-detail'><div class='row' customer='" + r.CustomerKey + "'></div></div>");
                zSb.AppendLine("                </td>");
                zSb.AppendLine("            </tr>");
            }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Literal_Table.Text = zSb.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Category">loai khach, 1 quan tam, 2 giao dich, 3 ký gửi giao dịch, 0 tat ca</param>
        /// <param name="Status">muc do tiem nang</param>
        /// <param name="Project"></param>
        /// <param name="CategoryAsset"></param>
        /// <param name="Name"></param>
        /// <param name="Phone"></param>
        /// <param name="TradeType">loai giao dich ban moi, chuyen nhuong, cho thue</param>
        /// <param name="Department"></param>
        /// <param name="Employee"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string Search(int Category, string Status, string Project, string CategoryAsset, string Name, string Phone, string TradeType, int Department, int Employee, string FromDate, string ToDate, string Inside, string Bed)
        {
            ItemCRMSearch ISearch = new ItemCRMSearch();
            ISearch.Category = Category.ToString();
            ISearch.Project = Project;
            ISearch.Employee = Employee.ToString();
            ISearch.Department = Department.ToString();
            ISearch.CategoryAsset = CategoryAsset;
            ISearch.Name = Name;
            ISearch.Phone = Phone;
            ISearch.FromDate = FromDate;
            ISearch.ToDate = ToDate;
            ISearch.Inside = Inside;
            ISearch.Bed = Bed;
            if (Status != string.Empty)
                ISearch.Status = Status.Remove(Status.LastIndexOf(","));
            if (TradeType != string.Empty)
                ISearch.TradeType = TradeType.Remove(TradeType.LastIndexOf(","));
            List<ItemCustomer> zList = new List<ItemCustomer>();
            string ListKey = "";
            switch (Category)
            {
                case 0:
                    zList = Customer_Data.Search_List(Department, Employee, Name, Phone, string.Empty, string.Empty, FromDate, ToDate);
                    break;

                case 1:
                    List<ItemConsent> zListWant = Customer_Data.SearchWant(Project, CategoryAsset, Inside, Bed);
                    foreach (ItemConsent item in zListWant)
                    {
                        ListKey += item.CustomerKey + ",";
                    }
                    if (ListKey != string.Empty)
                        ListKey = ListKey.Remove(ListKey.LastIndexOf(","));
                    zList = Customer_Data.Search_List(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ListKey, FromDate, ToDate);
                    break;

                case 2:
                    List<ItemTrade> zListTrade = Customer_Data.SearchTrade(Project, CategoryAsset, ISearch.TradeType, 2, FromDate, ToDate);//isower = 2 tim khách có giao dịch
                    foreach (ItemTrade item in zListTrade)
                    {
                        ListKey += item.CustomerKey + ",";
                    }
                    if (ListKey != string.Empty)
                        ListKey = ListKey.Remove(ListKey.LastIndexOf(","));
                    zList = Customer_Data.Search_List(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ListKey, FromDate, ToDate);
                    break;

                case 3:
                    List<ItemTrade> zListTrade2 = Customer_Data.SearchTrade(Project, CategoryAsset, ISearch.TradeType, 1, FromDate, ToDate);//isower = 2 tim khách có giao dịch và là chủ nhà
                    foreach (ItemTrade item in zListTrade2)
                    {
                        ListKey += item.CustomerKey + ",";
                    }
                    if (ListKey != string.Empty)
                        ListKey = ListKey.Remove(ListKey.LastIndexOf(","));
                    zList = Customer_Data.Search_List(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ListKey, FromDate, ToDate);
                    break;
            }

            StringBuilder zSb = new StringBuilder();
            int n = 1;
            foreach (ItemCustomer r in zList)
            {
                zSb.AppendLine("            <tr id='" + r.CustomerKey + "' type='" + r.CategoryKey + "'>");
                zSb.AppendLine("               <td>" + (n++).ToString() + "</td>");
                zSb.AppendLine("               <td>" + r.CustomerName + "</td>");
                zSb.AppendLine("               <td>" + r.Phone1 + "<br>" + r.Phone2 + "</td>");
                zSb.AppendLine("               <td>" + r.Email1 + "<br>" + r.Email2 + "</td>");
                zSb.AppendLine("               <td>" + r.ProjectName + "</td>");
                zSb.AppendLine("               <td>" + r.CategoryName + "</td>");
                zSb.AppendLine("               <td>" + r.Status + "</td>");
                zSb.AppendLine("               <td>" + r.ModifiedDate + "</td>");
                zSb.AppendLine("               <td class='notclick'><div class='action-buttons'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>");
                zSb.AppendLine("            </tr>");
                zSb.AppendLine("            <tr class='detail-row'>");
                zSb.AppendLine("                <td colspan='9'>");
                zSb.AppendLine("                    <div class='table-detail'><div class='row' customer='" + r.CustomerKey + "'></div></div>");
                zSb.AppendLine("                </td>");
                zSb.AppendLine("            </tr>");
            }

            ISearch.ListKey = ListKey;

            HttpContext.Current.Session.Add("SearchCRM", ISearch);
            HttpCookie zCook = new HttpCookie("Search");
            zCook.Values["CRM"] = HttpUtility.UrlEncode(JsonConvert.SerializeObject(ISearch).ToString());
            HttpContext.Current.Response.Cookies.Add(zCook);

            return zSb.ToString();
        }
        [WebMethod]
        public static string GetMoreDetail(int Customer)
        {
            #region [Quan tâm]
            string html = "";
            List<ItemConsent> zList2 = Consents_Data.List(Customer);
            if (zList2.Count > 0)
            {
                for (var j = 0; j < zList2.Count; j++)
                {
                    html += "<div class='col-xs-6'>";
                    html += "<div class='row'>";
                    html += "   <div class='col-xs-6 no-padding'>";
                    #region [danh sách quan tâm]
                    html += "       <div class='widget-box transparent'>";
                    html += "           <div class='widget-header'><h6 class='widget-title grey lighter'><i class='ace-icon fa fa-bell red'></i>Quan tâm</h6><div class='widget-toolbar no-border'>" + zList2[j].CreatedDate.ToDateString() + "</div></div>";
                    html += "           <div class='widget-body'>";
                    html += "              <div class='widget-main padding-0'>";
                    html += "                  <div class='profile-user-info profile-user-info-striped'>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Nhu cầu</div><div class='profile-info-value'>" + zList2[j].Want + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Dự án</div><div class='profile-info-value' id='loai'>" + zList2[j].Project + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Loại</div><div class='profile-info-value' id='loai'>" + zList2[j].Category + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Số phòng</div><div class='profile-info-value' id='sophong'>" + zList2[j].Room + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Nội thất</div><div class='profile-info-value' id='noithat'>" + zList2[j].Furniture + "</div></div>";
                    html += "                      <div class='profile-info-row'><div class='profile-info-name' style='width: 35% !important'>Giá</div><div class='profile-info-value giatien' id='giatien'>" + zList2[j].Price.ToDoubleString() + "</div></div>";
                    html += "                   </div>";
                    html += "               </div>";
                    html += "          </div>";
                    html += "    </div>";
                    #endregion
                    html += "   </div>";
                    html += "   <div class='col-xs-6 no-padding-right' id='divcare'>";
                    #region [Cham sóc]

                    html += "       <div class='widget-box transparent'>";
                    html += "           <div class='widget-header'><h6 class='widget-title grey lighter'><i class='ace-icon fa fa-comments green'></i>Nội dung trao đổi</h6></div>";
                    html += "           <div class='widget-body'>";
                    DataTable zList = Care_Data.List(Customer, zList2[j].RecordKey.ToInt());
                    foreach (DataRow r in zList.Rows)
                    {
                        html += "   <div class='widget-box transparent' style='border: 1px rgba(67, 142, 185, 0.5) solid !important;'>";
                        html += "       <div class='widget-header' style='min-height:auto; border-bottom:0px'><h6 class='widget-title grey lighter' style='line-height:0px'><i class='ace-icon fa fa-angle-right bigger-130 green'></i>" + r["CreatedBy"].ToString() + "</h6><div class='widget-toolbar no-border' style='line-height:17px'>" + r["CreatedDate"].ToDateString() + "</div></div>";
                        html += "       <div class='widget-body'><div class='widget-main no-padding-top no-padding-bottom'>" + r["ContentDetail"].ToString() + "</div></div>";
                        html += "   </div>";
                    }
                    html += "           </div>";
                    html += "           </div>";
                    html += "       </div>";
                    #endregion
                    html += "   </div>";
                    //---
                    html += "<div class='hr hr-double no-margin'></div>";
                    html += "</div>";
                }
            }

            #endregion
            return html;
        }
        #region [Roles]
        protected static string[] _Permitsion; //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        protected void InitView()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            string RolePage = "SAL03";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            List<ItemCustomer> zList = new List<ItemCustomer>();
            ItemCRMSearch ISearch = null;
            if (Request.Cookies["Search"] != null && 
                Request.Cookies["Search"]["CRM"] != null)
            {
                ISearch = JsonConvert.DeserializeObject<ItemCRMSearch>(HttpContext.Current.Request.Cookies["Search"]["CRM"].ToUrlDecode());
                switch (ISearch.Category.ToInt())
                {
                    case 0:
                        zList = Customer_Data.Search_List(ISearch.Department.ToInt(), Employee, ISearch.Name, ISearch.Phone, string.Empty, string.Empty, ISearch.FromDate, ISearch.ToDate);
                        break;

                    case 1:
                        zList = Customer_Data.Search_List(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ISearch.ListKey, ISearch.FromDate, ISearch.ToDate);
                        break;

                    case 2:
                        zList = Customer_Data.Search_List(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ISearch.ListKey, ISearch.FromDate, ISearch.ToDate);
                        break;

                    case 3:
                        zList = Customer_Data.Search_List(ISearch.Department.ToInt(), ISearch.Employee.ToInt(), ISearch.Name, ISearch.Phone, ISearch.Status, ISearch.ListKey, ISearch.FromDate, ISearch.ToDate);
                        break;
                }

                if (UnitLevel < 7)
                {
                    Tools.DropDown_DDL(DDL_Project, "SELECT DISTINCT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey IN(" + ViewDepartment + ") ORDER BY ProjectName", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE DepartmentKey IN (" + ViewDepartment + ") AND IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);

                    zList = Customer_Data.Search(ViewDepartment, 0, string.Empty, string.Empty, string.Empty, string.Empty, 100);
                }
                else
                {
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee, false);

                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;

                    zList = Customer_Data.Search(ViewDepartment, Employee, string.Empty, string.Empty, string.Empty, string.Empty, 100);
                }
            }
            else
            {
                if (UnitLevel < 7)
                {
                    Tools.DropDown_DDL(DDL_Project, "SELECT DISTINCT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey IN(" + ViewDepartment + ") ORDER BY ProjectName", false);
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE DepartmentKey IN (" + ViewDepartment + ") AND IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);

                    //zList = Customer_Data.Search(ViewDepartment, 0, string.Empty, string.Empty, string.Empty, string.Empty, 100);
                }
                else
                {
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee, false);

                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;

                    //zList = Customer_Data.Search(ViewDepartment, Employee, string.Empty, string.Empty, string.Empty, string.Empty, 100);
                }
            }
            ViewHTML(zList);
        }
        #endregion
    }
}