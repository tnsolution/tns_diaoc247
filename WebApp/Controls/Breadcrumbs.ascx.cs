﻿using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.Controls
{
    public partial class Breadcrumbs : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    string module = HttpContext.Current.Request.Url.AbsolutePath.Split('/')[1];
            //    string index = HttpContext.Current.Request.Url.AbsolutePath;
            //    DataTable zTable = WebMenu.Get();
            //    var sb = new StringBuilder();
            //    Literal_Breadcrumb.Text = GenerateBreadcrumb(zTable.Select(string.Format("[Module] = '{0}'", module)), index);
            //}
        }

        string GenerateBreadcrumb(DataRow[] menu, string index)
        {
            StringBuilder sb = new StringBuilder();
            if (menu.Length > 0)
            {
                foreach (DataRow dr in menu)
                {
                    string line = "<li class=''>";
                    string menuText = dr["MenuName"].ToString();
                    string handler = "<a href=" + dr["MenuLink"].ToString() + ">" + menuText + "</a>";

                    if (dr["MenuLink"].ToString() == index)
                    {
                        line = "<li class='active'>";
                        handler = menuText;
                    }

                    line += handler;
                    sb.Append(line);
                    sb.Append("</li>");
                }
            }

            return sb.ToString();
        }
    }
}