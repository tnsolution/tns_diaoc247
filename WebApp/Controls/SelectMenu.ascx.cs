﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using HelpHRM = Lib.HRM.Helper;

namespace WebApp.Controls
{
    public partial class SelectMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                string Spec = HttpContext.Current.Request.Cookies["UserLog"]["SpecKey"];
                string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
                StringBuilder zSb = new StringBuilder();
                DataTable zTable = new DataTable();

                //Phân cấp
                if (UnitLevel <= 3)
                {
                    //Phân sàn
                    zSb.AppendLine(@"<a id='san' data-toggle='dropdown' class='dropdown-toggle' href='#'> Xem sàn <i class='ace-icon fa fa-angle-down bigger-110'></i></a>");
                    zSb.AppendLine("        <ul class='dropdown-menu dropdown-light-blue dropdown-caret' id='OptionSan'>");
                    zTable = HelpHRM.GroupList();
                    zSb.AppendLine("            <li value=0 class='option san'>--Tất cả--</li>");
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("<li value=" + r["SpecKey"].ToString() + " class='option san'>" + r["SpecName"].ToString() + "</li>");
                    }
                    zSb.AppendLine("        </ul>");
                    Literal_Group.Text = zSb.ToString();
                    //Phân nhóm
                    zTable = HelpHRM.DepartmentList(Department);
                    zSb = new StringBuilder();
                    zSb.AppendLine(@"<a id='khuvuc' data-toggle='dropdown' class='dropdown-toggle' href='#'> Xem khu vực <i class='ace-icon fa fa-angle-down bigger-110'></i></a>");
                    zSb.AppendLine("        <ul class='dropdown-menu dropdown-light-blue dropdown-caret' id='OptionKhuvuc'>");                                     

                    zSb.AppendLine("            <li value='" + Department + "' class='option khuvuc'>--Tất cả--</li>");
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("        <li value=" + r["DepartmentKey"].ToString() + " class='option khuvuc'>" + r["DepartmentName"].ToString() + "</li>");
                    }
                    zSb.AppendLine("        </ul>");
                    Literal_Branch.Text = zSb.ToString();
                }
                else
                {
                    //Phân sàn
                    if (Spec == "1")
                    {
                        zSb.AppendLine(@"<a id='san' data-toggle='dropdown' class='dropdown-toggle' href='#'> Địa ốc 247 </a>");
                    }
                    if (Spec == "2")
                    {
                        zSb.AppendLine(@"<a id='san' data-toggle='dropdown' class='dropdown-toggle' href='#'> Phát phát </a>");
                    }
                    Literal_Group.Text = zSb.ToString();
                    //Phân nhóm
                    zTable = HelpHRM.DepartmentList(Department);
                    zSb = new StringBuilder();
                    if (zTable.Rows.Count > 1)
                    {
                        zSb.AppendLine("<a id='khuvuc' data-toggle='dropdown' class='dropdown-toggle' href='#'> Xem nhóm <i class='ace-icon fa fa-angle-down bigger-110'></i></a>");
                        zSb.AppendLine("    <ul class='dropdown-menu dropdown-light-blue dropdown-caret' id='OptionKhuvuc'>");
                        zSb.AppendLine("        <li value='" + Department + "' class='option khuvuc'>--Tất cả--</li>");
                        foreach (DataRow r in zTable.Rows)
                        {
                            zSb.AppendLine("    <li value=" + r["DepartmentKey"].ToString() + " class='option khuvuc'>" + r["DepartmentName"].ToString() + "</li>");
                        }
                        zSb.AppendLine("    </ul>");
                    }
                    else
                    {
                        DataRow r = zTable.Rows[0];
                        zSb.AppendLine("<a id='khuvuc' data-toggle='dropdown' class='dropdown-toggle' href='#'> " + new Departments_Info(r["DepartmentKey"].ToInt()).DepartmentName + " </a>");
                    }

                    Literal_Branch.Text = zSb.ToString();
                }
            }
        }
    }
}