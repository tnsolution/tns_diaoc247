﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarMenu.ascx.cs" Inherits="WebApp.Controls.SidebarMenu" %>
<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse ace-save-state sidebar-fixed noprint" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
    <script type="text/javascript">
        try { ace.settings.loadState('sidebar') } catch (e) { }
    </script>
    <div class="nav-wrap-up pos-rel">
        <div class="nav-wrap">
            <asp:Literal ID="Literal_Menu" runat="server"></asp:Literal>
        </div>
    </div>
</div>
