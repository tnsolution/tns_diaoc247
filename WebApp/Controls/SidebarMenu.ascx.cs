﻿using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
namespace WebApp.Controls
{
    public partial class SidebarMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //DataTable zTable = WebMenu.Get();
                //var sb = new StringBuilder();
                //DataRow[] menu = zTable.Select("[Parent] = 0");

                //Literal_Menu.Text = GenerateUL(menu, zTable, sb, 0);

                LoadMenu();
            }
        }

        //de quy
        protected string GenerateUL(DataRow[] menu, DataTable table, StringBuilder sb, int level)
        {
            if (level == 0)
                sb.AppendLine("<ul class='nav nav-list'>");
            else
                sb.AppendLine("<ul class='submenu'>");

            if (menu.Length > 0)
            {
                string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

                foreach (DataRow dr in menu)
                {
                    string MenuID = dr["MenuID"].ToString();
                    int IsAllow = WebMenu.IsAllow_Menu(UserKey, MenuID);
                    if (IsAllow == 1)
                    {
                        string menuText = dr["MenuName"].ToString();
                        string icon = dr["Icon"].ToString() != "" ? "<i class='menu-icon " + dr["Icon"].ToString() + "'></i>" : "";
                        string handler = dr["Link"].ToString() == "#"
                            ? "<a class='dropdown-toggle' href=" + dr["Link"].ToString() + ">" + icon + "<span class='menu-text'>" + menuText + "</span></a>"
                            : "<a href=" + dr["Link"].ToString() + ">" + icon + "<span class='menu-text'>" + menuText + "</a>";
                        string line = "<li class='hover'>";
                        line += handler;
                        sb.Append(line);

                        string pid = dr["MenuKey"].ToString();
                        string parentId = dr["Parent"].ToString();

                        DataRow[] subMenu = table.Select("[Parent] = " + pid);
                        if (subMenu.Length > 0 && !pid.Equals(parentId))
                        {
                            var subMenuBuilder = new StringBuilder();
                            sb.Append(GenerateUL(subMenu, table, subMenuBuilder, 1));
                        }
                        sb.Append("</li>");
                    }
                }
            }
            sb.Append("</ul>");
            return sb.ToString();
        }
        protected void LoadMenu()
        {
            StringBuilder zSb = new StringBuilder();
            StringBuilder zSb2;
            zSb.AppendLine("<ul class='nav nav-list'>");

            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            DataTable zMainMenu = WebMenu.Get(0);
            foreach (DataRow r0 in zMainMenu.Rows)
            {
                string MenuID = r0["MenuID"].ToString();
                int IsAllow = WebMenu.IsAllow_Menu(UserKey, MenuID);
                if (IsAllow == 1)
                {
                    zSb.Append("<li class='hover'>");
                    zSb.Append(MenuHandler(r0));

                    string pid = r0["MenuKey"].ToString();
                    string parentId = r0["Parent"].ToString();

                    DataTable zSubMenu = WebMenu.Get(pid.ToInt());
                    if (zSubMenu.Rows.Count > 0)
                    {
                        zSb2 = new StringBuilder();
                        zSb2.Append("<ul class='submenu'>");
                        foreach (DataRow r1 in zSubMenu.Rows)
                        {
                            zSb2.Append("<li class='hover'>");
                            zSb2.Append(MenuHandler(r1));
                            zSb2.Append("</li>");
                        }
                        zSb2.Append("</ul>");

                        zSb.Append(zSb2.ToString());
                    }
                    zSb.Append("</li>");
                }
            }

            zSb.Append("</ul>");

            Literal_Menu.Text = zSb.ToString();
        }
        protected string MenuHandler(DataRow r0)
        {
            string Type = r0["Type"].ToString();
            string Html = "";
            int Noti = Notification(Type);
            if (Noti != 0)
            {
                Html = Noti.ToString();
            }

            string menuText = r0["MenuName"].ToString();
            string icon = r0["Icon"].ToString() != "" ? "<i class='menu-icon " + r0["Icon"].ToString() + "'></i>" : "";
            string handler = r0["Link"].ToString() == "#"
                ? "<a class='dropdown-toggle' href=" + r0["Link"].ToString() + ">" + icon + "<span class='menu-text'>" + menuText + "</span><span class='badge badge-warning'>" + Html + "</span></a>"
                : "<a href=" + r0["Link"].ToString() + ">" + icon + "<span class='menu-text'>" + menuText + "</span><span class='badge badge-warning' style='top:7px; left:70%'>" + Html + "</span></a>";
            return handler;
        }
        protected int Notification(string Type)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Spec = HttpContext.Current.Request.Cookies["UserLog"]["SpecKey"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];

            int NumBirthday = 0;
            int NumTrade = 0;
            int NumAsset = 0;
            int NumTask = 0;
            int NumTransfer = 0;

            if (UnitLevel < 7)
            {
                NumBirthday = Notification_Data.Count_Notification(Department, 0, 1);
                NumTrade = Notification_Data.Count_Notification(Department, 0, 2);
                NumAsset = Notification_Data.Count_Notification(Department, 0, 3);
                NumTransfer = Notification_Data.Count_Transfer(Department, 0);
                NumTask = Notification_Data.Count_Task(Department, 0);
            }
            else
            {
                NumBirthday = Notification_Data.Count_Notification(Department, Employee, 1);
                NumTrade = Notification_Data.Count_Notification(Department, Employee, 2);
                NumAsset = Notification_Data.Count_Notification(Department, Employee, 3);
                NumTransfer = Notification_Data.Count_Transfer(Department, Employee);
                NumTask = Notification_Data.Count_Task(Department, Employee);
            }
            switch (Type)
            {
                case "01":
                    return (NumBirthday + NumTrade + NumAsset);
                case "11":
                    return NumBirthday;
                case "12":
                    return NumTrade;
                case "13":
                    return NumAsset;
                case "02":
                    return NumTask + NumTransfer;
                case "21":
                    return NumTask;
                case "22":
                    return NumTransfer;
                default:
                    return 0;
            }
        }
    }
}