﻿using Lib.SYS;
using System;
using System.Web;

namespace WebApp.Controls
{
    public partial class TopLeftMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int Spec = HttpContext.Current.Request.Cookies["UserLog"]["SpecKey"].ToInt();
                int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
                string ViewDepartment = "";
                if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                    ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
                else
                    ViewDepartment = Department;
                string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

                int NumCapital = 0;
                int NumBirthday = 0;
                int NumTrade = 0;
                int NumAsset = 0;
                int NumTask = 0;
                int NumReceipt = 0;
                int NumPayment = 0;
                int NumTransfer = 0;
                int NumTicketOff = Notification_Data.Count_TicketOff(Employee);
                int Salary = Notification_Data.Count_Salary(Employee);

                if (UnitLevel < 7)
                {
                    NumBirthday = Notification_Data.Count_Notification(ViewDepartment, 0, 1);
                    NumTrade = Notification_Data.Count_Notification(ViewDepartment, 0, 2);
                    NumAsset = Notification_Data.Count_Notification(ViewDepartment, 0, 3);
                    NumTransfer = Notification_Data.Count_Transfer(ViewDepartment, 0);
                    NumTask = Notification_Data.Count_Task(ViewDepartment, 0);
                    NumTransfer = Notification_Data.Count_Transfer(ViewDepartment, 0);
                    NumReceipt = Notification_Data.Count_Receipt(ViewDepartment, 1);
                    NumPayment = Notification_Data.Count_Payment(ViewDepartment, 1);
                }
                else
                {
                    NumBirthday = Notification_Data.Count_Notification(ViewDepartment, Employee, 1);
                    NumTrade = Notification_Data.Count_Notification(ViewDepartment, Employee, 2);
                    NumAsset = Notification_Data.Count_Notification(ViewDepartment, Employee, 3);
                    NumTransfer = Notification_Data.Count_Transfer(ViewDepartment, Employee);
                    NumTransfer = Notification_Data.Count_Transfer(ViewDepartment, Employee);
                    NumTask = Notification_Data.Count_Task(ViewDepartment, Employee);
                    NumReceipt = Notification_Data.Count_Receipt(ViewDepartment, 1);
                    NumPayment = Notification_Data.Count_Payment(ViewDepartment, 1);
                }

                Lit_TicketOff.Text = NumTicketOff.ToString();
                Lit_Transfer.Text = NumTransfer.ToString();
                Lit_Birthday.Text = NumBirthday.ToString();
                Lit_TradeExpired.Text = NumTrade.ToString();
                Lit_AssetExpired.Text = NumAsset.ToString();
                Lit_Task.Text = NumTask.ToString();
                Lit_TotalRemind.Text = (NumBirthday + NumTrade + NumAsset).ToString();
                Lit_TotalTask.Text = (NumTask + NumTransfer).ToString();
                Lit_FeePlay.Text = Notification_Data.Count_FeePlay(Employee).ToString();

                MenuFNC(NumPayment.ToString(), NumReceipt.ToString(), NumCapital.ToString(), (NumPayment + NumCapital + NumReceipt).ToString(), Salary.ToString(), UserKey);
            }
        }
        protected void MenuFNC(string PayCount, string ReceiptCount, string CapitalCount, string TotalCount, string Salary, string UserKey)
        {
            string[] result;
            string RolePage = "";

            RolePage = "ACC";
            result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            LitMenuFNC.Text = Html_MenuFinancal(result, TotalCount, CapitalCount, ReceiptCount, PayCount);
            RolePage = "ACC01";
            result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            LitMenuSalary.Text = Html_MenuSalary(result, Salary);
        }
        protected string Html_MenuSalary(string[] Allow, string Salary)
        {
            string Bag = "";
            if (Salary != "0")
                Bag = "<span class='badge badge-warning bigger-110'>" + Salary + "</span>";

            if (Allow[3].ToInt() == 1)
                return @"
    <li>
        <a href='#' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-envelope bigger-110'></i>&nbsp;Lương&nbsp;" + Bag + @"
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>
            <li>
                <a href='../HRM/SalaryList2.aspx'>
                    <i class='ace-icon fa fa-info bigger-110 blue'></i>
                    Bảng lương              
                </a>
            </li>
            <li>
                <a href='../HRM/SalaryEdit.aspx'>
                    <i class='ace-icon fa fa-info bigger-110 blue'></i>
                    Làm lương                 
                </a>
            </li>
        </ul>
    </li>";
            else
                return "<li><a href='../HRM/SalaryList1.aspx'><i class='ace-icon fa fa-envelope bigger-110'></i>&nbsp;Lương&nbsp;" + Bag + "</a></li>";
        }
        protected string Html_MenuFinancal(string[] Allow, string TotalCount, string CapitalCount, string ReceiptCount, string PayCount)
        {
            if (Allow[0].ToInt() == 0)
                return string.Empty;
            if (Allow[3].ToInt() == 1)
            {
                return @"
    <li id='menuFNC'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-usd bigger-110'></i>&nbsp;Thu - Chi
            <span class='badge badge-warning icon-animated-vertical'>" + TotalCount + @"</span>&nbsp;<i class='ace-icon fa fa-angle-down bigger-110'></i>
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>
            <li><a href='../FNC/Capital.aspx'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Quỹ công ty <span class='badge badge-warning'>" + CapitalCount + @"</span></a></li>         
            <li><a href='../FNC/Receipt.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Thu <span class='badge badge-warning'>" + ReceiptCount + @"</span></a></li>
            <li><a href='../FNC/Payment.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Chi <span class='badge badge-warning'>" + PayCount + @"</span></a></li>
            <li id='subFNC'><a href='../FNC/FeeCheck.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Bảng cân đối</a></li>
        </ul>
    </li>";
            }
            else
            {
                return @"
    <li id='menuFNC'>
        <a href='#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'><i class='ace-icon fa fa-usd bigger-110'></i>&nbsp;Thu - Chi
            <span class='badge badge-warning icon-animated-vertical'>" + TotalCount + @"</span>&nbsp;<i class='ace-icon fa fa-angle-down bigger-110'></i>
        </a>
        <ul class='dropdown-menu dropdown-light-blue dropdown-caret'>          
            <li><a href='../FNC/Receipt.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Thu <span class='badge badge-warning'>" + ReceiptCount + @"</span></a></li>
            <li><a href='../FNC/Payment.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Chi <span class='badge badge-warning'>" + PayCount + @"</span></a></li>
            <li id='subFNC'><a href='../FNC/FeeCheck.aspx?Category=1'><i class='ace-icon fa fa-usd bigger-110 blue'></i>Bảng cân đối</a></li>
        </ul>
    </li>";
            }
        }
    }
}