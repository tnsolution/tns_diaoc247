﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ChiTietSanPhamKyGui.aspx.cs" Inherits="WebApp.DEMO.ChiTietSanPhamKyGui" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết sản phẩm ký gửi
            </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="tabbale table-detail" id="Tabs">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#info">
                                    <i class="pink ace-icon fa fa-briefcase bigger-110"></i>
                                    Thông tin sản phẩm
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#info1">
                                    <i class="blue ace-icon fa-book bigger-110"></i>
                                    Thông tin quản lý
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="info" class="tab-pane in active">
                                <div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" custype="0">Mã căn</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="GG-03.07" required />
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txt_Id1" runat="server" value="Tháp" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txt_Id2" runat="server" value="Tầng" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txt_Id3" runat="server" value="Căn" CssClass="form-control" required></asp:TextBox>
                                            </div>
                                            <label class="col-sm-2 control-label">Nhu cầu</label>
                                            <div class="col-sm-4 no-padding">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="ace" value="CN" name="chkPurpose" id="chkCN" />
                                                        <span class="lbl">Chuyển nhượng</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox" style="display: none">
                                                    <label>
                                                        <input type="checkbox" class="ace" value="KT" name="chkPurpose" />
                                                        <span class="lbl">Khai thác kinh doanh</span>
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="ace" value="CT" name="chkPurpose" id="chkCT" />
                                                        <span class="lbl">Cho thuê</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Loại sản phẩm</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DDL_Category" runat="server" CssClass="select2" required AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-2 control-label">Giá bán</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="22,500,000,000" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Số phòng ngủ</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="0" required />
                                            </div>
                                            <label class="col-sm-2 control-label">Giá thuê</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="12,500,500" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label ">Diện tích tìm đường</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="33,32 m2" required />
                                            </div>
                                            <label class="col-sm-2 control-label">Giá mua</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="0" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Diện tích thông thủy</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="33,32 m2" required />
                                            </div>
                                            <label class="col-sm-2 control-label">Giá mua</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="35.00 m2" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Hướng view</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DDL_DirectionView" runat="server" CssClass="select2" required="">
                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="Đông" Text="Đông"></asp:ListItem>
                                                    <asp:ListItem Value="Tây" Text="Tây"></asp:ListItem>
                                                    <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                                                    <asp:ListItem Value="Bắc" Text="Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Đông Nam" Text="Đông Nam"></asp:ListItem>
                                                    <asp:ListItem Value="Tây Bắc" Text="Tây Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Đông Bắc" Text="Đông Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Tây Nam" Text="Tây Nam"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-2 control-label">Ngày liên hệ</label>
                                            <div class="col-sm-4">
                                                <input type="text" runat="server" class="form-control" value="14/09/2019" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Hướng cửa</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="select2" required="">
                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="Đông" Text="Đông"></asp:ListItem>
                                                    <asp:ListItem Value="Tây" Text="Tây"></asp:ListItem>
                                                    <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                                                    <asp:ListItem Value="Bắc" Text="Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Đông Nam" Text="Đông Nam"></asp:ListItem>
                                                    <asp:ListItem Value="Tây Bắc" Text="Tây Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Đông Bắc" Text="Đông Bắc"></asp:ListItem>
                                                    <asp:ListItem Value="Tây Nam" Text="Tây Nam"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-sm-2 control-label">Tình trạng</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DDL_Status" CssClass="select2" runat="server" AppendDataBoundItems="true" required="">
                                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Pháp lý</label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="select2" required="">
                                                    <asp:ListItem Value="0" Text="Hợp đồng mua bán" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="info1" class="tab-pane">
                                <div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" custype="0">Người khởi tạo</label>
                                            <div class="col-sm-9">
                                                <input type="text" runat="server" class="form-control" value="Nguyễn Hoàng Phương" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" custype="0">Ngày khởi tạo</label>
                                            <div class="col-sm-9">
                                                <input type="text" runat="server" class="form-control" value="15/09/2019" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" custype="0">Người cập nhật</label>
                                            <div class="col-sm-9">
                                                <input type="text" runat="server" class="form-control" value="Nguyễn An Đông" required />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" custype="0">Ngày cập nhật</label>
                                            <div class="col-sm-9">
                                                <input type="text" runat="server" class="form-control" value="16/09/2019" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <ul class="text-warning list-unstyled spaced">
                                                <li><i class="ace-icon fa fa-exclamation-triangle"></i>Cho phép nhân viên chia sẽ được cập nhật sản phẩm này</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                        <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-8">
                                                    <button type='button' class='btn btn-white btn-default btn-round btn-sm' id='btnSaveShare'>
                                                        <i class="ace-icon fa fa-plus"></i>Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-6"></div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class='profile-user-info profile-user-info-striped'>
                                                <div class='profile-info-row'>
                                                    <div class='profile-info-name'>Chia sản phẩm</div>
                                                    <div class='profile-info-value'>
                                                        Nguyễn Hoàng Phương
                                                        <div class='hidden-sm hidden-xs action-buttons pull-right'><a class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</a></div>
                                                    </div>
                                                </div>
                                                <div class='profile-info-row'>
                                                    <div class='profile-info-name'>Chia sản phẩm</div>
                                                    <div class='profile-info-value'>
                                                        Nguyễn An Đông
                                                        <div class='hidden-sm hidden-xs action-buttons pull-right'><a class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="tab-content">
                        <div style="font-weight: bold">Thông tin khách hàng</div>
                        <span class="pull-right"><a href="#mInitCustomer" class="" data-toggle="modal">
                            <i class="ace-icon fa fa-plus"></i>
                            Thêm
                        </a></span>
                        <table class='table table-hover table-bordered'>
                            <thead>
                                <tr>
                                    <th>Tên Khách Hàng</th>
                                    <th>SĐT</th>
                                    <th>Email</th>
                                    <th style="white-space: nowrap; width: 1%"></th>
                                </tr>
                            </thead>
                            <tr>
                                <td>Nguyễn Duy Tâm</td>
                                <td>0912992381</td>
                                <td>tam.nd@hht.vn</td>
                                <td style="white-space: nowrap;">
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-angle-double-down'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>

                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>

                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <%--                        <tr>
                            <td colspan="10" style="background-color: #62a8d1">
                                <table class='table table-hover table-bordered'>
                                    <thead>
                                        <tr>
                                            <th>Địa chỉ</th>
                                            <th>Ngày cập nhật</th>
                                            <th style="white-space: nowrap; width: 1%"></th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>L37 (số 24) Đường 11, KDC Him Lam, Kênh Tẻ, P. Tân Hưng, Q.7/ 74/5 Thạch Thị Thanh, P.Tân Định, Q.1</td>
                                        <td>16/09/2019</td>
                                        <td style="white-space: nowrap;">
                                            <div class='action-buttons'>
                                                <a href='#' class='green bigger-140 show-details-btn'>
                                                    <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                    <span class='sr-only'>Details</span>
                                                </a>
                                                <a href='#' class='green bigger-140 show-details-btn'>
                                                    <i class="ace-icon fa fa-trash-o orange2"></i>
                                                    <span class='sr-only'>Details</span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>--%>
                        </table>
                    </div>
                    <div class="space-6"></div>
                    <div class="tab-content">
                        <div style="font-weight: bold">Thông tin chăm sóc</div>
                        <span class="pull-right"><a href="#mInitCustomer" class="" data-toggle="modal">
                            <i class="ace-icon fa fa-plus"></i>
                            Thêm
                        </a></span>
                        <table class='table table-hover table-bordered'>
                            <thead>
                                <tr>
                                    <th>Ngày</th>
                                    <th>Nhân viên chăm sóc</th>
                                    <th>Nội dung chăm sóc</th>
                                    <th style="white-space: nowrap; width: 1%"></th>
                                </tr>
                            </thead>
                            <tr>
                                <td>16/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>Hỏi thông tin khách hàng</td>
                                <td style="white-space: nowrap;">
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>16/09/2019</td>
                                <td>Đỗ Nguyễn Minh Châu</td>
                                <td>Thăm hỏi khách hàng</td>
                                <td style="white-space: nowrap;">
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>


                    </div>
                    <div class="space-6"></div>
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="text-warning list-unstyled spaced">
                                    <li><i class="ace-icon fa fa-exclamation-triangle"></i>Hình ảnh sản phẩm (bạn có thể chọn nhiều tập tin)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
                            </div>
                            <div class="col-xs-7">
                                <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
                                    <i class="ace-icon fa fa-plus"></i>
                                    Upload
                                </a>
                            </div>
                        </div>
                        <div class="space-6"></div>
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="ace-thumbnails clearfix">
                                    <a href="#mProduct" id="createNew" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i>&nbsp;Hình 1</a>
                                    <a href="#mProduct" id="createNew" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i>&nbsp;Hình 2</a>
                                    <a href="#mProduct" id="createNew" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i>&nbsp;Hình 3</a>
                                    <a href="#mProduct" id="createNew" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i>&nbsp;Hình 4</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="space-6"></div>
                    <div class="tab-content">
                        <ul class="text-warning list-unstyled spaced">
                            <li><i class="ace-icon fa fa-exclamation-triangle"></i>Dùng để đăng tin sản phẩm                                           
                            </li>
                        </ul>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Post</label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="DDL_WebPublished" CssClass="form-control select2" runat="server">
                                        <asp:ListItem Value="0" Text="Không đăng tin"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Đăng tin"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tiêu đề tin</label>
                                <div class="col-sm-10">
                                    <input type="text" runat="server" class="form-control" id="txt_WebTitle" placeholder="Nội dung tóm tắt" />
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea runat="server" class="form-control" id="txt_WebContent"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-6"></div>
        <div class="row">
            <div class="tab-content">
                <div style="font-weight: bold">Khách Hàng Gợi Ý</div>
                <table class='table table-hover table-bordered'>
                    <thead>
                        <tr>
                            <th>
                                <select id="khachhang" class="form-control">
                                    <option>Tên Khách Hàng</option>
                                </select></th>
                            <th>
                                <select id="sodt" class="form-control">
                                    <option>Số ĐT</option>
                                </select></th>
                            <th>
                                <select id="duan" class="form-control">
                                    <option>Dự án</option>
                                </select></th>
                            <th>
                                <select id="sanpham" class="form-control">
                                    <option>Số ĐT</option>
                                </select></th>
                            <th>
                                <select id="trangthai" class="form-control">
                                    <option>Trạng Thái</option>
                                </select></th>
                            <th>Nội dung chăm sóc</th>
                            <th>Ghi Chú</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>Nguyễn Thành Công</td>
                        <td>0330912399</td>
                        <td>The SunHouse</td>
                        <td>MG.01.09</td>
                        <td>Rất tiềm năng</td>
                        <td>đầy đủ tiện nghi</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="space-6"></div>
        <div class="row">
            <div class="tab-content">
                <div class="row">
                    <div class="col-md-2">
                        <div style="font-weight: bold">Danh sách sản phẩm</div>
                    </div>
                    <div class="col-md-1">
                        <label>Thành Phố</label>
                    </div>
                    <div class="col-md-1">
                        <label style="font-weight: bold">Hồ Chí Minh</label>
                    </div>
                    <div class="col-md-1">
                        <label>Quận</label>
                    </div>
                    <div class="col-md-1">
                        <label style="font-weight: bold">7</label>
                    </div>
                </div>
            </div>

            <table class='table table-hover table-bordered'>
                <thead>
                    <tr>
                        <th>
                            <select id="duan1" class="form-control">
                                <option>Dự án</option>
                            </select></th>
                        <th>
                            <select id="masp" class="form-control">
                                <option>Mã sản phẩm</option>
                            </select></th>
                        <th>
                            <select id="loasp" class="form-control">
                                <option>Loại Sản Phẩm</option>
                            </select></th>
                        <th>Mục đích/Giá</th>
                        <th>
                            <select id="sopn" class="form-control">
                                <option>Số PN</option>
                            </select></th>
                        <th>Diện tích</th>
                        <th>
                            <select id="tinhtrang" class="form-control">
                                <option>Tình trạng</option>
                            </select></th>
                        <th>
                            <select id="noithat" class="form-control">
                                <option>Nội thất</option>
                            </select></th>
                        <th>Lịch sử chăm sóc</th>
                    </tr>
                </thead>
                <tr>
                    <td>Botanica Premier</td>
                    <td>BP 01.09</td>
                    <td>ShopHouse</td>
                    <td>
                        <div class='row'>
                            <div class='col-xs-6'>Chuyển nhượng</div>
                            <div class='col-xs-6 giatien'>22,500,000,000</div>
                        </div>
                        <div class='row'>
                            <div class='col-xs-6'>Cho thuê</div>
                            <div class='col-xs-6 giatien'>10,000,000</div>
                        </div>
                    </td>
                    <td>2</td>
                    <td>150m2</td>
                    <td>Chưa giao dịch</td>
                    <td>Cơ bản</td>
                    <td>Không có</td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
