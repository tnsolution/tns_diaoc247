﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Cusomer_Person_Edit.aspx.cs" Inherits="WebApp.DEMO.Cusomer_Person_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chỉnh sửa thông tin khách hàng cá nhân
                <span class="tools pull-right">
                    <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o danger"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSaveCustomer">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <a class="btn btn-white btn-default btn-bold" href="CustomerList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-5">
                    <div class="tabbale table-detail" id="Tabs">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#info">
                                    <i class="pink ace-icon fa fa-tachometer bigger-110"></i>
                                    Thông tin cơ bản
                                </a>
                            </li>

                            <li>
                                <a data-toggle="tab" href="#info2">
                                    <i class="blue ace-icon fa fa-phone bigger-110"></i>
                                    Chi tiết liên lạc
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#share">
                                    <i class="blue ace-icon fa fa-info bigger-110"></i>
                                    Chia sẻ
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#deal">
                                    <i class="blue ace-icon fa fa-money bigger-110"></i>
                                    Giao dịch
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div id="info" class="tab-pane in active">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" custype="0">Họ tên</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="Nguyễn Thanh Long" runat="server" class="form-control" id="txt_CustomerName" placeholder="" required />
                                        </div>
                                        <label class="col-sm-2 control-label" custype="0">Mã KH</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="KH-00123" runat="server" class="form-control" id="Text1" placeholder="" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ngày sinh</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right" value="13/11/1990"
                                                    id="Text2" placeholder="Chọn ngày tháng năm" />
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Mã số thuế</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="0105102345" runat="server" class="form-control" id="txt_Phone2" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Giới tính</label>
                                        <div class="col-sm-1">
                                            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="select2" AppendDataBoundItems="true" required>
                                                <asp:ListItem Value="0" Text="Nam" Selected="True" disabled></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Nữ"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ghi chú</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 30px;" placeholder="...."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nguồn khách</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Source" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" required>
                                                <asp:ListItem Value="0" Text="Call" Selected="True" disabled></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-sm-2 control-label">Trạng Thái</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Status" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" required>
                                                <asp:ListItem Value="0" Text="Rất tiềm năng" Selected="True" disabled></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nguồn</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 30px;" placeholder="...."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="info2" class="tab-pane">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Thông tin liên lạc
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">CMND</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="301544666" class="form-control" id="txt_Email2" placeholder="" />
                                        </div>
                                        <label class="col-sm-2 control-label">SĐT</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="0396133145" class="form-control" id="txt_Email1" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ngày cấp</label>
                                        <div class="col-sm-4">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" runat="server" role="datepicker" class="form-control pull-right" value="15/01/2010"
                                                    id="Text3" placeholder="Chọn ngày tháng năm" />
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="long.nt@gmail.com" class="form-control" id="txt_Email1" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nơi cấp</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="CA Long An" class="form-control" id="txt_Email1" placeholder="" />
                                        </div>
                                        <label class="col-sm-2 control-label">Nguyên quán</label>
                                        <div class="col-sm-4">
                                            <input type="text" value="Bến Lức-Long An" class="form-control" id="txt_Email1" placeholder="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="text-warning list-unstyled spaced">
                                            <li><i class="ace-icon fa fa-exclamation-triangle"></i>Thông tin địa chỉ
                                                <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mAddress" data-toggle="modal">
                                                    <i class="ace-icon fa fa-plus"></i>
                                                    Add
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Quốc gia</th>
                                                    <th>Tỉnh/Thành phố</th>
                                                    <th>Quận/Huyện</th>
                                                    <th>Phường/Xã</th>
                                                    <th>Địa chỉ</th>
                                                    <th>Cư trú</th>
                                                    <th>Thao tác</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Việt Nam</td>
                                                    <td>Long An</td>
                                                    <td>Bến Lức</td>
                                                    <td>Bình Đức</td>
                                                    <td>64A, Ấp 3</td>
                                                    <td>Thường trú</td>
                                                    <td>
                                                        <div class="hidden-sm hidden-xs action-buttons pull-right">
                                                            <a href='#' class='green bigger-140 show-details-btn'>
                                                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                                <span class='sr-only'>Details</span>
                                                            </a>
                                                            <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                                        </div>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Việt Nam</td>
                                                    <td>Thành phố HCM</td>
                                                    <td>7</td>
                                                    <td>Tân Thuận Tây</td>
                                                    <td>96/14 đường Tân Mỹ, khu phố 4 </td>
                                                    <td>Tạm trú</td>
                                                    <td>
                                                        <div class="hidden-sm hidden-xs action-buttons pull-right">
                                                            <a href='#' class='green bigger-140 show-details-btn'>
                                                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                                <span class='sr-only'>Details</span>
                                                            </a>
                                                            <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Thông tin quản lý
                                        </li>
                                    </ul>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td>Người tạo</td>
                                                    <td>Nguyễn An Đông</td>
                                                </tr>
                                                <tr>
                                                    <td>Ngày tạo</td>
                                                    <td>09/09/2019</td>
                                                </tr>
                                                <tr>
                                                    <td>Người cập nhật</td>
                                                    <td>Lê Hải Lý</td>
                                                </tr>
                                                <tr>
                                                    <td>Ngày cập nhật</td>
                                                    <td>12/09/2019</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="share" class="tab-pane">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="text-warning list-unstyled spaced">
                                            <li><i class="ace-icon fa fa-exclamation-triangle"></i>Cho phép nhân viên nhận được thông tin này
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Chia sẽ cho</label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDownList11" runat="server" CssClass="form-control select2">
                                                        <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Nguyễn Minh Long"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Lê Hải Lý"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-1">
                                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mShare" data-toggle="modal">
                                                        <i class="ace-icon fa fa-plus"></i>
                                                        Add
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hr hr2 hr-double"></div>
                                <div class="space-6"></div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Nhân viên</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Lê Hải Lý</td>
                                                    <td>
                                                        <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Nguyễn Văn Thanh</td>
                                                    <td>
                                                        <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Nguyễn Thi Huyền</td>
                                                    <td>
                                                        <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div id="deal" class="tab-pane">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Loại giao dịch</th>
                                                    <th>Mã sản phẩm</th>
                                                    <th>Loại khách là</th>
                                                    <th>Dự án</th>
                                                    <th>Doanh thu</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Chuyển nhượng</td>
                                                    <td>OG-0727</td>
                                                    <td>Chủ nhà</td>
                                                    <td>Orchard Garden</td>
                                                    <td>20,000,000</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Chuyển nhượng</td>
                                                    <td>OG-0728</td>
                                                    <td>Chủ nhà</td>
                                                    <td>Orchard Garden</td>
                                                    <td>30,000,000</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Chuyển nhượng</td>
                                                    <td>OG-0729</td>
                                                    <td>Chủ nhà</td>
                                                    <td>Orchard Garden</td>
                                                    <td>40,000,000</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="text-warning list-unstyled spaced">
                                <li><i class="ace-icon fa fa-exclamation-triangle"></i>Thông tin quan tâm của khách hàng
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mRecord" data-toggle="modal" data-target="#mRecord">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Add
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="hr hr2 hr-double"></div>
                    <div class="space-6"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered">
                                <tr>
                                    <th>
                                        <select id="duan1" class="form-control">
                                            <option>Nhu cầu</option>
                                        </select>
                                    </th>
                                    <th>
                                        <select id="duan1" class="form-control">
                                            <option>Dự án</option>
                                        </select>
                                    </th>
                                    <th>
                                        <select id="duan1" class="form-control">
                                            <option>Loại sản phẩm</option>
                                        </select>
                                    </th>
                                    <th>Giá</th>
                                    <th>Diện tích</th>
                                    <th>
                                        <select id="duan1" class="form-control">
                                            <option>Số phòng</option>
                                        </select>
                                    </th>
                                    <th>
                                        <select id="duan1" class="form-control">
                                            <option>Nội thất</option>
                                        </select>
                                    </th>
                                    <th>Quan tâm khác</th>
                                    <th>
                                        <select id="duan1" class="form-control">
                                            <option>Kết quả xử lý</option>
                                        </select>
                                    </th>
                                    <th>Thao tác</th>
                                </tr>
                                <tr>
                                    <td>Mua cho thuê</td>
                                    <td>The Sun Avennue</td>
                                    <td>Shophouse</td>
                                    <td>100,000</td>
                                    <td>100m2</td>
                                    <td>2 phòng ngủ</td>
                                    <td>Cơ bản</td>
                                    <td>Dễ nhìn</td>
                                    <td>Đang xử lý</td>
                                    <td style="white-space: nowrap;">
                                        <div class="hidden-sm hidden-xs action-buttons pull-right">
                                            <a href='#' class='green bigger-140 show-details-btn'>
                                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                <span class='sr-only'>Details</span>
                                            </a>
                                            <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                        </div>
                                    </td>
                                    <tr>
                                        <td>Mua cho thuê</td>
                                        <td>The Sun Avennue</td>
                                        <td>Shophouse</td>
                                        <td>100,000</td>
                                        <td>100m2</td>
                                        <td>2 phòng ngủ</td>
                                        <td>Cơ bản</td>
                                        <td>Dễ nhìn</td>
                                        <td>Đang xử lý</td>
                                        <td style="white-space: nowrap;">
                                            <div class="hidden-sm hidden-xs action-buttons pull-right">
                                                <a href='#' class='green bigger-140 show-details-btn'>
                                                    <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                    <span class='sr-only'>Details</span>
                                                </a>
                                                <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mua cho thuê</td>
                                        <td>The Sun Avennue</td>
                                        <td>Shophouse</td>
                                        <td>100,000</td>
                                        <td>100m2</td>
                                        <td>2 phòng ngủ</td>
                                        <td>Cơ bản</td>
                                        <td>Dễ nhìn</td>
                                        <td>Đang xử lý</td>
                                        <td style="white-space: nowrap;">
                                            <div class="hidden-sm hidden-xs action-buttons pull-right">
                                                <a href='#' class='green bigger-140 show-details-btn'>
                                                    <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                    <span class='sr-only'>Details</span>
                                                </a>
                                                <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                            </div>
                                        </td>
                                    </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="text-warning list-unstyled spaced">
                                <li><i class="ace-icon fa fa-exclamation-triangle"></i>Nội dung làm việc, trao đổi với khách hàng
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mCare" data-toggle="modal">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Add
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="space-6"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Ngày</th>
                                    <th>Nhân viên chăm sóc</th>
                                    <th>Kênh chăm sóc</th>
                                    <th>Nội dung chăm sóc</th>
                                    <th>...</th>
                                </tr>
                                <tr>
                                    <td>09/09/2019</td>
                                    <td>Lê Hải Lý</td>
                                    <td>Mail</td>
                                    <td>Nội dung 1</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs action-buttons pull-right">
                                            <a href='#' class='green bigger-140 show-details-btn'>
                                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                <span class='sr-only'>Details</span>
                                            </a>
                                            <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td>10/09/2019</td>
                                    <td>Lê Hải Lý</td>
                                    <td>Mail</td>
                                    <td>Nội dung 2</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs action-buttons pull-right">
                                            <a href='#' class='green bigger-140 show-details-btn'>
                                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                <span class='sr-only'>Details</span>
                                            </a>
                                            <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11/09/2019</td>
                                    <td>Lê Hải Lý</td>
                                    <td>Mail</td>
                                    <td>Nội dung 3</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs action-buttons pull-right">
                                            <a href='#' class='green bigger-140 show-details-btn'>
                                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                                <span class='sr-only'>Details</span>
                                            </a>
                                            <a href="#" btn="btnDelWant" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mRecord" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin quan tâm</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nhu cầu</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Mua ở"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Cho thuê"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đảo kim cương"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="M-One"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án khác</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại sản phẩm</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Biệt thự"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Căn hộ"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Giá (VNĐ)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Price" placeholder="Nhập số" moneyinput value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Diện tích (m<sup>2</sup>)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Area" placeholder="Nhập số" areainput value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số PN</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Bed" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1PN"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2PN"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3PN"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4PN"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5PN"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nội thất</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Cơ bản"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Đầy đủ"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Quan tâm khác
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 60px;" placeholder="...."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kết quả xử lý</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList12" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Đang xử lý"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đã xử lý"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveRecord" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>Cập nhật                   
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mCare" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Nội dung chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kênh</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Status2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Zalo" Disable="disabled"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="FaceBook" Disable="disabled"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quan tâm</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList13" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Quan tâm 1"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Quan tâm 2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nội dung</label>
                            <div class="col-sm-9">
                                <textarea id="txt_Care" rows="5" cols="20" class="form-control"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveCare" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mAddress" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin địa chỉ</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quốc gia</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Việt Nam"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tỉnh/Thành phố</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList7" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Thành phố Hồ Chí Minh"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Long An"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quận/Huyện</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList8" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="7"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="8"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phường/Xã</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList9" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Tân Thuận Tây"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Tân Hưng"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Địa chỉ</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 60px;" placeholder="...."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Hình thức
                            </label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList10" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Thường trú"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Tạm trú"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveRecord" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>Cập nhật                   
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mBank" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin ngân hàng</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mã ngân hàng</label>
                            <div class="col-sm-9">
                                <input type="text" value="Ari-001" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên ngân hàng</label>
                            <div class="col-sm-9">
                                <input type="text" value="Aribank Tân Thuận Tây" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số tài khoản</label>
                            <div class="col-sm-9">
                                <input type="text" value="012345678" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Địa chỉ</label>
                            <div class="col-sm-9">
                                <input type="text" value="78/18,Tân Kiểng,Tân Thuận Tây" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ghi chú</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 60px;" placeholder="...."></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveRecord" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>Cập nhật                   
                    </button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
