﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Customer_Option.aspx.cs" Inherits="WebApp.DEMO.Customer_Option" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
      <div class="page-content">
        <div class="page-header">
            <h1>Tùy chỉnh cột hiển thị dữ liệu khách hàng              
            </h1>
        </div>
        <div class="row" style="padding-bottom: 10px">
            <div class="col-md-2">
               <h5>Đối tượng khách hàng</h5> 
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="DDL_Customer" runat="server" class="form-control select2" AppendDataBoundItems="true">
                    <asp:ListItem Value="0" Text="--Chọn loại khách--" Selected="True" Disable="disabled"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Quan tâm" Disable="disabled"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Chủ nhà đã giao dịch" Disable="disabled"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Giao dịch" Disable="disabled"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <table class="table table-bordered">
                    <tr>
                        <th>Tên cột</th>
                        <th>Hiển thị</th>
                        <th>Thứ tự</th>
                    </tr>
                    <tr>
                        <td>Stt</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>Họ tên</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>SĐT</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>Dự án</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Sản phẩm</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Trạng thái</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>6</td>
                    </tr>
                    <tr>
                        <td>Nội dung chăm sóc</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>7</td>
                    </tr>
                    <tr>
                        <td>Ghi chú</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>8</td>
                    </tr>
                    <tr>
                        <td>Ngày cập nhật</td>
                        <td>
                            <div class="checkbox" style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>9</td>
                    </tr>
                    <tr>
                        <td>Nhân viên cập nhật</td>
                        <td>
                            <div class="checkbox"style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Phòng</td>
                        <td>
                            <div class="checkbox"style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>Địa chỉ</td>
                        <td>
                            <div class="checkbox"style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>12</td>
                    </tr>
                    <tr>
                        <td>Nguồn</td>
                        <td>
                            <div class="checkbox"style="margin-top:0;margin-bottom:0">
                                <label>
                                    <input name="read" type="checkbox" class="ace"><span class="lbl"></span><i class="fa fa-star"></i>
                                </label>
                            </div>
                        </td>
                        <td>13</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="row" style="padding-bottom: 10px">
            <div class="col-md-4">
                <button type="button" class="btn btn-info" id="btnSaveCare" data-dismiss="modal" style="float:right">
                    <i class="ace-icon fa fa-plus"></i>
                    Cập nhật
                </button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
