﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="DanhSachKhachHang.aspx.cs" Inherits="WebApp.DEMO.DanhSachKhachHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Danh sách khách hàng
            </h1>
        </div>
        <div class="row">
            <table class="table table-bordered">
                <tr>
                    <th>STT</th>
                    <th>Tên Khách Hàng</th>
                    <th>SĐT</th>
                    <th>
                        <select id="duan1" class="form-control">
                            <option>Dự án</option>
                        </select>
                    </th>
                    <th>
                        <select id="duan1" class="form-control">
                            <option>Sản phẩm</option>
                        </select>
                    </th>
                    <th>
                        <select id="duan1" class="form-control">
                            <option>Trạng thái</option>
                        </select>
                    </th>
                    <th>Nội dung chăm sóc</th>
                    <th>Ghi chú</th>
                    <th style="white-space: nowrap; width: 1%"></th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn An Đông</td>
                    <td>039921309</td>
                    <td>CRM 247</td>
                    <td>Phần mềm quản lý</td>
                    <td>Tiềm năng</td>
                    <td>Thiết kế giao diện</td>
                    <td>Ngày 14/09/2019</td>
                    <td style="white-space: nowrap;">
                        <div class='action-buttons'>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-angle-double-down'></i>
                                <span class='sr-only'>Details</span>
                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                <span class='sr-only'>Details</span>

                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class="ace-icon fa fa-trash-o orange2"></i>
                                <span class='sr-only'>Details</span>

                            </a>
                        </div>
                    </td>

                </tr>
                <tr>
                    <td colspan="10" style="background-color: #62a8d1">
                        <div class="col-md-3" style="font-weight: bold; color: white">
                            <a href="#mRecord" id="createNew" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i></a>
                            NỘI DUNG QUAN TÂM                
                        </div>
                        <table class="table table-hover">
                            <tr>
                                <%--                             <th style="white-space: nowrap; width: 1%">Ngày cập nhật</th>--%>
                                <th>
                                    <select id="duan1" class="form-control">
                                        <option>Nhu cầu</option>
                                    </select>
                                </th>
                                <th>
                                    <select id="duan1" class="form-control">
                                        <option>Dự án</option>
                                    </select>
                                </th>
                                <th>
                                    <select id="duan1" class="form-control">
                                        <option>Loại sản phẩm</option>
                                    </select>
                                </th>
                                <th>Giá</th>
                                <th>Diện tích</th>
                                <th>
                                    <select id="duan1" class="form-control">
                                        <option>Số phòng</option>
                                    </select>
                                </th>
                                <th>
                                    <select id="duan1" class="form-control">
                                        <option>Nội thất</option>
                                    </select>
                                </th>
                                <th>Quan tâm khác</th>
                                <th>
                                    <select id="duan1" class="form-control">
                                        <option>Kết quả xử lý</option>
                                    </select>
                                </th>
                                <th style="white-space: nowrap; width: 1%"></th>
                            </tr>
                            <tr>
                                <%--                                <td>14/09/2019</td>--%>
                                <td>Đẹp</td>
                                <td>CRM 247</td>
                                <td>CRM 247</td>
                                <td>100,000</td>
                                <td>100m2</td>
                                <td>3</td>
                                <td>Đầy đủ</td>
                                <td>Dễ nhìn</td>
                                <td>Đang xử lý</td>
                                <td style="white-space: nowrap;">
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <%--                                <td>14/09/2019</td>--%>
                                <td>Đẹp</td>
                                <td>CRM 247</td>
                                <td>CRM 247</td>
                                <td>100,000</td>
                                <td>100m2</td>
                                <td>3</td>
                                <td>Đầy đủ</td>
                                <td>Dễ nhìn</td>
                                <td>Đang xử lý</td>
                                <td>
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <%--                                <td>14/09/2019</td>--%>
                                <td>Đẹp</td>
                                <td>CRM 247</td>
                                <td>CRM 247</td>
                                <td>100,000</td>
                                <td>100m2</td>
                                <td>3</td>
                                <td>Đầy đủ</td>
                                <td>Dễ nhìn</td>
                                <td>Đang xử lý</td>
                                <td>
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="col-md-3" style="font-weight: bold; color: white">
                            <a href="#mCare" id="" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i></a>
                            NỘI DUNG CHĂM SÓC              
                        </div>
                        <table class="table table-hover">
                            <tr>
                                <th style="white-space: nowrap; width: 1%">Ngày</th>
                                <th>Nhân viên</th>
                                <th>Kênh chăm sóc</th>
                                <th>Nội dung chăm sóc</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>Dự án</td>
                                <td>thiết kế giao diện Customer</td>
                                <td style="white-space: nowrap; width: 1%">
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>Facebook</td>
                                <td>thiết kế giao diện Customer</td>
                                <td>
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>SMS</td>
                                <td>thiết kế giao diện Customer</td>
                                <td>
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>SMS</td>
                                <td>thiết kế giao diện Customer</td>
                                <td>
                                    <div class='action-buttons'>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                        <a href='#' class='green bigger-140 show-details-btn'>
                                            <i class="ace-icon fa fa-trash-o orange2"></i>
                                            <span class='sr-only'>Details</span>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Nguyễn Duy Tâm</td>
                    <td>0981823849</td>
                    <td>CRM 247</td>
                    <td>Phần mềm quản lý</td>
                    <td>Tiềm năng</td>
                    <td>Thiết kế giao diện</td>
                    <td>Ngày 14/09/2019</td>
                    <td style="white-space: nowrap;">
                        <div class='action-buttons'>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-angle-double-down'></i>
                                <span class='sr-only'>Details</span>
                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                <span class='sr-only'>Details</span>

                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class="ace-icon fa fa-trash-o orange2"></i>
                                <span class='sr-only'>Details</span>

                            </a>
                        </div>
                    </td>
                </tr>
                <%--                <tr>
                    <td colspan="7" style="background-color:bisque">
                        <div class="col-md-3">NỘ DUNG QUAN TÂM</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Nhu cầu</th>
                                <th>Dự án</th>
                                <th>Loại sản phẩm</th>
                                <th>Giá</th>
                                <th>Diện tích</th>
                                <th>Số phòng</th>
                                <th>Nội thất</th>
                                <th>Quan tâm khác</th>
                                <th>Kết quả xử lý</th>
                            </tr>
                            <tr>
                                <td>Đẹp</td>
                                <td>CRM 247</td>
                                <td>CRM 247</td>
                                <td>100,000</td>
                                <td>100m2</td>
                                <td>3</td>
                                <td>Đầy đủ</td>
                                <td>Dễ nhìn</td>
                                <td>Đang xử lý</td>
                            </tr>
                        </table>
                        <div class="col-md-3">NỘI DUNG CHĂM SÓC</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Ngày chăm sóc</th>
                                <th>Nhân viên</th>
                                <th>Kênh</th>
                                <th>Nọi dung</th>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>Dự án</td>
                                <td>thiết kế giao diện Customer</td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <tr>
                    <td>3</td>
                    <td>Thái Doãn Thọ</td>
                    <td>0912388832</td>
                    <td>CRM 247</td>
                    <td>Phần mềm quản lý</td>
                    <td>Tiềm năng</td>
                    <td>Thiết kế giao diện</td>
                    <td>Ngày 14/09/2019</td>
                    <td style="white-space: nowrap;">
                        <div class='action-buttons'>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-angle-double-down'></i>
                                <span class='sr-only'>Details</span>
                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                <span class='sr-only'>Details</span>

                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class="ace-icon fa fa-trash-o orange2"></i>
                                <span class='sr-only'>Details</span>

                            </a>
                        </div>
                    </td>
                </tr>
                <%--                <tr>
                    <td colspan="7" style="background-color:bisque">
                        <div class="col-md-3">NỘI DUNG QUAN TÂM</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Nhu cầu</th>
                                <th>Dự án</th>
                                <th>Loại sản phẩm</th>
                                <th>Giá</th>
                                <th>Diện tích</th>
                                <th>Số phòng</th>
                                <th>Nội thất</th>
                                <th>Quan tâm khác</th>
                                <th>Kết quả xử lý</th>
                            </tr>
                            <tr>
                                <td>Đẹp</td>
                                <td>CRM 247</td>
                                <td>CRM 247</td>
                                <td>100,000</td>
                                <td>100m2</td>
                                <td>3</td>
                                <td>Đầy đủ</td>
                                <td>Dễ nhìn</td>
                                <td>Đang xử lý</td>
                            </tr>
                        </table>
                        <div class="col-md-3">NỘI DUNG CHĂM SÓC</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Ngày chăm sóc</th>
                                <th>Nhân viên</th>
                                <th>Kênh</th>
                                <th>Nọi dung</th>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>Dự án</td>
                                <td>thiết kế giao diện Customer</td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <tr>
                    <td>4</td>
                    <td>Nguyễn Hoàng Phương</td>
                    <td>0942295968</td>
                    <td>CRM 247</td>
                    <td>Phần mềm quản lý</td>
                    <td>Tiềm năng</td>
                    <td>Thiết kế giao diện</td>
                    <td>Ngày 14/09/2019</td>
                    <td style="white-space: nowrap;">
                        <div class='action-buttons'>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-angle-double-down'></i>
                                <span class='sr-only'>Details</span>
                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class='ace-icon fa fa-pencil-square-o orange'></i>
                                <span class='sr-only'>Details</span>

                            </a>
                            <a href='#' class='green bigger-140 show-details-btn'>
                                <i class="ace-icon fa fa-trash-o orange2"></i>
                                <span class='sr-only'>Details</span>

                            </a>
                        </div>
                    </td>
                </tr>
                <%--                <tr>
                    <td colspan="7" style="background-color:bisque">
                        <div class="col-md-3">NỘI DUNG QUAN TÂM</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Nhu cầu</th>
                                <th>Dự án</th>
                                <th>Loại sản phẩm</th>
                                <th>Giá</th>
                                <th>Diện tích</th>
                                <th>Số phòng</th>
                                <th>Nội thất</th>
                                <th>Quan tâm khác</th>
                                <th>Kết quả xử lý</th>
                            </tr>
                            <tr>
                                <td>Đẹp</td>
                                <td>CRM 247</td>
                                <td>CRM 247</td>
                                <td>100,000</td>
                                <td>100m2</td>
                                <td>3</td>
                                <td>Đầy đủ</td>
                                <td>Dễ nhìn</td>
                                <td>Đang xử lý</td>
                            </tr>
                        </table>
                        <div class="col-md-3">NỘI DUNG CHĂM SÓC</div>
                        <table class="table table-bordered">
                            <tr>
                                <th>Ngày chăm sóc</th>
                                <th>Nhân viên</th>
                                <th>Kênh</th>
                                <th>Nọi dung</th>
                            </tr>
                            <tr>
                                <td>14/09/2019</td>
                                <td>Lê Hải Lý</td>
                                <td>Dự án</td>
                                <td>thiết kế giao diện Customer</td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
            </table>
        </div>
    </div>
    <div class="modal fade" id="mRecord" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Thông tin quan tâm</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nhu cầu</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Mua ở"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Cho thuê"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đảo kim cương"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="M-One"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dự án khác</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Other" placeholder="..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Loại sản phẩm</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Biệt thự"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Căn hộ"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Giá (VNĐ)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Price" placeholder="Nhập số" moneyinput value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Diện tích (m<sup>2</sup>)</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txt_Area" placeholder="Nhập số" areainput value="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số PN</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DDL_Bed" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="1PN"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2PN"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3PN"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4PN"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5PN"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nội thất</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Cơ bản"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Đầy đủ"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                Quan tâm khác
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="txt_Note" maxlength="500" style="height: 60px;" placeholder="...."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveRecord" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>Cập nhật                   
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mCare" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Nội dung chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kênh chăm sóc</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Zalo" Disable="disabled"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="FaceBook" Disable="disabled"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nội dung quan tâm</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="DropDownList7" runat="server" CssClass="form-control select2">
                                    <asp:ListItem Value="0" Text="Quan tâm 1" Selected="True" Disable="disabled"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea id="txt_Care" rows="5" cols="20" class="form-control" placeholder="Nội dung chăm sóc"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSaveCare" data-dismiss="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
