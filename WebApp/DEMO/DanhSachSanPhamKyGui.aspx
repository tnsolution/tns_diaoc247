﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="DanhSachSanPhamKyGui.aspx.cs" Inherits="WebApp.DEMO.DanhSachSanPhamKyGui" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Danh sách sản phẩm ký gửi
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class='table table-hover table-bordered' id='tblData'>
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>
                                <select id="duan" class="form-control">
                                    <option>Dự án</option>
                                </select></th>
                            <th>
                                <select id="masanpham" class="form-control">
                                    <option>Mã Sản Phẩm</option>
                                </select></th>
                            <th>
                                <select id="loaisanpham" class="form-control">
                                    <option>Loại Sản Phẩm</option>
                                </select></th>
                            <th>
                                <select id="mucdich/gia" class="form-control">
                                    <option>Mục đích / Giá</option>
                                </select></th>
                            <th>
                                <select id="sopn/dt" class="form-control">
                                    <option>Số PN,DT</option>
                                </select></th>
                            <th>
                                <select id="tinhtrang" class="form-control">
                                    <option>Tình trạng</option>
                                </select></th>
                            <th>
                                <select id="noithat" class="form-control">
                                    <option>Nội thất</option>
                                </select></th>
                            <th>
                                <select id="hinhanh" class="form-control">
                                    <option>Hình Ảnh</option>
                                </select></th>
                            <th>
                                <select id="nhanvien" class="form-control">
                                    <option>Nhân Viên</option>
                                </select></th>
                            <th>Lịch sử chăm sóc</th>
                            <th>Ngày cập nhật</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>1</td>
                        <td>The Sun Avenue</td>
                        <td>GG.10.12</td>
                        <td>Shophouse</td>
                        <td>
                            <div class='row'>
                                <div class='col-xs-6'>Chuyển nhượng</div>
                                <div class='col-xs-6 giatien'>22,500,000,000</div>
                            </div>
                            <div class='row'>
                                <div class='col-xs-6'>Cho thuê</div>
                                <div class='col-xs-6 giatien'>10,000,000</div>
                            </div>
                        </td>
                        <td>3 PN,101 m2</td>
                        <td>Chưa giao dịch</td>
                        <td>Cơ bản</td>
                        <td>Không</td>
                        <td>Nguyễn An Đông</td>
                        <td>cho thuê</td>
                        <td>16/09/2019</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Gaden Gate</td>
                        <td>GG.10.13</td>
                        <td>Office</td>
                        <td>
                            <div class='row'>
                                <div class='col-xs-6'>Chuyển nhượng</div>
                                <div class='col-xs-6 giatien'>25,500,000,000</div>
                            </div>
                            <div class='row'>
                                <div class='col-xs-6'>Cho thuê</div>
                                <div class='col-xs-6 giatien'>12,000,000</div>
                            </div>
                        </td>
                        <td>0 PN,104 m2</td>
                        <td>Chưa giao dịch</td>
                        <td>Cơ bản</td>
                        <td>Không</td>
                        <td>Nguyễn Hoàng Phương</td>
                        <td>cho thuê</td>
                        <td>16/09/2019</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>The Sun Avenue</td>
                        <td>GG.10.14</td>
                        <td>Office</td>
                        <td>
                            <div class='row'>
                                <div class='col-xs-6'>Chuyển nhượng</div>
                                <div class='col-xs-6 giatien'>32,500,000,000</div>
                            </div>
                            <div class='row'>
                                <div class='col-xs-6'>Cho thuê</div>
                                <div class='col-xs-6 giatien'>17,000,000</div>
                            </div>
                        </td>
                        <td>9 PN,101 m2</td>
                        <td>Chưa giao dịch</td>
                        <td>Cơ bản</td>
                        <td>Không</td>
                        <td>Lê Hải Lý</td>
                        <td>cho thuê</td>
                        <td>16/09/2019</td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
