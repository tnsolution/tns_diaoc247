﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="PhieuBanHang(ChuyenNhuong).aspx.cs" Inherits="WebApp.PhieuBanHang_ChuyenNhuong_" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <link rel="stylesheet" href="../TicketCSS.css" />
    <style>
        #printableArea {
            display: none;
        }

        #printableArea {
            display: none;
        }

        @media print {
            .noprint {
                display: none;
            }

            #printableArea {
                display: block;
            }

                #printableArea .hr:not(.hr-dotted) {
                    background-color: black !important;
                }

                #printableArea .hr {
                    margin: 0px !important;
                    height: 1px !important;
                }

                #printableArea .table {
                    margin-bottom: 0px !important;
                    border: 1px solid black !important;
                }

                #printableArea .table-bordered {
                    margin-bottom: 0px !important;
                    border: 1px solid black !important;
                }

                #printableArea .table > tbody > tr > td,
                #printableArea .table > tbody > tr > th,
                #printableArea .table > tfoot > tr > td,
                #printableArea .table > tfoot > tr > th,
                #printableArea .table > thead > tr > td,
                #printableArea .table > thead > tr > th {
                    vertical-align: top !important;
                    padding: 0px !important;
                    padding-left: 4px !important;
                    padding-top: 2px !important;
                    border-top: 0px !important;
                }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content noprint">
        <div class="row">
            <div class="col-lg-9">
                <asp:Literal ID="LitMessage" runat="server"></asp:Literal>
                <div class="row" id="noidung">
                    <table class="table">
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <img src="http://diaoc247.vn/wp-content/themes/central/images/logo.png" height="60" /></td>
                            <td class="center">
                                <b>CÔNG TY CỔ PHẦN ĐẦU TƯ ĐỊA ỐC 247</b>
                            </td>

                            <td class="center">
                                <b>PHIẾU BÁN HÀNG</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM</b>
                            </td>
                            <td class="center">
                                <b>Số:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center">
                                <b>Điện thoại:</b> (028) 6685 1818 &nbsp;&nbsp;&nbsp;&nbsp; <b>Website</b>  : www.diaoc247.vn
                            </td>
                            <td class="center">
                                <b>Ngày lập:<asp:Label ID="lblNgayLap" runat="server" Text=".."></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td class="center"></td>
                            <td class="center"></td>
                            <td class="center">
                                <b>Ngày cọc:<asp:TextBox ID="TextBox1" runat="server" placeholder="Ngày cọc" require="true"></asp:TextBox></b>
                            </td>
                        </tr>
                    </table>
                    <table class="table">
                        <tr>
                            <th class="td15">Dự án:</th>
                            <td>
                                <asp:DropDownList ID="DDLDuAn" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" require="true">
                                    <asp:ListItem Value="0" Text="Beau Rivage Nha Trang"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Đảo Kim Cương"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Garden Gate"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Beau Rivage Nha Trang"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th class="td15">ID(Mã Căn):</th>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control select2" AppendDataBoundItems="true" require="true">
                                    <asp:ListItem Value="0" Text="MN003"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="MN001"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="MN002"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th class="td15">Giao dịch:</th>
                            <td>Chuyển nhượng</td>
                        </tr>
                        <tr>
                            <th>Phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLPhong" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Giao Dịch"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Trưởng phòng:</th>
                            <td>
                                <asp:DropDownList ID="DDLTruongPhong" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text=" Nguyễn Hoàng Phương"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Nhân viên:</th>
                            <td class="td200">
                                <asp:DropDownList ID="DDLNhanVien" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Lê Hải Lý"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Bên A</b>
                                <small>(Bên Bán)</small>:</td>
                            <td>
                                <asp:TextBox ID="txtHotenA" runat="server" CssClass="form-control" Value="Nguyễn An Đông" require="true"></asp:TextBox>
                            </td>
                            <th>ID(SĐT):</th>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Value="0977123881" require="true"></asp:TextBox>
                            </td>
                            <th>Ngày sinh:</th>
                            <td>
                                <asp:TextBox ID="txtNgaySinhA" runat="server" CssClass="form-control" value="20/09/2019" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport số:</th>
                            <td>
                                <asp:TextBox ID="txtCMNDA" runat="server" CssClass="form-control" Value="377827181" require="true"></asp:TextBox>
                            </td>
                            <th>Cấp ngày:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapA" runat="server" CssClass="form-control" value="20/09/2019" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Tại:</th>
                            <td>
                                <asp:TextBox ID="txtNoiCapA" runat="server" CssClass="form-control" Value="Long An" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiLienHeA" runat="server" CssClass="form-control" Value="Bến Lức ,Long An" require="true"></asp:TextBox>
                            </td>
                            <th>Điện thoại:</th>
                            <td>
                                <asp:TextBox ID="txtDienThoaiA" runat="server" CssClass="form-control" Value="0988124912" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ thường trú:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiThuongTruA" runat="server" CssClass="form-control" Value="96/14,Tân Mỹ,Quận 7,TP HCM" require="true"></asp:TextBox>
                            </td>
                            <th>Email:</th>
                            <td>
                                <asp:TextBox ID="txtEmailA" runat="server" CssClass="form-control noneed" value="dongna@hht.vn" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div class="hr hr-dotted hr1"></div>
                            </th>
                        </tr>
                        <tr>
                            <td><b>Bên B</b><small>(Bên Mua)</small>:</td>
                            <td>
                                <asp:TextBox ID="txtHotenB" runat="server" CssClass="form-control" Value="Mai Thảo Minh" require="true"></asp:TextBox>
                            </td>
                            <th>ID(SĐT):</th>
                            <td>
                                <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" Value="0988123881" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Ngày sinh:</th>
                            <td>
                                <asp:TextBox ID="txtNgaySinhB" runat="server" CssClass="form-control" Value="20/10/1994" role='datepicker' require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>CMND/Passport:</th>
                            <td>
                                <asp:TextBox ID="txtCMNDB" runat="server" CssClass="form-control" Value="3284882839" require="true"></asp:TextBox>
                            </td>
                            <th>Cấp ngày:</th>
                            <td>
                                <asp:TextBox ID="txtNgayCapB" runat="server" CssClass="form-control" Value="20/10/2019" role='datepicker' require="true"></asp:TextBox>
                            </td>
                            <th>Tại:</th>
                            <td>
                                <asp:TextBox ID="txtNoiCapB" runat="server" CssClass="form-control" Value="Long An" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ liên hệ:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiLienHeB" runat="server" CssClass="form-control" Value="Tân An , Long An" require="true"></asp:TextBox>
                            </td>
                            <th>Điện thoại:</th>
                            <td>
                                <asp:TextBox ID="txtDienThoaiB" runat="server" CssClass="form-control" Value="0918238123" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ thường trú:</th>
                            <td colspan="3">
                                <asp:TextBox ID="txtDiaChiThuongTruB" runat="server" CssClass="form-control" Value="Tân Bình,TP HCM" require="true"></asp:TextBox>
                            </td>
                            <th>Email:</th>
                            <td>
                                <asp:TextBox ID="txtEmailB" runat="server" CssClass="form-control noneed" Value="thaominh@hht.vn" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </th>
                        </tr>
                        <tr>
                            <th>Mã căn:</th>
                            <td>
                                <asp:TextBox ID="txtMaCan" runat="server" CssClass="form-control" placeholder="Nhập text" require="true"></asp:TextBox>
                            </td>
                            <th>DT tim tường:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichTimTuong" runat="server" placeholder="Nhập số" require="true" Style="width: 50px"></asp:TextBox>m2
                            </td>
                            <th>DT thông thủy:</th>
                            <td>
                                <asp:TextBox ID="txtDienTichThongThuy" runat="server" placeholder="Nhập số" require="true" Style="width: 50px"></asp:TextBox>m2
                            </td>
                        </tr>
                        <tr>
                            <th>Loại hình:</th>
                            <td>
                                <asp:DropDownList ID="DDLLoaiCanHo" runat="server" CssClass="form-control select2" require="true">
                                    <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th>Giá chuyển nhượng</th>
                            <td>
                                <asp:TextBox ID="txtGiaChuyenNhuong" runat="server" placeholder="Nhập số" moneyinput require="true" Style="width: 80px"></asp:TextBox>
                                <asp:DropDownList ID="DDLDonVi" runat="server" Style="width: 50px; border: 0px;">
                                    <asp:ListItem Value="1" Text="VNĐ"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="USD"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="2"><b>Bằng chữ: </b>
                                <asp:Label ID="giachuyennhuongbangchu" runat="server" Text="..."></asp:Label></td>
                        </tr>
                        <tr>
                            <th>Bao gồm:</th>
                            <td colspan="5">
                                <asp:TextBox ID="txtBaoGom" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <b>Phương thức thanh toán:</b> Bằng tiền mặt hoặc chuyển khoản qua tài khoản ngân hàng theo lịch thanh toán như sau
                                    <span class="pull-right noprint">
                                        <button class="btn btn-minier btn-white" type="button" id="AddRow"><i class="ace-icon fa fa-plus-circle blue"></i>Thêm dòng</button>
                                    </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <b>Chủ tài khoản:</b>
                                <asp:TextBox ID="txtChuTaiKhoan" runat="server" CssClass="" placeholder="Nhập text" require="true"></asp:TextBox>
                                <b>STK</b>
                                <asp:TextBox ID="txtSoTaiKhoan" runat="server" CssClass="" placeholder="Số tài khoản" require="true"></asp:TextBox>
                                <b>NH:</b><asp:TextBox ID="txtNganHang" runat="server" CssClass="" placeholder="Ngân hàng" require="true"></asp:TextBox>
                                <b>CN</b><asp:TextBox ID="txtChiNhanh" runat="server" CssClass="" placeholder="Chi nhánh" require="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table class='table table-hover table-bordered' id='tblData'>
                                    <thead>
                                        <tr>
                                            <th>Nội dung</th>
                                            <th>Ngày</th>
                                            <th>Số tiền</th>
                                            <th>Bằng Chữ</th>
                                            <th>Ghi Chú</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <th>Đợt 1</th>
                                        <th>10/09/2019</th>
                                        <th>20,000,000</th>
                                        <th>Hai chục triệu đồng</th>
                                        <th></th>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6"><b>Phí dịch vụ của Bên Tư Vấn được hưởng</b>&nbsp;
                                <asp:TextBox ID="txtPhiC" runat="server" Style="width: 100px" moneyinput require="true" placeholder="Số tiền"></asp:TextBox>
                                <span loaitien>VNĐ</span>,
                                <asp:Label ID="phicbangchu" runat="server" Text="(Bằng chữ: ....)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6"><b>Ngày công chứng dự kiến:</b>
                                <asp:TextBox ID="txtNgayCongChung1" runat="server" placeholder="Nhập text" CssClass="" require="true" Style="width: 80px"></asp:TextBox>
                                <b>không vượt ngày</b>
                                <asp:TextBox ID="txtNgayCongChung2" runat="server" placeholder="Nhập text" CssClass="" require="true" Style="width: 80px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div class="hr hr-double hr1"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Thỏa thuận khác:</th>
                            <td colspan="5">
                                <asp:TextBox ID="txtThoaThuanKhac" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden" require="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-3 noprint" id="right">
                <div class="row">
                    <div class="col-md-12" an="true">Đại diện Bên Tư vấn</div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="DDLDaiDienC" runat="server" CssClass="select2" require="true" AppendDataBoundItems="true">
                            <asp:ListItem Value="--0--" Text="Chọn đại diện bên C" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-white dropdown-toggle">
                                Xử lý<i class="ace-icon fa fa-angle-down icon-on-right"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a id="btnSave" href="#"><i class="ace-icon fa fa-floppy-o"></i>&nbsp;Lưu</a>
                                </li>
                                <%--<li>
                                    <a id="btnCopy" href="#"><i class="ace-icon fa fa-copy"></i>&nbsp;Sao chép</a>
                                </li>--%>
                                <li style="display: none">
                                    <a id="btnSendApprove" href="#"><i class="ace-icon fa fa-send-o"></i>&nbsp;Gửi Duyệt</a>
                                </li>
                                <li>
                                    <a id="btnIn" href="javascript:window.print();"><i class="ace-icon fa fa-print"></i>&nbsp;In</a>
                                </li>
                                <li>
                                    <a id="btnSendTrade" href="#"><i class="ace-icon fa fa-send"></i>&nbsp;Lập giao dịch</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Thông tin</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <asp:Literal ID="LitInfo" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Xử lý file <small>Chọn file cần xuất thông tin</small></h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="DDLFile" runat="server" CssClass="select2" require="true">
                                                    <asp:ListItem Value="003 Hợp Đồng Đặt Cọc Chuyển Nhượng (Tiếng Việt) Remark1.docx" Text="Hợp Đồng Đặt Cọc Chuyển Nhượng"></asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td class="text-right"><a id="btndownload" href="#"><i class="ace-icon fa fa-download"></i>&nbsp;Xuất file</a></td>
                                        </tr>
                                    </table>
                                    <div class="space-2"></div>
                                    <asp:Literal ID="Lit_ListFolder" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box collapsed">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Lịch sử xử lý</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <asp:Literal ID="LitStatus" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="widget-box" style="display: none;">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title">Thảo luận</h4>
                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="discussion" class="ace-scroll" style="max-height: 350px; overflow-y: scroll">
                                        <asp:Literal ID="Literal_Chat" runat="server"></asp:Literal>
                                    </div>
                                    <div class="space-2"></div>
                                    <div class="input-group">
                                        <input type="text" name="message" placeholder="Type Message ..." class="form-control" id="txt_Message" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-warning btn-sm" id="buttonSend" type="button">Send</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Literal ID="LitHtml" runat="server"></asp:Literal>
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_LoaiCan" runat="server" Value="loaihinh" />
    <asp:HiddenField ID="HID_BangThanhToan" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TienThanhToan" runat="server" Value="0" />
    <asp:Button ID="btnTriggerSave" runat="server" Text="Save" Style="display: none; visibility: hidden" />
    <asp:Button ID="btnWord" runat="server" Text="Save" Style="display: none; visibility: hidden" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/numeric-input-example.js"></script>
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="/SoSangChu.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script type="text/javascript" src="TicketEdit2.js"></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            //var level = Page.getUnitLevel();
            //if (level <= 1) {
            //    $("div[an]").show();
            //}
            //else {
            //    $("div[an]").hide();
            //}
        });
    </script>
</asp:Content>
