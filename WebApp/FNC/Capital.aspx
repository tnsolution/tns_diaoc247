﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Capital.aspx.cs" Inherits="WebApp.FNC.Capital" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        sup {
            vertical-align: sup;
            font-size: smaller;
        }

        .td10 {
            width: 10%;
        }

        .td20 {
            width: 20%;
        }
    </style>
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="LitPageHead" runat="server"></asp:Literal>
                <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                <span class="pull-right action-buttons">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <asp:Literal ID="LitTable1" runat="server"></asp:Literal>
            </div>
            <div class="col-xs-6">
                <asp:Literal ID="LitTable2" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mCapitalIn" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin thu quỹ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="control-label">Người giao tiền<sup>*</sup></label>
                                        <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="control-label">Người nhận tiền<sup>*</sup></label>
                                        <asp:DropDownList ID="DDL_Employee2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ngày thu/ nhận tiền<sup>*</sup></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_InCapitalDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung</label>
                                <input type="text" class="form-control" id="txt_InCapitalContents" placeholder="Nhập text" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Số tiền<sup>*</sup></label>
                                <input type="text" class="form-control" id="txt_InCapitalAmount" placeholder="Nhập số" moneyinput="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú</label>
                                <textarea class="form-control" id="txt_InCapitalDescription" placeholder="Nhập text" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveInCapital" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mCapitalOut" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin chi quỹ</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Phòng cần chi<sup>*</sup></label>
                                <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Người cần chi<sup>*</sup></label>
                                <asp:DropDownList ID="DDL_Employee3" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ngày chi<sup>*</sup></label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="txt_OutCapitalDate" role='datepicker' placeholder="Chọn" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nội dung</label>
                        <input type="text" class="form-control" id="txt_OutCapitalContents" placeholder="Nhập text" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Số tiền<sup>*</sup></label>
                        <input type="text" class="form-control" id="txt_OutCapitalAmount" placeholder="Nhập số" value="0" moneyinput="true" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ghi chú</label>
                        <textarea class="form-control" id="txt_OutCapitalDescription" placeholder="Nhập text" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveOutCapital" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm thông tin
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="position: relative;">                    
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="txt_FromDate" placeholder="Từ ngày" />
                        </div>
                    </div>
                    <div class="form-group" style="position: relative;">                  
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="txt_ToDate" placeholder="Đến ngày" />
                        </div>
                    </div>
                    <div class="center">
                        <a class="btn btn-primary" data-dismiss="modal" id="triggerclick">
                            <i class="ace-icon fa fa-search"></i>
                            Tìm
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <asp:HiddenField ID="HID_InKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_OutKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Date" runat="server" />
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:Button ID="btnSearch" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnSearch_Click" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txt_FromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
                $("[id$=HID_FromDate]").val(picker.toString());
                //console.log(picker.toString());
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txt_ToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
                $("[id$=HID_ToDate]").val(picker2.toString());
                //console.log(picker2.toString());
            }
        });

        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();
        });
        $(document).ready(function () {
            $("#txtTuNgay").val(Page.getFirstDayMonth());
            $("#txtDenNgay").val(Page.getLastDayMonth());

            $("#ViewPrevious").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $("#thuquy").click(function () {
                $("[id$=HID_InKey]").val(0);
                $('#mCapitalIn').modal('show');
            });
            $("#chiquy").click(function () {
                $("[id$=HID_OutKey]").val(0);
                $('#mCapitalOut').modal('show');
            });

            $("input[moneyinput]").number(true, 0);
            $("#txt_OutCapitalDate").val();
            $("#txt_InCapitalDate").val();
            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });

            $("#triggerclick").click(function () {
                var date = $("#txt_FromDate").val() + "-" + $("#txt_ToDate").val();
                $('[id$=HID_Date]').val(date);
                $('[id$=btnSearch]').trigger('click');
            });
            $("#tblInCapital").on("click", ".show-details-btn", function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).closest('tr').next().toggleClass('open');
                $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
            });
            $("#tblOutCapital").on("click", ".show-details-btn", function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).closest('tr').next().toggleClass('open');
                $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
            });
                    
            $("#tblInCapital td")
                .not(".noclick")
                .not(":nth-child(2)")
                .not(":last-child")
                .on("click", function () {
                    var trid = $(this).closest('tr').attr('id');
                    $.ajax({
                        type: 'POST',
                        url: '/FNC/Capital.aspx/GetInCapital',
                        data: JSON.stringify({
                            'AutoKey': trid,
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d.Message != "") {
                                alert(msg.d.Message);
                                return false;
                            }
                            $('#mCapitalIn').modal('show');

                            $("[id$=HID_InKey]").val(msg.d.AutoKey);
                            $('[id$=DDL_Employee1]').val(msg.d.CapitalFrom).trigger("change");
                            $('[id$=DDL_Employee2]').val(msg.d.CapitalBy).trigger("change");
                            $('[id$=txt_InCapitalDate]').val(msg.d.CapitalDate);
                            $('[id$=txt_InCapitalContents]').val(msg.d.Contents);
                            $('[id$=txt_InCapitalAmount]').val(msg.d.Amount);
                            $('[id$=txt_InCapitalDescription]').val(msg.d.Description);

                            if (msg.d.IsApproved == 1)
                                $("#mCapitalIn :input").attr("disabled", true);
                            else
                                $("#mCapitalIn :input").attr("disabled", false);
                        },
                        complete: function () {
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //console.log(xhr.status);
                            //console.log(xhr.responseText);
                            //console.log(thrownError);
                        }
                    });
                });
            $("#tblOutCapital td")
                .not(".noclick")
                .not(":nth-child(2)")
                .not(":last-child")
                .on("click", function () {
                    var trid = $(this).closest('tr').attr('id');
                    $.ajax({
                        type: 'POST',
                        url: '/FNC/Capital.aspx/GetOutCapital',
                        data: JSON.stringify({
                            'AutoKey': trid,
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (msg) {
                            if (msg.d.Message != "") {
                                alert(msg.d.Message);
                                return false;
                            }
                            console.log(msg.d.CapitalDate);
                            $('#mCapitalOut').modal('show');
                            $("[id$=HID_OutKey]").val(msg.d.AutoKey);
                            $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                            $('[id$=DDL_Employee3]').val(msg.d.CapitalBy).trigger("change");
                            $('[id$=txt_OutCapitalDate]').val(msg.d.CapitalDate);
                            $('[id$=txt_OutCapitalContents]').val(msg.d.Contents);
                            $('[id$=txt_OutCapitalAmount]').val(msg.d.Amount);
                            $('[id$=txt_OutCapitalDescription]').val(msg.d.Description);

                            if (msg.d.IsApproved == 1)
                                $("#mCapitalOut :input").attr("disabled", true);
                            else
                                $("#mCapitalOut :input").attr("disabled", false);
                        },
                        complete: function () {
                            $('.se-pre-con').fadeOut('slow');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //console.log(xhr.status);
                            //console.log(xhr.responseText);
                            //console.log(thrownError);
                        }
                    });
                });

            $("#btnSaveInCapital").on('click', function () {
                if ($('[id$=txt_InCapitalDate]').val() == '' ||
                     $('[id$=txt_InCapitalAmount]').val() == '' ||
                    $('[id$=DDL_Employee1]').val() == undefined ||
                    $('[id$=DDL_Employee2]').val() == undefined) {
                    alert("Bạn phải nhập đủ thông tin");
                    return false;
                }

                $.ajax({
                    type: 'POST',
                    url: '/FNC/Capital.aspx/SaveInCapital',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_InKey]").val(),
                        'Employee1': $('[id$=DDL_Employee1]').val(),//giao tien
                        'Employee2': $('[id$=DDL_Employee2]').val(),//nhan tien
                        'Date': $('[id$=txt_InCapitalDate]').val(),
                        'Contents': $('[id$=txt_InCapitalContents]').val(),
                        'Description': $('[id$=txt_InCapitalDescription]').val(),
                        'Amount': $('[id$=txt_InCapitalAmount]').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $("[id$=HID_InKey]").val(0);
                            location.href = '/FNC/Capital.aspx';
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSaveOutCapital").on('click', function () {

                if ($('[id$=txt_OutCapitalDate]').val() == '' ||
                    $('[id$=txt_OutCapitalAmount]').val() == '' ||
                    $('[id$=DDL_Department1]').val() == undefined ||
                    $('[id$=DDL_Employee3]').val() == undefined) {
                    alert("Bạn phải nhập đủ thông tin");
                    return false;
                }

                $.ajax({
                    type: 'POST',
                    url: '/FNC/Capital.aspx/SaveOutCapital',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_OutKey]").val(),
                        'Department': $('[id$=DDL_Department1]').val(),//phong nhan
                        'Employee1': $('[id$=DDL_Employee3]').val(),//nguoi nhan
                        'Date': $('[id$=txt_OutCapitalDate]').val(),
                        'Contents': $('[id$=txt_OutCapitalContents]').val(),
                        'Description': $('[id$=txt_OutCapitalDescription]').val(),
                        'Amount': $('[id$=txt_OutCapitalAmount]').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $("[id$=HID_OutKey]").val(0);
                            location.href = '/FNC/Capital.aspx';
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () { },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        function UnApproveCheckIn(trid) {
            if (confirm("Bạn có chắc gỡ duyệt thông tin !")) {
                console.log(trid);
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Capital.aspx/GetInCapitalStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function ApproveCheckIn(trid) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Capital.aspx/GetInCapitalStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function UnApproveCheckOut(trid) {
            console.log(trid);
            if (confirm("Bạn có chắc gỡ duyệt thông tin !.")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Capital.aspx/GetOutCapitalStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showNotiMessageError("Thông báo !", msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function ApproveCheckOut(trid) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được ?. Sau khi duyệt phần mềm sẽ tự tạo phiếu thu cho phòng bạn đã chỉ định.")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/Capital.aspx/GetOutCapitalStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showNotiMessageError("Thông báo !", msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function deleteOutput(id) {
            if (confirm("Bạn có chắc xóa thông tin")) {
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/DeleteOutput_Capital",
                    data: JSON.stringify({
                        "AutoKey": id,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (r) {
                        if (r == "OK")
                            location.reload();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function deleteInput(id) {
            if (confirm("Bạn có chắc xóa thông tin")) {
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/DeleteInput_Capital",
                    data: JSON.stringify({
                        "AutoKey": id,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (r) {
                        if (r.d == "OK")
                            location.reload();
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>