﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CapitalInput.aspx.cs" Inherits="WebApp.FNC.CapitalInput" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td10 {
            width: 10%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1 id="title">Nhập quỹ
                <span class="tools pull-right">
                    <a href="#mCapital" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:literal id="Literal_Table" runat="server"></asp:literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm thông tin
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:dropdownlist id="DDL_Department" runat="server" class="select2" appenddatabounditems="true" style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả phòng --" Selected="True"></asp:ListItem>
                        </asp:dropdownlist>
                    </div>
                    <div class="form-group">
                        <asp:dropdownlist id="DDL_Month" runat="server" class="select2" appenddatabounditems="true" style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả tháng --" Selected="True"></asp:ListItem>
                        </asp:dropdownlist>
                    </div>
                    <div class="center">
                        <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                            <i class="ace-icon fa fa-search"></i>
                            Tìm
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade" id="mCapital" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin thu quỹ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="control-label">Người giao tiền</label>
                                        <asp:dropdownlist id="DDL_Employee2" runat="server" class="form-control select2" appenddatabounditems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:dropdownlist>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="control-label">Người nhận tiền</label>
                                        <asp:dropdownlist id="DDL_Employee1" runat="server" class="form-control select2" appenddatabounditems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:dropdownlist>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ngày thu/ nhận tiền</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_CapitalDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung</label>
                                <input type="text" class="form-control" id="txt_CapitalContents" placeholder="Nhập text" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <input type="text" class="form-control" id="txt_CapitalAmount" placeholder="Nhập số" moneyinput="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú</label>
                                <textarea class="form-control" id="txt_CapitalDescription" placeholder="Nhập text" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveCapital" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>  
    <asp:hiddenfield id="HID_AutoKey" runat="server" value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();
        });
        $(document).ready(function () {
            $("input[moneyinput]").number(true, 0);
            $("#txt_CapitalDate").val(Page.getCurrentDate());
            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });
            //----

            $("#tblCapital tr td:not(:nth-child(7))").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                $.ajax({
                    type: 'POST',
                    url: '/FNC/CapitalInput.aspx/GetCapital',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            alert(msg.d.Message);
                            return false;
                        }
                        $('#mCapital').modal('show');

                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);                     
                        $('[id$=DDL_Employee1]').val(msg.d.CapitalFrom).trigger("change");
                        $('[id$=DDL_Employee2]').val(msg.d.CapitalBy).trigger("change");
                        $('[id$=txt_CapitalDate]').val(msg.d.CapitalDate);
                        $('[id$=txt_CapitalContents]').val(msg.d.Contents);
                        $('[id$=txt_CapitalAmount]').val(msg.d.Amount);
                        $('[id$=txt_CapitalDescription]').val(msg.d.Description);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSaveCapital").on('click', function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/CapitalInput.aspx/SaveCapital',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),                   
                        'Employee1': $('[id$=DDL_Employee1]').val(),
                        'Employee2': $('[id$=DDL_Employee2]').val(),                   
                        'Date': $('[id$=txt_CapitalDate]').val(),
                        'Contents': $('[id$=txt_CapitalContents]').val(),
                        'Description': $('[id$=txt_CapitalDescription]').val(),
                        'Amount': $('[id$=txt_CapitalAmount]').val(),                     
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $("[id$=HID_AutoKey]").val(0);
                            location.href = '/FNC/CapitalInput.aspx';
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        function ApproveCheck(trid) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/CapitalInput.aspx/CapitalStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
