﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="CapitalOutput.aspx.cs" Inherits="WebApp.FNC.CapitalOutput" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td10 {
            width: 10%;
        }

        .td20 {
            width: 20%;
        }

        .gachngang {
            text-decoration: line-through !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1 id="title">Chi Quỹ
                <span class="tools pull-right">
                    <a href="#mCapital" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mCapital" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin chi quỹ</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Phòng cần chi</label>
                                <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Người cần chi</label>
                                <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ngày chi</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="txt_CapitalDate" role='datepicker' placeholder="Chọn" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Nội dung</label>
                        <input type="text" class="form-control" id="txt_CapitalContents" placeholder="Nhập text" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Số tiền</label>
                        <input type="text" class="form-control" id="txt_CapitalAmount" placeholder="Nhập số" value="0" moneyinput="true" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ghi chú</label>
                        <textarea class="form-control" id="txt_CapitalDescription" placeholder="Nhập text" rows="4"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSave" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script>
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            $('input[moneyinput]').number(true, 0);
            $("#txt_CapitalDate").val(Page.getCurrentDate());
            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });
            $("#tblCapital").on("click", "tr td:not(.noclick)", function () {
                var trid = $(this).closest('tr').attr('id');
                $.ajax({
                    type: 'POST',
                    url: '/FNC/CapitalOutput.aspx/GetCapital',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            alert(msg.d.Message);
                            return false;
                        }
                        console.log(msg.d.CapitalDate);
                        $('#mCapital').modal('show');
                        $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                        $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                        $('[id$=DDL_Employee1]').val(msg.d.ReceiptFrom).trigger("change");
                        $('[id$=txt_CapitalDate]').val(msg.d.ReceiptDate);
                        $('[id$=txt_CapitalContents]').val(msg.d.Contents);
                        $('[id$=txt_CapitalAmount]').val(msg.d.Amount);
                        $('[id$=txt_CapitalDescription]').val(msg.d.Description);
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSave").on('click', function () {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/CapitalOutput.aspx/SaveCapital',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Department': $('[id$=DDL_Department1]').val(),
                        'Employee1': $('[id$=DDL_Employee1]').val(),
                        'Date': $('[id$=txt_CapitalDate]').val(),
                        'Contents': $('[id$=txt_CapitalContents]').val(),
                        'Description': $('[id$=txt_CapitalDescription]').val(),
                        'Amount': $('[id$=txt_CapitalAmount]').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $("[id$=HID_AutoKey]").val(0);
                            location.href = '/FNC/CapitalOutput.aspx';
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });

            });
        });
        function ApproveCheck(trid) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được ?. Sau khi duyệt phần mềm sẽ tự tạo phiếu thu cho phòng bạn đã chỉ định.")) {
                $.ajax({
                    type: 'POST',
                    url: '/FNC/CapitalOutput.aspx/CapitalStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showNotiMessageError("Thông báo !", msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
