﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

/*
 role ACC
 nếu có quyền edit => quyền duyệt
 nếu có quyền delete => quyền xóa và gỡ duyệt
     */

namespace WebApp.FNC
{
    public partial class Receipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRoles();

                if (Request["Category"] != null)
                    HID_Category.Value = Request["Category"];

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");

                LoadData(FromDate, ToDate);
            }
        }
        protected void LoadData(DateTime FromDate, DateTime ToDate)
        {
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            int Category = HID_Category.Value.ToInt();
            DataTable zTable = new DataTable();
            zTable = Receipt_Detail_Data.List(FromDate, ToDate, ViewDepartment, Category);

            lbltime.Text = "Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy");
            Literal_Table.Text = HtmlData(zTable);
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            LoadData(FromDate, ToDate);
        }                
        protected static string HtmlData(DataTable TableIn)
        {
            StringBuilder zSb = new StringBuilder();
            #region [Thu]
            zSb.AppendLine("<table class='table table-bordered table-hover table-striped' id='tblReceipt'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Ngày thu</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("        <th>Người giao tiền</th>");
            zSb.AppendLine("        <th>Người nhận tiền</th>");
            zSb.AppendLine("        <th>Số tiền thu</th>");
            if (_Permitsion[3].ToInt() == 1)
                zSb.AppendLine("        <th isdelete class='td5 noprint'>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (TableIn.Rows.Count > 0)
            {
                int No = 1;
                double Total = 0;
                foreach (DataRow r in TableIn.Rows)
                {
                    string zDeleteButton = "";
                    string zStatus = "";
                    string zDisableClick = "";
                    int Approved = r["IsApproved"].ToInt();

                    if (Approved == 1)
                    {
                        zDisableClick = "noclick";
                        zStatus = "<img width=20 class='noprint' style='cursor:pointer;float:right' onclick='UnApproveCheck(" + r["AutoKey"].ToString() + ")' id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/true.png' />";
                        Total += Convert.ToDouble(r["Amount"]);
                    }
                    else
                    {
                        zDisableClick = "";
                        zDeleteButton = "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["AutoKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                        zStatus = "<img width=20 class='noprint' style='cursor:pointer;float:right' onclick='ApproveCheck(" + r["AutoKey"].ToString() + ")' id='" + r["AutoKey"].ToString() + "' check='" + r["IsApproved"].ToInt() + "' alt='' src='../template/custom-image/false.png' />";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "'>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + (No++) + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["Department"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["ReceiptDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["Contents"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + "'>" + r["ReceiptByName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='noclick'>" + zStatus + r["ReceiptFromName"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='" + zDisableClick + " giatien td10'>" + r["Amount"].ToDoubleString() + "</td>");
                    if (_Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td class='noclick noprint' isdelete>" + zDeleteButton + "</td>");
                    zSb.AppendLine("            </tr>");
                }

                zSb.AppendLine("            <tr>");
                zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("               <td colspan='6' style='text-align: right'><b>Tổng</b></td>");
                zSb.AppendLine("               <td class='giatien td10'>" + Total.ToDoubleString() + "</td>");
                if (_Permitsion[3].ToInt() == 1)
                    zSb.AppendLine("               <td>#</td>");
                zSb.AppendLine("            </tr>");
            }
            else { zSb.AppendLine("<tr><td></td><td colspan='8'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            #endregion

            return zSb.ToString();
        }
        protected static void AutoMessage(int Key)
        {
            string SQL = "";
            DataTable zTable = Employees_Data.ListWorking();
            foreach (DataRow r in zTable.Rows)
            {
                SQL += @" INSERT INTO SYS_Message [ObjectTable], [ObjectKey], [EmployeeKey], [Readed],[Description]) VALUES (N'FNC_Receipt_Detail'," + Key + "," + r["EmployeeKey"].ToInt() + ",0,N'XapXinhThu')";
            }

            CustomInsert.Exe(SQL);
        }
        #region [Send Mail]
        [WebMethod]
        public static void SendMail(string noidung)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            smtpClient.Credentials = new System.Net.NetworkCredential("247autocrm@gmail.com", "auto247crm");
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.Body = "Có thông tin thu $$ đã chỉnh sửa " + noidung + " !. Vui lòng không phản hồi mail này";
            mail.From = new MailAddress("247autocrm@gmail.com", "CRM.DIAOC247.VN");
            mail.To.Add(new MailAddress("troly.diaoc247@gmail.com"));

            smtpClient.Send(mail);
        }
        #endregion
        #region [Excel]
        protected void ExportExcel()
        {
            DateTime FromDate = Tools.ConvertToDate(HID_FromDate.Value);
            DateTime ToDate = Tools.ConvertToDate(HID_ToDate.Value);
            DataTable zTable = new DataTable();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            int Category = HID_Category.Value.ToInt();
            zTable = Receipt_Detail_Data.List(FromDate, ToDate, ViewDepartment, Category);

            DataTable dtExcel = new DataTable();
            dtExcel.Columns.Add("Phòng", typeof(string));
            dtExcel.Columns.Add("Ngày thu", typeof(string));
            dtExcel.Columns.Add("Nội dung", typeof(string));
            dtExcel.Columns.Add("Ghi chú", typeof(string));
            dtExcel.Columns.Add("Người giao tiền", typeof(string));
            dtExcel.Columns.Add("Người nhận tiền", typeof(string));
            dtExcel.Columns.Add("Số tiền", typeof(string));

            double tong1 = 0;
            foreach (DataRow r in zTable.Rows)
            {
                DataRow rExcel = dtExcel.NewRow();
                rExcel[0] = r["Department"].ToString();
                rExcel[1] = r["ReceiptDate"].ToDateString();
                rExcel[2] = r["Contents"].ToString();
                rExcel[3] = r["Description"].ToString();
                rExcel[4] = r["ReceiptByName"].ToString();
                rExcel[5] = r["ReceiptFromName"].ToString();
                rExcel[6] = r["Amount"].ToDoubleString();
                dtExcel.Rows.Add(rExcel);

                tong1 += r["Amount"].ToDouble();
            }
            DataRow rTong = dtExcel.NewRow();
            rTong[0] = "Tổng cộng";
            rTong[6] = tong1.ToString("n0");
            dtExcel.Rows.Add(rTong);
            DownloadExcel(dtExcel);
        }
        protected void DownloadExcel(DataTable dtExcel)
        {
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView GridView1 = new GridView();
                GridView1.AllowPaging = false;
                GridView1.DataSource = dtExcel;
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = Color.WhiteSmoke;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = Color.LemonChiffon;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Thu.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }
        #endregion
        #region [Ajax]
        [WebMethod]
        public static ItemReturn Search(string FromDate, string ToDate, string Department, string Category)
        {
            DateTime dtFromDate = Tools.ConvertToDate(FromDate);
            DateTime dtToDate = Tools.ConvertToDate(ToDate);
            dtFromDate = new DateTime(dtFromDate.Year, dtFromDate.Month, dtFromDate.Day, 0, 0, 0);
            dtToDate = new DateTime(dtToDate.Year, dtToDate.Month, dtToDate.Day, 23, 59, 59);

            ItemReturn zResult = new ItemReturn();
            DataTable zTable = Receipt_Detail_Data.List(dtFromDate, dtToDate, Department.ToInt(), Category.ToInt());
            zResult.Result = HtmlData(zTable);
            return zResult;
        }
        [WebMethod]
        public static ItemReceipt GetReceipt(int AutoKey)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            ItemReceipt zItem = new ItemReceipt();
            zItem.AutoKey = zInfo.AutoKey.ToString();
            zItem.ReceiptDate = zInfo.ReceiptDate.ToDateString();
            zItem.DepartmentKey = zInfo.DepartmentKey.ToString();
            zItem.Contents = zInfo.Contents;
            zItem.Description = zInfo.Description;
            zItem.Amount = zInfo.Amount.ToDoubleString();
            zItem.ReceiptFrom = zInfo.ReceiptFrom.ToString();
            zItem.ReceiptBy = zInfo.ReceiptBy.ToString();
            if (zInfo.IsApproved == 1)
                zItem.Message = "Thông tin này đã duyệt, bạn không được chỉnh sửa !.";
            else
                zItem.Message = "";
            return zItem;
        }
        [WebMethod]
        public static ItemReturn ReceiptStatus(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string SaveReceipt(int AutoKey, string Department, string Employee1, string Employee2,
            string Category, string Date, string Contents, string Description, string Amount)
        {
            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            zInfo.ReceiptDate = Tools.ConvertToDate(Date);
            zInfo.ReceiptFrom = Employee1.ToInt();
            zInfo.ReceiptBy = Employee2.ToInt();
            zInfo.Contents = Contents.Trim();
            zInfo.Description = Description.Trim();
            zInfo.Amount = double.Parse(Amount);
            zInfo.CategoryKey = Category.ToInt();
            zInfo.DepartmentKey = Department.ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            if (AutoKey != 0)
                SendMail(Contents + " " + Description);
            if (zInfo.Message == string.Empty)
                return "OK";
            else
                return "Lỗi không lưu được";
        }
        #endregion
        static string[] _Permitsion;
        #region [CheckRoles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        protected void CheckRoles()
        {           
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();           
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();        
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];

            string zRolePage = "ACC";
            _Permitsion = User_Data.RolesCheck(UserKey, zRolePage).Split(',');
            string zScript = "<script>";
            if (UnitLevel < 7)
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE DepartmentKey IN (" + ViewDepartment + ") AND IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE DepartmentKey IN (" + ViewDepartment + ") AND IsWorking = 2 ORDER BY LastName", false);
                DDL_Department.Visible = true;
            }
            else
            {
                Tools.DropDown_DDL(DDL_Department1, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey=" + ViewDepartment + " ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments DepartmentKey=" + ViewDepartment + "ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee1, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE EmployeeKey=" + Employee + " AND IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME  FROM HRM_Employees WHERE EmployeeKey=" + Employee + " AND IsWorking = 2 ORDER BY LastName", false);
                DDL_Department.Visible = false;
                DDL_Department1.SelectedValue = Department.ToString();
                DDL_Department.SelectedValue = Department.ToString();
            }

            if (_Permitsion[2].ToInt() == 1)
            {
                //_IsApprove = true;
                #region [Duyet]
                zScript += Html_Approve();
                #endregion
            }
            if (_Permitsion[3].ToInt() == 1)
            {
                #region [xóa, gỡ duyệt]
                zScript += Html_Undo() + Html_Delete();
                #endregion
            }

            LitScript.Text = zScript + "</script>";
        }
        #endregion

        protected string Html_Approve()
        {
            return @"
function ApproveCheck(trid) {
    if (confirm('Bạn có chắc duyệt thông tin !.Sau khi duyệt thông tin sẽ không thay đổi được')) {
        $.ajax({
                type: 'POST',
            url: '/FNC/Receipt.aspx/ReceiptStatus',
            data: JSON.stringify({
                        'AutoKey': trid,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function() {
                $('.se-pre-con').fadeIn('slow');
                    },
            success: function(msg) {
                        if (msg.d.Message != ''){
                        Page.showPopupMessage('Lỗi !', msg.d.Message);
                        $('.se-pre-con').fadeOut('slow');}
                        else{
                            location.reload();
                        }
                    },
            complete: function() {$('.se-pre-con').fadeOut('slow');},
            error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }";
        }
        protected string Html_Undo()
        {
            return @"
function UnApproveCheck(trid)
        {
            if (confirm('Bạn có chắc gỡ duyệt thông tin !.'))
            {
        $.ajax({
                type: 'POST',
            url: '/FNC/Receipt.aspx/ReceiptStatus',
            data: JSON.stringify({
                        'AutoKey': trid,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function() {
                $('.se-pre-con').fadeIn('slow');
                    },
            success: function(msg) {
                        if (msg.d.Message != ''){
                            Page.showPopupMessage('Lỗi !', msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else{
                            location.reload();
                        }
                    },
            complete: function() {$('.se-pre-con').fadeOut('slow');},
            error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }";
        }
        protected string Html_Delete()
        {
            return @"
function deleteInput(id) {
    if (confirm('Bạn có chắc xóa thông tin')) {
        $.ajax({
                type: 'POST',
            url: '/Ajax.aspx/DeleteReceipt',
            data: JSON.stringify({'AutoKey': id,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function() {
                    $('.se-pre-con').fadeIn('slow');
                    },success: function(r) {
                        if (r.d == 'OK')
                            location.reload();
                    },complete: function() {$('.se-pre-con').fadeOut('slow');
                    },error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }";
        }
    }
}