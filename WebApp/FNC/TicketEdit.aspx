﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master"
    AutoEventWireup="true" CodeBehind="TicketEdit.aspx.cs"
    Inherits="WebApp.FNC.TicketEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td15 {
            width: 15%;
        }

        .td10 {
            width: 10%;
        }

        sup {
            vertical-align: sup;
            font-size: smaller;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Phiếu bán hàng
                <span class="tools pull-right">
                    <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                    <a type="button" class="btn btn-white btn-default btn-bold" href="TradeList.aspx">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Về danh sách
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_TicketCategory" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
</asp:Content>
