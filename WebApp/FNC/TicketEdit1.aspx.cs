﻿using FindAndReplace;
using Lib.CRM;
using Lib.FNC;
using Lib.HRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
//thue
namespace WebApp.FNC
{
    public partial class TicketEdit1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
                string ViewDepartment = "";
                if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                    ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
                else
                    ViewDepartment = Department;

                if (UnitLevel < 7)
                {
                    Tools.DropDown_DDL(DDLNhanVien, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDLTruongPhong, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND PositionKey = 3 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDLPhong, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDLDuAn, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                    Tools.DropDown_DDL(DDLLoaiCanHo, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                    Tools.DropDown_DDL(DDLDaiDienC, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                }
                else
                {
                    Tools.DropDown_DDL(DDLNhanVien, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDLTruongPhong, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND PositionKey = 3 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDLPhong, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);
                    Tools.DropDown_DDL(DDLDuAn, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                    Tools.DropDown_DDL(DDLLoaiCanHo, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                    Tools.DropDown_DDL(DDLDaiDienC, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                }

                lblNgayLap.Text = DateTime.Now.ToString("dd/MM/yyyy");

                LoadInfo();
                LoadFile();
                TicketTracking();
            }
        }
        protected void btnWord_Click(object sender, EventArgs e)
        {
            GenerateAll();
            LoadFile();
        }
        protected void btnTriggerSave_Click(object sender, EventArgs e)
        {
            SaveInfo();
            TicketTracking();
        }

        protected void GenerateAll()
        {
            #region Init Bên C
            int EmployeeKey = DDLDaiDienC.SelectedValue.ToInt();
            Employees_Info zInfoEmployee = new Employees_Info(EmployeeKey);
            Departments_Info zInfoDepartment = new Departments_Info(zInfoEmployee.DepartmentKey);
            string hotenC = DDLDaiDienC.SelectedItem.Text;
            string chucvudaidien = zInfoEmployee.Position;
            string diachiphongban = zInfoDepartment.Address;
            string giayuyquyen = zInfoDepartment.Authority; //"(Theo giấy ủy quyền của Tổng Giám Đốc số UQ-02/DO247/2017 ký ngày 01/01/2017)";
            //string diachiC = "Số 12, Đường số 09, Phường Bình Trưng Đông, Quận 2, Tp.HCM";
            #endregion

            string baogom = "";
            if (txtBaoGom.Text.Trim().Length == 0)
                baogom = "..";
            else
                baogom = txtBaoGom.Text.Trim();

            Project_Info zProject = new Project_Info(DDLDuAn.SelectedValue.ToInt());

            //xx đang xử lý
            #region [lịch thanh toán]
            List<TicketPayment> zList = new List<TicketPayment>();
            string[] row;
            if (HID_BangThanhToan.Value.Contains(";"))
            {
                row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                if (row.Length > 0)
                {
                    for (int i = 0; i < row.Length; i++)
                    {
                        string item = row[i];
                        string[] col = item.Split(':');
                        TicketPayment zPayment = new TicketPayment();
                        zPayment.Content1 = col[0].Trim();
                        zPayment.Content2 = col[1].Trim();
                        zPayment.Content3 = col[2].Trim().ToDoubleString();
                        zPayment.Content4 = Utils.NumberToWordsVN(col[2].Trim().ToDouble()).FirstCharToUpper();
                        zPayment.Content5 = col[5].Trim();
                        zPayment.Content6 = col[6].Trim();
                        zList.Add(zPayment);
                    }
                }
            }
            #endregion

            string FileName = DDLFile.SelectedValue;
            string PathIn = @"/Upload/File/HD/" + FileName;
            string PathOut = @"/Upload/File/HD/" + FileName.Split('.')[0] + "_Output_" + DateTime.Now.ToString("yyMMdd_hhmm") + ".docx";

            string InputFile = HttpContext.Current.Server.MapPath(@"/") + PathIn;
            string OutputFile = HttpContext.Current.Server.MapPath(@"/") + PathOut;

            File.Copy(InputFile, OutputFile, true);

            #region [xử lý file]
            var stream = new MemoryStream();
            using (var fileStream = File.OpenRead(OutputFile))
            {
                fileStream.CopyTo(stream);
                using (var flatDocument = new FlatDocument(stream))
                {
                    #region [tieu de]                    
                    flatDocument.FindAndReplace("#loaican", HID_LoaiCan.Value.Trim());
                    flatDocument.FindAndReplace("#macan", txtMaCan.Text.Trim());
                    flatDocument.FindAndReplace("#dientich", txtDienTichTimTuong.Text.Trim());
                    flatDocument.FindAndReplace("#duan", DDLDuAn.SelectedItem.Text);

                    flatDocument.FindAndReplace("#ngaylap", Tools.ConvertToDate(txtNgayCoc.Text).ToString("dd"));
                    flatDocument.FindAndReplace("#thanglap", Tools.ConvertToDate(txtNgayCoc.Text).ToString("MM"));
                    flatDocument.FindAndReplace("#namlap", Tools.ConvertToDate(txtNgayCoc.Text).Year.ToString());

                    flatDocument.FindAndReplace("#diachiduan", zProject.Address);
                    #endregion

                    #region [--A--]
                    flatDocument.FindAndReplace("#hotenA", txtHotenA.Text.Trim());
                    flatDocument.FindAndReplace("#ngaysinhA", txtNgaySinhA.Text.Trim());
                    flatDocument.FindAndReplace("#cmndA", txtCMNDA.Text.Trim());
                    flatDocument.FindAndReplace("#ngaycapA", txtNgayCapA.Text.Trim());
                    flatDocument.FindAndReplace("#noicapA", txtNoiCapA.Text.Trim());
                    flatDocument.FindAndReplace("#diachithuongtruA", txtDiaChiThuongTruA.Text.Trim());
                    flatDocument.FindAndReplace("#diachilienlacA", txtDiaChiLienHeA.Text.Trim());
                    flatDocument.FindAndReplace("#dienthoaiA", txtDienThoaiA.Text.Trim());
                    if (txtEmailA.Text.Trim().Length > 0)
                        flatDocument.FindAndReplace("#EmailA", txtEmailA.Text.Trim());
                    else
                        flatDocument.FindAndReplace("#EmailA", " ");
                    #endregion

                    #region [--B--]
                    flatDocument.FindAndReplace("#hotenB", txtHotenB.Text.Trim());
                    flatDocument.FindAndReplace("#ngaysinhB", txtNgaySinhB.Text.Trim());
                    flatDocument.FindAndReplace("#cmndB", txtCMNDB.Text.Trim());
                    flatDocument.FindAndReplace("#ngaycapB", txtNgayCapB.Text.Trim());
                    flatDocument.FindAndReplace("#noicapB", txtNoiCapB.Text.Trim());
                    flatDocument.FindAndReplace("#diachithuongtruB", txtDiaChiThuongTruB.Text.Trim());
                    flatDocument.FindAndReplace("#diachilienlacB", txtDiaChiLienHeB.Text.Trim());
                    flatDocument.FindAndReplace("#dienthoaiB", txtDienThoaiB.Text.Trim());
                    if (txtEmailB.Text.Trim().Length > 0)
                        flatDocument.FindAndReplace("#EmailB", txtEmailB.Text.Trim());
                    else
                        flatDocument.FindAndReplace("#EmailB", " ");
                    #endregion

                    #region [--C--]
                    if (chucvudaidien.Contains("Trưởng"))
                    {
                        flatDocument.FindAndReplace("#uyquyenC", giayuyquyen);
                    }
                    else
                    {
                        flatDocument.FindAndReplace("#uyquyenC", " ");
                    }

                    flatDocument.FindAndReplace("#chucvuC", chucvudaidien);
                    flatDocument.FindAndReplace("#hotenC", hotenC);
                    //flatDocument.FindAndReplace("#diachiC", diachiC);
                    flatDocument.FindAndReplace("#diachilienheC", diachiphongban);
                    #endregion

                    #region [--Thanh toán--]
                    for (int i = 0; i < zList.Count; i++)
                    {
                        TicketPayment zPayment = zList[i];
                        switch (i)
                        {
                            case 0:
                                flatDocument.FindAndReplace("#ngaycoc", zPayment.Content2);
                                flatDocument.FindAndReplace("#sotiencocbangso", zPayment.Content3 + " VNĐ");
                                flatDocument.FindAndReplace("#sotiencocbangchu", zPayment.Content4);
                                break;
                            case 1:
                                flatDocument.FindAndReplace("#ngaythuedotdau", zPayment.Content2);
                                flatDocument.FindAndReplace("#sotienthuedotdaubangso", zPayment.Content3 + " VNĐ");
                                flatDocument.FindAndReplace("#sotienthuedotdaubangchu", zPayment.Content4);
                                break;

                            default:
                                break;
                        }
                    }
                    //tiếp theo
                    string tienthanhtoan = Utils.NumberToWordsVN(txtSoTienThanhToan.Text.Trim().ToDouble()).FirstCharToUpper();
                    flatDocument.FindAndReplace("#solanthanhtoan", txtLan.Text.Trim());                             //Bên Thuê sẽ thanh toán cho Bên Cho Thuê
                    flatDocument.FindAndReplace("#sotungay", txtTuNgay.Text.Trim());                                   //lần vào ngày 
                    flatDocument.FindAndReplace("#sodenngay", txtDenNgay.Text.Trim());                      //đến ngày 
                    flatDocument.FindAndReplace("#sothangthanhtoan", txtMoiThang.Text.Trim());                       //đầu mỗi 
                    flatDocument.FindAndReplace("#sotienthanhtoanbangso", txtSoTienThanhToan.Text.Trim() + " VNĐ");                  //số tiền 
                    flatDocument.FindAndReplace("#sotienthanhtoanbangchu", tienthanhtoan);                      //Bằng chữ
                    #endregion

                    string giatri = Utils.NumberToWordsVN(txtGiaThue.Text.Trim().ToDouble()).FirstCharToUpper();
                    flatDocument.FindAndReplace("#giatrihdbangso", txtGiaThue.Text.Trim() + " VNĐ");
                    flatDocument.FindAndReplace("#giatrihdbangchu", giatri);

                    string PhiCBangChu = Utils.NumberToWordsVN(txtPhiC.Text.Trim().ToDouble()).FirstCharToUpper();
                    flatDocument.FindAndReplace("#sotienbenCbangso", txtPhiC.Text.Trim() + " VNĐ");
                    flatDocument.FindAndReplace("#sotienbenCbangchu", PhiCBangChu);

                    if (DDLFile.SelectedValue.Contains("Đặt Cọc Thuê"))
                    {
                        flatDocument.FindAndReplace("#thoigiankyhopdong", txtThoiGianKyHD.Text.Trim());
                    }
                    if (DDLFile.SelectedValue.Contains("Hợp Đồng Thuê"))
                    {
                        flatDocument.FindAndReplace("#tentaikhoanA", txtChuTaiKhoan.Text.Trim());
                        flatDocument.FindAndReplace("#sotaikhoanA", txtSoTaiKhoan.Text.Trim());
                        flatDocument.FindAndReplace("#tennganhangA", txtNganHang.Text.Trim());
                        flatDocument.FindAndReplace("#chinhanhA", txtChiNhanh.Text.Trim());
                    }

                    flatDocument.FindAndReplace("#noidungbaogom", txtBaoGom.Text.Trim());

                    if (txtThoaThuanKhac.Text.Trim().Length > 0)
                        flatDocument.FindAndReplace("#thoathuankhac", txtThoaThuanKhac.Text.Trim());
                    else
                        flatDocument.FindAndReplace("#thoathuankhac", " ");

                    flatDocument.FindAndReplace("#tentaikhoanA", txtChuTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#sotaikhoanA", txtSoTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#tennganhangA", txtNganHang.Text.Trim());
                    flatDocument.FindAndReplace("#chinhanh", txtChiNhanh.Text.Trim());

                    flatDocument.FindAndReplace("#thuetungay", txtThueTuNgay.Text.Trim());
                    flatDocument.FindAndReplace("#thuedenngay", txtThueDengnay.Text.Trim());
                    flatDocument.FindAndReplace("#thoigianbangiao", txtThoiGianBanGiao.Text.Trim());
                    flatDocument.FindAndReplace("#thoigiantinhtienthue", txtThoiGianTinhTienThue.Text.Trim());
                }
            }
            #endregion

            File.WriteAllBytes(OutputFile, stream.ToArray());
            FileInfo file = new FileInfo(OutputFile);
            if (file.Exists)
            {

                string zSQL = @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey ,ImagePath, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES (N'FNC_Ticket','" + HID_TicketKey.Value.ToInt() + "',N'" + OutputFile + "',N'" + PathOut + "',N'" + DDLFile.SelectedItem.Text + "',GETDATE(), GETDATE(),'"
                    + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                    + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                    + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                    + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";

                CustomInsert.Exe(zSQL);
            }
        }
        protected void SaveInfo()
        {
            int Key = HID_TicketKey.Value.ToInt();

            #region [thong tin phieu ban hang]
            Ticket_Info zInfo = new Ticket_Info(Key);
            zInfo.CodeDuAn = DDLDuAn.SelectedValue;
            zInfo.Category = 1;
            zInfo.NgayCoc = Tools.ConvertToDate(txtNgayCoc.Text.Trim());

            zInfo.DepartmentKey = DDLPhong.SelectedValue;
            zInfo.Department = DDLPhong.SelectedItem.Text;
            zInfo.EmployeeKey = DDLNhanVien.SelectedValue;
            zInfo.EmployeeName = DDLNhanVien.SelectedItem.Text;
            zInfo.ManagerKey = DDLTruongPhong.SelectedValue;
            zInfo.ManagerName = DDLTruongPhong.SelectedItem.Text;
            zInfo.DaiDien = DDLDaiDienC.SelectedValue;

            zInfo.HotenA = txtHotenA.Text.Trim();
            zInfo.NgaySinhA = txtNgaySinhA.Text.Trim();
            zInfo.CMNDA = txtCMNDA.Text.Trim();
            zInfo.NgayCapA = txtNgayCapA.Text.Trim();
            zInfo.NoiCapA = txtNoiCapA.Text.Trim();
            zInfo.DiaChiLienHeA = txtDiaChiLienHeA.Text.Trim();
            zInfo.DiaChiLienLacA = txtDiaChiThuongTruA.Text.Trim();
            zInfo.DienThoaiA = txtDienThoaiA.Text.Trim();
            zInfo.EmailA = txtEmailA.Text.Trim();

            zInfo.HotenB = txtHotenB.Text.Trim();
            zInfo.NgaySinhB = txtNgaySinhB.Text.Trim();
            zInfo.CMNDB = txtCMNDB.Text.Trim();
            zInfo.NgayCapB = txtNgayCapB.Text.Trim();
            zInfo.NoiCapB = txtNoiCapB.Text.Trim();
            zInfo.DiaChiLienHeB = txtDiaChiLienHeB.Text.Trim();
            zInfo.DiaChiLienLacB = txtDiaChiThuongTruB.Text.Trim();
            zInfo.DienThoaiB = txtDienThoaiB.Text.Trim();
            zInfo.EmailB = txtEmailB.Text.Trim();

            zInfo.MaCan = txtMaCan.Text.Trim();
            zInfo.LoaiCan = DDLLoaiCanHo.SelectedItem.Text.Trim();
            zInfo.DienTichTimTuong = txtDienTichTimTuong.Text.Trim();
            zInfo.DienTichThongThuy = txtDienTichThongThuy.Text.Trim();
            zInfo.GiaChoThue = txtGiaThue.Text.Trim();
            zInfo.GiaChoThueBangChu = Utils.NumberToWordsVN(txtGiaThue.Text.ToDouble()).FirstCharToUpper();
            zInfo.ThueBaoGom = txtBaoGom.Text.Trim();

            zInfo.ChuTaiKhoan = txtChuTaiKhoan.Text.Trim();
            zInfo.SoTaiKhoan = txtSoTaiKhoan.Text.Trim();
            zInfo.NganHang = txtNganHang.Text.Trim();
            zInfo.ChiNhanh = txtChiNhanh.Text.Trim();

            zInfo.BThanhToanThueA = txtSoTienThanhToan.Text.Trim();
            zInfo.SoLan = txtLan.Text.Trim();
            zInfo.TuNgay = txtTuNgay.Text.Trim();
            zInfo.DenNgay = txtDenNgay.Text.Trim();
            zInfo.DauMoi = txtMoiThang.Text.Trim();

            zInfo.PhiDichVuC = txtPhiC.Text.Trim();
            zInfo.ThoiGianKyHD = txtThoiGianKyHD.Text.Trim();
            zInfo.ThueTuNgay = txtThueTuNgay.Text.Trim();
            zInfo.ThueDenNgay = txtThueDengnay.Text.Trim();
            zInfo.ThoiGianBanGiao = txtThoiGianBanGiao.Text.Trim();
            zInfo.ThoiGianTinhTien = txtThoiGianTinhTienThue.Text.Trim();

            zInfo.ThoaThuanThueKhac = txtThoaThuanKhac.Text.Trim();
            zInfo.Unit = DDLDonVi.SelectedValue;
            zInfo.ChiNhanh = txtChiNhanh.Text.Trim();

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            string[] row = HID_TienThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                for (int i = 0; i < row.Length; i++)
                {
                    string[] coladdin = row[i].Split(':');
                    double moneyaddin = coladdin[2].Trim().ToDouble();
                    string addin = coladdin[0].Trim() + " ngày " + coladdin[1].Trim()
                               + " số tiền " + moneyaddin.ToString("n0")
                               + " bằng chữ " + Utils.NumberToWordsVN(moneyaddin)
                               + " tương đương " + coladdin[4].Trim()
                               + " " + coladdin[5].Trim();

                    zInfo.NgayThueThangDau = coladdin[1].Trim();
                    zInfo.TienThueThangDau = moneyaddin.ToString("n0");
                    zInfo.SoTienThueThangBangChu = Utils.NumberToWordsVN(moneyaddin).FirstCharToUpper();
                    zInfo.SoThangThueDau = coladdin[5].Trim();
                }
            }

            zInfo.Save();
            #endregion

            HID_TicketKey.Value = zInfo.TicketKey.ToString();
            if (zInfo.Message != string.Empty)
            {
                LitMessage.Text = "Lỗi liên hệ Admin " + zInfo.Message;
                return;
            }

            #region [thong tin bang thanh toan]
            row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                string SQL = " DELETE FNC_TicketPayment WHERE TicketKey =" + zInfo.TicketKey;
                foreach (string item in row)
                {
                    string[] col = item.Split(':');
                    double money = col[2].Trim().ToDouble();
                    string MoneyChu = "";
                    if (DDLDonVi.SelectedValue.ToString() == "1")
                        MoneyChu = Utils.NumberToWordsVN(money);
                    else
                        MoneyChu = Utils.NumberToWordsEN((long)money);

                    SQL += @"
INSERT INTO FNC_TicketPayment(Title ,TimePayment, Description ,Amount, AmountInWord, Unit,Number ,CreatedDate ,ModifiedDate ,TicketKey )
VALUES (N'" + col[0] + "' ,N'" + col[1] + "',N'" + col[6] + "' ,N'" + money.ToString("n0") + "',N'"
    + MoneyChu.FirstCharToUpper() + "',N'"
    + DDLDonVi.SelectedItem.Text + "' ,N'" + col[5]
    + "',GETDATE() ,GETDATE() ," + zInfo.TicketKey + " ) ";

                }

                string Message = CustomInsert.Exe(SQL).Message;
                if (Message != string.Empty)
                    LitMessage.Text = "Lỗi liên hệ Admin " + Message;
            }
            #endregion

            TrackStatus(HID_TicketKey.Value.ToInt(),
                "Cập nhật thông tin phiếu bán hàng !.",
                HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            string url = HttpContext.Current.Request.Url.ToString();
            if (!url.Contains("?"))
                Response.Redirect(url + "?id=" + zInfo.TicketKey, true);
            else
                Response.Redirect(url);
        }
        protected void LoadInfo()
        {
            StringBuilder zSb = new StringBuilder();

            string TienThueNha = "";
            string NgayThueNha = "";
            //string DonViTien = "";
            string SoTienBangChu = "";
            string SoThang = "";

            if (Request["ID"] != null)
            {
                #region [MyRegion]
                HID_TicketKey.Value = Request["ID"];
                int Key = HID_TicketKey.Value.ToInt();
                Ticket_Info zInfo = new Ticket_Info(Key);
                DDLDuAn.SelectedValue = zInfo.CodeDuAn;
                DDLPhong.SelectedValue = zInfo.DepartmentKey.ToString();
                DDLNhanVien.SelectedValue = zInfo.EmployeeKey.ToString();
                DDLTruongPhong.SelectedValue = zInfo.ManagerKey.ToString();
                DDLDaiDienC.SelectedValue = zInfo.DaiDien.ToString();
                DDLLoaiCanHo.SelectedItem.Text = zInfo.LoaiCan;

                txtNgayCoc.Text = zInfo.NgayCoc.ToString("dd/MM/yyyy");
                txtHotenA.Text = zInfo.HotenA;
                txtNgaySinhA.Text = zInfo.NgaySinhA;
                txtCMNDA.Text = zInfo.CMNDA;
                txtNgayCapA.Text = zInfo.NgayCapA;
                txtNoiCapA.Text = zInfo.NoiCapA;
                txtDiaChiLienHeA.Text = zInfo.DiaChiLienHeA;
                txtDiaChiThuongTruA.Text = zInfo.DiaChiLienLacA;
                txtDienThoaiA.Text = zInfo.DienThoaiA;
                txtEmailA.Text = zInfo.EmailA;
                txtHotenB.Text = zInfo.HotenB;
                txtNgaySinhB.Text = zInfo.NgaySinhB;
                txtCMNDB.Text = zInfo.CMNDB;
                txtNgayCapB.Text = zInfo.NgayCapB;
                txtNoiCapB.Text = zInfo.NoiCapB;
                txtDiaChiLienHeB.Text = zInfo.DiaChiLienHeB;
                txtDiaChiThuongTruB.Text = zInfo.DiaChiLienLacB;
                txtDienThoaiB.Text = zInfo.DienThoaiB;
                txtEmailB.Text = zInfo.EmailB;
                txtMaCan.Text = zInfo.MaCan;
                txtDienTichTimTuong.Text = zInfo.DienTichTimTuong;
                txtDienTichThongThuy.Text = zInfo.DienTichThongThuy;
                txtGiaThue.Text = zInfo.GiaChoThue;
                giathuebangchu.Text = zInfo.GiaChoThueBangChu.FirstCharToUpper();
                txtBaoGom.Text = zInfo.ThueBaoGom;
                txtChuTaiKhoan.Text = zInfo.ChuTaiKhoan;
                txtSoTaiKhoan.Text = zInfo.SoTaiKhoan;
                txtNganHang.Text = zInfo.NganHang;
                txtSoTienThanhToan.Text = zInfo.BThanhToanThueA;
                txtLan.Text = zInfo.SoLan;
                txtTuNgay.Text = zInfo.TuNgay;
                txtDenNgay.Text = zInfo.DenNgay;
                txtMoiThang.Text = zInfo.DauMoi;
                txtPhiC.Text = zInfo.PhiDichVuC;
                phicbangchu.Text = zInfo.PhiDichVuCBangChu.FirstCharToUpper();
                txtChiNhanh.Text = zInfo.ChiNhanh;

                txtThoiGianKyHD.Text = zInfo.ThoiGianKyHD;
                txtThueDengnay.Text = zInfo.ThueDenNgay;
                txtThueTuNgay.Text = zInfo.ThueTuNgay;
                txtThoiGianBanGiao.Text = zInfo.ThoiGianBanGiao;
                txtThoiGianTinhTienThue.Text = zInfo.ThoiGianTinhTien;

                txtThoaThuanKhac.Text = zInfo.ThoaThuanThueKhac;
                txtThoaThuanKhac.Text = zInfo.ThoaThuanThueKhac;

                DDLDonVi.SelectedValue = zInfo.Unit;
                #endregion

                TienThueNha = zInfo.TienThueThangDau;
                NgayThueNha = zInfo.NgayThueThangDau;
                //if (zInfo.Unit == "1")
                //    DonViTien = "VNĐ";
                //else
                //    DonViTien = "USD";
                SoTienBangChu = zInfo.SoTienThueThangBangChu.FirstCharToUpper();
                SoThang = zInfo.SoThangThueDau;

                HID_LoaiCan.Value = zInfo.LoaiCan;

                #region MyRegion
                zSb.AppendLine("<table class='table' style='border:0px !important'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người khởi tạo:</td><td>" + zInfo.CreatedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày khởi tạo:</td><td>" + zInfo.CreatedDate + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate + "</td></tr>");
                //zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người duyệt:</td><td></td></tr>");
                //zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày duyệt:</td><td>" + zInfo.ApprovedDate + "</td></tr>");
                zSb.AppendLine("</table>");
                LitInfo.Text = zSb.ToString();
                #endregion

                LitHtml.Text = GenHTML(zInfo.CreatedDate.ToString("dd/MM/yyyy"));
            }

            #region MyRegion
            DataTable zTable = TicketPayment_Data.List(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                #region bang data
                string table = @"<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Nội dung</th>
                                        <th class='td10'>Ngày</th>
                                        <th colspan='2' class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th colspan='2'>Ghi chú (tương đương)</th>
                                        <th class='noprint' >#</th>
                                    </tr>
                                </thead>
                                <tbody>";
                foreach (DataRow r in zTable.Rows)
                {
                    table += @"<tr id='" + r["AutoKey"].ToString() + "'><td>"
                        + r["Title"].ToString() + "</td><td tabindex = '1'>"
                        + r["TimePayment"].ToString() + "</td><td onclick='addInput(this)'>"
                        + r["Amount"].ToDoubleString() + "</td><td tabindex = '1'>"
                        + r["Unit"].ToString() + "</td><td tabindex = '1'>"
                        + r["AmountInWord"].ToString() + "</td><td tabindex='1'>"
                        + r["Number"].ToString() + "</td><td tabindex='1' class='edit-disabled'>"
                        + r["Description"].ToString() + "</td><td class='edit-disabled noprint' ><span btn='btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i></span></td></tr> ";
                }

                table += "</tbody>";
                table += "</table>";
                LitBangThanhToan.Text = table;
                #endregion               
            }
            else
            {
                #region bang trong
                LitBangThanhToan.Text = @"
<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Nội dung</th>
                                        <th class='td10'>Ngày</th>
                                        <th colspan='2' class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th colspan='2'>Ghi chú (tương đương)</th>
                                        <th class='noprint' >#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id=1>
                                        <td>Tiền đặt cọc</td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1' onclick='addInput(this)'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint' ><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i></span></td>
                                    </tr>
                                    <tr id=2>
                                        <td>Tiền thuê nhà đợt đầu</td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1' onclick='addInput(this)'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint' ><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i></span></td>
                                    </tr>
                                </tbody>
                               
                            </table>";
                #endregion               
            }
            #endregion            
        }
        protected void LoadFile()
        {
            List<ItemDocument> zList = Document_Data.List(HID_TicketKey.Value.ToInt(), "FNC_Ticket");
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblFile'>");
            zSb.AppendLine("  <thead>");
            zSb.AppendLine("     <tr>");
            zSb.AppendLine("         <th>#</th>");
            zSb.AppendLine("         <th>Tập tin</th>");
            zSb.AppendLine("         <th>#</th>");
            zSb.AppendLine("     </tr>");
            zSb.AppendLine(" </thead>");
            zSb.AppendLine(" <tbody>");
            int i = 1;
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td style='text-align:right'><a target='_blank' href='" + item.ImageUrl + "'><i class='ace-icon fa fa-download'></i></a> | <span btn='btnDeleteFile' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i></span></td></tr>");
            }
            zSb.AppendLine(" </tbody>");
            zSb.AppendLine(" </table>");
            Lit_ListFolder.Text = zSb.ToString();
        }
        protected void LoadChat()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Ticket_Data.Tracking(HID_TicketKey.Value.ToInt(), "CHAT");

            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<div class='direct-chat-msg well well-sm'>");
                zSb.AppendLine("  <div class='direct-chat-info clearfix'>");
                zSb.AppendLine("   <span class='direct-chat-name pull-left'>" + r["Title"].ToString() + "</span>");
                zSb.AppendLine("   <span class='direct-chat-timestamp pull-right'>" + Convert.ToDateTime(r["Date"]).ToString("hh:mm") + "</span>");
                zSb.AppendLine("  </div>");
                zSb.AppendLine(" <div class='direct-chat-text'>" + r["Message"].ToString() + "</div>");
                zSb.AppendLine("</div>");
            }

            Literal_Chat.Text = zSb.ToString();
        }
        protected void TicketTracking()
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<ul class='list-unstyled spaced'>");
            DataTable zTable = Ticket_Data.Tracking(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<li><i class='ace-icon fa fa-bell-o bigger-110 purple'></i>" + r["Date"].ToDateTimeString() + " | " + r["Title"].ToString() + " | " + r["Message"].ToString() + "</li>");
                }
            }
            zSb.AppendLine("</ul>");
            LitStatus.Text = zSb.ToString();
        }
        protected string GenHTML(string NgayLap)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div id='printableArea' class='seeprint'>");
            #region [Header]
            zSb.AppendLine(@"
<table class='table'>
                        <tbody><tr>
                            <td rowspan='3' style='width: 75px'>
                                <img src='http://diaoc247.vn/wp-content/themes/central/images/logo.png' height='60'></td>
                            <td class='center'>
                                <b>CÔNG TY CỔ PHẦN ĐẦU TƯ ĐỊA ỐC 247</b>
                            </td>
                            <td class='center'>
                                <b>PHIẾU BÁN HÀNG</b>
                            </td>
                        </tr>
                        <tr>
                            <td class='center'>
                                <b>12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM</b>
                            </td>
                            <td class='center'>
                                <b>Số:</b>
                            </td>
                        </tr>
                        <tr>
                            <td class='center'>
                                <b>Điện thoại:</b> (028) 6685 1818 &nbsp;&nbsp;&nbsp;&nbsp; <b>Website</b>  : www.diaoc247.vn
                            </td>
                            <td class='center'>
                                <b>Ngày lập:" + NgayLap + @"</span></b>
                            </td>
                        </tr>
                    </tbody></table>
");
            #endregion

            zSb.AppendLine(@"<table class='table'><tbody>");

            #region [Row]
            zSb.AppendLine(@"<tr><th >Dự án:</th><td>" + DDLDuAn.SelectedItem.Text + "</td><th >Giao dịch:</th><td>Cho thuê</td><th >Ngày cọc:</th><td>" + txtNgayCoc.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th>Phòng:</th><td>" + DDLPhong.SelectedItem.Text + "</td><th>Trưởng phòng:</th><td>" + DDLTruongPhong.SelectedItem.Text.Trim() + "</td><th>Nhân viên:</th><td>" + DDLNhanVien.SelectedItem.Text + "</td></tr>");
            #endregion

            zSb.AppendLine(@"<tr><td colspan=6><div class='hr hr-double hr1'></div></td></tr>");

            zSb.AppendLine(@"<tr><td><b>Bên A</b><small>(Cho thuê):</small></td><td colspan=3> " + txtHotenA.Text.Trim() + "</td><th>Ngày sinh:</th><td>" + txtNgaySinhA.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th>CMND/Passport:</th><td>" + txtCMNDA.Text.Trim() + "</td><th>Cấp ngày:</th><td>" + txtNgayCapA.Text.Trim() + "</td><th>Tại:</th><td>" + txtNoiCapA.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th >Địa chỉ liên hệ:</th><td colspan=3>" + txtDiaChiLienHeA.Text.Trim() + "</td><th>Điện thoại:</th><td>" + txtDienThoaiA.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th >Địa chỉ thường trú:</th><td colspan=3>" + txtDiaChiThuongTruA.Text.Trim() + "</td><th>Email:</th><td>" + txtEmailA.Text.Trim() + "</td></tr>");

            zSb.AppendLine(@"<tr><th colspan=6><div class='hr hr-dotted hr1'></div></th></tr>");

            zSb.AppendLine(@"<tr><td><b>Bên B</b><small>(Bên thuê):</small></td><td colspan=3>" + txtHotenB.Text.Trim() + "</td><th>Ngày sinh:</th><td>" + txtNgaySinhB.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th>CMND/Passport:</th><td>" + txtCMNDB.Text.Trim() + "</td><th>Cấp ngày:</th><td>" + txtNgayCapB.Text.Trim() + "</td><th>Tại:</th><td>" + txtNoiCapB.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th >Địa chỉ liên hệ:</th><td colspan=3>" + txtDiaChiLienHeB.Text.Trim() + "</td><th>Điện thoại:</th><td>" + txtDienThoaiB.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><th >Địa chỉ thường trú:</th><td colspan=3>" + txtDiaChiThuongTruB.Text.Trim() + "</td><th>Email:</th><td>" + txtEmailB.Text.Trim() + "</td></tr>");

            zSb.AppendLine(@"<tr><td colspan=6><div class='hr hr-double hr1'></div></td></tr>");

            zSb.AppendLine(@"<tr><th>Mã căn:</th><td>" + txtMaCan.Text.Trim() + "</td><th>DT tim tường:</th><td>" + txtDienTichTimTuong.Text.Trim() + " m2</td><th>DT thông thủy:</th><td>" + txtDienTichThongThuy.Text.Trim() + " m2</td></tr>");

            string giatri = Utils.NumberToWordsVN(txtGiaThue.Text.Trim().ToDouble()).FirstCharToUpper();
            zSb.AppendLine(@"<tr><th>Loại hình:</th><td>" + DDLLoaiCanHo.SelectedItem.Text + "</td><th>Giá:</th><td colspan=3>" + txtGiaThue.Text.Trim() + lblDonvi.Text + " (" + giatri + ")</td></tr>");
            zSb.AppendLine(@"<tr><th>Bao gồm:</th><td colspan=5>" + txtBaoGom.Text.Trim() + "</td></tr>");

            zSb.AppendLine(@"<tr><td colspan=6><div class='hr hr-double hr1'></div></td></tr>");

            zSb.AppendLine(@"<tr><td colspan=6><b>Phương thức thanh toán:</b> Bằng tiền mặt hoặc chuyển khoản qua tài khoản ngân hàng theo lịch thanh toán như sau</td></tr>");
            zSb.AppendLine(@"<tr><td><b>Chủ tài khoản:</b></td><td>" + txtChuTaiKhoan.Text.Trim() + "</td><td><b>STK:</b></td><td>" + txtSoTaiKhoan.Text.Trim() + "</td><td><b>NH:</b></td><td>" + txtNganHang.Text + "-" + txtChiNhanh.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><td colspan=6>");

            #region MyRegion
            DataTable zTable = TicketPayment_Data.List(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                #region table data
                string table = @"<table class='table table-bordered'>
                                <thead>
                                    <tr>
                                        <th class='td15'>Nội dung</th>
                                        <th>Ngày</th>
                                        <th colspan='2'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th>Ghi chú</th>                                       
                                    </tr>
                                </thead>
                                <tbody>";
                foreach (DataRow r in zTable.Rows)
                {
                    table += @"<tr id='" + r["AutoKey"].ToString() + "'><td>"
                        + r["Title"].ToString() + "</td><td>"
                        + r["TimePayment"].ToString() + "</td><td style='text-align:right'>"
                        + r["Amount"].ToDoubleString() + "</td><td>"
                        + r["Unit"].ToString() + "</td><td>"
                        + r["AmountInWord"].ToString().FirstCharToUpper() + "</td><td>"
                        + r["Number"].ToString() + "-" + r["Description"].ToString() + "</td></tr> ";
                }

                table += "</tbody>";
                table += "</table>";
                zSb.AppendLine(table);
                #endregion               
            }
            #endregion

            zSb.AppendLine(@"</td></tr>");
            string giatri1 = Utils.NumberToWordsVN(txtSoTienThanhToan.Text.Trim().ToDouble()).FirstCharToUpper();
            zSb.AppendLine(@"<tr><td colspan=6><b>Các đợt tiếp theo:</b>Bên B thanh toán bên A số tiền " + txtSoTienThanhToan + lblDonvi.Text + " thanh toán " + txtLan.Text.Trim() + " lần, từ ngày " + txtTuNgay.Text.Trim() + " đến ngày" + txtDenNgay.Text.Trim() + " đầu mỗi " + txtMoiThang.Text.Trim() + " tháng</td></tr>");

            zSb.AppendLine(@"<tr><td colspan=6><div class='hr hr-double hr1'></div></td></tr>");
            string giatri3 = Utils.NumberToWordsVN(txtPhiC.Text.Trim().ToDouble()).FirstCharToUpper();
            zSb.AppendLine(@"<tr><td colspan=6><b>Phí dịch vụ của Bên Tư Vấn được hưởng:</b> " + txtPhiC.Text.Trim() + lblDonvi.Text + ",Bằng chữ: " + giatri3 + "</span></td></tr>");
            zSb.AppendLine(@"<tr><td colspan=6><div class='hr hr-double hr1'></div></td></tr>");
            zSb.AppendLine(@"<tr><td colspan=6>Thời điểm giao nhận, cho thuê</td></tr>");
            zSb.AppendLine(@"<tr><td colspan=6>Thời gian thuê: từ ngày " + txtThueTuNgay.Text.Trim() + " đến ngày " + txtThueDengnay.Text.Trim() + " Thời gian ký hợp đồng: " + txtThoiGianKyHD.Text.Trim() + " Thời gian bàn giao: " + txtThoiGianBanGiao.Text.Trim() + " Thời gian tính tiền thuê:" + txtThoiGianTinhTienThue.Text.Trim() + "</td></tr>");
            zSb.AppendLine(@"<tr><td colspan=6><div class='hr hr-double hr1'></div></td></tr>");
            zSb.AppendLine(@"<tr><th >Thỏa thuận khác:</th><td colspan=5>" + txtThoaThuanKhac.Text.Trim() + "</td></tr>");
            zSb.AppendLine("</tbody></table>");
            zSb.AppendLine("</div>");

            return zSb.ToString();
        }

        #region [Ajax]
        [WebMethod]
        public static ItemReturn DeleteFile(int FileKey)
        {
            ItemReturn zResult = new ItemReturn();
            Document_Info zInfo = new Document_Info(FileKey);
            zInfo.Delete();
            zResult.Message = zInfo.Message;

            if (zInfo.Message == string.Empty)
            {
                string OutputFile = HttpContext.Current.Server.MapPath(@"/") + zInfo.ImageUrl;
                if (File.Exists(OutputFile))
                    File.Delete(OutputFile);
            }
            return zResult;
        }
        [WebMethod]
        public static ItemReturn SendApprove(int TicketKey)
        {
            TrackStatus(TicketKey,
            "Gửi duyệt !.",
            HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = "OK";
            return zResult;
        }
        [WebMethod]
        public static ItemReturn Approve(int TicketKey)
        {
            Ticket_Info zTicket = new Ticket_Info();
            zTicket.TicketKey = TicketKey;
            zTicket.Approved = 1;
            zTicket.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zTicket.UpdateStatus();

            TrackStatus(TicketKey,
            "Đã duyệt !.",
            HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zTicket.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn NotApprove(int TicketKey)
        {
            Ticket_Info zTicket = new Ticket_Info();
            zTicket.TicketKey = TicketKey;
            zTicket.Approved = 0;
            zTicket.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zTicket.UpdateStatus();

            TrackStatus(TicketKey,
            "Không duyệt !.",
            HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Message = zTicket.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn SendTrade(int TicketKey)
        {
            Ticket_Info zTicket = new Ticket_Info(TicketKey);

            #region check data
            int CustomerKeyA = Customer_Data.CheckExists(zTicket.DienThoaiA);
            int CustomerKeyB = Customer_Data.CheckExists(zTicket.DienThoaiB);
            int TradeKey = Trade_Data.CheckExists(TicketKey);
            #endregion

            #region pre init data
            Trade_Info zTrade = new Trade_Info(TradeKey);
            Project_Info zProject = new Project_Info(zTicket.CodeDuAn.ToInt());
            Product_Info zProduct = new Product_Info(zTicket.MaCan, zProject.ShortTable);
            ItemAsset zAsset = zProduct.ItemAsset;
            #endregion

            #region  thong tin giao dich
            zTrade.DateDeposit = zTicket.NgayCoc;
            zTrade.TransactionDate = DateTime.Now;
            zTrade.EmployeeKey = zTicket.EmployeeKey.ToInt();
            zTrade.DepartmentKey = zTicket.DepartmentKey.ToInt();
            zTrade.ProjectKey = zAsset.ProjectKey.ToInt();
            zTrade.AssetKey = zAsset.AssetKey.ToInt();
            zTrade.AssetID = zAsset.AssetID;
            zTrade.AssetType = zAsset.AssetType;
            zTrade.DateContractEnd = Tools.ConvertToDate(zTicket.ThueDenNgay);

            float dientich = 0;
            float.TryParse(zAsset.AreaWall, out dientich);
            zTrade.Area = dientich;
            zTrade.AssetCategory = zAsset.CategoryKey.ToInt();

            if (zTicket.Category == 1)
                zTrade.TradeCategoryKey = 227;
            else
                zTrade.TradeCategoryKey = 228;

            zTrade.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zTrade.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zTrade.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zTrade.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];

            zTrade.IsDraft = zTicket.TicketKey; // dùng xác nhận giao dịch từ phiếu báng hàng
            zTrade.Save();
            #endregion

            #region thong tin khach a           
            Customer_Info zCustomer = new Customer_Info(CustomerKeyA);
            zCustomer.CustomerName = zTicket.HotenA;
            zCustomer.Phone1 = zTicket.DienThoaiA;
            zCustomer.CardID = zTicket.CMNDA;
            zCustomer.CardPlace = zTicket.NoiCapA;
            zCustomer.Birthday = Tools.ConvertToDate(zTicket.NgaySinhA);
            zCustomer.CardDate = Tools.ConvertToDate(zTicket.NgayCapA);
            zCustomer.Email1 = zTicket.EmailA;
            zCustomer.Address1 = zTicket.DiaChiLienHeA;
            zCustomer.Address2 = zTicket.DiaChiLienLacA;
            zCustomer.HasTrade = 1;
            zCustomer.Save();
            CustomerKeyA = zCustomer.Key;

            Trade_Customer_Info zTradeCustomer = new Trade_Customer_Info();
            zTradeCustomer.CustomerKey = CustomerKeyA;
            zTradeCustomer.Key = zTrade.Key;
            zTradeCustomer.IsOwner = 1;
            zTradeCustomer.Create();
            #endregion

            #region  thong tin khach b           
            zCustomer = new Customer_Info(CustomerKeyB);
            zCustomer.CustomerName = zTicket.HotenB;
            zCustomer.Phone1 = zTicket.DienThoaiB;
            zCustomer.CardID = zTicket.CMNDB;
            zCustomer.CardPlace = zTicket.NoiCapB;
            zCustomer.Birthday = Tools.ConvertToDate(zTicket.NgaySinhB);
            zCustomer.CardDate = Tools.ConvertToDate(zTicket.NgayCapB);
            zCustomer.Email1 = zTicket.EmailB;
            zCustomer.Address1 = zTicket.DiaChiLienHeB;
            zCustomer.Address2 = zTicket.DiaChiLienLacB;
            zCustomer.HasTrade = 1;
            zCustomer.CompanyName = zTicket.CongTy;
            zCustomer.Address3 = zTicket.DiaChiLienHeB;
            zCustomer.TaxCode = zTicket.MaSoThue;
            zCustomer.Description = "Đại diện " + zTicket.DaiDien;
            zCustomer.HasTrade = 1;
            zCustomer.Save();
            CustomerKeyB = zCustomer.Key;

            zTradeCustomer = new Trade_Customer_Info();
            zTradeCustomer.CustomerKey = CustomerKeyB;
            zTradeCustomer.Key = zTrade.Key;
            zTradeCustomer.IsOwner = 2;
            zTradeCustomer.Create();
            #endregion

            TrackStatus(TicketKey,
               "Đã gửi lập phiếu giao dịch !.",
               HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);

            ItemReturn zResult = new ItemReturn();
            zResult.Result = zTrade.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static void TrackStatus(int TicketKey, string Message, string Employee)
        {
            string SQL = @"INSERT INTO SYS_Message 
(Message,Title,Date,ObjectTable,ObjectKey,EmployeeKey,DepartmentKey,Readed,Description) 
VALUES (N'" + Message + "',N'" + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode() + "', GETDATE(), N'FNC_Ticket',"
+ TicketKey + "," + Employee + "," + Employees_Data.GetDepartment(Employee.ToInt()) + ",0,N'Tình Trạng Ticket')";

            CustomInsert.Exe(SQL);
        }
        #endregion        
    }
}