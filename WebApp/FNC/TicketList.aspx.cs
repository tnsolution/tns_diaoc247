﻿using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TicketList : System.Web.UI.Page
    {
        #region [Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        protected void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();            
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            CookEmployee.Text = Employee.ToString();          
            CookUnitLevel.Text = UnitLevel.ToString();
            CookUserKey.Text = UserKey.ToString();
            CookDepartmentRole.Text = ViewDepartment;
            string RolePage = "FNC";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            if (UnitLevel < 7)
            {
                Tools.DropDown_DDL(DDL_Project, "SELECT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey IN (" + ViewDepartment + ") ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;
            }
            else
            {
                Tools.DropDown_DDL(DDL_Project, "SELECT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey IN (" + ViewDepartment + ") ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey =" + ViewDepartment + " ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + "ORDER BY LastName", false);
                DDL_Employee.SelectedValue = Employee.ToString();
                DDL_Department.Visible = false;
                DDL_Employee.Visible = false;
            }          
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
                LoadData();
            }
        }
        protected void LoadData()
        {
            int UnitLevel = CookUnitLevel.Text.ToInt();           
            int Employee = CookEmployee.Text.ToInt();
            string Department = CookDepartmentRole.Text;

            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = new DataTable();
            if (Session["SearchTicket"] != null)
            {
                Item_Trade_Search zSession = (Item_Trade_Search)Session["SearchTicket"];
                zTable = Ticket_Data.List(zSession.Employee, zSession.Department, zSession.AssetID, zSession.Project, Tools.ConvertToDate(zSession.FromDate), Tools.ConvertToDate(zSession.ToDate));
            }
            else
            {
                if (UnitLevel < 7)
                {
                    zTable = Ticket_Data.List(startDate, endDate, Department, 0, 0, string.Empty);
                }
                else
                {
                    zTable = Ticket_Data.List(startDate, endDate, Department, Employee, 0, string.Empty);
                }                    
            }

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tableTicketTrade'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>Loại</th>");
            zSb.AppendLine("        <th>Ngày cọc</th>");
            zSb.AppendLine("        <th>Giá trị</th>");
            if (_Permitsion[3].ToInt() == 1)
                zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TicketKey"].ToString() + "' category=" + r["Category"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["NgayCoc"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["GiaTriHopDong"].ToDoubleString() + "</td>");
                    if (_Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TicketKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else { zSb.AppendLine("<tr><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Literal_Table.Text = zSb.ToString();
        }
        protected static string ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();
            if (Table.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TicketKey"].ToString() + "' category=" + r["Category"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["NgayCoc"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td>" + r["GiaTriHopDong"].ToDoubleString() + "</td>");
                    if (_Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TicketKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else { zSb.AppendLine("<tr><td colspan='5'>Chưa có dữ liệu</td></tr>"); }
            return zSb.ToString();
        }

        [WebMethod]
        public static string Delete(int Key)
        {
            Ticket_Info zInfo = new Ticket_Info();
            zInfo.TicketKey = Key;
            zInfo.Delete();
            return zInfo.Message;
        }
        [WebMethod]
        public static string Search(int Department, int Employee, string FromDate, string ToDate, int Project, string AssetID)
        {
            DataTable Table = Ticket_Data.List(Employee, Department, AssetID, Project, Tools.ConvertToDate(FromDate), Tools.ConvertToDate(ToDate));
            Item_Trade_Search ISearch = new Item_Trade_Search();
            ISearch.Department = Department;
            ISearch.Employee = Employee;
            ISearch.FromDate = FromDate;
            ISearch.ToDate = ToDate;
            ISearch.Project = Project;
            ISearch.AssetID = AssetID;
            HttpContext.Current.Session.Add("SearchTicket", ISearch);
            return ViewHtml(Table);
        }
        
    }
}