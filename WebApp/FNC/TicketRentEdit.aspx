﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="TicketRentEdit.aspx.cs" Inherits="WebApp.FNC.TicketRentEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .highlight {
            background-color: rgba(255, 0, 0, 0.26) !important;
        }

        input[type=text], textarea {
            background-color: transparent;
            height: 25px;
            color: #CCC;
            border: 0px !important;
            border-radius: 0px !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            -webkit-transition: none !important;
            -o-transition: none !important;
            transition: none !important;
        }

        .td5 {
            width: 5%;
        }

        .td10 {
            width: 10%;
        }

        .td15 {
            width: 15%;
        }

        .select2-container--default .select2-selection--single {
            border: 0px !important;
            border-radius: 0px !important;
        }

        .table > tbody > tr > td, .table > tbody > tr > th,
        .table > tfoot > tr > td, .table > tfoot > tr > th,
        .table > thead > tr > td, .table > thead > tr > th {
            vertical-align: middle !important;
            padding: 0px !important;
            padding-left: 4px !important;
            padding-top: 4px !important;
            border-top: 0px !important;
        }

        .hr:not(.hr-dotted) {
            background-color: black !important;
        }

        .hr {
            margin: 0px !important;
            height: 1px !important;
        }

        .table {
            margin-bottom: 0px;
            border: 1px solid black !important;
        }

        input:focus, select:focus, textarea:focus {
            outline: none !important;
            border-bottom: 1px dotted !important;
            box-shadow: 0 0 10px #719ECE !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="col-md-9" id="printable">
            <asp:Literal ID="LitMessage" runat="server"></asp:Literal>
            <div class="row" id="noidung">
                <table class="table">
                    <tr>
                        <td rowspan="3" style="width: 75px">
                            <img src="http://diaoc247.vn/wp-content/themes/central/images/logo.png" height="60" /></td>
                        <td class="center">
                            <b>CÔNG TY CỔ PHẦN ĐẦU TƯ ĐỊA ỐC 247</b>
                        </td>

                        <td class="center">
                            <b>PHIẾU BÁN HÀNG</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="center">
                            <b>12 Đường số 9, Phường Bình Trưng Đông, Quận 2, Tp.HCM</b>
                        </td>
                        <td class="center">
                            <b>Số:</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="center">
                            <b>Điện thoại:</b> (028) 6685 1818 &nbsp;&nbsp;&nbsp;&nbsp; <b>Website</b>  : www.diaoc247.vn
                        </td>
                        <td class="center">
                            <b>Ngày lập:<asp:Label ID="lblNgayLap" runat="server" Text=".."></asp:Label></b>
                        </td>
                    </tr>
                </table>
                <table class="table">
                    <tr>
                        <th class="td15">Dự án:</th>
                        <td>
                            <asp:DropDownList ID="DDLDuAn" runat="server" CssClass="form-control select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <th class="td15">Loại giao dịch:</th>
                        <td>Cho thuê</td>
                        <th class="td15">Ngày cọc:</th>
                        <td>
                            <asp:TextBox ID="txtNgayCoc" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>Phòng:</th>
                        <td>
                            <asp:DropDownList ID="DDLPhong" runat="server" CssClass="form-control select2">
                                <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <th>Trưởng phòng:</th>
                        <td>
                            <asp:DropDownList ID="DDLTruongPhong" runat="server" CssClass="form-control select2">
                                <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <th>Nhân viên:</th>
                        <td>
                            <asp:DropDownList ID="DDLNhanVien" runat="server" CssClass="form-control select2">
                                <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="hr hr-double hr1"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>Bên A (Bên cho thuê):</th>
                        <td colspan="3">
                            <asp:TextBox ID="txtHotenA" runat="server" CssClass="form-control" placeholder="Họ tên bên chuyển nhượng"></asp:TextBox>
                        </td>
                        <th>Ngày sinh:</th>
                        <td>
                            <asp:TextBox ID="txtNgaySinhA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>CMND/Passport:</th>
                        <td>
                            <asp:TextBox ID="txtCMNDA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Ngày cấp:</th>
                        <td>
                            <asp:TextBox ID="txtNgayCapA" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                        </td>
                        <th>Nơi cấp:</th>
                        <td>
                            <asp:TextBox ID="txtNoiCapA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>Địa chỉ liên hệ:</th>
                        <td colspan="3">
                            <asp:TextBox ID="txtDiaChiLienHeA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Điện thoại:</th>
                        <td>
                            <asp:TextBox ID="txtDienThoaiA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>Địa chỉ thường trú:</th>
                        <td colspan="3">
                            <asp:TextBox ID="txtDiaChiThuongTruA" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Email:</th>
                        <td>
                            <asp:TextBox ID="txtEmailA" runat="server" CssClass="form-control noneed" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="6">
                            <div class="hr hr-dotted hr1"></div>
                        </th>
                    </tr>
                    <tr>
                        <th>Bên B (Bên thuê):</th>
                        <td colspan="3">
                            <asp:TextBox ID="txtHotenB" runat="server" CssClass="form-control" placeholder="Họ tên bên nhận chuyển nhượng"></asp:TextBox>
                        </td>
                        <th>Ngày sinh:</th>
                        <td>
                            <asp:TextBox ID="txtNgaySinhB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>CMND/Passport:</th>
                        <td>
                            <asp:TextBox ID="txtCMNDB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Ngày cấp:</th>
                        <td>
                            <asp:TextBox ID="txtNgayCapB" runat="server" CssClass="form-control" placeholder="Chọn" role='datepicker'></asp:TextBox>
                        </td>
                        <th>Nơi cấp:</th>
                        <td>
                            <asp:TextBox ID="txtNoiCapB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>Địa chỉ liên hệ:</th>
                        <td colspan="3">
                            <asp:TextBox ID="txtDiaChiLienHeB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Điện thoại:</th>
                        <td>
                            <asp:TextBox ID="txtDienThoaiB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>Địa chỉ thường trú:</th>
                        <td colspan="3">
                            <asp:TextBox ID="txtDiaChiThuongTruB" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Email:</th>
                        <td>
                            <asp:TextBox ID="txtEmailB" runat="server" CssClass="form-control noneed" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="6">
                            <div class="hr hr-double hr1"></div>
                        </th>
                    </tr>
                    <tr>
                        <th>Mã căn:</th>
                        <td>
                            <asp:TextBox ID="txtMaCan" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Diện tích tim tường:</th>
                        <td>
                            <asp:TextBox ID="txtDienTichTimTuong" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                        </td>
                        <th>Diện tích thông thủy:</th>
                        <td>
                            <asp:TextBox ID="txtDienTichThongThuy" runat="server" CssClass="form-control" placeholder="Nhập số"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>Loại hình:</th>
                        <td>
                            <asp:DropDownList ID="DDLLoaiCanHo" runat="server" CssClass="form-control select2">
                                <asp:ListItem Value="0" Text="Chọn"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <th>Giá cho thuê:</th>
                        <td>
                            <asp:TextBox ID="txtGiaThue" runat="server" CssClass="form-control" placeholder="Nhập số" moneyinput></asp:TextBox>
                        </td>
                        <th colspan="2">Bằng chữ: <span id="giathuebangchu"></span></th>
                    </tr>
                    <tr>
                        <th>Bao gồm:</th>
                        <td colspan="5">
                            <asp:TextBox ID="txtBaoGom" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <b>Phương thức thanh toán:</b> Bằng tiền mặt hoặc chuyển khoản qua tài khoản ngân hàng theo lịch thanh toán như sau
                                    <span class="pull-right noprint">
                                        <button class="btn btn-minier btn-white" type="button" id="AddRow"><i class="ace-icon fa fa-plus-circle blue"></i>Thêm dòng</button>
                                    </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="hr hr-double hr1"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>Chủ tài khoản
                        </th>
                        <td>
                            <asp:TextBox ID="txtChuTaiKhoan" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Số tài khoản
                        </th>
                        <td>
                            <asp:TextBox ID="txtSoTaiKhoan" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                        <th>Ngân hàng</th>
                        <td>
                            <asp:TextBox ID="txtNganHang" runat="server" CssClass="form-control" placeholder="Nhập text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Literal ID="LitBangThanhToan" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <th>Các đợt tiếp theo:</th>
                        <td colspan="5">Bên B thanh toán bên A số tiền &nbsp;<asp:TextBox ID="txtSoTienThanhToan" runat="server" Style="width: 100px" placeholder="Nhập số" moneyinput></asp:TextBox>
                            <asp:TextBox ID="txtLan" runat="server" Style="width: 50px" placeholder="1,2 lần" moneyinput></asp:TextBox>&nbsp;lần,
                            từ ngày&nbsp;<asp:TextBox ID="txtTuNgay" runat="server" Style="width: 50px" MaxLength="2" moneyinput></asp:TextBox>
                            đến ngày&nbsp;<asp:TextBox ID="txtDenNgay" runat="server" Style="width: 50px" MaxLength="2" moneyinput></asp:TextBox>
                            đầu mỗi
                            <asp:TextBox ID="txtMoiThang" runat="server" Style="width: 50px" MaxLength="2" moneyinput></asp:TextBox>
                            tháng</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="hr hr-double hr1"></div>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="6">Thời điểm giao nhận, cho thuê</th>
                    </tr>
                    <tr>
                        <th colspan="2">Thời gian thuê:</th>
                        <td colspan="4">
                            <asp:TextBox ID="txtThoiGianThue" runat="server" placeholder="Nhập text" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Thơi gian bàn giao:</th>
                        <td colspan="4">
                            <asp:TextBox ID="txtThoiGianBanGiao" runat="server" placeholder="Chọn" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Thời gian tính tiền thuê:</th>
                        <td colspan="4">
                            <asp:TextBox ID="txtThoiGianTinhTienThue" runat="server" placeholder="Chọn" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div class="hr hr-double hr1"></div>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Thỏa thuận khác:</th>
                        <td colspan="4">
                            <asp:TextBox ID="txtThoaThuanKhac" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Nhập text" onkeyup="textAreaAdjust(this)" Style="overflow: hidden"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="right" class="col-xs-3 noprint">
            <div class="widget-box">
                <div class="widget-header widget-header-flat">
                    <h4 class="widget-title">...</h4>
                    <div class="widget-toolbar">
                        <a id="btndownload" href="#"><i class="ace-icon fa fa-download"></i>&nbsp;Download</a>
                        <a id="btnIn" href="javascript:window.print()"><i class="ace-icon fa fa-print"></i>&nbsp;In</a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <asp:Literal ID="LitFile" runat="server"></asp:Literal>
                        <div class="space-2"></div>
                        <asp:DropDownList ID="DDLFile" runat="server" CssClass="select2">
                            <asp:ListItem Value="001MauHopDongChoThue.docx" Text="Hợp đồng cho thuê"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_TicketKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_LoaiCan" runat="server" Value="loaihinh" />
    <asp:HiddenField ID="HID_BangThanhToan" runat="server" />
    <asp:HiddenField ID="HID_TienThanhToan" runat="server" />
    <asp:Button ID="btnWord" runat="server" CssClass="btn btn-xs btn-info" Text="Save" OnClick="btnWord_Click" Style="display: none; visibility: hidden" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="../SoSangChu.js"></script>
    <script>
        var isFormValid = true;
        $(document).ready(function () {
            $("#btnIn").click(function () {
                $(':input').removeAttr('placeholder');
            });
            $("#btndownload").click(function () {
                if ($("[id$=DDLFile]").val() == null) {
                    alert("Bạn phải chọn tập tin cần xử lý");
                    return false;
                }
                else {
                    $(".require").not(".noneed").each(function () {
                        isFormValid = true;
                        if ($.trim($(this).val()).length == 0) {
                            $(this).addClass("highlight");
                            isFormValid = false;
                            $(this).focus();
                            return false;
                        }
                        else {
                            $(this).removeClass("highlight");
                            isFormValid = true;
                        }
                    });
                    if (!isFormValid) {
                        alert("Bạn phải nhập đủ các thông tin !.");
                        return false;
                    }
                    else {
                        $("[id$=btnTriggerSave]").trigger("click");
                    }
                }
            });

            $("input[moneyinput]").number(true, 0);
            $(".select2").select2({ width: "100%" });
            $("[role='datepicker']").datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                startDate: '01/01/1900',
                todayHighlight: true
            });
            $("[id$=DDLLoaiCanHo]").on('change', function (e) {
                $("[id$=HID_LoaiCan]").val($("[id$=DDLLoaiCanHo] option:selected").text());
            });
            $("[id$=DDLDuAn]").on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/Ajax.aspx/GetCategoryAsset",
                        data: JSON.stringify({
                            "ProjectKey": valueSelected,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                        },
                        success: function (msg) {
                            var District = $("[id$=DDLLoaiCanHo]");
                            District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                            $.each(msg.d, function () {
                                var object = this;
                                if (object !== '') {
                                    District.append($("<option></option>").val(object.Value).html(object.Text));
                                }
                            });
                        },
                        complete: function () {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });
            $("[id$=btnWord]").click(function () {            
                var total = $('#tblBangThanhToan tbody tr').length;
                var row = "";
                var num = 0;
                $('#tblBangThanhToan tbody tr').each(function () {
                    num++;
                    if (num < total)
                        row += ""
                            + $(this).find('td:eq(0)').text() + ":"
                            + $(this).find('td:eq(1)').text() + ":"
                            + $(this).find('td:eq(2)').text() + ":"
                            + $(this).find('td:eq(3)').text() + ":"
                            + $(this).find('td:eq(4)').text() + ":"
                            + $(this).find('td:eq(5)').text() + ";";
                    else
                        row += ""
                        + $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text();
                });
                $("[id$=HID_BangThanhToan]").val(row);
                $('#tblBangThanhToan tfoot tr').each(function () {
                    row = ""
                        + $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text();
                });
                $("[id$=HID_TienThanhToan]").val(row);
            });
            $("[id$=txtGiaThue]").on("blur", function () {
                var value = parseFloat($(this).val().replace(/,/g, ''));
                $("#giathuebangchu").text(DOCSO.doc(value));
            });

            $("#AddRow").click(function () {
                var num = $('#tblBangThanhToan tr').length;
                var row = '<tr id=' + num + '><td tabindex=1>Đặt cọc đợt ' + (num++) + '</td><td tabindex=1>&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1>1</td><td tabindex=1>tháng thuê nhà</td><td tabindex=1><span btn="btnDelete" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i>Xóa</span></td></tr>';
                $('#tblBangThanhToan tbody').append(row);
            });
            $("#tblBangThanhToan").on("click", "[btn='btnDelete']", function (e) {
                var Key = $(this).closest('tr').attr('id');
                $("tr[id=" + Key + "]").remove();
            });
            $("#tblBangThanhToan").editableTableWidget()
                .focus(function () {
                    $(this).select();
                });
        });
        function textAreaAdjust(o) {
            o.style.height = "1px";
            o.style.height = (25 + o.scrollHeight) + "px";
        }
    </script>
</asp:Content>
