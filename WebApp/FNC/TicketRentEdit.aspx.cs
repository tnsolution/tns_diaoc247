﻿using FindAndReplace;
using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.IO;
using System.Web;

namespace WebApp.FNC
{
    public partial class TicketRentEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDLNhanVien, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDLTruongPhong, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDLPhong, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDLDuAn, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);
                Tools.DropDown_DDL(DDLLoaiCanHo, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);

                lblNgayLap.Text = DateTime.Now.ToString("dd/MM/yyyy");
                LitFile.Text = "Sau khi điền đủ thông tin, chọn file mẫu cần xử lý, click tải file để tải file word đã xử lý !.";

                LoadInfo();
            }
        }

        protected void btnWord_Click(object sender, EventArgs e)
        {
            SaveInfo();
            GenerateAll();
        }

        void GenerateAll()
        {
            string baogom = "";
            if (txtBaoGom.Text.Trim().Length == 0)
                baogom = "..";
            else
                baogom = txtBaoGom.Text.Trim();

            Project_Info zProject = new Project_Info(DDLDuAn.SelectedValue.ToInt());

            #region [lịch thanh toán]
          
            string zList = "";
            string[] row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                for (int i = 0; i < row.Length; i++)
                {
                    string item = row[i];
                    string[] col = item.Split(':');

                    double money = col[2].Trim().ToDouble();
                    string data = "";
                    data = col[0].Trim() + " ngày " + col[1].Trim()
                        + " số tiền " + money.ToString("n0")
                        + " bằng chữ " + Utils.NumberToWordsVN(money)
                        + " tương đương " + col[4].Trim()
                        + " " + col[5].Trim();
                    zList += data + ". ";
                }
            }
            #endregion

            #region tien thanh toan
            string ngaythuethangdau = "..";
            string sothangthue = "..";
            row = HID_TienThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                for (int i = 0; i < row.Length; i++)
                {
                    string[] coladdin = row[i].Split(':');
                    double moneyaddin = coladdin[2].Trim().ToDouble();
                    string addin = coladdin[0].Trim() + " ngày " + coladdin[1].Trim()
                               + " số tiền " + moneyaddin.ToString("n0")
                               + " bằng chữ " + Utils.ConvertDecimalToStringVN(moneyaddin)
                               + " tương đương " + coladdin[4].Trim()
                               + " " + coladdin[5].Trim();
                    zList += addin + ". ";
                    ngaythuethangdau = coladdin[1].Trim();
                    sothangthue = coladdin[4].Trim();
                }
            }

            #endregion

            #region [xử lý download]
            string FileName = "001MauHopDongChoThue.docx"; //DDLFile.SelectedValue;

            string PathIn = @"\Upload\File\HD\" + FileName;
            string PathOut = @"\Upload\File\HD\" + FileName.Split('.')[0] + "_Output.docx";

            string InputFile = HttpContext.Current.Server.MapPath(@"\") + PathIn;
            string OutputFile = HttpContext.Current.Server.MapPath(@"\") + PathOut;

            File.Copy(InputFile, OutputFile, true);

            var stream = new MemoryStream();
            using (var fileStream = File.OpenRead(OutputFile))
            {
                fileStream.CopyTo(stream);
                using (var flatDocument = new FlatDocument(stream))
                {
                    #region [tieu de]
                    flatDocument.FindAndReplace("#txt_ngaycoc", Tools.ConvertToDate(txtNgayCoc.Text).Day.ToString());
                    flatDocument.FindAndReplace("#txt_thangcoc", Tools.ConvertToDate(txtNgayCoc.Text).Month.ToString());
                    flatDocument.FindAndReplace("#txt_nam", Tools.ConvertToDate(txtNgayCoc.Text).Year.ToString());
                    flatDocument.FindAndReplace("#txt_duan", DDLDuAn.SelectedItem.Text);
                    flatDocument.FindAndReplace("#txt_diachiduan", zProject.Address);
                    #endregion

                    #region [--A--]
                    flatDocument.FindAndReplace("#txt_hotenA", txtHotenA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_cmndA", txtCMNDA.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycapA", txtNgayCapA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_noicapA", txtNoiCapA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachithuongtruA", txtDiaChiThuongTruA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachilienlacA", txtDiaChiLienHeA.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dienthoaiA", txtDienThoaiA.Text.Trim());
                    #endregion

                    #region [--B--]
                    flatDocument.FindAndReplace("#txt_hotenB", txtHotenB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_cmndB", txtCMNDB.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaycapB", txtNgayCapB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_noicapB", txtNoiCapB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachithuongtruB", txtDiaChiThuongTruB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_diachilienlacB", txtDiaChiLienHeB.Text.Trim());
                    flatDocument.FindAndReplace("#txt_dienthoaiB", txtDienThoaiB.Text.Trim());
                    #endregion

                    flatDocument.FindAndReplace("#txt_nguoilap", DDLTruongPhong.SelectedItem.Text);
                    flatDocument.FindAndReplace("#txt_chutaikhoan", txtChuTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_sotaikhoan", txtSoTaiKhoan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_nganhang", txtNganHang.Text.Trim());

                    flatDocument.FindAndReplace("#txt_macan", txtMaCan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_Uloaican", HID_LoaiCan.Value.Trim().ToUpper());
                    flatDocument.FindAndReplace("#txt_loaican", HID_LoaiCan.Value.Trim());
                    flatDocument.FindAndReplace("#txt_dientich", txtDienTichTimTuong.Text.Trim());

                    flatDocument.FindAndReplace("#txt_sotienthuebangchu", Utils.NumberToWordsVN(txtGiaThue.Text.ToDouble()).ToUpperInvariant());
                    flatDocument.FindAndReplace("#txt_sotienthuethangdau", txtSoTienThanhToan.Text.Trim().ToDoubleString());

                    flatDocument.FindAndReplace("#txt_lan", txtLan.Text.Trim());
                    flatDocument.FindAndReplace("#txt_sotienthue", txtGiaThue.Text.Trim().ToDoubleString());
                    flatDocument.FindAndReplace("#txt_baobom", baogom);

                    flatDocument.FindAndReplace("#tblbangthanhtoan", zList);
                    flatDocument.FindAndReplace("#dt_ngaythuethangdau", ngaythuethangdau);

                    flatDocument.FindAndReplace("#txt_sothangthue", sothangthue);
                    flatDocument.FindAndReplace("#txt_tungay", txtTuNgay.Text.Trim());
                    flatDocument.FindAndReplace("#txt_denngay", txtDenNgay.Text.Trim());
                    flatDocument.FindAndReplace("#txt_thang", txtMoiThang.Text.Trim());

                    flatDocument.FindAndReplace("#txt_thoigianthue", txtThoiGianThue.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaybangiao", txtThoiGianBanGiao.Text.Trim());
                    flatDocument.FindAndReplace("#dt_ngaytinhtien", txtThoiGianTinhTienThue.Text.Trim());
                }
            }

            File.WriteAllBytes(OutputFile, stream.ToArray());
            FileInfo file = new FileInfo(OutputFile);
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();
            }
            else
            {
                Response.Write("This file does not exist.");
            }
            #endregion
        }
        void SaveInfo()
        {
            int Key = HID_TicketKey.Value.ToInt();

            #region [MyRegion]
            Ticket_Info zInfo = new Ticket_Info(Key);
            zInfo.DuAn = DDLDuAn.SelectedItem.Text;
            zInfo.CodeDuAn = DDLDuAn.SelectedValue;
            zInfo.Category = 1;
            zInfo.NgayCoc = Tools.ConvertToDate(txtNgayCoc.Text.Trim());

            zInfo.DepartmentKey = DDLPhong.SelectedValue;
            zInfo.Department = DDLPhong.SelectedItem.Text;
            zInfo.EmployeeKey = DDLNhanVien.SelectedValue;
            zInfo.EmployeeName = DDLNhanVien.SelectedItem.Text;
            zInfo.ManagerKey = DDLTruongPhong.SelectedValue;
            zInfo.ManagerName = DDLTruongPhong.SelectedItem.Text;

            zInfo.HotenA = txtHotenA.Text.Trim();
            zInfo.NgaySinhA = txtNgaySinhA.Text.Trim();
            zInfo.CMNDA = txtCMNDA.Text.Trim();
            zInfo.NgayCapA = txtNgayCapA.Text.Trim();
            zInfo.NoiCapA = txtNoiCapA.Text.Trim();
            zInfo.DiaChiLienHeA = txtDiaChiLienHeA.Text.Trim();
            zInfo.DiaChiLienLacA = txtDiaChiThuongTruA.Text.Trim();
            zInfo.DienThoaiA = txtDienThoaiA.Text.Trim();
            zInfo.EmailA = txtEmailA.Text.Trim();

            zInfo.HotenB = txtHotenB.Text.Trim();
            zInfo.NgaySinhB = txtNgaySinhB.Text.Trim();
            zInfo.CMNDB = txtCMNDB.Text.Trim();
            zInfo.NgayCapB = txtNgayCapB.Text.Trim();
            zInfo.NoiCapB = txtNoiCapB.Text.Trim();
            zInfo.DiaChiLienHeB = txtDiaChiLienHeB.Text.Trim();
            zInfo.DiaChiLienLacB = txtDiaChiThuongTruB.Text.Trim();
            zInfo.DienThoaiB = txtDienThoaiB.Text.Trim();
            zInfo.EmailB = txtEmailB.Text.Trim();

            zInfo.MaCan = txtMaCan.Text.Trim();
            zInfo.LoaiCan = DDLLoaiCanHo.SelectedItem.Text;
            zInfo.DienTichTimTuong = txtDienTichTimTuong.Text;
            zInfo.DienTichThongThuy = txtDienTichThongThuy.Text;
            zInfo.GiaChoThue = txtGiaThue.Text.Trim();
            zInfo.GiaChoThueBangChu = Utils.ConvertDecimalToStringVN(txtGiaThue.Text.ToDouble());
            zInfo.ThueBaoGom = txtBaoGom.Text.Trim();

            zInfo.ChuTaiKhoan = txtChuTaiKhoan.Text.Trim();
            zInfo.SoTaiKhoan = txtSoTaiKhoan.Text.Trim();
            zInfo.NganHang = txtNganHang.Text.Trim();

            zInfo.BThanhToanThueA = txtSoTienThanhToan.Text;
            zInfo.SoLan = txtLan.Text;
            zInfo.TuNgay = txtTuNgay.Text;
            zInfo.DenNgay = txtDenNgay.Text;
            zInfo.DauMoi = txtMoiThang.Text;

            zInfo.ThoiGianThue = txtThoiGianThue.Text;
            zInfo.ThoiGianBanGiao = txtThoiGianBanGiao.Text;
            zInfo.ThoiGianTinhTien = txtThoiGianTinhTienThue.Text;

            zInfo.ThoaThuanThueKhac = txtThoaThuanKhac.Text.Trim();

            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];

            zInfo.Save();
            #endregion

            HID_TicketKey.Value = zInfo.TicketKey.ToString();
            if (zInfo.Message != string.Empty)
            {
                LitMessage.Text = "Lỗi liên hệ Admin " + zInfo.Message;
                return;
            }

            string[] row = HID_BangThanhToan.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (row.Length > 0)
            {
                string SQL = " DELETE FNC_TicketPayment WHERE TicketKey =" + zInfo.TicketKey;
                foreach (string item in row)
                {
                    //string data = "";
                    string[] col = item.Split(':');
                    double money = col[2].Trim().ToDouble();
                    //data = col[0] + " ngày " + col[1] + " số tiền " + money.ToString("n0") + " bằng chữ " + Utils.ConvertDecimalToString(money) + col[3];

                    SQL += @"
INSERT INTO FNC_TicketPayment(Title ,TimePayment, Description ,Amount, AmountInWord,Number ,CreatedDate ,ModifiedDate ,TicketKey )
VALUES (N'" + col[0] + "' ,N'" + col[1] + "',N'" + col[5] + "' ,N'" + money.ToString("n0") + "',N'" + Utils.NumberToWordsVN(money) + "',N'" + col[4] + "',GETDATE() ,GETDATE() ," + zInfo.TicketKey + " ) ";
                }

                string Message = CustomInsert.Exe(SQL).Message;
                if (Message != string.Empty)
                    LitMessage.Text = "Lỗi liên hệ Admin " + Message;
            }
        }
        void LoadInfo()
        {
            if (Request["ID"] != null)
            {
                #region [MyRegion]
                HID_TicketKey.Value = Request["ID"];
                int Key = HID_TicketKey.Value.ToInt();
                Ticket_Info zInfo = new Ticket_Info(Key);
                DDLDuAn.SelectedValue = zInfo.CodeDuAn;
                txtNgayCoc.Text = zInfo.NgayCoc.ToString("dd/MM/yyyy");
                DDLPhong.SelectedValue = zInfo.DepartmentKey.ToString();
                DDLNhanVien.SelectedValue = zInfo.EmployeeKey.ToString();
                DDLTruongPhong.SelectedValue = zInfo.ManagerKey.ToString();
                txtHotenA.Text = zInfo.HotenA;
                txtNgaySinhA.Text = zInfo.NgaySinhA;
                txtCMNDA.Text = zInfo.CMNDA;
                txtNgayCapA.Text = zInfo.NgayCapA;
                txtNoiCapA.Text = zInfo.NoiCapA;
                txtDiaChiLienHeA.Text = zInfo.DiaChiLienHeA;
                txtDiaChiThuongTruA.Text = zInfo.DiaChiLienLacA;
                txtDienThoaiA.Text = zInfo.DienThoaiA;
                txtEmailA.Text = zInfo.EmailA;
                txtHotenB.Text = zInfo.HotenB;
                txtNgaySinhB.Text = zInfo.NgaySinhB;
                txtCMNDB.Text = zInfo.CMNDB;
                txtNgayCapB.Text = zInfo.NgayCapB;
                txtNoiCapB.Text = zInfo.NoiCapB;
                txtDiaChiLienHeB.Text = zInfo.DiaChiLienHeB;
                txtDiaChiThuongTruB.Text = zInfo.DiaChiLienLacB;
                txtDienThoaiB.Text = zInfo.DienThoaiB;
                txtEmailB.Text = zInfo.EmailB;
                txtMaCan.Text = zInfo.MaCan;
                DDLLoaiCanHo.Text = zInfo.LoaiCan;
                txtDienTichTimTuong.Text = zInfo.DienTichTimTuong;
                txtDienTichThongThuy.Text = zInfo.DienTichThongThuy;
                txtGiaThue.Text = zInfo.GiaChoThue;
                txtBaoGom.Text = zInfo.ThueBaoGom;
                txtChuTaiKhoan.Text = zInfo.ChuTaiKhoan;
                txtSoTaiKhoan.Text = zInfo.SoTaiKhoan;
                txtNganHang.Text = zInfo.NganHang;
                txtSoTienThanhToan.Text = zInfo.BThanhToanThueA;
                txtLan.Text = zInfo.SoLan;
                txtTuNgay.Text = zInfo.TuNgay;
                txtDenNgay.Text = zInfo.DenNgay;
                txtMoiThang.Text = zInfo.DauMoi;
                txtThoiGianThue.Text = zInfo.ThoiGianThue;
                txtThoiGianBanGiao.Text = zInfo.ThoiGianBanGiao;
                txtThoiGianTinhTienThue.Text = zInfo.ThoiGianTinhTien;
                txtThoaThuanKhac.Text = zInfo.ThoaThuanThueKhac;
                txtThoaThuanKhac.Text = zInfo.ThoaThuanThueKhac;
                #endregion
            }

            DataTable zTable = TicketPayment_Data.List(HID_TicketKey.Value.ToInt());
            if (zTable.Rows.Count > 0)
            {
                string table = @"<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Đợt</th>
                                        <th class='td10'>Ngày</th>
                                        <th class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th colspan='2'>Ghi chú (tương đương)</th>
                                        <th class='noprint'>#</th>
                                    </tr>
                                </thead>
                                <tbody>";
                foreach (DataRow r in zTable.Rows)
                {
                    table += @"<tr id='" + r["AutoKey"].ToString() + "'><td>" + r["Title"].ToString() + "</td><td tabindex = '1'>" + r["TimePayment"].ToString() + "</td><td tabindex='1'>" + r["Amount"].ToDoubleString() + "</td><td tabindex = '1'>" + r["AmountInWord"].ToString() + "</td><td tabindex='1'>" + r["Number"].ToString() + "</td><td tabindex='1' class='edit-disabled'>" + r["Description"].ToString() + "</td><td class='edit-disabled noprint'><span btn='btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td></tr> ";
                }

                table += "</tbody></table>";
                LitBangThanhToan.Text = table;
            }
            else
            {
                LitBangThanhToan.Text = @"
<table class='table table-bordered' id='tblBangThanhToan'>
                                <thead>
                                    <tr>
                                        <th class='td10'>Đợt</th>
                                        <th class='td10'>Ngày</th>
                                        <th class='td15'>Số tiền</th>
                                        <th>Bằng chữ</th>
                                        <th colspan='2'>Ghi chú (tương đương)</th>
                                        <th class='noprint'>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id=1>
                                        <td>Đợt 1</td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint'><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td>
                                    </tr>
                                    <tr id=2>
                                        <td>Đợt 2</td>
                                        <td tabindex='1' ></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'></td>
                                        <td tabindex='1'>1</td>
                                        <td tabindex='1' class='edit-disabled'>tháng thuê nhà</td>
                                        <td class='edit-disabled noprint'><span btn = 'btnDelete' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i>Xóa</span></td>
                                    </tr>
                                </tbody>
                            </table>";
            }
        }
    }
}