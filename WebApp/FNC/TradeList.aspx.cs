﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.FNC
{
    public partial class TradeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["Type"] != null)
                    HID_TradeType.Value = Request["Type"];

                InitView();
            }
        }

        [WebMethod(EnableSession = true)]
        public static string Search(int Department, int Employee, string FromDate, string ToDate, int Project, int CategoryTrade, int TradeType, string AssetID)
        {
            DataTable Table = Trade_Data.Get(Department, Employee, TradeType, Tools.ConvertToDate(FromDate), Tools.ConvertToDate(ToDate), Project, CategoryTrade, AssetID);
            StringBuilder zSb = new StringBuilder();

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    if (r["IsApproved"].ToInt() == 0 && _Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TransactionKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    else
                        zSb.AppendLine("               <td class='notclick'></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td>");
                zSb.AppendLine("            <td colspan='11'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            Item_Trade_Search ISearch = new Item_Trade_Search();
            ISearch.Department = Department;
            ISearch.Employee = Employee;
            ISearch.FromDate = FromDate;
            ISearch.ToDate = ToDate;
            ISearch.Project = Project;
            ISearch.CategoryTrade = CategoryTrade;
            ISearch.TradeType = TradeType;
            ISearch.AssetID = AssetID;
            HttpContext.Current.Session.Add("SearchTrade", ISearch);

            return zSb.ToString();
        }
        protected void ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Ngày lập GD</th>");
            zSb.AppendLine("        <th>Ngày ký hợp đồng</th>");
            zSb.AppendLine("        <th>Tên dự án</th>");
            zSb.AppendLine("        <th>Mã căn hộ</th>");
            zSb.AppendLine("        <th>Loại căn hộ</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Giao dịch</th>");
            zSb.AppendLine("        <th>Doanh thu</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (Table.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("               <td>" + no++ + "</td>");
                    if (r["TransactionDate"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    if (r["DateContract"] != DBNull.Value)
                        zSb.AppendLine("               <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                    else
                        zSb.AppendLine("               <td></td>");

                    zSb.AppendLine("               <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["AssetCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("               <td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("               <td>" + r["EmployeeName"].ToString() + "</td>");
                    if (r["IsApproved"].ToInt() == 0 && _Permitsion[3].ToInt() == 1)
                        zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' onclick='deleteInput(" + r["TransactionKey"].ToString() + ")' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    else
                        zSb.AppendLine("               <td></td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("            <td></td>");
                zSb.AppendLine("            <td colspan='11'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int TradeType = HID_TradeType.Value.ToInt();
            DDL_Status.SelectedValue = TradeType.ToString();
            DataTable zTable = new DataTable();
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (UnitLevel < 7)
            {
                zTable = Trade_Data.Get(FromDate, ToDate, ViewDepartment, 0, TradeType);
            }
            else
            {
                zTable = Trade_Data.Get(FromDate, ToDate, ViewDepartment, Employee, TradeType);
            }

            lbltime.Text = " Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy");

            ViewHtml(zTable);
        }

        [WebMethod]
        public static string Delete(int Key)
        {
            Trade_Info zInfo = new Trade_Info(Key);
            zInfo.Key = Key;
            zInfo.Delete();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = zInfo.DepartmentKey;
            CancelKPI(Key, zInfo.EmployeeKey, zInfo.DepartmentKey, "Xóa giao dich", 0);
            return zInfo.Message;
        }

        public static string CancelKPI(int TradeKey, int EmployeeKey, int DepartmentKey, string Description, int Category)
        {
            string MessageKPI = Helper.KPI(TradeKey, EmployeeKey, DepartmentKey, DateTime.Now, Description, 0, Category, 4);//delete kpi giao dichsz 
            return MessageKPI;
        }

        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        protected void InitView()
        {
            int TradeType = HID_TradeType.Value.ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            string RolePage = "FNC";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            if (UnitLevel < 7)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT DISTINCT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey IN (" + ViewDepartment + ") ORDER BY ProjectName", false);
                DDL_Employee.Visible = true;
                DDL_Employee2.Visible = true;
                DDL_Department.Visible = true;
            }
            else
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT DISTINCT A.AssetKey, dbo.FNC_GetProjectName(A.AssetKey) AS ProjectName FROM PUL_SharePermition A WHERE ObjectTable = 'Project' AND EmployeeKey IN (" + ViewDepartment + ") ORDER BY ProjectName", false);
                DDL_Employee.SelectedValue = Employee.ToString();
                DDL_Department.Visible = false;
                DDL_Employee.Visible = false;
                DDL_Employee2.Visible = true;
            }

            if (TradeType == 1)
            {
                Lit_TitlePage.Text = "Các giao dịch chưa duyệt";
            }
            if (TradeType == 2)
            {
                Lit_TitlePage.Text = "Các giao dịch chưa thanh toán";
            }
            if (TradeType == 3 ||
                TradeType == 0)
            {
                Lit_TitlePage.Text = "Các giao dịch";
            }

            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
            HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            lbltime.Text = " Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy");
            DDL_Status.SelectedValue = TradeType.ToString();
            DataTable zTable = new DataTable();

            if (UnitLevel < 7)
            {
                zTable = Trade_Data.Get(FromDate, ToDate, ViewDepartment, 0, TradeType);
            }
            else
            {
                zTable = Trade_Data.Get(FromDate, ToDate, ViewDepartment, Employee, TradeType);
            }

            ViewHtml(zTable);
        }
    }
}