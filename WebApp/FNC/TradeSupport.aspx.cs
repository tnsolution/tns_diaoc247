﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.FNC
{
    public partial class TradeSupport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
                htmlSupport();
            }
        }

        void LoadData()
        {
            DateTime startDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime endDate = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

            //DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            //DateTime endDate = startDate.AddMonths(1).AddDays(-1);
            //endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

            DataTable zTable = Trade_Data.Get(0, 0, 3, startDate, endDate);
            html(zTable);
        }

        void html(DataTable TableIn)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Mã căn hộ</th>");
            zSb.AppendLine("        <th>Doanh thu</th>");
            zSb.AppendLine("        <th>Giao dịch</th>");
            zSb.AppendLine("        <th>Nhân viên</th>");
            zSb.AppendLine("        <th>Trưởng phòng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (TableIn.Rows.Count > 0)
            {
                int no = 1;
                foreach (DataRow r in TableIn.Rows)
                {
                    zSb.AppendLine("<tr id='" + r["TransactionKey"].ToString() + "'>");
                    zSb.AppendLine("<td>" + no++ + "</td>");
                    zSb.AppendLine("<td>" + r["AssetID"].ToString() + "</td>");
                    zSb.AppendLine("<td class='giadien'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                    zSb.AppendLine("<td>" + r["TradeCategory"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("<td>" + r["ManagerName"].ToString() + "</td>");
                    zSb.AppendLine("</tr>");
                }
                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
            }
            LitTableTrade.Text = zSb.ToString();
        }
        void htmlSupport()
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData' style='cursor:pointer'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Loại phí</th>");
            zSb.AppendLine("        <th>Số tiền</th>");
            zSb.AppendLine("        <th>Thụ hưởng</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            DataTable zTable = Trade_Data.GetSupport(277);
            int i = 1;
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>" + (i++) + "</td>");
                zSb.AppendLine("                <td>" + r["CategoryName"].ToString() + "</td>");
                zSb.AppendLine("                <td>" + r["Money"].ToDoubleString() + "</td>");
                zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("            </tr>");
            }
            zSb.AppendLine("      </tbody>");
            zSb.AppendLine("</table>");

            LitTableSupport.Text = zSb.ToString();
        }
    }
}