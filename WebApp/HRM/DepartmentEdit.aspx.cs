﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Web.Services;

namespace WebApp.HRM
{
    public partial class DepartmentEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_DepartmentKey.Value = Request["ID"];
                Tools.DropDown_DDL(DDL_Spec, "SELECT SpecKey,SpecName FROM HRM_Specified", true);
                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_DepartmentKey.Value != string.Empty && 
                HID_DepartmentKey.Value != "0")
            {
                Departments_Info zInfo = new Departments_Info(HID_DepartmentKey.Value.ToInt());
                txt_DepartmentName.Value = zInfo.DepartmentName;
                txt_Description.Value = zInfo.Description;
                txt_Authority.Value = zInfo.Authority;
                DDL_Spec.SelectedValue = zInfo.SpecKey.ToString();
                Lit_TitlePage.Text = "Sửa thông tin " + zInfo.DepartmentName;
            }
        }

        [WebMethod]
        public static ItemReturn SaveDepartment(int DepartmentKey, string DepartmentName, string Authority, string Description, string Rank)
        {
            ItemReturn zResult = new ItemReturn();
            Departments_Info zInfo = new Departments_Info(DepartmentKey);
            zInfo.DepartmentName = DepartmentName;
            zInfo.Description = Description;
            zInfo.Authority = Authority;
            zInfo.Rank = Rank.ToInt();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn DeleteDepartment(int DepartmentKey)
        {
            ItemReturn zResult = new ItemReturn();
            Departments_Info zInfo = new Departments_Info();
            zInfo.Key = DepartmentKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}