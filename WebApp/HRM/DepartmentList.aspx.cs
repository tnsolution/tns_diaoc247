﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class DepartmentList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                LoaData();
            }
        }

        void LoaData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Departments_Data.List();

            if (HID_Type.Value == "1")
            {
                zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
                zSb.AppendLine("   <thead>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th>STT</th>");
                zSb.AppendLine("        <th>Sàn/Phòng</th>");
                zSb.AppendLine("        <th>Ủy quyền</th>");
                zSb.AppendLine("        <th>Ghi chú</th>");
                zSb.AppendLine("        <th>Thứ tự sắp xếp</th>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");

                if (zTable.Rows.Count > 0)
                {
                    int i = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr id='" + r["DepartmentKey"].ToString() + "'>");
                        zSb.AppendLine("                <td>" + (i++) + "</td>");
                        zSb.AppendLine("                <td>" + r["DepartmentName"].ToString() + "</td>");
                        zSb.AppendLine("                <td>" + r["Authority"].ToString() + "</td>");
                        zSb.AppendLine("                <td>" + r["Description"].ToString() + "</td>");
                        zSb.AppendLine("                <td><span class='editable editable-click'>" + r["Rank"].ToString() + "</span></td>");
                        zSb.AppendLine("            </tr>");
                    }
                }

                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
            }
            else
            {
                zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
                zSb.AppendLine("   <thead>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <th>STT</th>");
                zSb.AppendLine("        <th>Sàn</th>");
                zSb.AppendLine("        <th>Ghi chú</th>");               
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("   </thead>");
                zSb.AppendLine("        <tbody>");

                if (zTable.Rows.Count > 0)
                {
                    int i = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        zSb.AppendLine("            <tr id='" + r["SpecKey"].ToString() + "'>");
                        zSb.AppendLine("                <td>" + (i++) + "</td>");
                        zSb.AppendLine("                <td><span class='editable editable-click'>" + r["SpecName"].ToString() + "</span></td>");
                        zSb.AppendLine("                <td><span class='editable editable-click'>" + r["Description"].ToString() + "</span></td>");                        
                        zSb.AppendLine("            </tr>");
                    }
                }

                zSb.AppendLine("        </tbody>");
                zSb.AppendLine("</table>");
            }
            Literal_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemReturn ChangeRank(int DepartmentKey, int Rank)
        {
            ItemReturn zResult = new ItemReturn();
            Departments_Info zInfo = new Departments_Info(DepartmentKey);
            zInfo.Key = DepartmentKey;
            zInfo.Rank = Rank;
            zInfo.Update();
            zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}