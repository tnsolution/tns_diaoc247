﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="DepartmentRole.aspx.cs" Inherits="WebApp.HRM.DepartmentRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết phòng
                <asp:Literal ID="txt_DepartmentName" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <span class="tools pull-right">
                        <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                        <button type="button" class="btn btn-white btn-default btn-bold" onclick="window.history.go(-1)">
                            <i class="ace-icon fa fa-reply blue"></i>
                            Trờ về
                        </button>
                    </span>
                </span>
            </h1>
        </div>

        <div class="row">
            <div class="col-xs-9">
                <div class="profile-user-info profile-user-info-striped">
                    <div class="profile-info-row">
                        <div class="profile-info-name">Ghi chú</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_Description" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name">Thứ tự sắp xếp</div>
                        <div class="profile-info-value">
                            <asp:Literal ID="txt_Rank" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_DepartmentKey" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(function () {
            $("#btnEdit").click(function () {
                window.location = "DepartmentEdit.aspx?ID=" + Page.getUrlParameter("ID");
            });
        });
    </script>
</asp:Content>
