﻿using Lib.Config;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.HRM
{
    public partial class EmployeeEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Tools.DropDown_DDL(DDL_Spec, "SELECT SpecKey, SpecName FROM HRM_Specified", false);
                Tools.DropDown_DDL(DDL_Unit, "SELECT AutoKey, Unit + ' | ' + Description FROM SYS_Unit ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Position, "SELECT PositionKey, Position FROM HRM_Positions ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Manager, "SELECT EmployeeKey, LastName  + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND PositionKey <=4", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);
                if (Request["ID"] != null)
                {
                    HID_EmployeeKey.Value = Request["ID"];
                }

                CheckRoles();
                Load_Employee();
            }
        }
        void Load_Employee()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_EmployeeKey.Value != string.Empty &&
                HID_EmployeeKey.Value != "0")
            {
                Employees_Info zInfo = new Employees_Info(HID_EmployeeKey.Value.ToInt());
                DDL_Position.SelectedValue = zInfo.PositionKey.ToString();
                DDL_Department.SelectedValue = zInfo.DepartmentKey.ToString();
                DDL_Manager.SelectedValue = zInfo.ManagerKey.ToString();
                DDL_IsWorking.SelectedValue = zInfo.IsWorking.ToString();
                DDL_Gender.SelectedValue = zInfo.Gender.ToString();
                DDL_Unit.SelectedValue = zInfo.UnitLevel;
                DDL_Spec.SelectedValue = zInfo.SpecKey.ToString();

                txt_Class.Value = zInfo.Class;
                txt_FirstName.Value = zInfo.FirstName;
                txt_LastName.Value = zInfo.LastName;
                txt_IDCard.Value = zInfo.IDCard;
                txt_IDPlace.Value = zInfo.IDPlace;

                if (zInfo.Birthday != DateTime.MinValue)
                {
                    txt_Birthday.Value = zInfo.Birthday.ToString("dd/MM/yyyy");
                }

                if (zInfo.IDDate != DateTime.MinValue)
                {
                    txt_IDDate.Value = zInfo.IDDate.ToString("dd/MM/yyyy");
                }

                if (zInfo.DateEnd != DateTime.MinValue)
                {
                    txt_DateEnd.Value = zInfo.DateEnd.ToString("dd/MM/yyyy");
                }

                if (zInfo.DateStart != DateTime.MinValue)
                {
                    txt_DateStart.Value = zInfo.DateStart.ToString("dd/MM/yyyy");
                }

                if (zInfo.DateOffAllow != DateTime.MinValue)
                {
                    txtAllowOff.Value = zInfo.DateOffAllow.ToString("dd/MM/yyyy");
                }

                txt_Address1.Value = zInfo.Address1;
                txt_Address2.Value = zInfo.Address2;
                txt_Email1.Value = zInfo.Email1;
                txt_CarNumber.Value = zInfo.CarNumber;
                txt_Phone1.Value = zInfo.Phone1;

                txtBasicSalary.Value = zInfo.BasicSalary.ToString("n0");
                txtSupportFee.Value = zInfo.SupportFee.ToString("n0");
                txtInsurance.Value = zInfo.Insurance.ToString("n0");
                txtExtraFee.Value = zInfo.ExtraFee.ToString("n0");

                txtBasicSalary1.Value = zInfo.BasicSalary1.ToString("n0");
                txtSupportFee1.Value = zInfo.SupportFee1.ToString("n0");
                txtInsurance1.Value = zInfo.Insurance1.ToString("n0");
                txtExtraFee1.Value = zInfo.ExtraFee1.ToString("n0");
                if (zInfo.CheckSaleryTable == true)
                    RadioCheckSaleryTable1.Checked = true;
                else
                    RadioCheckSaleryTable2.Checked = true;


                if (zInfo.DateOffAllow.ToString().Length > 0)
                {
                    LitButton.Text = @" <button type='button' class='btn btn-white btn-default btn-bold' id='ActiveOff'>Đã bắt đầu tính phép năm</button>";
                }
                else
                {
                    LitButton.Text = @" <button type='button' class='btn btn-white btn-default btn-bold' id='ActiveOff'>Kích hoạt phép năm</button>";
                }

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();

                Lit_TitlePage.Text = "Sửa thông tin nhân viên " + zInfo.LastName + " " + zInfo.FirstName;
            }
        }

        [WebMethod]
        public static string CreateUser(int EmployeeKey, string UserName)
        {
            //string zUserName = Tools.RemoveVietnamese(UserName);
            //zUserName += Tools.Random();

            //SessionUser zUser = User_Data.CheckUser(UserName, "123456");
            //if (zUser.EmployeeKey > 0)
            //{
            //    return "Nhân viên này đã có tài khoản rồi !";
            //}

            //User_Info zInfo = new User_Info();
            //zInfo.EmployeeKey = EmployeeKey;
            //zInfo.Name = UserName;
            //zInfo.Password = "123456";
            //zInfo.Activated = 1;
            //zInfo.ExpireDate = DateTime.Now.AddYears(1);
            //zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            //zInfo.CreatedDate = DateTime.Now;

            //zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"];
            //zInfo.ModifiedDate = DateTime.Now;

            //zInfo.Create();

            //if (zInfo.Message != string.Empty)
            //{
            //    return "Lỗi tạo user, xin liên hệ Admin !";
            //}

            return "OK";
        }
        [WebMethod]
        public static string NgayTinhLuong(int EmployeeKey, string txt)
        {
            int Key = Employees_Data.GetExistCloseMonth(EmployeeKey, Tools.ConvertToDate(txt));
            int Month = 12 - DateTime.Now.Month;
            string SQL = "";
            if (Key > 0)
            {
                SQL = "UPDATE HRM_CloseYear SET CurrentYear = " + Month + " WHERE AutoKey =" + Key;
            }
            else
            {
                SQL = "INSERT INTO HRM_CloseYear (EmployeeKey,PreviousYear,CurrentYear,CalculateYear) VALUES (" + EmployeeKey + ",0," + Month + "," + Tools.ConvertToDate(txt).Year + ")";
            }

            return CustomInsert.Exe(SQL).Message;
        }
        [WebMethod]
        public static ItemReturn DeleteEmployee(int EmployeeKey)
        {
            ItemReturn zResult = new ItemReturn();
            Employees_Info zInfo = new Employees_Info();
            zInfo.Key = EmployeeKey;
            zInfo.Delete();
            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
            }

            return zResult;
        }
        [WebMethod]
        public static ItemReturn SaveEmployee(
            int EmployeeKey, int DepartmentKey, int ManagerKey, int PositionKey,
            string Class, string FirstName, string LastName, int Gender,
            string Birthday, string IDCard, string IDDate, string IDPlace,
            string Phone1, string Email1, string Address1, string Address2,
            string DateStart, string DateEnd, string CarNumber, int IsWorking,
            string UnitLevel, string AllowOff, string BasicSalary, string Insurance,
            string SupportFee, string ExtraFee, string BasicSalary1, string Insurance1,
            string SupportFee1, string ExtraFee1, string Spec, string CheckSaleryTable)
        {
            ItemReturn zResult = new ItemReturn();
            Employees_Info zInfo = new Employees_Info(EmployeeKey);
            zInfo.SpecKey = Spec.ToInt();
            zInfo.PositionKey = PositionKey;
            zInfo.DepartmentKey = DepartmentKey;
            zInfo.DepartmentRole = DepartmentKey.ToString();
            zInfo.ManagerKey = ManagerKey;
            zInfo.IsWorking = IsWorking;
            zInfo.Gender = Gender;
            zInfo.FirstName = FirstName;
            zInfo.LastName = LastName;
            zInfo.Birthday = Tools.ConvertToDate(Birthday);
            zInfo.IDCard = IDCard;
            zInfo.IDPlace = IDPlace;
            zInfo.IDDate = Tools.ConvertToDate(IDDate);
            zInfo.Class = Class;
            zInfo.Phone1 = Phone1;
            zInfo.Email1 = Email1;
            zInfo.DateStart = Tools.ConvertToDate(DateStart);
            zInfo.DateEnd = Tools.ConvertToDate(DateEnd);
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.CarNumber = CarNumber;
            zInfo.UnitLevel = UnitLevel;
            zInfo.DateOffAllow = Tools.ConvertToDate(AllowOff);
            zInfo.CheckSaleryTable = Convert.ToBoolean(CheckSaleryTable);

            float tempbh = 0;
            float.TryParse(Insurance, out tempbh);
            zInfo.Insurance = tempbh;

            double temp = 0;
            double.TryParse(BasicSalary, out temp);
            zInfo.BasicSalary = temp;

            temp = 0;
            double.TryParse(SupportFee, out temp);
            zInfo.SupportFee = temp;

            temp = 0;
            double.TryParse(ExtraFee, out temp);
            zInfo.ExtraFee = temp;

            tempbh = 0;
            float.TryParse(Insurance1, out tempbh);
            zInfo.Insurance1 = tempbh;

            temp = 0;
            double.TryParse(BasicSalary1, out temp);
            zInfo.BasicSalary1 = temp;

            temp = 0;
            double.TryParse(SupportFee1, out temp);
            zInfo.SupportFee1 = temp;

            temp = 0;
            double.TryParse(ExtraFee1, out temp);
            zInfo.ExtraFee1 = temp;

            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save();
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static int KiemTraPhep(int EmployeeKey)
        {
            SqlContext zSql = new SqlContext();
            return zSql.GetObject("SELECT COUNT(EmployeeKey) FROM HRM_TicketOff WHERE Approve = 0 AND EmployeeKey =" + EmployeeKey).ToInt();
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;

            if (_Permitsion[2].ToInt() == 0)
            {
                txt_Email1.Visible = false;
                txt_Phone1.Visible = false;
            }
        }
        #endregion
    }
}