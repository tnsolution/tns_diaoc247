﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="SalaryEdit.aspx.cs" Inherits="WebApp.HRM.SalaryEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td10 {
            width: 25%;
        }

        .tong {
            background-color: #a5d4f4;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Chi tiết bảng lương.<small><asp:Label ID="lblTitle1" runat="server" Text="..."></asp:Label></small>
                <span class="tools pull-right">
                    <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row" id="tblInfo">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="widget-box transparent" id="thongtin" style="display: none;">
                    <div class="widget-header widget-header-small">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fa fa-product-hunt orange"></i>
                            Thông tin xử lý lương
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main padding-8">
                            <table class="table table-bordered" id="header" style="display: none">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2">
                                            <asp:ListItem Value="0" Text="--Chọn nhân viên--" disable="disable" Selected="True"></asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="width: 200px">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" role='datepicker'
                                                id="txtFromDate" placeholder="Từ ngày" runat="server" />
                                        </div>
                                    </td>
                                    <td style="width: 200px">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" role='datepicker'
                                                id="txtToDate" placeholder="Đến ngày" runat="server" />
                                        </div>
                                    </td>
                                    <td style="width: 50px" id="tdbutton">
                                        <button type="button" class="btn btn-white btn-sm btn-info btn-bold" id="btnProcess">
                                            <i class="ace-icon fa fa-bolt"></i>
                                            Tính lương
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="widget-box transparent" id="tinhtoanluong" style="display: none;">
                    <div class="widget-header widget-header-small">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fa fa-product-hunt orange"></i>
                            Lương hiệu quả <small>Nguồn giao dịch: Trực tiếp/Liên kết</small>
                            <span class="pull-right">
                                <label>
                                    <input name='form-field-checkbox' class='ace' type='checkbox' runat='server' id='chkDevined' />
                                    <span class='lbl'>&nbsp Chia/1.1 (Bán mới)</span>
                                </label>
                            </span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main padding-8" id="bangtinh">
                            <asp:Literal ID="Lit_Commission" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="widget-box transparent">
                    <div class="widget-header widget-header-small">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fa fa-product-hunt orange"></i>
                            Bảng lương&nbsp;<asp:Label ID="lblTitle" runat="server" Text="..."></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main padding-8">
                            <table class="table table-bordered table-striped table-hover" id="body" style="display: none;">
                                <thead>
                                    <tr>
                                        <th class='td10'>Thông tin</th>
                                        <th>Nội dung</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>1. Lương cơ bản</td>
                                    <td class="text-right">
                                        <input id="txt1" type="text" value="4,000,000" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>2. Phụ cấp</td>
                                    <td class="text-right">
                                        <input id="txt14" type="text" value="900,000" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>3. Phụ cấp trách nhiệm</td>
                                    <td class="text-right">
                                        <input id="txt6" type="text" value="1,500,000" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>4. Ngày công chuẩn</td>
                                    <td class="text-right">
                                        <input id="txt2" type="text" value="26" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>5. Số ngày nghỉ trong tháng</td>
                                    <td class="text-right">
                                        <input id="txt3" type="text" value="0" class="text-right" maxlength="4" onkeypress="Page.FloatInput(this)" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>6. Ngày công thực tế</td>
                                    <td class="text-right">
                                        <input id="txt4" type="text" value="26" class="text-right" maxlength="4" onkeypress="Page.FloatInput(this)" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>7. Bảo hiểm</td>
                                    <td class="text-right">
                                        <input id="txt5" type="text" value="10.5" class="text-right" maxlength="4" onkeypress="Page.FloatInput(this)" runat="server" disabled />
                                        <input id="txt51" type="text" value="0" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>8. Lương cơ bản thực nhận</td>
                                    <td class="text-right">
                                        <input id="txt7" type="text" value="0" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>9. Hiệu quả công việc</td>
                                    <td class="text-right">
                                        <input id="txt8" type="text" value="0" class="text-right" moneyinput="true" runat="server" disabled /></td>
                                </tr>
                                <tr>
                                    <td>10. Thuế TNCN</td>
                                    <td class="text-right">
                                        <input id="txt9" type="text" value="0" class="text-right" moneyinput="true" runat="server" iscal="yes" /></td>
                                </tr>
                                <tr>
                                    <td>11. Thưởng</td>
                                    <td class="text-right">
                                        <asp:DropDownList ID="DDLTag" runat="server">
                                            <asp:ListItem Value="1" Text="Lần 1"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Lần 2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <input id="txtNote1" type="text" runat="server" placeholder="Nội dung" style="width: 300px" />
                                        <input id="txt13" type="text" value="0" class="text-right" moneyinput="true" runat="server" iscal="yes" /></td>
                                </tr>
                                <tr>
                                    <td>12. Lương thực nhận sau thuế</td>
                                    <td class="text-right">
                                        <input id="txt10" type="text" value="0" class="text-right" moneyinput="true" style="border: 2px solid #c74444; color: #c74444" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>13. Lãnh lương lần 1</td>
                                    <td class="text-right">
                                        <input id="txt11" type="text" value="0" class="text-right" moneyinput="true" style="border: 2px solid #c74444; color: #c74444" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td>14. Lãnh lương lần 2</td>
                                    <td class="text-right">
                                        <input id="txt12" type="text" value="0" class="text-right" moneyinput="true" style="border: 2px solid #c74444; color: #c74444" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <textarea class="form-control" rows="5" placeholder="Ghi chú" id="txtdescription" runat="server"></textarea></td>
                                </tr>
                            </table>
                            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <asp:Literal ID="LitProcess" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <asp:Label ID="CookEmployee" runat="server" Text=""></asp:Label>
        <asp:Label ID="CookDepartment" runat="server" Text=""></asp:Label>
        <asp:Label ID="CookUnitLevel" runat="server" Text=""></asp:Label>
        <asp:Label ID="CookUserKey" runat="server" Text=""></asp:Label>
    </div>
    <asp:HiddenField ID="HID_Chia" runat="server" />
    <asp:HiddenField ID="HID_Tong" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Insurance" runat="server" Value="0" />
    <asp:Button ID="btnExportExcel" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnExportExcel_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script type="text/javascript" src="/template/table-edits.min.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script src="SalaryEdit.js"></script>
    <script>
        $(function () {
            $("#btnExcel").click(function () {
                $("[id$=btnExportExcel]").trigger("click");
            });
        });
       
    </script>
</asp:Content>
