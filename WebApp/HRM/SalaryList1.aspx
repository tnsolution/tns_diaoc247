﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="SalaryList1.aspx.cs" Inherits="WebApp.HRM.SalaryList1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Bảng lương
                <small>
                    <asp:Label ID="lbltime" runat="server" Text="Label"></asp:Label></small>
                <asp:Literal ID="LitButton" runat="server"></asp:Literal>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12" id="divtable">
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $("#tblData tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                location.href = '/HRM/SalaryEdit.aspx?ID=' + trid;
            });
        });
    </script>
</asp:Content>
