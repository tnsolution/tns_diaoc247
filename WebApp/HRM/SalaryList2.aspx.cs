﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.HRM
{
    public partial class SalaryList2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
                DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
                LoadData(FromDate, ToDate);

                LitButton.Text = @" 
                <span class='pull-right action-buttons'>
                    <a href='#' id='ViewPrevious'>← Tháng trước</a> |
                    <a href='#' id='ViewNext'>Tháng sau →</a>
                </span>";
            }
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }
            if (ViewTime == -1)
            {
                FromDate = Tools.ConvertToDate(HID_FromDate.Value);
                ToDate = Tools.ConvertToDate(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString("dd/MM/yyyy");
                HID_ToDate.Value = ToDate.ToString("dd/MM/yyyy");
            }

            LoadData(FromDate, ToDate);
        }

        protected void LoadData(DateTime FromDate, DateTime ToDate)
        {
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;

            Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY [RANK]", false);
            Tools.DropDown_DDL(DDL_Employee2, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);

            DataTable zTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            lbltime.Text = "Từ ngày: " + FromDate.ToString("dd/MM/yyyy") + " đến ngày: " + ToDate.ToString("dd/MM/yyyy") + " tổng <b style=style='color: #c74444'>" + Salary_Data.SumSalary(DDL_Employee2.SelectedValue.ToInt(), FromDate, ToDate).ToString("n0") + "</b>";

            zTable = Salary_Data.List(FromDate, ToDate, ViewDepartment);
            Literal1.Text = HtmlView(zTable);

            HID_Employee.Value = "0";
            HID_Department.Value = ViewDepartment;
        }

        [WebMethod]
        public static ItemReturn Search(string Department, string Employee, string FromDate, string ToDate)
        {
            DateTime zFromDate = Tools.ConvertToDate(FromDate);
            DateTime zToDate = Tools.ConvertToDate(ToDate);

            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            zTable = Salary_Data.List(zFromDate, zToDate, Employee.ToInt(), Department.ToInt());

            ItemReturn Result = new ItemReturn();
            Result.Result = AjaxHtmlView(zTable);
            Result.Result2 = Salary_Data.SumSalary(Employee.ToInt(), Department.ToInt(), zFromDate, zToDate).ToString("n0");
            return Result;
        }
        public static string AjaxHtmlView(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Từ ngày</th>");
            zSb.AppendLine("        <th>Đến ngày</th>");
            zSb.AppendLine("        <th>Lần 1</th>");
            zSb.AppendLine("        <th>Lần 2</th>");
            zSb.AppendLine("        <th>Tổng</th>");
            zSb.AppendLine("        <th class='btnclass'>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                int Key = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                foreach (DataRow r in zTable.Rows)
                {
                    string Style = "";
                    if (r["Current"].ToInt() == Key && r["CurrentStatus"].ToInt() == 0)
                    {
                        Style = "style='Background:#ff00004f'";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' employee='" + r["EmployeeKey"].ToString() + "' " + Style + ">");
                    zSb.AppendLine("            <td>" + (i++) + "</td>");
                    zSb.AppendLine("            <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FromDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["ToDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param11"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param12"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FINAL"].ToDoubleString() + "</td>");

                    if (r["Status"].ToString() == "0")
                    {
                        zSb.AppendLine("            <td class='btnclass'><button type='button' class='btn btn-sm btn-white btn-warning btn-bold' btnSend><i class='ace-icon fa fa-send blue'></i>Gửi đi</button></td>");
                    }
                    else
                    {
                        zSb.AppendLine("            <td class='btnclass'>Đã gửi</td>");
                    }
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='8'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }

        string HtmlView(DataTable zTable)
        {
            StringBuilder zSb = new StringBuilder();

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Phòng</th>");
            zSb.AppendLine("        <th>Tên</th>");
            zSb.AppendLine("        <th>Từ ngày</th>");
            zSb.AppendLine("        <th>Đến ngày</th>");
            zSb.AppendLine("        <th>Lần 1</th>");
            zSb.AppendLine("        <th>Lần 2</th>");
            zSb.AppendLine("        <th>Tổng</th>");
            zSb.AppendLine("        <th class='btnclass'>#</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                int Key = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                foreach (DataRow r in zTable.Rows)
                {
                    string Style = "";
                    if (r["Current"].ToInt() == Key && 
                        r["CurrentStatus"].ToInt() == 0)
                    {
                        Style = "style='Background:#ff00004f'";
                    }

                    zSb.AppendLine("            <tr id='" + r["AutoKey"].ToString() + "' employee='" + r["EmployeeKey"].ToString() + "' " + Style + ">");
                    zSb.AppendLine("            <td>" + (i++) + "</td>");
                    zSb.AppendLine("            <td>" + r["DepartmentName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["EmployeeName"].ToString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FromDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["ToDate"].ToDateString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param11"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["Param12"].ToDoubleString() + "</td>");
                    zSb.AppendLine("            <td>" + r["FINAL"].ToDoubleString() + "</td>");

                    if (r["Status"].ToString() == "0")
                    {
                        zSb.AppendLine("            <td class='btnclass'><button type='button' class='btn btn-sm btn-white btn-warning btn-bold' btnSend><i class='ace-icon fa fa-send blue'></i>Gửi đi</button></td>");
                    }
                    else
                    {
                        zSb.AppendLine("            <td class='btnclass'>Đã gửi</td>");
                    }

                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='8'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            return zSb.ToString();
        }

        [WebMethod]
        public static string SendMessage(int sAutoKey, int sEmployee)
        {
            //Salary_Info zInfo = new Salary_Info(sAutoKey);
            //string Title = "Bảng lương" + zInfo.EmployeeName;

            string SQL = @"
            DELETE SYS_Message WHERE ObjectTable=N'HRM_Salary' AND ObjectKey=" + sAutoKey + " AND EmployeeKey = " + sEmployee + @"

            INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey,Readed, Description) 
            VALUES (N'Bảng lương',N'Bảng lương', GETDATE(), N'HRM_Salary'," + sAutoKey + "," + sEmployee + ",0,N'Tình Trạng bảng lương')";

            string Message = CustomInsert.Exe(SQL).Message;
            if (Message == string.Empty)
                return "OK";
            else
                return "ERROR";
        }

        #region [Excel]
       // void ExportExcel()
       // {
       //     DateTime FromDate = Tools.ConvertToDate(HID_FromDate.Value);
       //     DateTime ToDate = Tools.ConvertToDate(HID_ToDate.Value);

       //     FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
       //     ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

       //     DataTable zTable = new DataTable();

       //     string Department = HID_Department.Value;
       //     string Employee = HID_Employee.Value;

       //     zTable = Salary_Data.List(FromDate, ToDate, Employee.ToInt(), Department.ToInt());

       //     DataTable dtExcel = new DataTable();
       //     dtExcel.Columns.Add("Phòng", typeof(string));
       //     dtExcel.Columns.Add("Tên", typeof(string));
       //     dtExcel.Columns.Add("Từ ngày", typeof(string));
       //     dtExcel.Columns.Add("Đến ngày", typeof(string));
       //     dtExcel.Columns.Add("Lần 1", typeof(string));
       //     dtExcel.Columns.Add("Lần 2", typeof(string));
       //     dtExcel.Columns.Add("Tổng tiền", typeof(string));

       //     double tong1 = 0;
       //     foreach (DataRow r in zTable.Rows)
       //     {
       //         string VAT = ((r["Money"].ToDouble() * r["VAT"].ToInt()) / 100).ToDoubleString();

       //         DataRow rExcel = dtExcel.NewRow();
       //         rExcel[0] = r["DepartmentName"].ToString();
       //         rExcel[1] = r["EmployeeName"].ToDateString();
       //         rExcel[2] = r["FromDate"].ToDateString();
       //         rExcel[3] = r["ToDate"].ToDateString();
       //         rExcel[4] = r["Param11"].ToDoubleString();
       //         rExcel[5] = r["Param12"].ToDoubleString();
       //         rExcel[6] = r["FINAL"].ToDoubleString();
       //         dtExcel.Rows.Add(rExcel);

       //         tong1 += r["FINAL"].ToDouble();              
       //     }
       //     DataRow rTong = dtExcel.NewRow();
       //     rTong[0] = "Tổng cộng";          
       //     rTong[6] = tong1.ToString("n0");
       //     dtExcel.Rows.Add(rTong);
       //     DownloadExcel(dtExcel);
       // }

       //void DownloadExcel(DataTable dtExcel)
       // {
       //     using (StringWriter sw = new StringWriter())
       //     {
       //         HtmlTextWriter hw = new HtmlTextWriter(sw);

       //         //To Export all pages
       //         GridView GridView1 = new GridView();
       //         GridView1.AllowPaging = false;
       //         GridView1.DataSource = dtExcel;
       //         GridView1.DataBind();

       //         GridView1.HeaderRow.BackColor = Color.White;
       //         foreach (TableCell cell in GridView1.HeaderRow.Cells)
       //         {
       //             cell.BackColor = Color.WhiteSmoke;
       //         }
       //         foreach (GridViewRow row in GridView1.Rows)
       //         {
       //             row.BackColor = Color.White;
       //             foreach (TableCell cell in row.Cells)
       //             {
       //                 if (row.RowIndex % 2 == 0)
       //                 {
       //                     cell.BackColor = Color.LemonChiffon;
       //                 }
       //                 else
       //                 {
       //                     cell.BackColor = GridView1.RowStyle.BackColor;
       //                 }
       //                 cell.CssClass = "textmode";
       //             }
       //         }

       //         GridView1.RenderControl(hw);

       //         Response.Clear();
       //         Response.Buffer = true;
       //         Response.AddHeader("content-disposition", "attachment;filename=luong.xls");
       //         Response.Charset = "";
       //         Response.ContentType = "application/vnd.ms-excel";
       //         //style to format numbers to string
       //         string style = @"<style> .textmode { } </style>";
       //         Response.Write(style);
       //         Response.Output.Write(sw.ToString());
       //         Response.Flush();
       //         Response.End();
       //     }
       // }

       // protected void btnExport_Click(object sender, EventArgs e)
       // {
       //     ExportExcel();
       // }
        #endregion
    }
}