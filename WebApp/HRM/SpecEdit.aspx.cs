﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Web.Services;

namespace WebApp.HRM
{
    public partial class SpecEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_SpecKey.Value = Request["ID"];
                LoadInfo();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Tạo thông tin mới";
            if (HID_SpecKey.Value != string.Empty && HID_SpecKey.Value != "0")
            {
                Spec_Info zInfo = new Spec_Info(HID_SpecKey.Value.ToInt());
                txt_DepartmentName.Value = zInfo.SpecName;
                txt_Description.Value = zInfo.Description;

                Lit_TitlePage.Text = "Sửa thông tin sàn " + zInfo.SpecName;
            }
        }

        [WebMethod]
        public static ItemReturn SaveDepartment(int SpecKey, string SpecName, string Description)
        {
            ItemReturn zResult = new ItemReturn();
            Spec_Info zInfo = new Spec_Info(SpecKey);
            zInfo.SpecName = SpecName;
            zInfo.Description = Description;
            zInfo.Save();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn DeleteDepartment(int SpecKey)
        {
            ItemReturn zResult = new ItemReturn();
            Spec_Info zInfo = new Spec_Info();
            zInfo.Key = SpecKey;
            zInfo.Delete();

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}