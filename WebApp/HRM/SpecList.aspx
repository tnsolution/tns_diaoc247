﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="SpecList.aspx.cs" Inherits="WebApp.HRM.SpecList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap-editable.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Các phòng ban
                <span class="tools pull-right">
                    <a href="SpecEdit.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <asp:HiddenField ID="HID_Type" runat="server" Value="1" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/bootstrap-editable.min.js"></script>
    <script src="/template/ace-master/assets/js/ace-editable.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#tblData tbody tr td:not(:last-child)").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "SpecEdit.aspx?ID=" + trid;
            });
        });
    </script>
</asp:Content>
