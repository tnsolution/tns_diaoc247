﻿using Lib.HRM;
using System;
using System.Data;
using System.Text;

namespace WebApp.HRM
{
    public partial class SpecList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoaData();
            }
        }

        void LoaData()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Departments_Data.ViewType(2);

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Sàn</th>");
            zSb.AppendLine("        <th>Ghi chú</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["SpecKey"].ToString() + "'>");
                    zSb.AppendLine("                <td>" + (i++) + "</td>");
                    zSb.AppendLine("                <td>" + r["SpecName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Description"].ToString() + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
    }
}