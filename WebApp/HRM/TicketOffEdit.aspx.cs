﻿using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;
//chưa làm phép qua năm đấy.
namespace WebApp.HRM
{
    public partial class TicketOffEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_TicketKey.Value = Request["ID"];

                LoadInfo();
            }
        }
        void LoadInfo()
        {
            int Manager = 0;
            Manager = Employees_Data.GetManager(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());

            int Key = HID_TicketKey.Value.ToInt();
            TicketOff_Info zInfo = new TicketOff_Info(Key);
            if (zInfo.TicketKey != 0)
            {
                #region [Info]


                string Html = "";
                int chucvu = Employees_Data.GetPositionKey(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
                int cophep = TicketOff_Data.CoPhepNam(chucvu);
                if (cophep > 0)
                {
                    if (zInfo.CategoryKey == 1)
                        Html = @"
<select class='form-control' id=DDLLoaiPhep>
	<option value='0'>--Chọn--</option>
	<option selected='selected' value='1'>Không lương</option>
	<option value='2'>Phép năm</option>
</select>";
                    else
                        Html = @"
<select class='form-control' id=DDLLoaiPhep>
	<option value='0'>--Chọn--</option>
	<option value='1'>Không lương</option>
	<option selected='selected' value='2'>Phép năm</option>
</select>";
                }
                else
                {
                    Html = @"
<select class='form-control' id=DDLLoaiPhep>
	<option selected='selected' value='1'>Không lương</option>
</select>";
                }
                LitSelect.Text = Html;

                txtTuNgay.Text = zInfo.FromDate.ToString("dd/MM/yyyy");
                txtDenNgay.Text = zInfo.ToDate.ToString("dd/MM/yyyy");
                txtNgayXin.Text = zInfo.TicketDate.ToString("dd/MM/yyyy");
                txtNoiDung.Text = zInfo.Contents;
                txtSoNgayNghi.Text = zInfo.NumberDay.ToString();

                lblHoten.Text = "Họ tên: " + zInfo.EmployeeName;
                lblPhong.Text = "Phòng: " + zInfo.DepartmentName;
                lblQuanly.Text = "Quản lý trực tiếp: " + zInfo.ManagerName;
                lblChucvu.Text = Employees_Data.GetPosition(zInfo.EmployeeKey);
                #endregion

                string img = "<img style='cursor:pointer; width:16px;' src='../template/custom-image/false.png'>";
                if (zInfo.Confirmed == 1)
                {
                    img = "<img style='width:16px;' src='../template/custom-image/true.png'>";
                }
                else
                {
                    if (zInfo.Approve == 1)
                    {
                        img = "<img id=" + zInfo.TicketKey + " action=3 style='cursor:pointer; width:16px;' src='../template/custom-image/false.png'>";
                    }
                }

                #region [Extra Info]
                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table' style='border:0px !important'>");
                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người khởi tạo:</td><td style='padding: 4px 0 4px 0;'>" + zInfo.CreatedBy + "</td></tr>");
                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày khởi tạo:</td><td style='padding: 4px 0 4px 0;'>" + zInfo.CreatedDateTime.ToString("dd/MM/yyyy") + "</td></tr>");
                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td style='padding: 4px 0 4px 0;'>" + zInfo.ModifiedBy + "</td></tr>");
                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td style='padding: 4px 0 4px 0;'>" + zInfo.ModifiedDateTime.ToString("dd/MM/yyyy") + "</td></tr>");
                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Nhân sự xác nhận:</td><td style='padding: 4px 0 4px 0;'>" + img + "</td></tr>");
                if (zInfo.ConfirmDate != DateTime.MinValue)
                    zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày xác nhận:</td><td style='padding: 4px 0 4px 0;'>" + zInfo.ConfirmDate.ToString("dd/MM/yyyy") + "</td></tr>");
                else
                    zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày xác nhận:</td><td style='padding: 4px 0 4px 0;'></td></tr>");

                string Number1 = Employees_Data.OffWithMoney(zInfo.EmployeeKey, zInfo.FromDate).ToString();
                string Number2 = Employees_Data.GetOffYear(zInfo.EmployeeKey, DateTime.Now).ToString();

                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Nghĩ phép năm " + DateTime.Now.Year + ": </td><td style='padding: 4px 0 4px 0;'>" + Number1 + "/" + Number2 + "</td></tr>");
                zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Nghĩ phép không lương:</td><td style='padding: 4px 0 4px 0;'>" + Employees_Data.OffNoMoney(zInfo.EmployeeKey, zInfo.FromDate).ToString() + "</td></tr>");
                if (zInfo.OffLeft == string.Empty || zInfo.OffLeft == "0")
                    zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Phép năm còn lại:</td><td style='padding: 4px 0 4px 0;'>" + Employees_Data.OffLeft(zInfo.EmployeeKey, zInfo.FromDate).ToString() + "</td></tr>");
                else
                    zSb.AppendLine("    <tr><td style='padding: 4px 0 4px 0;'><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Phép năm còn lại:</td><td style='padding: 4px 0 4px 0;'>" + zInfo.OffLeft + "</td></tr>");
                zSb.AppendLine("</table>");
                LitInfo.Text = zSb.ToString();
                #endregion

                #region [GenHtmlSign]
                string html = "";
                string[] GetAllManager = zInfo.Step.Split(',');
                foreach (string s in GetAllManager)
                {
                    int CurrentEmployee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    int EmployeeKey = s.ToInt();
                    string EmployeeName = Employees_Data.GetManagerName(EmployeeKey);
                    int Unit = Employees_Data.GetLevel(EmployeeKey);
                    int Accept = Notification_Data.Check_TicketOff(EmployeeKey, zInfo.TicketKey);

                    string flag = "";
                    if (Accept == 0)
                    { flag = "false"; }
                    else
                    { flag = "true"; }

                    if (Accept == 0)
                    {
                        flag = "false";
                        if (CurrentEmployee == EmployeeKey &&
                            CurrentEmployee != zInfo.EmployeeKey &&
                            Unit <= 2)
                        {
                            html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='cursor:pointer; width: 32px' action='1' id='" + EmployeeKey + @"' alt='' src='../template/custom-image/" + flag + @".png' />
                            <img style='cursor:pointer; width: 32px' action='2' id='" + EmployeeKey + @"' alt='' src='../template/custom-image/cross.png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                        }
                        else
                        {
                            html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='cursor:pointer; width: 32px' action=1 id='" + EmployeeKey + @"' alt='' src='../template/custom-image/" + flag + @".png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                        }
                    }
                    if (Accept == 1)
                    {
                        flag = "true";
                        html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='width: 32px' alt='' src='../template/custom-image/" + flag + @".png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                    }
                    if (Accept == 2)
                    {
                        flag = "no";
                        html += @"<div class='col-md-4'>
                        <div class='center center-block'>
                            <img style='width: 32px' alt='' src='../template/custom-image/cross.png' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                    }
                }
                #endregion

                LiteralSign.Text = html;

                #region Lock Input
                if (zInfo.IsView == 0 ||
                    zInfo.IsView == 1)
                {
                    LitButton.Text = @"
<button type='button' class='btn btn-white btn-info btn-bold' id='btnSave'><i class='ace-icon fa fa-send-o blue'></i>Lưu</button>
<button type='button' class='btn btn-white btn-info btn-bold' id='btnDelete'><i class='ace-icon fa fa-trash-o blue'></i>Xóa</button>";
                }
                else
                {
                    LitScript.Text = @"<script>$(document).ready(function () { $('#tblInfo :input').prop('disabled', true); });</script>";
                    LitButton.Text = "";
                }
                #endregion
            }
            else
            {
                #region [OPEN NEW TICKET]
                LitButton.Text = @"<button type='button' class='btn btn-white btn-info btn-bold' id='btnSave'><i class='ace-icon fa fa-send-o blue'></i>Lưu</button>";
                txtNgayXin.Text = DateTime.Now.ToString("dd/MM/yyyy");
                lblHoten.Text = "Họ tên: " + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                lblPhong.Text = "Phòng: " + HttpContext.Current.Request.Cookies["UserLog"]["DepartmentName"].ToUrlDecode();
                lblQuanly.Text = "Quản lý trực tiếp: " + Employees_Data.GetManagerName(Manager);
                lblChucvu.Text = Employees_Data.GetPosition(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());

                string html = "";
                DataTable ztblAll = Employees_Data.GetAllManager(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
                foreach (DataRow r in ztblAll.Rows)
                {
                    string EmployeeName = Employees_Data.GetManagerName(r["EmployeeKey"].ToInt());

                    html += @"<div class='col-md-4'>                        
                        <div class='center center-block'>
                            <img alt='' src='../template/custom-image/false.png' style='width: 32px' />
                        </div>
                        <div class='center center-block'>" + EmployeeName + @"</div>
                    </div>";
                }
                LiteralSign.Text = html;
                #endregion

                int chucvu = Employees_Data.GetPositionKey(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
                int cophep = TicketOff_Data.CoPhepNam(chucvu);
                if (cophep > 0)
                {
                    LitSelect.Text = @"
<select class='form-control' id=DDLLoaiPhep>
	<option value='0' selected='selected'>--Chọn--</option>
	<option value='1'>Không lương</option>
	<option value='2'>Phép năm</option>
</select>";
                }
                else
                {
                    LitSelect.Text = @"
<select class='form-control' id=DDLLoaiPhep>
	<option selected='selected' value='1'>Không lương</option>
</select>";
                }
            }
        }
        [WebMethod]
        public static ItemReturn SaveInfo(int TicketKey, string LoaiPhep, string NgayXin, string TuNgay, string DenNgay, string NoiDung, string SoNgay)
        {
            int Manager = 0;
            Manager = Employees_Data.GetManager(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());

            TicketOff_Info zInfo = new TicketOff_Info(TicketKey);

            zInfo.CategoryKey = LoaiPhep.ToInt();
            zInfo.TicketDate = Tools.ConvertToDate(NgayXin);
            zInfo.FromDate = Tools.ConvertToDate(TuNgay);
            zInfo.ToDate = Tools.ConvertToDate(DenNgay);
            zInfo.Contents = NoiDung.Trim();
            zInfo.NumberDay = float.Parse(SoNgay);

            if (zInfo.OffLeft == string.Empty)
                zInfo.OffLeft = Employees_Data.OffLeft(zInfo.EmployeeKey, zInfo.FromDate).ToString();

            zInfo.ManagerKey = Manager;
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            string temp = "";
            DataTable ztblAll = Employees_Data.GetAllManager(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
            foreach (DataRow r in ztblAll.Rows)
            {
                temp += r["EmployeeKey"].ToInt() + ",";
            }

            if (temp.Contains(","))
                zInfo.Step = temp.Remove(temp.LastIndexOf(","));
            else
                zInfo.Step = temp;

            zInfo.Save();

            ItemReturn zResult = new ItemReturn();
            if (zInfo.Message != string.Empty)
            {
                zResult.Message = zInfo.Message;
                zResult.Result = "ERROR";
            }
            else
            {
                zResult.Result = "/HRM/TicketOffEdit.aspx?ID=" + zInfo.TicketKey;
            }
            return zResult;
        }
        [WebMethod]
        public static ItemReturn Approve(int TicketKey, int CurrentEmployee, int Action)
        {
            int Manager = 0;
            Manager = Employees_Data.GetManager(CurrentEmployee);
            int phonghientai = Employees_Data.GetDepartment(CurrentEmployee);
            int phong = Employees_Data.GetDepartment(Manager.ToInt());
            int tructiep = Employees_Data.GetLevel(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt());
            string Message = "Đơn xin phép được xử lý bởi " + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            string SQL = "";

            TicketOff_Info zInfo = new TicketOff_Info(TicketKey);
            if (zInfo.EmployeeKey == CurrentEmployee)
            {
                zInfo.IsView = 2;
                zInfo.UpdateSend();
            }
            ItemReturn zResult = new ItemReturn();
            #region [Update Current]
            int Key = Notification_Data.Get_MessageKey(TicketKey, CurrentEmployee);
            if (Key == 0)
            {
                SQL = @"INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey, DepartmentKey, Readed,ReadedDate,Accept,AcceptDate, Description, Orders) 
            VALUES (N'" + Message + "',N'" + Message + "', GETDATE(), N'HRM_TicketOff'," + TicketKey + "," + CurrentEmployee + "," + phonghientai + ",1,GETDATE()," + Action + ",GETDATE(),N'Tình Trạng đơn phép'," + tructiep + ")";
                CustomInsert.Exe(SQL);
            }
            else
            {
                SQL = @"UPDATE SYS_Message SET Readed = 1, ReadedDate =GETDATE(), Accept=" + Action + ", AcceptDate=GETDATE() WHERE ObjectTable = N'HRM_TicketOff' AND ObjectKey =" + TicketKey + " AND EmployeeKey = " + CurrentEmployee;
                CustomInsert.Exe(SQL);
            }
            #endregion

            //khong duyet
            #region MyRegion
            if (Action == 2)
            {
                zInfo = new TicketOff_Info();
                zInfo.TicketKey = TicketKey;
                zInfo.Approve = 2;
                zInfo.ApproveBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.UpdateApprove();
                return zResult;
            }
            #endregion

            #region [Gửi lên]            
            if (Manager != 0)
            {
                #region [send for quan ly truc tiep]

                if (tructiep != 1)
                {
                    Message = "Đơn xin phép" + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                    SQL = @"INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey, DepartmentKey, Readed, Description, Orders) 
            VALUES (N'" + Message + "',N'" + Message + "', GETDATE(), N'HRM_TicketOff'," + TicketKey + "," + Manager + "," + phong + ",0,N'Tình Trạng đơn phép'," + tructiep + ")";

                    CustomInsert.Exe(SQL);
                }
                #endregion

                zResult.Message = "Đơn phép bạn đã được gửi đến cho " + Employees_Data.GetManagerName(Manager) + " !.";
                zResult.Result = "OK";
            }
            else
            {
                #region Cap nhat phieu off
                zInfo = new TicketOff_Info();
                zInfo.TicketKey = TicketKey;
                zInfo.Approve = 1;
                zInfo.ApproveBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.UpdateApprove();
                zResult.Result = "OK";
                #endregion

                #region [Gui cho Admin]
                Message = "Đơn xin phép đã xử lý bởi " + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                SQL = @"INSERT INTO SYS_Message 
            (Message, Title, Date, ObjectTable, ObjectKey, EmployeeKey, DepartmentKey, Readed, Description, Orders) 
            VALUES (N'" + Message + "',N'" + Message + "', GETDATE(), N'HRM_TicketOff'," + TicketKey + "," + 24 + "," + 13 + ",0,N'Tình Trạng đơn phép'," + 0 + ")";

                CustomInsert.Exe(SQL);
                #endregion

                zResult.Result = "OK";
            }
            #endregion

            return zResult;
        }
        [WebMethod]
        public static string Confirm(int TicketKey, int CurrentEmployee)
        {
            TicketOff_Info zInfo = new TicketOff_Info();
            zInfo.TicketKey = TicketKey;
            zInfo.ConfirmBy = CurrentEmployee;
            zInfo.UpdateConfirm();

            if (zInfo.Message == string.Empty)
            {
                string SQL = @"UPDATE SYS_Message SET Readed = 1, ReadedDate =GETDATE(), Accept=1, AcceptDate=GETDATE() WHERE ObjectTable = N'HRM_TicketOff' AND ObjectKey =" + TicketKey + " AND EmployeeKey = " + CurrentEmployee;
                CustomInsert.Exe(SQL);
            }

            return string.Empty;
        }
        [WebMethod]
        public static string Delete(int TicketKey)
        {
            TicketOff_Info zInfo = new TicketOff_Info();
            zInfo.TicketKey = TicketKey;
            zInfo.Delete();

            return string.Empty;
        }
    }
}