﻿using Lib.Config;
using Lib.KPI;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;

/*
 cách hạ chi tieu ngắn hạn
 cộng tất cả số tháng của giao dịch cá nhan trong thang nếu
 giao dịch la truc tiep -> 6th hạ 1
 giao dich la lien ket -> 12tg hạ 1
*/

/*Trang chủ Spec = 1 Là xem chung hết*/

namespace WebApp
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                {
                    Response.Redirect("/Login.aspx");
                }

                InitData();
            }
        }

        void InitData()
        {
            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            HID_FromDate.Value = FromDate.ToString();
            HID_ToDate.Value = ToDate.ToString();

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Spec = HttpContext.Current.Request.Cookies["UserLog"]["SpecKey"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            string ViewSpec = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
            {
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            }
            else
            {
                ViewDepartment = Department;
            }

            if (HttpContext.Current.Request.Cookies["ViewSpec"] != null)
            {
                ViewSpec = HttpContext.Current.Request.Cookies["ViewSpec"].Value.ToUrlDecode();
            }
            else
            {
                ViewSpec = Spec.ToString();
            }

            if (ViewSpec != "1")
            {
                if (UnitLevel < 7)
                {
                    Load_Target(FromDate, ToDate, ViewDepartment, 0);
                    Load_Panel(FromDate, ToDate, ViewDepartment, 0);

                    Tools.ListBox(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + ViewDepartment + " ORDER BY DepartmentName");
                    Lit_Button.Text = "<a href='#NewMessage' data-toggle='modal'><i class='ace-icon fa fa-plus'></i></a>";
                }
                else
                {
                    Load_Target(FromDate, ToDate, string.Empty, Employee);
                    Load_Panel(FromDate, ToDate, string.Empty, Employee);
                    Load_Staff(FromDate, ToDate, Department.ToString());

                    Tools.ListBox(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName");
                    Lit_Button.Text = "";
                }
            }
            else
            {
                if (UnitLevel < 7)
                {
                    Load_Target(FromDate, ToDate, ViewDepartment, 0);
                    Load_Panel(FromDate, ToDate, ViewDepartment, 0);

                    Tools.ListBox(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + ViewDepartment + " ORDER BY DepartmentName");
                    Lit_Button.Text = "<a href='#NewMessage' data-toggle='modal'><i class='ace-icon fa fa-plus'></i></a>";
                }
                else
                {
                    Load_Target(FromDate, ToDate, string.Empty, Employee);
                    Load_Panel(FromDate, ToDate, string.Empty, Employee);

                    Tools.ListBox(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName");
                    Lit_Button.Text = "";
                }
            }

            if (UnitLevel <= 1)
            {
                Load_Staff(FromDate, ToDate, ViewDepartment);
                Tools.ListBox(DDL_Spec, "SELECT SpecKey, SpecName FROM HRM_Specified ORDER BY SpecName");
            }
            else
            {
                Load_Staff(FromDate, ToDate, Spec);
                Tools.ListBox(DDL_Spec, "SELECT SpecKey, SpecName FROM HRM_Specified WHERE SpecKey = " + Spec + " ORDER BY SpecName");
            }

            Load_Message(Department.ToInt());
            Load_Schedule(Employee);

            Lit_TitleDay.Text = "Kế hoạch ngày " + DateTime.Now.ToString("dd");
            Lit_MonthTopStaff.Text = " " + FromDate.Month + "/" + FromDate.Year;
            Lit_TitleMonth.Text = ToDate.Month.ToString();
            Lit_Month.Text = ltMonth.Text = ToDate.Month.ToString();
            Lit_MonthRequire.Text = " tháng " + ToDate.Month.ToString();
        }

        void Load_Schedule(int Employee)
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HomePage.Get_Schedule(Employee);

            zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-border' id='tableTime'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Thời gian</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("    <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 0;
                foreach (DataRow r in zTable.Rows)
                {
                    i++;
                    zSb.AppendLine("<tr><td>" + i + "</td><td>" + r["Title"].ToString() + "</td><td>" + r["Description"].ToString() + "</td></tr>");
                }
            }
            else
            {
                zSb.AppendLine("<tr><td colspan=3>Chưa có kế hoạch</td></tr>");
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            Lit_Schedule.Text = zSb.ToString();
        }
        void Load_Message(int Department)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            DataTable zTable = HomePage.Get_Message_HomePage(Department);
            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("    <div class='itemdiv commentdiv' key='" + r["ID"].ToString() + "'>");
                zSb.AppendLine("    <div class='body'>");
                zSb.AppendLine("    <div class='time'>" + Convert.ToDateTime(r["CreatedDate"]).ToString("dd/MM hh:mm") + "</div>");
                zSb.AppendLine("    <div class='name'>" + r["CreatedBy"].ToString() + "   </div>");
                zSb.AppendLine("    <div class='text'>" + r["Contents"].ToString() + "</div>");

                if (r["EmployeeKey"].ToInt() == Employee)
                {
                    zSb.AppendLine("<div class='tools'><div class='action-buttons bigger-125'><a href='#' btn='btnEditMessage' key='" + r["ID"].ToString() + "'><i class='ace-icon fa fa-pencil blue'></i></a><a href='#' btn= 'btnDeleteMessage' key='" + r["ID"].ToString() + "'><i class='ace-icon fa fa-trash-o red'></i></a></div></div>");
                }

                zSb.AppendLine("</div>");
                zSb.AppendLine("</div>");
            }
            Lit_Annou.Text = zSb.ToString();
        }
        void Load_Staff(DateTime FromDate, DateTime ToDate, int Spec)
        {
            Lit_MonthTopStaff.Text = " " + FromDate.Month + "/" + FromDate.Year;

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HomePage.Get_ListStaff(FromDate, ToDate, Spec);
            if (zTable.Rows.Count > 0)
            {
                DataRow[] temp = zTable.Select("RowNumber <= 3 AND INCOME >0");
                DataTable zTop3 = new DataTable();

                #region [Top3]
                if (temp.Length > 0)
                {
                    zTop3 = zTable.Select("RowNumber <= 3 AND INCOME >0").CopyToDataTable();
                }

                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th colspan=2>Top 3</th>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("    <tbody>");

                string top3 = "";

                if (zTop3.Rows.Count > 0)
                {
                    for (int i = 0; i < zTop3.Rows.Count; i++)
                    {
                        DataRow r = zTop3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td><img src='Upload\\Image\\" + r["RowNumber"].ToString() + ".png' style='width:52px;' /></td>");
                        zSb.AppendLine("    <td><b>" + r["Name_Employee"].ToString() + "</br>");
                        zSb.AppendLine("    Doanh số: " + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</b></td>");
                        zSb.AppendLine("</tr>");

                        top3 += r["RowNumber"].ToString() + ",";
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\1.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\2.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\3.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                }
                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                Lit_Top3.Text = zSb.ToString();
                #endregion

                #region [Còn lại]
                DataTable zOver3 = new DataTable();
                if (zTop3.Rows.Count > 0)
                {
                    DataRow[] Array = zTable.Select("RowNumber NOT IN (" + top3.Remove(top3.LastIndexOf(",")) + ")");
                    if (Array.Length > 0)
                    {
                        zOver3 = Array.CopyToDataTable();
                    }
                }
                else
                {
                    zOver3 = zTable;
                }

                zSb = new StringBuilder();
                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th>Nhân viên</th>");
                zSb.AppendLine("        <th>Doanh số</th>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("    <tbody>");

                if (zOver3.Rows.Count > 0)
                {
                    for (int i = 0; i < zOver3.Rows.Count; i++)
                    {
                        DataRow r = zOver3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td>" + r["RowNumber"].ToString() + " : " + r["Name_Employee"].ToString() + "</td>");
                        zSb.AppendLine("    <td style='text-align:right'>" + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</td>");
                        zSb.AppendLine("</tr>");
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td colspan=2>Chưa có dữ liệu</td>");
                    zSb.AppendLine("</tr>");
                }

                zSb.AppendLine("    </tbody>");

                zSb.AppendLine("</table>");
                Lit_Other.Text = zSb.ToString();
                #endregion            
            }
        }
        void Load_Staff(DateTime FromDate, DateTime ToDate, string Department)
        {
            Lit_MonthTopStaff.Text = " " + FromDate.Month + "/" + FromDate.Year;

            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HomePage.Get_ListStaff(FromDate, ToDate, Department);
            if (zTable.Rows.Count > 0)
            {
                DataRow[] temp = zTable.Select("RowNumber <= 3 AND INCOME >0");
                DataTable zTop3 = new DataTable();

                #region [Top3]
                if (temp.Length > 0)
                {
                    zTop3 = zTable.Select("RowNumber <= 3 AND INCOME >0").CopyToDataTable();
                }

                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th colspan=2>Top 3</th>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("    <tbody>");

                string top3 = "";

                if (zTop3.Rows.Count > 0)
                {
                    for (int i = 0; i < zTop3.Rows.Count; i++)
                    {
                        DataRow r = zTop3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td><img src='Upload\\Image\\" + r["RowNumber"].ToString() + ".png' style='width:52px;' /></td>");
                        zSb.AppendLine("    <td><b>" + r["Name_Employee"].ToString() + "</br>");
                        zSb.AppendLine("    Doanh số: " + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</b></td>");
                        zSb.AppendLine("</tr>");

                        top3 += r["RowNumber"].ToString() + ",";
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\1.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\2.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\3.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                }
                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                Lit_Top3.Text = zSb.ToString();
                #endregion

                #region [Còn lại]
                DataTable zOver3 = new DataTable();
                if (zTop3.Rows.Count > 0)
                {
                    DataRow[] Array = zTable.Select("RowNumber NOT IN (" + top3.Remove(top3.LastIndexOf(",")) + ")");
                    if (Array.Length > 0)
                    {
                        zOver3 = Array.CopyToDataTable();
                    }
                }
                else
                {
                    zOver3 = zTable;
                }

                zSb = new StringBuilder();
                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th>Nhân viên</th>");
                zSb.AppendLine("        <th>Doanh số</th>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("    <tbody>");

                if (zOver3.Rows.Count > 0)
                {
                    for (int i = 0; i < zOver3.Rows.Count; i++)
                    {
                        DataRow r = zOver3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td>" + r["RowNumber"].ToString() + " : " + r["Name_Employee"].ToString() + "</td>");
                        zSb.AppendLine("    <td style='text-align:right'>" + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</td>");
                        zSb.AppendLine("</tr>");
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td colspan=2>Chưa có dữ liệu</td>");
                    zSb.AppendLine("</tr>");
                }

                zSb.AppendLine("    </tbody>");

                zSb.AppendLine("</table>");
                Lit_Other.Text = zSb.ToString();
                #endregion            
            }
        }
        void Load_Target(DateTime FromDate, DateTime ToDate, string Department, int Employee)
        {
            StringBuilder zSbTarget = new StringBuilder();
            StringBuilder zSbResult = new StringBuilder();

            int Result = 0, Doing = 0,
                Require = HomePage.Count_Require_Trade(FromDate, ToDate, Department, Employee),
                temp1 = HomePage.Count_Success_Sale(FromDate, ToDate, Department, Employee),
                temp2 = HomePage.Count_Success_Hire(FromDate, ToDate, Department, Employee),
                temp3 = HomePage.Count_Success_ShortTerm_1(FromDate, ToDate, Department, Employee),
                temp4 = HomePage.Count_Success_ShortTerm_2(FromDate, ToDate, Department, Employee);

            Doing = temp1 + temp2 + temp3 + temp4;
            if (Require != 0)
            {
                zSbTarget.AppendLine("<div class='TargetText'><span>" + Require + "</span></div>");
                if (Doing == 0)
                {
                    zSbResult.Append("Bạn chưa có giao dịch nào. Cố lên...Cố lên!!!");
                }

                Result = Doing - Require;
                if (Doing > 0 && Result < 0 && Result != 0)
                {
                    zSbResult.Append("Cố lên!!! Bạn cần thêm " + -Result + " giao dịch nữa.");
                }
                else if (Result == 0)
                {
                    zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                }
                else if (Result > 0)
                {
                    zSbResult.Append("WOW...Chúc mừng bạn đã vượt chỉ tiêu.");
                }
            }

            Lit_MonthRequire.Text = " tháng " + ToDate.Month.ToString();
            Lit_Require.Text = zSbTarget.ToString();
            Lit_Result.Text = zSbResult.ToString();
        }
        void Load_Panel(DateTime FromDate, DateTime ToDate, string Department, int Employee)
        {
            DataTable zTable = Categories_Data.ListDefine_Target_KPI();
            string Muctieu = "";
            foreach (DataRow r in zTable.Rows)
            {
                Muctieu += r["AutoKey"] + ",";
            }
            Lit_Income.Text = HomePage.Sum_Trade(FromDate, ToDate, Department, Employee).ToDoubleString();
            //Lit_TradeNeedApprove.Text = HomePage.Count_Trade_Need_Approved(FromDate, ToDate, Department, Employee).ToString();
            Lit_TradeNeedApprove.Text = HomePage.Count_Trade_Need_Approved(Convert.ToDateTime("2010-01-01 00:00:00"), ToDate, Department, Employee).ToString();
            //Lit_TradeNeedCollect.Text = HomePage.Count_Trade_Need_Collect(FromDate, ToDate, Department, Employee).ToString();
            Lit_TradeNeedCollect.Text = HomePage.Count_Trade_Need_Collect(Convert.ToDateTime("2010-01-01 00:00:00"), ToDate, Department, Employee).ToString();
            if (Employee != 0)
            {
                Lit_KPI.Text = Helper_Data.KPI_Employee_V3(FromDate, ToDate, Employee, Muctieu) + "%";
            }
            else
            {
                Lit_KPI.Text = Helper_Data.KPI_Company_V3(FromDate, ToDate, Department, Muctieu) + "%";
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int ViewTime = HID_NextPrev.Value.ToInt();
            if (ViewTime == 1)
            {
                FromDate = Convert.ToDateTime(HID_FromDate.Value);
                ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }
            if (ViewTime == -1)
            {
                FromDate = Convert.ToDateTime(HID_FromDate.Value);
                ToDate = Convert.ToDateTime(HID_ToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HID_FromDate.Value = FromDate.ToString();
                HID_ToDate.Value = ToDate.ToString();
            }

            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Spec = HttpContext.Current.Request.Cookies["UserLog"]["SpecKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string DepartmentRole = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
            {
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            }
            else
            {
                ViewDepartment = DepartmentRole;
            }

            Lit_TitleMonth.Text = ToDate.Month.ToString();
            Lit_Month.Text =ltMonth.Text = ToDate.Month.ToString();

            if (UnitLevel < 7)
            {
                Load_Target(FromDate, ToDate, ViewDepartment, 0);
                Load_Panel(FromDate, ToDate, ViewDepartment, 0);
            }
            else
            {
                Load_Target(FromDate, ToDate, ViewDepartment, Employee);
                Load_Panel(FromDate, ToDate, ViewDepartment, Employee);
            }

            if (UnitLevel <= 1)
            {
                Load_Staff(FromDate, ToDate, ViewDepartment);
            }
            else
            {
                Load_Staff(FromDate, ToDate, Spec);
            }


            //if (Spec != 1)
            //{
            //    if (UnitLevel < 7)
            //    {
            //        Load_Target(FromDate, ToDate, ViewDepartment, 0);
            //        Load_Panel(FromDate, ToDate, ViewDepartment, 0);
            //        Load_Staff(FromDate, ToDate, ViewDepartment);
            //    }
            //    else
            //    {
            //        Load_Target(FromDate, ToDate, ViewDepartment, Employee);
            //        Load_Panel(FromDate, ToDate, ViewDepartment, Employee);
            //        Load_Staff(FromDate, ToDate, ViewDepartment);
            //    }
            //}
            //else
            //{
            //    if (UnitLevel < 7)
            //    {
            //        Load_Target(FromDate, ToDate, ViewDepartment, 0);
            //        Load_Panel(FromDate, ToDate, ViewDepartment, 0);
            //        Load_Staff(FromDate, ToDate, ViewDepartment);
            //    }
            //    else
            //    {
            //        Load_Target(FromDate, ToDate, ViewDepartment, Employee);
            //        Load_Panel(FromDate, ToDate, ViewDepartment, Employee);
            //        Load_Staff(FromDate, ToDate, ViewDepartment);
            //    }
            //}
        }
        protected void btnSave_Message_Click(object sender, EventArgs e)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Key = HID_Meesage.Value.ToInt();

            Announce_Info zInfo = new Announce_Info(Key);
            zInfo.EmployeeKey = Employee;
            zInfo.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.CreatedDate = DateTime.Now;
            zInfo.Contents = txt_Contents.Value;
            zInfo.Save();

            foreach (ListItem i in DDL_Department.Items)
            {
                if (i.Selected && i.Value == "0")
                {
                    foreach (ListItem all in DDL_Department.Items)
                    {
                        Share_Permition_Info share = new Share_Permition_Info();
                        share.AssetKey = zInfo.ID;
                        share.EmployeeKey = all.Value.ToInt();
                        share.ObjectTable = "Announce";
                        share.Create();
                    }
                    break;
                }
                if (i.Selected)
                {
                    Share_Permition_Info share = new Share_Permition_Info();
                    share.AssetKey = zInfo.ID;
                    share.EmployeeKey = i.Value.ToInt();
                    share.ObjectTable = "Announce";
                    share.Create();
                }
            }

            Load_Message(Employee);
        }
        [WebMethod]
        public static ItemReturn GetRank(string FromDate, string ToDate, string Department)
        {
            DateTime zFromDate = Convert.ToDateTime(FromDate);
            DateTime zToDate = Convert.ToDateTime(ToDate);
            ItemReturn zResult = new ItemReturn();
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = HomePage.Get_ListStaff(zFromDate, zToDate, Department);
            if (zTable.Rows.Count > 0)
            {
                DataRow[] temp = zTable.Select("RowNumber <= 3 AND INCOME >0");
                DataTable zTop3 = new DataTable();

                #region [Top3]
                if (temp.Length > 0)
                {
                    zTop3 = zTable.Select("RowNumber <= 3 AND INCOME >0").CopyToDataTable();
                }

                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th colspan=2>Top 3</th>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("    <tbody>");

                string top3 = "";

                if (zTop3.Rows.Count > 0)
                {
                    for (int i = 0; i < zTop3.Rows.Count; i++)
                    {
                        DataRow r = zTop3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td><img src='Upload\\Image\\" + r["RowNumber"].ToString() + ".png' style='width:52px;' /></td>");
                        zSb.AppendLine("    <td><b>" + r["Name_Employee"].ToString() + "</br>");
                        zSb.AppendLine("    Doanh số: " + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</b></td>");
                        zSb.AppendLine("</tr>");

                        top3 += r["RowNumber"].ToString() + ",";
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\1.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\2.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td><img src='Upload\\Image\\3.png' style='width:52px;' /></td><td>&nbsp;</td>");
                    zSb.AppendLine("</tr>");
                }
                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                zResult.Result = zSb.ToString();
                #endregion

                #region [Còn lại]
                DataTable zOver3 = new DataTable();
                if (zTop3.Rows.Count > 0)
                {
                    DataRow[] rArray = zTable.Select("RowNumber NOT IN (" + top3.Remove(top3.LastIndexOf(",")) + ")");
                    if (rArray.Length > 0)
                    {
                        zOver3 = rArray.CopyToDataTable();
                    }
                }
                else
                {
                    zOver3 = zTable;
                }

                zSb = new StringBuilder();
                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th>Nhân viên</th>");
                zSb.AppendLine("        <th>Doanh số</th>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("    <tbody>");

                if (zOver3.Rows.Count > 0)
                {
                    for (int i = 0; i < zOver3.Rows.Count; i++)
                    {
                        DataRow r = zOver3.Rows[i];
                        zSb.AppendLine("<tr>");
                        zSb.AppendLine("    <td>" + r["RowNumber"].ToString() + " : " + r["Name_Employee"].ToString() + "</td>");
                        zSb.AppendLine("    <td style='text-align:right'>" + Convert.ToDouble(r["INCOME"]).ToString("n0") + "</td>");
                        zSb.AppendLine("</tr>");
                    }
                }
                else
                {
                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("        <td colspan=2>Chưa có dữ liệu</td>");
                    zSb.AppendLine("</tr>");
                }

                zSb.AppendLine("    </tbody>");

                zSb.AppendLine("</table>");
                zResult.Result2 = zSb.ToString();
                #endregion            
            }
            return zResult;
        }
        [WebMethod]
        public static string DeleteMessage(int id)
        {
            Announce_Info zInfo = new Announce_Info();
            zInfo.ID = id;

            Share_Permition_Info share = new Share_Permition_Info();
            share.Delete(id);
            if (share.Message == string.Empty)
            {
                zInfo.Delete();
            }
            else
            {
                return "Lỗi không xóa được";
            }

            if (zInfo.Message == string.Empty)
            {
                return "OK";
            }
            else
            {
                return "Lỗi không xóa được";
            }
        }

        [WebMethod]
        public static List<ItemWeb> HtmlDDL(string Spec)
        {
            List<ItemWeb> zList = new List<ItemWeb>();
            string sql = "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE SpecKey IN (" + Spec.Remove(Spec.LastIndexOf(",")) + ") ORDER BY DepartmentName";
            DataTable zTable = new SqlContext().GetData(sql);
            string value = "";
            string text = "--Tất cả--";

            foreach (DataRow r in zTable.Rows)
            {
                value += r["DepartmentKey"].ToString() + ",";
            }

            zList.Add(new ItemWeb
            {
                Value = value.Remove(value.LastIndexOf(',')),
                Text = text
            });

            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new ItemWeb
                {
                    Value = r["DepartmentKey"].ToString(),
                    Text = r["DepartmentName"].ToString()
                });
            }

            return zList;
        }

        [WebMethod]
        public static string SaveMessage(string Department, string NoiDung, string Key)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();

            Announce_Info zInfo = new Announce_Info(Key.ToInt());
            zInfo.EmployeeKey = Employee;
            zInfo.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zInfo.CreatedDate = DateTime.Now;
            zInfo.Contents = NoiDung;
            zInfo.Save();

            string[] array = Department.Split(',');
            foreach (string s in array)
            {
                Share_Permition_Info share = new Share_Permition_Info();
                share.AssetKey = zInfo.ID;
                share.EmployeeKey = s.ToInt();
                share.ObjectTable = "Announce";
                share.Create();
            }

            return string.Empty;
        }
    }
}