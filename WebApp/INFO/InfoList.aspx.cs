﻿using Lib.Config;
using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;

//Type 1 đã gửi, 2 chưa gửi
//KPI tham số categoryplan = 1 là từ công việc
//_UpdateAsset cap nhat san pham la 307
//_UpdatePhone cap nhat san pham la 329
namespace WebApp.INFO
{
    public partial class InfoList : System.Web.UI.Page
    {
        const int _UpdateAsset = 307;
        const int _UpdatePhone = 329;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
                InitView();
            }
        }
        protected void InitView()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            else
                ViewDepartment = Department;
            int Status = DDL_Status.SelectedValue.ToInt();
            int Type = DDL_Type.SelectedValue.ToInt();
            string ProjectKey = DDL_Project.SelectedValue;
            string CustomerName = txt_GuestName.Text.Trim();

            Tools.DropDown_DDL(DDL_Status, "SELECT ID, Name FROM TASK_Categories", false);
            Tools.DropDown_DDL(DDL_ProjectInput, "SELECT A.ProjectKey, A.ProjectName FROM PUL_Project A", false);
            Tools.DropDown_DDL(DDL_Project, "SELECT A.ProjectKey, A.ProjectName FROM PUL_Project A", false);
            Tools.DropDown_DDL(DDL_Legal, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 6", false);
            Tools.DropDown_DDL(DDL_Furnitur, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
            Tools.DropDown_DDL(DDL_StatusAsset, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);
            Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME FROM HRM_Employees WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
            Tools.DropDown_DDL(DDL_ToEmployee, "SELECT EmployeeKey, LastName + ' ' + FirstName AS NAME FROM HRM_Employees WHERE DepartmentKey IN (" + ViewDepartment + ") AND IsWorking = 2 ORDER BY LastName", false);
            Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);

            DataTable zTable = new DataTable();

            if (UnitLevel < 7)
            {
                DDL_Employee.Visible = true;
                DDL_Type.Visible = true;

                if (Type == 1)
                    zTable = Note_Data.ListInfo_Sent(ViewDepartment, 0, 500);
                else
                    zTable = Note_Data.ListInfo_New(ViewDepartment, 0, 500);
            }
            else
            {
                //making sure the previous selection has been cleared
                DDL_Employee.Visible = false;
                DDL_Employee.ClearSelection();
                DDL_Employee.Items.FindByValue(Employee.ToString()).Selected = true;
                DDL_Type.Visible = false;

                zTable = Note_Data.ListInfo_Sent(ViewDepartment, Employee, 500);
            }

            View_HtmlTable(zTable);
            View_HtmlSend(zTable);
        }

        #region [Search]
        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            SearchData();
        }
        void SearchData()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            DataTable zTable = new DataTable();
            int Status = DDL_Status.SelectedValue.ToInt();
            int TaskType = DDL_Type.SelectedValue.ToInt();
            int Employee = DDL_Employee.SelectedValue.ToInt();
            string CustomerName = txt_GuestName.Text.Trim();
            string ProjectKey = DDL_Project.SelectedValue;
            string Room = txt_Room.Text.Trim();
            int Category = DDL_Category.SelectedValue.ToInt();

            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;

            if (txt_FromDate.Text != string.Empty &&
                txt_ToDate.Text != string.Empty)
            {
                FromDate = Tools.ConvertToDate(txt_FromDate.Text);
                ToDate = Tools.ConvertToDate(txt_ToDate.Text);
                ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            }

            if (UnitLevel < 7)
            {
                if (TaskType == 1)
                    zTable = Note_Data.ListInfo_Sent(ProjectKey, txt_Name.Text.Trim(), CustomerName, Employee, Status, FromDate, ToDate, Room, Category);
                else
                    zTable = Note_Data.ListInfo_New(ProjectKey, txt_Name.Text.Trim(), CustomerName);
            }
            else
            {
                zTable = Note_Data.ListInfo_Sent(ProjectKey, txt_Name.Text.Trim(), CustomerName, Employee, Status, FromDate, ToDate, Room, Category);
            }

            View_HtmlTable(zTable);
            View_HtmlSend(zTable);

            ItemSearch_Info zISearch = new ItemSearch_Info();
            zISearch.Status = Status;
            zISearch.Type = TaskType;
            zISearch.Employee = Employee;
            zISearch.CustomerName = CustomerName;
            zISearch.ProjectKey = ProjectKey;
            Session["SearchInfo"] = zISearch;
        }
        #endregion

        protected void View_HtmlTable(DataTable TableSearch)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Thông tin</th>");
            zSb.AppendLine("        <th style='width:40%'>Địa chỉ</th>");
            zSb.AppendLine("        <th>Sản phẩm</th>");
            zSb.AppendLine("        <th>Tình trạng</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (TableSearch.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in TableSearch.Rows)
                {
                    string phone1 = r["Phone1"].ToString() == string.Empty ? string.Empty : r["Phone1"].ToString() + "<br />";
                    string phone2 = r["Phone2"].ToString() == string.Empty ? string.Empty : r["Phone2"].ToString() + "<br />";
                    string mail1 = r["Email1"].ToString() == string.Empty ? string.Empty : r["Email1"].ToString() + "<br />";
                    string mail2 = r["Email2"].ToString() == string.Empty ? string.Empty : r["Email2"].ToString();
                    string address1 = r["Address1"].ToString() == string.Empty ? string.Empty : "Liên hệ: " + r["Address1"].ToString() + "<br />";
                    string address2 = r["Address2"].ToString() == string.Empty ? string.Empty : "Thường trú: " + r["Address2"].ToString();
                    string status = r["Status"].ToString() == string.Empty ? string.Empty : "Xử lý: " + r["Status"].ToString() + "<br/>";
                    string employee = r["EmployeeName"].ToString() == string.Empty ? string.Empty : r["EmployeeName"].ToString();
                    string objectKey = r["ObjectKey"].ToString();
                    string objecttable = r["ObjectTable"].ToString();
                    string ProjectKey = r["ProductID"].ToString();
                    string link = "";
                    if (objectKey.ToInt() > 0)
                    {
                        link = "assetkey = '" + objectKey + "' assettype = '" + objecttable + "'";
                    }

                    zSb.AppendLine("            <tr project=" + ProjectKey + " assetid='" + r["Asset"].ToString() + "' id=" + r["Key"].ToString() + " relatekey=" + r["RelateKey"] + " assettype='" + r["AssetType"].ToString() + "' type=" + r["Type"].ToString() + " " + link + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>Họ tên: " + r["CustomerName"].ToString() + "<br/>Điện thoại: " + phone1 + phone2 + mail1 + mail2 + "</td>");
                    zSb.AppendLine("               <td>" + address1 + address2 + " </td>");
                    zSb.AppendLine("               <td>" + r["Product"].ToString() + "<br/>" + r["Asset"].ToString() + "<br/>PN:" + r["Room"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + status + employee + "</td>");
                    zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons' isEdit='true'><a class='red' href='#' btn='btndel'><i class='ace-icon fa fa-trash-o bigger-130'></i></a></div></td>");
                    zSb.AppendLine("            </tr>");
                }

                if (_Permitsion[1].ToInt() == 1)
                    Lit_Button.Text = ShowButton_Search() + ShowButton_Import() + ShowButton_Send();
                else
                    Lit_Button.Text = ShowButton_Search();
            }
            else
            {
                if (_Permitsion[1].ToInt() == 1)
                    Lit_Button.Text = ShowButton_Search() + ShowButton_Import();
                else
                    Lit_Button.Text = ShowButton_Search();
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_Table.Text = zSb.ToString();
        }
        protected void View_HtmlSend(DataTable TableSearch)
        {
            if (TableSearch != null && TableSearch.Rows.Count > 0)
            {
                DataView zView = new DataView(TableSearch);
                DataTable zTable = zView.ToTable(true, "ProductID", "Product");

                DDL_ProjectToSend.DataSource = zTable;
                DDL_ProjectToSend.DataTextField = "Product";
                DDL_ProjectToSend.DataValueField = "ProductID";
                DDL_ProjectToSend.DataBind();

                string zMess = "";
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    DataRow r = zTable.Rows[i];
                    DataTable tbl = TableSearch.Select("[ProductID] = '" + r["ProductID"].ToString() + "'").CopyToDataTable();
                    if (tbl.Rows.Count > 0)
                    {
                        zMess += "Dự án " + r["Product"].ToString().Trim() + " có " + tbl.Rows.Count + " thông tin <br/>";
                    }
                }

                Lit_mTitle.Text = "Kết quả tìm kiếm được là: <br/>" + zMess;
            }
            else
            {
                Lit_mTitle.Text = "Không tìm thấy kết quả phù hợp.";
            }
        }

        #region Button HTML
        string ShowButton_Search()
        {
            return @"
                    <a href = '#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mSearch'>
                        <i class='ace-icon fa fa-search icon-only bigger-110'></i>
                        Tìm lọc
                    </a>";
        }
        string ShowButton_Send()
        {
            return @"
                    <a href = '#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mSend'>
                        <i class='ace-icon fa fa-send'></i>
                        Gửi data
                    </a>";
        }
        string ShowButton_Import()
        {
            return @"
                    <a href = '#' class='btn btn-white btn-info btn-bold' data-toggle='modal' data-target='#mExcel'>
                        <i class='ace-icon fa fa-plus'></i>
                        Nhập mới data
                    </a>";
        }
        #endregion
        #region [Send Info]
        /*
         * vừa gửi và vừa chuyển tiếp thông tin
         * Key relate là infokey
         * Key là detailkey
         * khi lưu mới thi lưu infokey
         * khi chuyển tiếp thì update detailkey và insert relatekey
         * type 1 là chuyển, 2 là lưu mới
         * TASK_Excel_Detail là bảng chưa thông tin 
         * TASK_Note_Detail là bảng gửi cho nhân viên lấy thông tin liên kết từ excel,
         */
        void SendData()
        {
            DataTable zTable = new DataTable();
            int Type = DDL_Type.SelectedValue.ToInt();
            int Amount = txt_InfoNo.Text.ToInt();
            int Status = DDL_Status.SelectedValue.ToInt();
            int Employee = DDL_Employee.SelectedValue.ToInt();
            int Project = DDL_ProjectToSend.SelectedValue.ToInt();
            int Category = DDL_Category.SelectedValue.ToInt();
            string CustomerName = string.Empty;

            if (Type == 1)
            {
                zTable = Note_Data.ListInfo_Sent(Amount, Project, txt_Name.Text, CustomerName, Employee, Status, Category);
            }
            else
            {
                zTable = Note_Data.ListInfo_New(Amount, Project, txt_Name.Text, CustomerName, Category);
            }

            if (zTable.Rows.Count > 0)
            {
                #region [NoteInfo]
                Note_Info zInfo = new Note_Info();
                zInfo.SendFrom = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SendTo = DDL_ToEmployee.SelectedValue.ToInt();
                zInfo.Description = txt_Description.Text.Trim();
                zInfo.Product = DDL_Project.Text;
                zInfo.Amount = Amount;
                zInfo.NoteDate = DateTime.Now;
                zInfo.CreatedDate = DateTime.Now;
                zInfo.ModifiedDate = DateTime.Now;
                zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zInfo.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zInfo.Save();
                #endregion

                if (zInfo.Message != string.Empty)
                    Lit_Message.Text = "Lỗi gửi thông tin !";

                string Insert = "";
                string Update = "";
                ItemReturn zIResult = new ItemReturn();
                switch (Type)
                {
                    case 1://đã gửi
                        foreach (DataRow r in zTable.Rows)
                        {
                            Update += " UPDATE TASK_Note_Detail SET Description = StatusInfo, StatusInfo = 99 WHERE DetailKey = " + r["KEY"].ToString();
                            //99 chuyen thong tin
                            Insert += " INSERT INTO TASK_Note_Detail (NoteKey,InfoKey,StatusInfo) VALUES ('" + zInfo.NoteKey + "' ,'" + r["RelateKey"].ToString() + "' , '" + 6 + "') ";
                            //6 mặc định chưa hoàn thành trong bảng SYS_Categories
                            Insert += " INSERT INTO SYS_LogTranfer (OldStaff,NewStaff,ObjectTable,ObjectID,Note,CreatedDate) VALUES (" + DDL_Employee.SelectedValue + "," + DDL_ToEmployee.SelectedValue + ",N'TASK_Note_Detail'," + r["RelateKey"].ToString() + ",N'Chuyen Tiep',GETDATE())";
                        }

                        zIResult = CustomInsert.Exe(Insert);
                        if (zIResult.Message != string.Empty)
                        {
                            Lit_Message.Text = "Lỗi gửi chi tiết thông tin !" + zIResult.Message;
                            return;
                        }
                        zIResult = CustomInsert.Exe(Update);
                        if (zIResult.Message != string.Empty)
                        {
                            Lit_Message.Text = "Lỗi gửi chi tiết thông tin !'" + zIResult.Message;
                        }
                        break;

                    case 2://chưa gửi
                        foreach (DataRow r in zTable.Rows)
                        {
                            Insert += " INSERT INTO TASK_Note_Detail (NoteKey ,InfoKey ,StatusInfo  ) VALUES ('" + zInfo.NoteKey + "' ,'" + r["KEY"].ToString() + "' , '" + 6 + "') ";
                            //6 mặc định chưa hoàn thành trong bảng TASK_Categories
                            Update += " UPDATE TASK_Excel_Detail SET Status = 1 WHERE InfoKey =" + r["KEY"].ToString();
                        }

                        zIResult = CustomInsert.Exe(Insert);
                        if (zIResult.Message != string.Empty)
                        {
                            Lit_Message.Text = "Lỗi gửi chi tiết thông tin !" + zIResult.Message;
                            return;
                        }
                        zIResult = CustomInsert.Exe(Update);
                        if (zIResult.Message != string.Empty)
                        {
                            Lit_Message.Text = "Lỗi gửi chi tiết thông tin !'" + zIResult.Message;
                        }
                        break;

                    default:
                        Lit_Message.Text = "Không có thông tin gửi !";
                        break;
                }

                Lit_Message.Text = "Đã gửi thông tin thành công !";
            }
            else
            {
                Lit_Message.Text = "Truy xuất data để gửi đi không thành công !";
            }
        }
        protected void btnSend_ServerClick(object sender, EventArgs e)
        {
            Lit_Message.Text = "";
            SendData();
        }
        #endregion
        #region [Upload Excel]
        void UploadExcel()
        {
            string pathSave = "";
            string FileName = "";
            if (FU.HasFile)
            {
                try
                {
                    #region[Upload]   
                    string virtualPath;
                    string nFullPathName = "/Upload/Excel/";
                    virtualPath = HttpContext.Current.Request.ApplicationPath + nFullPathName;
                    string virtualPathRoot = HttpContext.Current.Request.ApplicationPath;
                    string pathRoot = Server.MapPath(virtualPathRoot);
                    pathSave = Server.MapPath(virtualPath) + DateTime.Now.ToString("yyyy-MM-dd");
                    string PathRead = pathRoot + nFullPathName + FU.FileName;

                    // Check Foder
                    DirectoryInfo nDir = new DirectoryInfo(pathSave);
                    if (!nDir.Exists)
                        nDir.Create();
                    // Check File

                    int LengFile = FU.PostedFile.ContentLength;
                    FileName = FU.FileName;

                    if (File.Exists(pathSave + "\\" + FileName))
                        File.Delete(pathSave + "\\" + FileName);

                    FU.SaveAs(pathSave + "\\" + FileName);
                    #endregion

                    #region [Import]
                    string Message = ImportExcel(pathSave + "\\" + FileName, Path.GetExtension(FU.FileName));
                    if (Message != string.Empty)
                        Lit_Message.Text = "<div class='alert alert-danger'>" + Message + "</div>";
                    else
                        Response.Redirect(Request.RawUrl);
                    #endregion
                }
                catch (Exception ex)
                {
                    Lit_Message.Text = "<div class='alert alert-danger'>" + ex.ToString() + "</div>";
                }
            }
            else
            {
                Lit_Message.Text = "<div class='alert alert-danger'>Bạn chưa chọn tập tin</div>";
            }
        }
        string ImportExcel(string Path, string Extension)
        {
            Project_Info zProject = new Project_Info(DDL_ProjectInput.SelectedValue.ToInt());
            string MainTable = zProject.MainTable;
            string ShortTable = zProject.ShortTable;

            DataTable zTable = Tools.ImportToDataTable(Path, "0");
            StringBuilder SQL = new StringBuilder();

            if (zTable != null &&
                zTable.Rows.Count != 0 &&
                zTable.Columns.Count > 1)
            {
                Excel_Info zExcelFile = new Excel_Info();
                zExcelFile.Date = DateTime.Now;
                zExcelFile.FileAttack = Path;
                zExcelFile.CreatedDate = DateTime.Now;
                zExcelFile.ModifiedDate = DateTime.Now;
                zExcelFile.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zExcelFile.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zExcelFile.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zExcelFile.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                zExcelFile.Create();

                if (zExcelFile.Message == string.Empty)
                {
                    StringBuilder zSb = new StringBuilder();
                    zSb.AppendLine("<table class='table table-hover table-bordered' id='tableInfomation'>");
                    zSb.AppendLine("   <thead>");
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <th>STT</th>");
                    zSb.AppendLine("        <th>Họ tên</th>");
                    zSb.AppendLine("        <th>SDT</th>");
                    zSb.AppendLine("        <th>Email</th>");
                    zSb.AppendLine("        <th>Địa chỉ</th>");
                    zSb.AppendLine("        <th>Dự án</th>");
                    zSb.AppendLine("        <th>Sản phẩm</th>");
                    zSb.AppendLine("    </tr>");
                    zSb.AppendLine("   </thead>");
                    zSb.AppendLine("        <tbody>");

                    int i = 1;
                    foreach (DataRow r in zTable.Rows)
                    {
                        string ProjectID = zProject.Key.ToString(); //r[1].ToString().Trim();
                        string Category = r[2].ToString().Trim();
                        string FullName = r[3].ToString().Trim();
                        string Phone1 = r[4].ToString().Trim();
                        string Phone2 = r[5].ToString().Trim();
                        string Email1 = r[6].ToString().Trim();
                        string Email2 = r[7].ToString().Trim();
                        string Address1 = r[8].ToString().Trim();
                        string Address2 = r[9].ToString().Trim();
                        string Project = r[11].ToString().Trim();
                        string AssetID = r[12].ToString().Trim();
                        string ID1 = r[13].ToString().Trim();
                        string ID2 = r[14].ToString().Trim();
                        string ID3 = r[15].ToString().Trim();
                        string Room = r[16].ToString().Trim();
                        string AreaWall = r[17].ToString().Trim();
                        string AreaWater = r[18].ToString().Trim();
                        string Price = r[19].ToString().Trim();
                        string Door = r[20].ToString().Trim();
                        string View = r[21].ToString().Trim();

                        zSb.AppendLine("            <tr>");
                        zSb.AppendLine("            <td>" + i++ + "</td>");
                        zSb.AppendLine("               <td>" + FullName + "</td>");
                        zSb.AppendLine("               <td>" + Phone1 + "<br>" + Phone2 + "</td>");
                        zSb.AppendLine("               <td>" + Email1 + "<br>" + Email2 + "</td>");
                        zSb.AppendLine("               <td><b>Thường trú:</b> " + Address1 + "<br><b>Liên hệ:</b> " + Address2 + "</td>");
                        zSb.AppendLine("               <td>" + Project + "</td>");
                        zSb.AppendLine("               <td>" + AssetID + "</td>");
                        zSb.AppendLine("            </tr>");

                        SQL.AppendLine(@" 
INSERT INTO TASK_Excel_Detail (
ExcelKey, InfoDate ,FullName ,
Phone1 ,Phone2 ,Email1 ,Email2 ,Address1 ,Address2, ProductID, Product ,
Asset, AssetType, AssetTable, ID1, ID2, ID3, AreaWall, AreaWater, Price, AssetCategory, Room, DirectionView, DirectionDoor, [Status], 
CreatedDate, CreatedBy, CreatedName, ModifiedDate, ModifiedBy, ModifiedName) 

VALUES ('" + zExcelFile.AutoKey + "','" + DateTime.Now + "', N'" + FullName.Trim()
+ "', N'" + Phone1 + "', N'" + Phone2 + "', N'" + Email1 + "', N'" + Email2 + "',N'" + Address1 + "', N'" + Address2 + "','" + DDL_ProjectInput.SelectedValue + "',N'" + Project
+ "', N'" + AssetID + "', N'" + ShortTable + "',N'" + MainTable + "',N'" + ID1 + "', N'" + ID2 + "', N'" + ID3 + "', N'" + AreaWall + "', N'" + AreaWater + "'," + Price.ToDouble()
+ "," + Category + ",'" + Room + "',N'" + View + "',N'" + Door + "',0,GETDATE(),"
+ HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + ",N'"
+ HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode() + "',GETDATE(),"
+ HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + ",N'"
+ HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode() + "') ");
                    }
                    zSb.AppendLine("        </tbody>");
                    zSb.AppendLine("</table>");
                    ItemReturn zIResult = CustomInsert.Exe(SQL.ToString());
                    if (zIResult.Message != string.Empty)
                    {
                        return "Lỗi nhập chi tiết excel không thành công !" + zIResult.Message;
                    }
                }
                else
                {
                    return "Lỗi nhập thông tin file excel không thành công !";
                }
            }
            else
            {
                return "Lỗi đọc file Excel !" + zTable.Rows[0].ToString();
            }
            return string.Empty;
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            Lit_Message.Text = "";
            UploadExcel();

            //sau khi upload load data mới upload chưa gửi
            DDL_Type.SelectedValue = "2";
            SearchData();
        }
        #endregion

        [WebMethod]
        public static ItemReturn GetTask(int ID, int TaskType, string Project)
        {
            string dropdown = Tools.Html_Dropdown_StatusTask("SELECT ID, Name, Parent FROM TASK_Categories WHERE Parent <> 0 ORDER BY ID");
            ItemReturn zResult = new ItemReturn();
            bool isExists = false;
            Excel_Detail_Info zInfo = new Excel_Detail_Info(ID, TaskType);
            //AssetType loại sản phẩm
            string AssetType = zInfo.AssetType;
            Product_Info zAsset = new Product_Info(zInfo.Asset.Trim(), AssetType, Project);
            ItemAsset zItem = zAsset.ItemAsset;
            ItemCustomer zGuest = null;

            if (zItem.AssetKey.ToInt() != 0)
            {
                DataTable zTableGuest = Owner_Data.List(zItem.AssetKey.ToInt(), AssetType);
                zGuest = new ItemCustomer();
                if (zTableGuest.Rows.Count > 0)
                {
                    isExists = true;
                    zGuest.CustomerName = zTableGuest.Rows[0]["CustomerName"].ToString();
                    zGuest.Phone1 = zTableGuest.Rows[0]["Phone1"].ToString();
                    zGuest.Phone2 = zTableGuest.Rows[0]["Phone2"].ToString();
                    zGuest.Address1 = zTableGuest.Rows[0]["Address1"].ToString();
                    zGuest.Address2 = zTableGuest.Rows[0]["Address2"].ToString();
                }
            }

            StringBuilder zSb = new StringBuilder();
            StringBuilder zSb2 = new StringBuilder();
            zSb2.AppendLine("<h4 class='modal-title blue'><i class='ace-icon fa fa-rss orange'></i> Xử lý thông tin <div class='widget-toolbar'><a href = '#' class='orange2' data-toggle='dropdown' aria-expanded='true'>Thực hiện<i class='ace-icon fa fa-chevron-down icon-on-right'></i></a>" + dropdown + "</div></h4>");

            if (isExists)
            {
                zSb.AppendLine("<div class='row'>");
                zSb.AppendLine("<div class='col-xs-6'>");
                #region [SP đã có]
                zSb.AppendLine(@"<div class='widget-box'>                   
                    <div class='widget-body'>
                        <div class='widget-header widget-header-flat'>
                            <h4 class='widget-title lighter'>Thông tin cũ</h4>
                        </div>
                        <div class='widget-main padding-4'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zItem.ProjectName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zItem.AssetID + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zGuest.CustomerName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zGuest.Phone1 + ", " + zGuest.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zGuest.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zGuest.Address2 + @"</div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>");
                #endregion
                zSb.AppendLine("</div>");
                zSb.AppendLine("<div class='col-xs-6'>");
                #region [SP xử lý]
                zSb.AppendLine(@"<div class='widget-box'>
                    <div class='widget-body'>
                        <div class='widget-header widget-header-flat'>
                            <h4 class='widget-title lighter'>Thông tin mới</h4>
                        </div>
                        <div class='widget-main padding-4'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value' id='needcheck'>" + zInfo.Asset + @"</div>
                                </div>                             
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>");
                #endregion
                zSb.AppendLine("</div>");
                zSb.AppendLine("</div>");
            }
            else
            {
                #region [SP xử lý]
                zSb.AppendLine(@"<div class='widget-box'>                  
                    <div class='widget-body'>
                        <div class='widget-main padding-0'>
                            <div class='profile-user-info profile-user-info-striped' style='width:100% !important'>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Dự án</div>
                                    <div class='profile-info-value'>" + zInfo.Product + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Sản phẩm</div>
                                    <div class='profile-info-value'>" + zInfo.Asset + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Họ tên</div>
                                    <div class='profile-info-value'>" + zInfo.FullName + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>SĐT</div>
                                    <div class='profile-info-value'>" + zInfo.Phone1 + ", " + zInfo.Phone2 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ liên lạc</div>
                                    <div class='profile-info-value'>" + zInfo.Address1 + @"</div>
                                </div>
                                <div class='profile-info-row'>
                                    <div class='profile-info-name'>Địa chỉ thường trú</div>
                                    <div class='profile-info-value'>" + zInfo.Address2 + @"</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>");
                #endregion
            }
            zResult.Result4 = zAsset.ItemAsset.ProjectKey;
            zResult.Result3 = zAsset.ItemAsset.AssetKey;
            zResult.Result2 = zSb2.ToString();
            zResult.Result = zSb.ToString();
            zResult.Message = zAsset.ItemAsset.AssetKey;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer GetOwner(int ID, int TaskType)
        {
            Excel_Detail_Info zInfo = new Excel_Detail_Info(ID, TaskType);
            ItemCustomer zGuest = new ItemCustomer();
            zGuest.CustomerName = zInfo.FullName;
            zGuest.Phone1 = zInfo.Phone1;
            zGuest.Phone2 = zInfo.Phone2;
            zGuest.Email1 = zInfo.Email1;
            zGuest.Address1 = zInfo.Address1;
            zGuest.Address2 = zInfo.Address2;
            return zGuest;
        }
        /// <summary>
        /// cap nhat chu nha
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static ItemReturn SaveOwner(int ID, int Status, int AssetKey, string AssetType, string CustomerName, string Phone1, string Phone2, string Email, string Address1, string Address2)
        {
            ItemReturn zResult = new ItemReturn();
            Note_Detail_Info zNote = new Note_Detail_Info(ID);
            zNote.StatusInfo = Status;
            zNote.DateRespone = DateTime.Now;
            zNote.UpdateStatus();

            Owner_Info zInfo = new Owner_Info();
            zInfo.CustomerName = CustomerName;
            zInfo.Phone1 = Phone1;
            zInfo.Phone2 = Phone2;
            zInfo.Email = Email;
            zInfo.Address1 = Address1;
            zInfo.Address2 = Address2;
            zInfo.AssetKey = AssetKey;
            zInfo.Create(AssetType);

            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "OK";

            return zResult;
        }
        /// <summary>
        /// Kiem tra sản phẩm đã có hay chưa
        /// </summary>
        /// <param name="AssetKey">Mã SP</param>
        /// <param name="AssetType">Loại SP</param>
        /// <returns></returns>
        [WebMethod]
        public static ItemAsset CheckAsset(int AssetKey, string AssetType)
        {
            Product_Info zAsset = new Product_Info(AssetKey, AssetType);
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }
        [WebMethod]
        public static ItemAsset CheckAssetID(string AssetID, string AssetType, string Project)
        {
            Product_Info zAsset = new Product_Info(AssetID, AssetType, Project);
            ItemAsset zItem = zAsset.ItemAsset;
            return zItem;
        }
        [WebMethod]
        public static ItemReturn SaveProduct(int ID, int StatusInfo, int TaskType, int AssetKey, string AssetType,
            string AssetStatus, string AssetRemind, string AssetPrice_VND, string AssetPriceRent_VND,
            string AssetPrice_USD, string AssetPriceRent_USD, string AssetFurnitur,
            string AssetPurpose, string AssetDescription, int UsdBan, int UsdThue, string Legal, string Project)
        {
            ItemReturn zResult = new ItemReturn();
            if (AssetType == string.Empty || AssetType == "0")
            {
                zResult.Message = "Liên hệ admin để xử lỗi này !. mã [AssetType]";
                return zResult;
            }

            Excel_Detail_Info zInfo = new Excel_Detail_Info(ID, TaskType);
            Product_Info zProduct = new Product_Info(AssetKey, AssetType, Project);
            ItemAsset zAsset = zProduct.ItemAsset;
            zAsset.AssetKey = AssetKey.ToString();
            zAsset.IsResale = "1";
            #region [Asset FixInfo]  kiem tra cheo thong tin neu bên data có thông tin cứng thì cấp nhật ko thì ko xử lý

            if (zInfo.ID1 != string.Empty &&
                zInfo.ID2 != string.Empty &&
                zInfo.ID3 != string.Empty &&
                zInfo.ProductID != string.Empty)
            {
                zAsset.ProjectKey = zInfo.ProductID;
                zAsset.CategoryKey = zInfo.AssetCategory;
                zAsset.AssetType = AssetType;
                zAsset.AssetID = zInfo.Asset;
                zAsset.ID1 = zInfo.ID1;
                zAsset.ID2 = zInfo.ID2;
                zAsset.ID3 = zInfo.ID3;
                zAsset.AreaWall = zInfo.AreaWall;
                zAsset.AreaWater = zInfo.AreaWater;
                zAsset.PricePurchase = zInfo.Price;
                zAsset.Room = zInfo.Room;
                zAsset.View = zInfo.DirectionView;
                zAsset.Door = zInfo.DirectionDoor;
            }
            if (zAsset.AssetID.Trim() == string.Empty)
                zAsset.AssetID = zInfo.Asset;
            #endregion
            #region [Asset SaleInfo]
            zAsset.Description = AssetDescription;
            zAsset.DateContractEnd = AssetRemind;
            zAsset.StatusKey = AssetStatus;

            double giabanUSD = 0;
            double.TryParse(AssetPrice_USD, out giabanUSD);
            double giabanVND = 0;
            double.TryParse(AssetPrice_VND, out giabanVND);
            double giathueUSD = 0;
            double.TryParse(AssetPriceRent_USD, out giathueUSD);
            double giathueVND = 0;
            double.TryParse(AssetPriceRent_VND, out giathueVND);

            if (UsdBan == 1)
            {
                zAsset.Price_USD = giabanUSD.ToString();
                zAsset.Price_VND = (giabanUSD * 22700).ToString();
            }
            else
            {
                zAsset.Price_USD = (giabanVND / 22700).ToString();
                zAsset.Price_VND = giabanVND.ToString();
            }

            if (UsdThue == 1)
            {
                zAsset.PriceRent_USD = giathueUSD.ToString();
                zAsset.PriceRent_VND = (giathueUSD * 22700).ToString();
            }
            else
            {
                zAsset.PriceRent_USD = (giathueVND / 22700).ToString();
                zAsset.PriceRent_VND = giathueVND.ToString();
            }

            zAsset.LegalKey = Legal;
            zAsset.FurnitureKey = AssetFurnitur;
            zAsset.PurposeKey = AssetPurpose;
            if (AssetPurpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = AssetPurpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }
            #endregion
            if (zProduct.ItemAsset.EmployeeKey.ToInt() == 0)
            {
                zAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            }

            zProduct.ItemAsset = zAsset;

            //thực hiện lưu
            zProduct.UpdateManagerment(AssetType);
            zProduct.Save(AssetType);

            if (zProduct.ItemAsset.Message != string.Empty)
            {
                zResult.Message = zProduct.ItemAsset.Message;
                zResult.Result = "Error";
                return zResult;
            }

            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            #region [Cap nhật chủ sở hữu]
            Owner_Info zOwner = new Owner_Info();
            zOwner.CustomerName = zInfo.FullName;
            zOwner.Phone1 = zInfo.Phone1;
            zOwner.Phone2 = zInfo.Phone2;
            zOwner.Email = zInfo.Email1;
            zOwner.Address1 = zInfo.Address1;
            zOwner.Address2 = zInfo.Address2;
            zOwner.AssetKey = zProduct.ItemAsset.AssetKey.ToInt();
            zOwner.Create(AssetType);

            if (zOwner.Message != string.Empty)
            {
                zResult.Message = "Lỗi xử lý chủ nhà !. " + zOwner.Message;
                zResult.Result = "Error";
                return zResult;
            }
            #endregion

            //cap nhat nguoc lai nguon tin de lien ket san pham
            Note_Detail_Info zNote_Detail = new Note_Detail_Info(ID);
            zNote_Detail.DetailKey = zInfo.DetailKey;
            zNote_Detail.StatusInfo = StatusInfo;
            zNote_Detail.DateRespone = DateTime.Now;
            zNote_Detail.ObjectKey = zAsset.AssetKey.ToInt();
            zNote_Detail.ObjectTable = AssetType;
            zNote_Detail.UpdateStatus();

            string Mess = SaveKPI_Asset(AssetKey, zAsset.AssetID, EmployeeKey, DepartmentKey, AssetType, Project.ToInt(), AssetDescription, _UpdateAsset);
            Mess = SaveKPI_Phone(zNote_Detail.NoteKey, zNote_Detail.DetailKey, zNote_Detail.InfoKey, EmployeeKey, DepartmentKey, StatusInfo, string.Empty);

            if (zNote_Detail.Message != string.Empty)
            {
                zResult.Message = "Lỗi xử lý nguồn tin";
                zResult.Result = "Error";
                return zResult;
            }

            zResult.Message = "OK";
            zResult.Result = zProduct.ItemAsset.AssetKey;
            return zResult;
        }
        /// <summary>
        /// luu thong tin thành khach hang
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Status"></param>
        /// <param name="CustomerName"></param>
        /// <param name="Phone1"></param>
        /// <param name="Phone2"></param>
        /// <param name="Email"></param>
        /// <param name="Address1"></param>
        /// <param name="Address2"></param>
        /// <returns></returns>
        [WebMethod]
        public static ItemReturn SaveCustomer(int ID, int Status, string CustomerName, string Phone1, string Phone2, string Email, string Address1, string Address2)
        {
            ItemReturn zResult = new ItemReturn();
            Note_Detail_Info zNote = new Note_Detail_Info(ID);
            zNote.StatusInfo = Status;
            zNote.DateRespone = DateTime.Now;
            zNote.UpdateStatus();
            //
            Excel_Detail_Info zExcel = new Excel_Detail_Info(zNote.InfoKey);
            Customer_Info zCustomer = new Customer_Info();
            zCustomer.CategoryKey = 0;
            zCustomer.FirstName = zExcel.FirstName.Trim();
            zCustomer.LastName = zExcel.LastName.Trim();
            zCustomer.CustomerName = CustomerName;
            zCustomer.Email1 = Email;
            zCustomer.Phone1 = Phone1;
            zCustomer.Phone2 = Phone2;
            zCustomer.Address1 = Address1;
            zCustomer.Address2 = Address2;
            zCustomer.ID = Phone1;
            zCustomer.EmployeeKey = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);
            zCustomer.DepartmentKey = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"]);
            zCustomer.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zCustomer.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zCustomer.ModifiedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zCustomer.CreatedName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            zCustomer.Save();

            if (zCustomer.Message != string.Empty)
                zResult.Message = zCustomer.Message;
            else
                zResult.Message = "OK";

            return zResult;
        }
        /// <summary>
        /// luu tinh trang thong tin
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        [WebMethod]
        public static ItemReturn SaveTask(int ID, int Status)
        {
            ItemReturn zResult = new ItemReturn();
            Note_Detail_Info zNote = new Note_Detail_Info(ID);
            zNote.DetailKey = ID;
            zNote.StatusInfo = Status;
            zNote.DateRespone = DateTime.Now;
            zNote.UpdateStatus();
            if (zNote.Message != string.Empty)
            {
                zResult.Message = zNote.Message;
                zResult.Result = "Error-1";
            }
            else
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                zResult.Message = "OK";
                string Mess = SaveKPI_Phone(zNote.NoteKey, zNote.DetailKey, zNote.InfoKey, EmployeeKey, DepartmentKey, Status, string.Empty);
            }
            return zResult;
        }
        [WebMethod]
        public static string DeleteTask(int ID)
        {
            int Result = new SqlContext().GetObject("DELETE TASK_Note_Detail WHERE DetailKey =" + ID).ToInt();
            if (Result <= 0)
                return "Error";
            else
                return "OK";
        }
        [WebMethod]
        public static ItemReturn GetNearestTask(string AssetID)
        {
            ItemReturn zResult = new ItemReturn();
            DataTable zTable = Note_Data.GetNearestTask(0, AssetID, 0);
            if (zTable.Rows.Count > 0)
            {
                DataRow r = zTable.Rows[0];
                zResult.Message = r["EmployeeName"].ToString() + " : " + r["DateRespone"].ToDateTimeString() + " : " + r["Respone"].ToString();
            }
            else
            {
                zResult.Message = "Chưa có thông tin";
            }
            return zResult;
        }

        #region [KPI]
        public static string SaveKPI_Asset(int AssetKey, string AssetID, int EmployeeKey, int DepartmentKey, string AssetType, int ProjectKey, string Description, int Category)
        {
            string MessageKPI = Helper.KPI_Asset(AssetKey, AssetID, AssetType, ProjectKey, EmployeeKey, DepartmentKey, DateTime.Now, Description, Category, 1);
            return MessageKPI;
        }
        public static string SaveKPI_Phone(int NoteKey, int DetailKey, int InfoKey, int EmployeeKey, int DepartmentKey, int Status, string Description)
        {
            string MessageKPI = Helper.KPI_Phone(NoteKey, DetailKey, InfoKey, EmployeeKey, DepartmentKey, Status, Description, DateTime.Now);
            return MessageKPI;
        }
        #endregion
        #region [Check roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        protected void CheckRole()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SYS04";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
        }
        #endregion
    }
}