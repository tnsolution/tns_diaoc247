﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
//135 tap tin khác > nhưng ky gửi là hình ảnh
namespace WebApp.INFO
{
    public partial class InfoProductEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_AssetKey.Value = Request["ID"];
                if (Request["Type"] != null)
                    HID_AssetType.Value = Request["Type"];

                Tools.DropDown_DDL(DDL_Furnitur, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_Legal, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 6", false);
                Tools.DropDown_DDL(DDL_Status, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                LoadAsset();
                LoadAllow();
                LoadPicture();
                LoadGuest();
                CheckRoles();
            }
        }

        void LoadAsset()
        {
            Product_Info zInfo = new Product_Info(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            txt_AssetID.Text = zInfo.ItemAsset.AssetID.Trim();
            txt_Id1.Text = zInfo.ItemAsset.ID1;
            txt_Id2.Text = zInfo.ItemAsset.ID2;
            txt_Id3.Text = zInfo.ItemAsset.ID3;
            txt_DateContractEnd.Value = zInfo.ItemAsset.DateContractEnd;
            txt_PriceVND.Value = zInfo.ItemAsset.Price_VND;
            txt_PriceRentVND.Value = zInfo.ItemAsset.PriceRent_VND;
            txt_PriceUSD.Value = zInfo.ItemAsset.Price_USD;
            txt_PriceRentUSD.Value = zInfo.ItemAsset.PriceRent_USD;
            txt_Room.Value = zInfo.ItemAsset.Room;
            txt_WallArea.Value = zInfo.ItemAsset.AreaWall;
            txt_WaterArea.Value = zInfo.ItemAsset.AreaWater;
            txt_Description.Value = zInfo.ItemAsset.Description;
            txt_WebTitle.Value = zInfo.ItemAsset.WebTitle;
            txt_WebContent.Value = zInfo.ItemAsset.WebContent;

            DDL_Category.SelectedValue = zInfo.ItemAsset.CategoryKey.ToString();
            DDL_Project.SelectedValue = zInfo.ItemAsset.ProjectKey.ToString();
            DDL_WebPublished.SelectedValue = zInfo.ItemAsset.WebPublished;
            DDL_Legal.SelectedValue = zInfo.ItemAsset.LegalKey;
            DDL_DirectionDoor.SelectedValue = zInfo.ItemAsset.Door;
            DDL_DirectionView.SelectedValue = zInfo.ItemAsset.View;
            DDL_Furnitur.SelectedValue = zInfo.ItemAsset.FurnitureKey;
            DDL_Status.SelectedValue = zInfo.ItemAsset.StatusKey;

            HID_Purpose.Value = zInfo.ItemAsset.PurposeKey;
            if (zInfo.ItemAsset.Hot.ToInt() == 5)
                chkHot.Checked = true;

            Lit_TitlePage.Text = "Cập nhật thông tin: " + zInfo.ItemAsset.AssetID;

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ItemAsset.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ItemAsset.ModifiedDate + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }
        void LoadPicture()
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemDocument> zList = Document_Data.List(135, HID_AssetKey.Value.ToInt());
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<li id='" + item.FileKey + "'>");
                zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageThumb + "' />");
                zSb.AppendLine("            <div class='text'>");
                zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                zSb.AppendLine("            </div>");
                zSb.AppendLine("    </a>");
                zSb.AppendLine("    <div class='tools tools-bottom'><a href='#' btn='btnDeleteImg'><i class='ace-icon fa fa-times red'></i>Xóa</a></div>");
                zSb.AppendLine("</li>");
            }

            Lit_ListPicture.Text = zSb.ToString();
        }
        void LoadAllow()
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblShare'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>Chia sản phẩm cho </div><div class='profile-info-value'>" + zTable.Rows[i]["EmployeeName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            Lit_AllowSearch.Text = zSb.ToString();
        }
        void LoadGuest()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTableOwners = Owner_Data.List(HID_AssetType.Value, HID_AssetKey.Value.ToInt());
            zSb.AppendLine("<table class='table  table-bordered table-hover' id='tblGuest'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Họ tên</th>");
            zSb.AppendLine("        <th>ĐT</th>");
            zSb.AppendLine("        <th>Email</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Ngày cập nhật</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTableOwners.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTableOwners.Rows)
                {
                    zSb.AppendLine("            <tr id=" + r["OwnerKey"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["CustomerName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Email1"].ToString() + "</td>");
                    zSb.AppendLine("               <td>Liên lạc: " + r["Address1"].ToString() + " <br/>Thường trú: " + r["Address2"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CreatedDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteGuest' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Owner.Text = zSb.ToString();
        }

        #region [Chủ nhà]
        [WebMethod]
        public static ItemCustomer GetGuest(string Type, int AutoKey)
        {
            Owner_Info zInfo = new Owner_Info(Type, AutoKey);
            ItemCustomer zItem = new ItemCustomer();
            zItem.AssetKey = zInfo.AssetKey.ToString();
            zItem.OwnerKey = zInfo.OwnerKey.ToString();
            zItem.CustomerName = zInfo.CustomerName;
            zItem.Phone1 = zInfo.Phone1;
            zItem.Phone2 = zInfo.Phone2;
            zItem.Email1 = zInfo.Email;
            zItem.Address1 = zInfo.Address1;
            zItem.Address2 = zInfo.Address2;
            zItem.Type = zInfo.Type;
            return zItem;
        }
        [WebMethod]
        public static ItemResult SaveGuest(string Type, int AutoKey, int AssetKey, string CustomerName, string Phone, string Phone2, string Email, string Address, string Address2)
        {
            ItemResult zResult = new ItemResult();
            Owner_Info zInfo = new Owner_Info(Type, AutoKey);
            zInfo.AssetKey = AssetKey;
            zInfo.CustomerName = CustomerName;
            zInfo.Phone1 = Phone.Trim();
            zInfo.Phone2 = Phone2.Trim();
            zInfo.Email = Email.Trim();
            zInfo.Address1 = Address.Trim();
            zInfo.Address2 = Address2.Trim();
            if (zInfo.OwnerKey > 0)
                zInfo.Update(Type);
            else
                zInfo.Create(Type);
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.OwnerKey.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemResult DeleteGuest(string Type, int AutoKey, int AssetKey)
        {
            ItemResult zResult = new ItemResult();
            Owner_Info zInfo = new Owner_Info();
            zInfo.OwnerKey = AutoKey;
            zInfo.AssetKey = AssetKey;
            zInfo.Delete(Type);
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetListGuest(string Type, int AssetKey)
        {
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            List<ItemCustomer> ListAsset = zTable.DataTableToList<ItemCustomer>();
            return ListAsset.ToArray();
        }
        #endregion

        #region [Share]
        [WebMethod]
        public static ItemResult SaveShare(int AssetKey, string Type, int EmployeeKey)
        {
            ItemResult zResult = new ItemResult();
            Share_Permition_Info zInfo = new Share_Permition_Info(AssetKey, EmployeeKey, Type);
            if (zInfo.AutoKey > 0)
            {
                zResult.Message = "Trùng thông tin, vui lòng chọn lại";
                return zResult;
            }

            zInfo.ObjectTable = Type;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.AssetKey = AssetKey;
            zInfo.Create();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemWeb[] GetShare(int AssetKey, string Type)
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
            List<ItemWeb> ListAsset = new List<ItemWeb>();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb iwe = new ItemWeb();
                iwe.Value = zTable.Rows[i]["AutoKey"].ToString();
                iwe.Text = zTable.Rows[i]["EmployeeName"].ToString();

                ListAsset.Add(iwe);
            }
            return ListAsset.ToArray();
        }
        [WebMethod]
        public static ItemResult DeleteShare(int AutoKey)
        {
            ItemResult zResult = new ItemResult();
            Share_Permition_Info zInfo = new Share_Permition_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        #endregion

        #region [File]
        void UploadImg()
        {
            int zCategoryKey = 135;
            int zProductKey = HID_AssetKey.Value.ToInt();
            string zSQL = "";
            string zPath = "~/Upload/File/" + HID_AssetType.Value + "/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                    string zFileThumb = zFileName + "_thumb" + Path.GetExtension(zInputFile.FileName);
                    string zFileLarge = Path.GetFileName(zInputFile.FileName);

                    string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                    string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                    string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                    string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                    if (File.Exists(zFilePathLarge))
                        File.Delete(zFilePathLarge);
                    zInputFile.SaveAs(zFilePathLarge);

                    Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                    System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(150, 150));
                    objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('" + HID_AssetType.Value + "','" + zProductKey + "','" + zCategoryKey + "',N'" + zFileLarge + "',N'" + zFileUrlThumb + "',N'" + zFileUrlLarge + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadPicture();
                }
            }
        }
        protected void btnUploadImg_Click(object sender, EventArgs e)
        {
            UploadImg();
        }
        #endregion

        [WebMethod]
        public static ItemResult DeleteFile(int FileKey)
        {
            ItemResult zResult = new ItemResult();
            Document_Info zInfo = new Document_Info();
            zInfo.FileKey = FileKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveProduct(int AssetKey, string Type, string Project, string Category,
            string AssetID, string ID1, string ID2, string ID3, string Hot,
            string Price_VND, string PriceRent_VND, string Price_USD, string PriceRent_USD,
            string AreaWall, string AreaWater, string Legal, string Furnitur, string Door, string View,
            string Remind, string Room, string Status, string Purpose, string Description,
            string WebPublish, string WebTitle, string WebContent)
        {
            ItemResult zResult = new ItemResult();
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            zInfo.ItemAsset.ProjectKey = Project;
            zInfo.ItemAsset.CategoryKey = Category;
            zInfo.ItemAsset.AssetID = AssetID.Trim();
            zInfo.ItemAsset.ID1 = ID1;
            zInfo.ItemAsset.ID2 = ID2;
            zInfo.ItemAsset.ID3 = ID3;
            zInfo.ItemAsset.Hot = Hot;
            zInfo.ItemAsset.Price_USD = Price_USD;
            zInfo.ItemAsset.PriceRent_USD = PriceRent_USD;
            zInfo.ItemAsset.Price_VND = Price_VND;
            zInfo.ItemAsset.PriceRent_VND = PriceRent_VND;
            zInfo.ItemAsset.AreaWall = AreaWall;
            zInfo.ItemAsset.AreaWater = AreaWater;
            zInfo.ItemAsset.LegalKey = Legal;
            zInfo.ItemAsset.FurnitureKey = Furnitur;
            zInfo.ItemAsset.Door = Door;
            zInfo.ItemAsset.View = View;
            zInfo.ItemAsset.DateContractEnd = Remind;
            zInfo.ItemAsset.Room = Room;
            zInfo.ItemAsset.StatusKey = Status;
            zInfo.ItemAsset.PurposeKey = Purpose;
            zInfo.ItemAsset.Description = Description.Trim();
            zInfo.ItemAsset.WebContent = WebContent;
            zInfo.ItemAsset.WebPublished = WebPublish;
            zInfo.ItemAsset.WebTitle = WebTitle;
            if (Purpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = Purpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zInfo.ItemAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }

            zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save(Type);
            return zResult;
        }

        [WebMethod]
        public static ItemResult DeleteProduct(string Type, int AssetKey)
        {
            Product_Info zInfo = new Product_Info();
            zInfo.ItemAsset.AssetKey = AssetKey.ToString();
            zInfo.Delete(Type);
            ItemResult zResult = new ItemResult();
            zResult.Message = zInfo.ItemAsset.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult InitProduct(string Type, string Project, string AssetID, string Note)
        {
            string table = "";
            switch (Type)
            {
                case "PUL_Resale_Apartment":
                    table = "ResaleApartment";
                    break;

                case "PUL_Resale_House":
                    table = "ResaleHouse";
                    break;
            }

            ItemResult zResult = new ItemResult();
            Product_Info zInfo = new Product_Info(AssetID, table);
            if (zInfo.ItemAsset.AssetKey.ToInt() > 0)
            {
                zResult.Message = "Đã có sản phẩm này [" + zInfo.ItemAsset.AssetID + "] !.";
            }
            else
            {
                zInfo.ItemAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zInfo.ItemAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

                zInfo.ItemAsset.AssetType = table;
                zInfo.ItemAsset.ProjectKey = Project;
                zInfo.ItemAsset.Description = Note;
                zInfo.ItemAsset.AssetID = AssetID;
                zInfo.Create(table);
            }
            zResult.Result = zInfo.ItemAsset.AssetKey;
            zResult.Result2 = zInfo.ItemAsset.AssetType;
            return zResult;
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[3].ToInt() == 1)
            {
                Lit_Button.Text = @" <button type='button' class='btn btn-white btn-warning btn-bold' id='btnDelete'><i class='ace-icon fa fa-trash-o orange2'></i>Xóa</button>";
            }
        }
        #endregion
    }
}