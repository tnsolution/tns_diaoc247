﻿$(function () {
    //kiem tra khi load
    $.ajax({
        type: "POST",
        url: "/SAL/InfoProductView.aspx/CheckReminder",
        data: JSON.stringify({
            "AssetKey": Page.getUrlParameter("ID"),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                $("[id$=HID_RemindKey]").val(msg.d.Result);
                Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                $("#btnExeReminder").show();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });

    $('#scroll').ace_scroll({
        size: 450
    });
    //mo rong tat ca accordion
    $(".toggle-accordion").on("click", function () {
        var accordionId = $(this).attr("accordion-id"),
          numPanelOpen = $(accordionId + ' .collapse.in').length;

        $(this).toggleClass("active");

        if (numPanelOpen == 0) {
            openAllPanels(accordionId);
        } else {
            closeAllPanels(accordionId);
        }
    })

    openAllPanels = function (aId) {
        console.log("setAllPanelOpen");
        $(aId + ' .panel-collapse:not(".in")').collapse('show');
    }
    closeAllPanels = function (aId) {
        console.log("setAllPanelclose");
        $(aId + ' .panel-collapse.in').collapse('hide');
    }

    $(".iframe").colorbox({ iframe: true, width: "100%", height: "100%" });
    $("#btnExeReminder").hide();
    $("#btnExeReminder").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/InfoProductView.aspx/ExeReminder",
            data: JSON.stringify({
                "RemindKey": $("[id$=HID_RemindKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Result == 1) {
                    Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                    $("#btnExeReminder").hide();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });

    $("#tblExtraData tbody").on("click", "tr", function () {
        var trid = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('fid');
        //load data on click
        $.ajax({
            type: "POST",
            url: "/SAL/InfoProductView.aspx/GetAsset",
            data: JSON.stringify({
                "AssetKey": trid,
                "Type": type
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                var giaban = msg.d.Price_USD + " (USD) " + msg.d.Price_VND + " (VNĐ)";
                var gaithue = msg.d.PriceRent_USD + " (USD) " + msg.d.PriceRent_VND + " (VNĐ)";

                $("#tieudetrang").text(msg.d.AssetID);
                $("#tinhtrang").text(msg.d.Status);
                $("#giaban").text(giaban);
                $("#giathue").text(gaithue);
                $("#noithat").text(msg.d.Furniture);
                $("#nhucau").text(msg.d.Purpose);
                $("#ghichu").text(msg.d.Description);
                $("#duan").text(msg.d.ProjectName);
                $("#masp").text(msg.d.AssetID);
                $("#loaisp").text(msg.d.CategoryName);
                $("#sophong").text(msg.d.Room);

                $("#ngaylienhe").text(msg.d.DateContractEnd);
                $("#dttimtuong").text(msg.d.AreaWall);
                $("#dtthongthuy").text(msg.d.AreaWater);
                $("#huongview").text(msg.d.View);
                $("#huongcua").text(msg.d.Door);

                $("#ghichu").text(msg.d.Description);
                $("#tieude").text(msg.d.WebTitle);
                $("#xuatban").text(msg.d.WebPublished);
                $("#noidung").text(msg.d.WebContent);

                //load chu nha on click
                $.ajax({
                    type: "POST",
                    url: "/SAL/InfoProductView.aspx/GetOwner",
                    data: JSON.stringify({
                        "AssetKey": trid,
                        "Type": type,
                        "EmployeeKey": msg.d.EmployeeKey,
                        "EmployeeName": msg.d.EmployeeName
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        $("#chunha").empty();
                        $("#chunha").append($(msg.d));
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });

                if (msg.d.Edit == 0) {
                    $("#btnEdit").hide();
                }
                else {
                    $("#btnEdit").show();
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showNotiMessageError("Lỗi,", "Vui lòng liên hệ Admin");
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        //load file on click
        $.ajax({
            type: "POST",
            url: "/SAL/InfoProductView.aspx/GetFile",
            data: JSON.stringify({
                "AssetKey": trid,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                $("#hinhanh").empty();
                $("#hinhanh").append($(msg.d));
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        //kiem tra ngay lien he on click
        $.ajax({
            type: "POST",
            url: "/SAL/InfoProductView.aspx/CheckReminder",
            data: JSON.stringify({
                "AssetKey": trid,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    $("[id$=HID_RemindKey]").val(msg.d.Result);
                    Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                    $("#btnExeReminder").show();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        $("[id$=HID_AssetKey]").val(trid);
        $("[id$=HID_AssetType]").val(type);
    });


    $("#btnSearch").click(function () {
        var Category = $("[id$=DDL_Category]").val();
        var Purpose = $("[id$=DDL_Purpose]").val();
        if (Category == null)
            Category = 0;
        if (Purpose == null)
            Purpose = "";

        $.ajax({
            type: "POST",
            url: "/SAL/InfoProductView.aspx/SearchAsset",
            data: JSON.stringify({
                "Project": $("[id$=HID_ProjectKey]").val(),
                "AssetID": $("#txt_Name").val(),
                "Room": $("#txt_Bed").val(),
                "Category": Category,
                "Purpose": Purpose,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $('#tblExtraData tbody').empty();
                $('#tblExtraData tbody').append($(msg.d));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnEdit").click(function () {
        $('.se-pre-con').fadeIn('slow');
        var link = '/SAL/ProductEdit.aspx?ID=' + $("[id$=HID_AssetKey]").val() + '&Type=' + $("[id$=HID_AssetType]").val();
        window.location = link;
    });
});