﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfoView.aspx.cs" Inherits="WebApp.INFO.InfoView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Xử lý thông tin
                <asp:Literal ID="Lit_InfoName" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <span class="tools pull-right">
                        <asp:Literal ID="Lit_Button" runat="server"></asp:Literal>
                        <a type="button" class="btn btn-white btn-default btn-bold" href="InfoList.aspx">
                            <i class="ace-icon fa fa-reply blue"></i>
                            Về danh sách
                        </a>
                    </span>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="widget-box transparent">
                    <div class="widget-header widget-header-small">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fa fa-rss orange"></i>
                            Chi tiết thông tin đang xử lý
                        </h4>
                        <div class="widget-toolbar">
                            <a href="#" class="orange2" data-toggle="dropdown" aria-expanded="true">Thực hiện															<i class="ace-icon fa fa-chevron-down icon-on-right"></i></a>
                            <asp:Literal ID="Lit_Dropdowns" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main padding-2">
                            <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Dự án</div>
                                    <div class="profile-info-value">
                                        <asp:Literal ID="txt_ProjectName" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Sản phẩm</div>
                                    <div class="profile-info-value">
                                        <asp:Literal ID="txt_AssetName" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Họ tên</div>
                                    <div class="profile-info-value" id="txtName">
                                        <asp:Literal ID="txt_CustomerName" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">SĐT</div>
                                    <div class="profile-info-value">
                                        <asp:Literal ID="txt_Phone" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Địa chỉ liên lạc</div>
                                    <div class="profile-info-value" id="txtAddress1">
                                        <asp:Literal ID="txt_Address1" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Địa chỉ thường trú</div>
                                    <div class="profile-info-value" id="txtAddress2">
                                        <asp:Literal ID="txt_Address2" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Tình trạng</div>
                                    <div class="profile-info-value">
                                        <asp:Literal ID="txt_Status" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Nhân viên xử lý</div>
                                    <div class="profile-info-value">
                                        <asp:Literal ID="txt_EmployeeName" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-6"></div>
                <%--<div class="col-xs-12">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#process">
                                    <i class="blue ace-icon fa fa-adjust bigger-110"></i>
                                    Xử lý thông tin
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#relate">
                                    <i class="blue ace-icon fa fa-info bigger-110"></i>
                                    Thông tin liên quan
                                    <span class="badge badge-danger icon-animated-vertical">
                                        <asp:Literal ID="txt_RelateInfo" runat="server"></asp:Literal></span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="process" class="tab-pane in active">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <asp:DropDownList ID="DDL_Status" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Phản hồi--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" id="Product">
                                        <asp:DropDownList ID="DDL_Table" class="form-control select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Sản phẩm là--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" id="Project">
                                        <asp:DropDownList ID="DDL_Project" class="form-control select2" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" id="Category">
                                        <asp:DropDownList ID="DDL_Category" CssClass="form-control select2" runat="server">
                                            <asp:ListItem Value="0" Text="--Chọn loại sản phẩm--" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="relate" class="tab-pane">
                                <h6>Các thông tin liên quan cùng mã sản phẩm đã có trong phần mềm !.</h6>
                                <asp:Literal ID="Lit_Relate" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>
            <div class="col-xs-4">
                <div class="widget-box transparent">
                    <div class="widget-header widget-header-small">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fa fa-rss orange"></i>
                            Thông tin liên quan nếu có
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mUpdateInfo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Cập nhật nguồn thông tin</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Họ và tên(*)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_InfoName" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT1</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_InfoPhone1" placeholder="Nhập text" role="phone" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT2</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_InfoPhone2" placeholder="Nhập text" role="phone" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_InfoEmail" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_InfoAddress1" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ liên hệ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_InfoAddress2" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveInfo" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mUpdateAssetGuest">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="mCustomerTitle">Cập nhật chủ nhà cho sản phẩm đã có</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Họ và tên(*)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_EditCustomerName" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT1</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_EditPhone1" placeholder="Nhập text" role="phone" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">SĐT2</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_EditPhone2" placeholder="Nhập text" role="phone" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_EditEmail1" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ thường trú</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_EditAddress1" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Địa chỉ liên hệ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_EditAddress2" placeholder="Nhập text" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveCustomer" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mUpdateAssetInfo">
    </div>

    <asp:HiddenField ID="HID_KeyInfo" runat="server" />
    <!--Key gốc tin-->
    <asp:HiddenField ID="HID_Key" runat="server" Value="0" />
    <!--Key tin đã gửi-->
    <asp:HiddenField ID="HID_Type" runat="server" Value="1" />
    <asp:HiddenField ID="HID_AssetType" runat="server" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $(".select2").select2({ width: "100%" });
            $("#btnProcessInfo").click(function () {
                var status = $("[id$=DDL_Status]").val();
                var table = $("[id$=DDL_Table]").val();
                var project = $("[id$=DDL_Project]").val();
                var category = $("[id$=DDL_Category]").val();

                //phai chọn xử lý
                if (status == null) {
                    Page.showNotiMessageError("...", "Bạn phải chọn các tùy chọn thông tin xử lý !");
                    return false;
                }
                else {
                    // nếu là ky gửi phải chọn bảng, dự án, loại, ...
                    if (status == 1) {
                        if (table == null || project == null) {
                            Page.showNotiMessageError("...", "Bạn phải chọn các tùy chọn thông tin xử lý như trình tự, 1-phản hồi, 2-sản phẩm, 3-dự án, 4-loại !");
                            return false;
                        }
                    }
                    else {
                        table = 0;
                        project = 0;
                    }

                    $.ajax({
                        type: 'POST',
                        url: '/INFO/InfoView.aspx/SaveProduct',
                        data: JSON.stringify({
                            "Key": $("[id$=HID_Key]").val(),
                            "Type": $("[id$=HID_Type]").val(),
                            "Status": status,
                            "InTable": table,
                            "Project": project,
                            "Category": category,
                        }),
                        contentType: "application/json; charset=utf-8",
                        beforeSend: function () {
                            $('.se-pre-con').fadeIn('slow');
                        },
                        success: function (r) {
                            if (status != 1) {
                                if (r.d.Result == "OK-1") {
                                    Page.showNotiMessageInfo("Đã cập nhật thông tin xử lý");
                                    location.reload();
                                }
                                if (r.d.Result == "OK-2") {
                                    Page.showNotiMessageInfo("Đã cập nhật thông tin khách");
                                    location.reload();
                                }
                                if (r.d.Result == "Error-1") {
                                    Page.showPopupMessage("Lỗi xử lý thông tin vui lòng liên hệ Admin.", r.d.Message);
                                }
                                if (r.d.Result == "Error-2") {
                                    Page.showPopupMessage("Lỗi xử lý thông tin vui lòng liên hệ Admin.", r.d.Message);
                                }
                            }
                            else {
                                if (r.d.Result == "Error") {
                                    Page.showPopupMessage("Lỗi xử lý thông tin vui lòng liên hệ Admin.", r.d.Message);
                                }
                                else {
                                    if (r.d.Result > 0) {
                                        window.location = "/Info/InfoProductEdit.aspx?ID=" + r.d.Result + "&Type=" + r.d.Result2;
                                    }
                                }
                            }
                        },
                        error: function () {
                        },
                        complete: function () {
                            $('.se-pre-con').fadeOut('slow');
                        }
                    });
                }
            });
            $("#btnSaveInfo").click(function () {
                saveInfo();
            });
            $("#btnSaveCustomer").click(function () {
                saveOwner();
            });
            $("#tblData tbody tr").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                var trtype = $(this).closest('tr').attr('type');
                if (trid != undefined && trtype != undefined) {
                    window.location = "/Info/InfoProductView.aspx?ID=" + trid + "&Type=" + trtype;
                }
            });

            $('#Product').hide();
            $('#Category').hide();
            $('#Project').hide();
            $('[id$=DDL_Status]').on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected == 1) {
                    $('#Product').show();
                    $('#Category').show();
                    $('#Project').show();
                }
                else {
                    $('#Product').hide();
                    $('#Category').hide();
                    $('#Project').hide();
                }
            });
        });

        function saveOwner() {
            $.ajax({
                type: 'POST',
                url: '/SAL/ProductEdit.aspx/SaveGuest',
                data: JSON.stringify({
                    'Type': $("[id$=HID_AssetType]").val(),
                    'AutoKey': 0,
                    'AssetKey': $("[id$=HID_AssetKey]").val(),
                    'CustomerName': $("[id$=txt_EditCustomerName]").val(),
                    'Phone': $("[id$=txt_EditPhone1]").val(),
                    'Email': $("[id$=txt_EditEmail1]").val(),
                    'Address': $("[id$=txt_EditAddress1]").val() + "/ " + $("[id$=txt_EditAddress2]").val(),
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "")
                        Page.showPopupMessage("Thông báo", "Lỗi vui lòng liên hệ Admin");
                    else
                        Page.showNotiMessageInfo("...", "Đã cập nhật thành công");
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        };
        function saveInfo() {
            $.ajax({
                type: 'POST',
                url: '/INFO/InfoView.aspx/SaveInfo',
                data: JSON.stringify({
                    'AutoKey': $("[id$=HID_KeyInfo]").val(),
                    'CustomerName': $("[id$=txt_InfoName]").val(),
                    'Phone': $("[id$=txt_InfoPhone1]").val(),
                    'Phone2': $("[id$=txt_InfoPhone2]").val(),
                    'Email': $("[id$=txt_InfoEmail]").val(),
                    'Address': $("[id$=txt_InfoAddress1]").val(),
                    'Address2': $("[id$=txt_InfoAddress2]").val(),
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "")
                        Page.showPopupMessage("Lỗi vui lòng liên hệ Admin", msg.d.Message);
                    else {
                        Page.showNotiMessageInfo("...", "Đã cập nhật thành công");
                        location.reload();
                    }
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        };
    </script>
</asp:Content>
