﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;

namespace WebApp
{
    public class NotificationsHub : Hub
    {
        string _name = "";
        public void Join(string groupName)
        {          
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
            context.Groups.Add(Context.ConnectionId, groupName);
            _name = groupName;
        }

        public void Send(string name, string time, string message)
        {
            Clients.Group(_name).addNewMessageToPage(name, time, message);
            //Clients.All.addNewMessageToPage(name, time, message);
            //string[] Exceptional = new string[0];
            //Clients.Group(_Group, Exceptional).addNewMessageToPage(name, time, message);
        }

        //public void Send(string name, string time, string message)
        //{
        //    IHubContext context = GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
        //    context.Clients.All.addNewMessageToPage(name, time, message);
        //}
    }
}

//objHub.server.saveMessagetoDB(UserName, msg); client call
//xu ly ham luu chat ở server goi ham ở client