﻿using Lib.Config;
using Lib.HRM;
using Lib.KPI;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class KPI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StringBuilder zSb = new StringBuilder();
                DataTable zTable = Categories_Data.ListDefine_Target_KPI();
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<div class='checkbox'><label><input type='checkbox' class='ace' value='" + r["AutoKey"] + "' name='chkPurpose' checked='checked' /><span class='lbl'>" + r["ProductGroup"] + "% " + r["Product"] + "</span></label></div>");
                    lblMuctieu.Text += r["AutoKey"] + ",";
                }
                Literal1.Text = zSb.ToString();

                InitData();

                Tools.DropDown_DDL_Month(DDLMonth);
                Tools.DropDown_DDL_Quarter(DDLQuarter);
                Tools.DropDown_DDL_Year(DDLYear);

                DDLMonth.SelectedValue = DateTime.Now.Month.ToString();
                DDLYear.SelectedValue = DateTime.Now.Year.ToString();
                DDLQuarter.SelectedValue = "1";
            }
        }

        protected void InitData()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
            {
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            }
            else
            {
                ViewDepartment = Department;
            }

            if (UnitLevel <= 3)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName ", false);

                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;

                Literal_Table.Text = SearchData("choncongty", "chonthang", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.Month.ToString(), "", DateTime.Now.Year.ToString(), lblMuctieu.Text, string.Empty, ViewDepartment).Result;
            }
            if (UnitLevel > 3 &&
                UnitLevel <= 6)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName ", false);

                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;

                Literal_Table.Text = SearchData("chonphong", "chonthang", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.Month.ToString(), "", DateTime.Now.Year.ToString(), lblMuctieu.Text, string.Empty, ViewDepartment).Result;
            }
            if (UnitLevel >= 7)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);

                DDL_Employee.SelectedValue = Employee.ToString();

                DDL_Department.Visible = false;
                DDL_Employee.Visible = false;

                Literal_Table.Text = SearchData("chonnhanvien", "chonthang", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.Month.ToString(), "", DateTime.Now.Year.ToString(), lblMuctieu.Text, Employee.ToString(), ViewDepartment).Result;
            }

            DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, FromDate.AddMonths(1).AddDays(-1).Day, 23, 59, 59);

            HIDFromDate.Value = FromDate.ToString("dd/MM/yyyy");
            HIDToDate.Value = ToDate.ToString("dd/MM/yyyy");
            HIDEmployee.Value = Employee.ToString();
            HIDDepartment.Value = Department.ToString();
        }

        [WebMethod]
        public static ItemReturn SearchData(string DoiTuong, string ThoiGian, string Ngay, string Thang, string Quy, string Nam, string MucTieu, string Employee, string Department)
        {
            string strResult = "";
            ItemReturn zResult = new ItemReturn();
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime QueryDate = new DateTime();

            if (DoiTuong == "choncongty")
            {
                //if (ThoiGian == "chonngay")
                //{
                //    if (Ngay != string.Empty)
                //        QueryDate = Tools.ConvertToDateTime(Ngay);
                //    else
                //        QueryDate = DateTime.Now;
                //    FromDate = new DateTime(QueryDate.Year, QueryDate.Month, QueryDate.Day, 0, 0, 0);
                //    ToDate = new DateTime(QueryDate.Year, QueryDate.Month, QueryDate.Day, 23, 59, 59);
                //    strResult = Process_Company_Day(MucTieu, FromDate, ToDate);
                //}
                if (ThoiGian == "chonthang")
                {
                    int nam = Nam.ToInt();
                    int thang = Thang.ToInt();
                    QueryDate = new DateTime(nam, thang, 1, 0, 0, 0);
                    FromDate = new DateTime(QueryDate.Year, QueryDate.Month, 1, 0, 0, 0);
                    ToDate = FromDate.AddMonths(1).AddDays(-1);
                    ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                    strResult = Process_Company_Time(MucTieu, FromDate, ToDate);
                }
                if (ThoiGian == "chonnam")
                {
                    int nam = Nam.ToInt();
                    FromDate = new DateTime(nam, 1, 1, 0, 0, 0);
                    ToDate = new DateTime(nam, 12, 31, 23, 59, 59);
                    strResult = Process_Company_Time(MucTieu, FromDate, ToDate);
                }
                if (ThoiGian == "chonquy")
                {
                    switch (Quy)
                    {
                        case "1":
                            FromDate = new DateTime(Nam.ToInt(), 1, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 3, 31, 23, 59, 59);
                            strResult = Process_Company_Time(MucTieu, FromDate, ToDate);
                            break;
                        case "2":
                            FromDate = new DateTime(Nam.ToInt(), 4, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 6, 30, 23, 59, 59);
                            strResult = Process_Company_Time(MucTieu, FromDate, ToDate);
                            break;
                        case "3":
                            FromDate = new DateTime(Nam.ToInt(), 7, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 9, 30, 23, 59, 59);
                            strResult = Process_Company_Time(MucTieu, FromDate, ToDate);
                            break;
                        case "4":
                            FromDate = new DateTime(Nam.ToInt(), 10, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 12, 31, 23, 59, 59);
                            strResult = Process_Company_Time(MucTieu, FromDate, ToDate);
                            break;
                    }
                }

                zResult.Result2 = "Năng suất làm việc từ ngày " + FromDate.ToString("dd/MM/yyyy") + " đến ngày " + ToDate.ToString("dd/MM/yyyy") + " của công ty !";
                zResult.Result3 = FromDate.ToString("dd/MM/yyyy");
                zResult.Result4 = ToDate.ToString("dd/MM/yyyy");
            }
            if (DoiTuong == "chonphong")
            {
                //if (ThoiGian == "chonngay")
                //{
                //    if (Ngay != string.Empty)
                //        QueryDate = Tools.ConvertToDateTime(Ngay);
                //    else
                //        QueryDate = DateTime.Now;
                //    FromDate = new DateTime(QueryDate.Year, QueryDate.Month, QueryDate.Day, 0, 0, 0);
                //    ToDate = new DateTime(QueryDate.Year, QueryDate.Month, QueryDate.Day, 23, 59, 59);
                //    strResult = Process_Department_Day(MucTieu, Department, FromDate, ToDate);
                //}
                if (ThoiGian == "chonthang")
                {
                    int nam = Nam.ToInt();
                    int thang = Thang.ToInt();
                    QueryDate = new DateTime(nam, thang, 1, 0, 0, 0);
                    FromDate = new DateTime(QueryDate.Year, QueryDate.Month, 1, 0, 0, 0);
                    ToDate = FromDate.AddMonths(1).AddDays(-1);
                    ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                    strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                }
                if (ThoiGian == "chonnam")
                {
                    int nam = Nam.ToInt();
                    FromDate = new DateTime(nam, 1, 1, 0, 0, 0);
                    ToDate = new DateTime(nam, 12, 31, 23, 59, 59);
                    strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                }
                if (ThoiGian == "chonquy")
                {
                    switch (Quy)
                    {
                        case "1":
                            FromDate = new DateTime(Nam.ToInt(), 1, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 3, 31, 23, 59, 59);
                            strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                            break;
                        case "2":
                            FromDate = new DateTime(Nam.ToInt(), 4, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 6, 30, 23, 59, 59);
                            strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                            break;
                        case "3":
                            FromDate = new DateTime(Nam.ToInt(), 7, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 9, 30, 23, 59, 59);
                            strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                            break;
                        case "4":
                            FromDate = new DateTime(Nam.ToInt(), 10, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 12, 31, 23, 59, 59);
                            strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                            break;
                    }

                }

                zResult.Result2 = "Năng suất làm việc từ ngày " + FromDate.ToString("dd/MM/yyyy") + " đến ngày " + ToDate.ToString("dd/MM/yyyy") + " của phòng !";
                zResult.Result3 = FromDate.ToString("dd/MM/yyyy");
                zResult.Result4 = ToDate.ToString("dd/MM/yyyy");
            }
            if (DoiTuong == "chonnhanvien")
            {
                //if (ThoiGian == "chonngay")
                //{
                //    if (Ngay != string.Empty)
                //        QueryDate = Tools.ConvertToDateTime(Ngay);
                //    else
                //        QueryDate = DateTime.Now;
                //    FromDate = new DateTime(QueryDate.Year, QueryDate.Month, QueryDate.Day, 0, 0, 0);
                //    ToDate = new DateTime(QueryDate.Year, QueryDate.Month, QueryDate.Day, 23, 59, 59);
                //    strResult = Process_Employee_Day(MucTieu, Department, Employee, FromDate, ToDate);
                //}
                if (ThoiGian == "chonthang")
                {
                    int nam = Nam.ToInt();
                    int thang = Thang.ToInt();
                    QueryDate = new DateTime(nam, thang, 1, 0, 0, 0);
                    FromDate = new DateTime(QueryDate.Year, QueryDate.Month, 1, 0, 0, 0);
                    ToDate = FromDate.AddMonths(1).AddDays(-1);
                    ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
                    strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                }
                if (ThoiGian == "chonnam")
                {
                    int nam = Nam.ToInt();
                    FromDate = new DateTime(nam, 1, 1, 0, 0, 0);
                    ToDate = new DateTime(nam, 12, 31, 23, 59, 59);
                    strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                }
                if (ThoiGian == "chonquy")
                {
                    switch (Quy)
                    {
                        case "1":
                            FromDate = new DateTime(Nam.ToInt(), 1, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 3, 31, 23, 59, 59);
                            strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                            break;
                        case "2":
                            FromDate = new DateTime(Nam.ToInt(), 4, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 6, 30, 23, 59, 59);
                            strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                            break;
                        case "3":
                            FromDate = new DateTime(Nam.ToInt(), 7, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 9, 30, 23, 59, 59);
                            strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                            break;
                        case "4":
                            FromDate = new DateTime(Nam.ToInt(), 10, 1, 0, 0, 0);
                            ToDate = new DateTime(Nam.ToInt(), 12, 31, 23, 59, 59);
                            strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                            break;
                    }

                }

                zResult.Result2 = "Năng suất làm việc từ ngày " + FromDate.ToString("dd/MM/yyyy") + " đến ngày " + ToDate.ToString("dd/MM/yyyy") + " của nhân viên !";
                zResult.Result3 = FromDate.ToString("dd/MM/yyyy");
                zResult.Result4 = ToDate.ToString("dd/MM/yyyy");
                zResult.Result5 = ListItem(MucTieu, Employee, FromDate.ToString("dd/MM/yyyy"), ToDate.ToString("dd/MM/yyyy"));
            }

            zResult.Result = strResult;
            return zResult;
        }

        ////tong cong ty
        //protected static string Process_Company_Day(string MucTieu, DateTime FromDate, DateTime ToDate)
        //{
        //    string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
        //    string ViewDepartment = "";
        //    if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
        //        ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
        //    else
        //        ViewDepartment = Department;

        //    SqlContext Sql = new SqlContext();
        //    StringBuilder zSb = new StringBuilder();
        //    string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));

        //    DataTable zData = Employees_Data.Staff_Working(ViewDepartment);
        //    DataTable zHeader = Helper_Data.List(muctieu);

        //    zSb.AppendLine("<table class='table table-bordered' id='tbl'>");
        //    #region [Tiêu đề]        
        //    zSb.AppendLine("        <tr>");
        //    zSb.AppendLine("            <th class=phong>Phòng</th>");
        //    zSb.AppendLine("            <th class=nhanvien>Nhân viên</th>");
        //    foreach (DataRow r in zHeader.Rows)
        //    {
        //        zSb.AppendLine("        <th>" + r["Name"].ToString() + "</th>");
        //    }
        //    zSb.AppendLine("        </tr>");

        //    #endregion
        //    if (zData.Rows.Count > 0)
        //    {
        //        int Flag = zData.Rows[0]["DepartmentKey"].ToInt();
        //        double[] Array_Doing_Depart = new double[50];
        //        double[] Array_Doing_Company = new double[50];

        //        foreach (DataRow r in zData.Rows)
        //        {
        //            int rDepartment = r["DepartmentKey"].ToInt();
        //            int rEmployee = r["EmployeeKey"].ToInt();
        //            string DepartmentName = r["DepartmentName"].ToString();
        //            string EmployeeName = r["EmployeeName"].ToString();

        //            if (Flag != rDepartment)
        //            {
        //                zSb.AppendLine(Sum_Day("Tổng phòng", Array_Doing_Depart, zHeader.Rows.Count, "tongphong"));
        //                Flag = rDepartment;
        //                Array_Doing_Depart = new double[50];
        //            }

        //            zSb.AppendLine("        <tr key=" + rEmployee + ">");
        //            zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
        //            zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");

        //            int n = 0;
        //            foreach (DataRow rValue in zHeader.Rows)
        //            {
        //                Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                string Item = Sql.GetObject("SELECT dbo.KPI_DoingV3(" + rEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate) ").ToString();
        //                zSb.AppendLine("        <td class=number>" + Item + "</td>");

        //                Array_Doing_Depart[n] = Array_Doing_Depart[n] + Item.ToDouble();
        //                Array_Doing_Company[n] = Array_Doing_Company[n] + Item.ToDouble();
        //                n++;
        //            }

        //            zSb.AppendLine("        </tr>");

        //        }

        //        zSb.AppendLine(Sum_Day("Tổng phòng", Array_Doing_Depart, zHeader.Rows.Count, "tongphong"));
        //        zSb.AppendLine(Sum_Day("Tổng công ty", Array_Doing_Company, zHeader.Rows.Count, "tongcongty"));
        //    }
        //    zSb.AppendLine("</table>");
        //    Sql.CloseConnect();
        //    return zSb.ToString();
        //}

        ////phong
        //protected static string Process_Department_Day(string MucTieu, string Department, DateTime FromDate, DateTime ToDate)
        //{
        //    SqlContext Sql = new SqlContext();
        //    StringBuilder zSb = new StringBuilder();
        //    string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));

        //    DataTable zData = Employees_Data.ListWorking(Department.ToInt());
        //    DataTable zHeader = Helper_Data.List(muctieu);

        //    zSb.AppendLine("<table class='table table-bordered' id='tbl'>");
        //    #region [Tiêu đề]        
        //    zSb.AppendLine("        <tr>");
        //    zSb.AppendLine("            <th class=phong>Phòng</th>");
        //    zSb.AppendLine("            <th class=nhanvien>Nhân viên</th>");
        //    foreach (DataRow r in zHeader.Rows)
        //    {
        //        zSb.AppendLine("        <th>" + r["Name"].ToString() + "</th>");
        //    }
        //    zSb.AppendLine("        </tr>");

        //    #endregion
        //    if (zData.Rows.Count > 0)
        //    {
        //        double[] Array_Doing_Depart = new double[50];
        //        foreach (DataRow r in zData.Rows)
        //        {
        //            int Employee = r["EmployeeKey"].ToInt();
        //            string DepartmentName = r["DepartmentName"].ToString();
        //            string EmployeeName = r["EmployeeName"].ToString();

        //            zSb.AppendLine("        <tr key=" + Employee + ">");
        //            zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
        //            zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");

        //            int n = 0;
        //            foreach (DataRow rValue in zHeader.Rows)
        //            {
        //                Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                string Item = Sql.GetObject("SELECT dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate) ").ToString();
        //                zSb.AppendLine("        <td class=number>" + Item + "</td>");

        //                Array_Doing_Depart[n] = Array_Doing_Depart[n] + Item.ToDouble();
        //                n++;
        //            }
        //            zSb.AppendLine("        </tr>");
        //        }

        //        zSb.AppendLine(Sum_Day("Tổng phòng", Array_Doing_Depart, zHeader.Rows.Count, "tongphong"));
        //    }
        //    zSb.AppendLine("</table>");
        //    Sql.CloseConnect();
        //    return zSb.ToString();
        //}

        ////nhan vien
        //protected static string Process_Employee_Day(string MucTieu, string Department, string Employee, DateTime FromDate, DateTime ToDate)
        //{
        //    SqlContext Sql = new SqlContext();
        //    StringBuilder zSb = new StringBuilder();
        //    string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));

        //    Employees_Info zEmployee = new Employees_Info(Employee.ToInt());
        //    string Name = zEmployee.LastName + " " + zEmployee.FirstName;
        //    string DepartmentName = zEmployee.DepartmentName;

        //    DataTable zHeader = Helper_Data.List(muctieu);
        //    zSb.AppendLine("<table class='table table-bordered' id='tbl'>");
        //    #region [Tiêu đề]
        //    zSb.AppendLine("        <tr>");
        //    zSb.AppendLine("            <th class=phong>Phòng</th>");
        //    zSb.AppendLine("            <th class=nhanvien>Nhân viên</th>");
        //    foreach (DataRow r in zHeader.Rows)
        //    {
        //        zSb.AppendLine("        <th>" + r["Name"].ToString() + "</th>");
        //    }
        //    zSb.AppendLine("        </tr>");

        //    #endregion
        //    zSb.AppendLine("        <tr key=" + Employee + ">");
        //    zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
        //    zSb.AppendLine("            <td class=nhanvien>" + Name + "</td>");
        //    foreach (DataRow rValue in zHeader.Rows)
        //    {

        //        Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //        Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //        string Item = Sql.GetObject("SELECT dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate) ").ToString();
        //        zSb.AppendLine("        <td class=number>" + Item + "</td>");
        //    }
        //    zSb.AppendLine("        </tr>");
        //    zSb.AppendLine("</table>");
        //    Sql.CloseConnect();
        //    return zSb.ToString();
        //}


        //tinh tong cuối cột mỗi dòng
        //protected static string Sum_Day(string Name, double[] ItemDoing, int Length, string CssClass)
        //{
        //    StringBuilder zSb = new StringBuilder();
        //    zSb.AppendLine("        <tr>");
        //    zSb.AppendLine("            <td colspan='2' class=" + CssClass + ">" + Name + "</td>");
        //    for (int i = 0; i < Length; i++)
        //    {
        //        zSb.AppendLine("        <td class='" + CssClass + " number'>" + ItemDoing[i] + "</td>");
        //    }
        //    zSb.AppendLine("        </tr>");
        //    return zSb.ToString();
        //}

        //get datalist
        [WebMethod]
        public static string ListItem(string MucTieu, string Employee, string FromDate, string ToDate)
        {
            try
            {
                DateTime zFromDate = Tools.ConvertToDate(FromDate);
                DateTime zToDate = Tools.ConvertToDate(ToDate);

                StringBuilder zSb = new StringBuilder();
                string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));
                int EmployeeKey = Employee.ToInt();
                DataTable zHeader = Helper_Data.List(muctieu);
                foreach (DataRow r in zHeader.Rows)
                {
                    string temp = string.Empty;

                    #region [SubTable]
                    StringBuilder zSub = new StringBuilder();
                    DataTable ztbSub = new DataTable();
                    int Category = r["AutoKey"].ToInt();
                    switch (Category)
                    {
                        case 290:
                        case 291:
                        case 306:
                            //khách hàng
                            ztbSub = Helper.GetKPI_Customer(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Thực hiện</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Khách hàng</td>");
                                zSub.AppendLine("                   <td>Điện thoại</td>");
                                zSub.AppendLine("                   <td class='noidung'>Nội dung</td>");
                                zSub.AppendLine("                   <td>Tiếp xúc</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("       <td>" + i + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["CustomerName"].ToString() + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["Phone1"].ToString().Replace(" ", "") + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["Description"].ToString() + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["Status"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;

                        case 295:
                        case 298:
                            //giao dich 
                            ztbSub = Helper.GetKPI_Trade(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class=nhanvien>Thực hiện</td>");
                                zSub.AppendLine("                   <td>Mã</td>");
                                zSub.AppendLine("                   <td>Dự án</td>");
                                zSub.AppendLine("                   <td>Doanh thu</td>");
                                //zSub.AppendLine("                   <td>Tiếp xúc</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("    <td>" + i + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["AssetID"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["ProjectName"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["Income"].ToDoubleString() + "</td>");
                                    //zSub.AppendLine("    <td>" + rSub["Status"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;

                        case 307:
                            //Cap nhat san pham
                            ztbSub = Helper.GetKPI_Asset(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Thực hiện</td>");
                                zSub.AppendLine("                   <td>Mã</td>");
                                zSub.AppendLine("                   <td>Dự án</td>");
                                zSub.AppendLine("                   <td class='noidung'>Nội dung</td>");
                                //zSub.AppendLine("                   <td>Tiếp xúc</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("    <td>" + i + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["AssetID"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["ProjectName"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["Description"].ToString() + "</td>");
                                    //zSub.AppendLine("    <td>" + rSub["Status"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;

                        case 329:
                            //sale phone
                            ztbSub = Helper.GetKPI_Phone(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Thực hiện</td>");
                                zSub.AppendLine("                   <td>Thông tin</td>");
                                zSub.AppendLine("                   <td>Sản phẩm</td>");
                                zSub.AppendLine("                   <td class='noidung'>Tình trạng</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    string phone1 = rSub["Phone1"].ToString() == string.Empty ? string.Empty : rSub["Phone1"].ToString() + "<br />";
                                    string phone2 = rSub["Phone2"].ToString() == string.Empty ? string.Empty : rSub["Phone2"].ToString() + "<br />";
                                    string mail1 = rSub["Email1"].ToString() == string.Empty ? string.Empty : rSub["Email1"].ToString() + "<br />";
                                    string mail2 = rSub["Email2"].ToString() == string.Empty ? string.Empty : rSub["Email2"].ToString();

                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("    <td>" + i + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>Họ tên: " + rSub["FullName"].ToString() + "<br/>Điện thoại: " + phone1 + phone2 + mail1 + mail2 + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["Product"].ToString() + "<br/>" + rSub["Asset"].ToString() + "<br/>PN:" + rSub["Room"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["StatusName"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;
                    }
                    #endregion

                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("    <td class=no>#</td>");
                    zSb.AppendLine("    <td><span style='line-height: 2;'>" + r["Name"].ToString() + "</span></td>");
                    zSb.AppendLine("</tr>");

                    zSb.AppendLine("<tr class='detail-row'>");
                    zSb.AppendLine("    <td colspan=2>");
                    zSb.AppendLine(zSub.ToString());
                    zSb.AppendLine("    </td>");
                    zSb.AppendLine("</tr>");
                }
                return zSb.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        //html-------------------------
        //xu ly xem phong
        protected static string Process_Department_Time(string MucTieu, string Department, DateTime FromDate, DateTime ToDate)
        {
            SqlContext Sql = new SqlContext();
            StringBuilder zSb = new StringBuilder();
            string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));
            DataTable zData = Employees_Data.ListWorking(Department.ToInt());
            DataTable zHeader = Helper_Data.List(muctieu);

            zSb.AppendLine("<table class='table table-bordered' id='tbl'>");

            #region [Tiêu đề]           
            #region [Th row 1]
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th rowspan=2 class=phong>Phòng</td>");
            zSb.AppendLine("            <th rowspan=2 class=nhanvien>Nhân viên</td>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("        <th colspan=3>" + r["ProductGroup"].ToString() + "% " + r["Name"].ToString() + "</th>");
            }
            zSb.AppendLine("            <th rowspan=2>Kết quả</td>");
            zSb.AppendLine("        </tr>");
            #endregion
            #region [Th row 2]
            zSb.AppendLine("        <tr>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Chỉ tiêu");
                zSb.AppendLine("            </th>");
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Thực hiện");
                zSb.AppendLine("            </th>");
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Tỷ lệ");
                zSb.AppendLine("            </th>");
            }
            zSb.AppendLine("        </tr>");

            #endregion
            #endregion

            #region [Data]
            if (zData.Rows.Count > 0)
            {
                double[] Array_Require_Depart = new double[50];
                double[] Array_Doing_Depart = new double[50];
                double[] Array_Rate_Depart = new double[50];

                double[] Array_Require_Employee = new double[50];
                double[] Array_Doing_Employee = new double[50];
                double[] Array_Rate_Doing_Employee = new double[50];
                double[] Array_Rate_Require_Employee = new double[50];

                foreach (DataRow r in zData.Rows)
                {
                    int Employee = r["EmployeeKey"].ToInt();
                    string DepartmentName = r["DepartmentName"].ToString();
                    string EmployeeName = r["EmployeeName"].ToString();

                    zSb.AppendLine("        <tr key=" + Employee + ">");
                    zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
                    zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");

                    int n = 0;
                    foreach (DataRow rValue in zHeader.Rows)
                    {
                        #region Go SQL GetData
                        Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                        Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                        DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                              + " dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
                                                                              + " dbo.KPI_RequirePercentV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                              + " dbo.KPI_RequireDefaultPercentV3(" + rValue["AutoKey"].ToString() + ")");
                        #endregion

                        #region Calculator
                        double YC = zRow.Rows[0][0].ToDouble();
                        double TH = zRow.Rows[0][1].ToDouble();
                        double TS = zRow.Rows[0][2].ToDouble();
                        double TL = 0;
                        if (YC != 0)
                        {
                            TL = (TH / YC) * TS;
                        }

                        Array_Require_Depart[n] = Array_Require_Depart[n] + YC;
                        Array_Doing_Depart[n] = Array_Doing_Depart[n] + TH;
                        Array_Rate_Depart[n] = zRow.Rows[0][3].ToDouble();

                        Array_Require_Employee[n] = YC;
                        Array_Doing_Employee[n] = TH;
                        Array_Rate_Doing_Employee[n] = TL;
                        Array_Rate_Require_Employee[n] = TS;
                        n++;
                        #endregion                                                
                    }

                    #region Show
                    zSb.AppendLine(Show_Data_Employee(Array_Require_Employee, Array_Doing_Employee, Array_Rate_Doing_Employee, Array_Rate_Require_Employee, zHeader.Rows.Count));
                    #endregion
                    zSb.AppendLine("        </tr>");
                }

                zSb.AppendLine(Show_Sum_Row("Tổng phòng", Array_Require_Depart, Array_Doing_Depart, Array_Rate_Depart, zHeader.Rows.Count, "tongphong"));
            }
            #endregion

            zSb.AppendLine("</table>");
            Sql.CloseConnect();
            return zSb.ToString();
        }
        //xu lý xem nhan vien
        protected static string Process_Employee_Time(string MucTieu, string Department, string Employee, DateTime FromDate, DateTime ToDate)
        {
            SqlContext Sql = new SqlContext();
            StringBuilder zSb = new StringBuilder();
            string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));

            Employees_Info zEmployee = new Employees_Info(Employee.ToInt());
            string EmployeeName = zEmployee.LastName + " " + zEmployee.FirstName;
            string DepartmentName = zEmployee.DepartmentName;

            double[] Array_Require_Employee = new double[50];
            double[] Array_Doing_Employee = new double[50];
            double[] Array_Rate_Doing_Employee = new double[50];
            double[] Array_Rate_Require_Employee = new double[50];

            DataTable zHeader = Helper_Data.List(muctieu);
            zSb.AppendLine("<table class='table table-bordered' id='tbl'>");
            #region [Tiêu đề]
            #region [Th row 1]
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th rowspan=2 class=phong>Phòng</td>");
            zSb.AppendLine("            <th rowspan=2 class=nhanvien>Nhân viên</td>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("        <th colspan=3>" + r["ProductGroup"].ToString() + "% " + r["Name"].ToString() + "</th>");
            }
            zSb.AppendLine("            <th rowspan=2>Kết quả</td>");
            zSb.AppendLine("        </tr>");
            #endregion
            #region [Th row 2]
            zSb.AppendLine("        <tr>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Chỉ tiêu");
                zSb.AppendLine("            </th>");
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Thực hiện");
                zSb.AppendLine("            </th>");
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Tỷ lệ");
                zSb.AppendLine("            </th>");
            }
            zSb.AppendLine("        </tr>");
            #endregion
            #endregion

            zSb.AppendLine("        <tr key=" + Employee + ">");
            zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
            zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");

            int n = 0;
            foreach (DataRow rValue in zHeader.Rows)
            {
                #region Go SQL GetData
                Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                      + " dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
                                                                      + " dbo.KPI_RequirePercentV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                      + " dbo.KPI_RequireDefaultPercentV3(" + rValue["AutoKey"].ToString() + ")");
                #endregion

                #region Calculator
                double YC = zRow.Rows[0][0].ToDouble();
                double TH = zRow.Rows[0][1].ToDouble();
                double TS = zRow.Rows[0][2].ToDouble();
                double TL = 0;
                if (YC != 0)
                {
                    TL = (TH / YC) * TS;
                }

                Array_Require_Employee[n] = YC;
                Array_Doing_Employee[n] = TH;
                Array_Rate_Doing_Employee[n] = TL;
                Array_Rate_Require_Employee[n] = TS;
                n++;
                #endregion
            }
            #region Show
            zSb.AppendLine(Show_Data_Employee(Array_Require_Employee, Array_Doing_Employee, Array_Rate_Doing_Employee, Array_Rate_Require_Employee, zHeader.Rows.Count));
            #endregion
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("</table>");
            return zSb.ToString();
        }
        //xu ly xem tong cong ty
        protected static string Process_Company_Time(string MucTieu, DateTime FromDate, DateTime ToDate)
        {
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
            {
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            }
            else
            {
                ViewDepartment = Department;
            }

            SqlContext Sql = new SqlContext();
            StringBuilder zSb = new StringBuilder();
            string muctieu = MucTieu.Remove(MucTieu.LastIndexOf(','));
            DataTable zData = Employees_Data.Staff_Working(ViewDepartment);
            DataTable zHeader = Helper_Data.List(muctieu);

            zSb.AppendLine("<table class='table table-bordered' id='tbl'>");

            #region [Tiêu đề]           
            #region [Th row 1]
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th rowspan=2 class=phong>Phòng</td>");
            zSb.AppendLine("            <th rowspan=2 class=nhanvien>Nhân viên</td>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("        <th colspan=3>" + r["ProductGroup"].ToString() + "% " + r["Name"].ToString() + "</th>");
            }
            zSb.AppendLine("            <th rowspan=2>Kết quả</td>");
            zSb.AppendLine("        </tr>");
            #endregion
            #region [Th row 2]
            zSb.AppendLine("        <tr>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Chỉ tiêu");
                zSb.AppendLine("            </th>");
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Thực hiện");
                zSb.AppendLine("            </th>");
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Tỷ lệ");
                zSb.AppendLine("            </th>");
            }
            zSb.AppendLine("        </tr>");
            #endregion
            #endregion

            #region [Data]
            if (zData.Rows.Count > 0)
            {
                int Flag = zData.Rows[0]["DepartmentKey"].ToInt();
                double[] Array_Require_Depart = new double[50];
                double[] Array_Doing_Depart = new double[50];
                double[] Array_Rate_Depart = new double[50];

                double[] Array_Require_Company = new double[50];
                double[] Array_Doing_Company = new double[50];
                double[] Array_Rate_Company = new double[50];

                double[] Array_Require_Employee = new double[50];
                double[] Array_Doing_Employee = new double[50];
                double[] Array_Rate_Doing_Employee = new double[50];
                double[] Array_Rate_Require_Employee = new double[50];

                foreach (DataRow r in zData.Rows)
                {
                    int zDepartment = r["DepartmentKey"].ToInt();
                    int zEmployee = r["EmployeeKey"].ToInt();
                    string DepartmentName = r["DepartmentName"].ToString();
                    string EmployeeName = r["EmployeeName"].ToString();

                    if (Flag != zDepartment)
                    {
                        zSb.AppendLine(Show_Sum_Row("Tổng phòng", Array_Require_Depart, Array_Doing_Depart, Array_Rate_Depart, zHeader.Rows.Count, "tongphong"));
                        Flag = zDepartment;

                        Array_Require_Depart = new double[50];
                        Array_Doing_Depart = new double[50];
                    }

                    zSb.AppendLine("        <tr key=" + zEmployee + ">");
                    zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
                    zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");

                    int n = 0;
                    foreach (DataRow rValue in zHeader.Rows)
                    {
                        #region Go SQL GetData
                        Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                        Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                        DataTable zRow = Sql.GetData("SELECT dbo.KPI_RequireV3(" + zEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                              + " dbo.KPI_DoingV3(" + zEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate), "
                                                                              + " dbo.KPI_RequirePercentV3(" + zEmployee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate),"
                                                                              + " dbo.KPI_RequireDefaultPercentV3(" + rValue["AutoKey"].ToString() + ")");
                        #endregion

                        #region Calculator
                        double YC = zRow.Rows[0][0].ToDouble();
                        double TH = zRow.Rows[0][1].ToDouble();
                        double TS = zRow.Rows[0][2].ToDouble();
                        double TL = 0;
                        if (YC != 0)
                        {
                            TL = (TH / YC) * TS;
                        }

                        Array_Require_Company[n] = Array_Require_Company[n] + YC;
                        Array_Doing_Company[n] = Array_Doing_Company[n] + TH;
                        Array_Rate_Company[n] = zRow.Rows[0][3].ToDouble();

                        Array_Require_Depart[n] = Array_Require_Depart[n] + YC;
                        Array_Doing_Depart[n] = Array_Doing_Depart[n] + TH;
                        Array_Rate_Depart[n] = zRow.Rows[0][3].ToDouble();

                        Array_Require_Employee[n] = YC;
                        Array_Doing_Employee[n] = TH;
                        Array_Rate_Doing_Employee[n] = TL;
                        Array_Rate_Require_Employee[n] = TS;
                        n++;
                        #endregion
                    }

                    #region [Show]
                    zSb.AppendLine(Show_Data_Employee(Array_Require_Employee, Array_Doing_Employee, Array_Rate_Doing_Employee, Array_Rate_Require_Employee, zHeader.Rows.Count));
                    #endregion

                    zSb.AppendLine("        </tr>");
                }

                zSb.AppendLine(Show_Sum_Row("Tổng phòng", Array_Require_Depart, Array_Doing_Depart, Array_Rate_Depart, zHeader.Rows.Count, "tongphong"));
                zSb.AppendLine(Show_Sum_Row("Tổng công ty", Array_Require_Company, Array_Doing_Company, Array_Rate_Company, zHeader.Rows.Count, "tongcongty"));
            }
            #endregion

            zSb.AppendLine("</table>");
            Sql.CloseConnect();
            return zSb.ToString();
        }

        //dòng data nhân viên
        protected static string Show_Data_Employee(double[] Require, double[] Doing, double[] Rate_Doing, double[] Rate_Require, int Length)
        {
            double tong = 0;
            string hot = "&nbsp;<i class='fa fa-thumbs-o-up icon-animated-vertical bigger-130 orange' aria-hidden='true'></i>";
            SqlContext Sql = new SqlContext();
            StringBuilder zSb = new StringBuilder();

            for (int n = 0; n < Length; n++)
            {
                zSb.AppendLine("        <td class=number>" + Require[n].ToString("n2") + "</td>");
                zSb.AppendLine("        <td class=number>" + Doing[n].ToString("n2") + "</td>");

                if (Rate_Doing[n].ToInt() > 0 && Rate_Doing[n].ToInt() >= Rate_Require[n].ToInt())
                {
                    zSb.AppendLine("        <td class=number style='background:#68f268'>" + Rate_Doing[n].ToString("n2") + "%</td>");
                }
                else
                {
                    zSb.AppendLine("        <td class=number>" + Rate_Doing[n].ToString("n2") + "%</td>");
                }

                tong += Rate_Doing[n].ToDouble();
            }

            // cột tổng cuối cùng mỗi dòng
            if (tong < 50)
            {
                zSb.AppendLine("            <td class=number>" + tong.ToString("n2") + "</td>");
            }

            if (tong > 50 && tong < 100)
            {
                zSb.AppendLine("            <td class=number style='background:#68f268'>" + tong.ToString("n2") + "%</td>");
            }

            if (tong > 100)
            {
                zSb.AppendLine("            <td class=number style='background:#68f268'>" + tong.ToString("n2") + "%" + hot + "</td>");
            }

            Sql.CloseConnect();
            return zSb.ToString();
        }
        //dòng tổng
        //tinh tyle tong
        protected static string Show_Sum_Row(string Name, double[] Require, double[] Doing, double[] Rate_Require, int Length, string CssClass)
        {
            double tong = 0;
            string hot = "&nbsp;<i class='fa fa-thumbs-o-up icon-animated-vertical bigger-130 orange' aria-hidden='true'></i>";

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <td colspan='2' class='number " + CssClass + "'>" + Name + "</td>");
            for (int n = 0; n < Length; n++)
            {
                double YC = Require[n].ToDouble();
                double TH = Doing[n].ToDouble();
                double KPI = Rate_Require[n].ToDouble();
                double TL = 0;
                if (YC != 0)
                {
                    TL = (TH / YC) * KPI;
                }

                zSb.AppendLine("        <td class='number " + CssClass + "'>" + Require[n].ToString("n2") + "</td>");
                zSb.AppendLine("        <td class='number " + CssClass + "'>" + Doing[n].ToString("n2") + "</td>");
                if (TL >= Rate_Require[n].ToInt())
                {
                    zSb.AppendLine("        <td class='number " + CssClass + "'>" + TL.ToString("n2") + "%</td>");
                }
                else
                {
                    zSb.AppendLine("        <td class='number " + CssClass + "'>" + TL.ToString("n2") + "%</td>");
                }

                tong += TL.ToDouble();
            }

            // cột tổng cuối cùng mỗi dòng          
            if (tong < 50)
            {
                zSb.AppendLine("            <td class='number " + CssClass + "'>" + tong.ToString("n2") + "</td>");
            }

            if (tong > 50 && tong < 100)
            {
                zSb.AppendLine("            <td class='number " + CssClass + "'>" + tong.ToString("n2") + "%</td>");
            }

            if (tong > 100)
            {
                zSb.AppendLine("            <td class='number" + CssClass + "'>" + tong.ToString("n2") + "%" + hot + "</td>");
            }

            zSb.AppendLine("        </tr>");
            return zSb.ToString();
        }
    }
}