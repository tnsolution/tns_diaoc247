﻿using Lib.CRM;
using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
//135 tap tin khác > nhưng ky gửi là hình ảnh
namespace WebApp.SAL
{
    public partial class ProductEdit : System.Web.UI.Page
    {
        //KPI tham số categoryplan = 1 là từ công việc, 2 là từ cập nhật sản phẩm
        //category cap nhat san pham la 307

        const int _Category = 307;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                {
                    HID_AssetKey.Value = Request["ID"];
                }

                if (Request["Type"] != null)
                {
                    HID_AssetType.Value = Request["Type"];
                }

                Lit_USD.Text = "<script>var usdRate = " + Tools.MoneyRate("USD") + ";</script>";

                Tools.DropDown_DDL(DDL_Furnitur, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 7", false);
                Tools.DropDown_DDL(DDL_Legal, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 6", false);
                Tools.DropDown_DDL(DDL_Status, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking = 2 ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Project, "SELECT ProjectKey, ProjectName FROM PUL_Project ORDER BY ProjectName", false);

                LoadAsset();
                LoadAllow();
                LoadPicture();
                LoadGuest();
                CheckRoles();
            }
        }
        void LoadAsset()
        {
            Product_Info zInfo = new Product_Info(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            txt_AssetID.Text = zInfo.ItemAsset.AssetID.Trim();
            txt_Id1.Text = zInfo.ItemAsset.ID1;
            txt_Id2.Text = zInfo.ItemAsset.ID2;
            txt_Id3.Text = zInfo.ItemAsset.ID3;
            txt_DateContractEnd.Value = zInfo.ItemAsset.DateContractEnd;

            txt_PriceVND.Value = zInfo.ItemAsset.Price_VND;
            txt_PriceRentVND.Value = zInfo.ItemAsset.PriceRent_VND;
            txt_PriceUSD.Value = zInfo.ItemAsset.Price_USD;
            txt_PriceRentUSD.Value = zInfo.ItemAsset.PriceRent_USD;
            txt_PricePurchase_USD.Value = zInfo.ItemAsset.PricePurchase_USD;
            txt_PricePurchase_VND.Value = zInfo.ItemAsset.PricePurchase_VND;

            txt_Room.Value = zInfo.ItemAsset.Room;
            txt_WallArea.Value = zInfo.ItemAsset.AreaWall;
            txt_WaterArea.Value = zInfo.ItemAsset.AreaWater;
            txt_Description.Value = zInfo.ItemAsset.Description;
            txt_WebTitle.Value = zInfo.ItemAsset.WebTitle;
            txt_WebContent.Value = zInfo.ItemAsset.WebContent;

            txt_GroundArea.Value = zInfo.ItemAsset.GroundSize;
            txt_GroundTotal.Value = zInfo.ItemAsset.GroundTotal;
            txt_Sctruc.Value = zInfo.ItemAsset.Structure;

            DDL_Category.SelectedValue = zInfo.ItemAsset.CategoryKey.ToString();
            DDL_Project.SelectedValue = zInfo.ItemAsset.ProjectKey.ToString();
            DDL_WebPublished.SelectedValue = zInfo.ItemAsset.WebPublished;
            DDL_Legal.SelectedValue = zInfo.ItemAsset.LegalKey;
            DDL_DirectionDoor.SelectedValue = zInfo.ItemAsset.Door;
            DDL_DirectionView.SelectedValue = zInfo.ItemAsset.View;
            DDL_Furnitur.SelectedValue = zInfo.ItemAsset.FurnitureKey;
            DDL_Status.SelectedValue = zInfo.ItemAsset.StatusKey;

            HID_Purpose.Value = zInfo.ItemAsset.PurposeKey;
            if (zInfo.ItemAsset.Hot.ToInt() == 5)
            {
                chkHot.Checked = true;
            }

            Lit_TitlePage.Text = "Cập nhật thông tin: " + zInfo.ItemAsset.AssetID;

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người khởi tạo:</td><td>" + zInfo.ItemAsset.CreatedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày khởi tạo:</td><td>" + zInfo.ItemAsset.CreatedDate + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ItemAsset.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ItemAsset.ModifiedDate + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }
        void LoadPicture()
        {
            StringBuilder zSb = new StringBuilder();
            List<ItemDocument> zList = Document_Data.List(135, HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            foreach (ItemDocument item in zList)
            {
                zSb.AppendLine("<li id='" + item.FileKey + "'>");
                zSb.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                zSb.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageThumb + "' />");
                zSb.AppendLine("            <div class='text'>");
                zSb.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                zSb.AppendLine("            </div>");
                zSb.AppendLine("    </a>");
                zSb.AppendLine("    <div class='tools tools-bottom'><a href='#' btn='btnDeleteImg'><i class='ace-icon fa fa-times red'></i>Xóa</a></div>");
                zSb.AppendLine("</li>");
            }

            Lit_ListPicture.Text = zSb.ToString();
        }
        void LoadAllow()
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(HID_AssetKey.Value.ToInt(), HID_AssetType.Value);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblShare'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>Chia sản phẩm cho </div><div class='profile-info-value'>" + zTable.Rows[i]["EmployeeName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            Lit_AllowSearch.Text = zSb.ToString();
        }
        void LoadGuest()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTableOwners = Owner_Data.List(HID_AssetType.Value, HID_AssetKey.Value.ToInt());
            zSb.AppendLine("<table class='table  table-bordered table-hover' id='tblGuest'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Họ tên</th>");
            zSb.AppendLine("        <th>ĐT</th>");
            zSb.AppendLine("        <th>Email</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Ngày cập nhật</th>");
            zSb.AppendLine("        <th>...</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTableOwners.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTableOwners.Rows)
                {
                    zSb.AppendLine("            <tr id=" + r["OwnerKey"].ToString() + ">");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");
                    zSb.AppendLine("               <td>" + r["CustomerName"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Phone1"].ToString() + "<br/>" + r["Phone2"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["Email1"].ToString() + "</td>");
                    zSb.AppendLine("               <td>Liên lạc: " + r["Address1"].ToString() + " <br/>Thường trú: " + r["Address2"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["CreatedDate"].ToDateString() + "</td>");
                    zSb.AppendLine("               <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteGuest' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("            </tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");
            Lit_Owner.Text = zSb.ToString();
        }

        #region [Chủ nhà]
        [WebMethod]
        public static ItemCustomer GetGuest(string Type, int AutoKey)
        {
            Owner_Info zInfo = new Owner_Info(Type, AutoKey);
            ItemCustomer zItem = new ItemCustomer();
            zItem.AssetKey = zInfo.AssetKey.ToString();
            zItem.OwnerKey = zInfo.OwnerKey.ToString();
            zItem.CustomerName = zInfo.CustomerName;
            zItem.Phone1 = zInfo.Phone1;
            zItem.Phone2 = zInfo.Phone2;
            zItem.Email1 = zInfo.Email;
            zItem.Address1 = zInfo.Address1;
            zItem.Address2 = zInfo.Address2;
            zItem.Type = zInfo.Type;
            return zItem;
        }
        [WebMethod]
        public static ItemReturn SaveGuest(string Type, int AutoKey, int AssetKey, string CustomerName, string Phone, string Phone2, string Email, string Address, string Address2)
        {
            ItemReturn zResult = new ItemReturn();
            Owner_Info zInfo = new Owner_Info(Type, AutoKey);
            zInfo.AssetKey = AssetKey;
            zInfo.CustomerName = CustomerName;
            zInfo.Phone1 = Phone.Trim();
            zInfo.Phone2 = Phone2.Trim();
            zInfo.Email = Email.Trim();
            zInfo.Address1 = Address.Trim();
            zInfo.Address2 = Address2.Trim();
            if (zInfo.OwnerKey > 0)
            {
                zInfo.Update(Type);
            }
            else
            {
                zInfo.Create(Type);
            }

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.OwnerKey.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemReturn DeleteGuest(string Type, int AutoKey, int AssetKey)
        {
            ItemReturn zResult = new ItemReturn();
            Owner_Info zInfo = new Owner_Info();
            zInfo.OwnerKey = AutoKey;
            zInfo.AssetKey = AssetKey;
            zInfo.Delete(Type);
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemCustomer[] GetListGuest(string Type, int AssetKey)
        {
            DataTable zTable = Owner_Data.List(Type, AssetKey);
            List<ItemCustomer> ListAsset = zTable.DataTableToList<ItemCustomer>();
            return ListAsset.ToArray();
        }
        #endregion

        #region [Share]
        [WebMethod]
        public static ItemReturn SaveShare(int AssetKey, string Type, int EmployeeKey)
        {
            ItemReturn zResult = new ItemReturn();
            Share_Permition_Info zInfo = new Share_Permition_Info(AssetKey, EmployeeKey, Type);
            if (zInfo.AutoKey > 0)
            {
                zResult.Message = "Trùng thông tin, vui lòng chọn lại";
                return zResult;
            }

            zInfo.ObjectTable = Type;
            zInfo.EmployeeKey = EmployeeKey;
            zInfo.AssetKey = AssetKey;
            zInfo.Create();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemWeb[] GetShare(int AssetKey, string Type)
        {
            DataTable zTable = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
            List<ItemWeb> ListAsset = new List<ItemWeb>();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb iwe = new ItemWeb();
                iwe.Value = zTable.Rows[i]["AutoKey"].ToString();
                iwe.Text = zTable.Rows[i]["EmployeeName"].ToString();

                ListAsset.Add(iwe);
            }
            return ListAsset.ToArray();
        }
        [WebMethod]
        public static ItemReturn DeleteShare(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Share_Permition_Info zInfo = new Share_Permition_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        #endregion

        #region [File]
        void UploadImg()
        {
            int zCategoryKey = 135;
            int zProductKey = HID_AssetKey.Value.ToInt();
            string zSQL = "";
            string zPath = "~/Upload/File/" + HID_AssetType.Value + "/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
            {
                nDir.Create();
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                    string zFileThumb = zFileName + "_thumb" + Path.GetExtension(zInputFile.FileName);
                    string zFileLarge = Path.GetFileName(zInputFile.FileName);

                    string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                    string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                    string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                    string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                    if (File.Exists(zFilePathLarge))
                    {
                        File.Delete(zFilePathLarge);
                    }

                    zInputFile.SaveAs(zFilePathLarge);

                    Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                    System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(150, 150));
                    objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('" + HID_AssetType.Value + "','" + zProductKey + "','" + zCategoryKey + "',N'" + zFileLarge + "',N'" + zFileUrlThumb + "',N'" + zFileUrlLarge + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadPicture();

                    int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                    int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                    SaveKPI(zProductKey, txt_AssetID.Text, EmployeeKey, DepartmentKey, HID_AssetType.Value, DDL_Project.SelectedValue.ToInt(), txt_Description.Value, 1);
                }
            }
        }
        protected void btnUploadImg_Click(object sender, EventArgs e)
        {
            UploadImg();
        }
        #endregion

        [WebMethod]
        public static ItemReturn DeleteFile(int FileKey)
        {
            ItemReturn zResult = new ItemReturn();
            Document_Info zInfo = new Document_Info(FileKey);
            zInfo.FileKey = FileKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;

            if (zInfo.Message == string.Empty)
            {
                if (File.Exists(zInfo.ImagePath))
                {
                    File.Delete(zInfo.ImagePath);
                }
            }
            return zResult;
        }

        [WebMethod]
        public static ItemReturn SaveProduct(int AssetKey, string Type, string Project, string Category,
            string AssetID, string ID1, string ID2, string ID3, string Hot,
            string Price_VND, string PriceRent_VND, string Price_USD,
            string PriceRent_USD, string PricePurchase_VND, string PricePurchase_USD,
            string AreaWall, string AreaWater, string Structure, string AreaBuild, string Area, string Legal, string Furnitur, string Door, string View,
            string Remind, string Room, string Status, string Purpose, string Description,
            string WebPublish, string WebTitle, string WebContent, int UsdMua, int UsdBan, int UsdThue)
        {
            ItemReturn zResult = new ItemReturn();
            Product_Info zInfo = new Product_Info(AssetKey, Type);
            zInfo.ItemAsset.ProjectKey = Project;
            zInfo.ItemAsset.CategoryKey = Category;
            zInfo.ItemAsset.AssetID = AssetID.Trim();
            zInfo.ItemAsset.ID1 = ID1;
            zInfo.ItemAsset.ID2 = ID2;
            zInfo.ItemAsset.ID3 = ID3;
            zInfo.ItemAsset.Hot = Hot;

            double giabanUSD = 0;
            double.TryParse(Price_USD, out giabanUSD);
            double giabanVND = 0;
            double.TryParse(Price_VND, out giabanVND);
            double giathueUSD = 0;
            double.TryParse(PriceRent_USD, out giathueUSD);
            double giathueVND = 0;
            double.TryParse(PriceRent_VND, out giathueVND);
            double giamuaUSD = 0;
            double.TryParse(PricePurchase_USD, out giamuaUSD);
            double giamuaVND = 0;
            double.TryParse(PricePurchase_VND, out giamuaVND);

            if (UsdMua == 1)
            {
                zInfo.ItemAsset.PricePurchase_USD = giamuaUSD.ToString();
                zInfo.ItemAsset.PricePurchase_VND = (giamuaUSD * 22700).ToString();
            }
            else
            {
                zInfo.ItemAsset.PricePurchase_USD = (giamuaVND / 22700).ToString();
                zInfo.ItemAsset.PricePurchase_VND = giamuaVND.ToString();
            }

            if (UsdBan == 1)
            {
                zInfo.ItemAsset.Price_USD = giabanUSD.ToString();
                zInfo.ItemAsset.Price_VND = (giabanUSD * 22700).ToString();
            }
            else
            {
                zInfo.ItemAsset.Price_USD = (giabanVND / 22700).ToString();
                zInfo.ItemAsset.Price_VND = giabanVND.ToString();
            }

            if (UsdThue == 1)
            {
                zInfo.ItemAsset.PriceRent_USD = giathueUSD.ToString();
                zInfo.ItemAsset.PriceRent_VND = (giathueUSD * 22700).ToString();
            }
            else
            {
                zInfo.ItemAsset.PriceRent_USD = (giathueVND / 22700).ToString();
                zInfo.ItemAsset.PriceRent_VND = giathueVND.ToString();
            }
            zInfo.ItemAsset.GroundSize = Area;
            zInfo.ItemAsset.GroundTotal = AreaBuild;
            zInfo.ItemAsset.Structure = Structure;

            zInfo.ItemAsset.AreaWall = AreaWall;
            zInfo.ItemAsset.AreaWater = AreaWater;
            zInfo.ItemAsset.LegalKey = Legal;
            zInfo.ItemAsset.FurnitureKey = Furnitur;
            zInfo.ItemAsset.Door = Door;
            zInfo.ItemAsset.View = View;
            zInfo.ItemAsset.DateContractEnd = Remind;
            zInfo.ItemAsset.Room = Room;
            zInfo.ItemAsset.StatusKey = Status;
            zInfo.ItemAsset.PurposeKey = Purpose;
            zInfo.ItemAsset.Description = Description.Trim();
            zInfo.ItemAsset.WebContent = WebContent;
            zInfo.ItemAsset.WebPublished = WebPublish;
            zInfo.ItemAsset.WebTitle = WebTitle;
            zInfo.ItemAsset.IsResale = "1";
            //if (Status == "313")//tinh trang dùng ở
            //    zInfo.ItemAsset.IsResale = "0";
            if (Purpose.Contains("/"))
            {
                string PurposeShow = "";
                string[] temp = Purpose.Split('/');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "CN":
                                PurposeShow += "Chuyển nhượng, ";
                                break;

                            case "CT":
                                PurposeShow += "Cho thuê, ";
                                break;

                            case "KT":
                                PurposeShow += "Kinh doanh, ";
                                break;
                        }
                    }

                    zInfo.ItemAsset.Purpose = PurposeShow.Remove(PurposeShow.LastIndexOf(","));
                }
            }

            if (zInfo.ItemAsset.EmployeeKey.ToInt() == 0 &&
                zInfo.ItemAsset.DepartmentKey.ToInt() == 0 &&
                zInfo.ItemAsset.CreatedBy == string.Empty &&
                zInfo.ItemAsset.CreatedDate == string.Empty &&
                zInfo.ItemAsset.CreatedName == string.Empty)
            {
                zInfo.ItemAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zInfo.ItemAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.UpdateManagerment(Type);
            }

            zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save(Type);
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            string Mess = SaveKPI(AssetKey, AssetID, EmployeeKey, DepartmentKey, Type, zInfo.ItemAsset.ProjectKey.ToInt(), Description, _Category);

            return zResult;
        }

        #region [KPI]
        public static string SaveKPI(int AssetKey, string AssetID, int EmployeeKey, int DepartmentKey, string AssetType, int ProjectKey, string Description, int Category)
        {
            string MessageKPI = Helper.KPI_Asset(AssetKey, AssetID, AssetType, ProjectKey, EmployeeKey, DepartmentKey, DateTime.Now, Description, Category, 1);
            return MessageKPI;
        }
        #endregion

        [WebMethod]
        public static ItemReturn DeleteProduct(string Type, int AssetKey)
        {
            Product_Info zInfo = new Product_Info();
            zInfo.ItemAsset.AssetKey = AssetKey.ToString();
            zInfo.Delete(Type);
            ItemReturn zResult = new ItemReturn();
            zResult.Message = zInfo.ItemAsset.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn InitProduct(string Type, string Project, string AssetID, string Note)
        {
            string table = "";
            switch (Type)
            {
                case "PUL_Resale_Apartment":
                    table = "ResaleApartment";
                    break;

                case "PUL_Resale_House":
                    table = "ResaleHouse";
                    break;

                default:
                    table = "ResaleApartment";
                    break;
            }

            ItemReturn zResult = new ItemReturn();
            Product_Info zInfo = new Product_Info(AssetID, table, Project);
            if (zInfo.ItemAsset.AssetKey.ToInt() > 0)
            {
                int CurrentKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                DataTable zTable = Share_Permition_Data.List_ShareEmployee(zInfo.ItemAsset.AssetKey.ToInt(), Type);
                foreach (DataRow r in zTable.Rows)
                {
                    if (CurrentKey == r["EmployeeKey"].ToInt())
                    {
                        zResult.Result3 = "Duplicate";
                        zResult.Message = "Đã có sản phẩm này [" + zInfo.ItemAsset.AssetID + "] bạn có muốn xem thông tin ?";
                        break;
                    }
                    else
                    {
                        zResult.Result3 = "NotAllow";
                        zResult.Message = "Đã có sản phẩm này [" + zInfo.ItemAsset.AssetID + "] và bạn chưa được chia sẽ vui lòng liên hệ [" + zInfo.ItemAsset.EmployeeName + "] !.";
                    }
                }
            }
            else
            {
                zInfo.ItemAsset.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                zInfo.ItemAsset.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
                zInfo.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                zInfo.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

                zInfo.ItemAsset.AssetType = table;
                zInfo.ItemAsset.ProjectKey = Project;
                zInfo.ItemAsset.Description = Note;
                zInfo.ItemAsset.AssetID = AssetID;
                zInfo.Create(table);
            }
            zResult.Result = zInfo.ItemAsset.AssetKey;
            zResult.Result2 = zInfo.ItemAsset.AssetType;
            return zResult;
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL02";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[3].ToInt() == 1)
            {
                Lit_Button.Text = @" <button type='button' class='btn btn-white btn-warning btn-bold' id='btnDelete'><i class='ace-icon fa fa-trash-o orange2'></i>Xóa</button>";
            }
        }
        #endregion
    }
}

