﻿var _Usdban = 0, _Usdthue = 0, _Usdmua = 0;
var hot = 0;
$(function () {
    $("input[type='checkbox']").change(function () {
        var id = this.id;
        if (id == 'chkCT') {
            if (!this.checked) {
                $("[id$=txt_PriceRentUSD]").val(0);
                $("[id$=txt_PriceRentVND]").val(0);
            }
            return;
        }
        if (id == 'chkCN') {
            if (!this.checked) {
                $("[id$=txt_PriceUSD]").val(0);
                $("[id$=txt_PriceVND]").val(0);
            }
            return;
        }
    });


    $(".iframe").colorbox({ iframe: true, width: "100%", height: "100%" });
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }

    tinymce.init({
        selector: '[id$=txt_WebContent]',
        height: 250,
        theme: 'modern',
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'
        ]
    });
});
$(function () {
    $('[moneyonly]').number(true, 0);
    $('[dientichonly]').number(true, 2);
    var unit = getUnitLevel();
    if (unit > 2) {
        $("[ishide]").hide();
    }

    $("[id$=txt_PriceRentUSD]").hide();
    $("[id$=txt_PriceUSD]").hide();
    $("[id$=txt_PricePurchase_USD]").hide();

    $("[id$=chkHot]").change(function () {
        if ($(this).is(':checked')) {
            hot = 5;
        }
        else {
            hot = 0;
        }
        console.log(hot);
    });

    $("[id$=Usdswitch1]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PriceUSD]").show();
            $("[id$=txt_PriceVND]").hide();
            $("#giaban").text("Giá bán USD");
            _Usdban = 1;
        }
        else {
            $("[id$=txt_PriceUSD]").hide();
            $("[id$=txt_PriceVND]").show();
            $("#giaban").text("Giá bán VNĐ");
            _Usdban = 2;
        }
    });
    $("[id$=Usdswitch2]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PriceRentUSD]").show();
            $("[id$=txt_PriceRentVND]").hide();
            $("#giathue").text("Giá thuê USD");
            _Usdthue = 1;
        }
        else {
            $("[id$=txt_PriceRentUSD]").hide();
            $("[id$=txt_PriceRentVND]").show();
            $("#giathue").text("Giá thuê VNĐ");
            _Usdthue = 2;
        }
    });
    $("[id$=Usdswitch3]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_PricePurchase_USD]").show();
            $("[id$=txt_PricePurchase_VND]").hide();
            $("#giamua").text("Giá mua USD");
            _Usdmua = 1;
        }
        else {
            $("[id$=txt_PricePurchase_USD]").hide();
            $("[id$=txt_PricePurchase_VND]").show();
            $("#giamua").text("Giá mua VNĐ");
            _Usdmua = 2;
        }
    });

    $("[id$=DDL_Project]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetCategoryAsset",
                data: JSON.stringify({
                    "ProjectKey": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                success: function (msg) {
                    var District = $("[id$=DDL_Category]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    var divtype = $("[id$=HID_AssetType]").val();
    $("div[type]").hide();
    $("div[type='" + divtype + "']").show();
    var SetPurpose = $("[id$=HID_Purpose]").val().split("/");
    for (var i in SetPurpose) {
        var val = $.trim(SetPurpose[i]);
        $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
    }

    $("#btnSave").click(function () {
        tinymce.triggerSave();

        var Purpose = "";
        var Door = "";
        var View = "";
        var Content = $("[id$=txt_WebContent]").val();
        var divtype = $("[id$=HID_AssetType]").val();
        console.log(divtype);
        if (divtype == 'ResaleHouse') {
            //kiem tra nha pho

        }
        if (divtype == 'ResaleApartment') {
            //kiem tra căn hộ
            if (!Page.validate()) {
                Page.showNotiMessageError("Bạn phải nhập đủ các thông tin !.", "Vui lòng kiểm tra các tab thông tin");
                return false;
            }
            if (!checkSave()) {
                return false;
            }
        }

        $("input[name='chkPurpose']:checked").each(function (i) {
            Purpose += $(this).val() + "/ ";
        });
        if ($("[id$=DDL_DirectionView]").val() != "0")
            View = $("[id$=DDL_DirectionView] option:selected").text();
        if ($("[id$=DDL_DirectionDoor]").val() != "0")
            Door = $("[id$=DDL_DirectionDoor] option:selected").text();

        $.ajax({
            type: "POST",
            url: "/SAL/ProductEdit.aspx/SaveProduct",
            data: JSON.stringify({
                "AssetKey": $('[id$=HID_AssetKey]').val(),
                "Type": $("[id$=HID_AssetType]").val(),
                "Project": $("[id$=DDL_Project]").val(),
                "Category": $("[id$=DDL_Category]").val(),
                "AssetID": $("[id$=txt_AssetID]").val(),
                "ID1": $("[id$=txt_Id1]").val(),
                "ID2": $("[id$=txt_Id2]").val(),
                "ID3": $("[id$=txt_Id3]").val(),
                "Hot": hot,
                "Price_VND": $("[id$=txt_PriceVND]").val(),
                "PriceRent_VND": $("[id$=txt_PriceRentVND]").val(),
                "Price_USD": $("[id$=txt_PriceUSD]").val(),
                "PriceRent_USD": $("[id$=txt_PriceRentUSD]").val(),
                "PricePurchase_VND": $("[id$=txt_PricePurchase_VND]").val(),
                "PricePurchase_USD": $("[id$=txt_PricePurchase_USD]").val(),
                "AreaWall": $("[id$=txt_WallArea]").val(),
                "AreaWater": $("[id$=txt_WaterArea]").val(),
                "Structure": $("[id$=txt_Sctruc]").val(),
                "AreaBuild": $("[id$=txt_GroundTotal]").val(),
                "Area": $("[id$=txt_GroundArea]").val(),
                "Legal": $("[id$=DDL_Legal]").val(),
                "Furnitur": $("[id$=DDL_Furnitur]").val(),
                "Door": Door,
                "View": View,
                "Remind": $("[id$=txt_DateContractEnd]").val(),
                "Room": $("[id$=txt_Room]").val(),
                "Status": $("[id$=DDL_Status]").val(),
                "Purpose": Purpose,
                "Description": $("[id$=txt_Description]").val(),
                "WebPublish": $("[id$=DDL_WebPublished]").val(),
                "WebTitle": $("[id$=txt_WebTitle]").val(),
                "WebContent": Content,
                "UsdMua": _Usdmua,
                "UsdBan": _Usdban,
                "UsdThue": _Usdthue,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.se-pre-con').fadeOut('slow');
                Page.showPopupMessage("Lỗi", "Liên hệ Admin");
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnDelete").click(function () {
        if (confirm("Bạn có chắc xóa.")) {
            $.ajax({
                type: "POST",
                url: "/SAL/ProductEdit.aspx/DeleteProduct",
                data: JSON.stringify({
                    "Type": $("[id$=HID_AssetType]").val(),
                    "AssetKey": $('[id$=HID_AssetKey]').val(),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message.length != 0) {
                        Page.showPopupMessage("Thông báo !.", msg.d.Message);
                    } else {
                        window.location = "/SAL/ProductList.aspx";
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });

    $(".select2").select2({ width: "100%" });

    //dell img
    $("a[btn='btnDeleteImg']").click(function () {
        var Key = $(this).closest('div').parent().attr('id');
        delImg(Key);
    });
    //save allow
    $("#btnSaveShare").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductEdit.aspx/SaveShare",
            data: JSON.stringify({
                "AssetKey": $('[id$=HID_AssetKey]').val(),
                "Type": $("[id$=HID_AssetType]").val(),
                "EmployeeKey": $('[id$=DDL_Employee]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getShare();
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //del allow
    $("#tblShare").on("click", "a[btn='btnDeleteShare']", function (e) {
        var Key = $(this).closest('div').parent().parent().attr('id');
        delShare(Key);
    });
    //save guest
    $("#btnSaveGuest").click(function () {
        $.ajax({
            type: 'POST',
            url: '/SAL/ProductEdit.aspx/SaveGuest',
            data: JSON.stringify({
                'Type': $("[id$=HID_AssetType]").val(),
                'AutoKey': $('[id$=HID_GuestKey]').val(),
                'AssetKey': $("[id$=HID_AssetKey]").val(),
                'CustomerName': $('[id$=txt_CustomerName]').val(),
                'Phone': $('[id$=txt_Phone]').val(),
                'Phone2': $('[id$=txt_Phone2]').val(),
                'Email': $('[id$=txt_Email]').val(),
                'Address': $('[id$=txt_Address]').val(),
                'Address2': $('[id$=txt_Address2]').val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getTableGuest();
                    $('[id$=HID_GuestKey]').val(0);
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //dell guest
    $("#tblGuest").on("click", "a[btn='btnDeleteGuest']", function () {
        var Key = $(this).closest('tr').attr('id');
        delGuest(Key);
    });
    //get guest
    $("#tblGuest tbody").on("click", "tr td:not(:last-child)", function (e) {
        var Key = $(this).closest('tr').attr('id');
        getGuest(Key);
    });

    var $overflow = '';
    var colorbox_params = {
        rel: 'colorbox',
        reposition: true,
        scalePhotos: true,
        scrolling: false,
        previous: '<i class="ace-icon fa fa-arrow-left"></i>',
        next: '<i class="ace-icon fa fa-arrow-right"></i>',
        close: '&times;',
        current: '{current} of {total}',
        maxWidth: '100%',
        maxHeight: '100%',
        onOpen: function () {
            $overflow = document.body.style.overflow;
            document.body.style.overflow = 'hidden';
        },
        onClosed: function () {
            document.body.style.overflow = $overflow;
        },
        onComplete: function () {
            $.colorbox.resize();
        }
    };

    $("#UploadImg").click(function () {
        $("[id$=btnUploadImg]").trigger("click");
    });

    $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
    $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon              

    $('#id-input-file-2').ace_file_input({
        style: 'well',
        no_file: 'No File ...',
        no_icon: 'ace-icon fa fa-cloud-upload',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        droppable: true,
        thumbnail: true, //| true | large
        whitelist: 'gif|png|jpg|jpeg',
        blacklist: 'exe|php'
    }).on("change", function () {
        if ($('#id-input-file-2').val().length > 0) {
            var ext2 = $('#id-input-file-2').val().split('.').pop().toLowerCase();
            if ($.inArray(ext2, ['jpg', 'png', 'gif']) == -1) {
                $("#UploadImg").addClass("disabled");
                alert('Tập tin không hợp lệ !');
            }
            else {
                $("#UploadImg").removeClass("disabled");
            }
        }
    });
});
function getShare() {
    $('#tblShare').empty();
    $.ajax({
        type: "POST",
        url: "/SAL/ProductEdit.aspx/GetShare",
        data: JSON.stringify({
            'AssetKey': $("[id$=HID_AssetKey]").val(),
            'Type': $('[id$=HID_AssetType]').val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function (msg) {
            for (var i = 0; i < msg.d.length; i++) {
                var html = "<div class='profile-info-row' id='" + msg.d[i].Value + "'>";
                html += "<div class='profile-info-name'>Chia sản phẩm cho </div><div class='profile-info-value'>" + msg.d[i].Text;
                html += "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                html += "</div>";
                $('#tblShare').append(html);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delShare(id) {
    $.ajax({
        type: 'POST',
        url: 'ProductEdit.aspx/DeleteShare',
        data: JSON.stringify({
            'AutoKey': id
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                getShare();
            }
        },
        complete: function () {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delImg(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductEdit.aspx/DeleteFile",
        data: JSON.stringify({
            'FileKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                $("ul #" + id + "").remove();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getTableGuest() {
    $('#tblGuest > tbody').empty();
    $.ajax({
        type: "POST",
        url: "/SAL/ProductEdit.aspx/GetListGuest",
        data: JSON.stringify({
            'Type': $('[id$=HID_AssetType]').val(),
            'AssetKey': $('[id$=HID_AssetKey]').val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            var num = 1;
            for (var i = 0; i < msg.d.length; i++) {
                $('#tblGuest > tbody:last').append('<tr class="" id="' + msg.d[i].OwnerKey + '">'
                        + '<td>' + (num++) + '</td>'
                        + '<td>' + msg.d[i].CustomerName + '</td>'
                        + '<td>' + msg.d[i].Phone1 + '<br/>' + msg.d[i].Phone2 + '</td>'
                        + '<td>' + msg.d[i].Email1 + '</td>'
                        + '<td>' + msg.d[i].Address1 + '<br/>' + msg.d[i].Address2 + '</td>'
                        + '<td>' + msg.d[i].CreatedDate + '</td>'
                        + '<td><div class="hidden-sm hidden-xs action-buttons pull-right"><a btn="btnDeleteGuest" href="#" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i> Xóa</a></div></td>'
                        + '</tr>');
            }
        },
        complete: function () {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getGuest(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductEdit.aspx/GetGuest",
        data: JSON.stringify({
            'Type': $('[id$=HID_AssetType]').val(),
            'AutoKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
            }
            else {
                $('#mGuest').modal({
                    backdrop: true,
                    show: true
                });

                $("[id$=HID_AssetType]").val(msg.d.Type);
                $("[id$=HID_GuestKey]").val(msg.d.OwnerKey);
                $("[id$=HID_AssetKey]").val(msg.d.AssetKey);
                $("[id$=txt_CustomerName]").val(msg.d.CustomerName);
                $("[id$=txt_Phone]").val(msg.d.Phone1);
                $("[id$=txt_Phone2]").val(msg.d.Phone2);
                $("[id$=txt_Email]").val(msg.d.Email1);
                $("[id$=txt_Address]").val(msg.d.Address1);
                $("[id$=txt_Address2]").val(msg.d.Address2);
            }
        },
        complete: function () {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delGuest(id) {
    if (confirm("Bạn có chắc xóa.")) {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductEdit.aspx/DeleteGuest",
            data: JSON.stringify({
                'Type': $('[id$=HID_AssetType]').val(),
                'AutoKey': id,
                'AssetKey': $('[id$=HID_AssetKey]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (msg) {
                if (msg.d.Message != '') {
                    Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
                } else {
                    getTableGuest();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}
function ConvertMoney() {
    if (_Usd == 1) {
        var priceUsd = $("[id$=txt_PriceUSD]").val();
        var priceRentUsd = $("[id$=txt_PriceRentUSD]").val();
        var pricePurchaseUSD = $("[id$=txt_PricePurchase_USD]").val();

        var priceVnd = Page.RemoveComma(priceUsd) * usdRate;
        var priceRentVnd = Page.RemoveComma(priceRentUsd) * usdRate;
        var pricePurchaseVnd = Page.RemoveComma(pricePurchaseUSD) * usdRate;

        $("[id$=txt_PricePurchase_VND]").val(Page.FormatMoney(pricePurchaseVnd));
        $("[id$=txt_PriceRentVND]").val(Page.FormatMoney(priceRentVnd));
        $("[id$=txt_PriceVND]").val(Page.FormatMoney(priceVnd));
    }
    else {
        var priceVnd = $("[id$=txt_PriceVND]").val();
        var priceRentVnd = $("[id$=txt_PriceRentVND]").val();
        var pricePurchaseVnd = $("[id$=txt_PricePurchase_VND]").val();

        var priceUsd = Page.RemoveComma(priceVnd) / usdRate;
        var priceRentUsd = Page.RemoveComma(priceRentVnd) / usdRate;
        var pricePurchaseUsd = Page.RemoveComma(pricePurchaseVnd) / usdRate;

        $("[id$=txt_PricePurchase_USD]").val(Page.FormatMoney(pricePurchaseUsd));
        $("[id$=txt_PriceRentUSD]").val(Page.FormatMoney(priceRentUsd));
        $("[id$=txt_PriceUSD]").val(Page.FormatMoney(priceUsd));
    }
}
function getUnitLevel() {
    cookieList = document.cookie.split('; ');
    cookies = {};
    for (i = cookieList.length - 1; i >= 0; i--) {
        var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
        if (cName == 'UserLog') {
            var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
            cookies = val.split('&')[3].split('=');
            return cookies[1];
        }
    }
}
function checkSave() {
    var status = $("[id$=DDL_Status]").val();
    var noithat = $("[id$=DDL_Furnitur]").val();

    //313 dùng ở bỏ qua hết
    if (status == 313 || status == 29) {
        return true;
    }

    if (status == null ||
        status == undefined ||
        status == 0) {
        Page.showNotiMessageError("Bạn phải chọn tình trạng !.");
        return false;
    }
    //314 lien hệ lại
    if (status == 314) {
        if ($("#txt_DateContractEnd").val() == "") {
            Page.showNotiMessageError("Bạn phải nhập đủ các thông tin !.", "Vui lòng nhập thông tin ngày liên hệ lại !.");
            return false;
        }
    }
    else {
        if (!$('#chkCT').prop('checked') &&
            !$('#chkCN').prop('checked')) {
            Page.showNotiMessageError("Bạn phải nhập chọn nhu cầu");
            return false;
        }
        if ($('#chkCT').prop('checked')) {
            if ($('[id$=txt_PriceRentVND]').val() == 0 && _Usdthue == 2) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá thuê VNĐ");
                return false;
            }
            if ($('[id$=txt_PriceRentUSD]').val() == 0 && _Usdthue == 1) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá thuê USD");
                return false;
            }
        }
        if ($('#chkCN').prop('checked')) {
            if ($('[id$=txt_PriceVND]').val() == 0 && _Usdban == 2) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá chuyển nhượng VNĐ");
                return false;
            }
            if ($('[id$=txt_PriceUSD]').val() == 0 && _Usdban == 1) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá chuyển nhượng USD");
                return false;
            }
        }
        if (noithat == null || noithat == undefined || noithat == 0) {
            Page.showNotiMessageError("Bạn phải chọn nội thất !.");
            return false;
        }
    }

    return true;
}