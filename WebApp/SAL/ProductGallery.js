﻿var
    _StatusActionInfo = 0, _StatusActionTask = 0, _Agent = 0,
    _Usd = 0, _Floor = 0, _Tower = 0;
$(document).on('change', '[type=checkbox]', function (e) {
    if ($(this).is(':checked')) {
        $("div[view='usd']").show();
        $("div[view='vnd']").hide();
    }
    else {
        $("div[view='vnd']").show();
        $("div[view='usd']").hide();
    }
});
$(document).on('click', '.modal-backdrop.in', function (e) {
    $('#closeLeft').trigger("click");
});
$(document).on('hidden.bs.modal', '#mViewInput', function (event) {
    Page.resetValue("mViewInput");
});
$(function () {
    _Agent = Page.getEmployee();
    console.log(_Agent);
    $("#infoNew").click(function () {
        $('.btn-group a.active').not(this).removeClass('active');
        $(this).toggleClass("active");
        if ($(this).hasClass("active")) {
            $("#tableData td:not(:first-child) div.ItemIcon[statustask=4][currentagent=" + _Agent + "]").css({ 'opacity': 1 });
            $("#tableData td:not(:first-child) div.ItemIcon:not([statustask=4][currentagent=" + _Agent + "])").css({ 'opacity': 0.35 });       
        }
        else {
            $("#tableData td:not(:first-child) div.ItemIcon").css({ 'opacity': 1 });
        }
    });
    $("#infoResale").click(function () {
        console.log(_Agent);
        $('.btn-group a.active').not(this).removeClass('active');
        $(this).toggleClass("active");
        if ($(this).hasClass("active")) {
            $("#tableData td:not(:first-child) div.ItemIcon[status=314][currentagent=" + _Agent + "]").css({ 'opacity': 1 });
            $("#tableData td:not(:first-child) div.ItemIcon:not([status=314][currentagent=" + _Agent + "])").css({ 'opacity': 0.35 });
        }
        else {
            $("#tableData td:not(:first-child) div.ItemIcon").css({ 'opacity': 1 });
        }
    });
    $("#infoTrade").click(function () {
        console.log(_Agent);
        $('.btn-group a.active').not(this).removeClass('active');
        $(this).toggleClass("active");
        if ($(this).hasClass("active")) {
            $("#tableData td:not(:first-child) div.ItemIcon[status=29][currentagent=" + _Agent + "]").css({ 'opacity': 1 });
            $("#tableData td:not(:first-child) div.ItemIcon:not([status=29][currentagent=" + _Agent + "])").css({ 'opacity': 0.35 });
        }
        else {
            $("#tableData td:not(:first-child) div.ItemIcon").css({ 'opacity': 1 });
        }
    });
    $("#infoBlank").click(function () {
        console.log(_Agent);
        $('.btn-group a.active').not(this).removeClass('active');
        $(this).toggleClass("active");
        if ($(this).hasClass("active")) {
            $("#tableData td:not(:first-child) div.ItemIcon[status=0][currentagent=" + _Agent + "]").css({ 'opacity': 1 });
            $("#tableData td:not(:first-child) div.ItemIcon:not([status=0][currentagent=" + _Agent + "])").css({ 'opacity': 0.35 });
        }
        else {
            $("#tableData td:not(:first-child) div.ItemIcon").css({ 'opacity': 1 });
        }
    });

    $("[id$=txt_updatePriceRentUSD]").hide();
    $("[id$=txt_updatePriceUSD]").hide();
    $("[id$=txt_updatePricePurchaseUSD]").hide();
    $("[id$=Usdswitch1]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_updatePriceRentUSD]").show();
            $("[id$=txt_updatePriceUSD]").show();
            $("[id$=txt_updatePricePurchaseUSD]").show();

            $("[id$=txt_updatePriceRentVND]").hide();
            $("[id$=txt_updatePriceVND]").hide();
            $("[id$=txt_updatePricePurchaseVND]").hide();

            $("#updategiathue").text("Giá thuê USD");
            $("#updategiaban").text("Giá bán USD");
            $("#updategiamua").text("Giá mua USD");
            _Usd = 1;
        }
        else {
            $("[id$=txt_updatePriceRentUSD]").hide();
            $("[id$=txt_updatePriceUSD]").hide();
            $("[id$=txt_updatePricePurchaseUSD]").hide();

            $("[id$=txt_updatePriceRentVND]").show();
            $("[id$=txt_updatePriceVND]").show();
            $("[id$=txt_updatePricePurchaseVND]").show();

            $("#updategiathue").text("Giá thuê VNĐ");
            $("#updategiaban").text("Giá bán VNĐ");
            $("#updategiamua").text("Giá mua VNĐ");
            _Usd = 2;
        }
    });
    $("[id$=txt_updatePricePurchaseUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_updatePricePurchaseVND]").blur(function () { ConvertMoney(); });
    $("[id$=txt_updatePriceUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_updatePriceRentUSD]").blur(function () { ConvertMoney(); });
    $("[id$=txt_updatePriceVND]").blur(function () { ConvertMoney(); });
    $("[id$=txt_updatePriceRentVND]").blur(function () { ConvertMoney(); });
    $("#DDL_Want").on("change", function () {
        var want = this.value;
        if (want == 0) {
            $("#tableData td:not(:first-child) div.ItemIcon[want][status=31]").css({ 'opacity': 1 });
            return;
        }
        $("#tableData td:not(:first-child) div.ItemIcon[want][status=31]").css({ 'opacity': 1 });
        $("#tableData td:not(:first-child) div.ItemIcon:not([want='" + want + "'][status=31])").css({ 'opacity': 0.25 });
    });
    $("#DDL_Status").on("change", function () {
        var status = this.value;
        if (status == 0) {
            $("#tableData td:not(:first-child) div.ItemIcon[status]").css({ 'opacity': 1 });
            return;
        }
        $("#tableData td:not(:first-child) div.ItemIcon[status]").css({ 'opacity': 1 });
        $("#tableData td:not(:first-child) div.ItemIcon:not([status='" + status + "'])").css({ 'opacity': 0.25 });
    });

    $('#isPrivacyTask').change(function () {
        if ($(this).prop('checked')) {
            $("#tableData td:not(:first-child) div.ItemIcon[statustask=6]").css({ 'opacity': 1 });
            $("#tableData td:not(:first-child) div.ItemIcon:not([statustask=6])").css({ 'opacity': 0.25 });
        }
        else {
            $("#tableData td:not(:first-child) div.ItemIcon[statustask]").css({ 'opacity': 1 });
        }
    })
    //-------------------------------Event action
    $(".modal.aside").ace_aside();
    $(".footer").css("display", "none");
    $(".select2").select2({ width: "100%" });
    var height = $(window).height() - 210;
    $('#parent').height(height);
    //---------------------------------End lay out
    $("#btnProcessInfo").click(function () {
        ActionSave();
    });
    $("#btnProcessTask").click(function () {
        SaveTask()
    });
    $("#btnSearch").click(function () {
        Search();
    });
    //----------------------------------
    GenerateTable();
});
function ConvertMoney() {
    if (_Usd == 1) {
        var priceUsd = $("[id$=txt_updatePriceUSD]").val();
        var priceRentUsd = $("[id$=txt_updatePriceRentUSD]").val();
        var pricePurchaseUSD = $("[id$=txt_updatePricePurchaseUSD]").val();

        var priceVnd = Page.RemoveComma(priceUsd) * usdRate;
        var priceRentVnd = Page.RemoveComma(priceRentUsd) * usdRate;
        var pricePurchaseVnd = Page.RemoveComma(pricePurchaseUSD) * usdRate;

        $("[id$=txt_updatePricePurchaseVND]").val(Page.FormatMoney(pricePurchaseVnd));
        $("[id$=txt_updatePriceRentVND]").val(Page.FormatMoney(priceRentVnd));
        $("[id$=txt_updatePriceVND]").val(Page.FormatMoney(priceVnd));
    }
    else {
        var priceVnd = $("[id$=txt_updatePriceVND]").val();
        var priceRentVnd = $("[id$=txt_updatePriceRentVND]").val();
        var pricePurchaseVnd = $("[id$=txt_updatePricePurchaseVND]").val();

        var priceUsd = Page.RemoveComma(priceVnd) / usdRate;
        var priceRentUsd = Page.RemoveComma(priceRentVnd) / usdRate;
        var pricePurchaseUsd = Page.RemoveComma(pricePurchaseVnd) / usdRate;

        $("[id$=txt_updatePricePurchaseUSD]").val(Page.FormatMoney(pricePurchaseUsd));
        $("[id$=txt_updatePriceRentUSD]").val(Page.FormatMoney(priceRentUsd));
        $("[id$=txt_updatePriceUSD]").val(Page.FormatMoney(priceUsd));
    }
}
function GenerateTable() {
    var tower = $("#DDL_Tower").val();
    var floor = $("#DDL_Floor").val();
    if (tower == null || tower <= 0)
        tower = $("#DDL_Tower").prop('selectedIndex', 1).val();//mac dinh chon 1 thap
    if (floor == null || floor <= 0)
        floor = $("#DDL_Floor").prop('selectedIndex', 1).val();
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GenerateTable",
        data: JSON.stringify({
            "ProjectKey": $("[id$=HID_ProjectKey]").val(),
            "MainTable": $("[id$=HID_MainTable]").val(),
            "ShortTable": $("[id$=HID_AssetType]").val(),
            "Tower": tower,
            "Floor": floor,
            "AssetNo": $("[id$=txtAssetNo]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            $('#parent').empty();
            $('#parent').append($(r.d));
        },
        complete: function () {
            setTimeout(function () {
                $('.se-pre-con').fadeOut('slow');
                $("#tableData").tableHeadFixer({
                    "head": false,
                    "left": 1
                });
            }, 3000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function Search() {
    var tower = $("#DDL_Tower").val();
    var floor = $("#DDL_Floor").val();
    if (tower == null || tower <= 0)
        tower = $("#DDL_Tower").prop('selectedIndex', 1).val();//mac dinh chon 1 thap
    if (floor == null || floor <= 0)
        floor = $("#DDL_Floor").prop('selectedIndex', 1).val();

    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GenerateTable",
        data: JSON.stringify({
            "ProjectKey": $("[id$=HID_ProjectKey]").val(),
            "MainTable": $("[id$=HID_MainTable]").val(),
            "ShortTable": $("[id$=HID_AssetType]").val(),
            "Tower": tower,
            "Floor": floor,
            "AssetNo": $("[id$=txtAssetNo]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            $('#parent').empty();
            $('#parent').append($(r.d));
        },
        complete: function () {
            setTimeout(function () {
                $('.se-pre-con').fadeOut('slow');
                $("#tableData").tableHeadFixer({
                    "head": false,
                    "left": 1
                });
            }, 3000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//---------------------------------Process Info
function OpenInfo(AssetKey, AssetType) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/OpenInfo",
        data: JSON.stringify({
            "AssetKey": AssetKey,
            "AssetType": AssetType,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            $("#PageHead").empty();
            $("#PageHead").append($(msg.d.Result2));
            $("#PageInfo").empty();
            $("#PageInfo").append($(msg.d.Result));
            $("#mView").modal({
                backdrop: true,
                show: true
            });

            setTimeout(function () { GetEditAsset(AssetKey, AssetType), 2000 });
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
    $("#ReplyInfo").text("");
}
function ActionInfo(ID, Txt) {
    _StatusActionInfo = ID;
    $("#mViewInput").modal("show");
    $("#btnProcessTask").hide();
    $("#btnProcessInfo").show();
    $("#kygui").hide();
    $("#chunha").hide();
    $("#msgInfo").text("Bạn chọn xử lý: [" + Txt + "]");

    if (ID == 1) {
        $("#kygui").show();
        $("#msgInfo").text("Bạn chọn xử lý: [" + Txt + "] vui lòng kiểm tra các thông tin liên quan !.");
    }
    if (ID == 9) {
        $("#chunha").show();
        $("#msgInfo").text("Bạn chọn xử lý: [" + Txt + "] vui lòng kiểm tra các thông tin liên quan !.");
    }
}
function ActionSave() {
    if (_StatusActionInfo == 1) {
        SaveAsset();
    }
    if (_StatusActionInfo == 9) {
        SaveGuest();
    }
}
function GetEditAsset(AssetKey, AssetType) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetEditAsset",
        data: JSON.stringify({
            "AssetKey": AssetKey,
            "AssetType": AssetType,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            //-----------Info Guest
            $("[id$=HID_AssetType]").val(msg.d.AssetType);
            $("[id$=HID_GuestKey]").val(msg.d.AutoKey);
            $("[id$=HID_AssetKey]").val(msg.d.AssetKey);
            $("[id$=txt_InfoName]").val(msg.d.CustomerName);
            $("[id$=txt_InfoPhone1]").val(msg.d.Phone1);
            $("[id$=txt_InfoPhone2]").val(msg.d.Phone2);
            $("[id$=txt_InfoEmail]").val(msg.d.Email1);
            $("[id$=txt_InfoAddress1]").val(msg.d.Address1);
            $("[id$=txt_InfoAddress2]").val(msg.d.Address2);
            //-----------Info Asset
            $("[id$=txt_updateDateContractEnd]").val(msg.d.DateContractEnd);
            $("[id$=txt_updatePriceRentVND]").val(msg.d.PriceRent_VND);
            $("[id$=txt_updatePriceRentUSD]").val(msg.d.PriceRent_USD);
            $("[id$=txt_updatePriceVND]").val(msg.d.Price_VND);
            $("[id$=txt_updatePriceUSD]").val(msg.d.Price_USD);
            $("[id$=txt_updatePricePurchaseVND]").val(msg.d.PricePurchase_VND);
            $("[id$=txt_updatePricePurchaseUSD]").val(msg.d.PricePurchase_USD);

            $("[id$=DDL_Legal]").val(msg.d.LegalKey).trigger("change");
            $("[id$=DDL_Furnitur2]").val(msg.d.FurnitureKey).trigger("change");
            $("[id$=DDL_StatusAsset2]").val(msg.d.StatusKey).trigger("change");
            $("[id$=txt_updateAssetDescription]").val(msg.d.Description);
            var SetPurpose = msg.d.PurposeKey.split("/");
            for (var i in SetPurpose) {
                var val = $.trim(SetPurpose[i]);
                $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveAsset() {
    var Purpose = "";
    $("input[name='chkPurpose']:checked").each(function (i) {
        Purpose += $(this).val() + "/ ";
    });
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/SaveAsset",
        data: JSON.stringify({
            "AssetKey": $("[id$=HID_AssetKey]").val(),
            "AssetType": $("[id$=HID_AssetType]").val(),
            "AssetStatus": $("[id$=DDL_StatusAsset2]").val(),
            "AssetRemind": $("[id$=txt_updateDateContractEnd]").val(),
            "AssetPrice_VND": $("[id$=txt_updatePriceVND]").val(),
            "AssetPriceRent_VND": $("[id$=txt_updatePriceRentVND]").val(),
            "AssetPrice_USD": $("[id$=txt_updatePriceUSD]").val(),
            "AssetPriceRent_USD": $("[id$=txt_updatePriceRentUSD]").val(),
            "AssetPricePurchase_VND": $("[id$=txt_updatePricePurchaseVND]").val(),
            "AssetPricePurchase_USD": $("[id$=txt_updatePricePurchaseUSD]").val(),
            "AssetFurnitur": $("[id$=DDL_Furnitur2]").val(),
            "AssetPurpose": Purpose,
            "AssetDescription": $("[id$=txt_updateAssetDescription]").val(),
            "AssetLegal": $("[id$=DDL_Legal]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
            $("#mViewInput").modal("hide");
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.se-pre-con').fadeOut('slow');
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveGuest() {
    $.ajax({
        type: 'POST',
        url: '/SAL/ProductGallery.aspx/SaveGuest',
        data: JSON.stringify({
            'AutoKey': $('[id$=HID_GuestKey]').val(),
            'AssetType': $("[id$=HID_AssetType]").val(),
            'AssetKey': $("[id$=HID_AssetKey]").val(),
            'CustomerName': $('[id$=txt_InfoName]').val(),
            'Phone1': $('[id$=txt_InfoPhone1]').val(),
            'Phone2': $('[id$=txt_InfoPhone2]').val(),
            'Email1': $('[id$=txt_InfoEmail]').val(),
            'Address1': $('[id$=txt_InfoAddress1]').val(),
            'Address2': $('[id$=txt_InfoAddress2]').val(),
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message.length > 0) {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            } else {
                $("#mViewInput").modal("hide");
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function JustViewInfo(AssetKey, AssetType) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/JustViewInfo",
        data: JSON.stringify({
            "AssetKey": AssetKey,
            "AssetType": AssetType,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            $("#PageHead").empty();
            $("#PageHead").append($(msg.d.Result2));
            $("#PageInfo").empty();
            $("#PageInfo").append($(msg.d.Result));
            $("#mView").modal({
                backdrop: true,
                show: true
            });

            var ProjectKey = $("[id$=HID_ProjectKey]").val();
            var TaskKey = msg.d.Result3;
            var Asset = msg.d.Result4;
            setTimeout(function () { GetNearestTask(ProjectKey, Asset, TaskKey); }, 1000);
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
    $("#ReplyInfo").text("");

    //if (confirm("SP này chưa có thông tin bạn có muốn tạo mới thông tin")) {
    //    $('.se-pre-con').fadeIn('slow');
    //    window.location = "/SAL/ProductEdit.aspx?ID=" + AssetKey + "&Type=" + AssetType;
    //}
}
//---------------------------------Process Task
function OpenTask(AssetKey, TaskKey) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/OpenTask",
        data: JSON.stringify({
            "TaskKey": TaskKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            $("[id$=HID_InfoKey]").val(msg.d.Message);

            $("#PageHead").empty();
            $("#PageHead").append($(msg.d.Result2));
            $("#PageInfo").empty();
            $("#PageInfo").append($(msg.d.Result));
            $("#mView").modal({
                backdrop: true,
                show: true
            });

            var ProjectKey = $("[id$=HID_ProjectKey]").val();
            var Asset = msg.d.Result4;

            setTimeout(function () { GetTaskGuest(TaskKey); }, 1000);
            setTimeout(function () { GetNearestTask(ProjectKey, Asset, TaskKey); }, 1000);
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
    $("#kygui").hide();
    $("#chunha").hide();
    $("#msgInfo").text("");
    $("#ReplyInfo").text("");
    $("[id$=HID_AssetKey]").val(AssetKey);
    $("#btnUpdateInfo").hide();
}
function ActionTask(ID, Txt) {
    _StatusActionTask = ID;

    if (ID == 1) {
        $("#mViewInput").modal("show");
        $("#btnProcessInfo").hide();
        $("#btnProcessTask").show();

        $("#kygui").show();
        $("#chunha").show();
        $("#msgInfo").text("Bạn chọn xử lý: [" + Txt + "] vui lòng kiểm tra các thông tin liên quan !.");
    }
    else {
        //$("#ReplyInfo").text("Bạn chọn xử lý [" + Txt + "] !.");
        //$("#btnUpdateInfo").show();
        if (confirm("Bạn có đồng ý xử lý [" + Txt + "]")) {
            UpdateTask();
        }
    }
}
function ActionSaveTask() {
    if (_StatusActionTask == 1) {
        SaveTask();
    }
    else {
        UpdateTask();
    }
}
function GetTaskGuest(TaskKey) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetTaskGuest",
        data: JSON.stringify({
            "TaskKey": TaskKey
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () { },
        success: function (msg) {
            $("#txt_InfoName").val(msg.d.CustomerName);
            $("#txt_InfoPhone1").val(msg.d.Phone1);
            $("#txt_InfoPhone2").val(msg.d.Phone2);
            $("#txt_InfoEmail").val(msg.d.Email1);
            $("#txt_InfoAddress1").val(msg.d.Address1);
            $("#txt_InfoAddress2").val(msg.d.Address2);
        },
        complete: function () { },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveTask() {
    var TaskKey = $("[id$=HID_InfoKey]").val();
    var AssetKey = $("[id$=HID_AssetKey]").val();
    var Purpose = "";
    $("input[name='chkPurpose']:checked").each(function (i) {
        Purpose += $(this).val() + "/ ";
    });
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/SaveTask",
        data: JSON.stringify({
            "TaskKey": TaskKey,
            "TaskStatus": _StatusActionTask,
            "AssetKey": AssetKey,
            "AssetType": $("[id$=HID_AssetType]").val(),
            "AssetStatus": $("[id$=DDL_StatusAsset2]").val(),
            "AssetRemind": $("[id$=txt_updateDateContractEnd]").val(),
            "AssetPrice_VND": $("[id$=txt_updatePriceVND]").val(),
            "AssetPriceRent_VND": $("[id$=txt_updatePriceRentVND]").val(),
            "AssetPrice_USD": $("[id$=txt_updatePriceUSD]").val(),
            "AssetPriceRent_USD": $("[id$=txt_updatePriceRentUSD]").val(),
            "AssetPricePurchase_VND": $("[id$=txt_updatePricePurchaseVND]").val(),
            "AssetPricePurchase_USD": $("[id$=txt_updatePricePurchaseUSD]").val(),
            "AssetFurnitur": $("[id$=DDL_Furnitur2]").val(),
            "AssetPurpose": Purpose,
            "AssetDescription": $("[id$=txt_updateAssetDescription]").val(),
            "AssetLegal": $("[id$=DDL_Legal]").val(),
            'CustomerName': $("[id$=txt_InfoName]").val(),
            'Email': $("[id$=txt_InfoEmail]").val(),
            'Phone1': $("[id$=txt_InfoPhone1]").val(),
            'Phone2': $("[id$=txt_InfoPhone2]").val(),
            'Address1': $("[id$=txt_InfoAddress1]").val(),
            'Address2': $("[id$=txt_InfoAddress2]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                location.reload();
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.se-pre-con').fadeOut('slow');
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function UpdateTask() {
    $.ajax({
        type: 'POST',
        url: '/SAL/ProductGallery.aspx/UpdateTask',
        data: JSON.stringify({
            'TaskKey': $("[id$=HID_InfoKey]").val(),
            'TaskStatus': _StatusActionTask,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu cập nhật thông tin thành công.");
                location.reload();
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (xhr.responseText.indexOf('network') != -1)
                Page.showNotiMessageInfo("Lỗi mạng", "Vui lòng thử lại sau ít phút !.");
            else
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//--------------------------------Process Task and Info
function OpenTaskAndInfo(AssetKey, AssetType, TaskKey) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/OpenTaskAndInfo",
        data: JSON.stringify({
            "AssetKey": AssetKey,
            "AssetType": AssetType,
            "TaskKey": TaskKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            $("#PageHead").empty();
            $("#PageHead").append($(msg.d.Result2));
            $("#PageInfo").empty();
            $("#PageInfo").append($(msg.d.Result));
            $("#mView").modal({
                backdrop: true,
                show: true
            });

            var ProjectKey = $("[id$=HID_ProjectKey]").val();
            var Asset = msg.d.Result4;
            setTimeout(function () { GetEditAsset(AssetKey, AssetType); }, 1000);
            setTimeout(function () { GetTaskGuest(TaskKey); }, 1000);
            setTimeout(function () { GetNearestTask(ProjectKey, Asset, TaskKey); }, 1000);
        },
        complete: function () {
            $(".se-pre-con").fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
    $("#ReplyInfo").text("");
    $("[id$=HID_InfoKey]").val(TaskKey);
}
function GetNearestTask(ProjectKey, Asset, TaskKey) {
    $("#ReplyInfoEarlier").text("");
    $.ajax({
        type: "POST",
        url: "/SAL/ProductGallery.aspx/GetNearestTask",
        data: JSON.stringify({
            "ProjectKey": ProjectKey,
            "Asset": Asset,
            "TaskKey": TaskKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function (msg) {
            $("#ReplyInfoEarlier").text(msg.d.Message);
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}