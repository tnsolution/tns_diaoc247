﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="WebApp.SAL.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
    <style>
        #ui-id-1 {
            z-index: 10000;
        }

        sup {
            vertical-align: sup;
            font-size: smaller;
        }

        #tblData th {
            padding: 0px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Sản phẩm..
                <span class="pull-right">
                    <a href="#" id="viewExten" class="btn btn-primary btn-white" onclick="$('.se-pre-con').fadeIn('slow')" style="display: none"><i class="ace-icon fa fa-external-link"></i>&nbsp;Xem biểu tượng</a>
                    <a href="#mProduct" id="createNew" class="btn btn-primary btn-white" data-toggle="modal"><i class="ace-icon fa fa-plus"></i>&nbsp;Tạo mới</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <asp:Literal ID="Lit_Message" runat="server"></asp:Literal>
                <table class='table table-hover table-bordered' id='tblData'>
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>
                                <select id="duan" class="form-control"></select></th>
                            <th>
                                <input class="input-sm form-control" type="text" value="" placeholder="Mã sản phẩm" onkeyup="myFunction(this,2)" id="input1" /></th>
                            <th>
                                <select id="loaisp" class="form-control"></select></th>
                            <th style="width:230px">
                                <input class="input-sm form-control" type="text" value="" placeholder="Mục đích/ Giá" onkeyup="myFunction(this,4)" id="input2" /></th>
                            <th style="width: 10%;">
                                <input class="input-sm form-control" type="text" value="" placeholder="Số PN/ DT" onkeyup="myFunction(this,5)" id="input3" /></th>
                            <th>Tình trạng</th>
                            <th>Nội thất</th>
                            <th>Hình ảnh</th>
                            <th>Ngày cập nhật</th>
                            <th>Nhân viên</th>
                            <th>Ghi chú</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID="LitTable_Body" runat="server"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm sản phẩm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%" required>
                            <asp:ListItem Value="0" Text="-- Chọn sản phẩm khu vực --" disabled="disabled" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Project" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Tất cả dự án--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Category" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Tất cả sản phẩm--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space-6"></div>
                        <div id="accordion" class="accordion-style1 panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                                            <i class="bigger-110 ace-icon fa fa-angle-down" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                                            &nbsp;Nâng cao
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="collapseOne" aria-expanded="true">
                                    <div class="panel-body no-padding">
                                        <input type="text" id="txt_ID1" class="form-control" placeholder="-- Tháp/ Khu --" />
                                        <input type="text" id="txt_ID2" class="form-control" placeholder="-- Tầng/ Dãy --" />
                                        <input type="text" id="txt_ID3" class="form-control" placeholder="-- Số căn --" />
                                        <div class="space-6"></div>
                                        <input type="text" id="txt_Bed" class="form-control" placeholder="-- Số phòng ngủ --" />
                                        <input type="text" id="txt_Name" class="form-control" placeholder="-- Mã sản phẩm--" />
                                        <div class="space-6"></div>
                                        <asp:DropDownList ID="DDL_View" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                                            <asp:ListItem Value="0" Text="--Chọn hướng view--" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="Đông" Text="Đông"></asp:ListItem>
                                            <asp:ListItem Value="Tây" Text="Tây"></asp:ListItem>
                                            <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                                            <asp:ListItem Value="Bắc" Text="Bắc"></asp:ListItem>
                                            <asp:ListItem Value="Đông nam" Text="Đông nam"></asp:ListItem>
                                            <asp:ListItem Value="Tây Bắc" Text="Tây Bắc"></asp:ListItem>
                                            <asp:ListItem Value="Đông bắc" Text="Đông bắc"></asp:ListItem>
                                            <asp:ListItem Value="Tây Nam" Text="Tây Nam"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="DDL_Door" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                                            <asp:ListItem Value="0" Text="--Chọn hướng cửa--" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="Đông" Text="Đông"></asp:ListItem>
                                            <asp:ListItem Value="Tây" Text="Tây"></asp:ListItem>
                                            <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                                            <asp:ListItem Value="Bắc" Text="Bắc"></asp:ListItem>
                                            <asp:ListItem Value="Đông nam" Text="Đông nam"></asp:ListItem>
                                            <asp:ListItem Value="Tây Bắc" Text="Tây Bắc"></asp:ListItem>
                                            <asp:ListItem Value="Đông bắc" Text="Đông bắc"></asp:ListItem>
                                            <asp:ListItem Value="Tây Nam" Text="Tây Nam"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space-6"></div>
                                        <input type="text" id="txt_Customer" class="form-control" placeholder="-- Chủ nhà --" />
                                        <input type="text" id="txt_Phone" class="form-control" placeholder="-- Số điện thoại --" />
                                        <div class="space-6"></div>
                                        <label>Ngày cập nhật</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right"
                                                id="txtFromDate" placeholder="Từ " />
                                        </div>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right"
                                                id="txtToDate" placeholder="Đến " />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nhu cầu</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace" value="CN" name="chkPurpose" />
                                <span class="lbl">Chuyển nhượng</span>
                            </label>
                        </div>
                        <div class="checkbox" style="display: none">
                            <label>
                                <input type="checkbox" class="ace" value="KT" name="chkPurpose" />
                                <span class="lbl">Khai thác kinh doanh</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace" value="CT" name="chkPurpose" />
                                <span class="lbl">Cho thuê</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tình trạng</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace" value="314" name="chkStatus" />
                                <span class="lbl">Liên hệ lại</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace" value="29" name="chkStatus" />
                                <span class="lbl">Đã bán</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace" value="30" name="chkStatus" />
                                <span class="lbl">Đã cho thuê</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace" value="31" name="chkStatus" />
                                <span class="lbl">Chưa giao dịch</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="ace input" value="1" id="chkPersonal" />
                                <span class="lbl">Sản phẩm của cá nhân
                                    <br />
                                    không bao gồm cả các sản phẩm được chia sẽ</span>
                            </label>
                        </div>
                        <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                            <i class="ace-icon fa fa-search"></i>
                            Tìm
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade modal-default" id="mProduct" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">KHỞI TẠO THÔNG TIN CƠ BẢN CHO SẢN PHẨM</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td style="width: 150px;">Sản phẩm là(*)</td>
                                        <td>
                                            <asp:DropDownList ID="DDL_Table2" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn 1--" disabled="disabled" Selected="True"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>Tên dự án (*)</td>
                                        <td>
                                            <asp:DropDownList ID="DDL_Project2" CssClass="form-control select2" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn dự án--" disabled="disabled" Selected="True"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td>Mã sản phẩm</td>
                                        <td>
                                            <input type="text" class="form-control typeahead" id="txt_AssetID" placeholder="Nhập mã sản phẩm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ghi chú</td>
                                        <td>
                                            <textarea id="txt_Asset_Description" cols="20" rows="4" class="form-control" placeholder="Ghi chú"></textarea></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal">Hủy</button>
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnInitProduct">Tiếp tục</button>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="HID_View" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Asset" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/jquery.gritter.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script src="/SAL/ProductList.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txtFromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txtToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });

        $(function () {
            $('[data-toggle="popover"]').popover();
            $('[data-toggle="popover"]').on("click", function (e) {
                $('[data-toggle="popover"]').not(this).popover('hide');
            });

            makeSelectFromColumn("duan", 2);
            makeSelectFromColumn("loaisp", 4);

            $("#loaisp").on("change", function (e) {
                myFunction(this, 3);
            });
            $("#duan").on("change", function (e) {
                myFunction(this, 1);
            });
        });
        function makeSelectFromColumn(id, n) {
            var arr = [];
            $("#tblData td:nth-child(" + n + ")").each(function () {
                if ($.inArray($(this).text(), arr) == -1)
                    arr.push($(this).text());
            });
            arr.sort();
            //Create your select
            $("#" + id).empty();
            for (var i = 0; i < arr.length; i++) {
                $("<option value=" + arr[i] + ">" + arr[i] + "</option>").appendTo("#" + id);
            }
        }
        function myFunction(arg, no) {
            var input, filter, table, tr, td, i;
            input = $("#" + arg.getAttribute("id")).val();
            filter = input.toUpperCase();
            console.log(filter);
            table = $("#tblData");
            tr = table.find("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[no];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
</asp:Content>
