﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ProjectEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ProjectKey.Value = Request["ID"];

                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY RANK", false);
                Tools.DropDown_DDL(DDL_Province, "SELECT ProvinceKey, Type + ' ' + Name FROM SYS_Province ORDER BY TYPE", false);
                Tools.DropDown_DDL(DDL_AssetCategory, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                Tools.DropDown_DDL(DDL_Folder, "SELECT AutoKey, RIGHT(Product,LEN(Product)-2) AS FolderName FROM SYS_Categories WHERE TYPE = 20 AND AutoKey NOT IN (130, 309, 310, 311) ORDER BY Product ", false);
                Tools.DropDown_DDL(DDL_Folder2, "SELECT AutoKey, RIGHT(Product,LEN(Product)-2) AS FolderName FROM SYS_Categories WHERE TYPE = 20 AND AutoKey IN (309, 310, 311) ORDER BY Product ", false);

                LoadData();
                LoadAllow();
                LoadCategory();
                LoadAsset();
                LoadFolder();
                LoadPicture();
                LoadFee();
            }
        }
        void LoadData()
        {
            Lit_TitlePage.Text = "Khởi tạo thông tin dự án";
            if (HID_ProjectKey.Value != string.Empty &&
                HID_ProjectKey.Value != "0")
            {
                Project_Info zInfo = new Project_Info(HID_ProjectKey.Value.ToInt());
                string LatLng = zInfo.LatLng == string.Empty ? "10.8231, 106.6297" : zInfo.LatLng;
                txt_AddressFull.Value = zInfo.Address;
                txt_Address.Value = zInfo.Street;
                txt_BasicPrice.Value = zInfo.BasicPrice.ToString("n0");
                txt_Investor.Value = zInfo.Investor;
                txt_ProjectName.Value = zInfo.ProjectName;
                txt_Content.Value = zInfo.Contents;

                Tools.DropDown_DDL(DDL_District, "SELECT DistrictKey, Type + ' ' + Name FROM dbo.SYS_District WHERE ProvinceKey = " + zInfo.ProvinceKey + " ORDER BY RANK, TYPE", true);
                DDL_District.SelectedValue = zInfo.DistrictKey.ToString();
                DDL_Province.SelectedValue = zInfo.ProvinceKey.ToString();

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();

                Lit_TitlePage.Text = "Cập nhật dự án " + zInfo.ProjectName;
                Lit_Script.Text = "<script> var myLatLng= new google.maps.LatLng(" + LatLng + "), myCenter = new google.maps.LatLng(" + LatLng + ");</script>";
            }
        }
        void LoadAllow()
        {
            DataTable zTable = Share_Permition_Data.List_ShareDepartment(HID_ProjectKey.Value.ToInt(), "Project");
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblShare'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>Chia dự án cho </div><div class='profile-info-value'>" + zTable.Rows[i]["EmployeeName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            Lit_AllowSearch.Text = zSb.ToString();
        }
        void LoadAsset()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Project_Assets_Data.List(HID_ProjectKey.Value.ToInt());
            zSb.AppendLine("    <table class='table  table-bordered table-hover' id='tblAsset'>");
            zSb.AppendLine("         <thead>");
            zSb.AppendLine("               <tr>");
            zSb.AppendLine("                   <th>STT</th>");
            zSb.AppendLine("                   <th>Loại sản phẩm</th>");
            zSb.AppendLine("                   <th>Số phòng ngủ</th>");
            zSb.AppendLine("                   <th>Diện tích (m<sup>2</sup>)</th>");
            zSb.AppendLine("                   <th>Giá bán (VNĐ)</th>");
            zSb.AppendLine("                   <th>Giá thuê (VNĐ)</th>");
            zSb.AppendLine("                   <th>Diễn giải</th>");
            zSb.AppendLine("                   <th>...</th>");
            zSb.AppendLine("               </tr>");
            zSb.AppendLine("          </thead>");
            zSb.AppendLine("          <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int no = 1;
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    zSb.AppendLine("<tr class='' id=" + zTable.Rows[i]["AssetKey"].ToString() + " fid=" + zTable.Rows[i]["CategoryKey"].ToString() + ">");
                    zSb.AppendLine("    <td>" + no++ + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["Bed"].ToString() + "</td>");
                    zSb.AppendLine("    <td>" + float.Parse(zTable.Rows[i]["Area"].ToString()) + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["Money2"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["Name"].ToString() + " " + zTable.Rows[i]["Description"].ToString() + "</td>");
                    zSb.AppendLine("    <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteAsset' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                    zSb.AppendLine("</tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("                   <td></td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }
            zSb.AppendLine("          </tbody>");
            zSb.AppendLine("      </table>");

            Lit_Product.Text = zSb.ToString();
        }
        void LoadCategory()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Project_Category_Data.List(HID_ProjectKey.Value.ToInt());

            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblCategory'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row' id='" + zTable.Rows[i]["CategoryKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>Loại căn </div><div class='profile-info-value'>" + zTable.Rows[i]["CategoryName"].ToString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDeleteCategory' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");

            Lit_Category.Text = zSb.ToString();
        }
        void LoadPicture()
        {
            int no = 1;
            DataTable zTable = Categories_Data.ListFolder("309,310,311");
            StringBuilder zMaster = new StringBuilder();
            foreach (DataRow r in zTable.Rows)
            {
                int FolderKey = r["AutoKey"].ToInt();
                string FolderName = r["Product"].ToString();
                List<ItemDocument> zList = Document_Data.List(FolderKey, HID_ProjectKey.Value.ToInt());

                zMaster.AppendLine("<div class='panel panel-default'>");
                zMaster.AppendLine("    <div class='panel-heading'>");
                zMaster.AppendLine("        <h4 class='panel-title'>");
                zMaster.AppendLine("            <a class='accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion2' href='#collapse2" + no + "' aria-expanded='false'>");
                zMaster.AppendLine("                <i class='bigger-110 ace-icon fa fa-angle-right' data-icon-hide='ace-icon fa fa-angle-down' data-icon-show='ace-icon fa fa-angle-right'></i>");
                zMaster.AppendLine("                    &nbsp;#" + no + " " + FolderName.Split('.')[1] + "<span class='pull-right'> Có " + zList.Count() + " tập tin</span>");
                zMaster.AppendLine("            </a>");
                zMaster.AppendLine("        </h4>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("    <div class='panel-collapse collapse' id='collapse2" + no + "' aria-expanded='false' style='height: 0px;'>");
                zMaster.AppendLine("        <div class='panel-body'>");

                #region [Child]
                StringBuilder zChild = new StringBuilder();
                zChild.AppendLine("<ul class='ace-thumbnails clearfix'>");

                foreach (ItemDocument item in zList)
                {
                    zChild.AppendLine("<li id='" + item.FileKey + "'>");
                    zChild.AppendLine("    <a href='" + item.ImageUrl + "' data-rel='colorbox' class='cboxElement'>");
                    zChild.AppendLine("        <img width='150' height='150' alt='150x150' src='" + item.ImageUrl + "' />");
                    zChild.AppendLine("            <div class='text'>");
                    zChild.AppendLine("                <div class='inner'>" + item.ImageName + "</div>");
                    zChild.AppendLine("            </div>");
                    zChild.AppendLine("    </a>");
                    zChild.AppendLine("    <div class='tools tools-bottom'><a href='#' btn='btnDeleteImg'><i class='ace-icon fa fa-times red'></i>Xóa</a></div>");
                    zChild.AppendLine("</li>");
                }
                zChild.AppendLine(" </ul>");
                zMaster.AppendLine(zChild.ToString());
                #endregion

                zMaster.AppendLine("         </div>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("</div>");
                no++;
            }

            Lit_ListPicture.Text = zMaster.ToString();
        }
        void LoadFolder()
        {
            DataTable zTable = Categories_Data.ListFolder("129,131,132,133,134,135");
            StringBuilder zSb = new StringBuilder();
            int no = 1;
            foreach (DataRow r in zTable.Rows)
            {
                int FolderKey = r["AutoKey"].ToInt();
                string FolderName = r["Product"].ToString();
                List<ItemDocument> zList = Document_Data.List(FolderKey, HID_ProjectKey.Value.ToInt());
                StringBuilder zMaster = new StringBuilder();

                zMaster.AppendLine("<div class='panel panel-default'>");
                zMaster.AppendLine("    <div class='panel-heading'>");
                zMaster.AppendLine("        <h4 class='panel-title'>");
                zMaster.AppendLine("            <a class='accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' href='#collapseOne" + no + "' aria-expanded='false'>");
                zMaster.AppendLine("                <i class='bigger-110 ace-icon fa fa-angle-right' data-icon-hide='ace-icon fa fa-angle-down' data-icon-show='ace-icon fa fa-angle-right'></i>");
                zMaster.AppendLine("                    &nbsp;#" + no + " " + FolderName.Split('.')[1] + "<span class='pull-right'> Có " + zList.Count() + " tập tin</span>");
                zMaster.AppendLine("            </a>");
                zMaster.AppendLine("        </h4>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("    <div class='panel-collapse collapse' id='collapseOne" + no + "' aria-expanded='false' style='height: 0px;'>");
                zMaster.AppendLine("        <div class='panel-body'>");
                #region [Child]
                StringBuilder zChild = new StringBuilder();
                zChild.AppendLine("<table class='table table-hover table-bordered' id='tblFile'>");
                zChild.AppendLine("  <thead>");
                zChild.AppendLine("     <tr>");
                zChild.AppendLine("         <th>STT</th>");
                zChild.AppendLine("         <th>Tập tin</th>");
                zChild.AppendLine("         <th>Diển giải</th>");
                zChild.AppendLine("         <th>Ngày cập nhật</th>");
                zChild.AppendLine("         <th>Người cập nhật</th>");
                zChild.AppendLine("         <th>...</th>");
                zChild.AppendLine("     </tr>");
                zChild.AppendLine(" </thead>");
                zChild.AppendLine(" <tbody>");
                int i = 1;
                foreach (ItemDocument item in zList)
                {
                    zChild.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td>" + item.Description + "</td><td>" + item.ModifiedDate + "</td><td>" + item.ModifiedName + "</td><td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteFile' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td></tr>");
                }
                zChild.AppendLine(" </tbody>");
                zChild.AppendLine(" </table>");
                zMaster.AppendLine(zChild.ToString());
                #endregion
                zMaster.AppendLine("         </div>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("</div>");

                zSb.AppendLine(zMaster.ToString());
                no++;
            }

            Lit_Folder.Text = zSb.ToString();
        }
        void LoadFee()
        {
            DataTable zTable = Project_Fee_Data.List(HID_ProjectKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblFee'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>" + zTable.Rows[i]["Name"].ToString() + "</div>");
                zSb.AppendLine("     <div class='profile-info-value'>" + zTable.Rows[i]["Description"].ToString() + "</div>");
                zSb.AppendLine("     <div class='profile-info-value giatien'>" + zTable.Rows[i]["Money"].ToDoubleString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDelFee' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> | Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            Lit_Fee.Text = zSb.ToString();
        }
        void UploadFile()
        {
            int zCategoryKey = DDL_Folder.SelectedValue.ToInt();
            int zProductKey = HID_ProjectKey.Value.ToInt();
            string zSQL = "";

            string zPath = "~/Upload/File/Project/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileName(zInputFile.FileName);
                    string zFilePath = Server.MapPath(Path.Combine(zPath, zFileName));
                    string zFileUrl = (zPath + zFileName).Substring(1, zPath.Length + zFileName.Length - 1);

                    if (File.Exists(zFilePath))
                        File.Delete(zFilePath);
                    zInputFile.SaveAs(zFilePath);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, Description ,ImagePath, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('PUL_Project','" + zProductKey + "','" + zCategoryKey + "',N'" + txt_Description.Text + "',N'" + zFilePath + "',N'" + zFileUrl + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadFolder();
                }
            }
        }
        void UploadImg()
        {
            int zCategoryKey = DDL_Folder2.SelectedValue.ToInt();
            int zProductKey = HID_ProjectKey.Value.ToInt();
            string zSQL = "";

            string zPath = "~/Upload/File/Project/" + zProductKey + "/";
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(Server.MapPath(zPath));
            if (!nDir.Exists)
                nDir.Create();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile zInputFile = Request.Files[i];
                if (zInputFile.ContentLength > 0)
                {
                    string zFileName = Path.GetFileNameWithoutExtension(zInputFile.FileName);
                    string zFileThumb = zFileName + "_thumb." + Path.GetExtension(zInputFile.FileName);
                    string zFileLarge = Path.GetFileName(zInputFile.FileName);

                    string zFilePathThumb = Server.MapPath(Path.Combine(zPath, zFileThumb));
                    string zFilePathLarge = Server.MapPath(Path.Combine(zPath, zFileLarge));

                    string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
                    string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

                    if (File.Exists(zFilePathLarge))
                        File.Delete(zFilePathLarge);
                    zInputFile.SaveAs(zFilePathLarge);

                    Bitmap bitmap = new Bitmap(zInputFile.InputStream);
                    System.Drawing.Image objImage = Tools.resizeImage(bitmap, new Size(150, 150));
                    objImage.Save(zFilePathThumb, ImageFormat.Jpeg);

                    zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, Description, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('PUL_Project','" + zProductKey + "','" + zCategoryKey + "', N'" + txt_Description2.Text + "', N'" + zFileLarge + "', N'" + zFileUrlThumb + "', N'" + zFileUrlLarge + "', N'" + zFileName + "',GETDATE(), GETDATE(),'"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                      + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                      + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
                }
            }

            if (zSQL != string.Empty)
            {
                string Message = Document_Info.AutoInsert(zSQL);
                if (Message != string.Empty)
                {
                    Response.Write("<script>alert('Lỗi !'" + Message + ");</script>");
                }
                else
                {
                    LoadPicture();
                }
            }
        }

        #region [Sản phẩm cơ bản]
        [WebMethod]
        public static ItemAsset OpenAsset(int Key)
        {
            Project_Assets_Info zInfo = new Project_Assets_Info(Key);
            ItemAsset zAsset = new ItemAsset();
            zAsset.AssetKey = zInfo.AssetKey.ToString();
            zAsset.Room = zInfo.Bed.ToString();
            zAsset.CategoryKey = zInfo.CategoryKey.ToString();
            zAsset.Area = zInfo.Area.ToString();
            zAsset.Money = zInfo.Money.ToDoubleString();
            zAsset.Money2 = zInfo.Money2.ToDoubleString();
            zAsset.Description = zInfo.Description;
            return zAsset;
        }
        [WebMethod]
        public static ItemReturn SaveAsset(int Key, int AssetKey, int CategoryKey, int Bedroom, string Amount, string Amount2, string Area, string Description)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Assets_Info zInfo = new Project_Assets_Info(AssetKey);
            zInfo.ProjectKey = Key;
            zInfo.CategoryKey = CategoryKey;
            zInfo.Bed = Bedroom;
            if (Amount != string.Empty)
                zInfo.Money = double.Parse(Amount.Replace(",", ""));
            if (Amount2 != string.Empty)
                zInfo.Money2 = double.Parse(Amount2.Replace(",", ""));
            if (Area != string.Empty)
                zInfo.Area = float.Parse(Area);
            zInfo.Description = Description;
            zInfo.Save();

            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.AssetID.ToString();

            return zResult;
        }
        [WebMethod]
        public static ItemReturn DeleteAsset(int Key, int AssetKey)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Assets_Info zInfo = new Project_Assets_Info();
            zInfo.AssetKey = AssetKey;
            zInfo.ProjectKey = Key;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string GetAsset(int Key)
        {
            StringBuilder zSb = new StringBuilder();
            int no = 0;
            DataTable zTable = Project_Assets_Data.List(Key);
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<tr class='' id=" + zTable.Rows[i]["AssetKey"].ToString() + " fid=" + zTable.Rows[i]["CategoryKey"].ToString() + ">");
                zSb.AppendLine("    <td>" + no++ + "</td>");
                zSb.AppendLine("    <td>" + zTable.Rows[i]["CategoryName"].ToString() + "</td>");
                zSb.AppendLine("    <td>" + zTable.Rows[i]["Bed"].ToString() + "</td>");
                zSb.AppendLine("    <td>" + float.Parse(zTable.Rows[i]["Area"].ToString()) + "</td>");
                zSb.AppendLine("    <td>" + zTable.Rows[i]["Money"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td>" + zTable.Rows[i]["Money2"].ToDoubleString() + "</td>");
                zSb.AppendLine("    <td>" + zTable.Rows[i]["Name"].ToString() + " " + zTable.Rows[i]["Description"].ToString() + "</td>");
                zSb.AppendLine("    <td><div class='hidden-sm hidden-xs action-buttons pull-right'><a btn='btnDeleteAsset' href='#' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div></td>");
                zSb.AppendLine("</tr>");
            }

            return zSb.ToString();
        }
        #endregion

        #region [Loại sản phẩm cơ bản]
        [WebMethod]
        public static ItemReturn DeleteCategory(int Key, int CategoryKey)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Category_Info zInfo = new Project_Category_Info();
            zInfo.CategoryKey = CategoryKey;
            zInfo.ProjectKey = Key;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn SaveCategory(int Key, int CategoryKey)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Category_Info zInfo = new Project_Category_Info(Key, CategoryKey);
            if (zInfo.ID > 0)
            {
                zResult.Message = "Trùng loại sản phẩm. Vui lòng nhập lại !";
                return zResult;
            }
            zInfo.ProjectKey = Key;
            zInfo.CategoryKey = CategoryKey;
            zInfo.Create();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.CategoryKey.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemAsset[] GetCategory(int Key)
        {
            DataTable zTable = Project_Category_Data.List(Key);
            List<ItemAsset> zList = zTable.DataTableToList<ItemAsset>();
            return zList.ToArray();
        }
        #endregion

        [WebMethod]
        public static ItemReturn SaveFee(int Key, string Name, string Money, string Description)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Fee_Info zInfo = new Project_Fee_Info();
            zInfo.ProjectKey = Key;
            zInfo.Name = Name;
            double Fee = 0;
            double.TryParse(Money, out Fee);
            zInfo.Money = Fee;
            zInfo.Description = Description;
            zInfo.Create();
            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static string GetFee(int Key)
        {
            DataTable zTable = Project_Fee_Data.List(Key);
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped' id='tblFee'>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<div class='profile-info-row'  id='" + zTable.Rows[i]["AutoKey"].ToString() + "'>");
                zSb.AppendLine("    <div class='profile-info-name'>" + zTable.Rows[i]["Name"].ToString() + "</div>");
                zSb.AppendLine("     <div class='profile-info-value'>" + zTable.Rows[i]["Description"].ToString() + "</div>");
                zSb.AppendLine("     <div class='profile-info-value giatien'>" + zTable.Rows[i]["Money"].ToDoubleString() + "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDelFee' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> | Xóa</a></div></div>");
                zSb.AppendLine("</div>");
            }
            zSb.AppendLine("</div>");
            return zSb.ToString();
        }
        [WebMethod]
        public static ItemReturn DeleteFee(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Fee_Info zInfo = new Project_Fee_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn DeleteFile(int FileKey)
        {
            ItemReturn zResult = new ItemReturn();
            Document_Info zInfo = new Document_Info();
            zInfo.FileKey = FileKey;
            zInfo.Delete();
            zResult.Message = zInfo.Message;

            if (zInfo.Message == string.Empty)
            {
                if (File.Exists(zInfo.ImagePath))
                    File.Delete(zInfo.ImagePath);
            }
            return zResult;
        }
        [WebMethod]
        public static ItemReturn SaveShare(int Key, int DepartmentKey)
        {
            ItemReturn zResult = new ItemReturn();
            Share_Permition_Info zInfo = new Share_Permition_Info(Key, DepartmentKey, "Project");
            if (zInfo.AutoKey > 0)
            {
                zResult.Message = "Trùng thông tin, vui lòng chọn lại";
                return zResult;
            }

            zInfo.ObjectTable = "Project";
            zInfo.EmployeeKey = DepartmentKey;
            zInfo.AssetKey = Key;
            zInfo.Create();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemWeb[] GetShare(int Key)
        {
            DataTable zTable = Share_Permition_Data.List_ShareDepartment(Key, "Project");
            List<ItemWeb> ListAsset = new List<ItemWeb>();
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                ItemWeb iwe = new ItemWeb();
                iwe.Value = zTable.Rows[i]["AutoKey"].ToString();
                iwe.Text = zTable.Rows[i]["EmployeeName"].ToString();

                ListAsset.Add(iwe);
            }
            return ListAsset.ToArray();
        }
        [WebMethod]
        public static ItemReturn DeleteShare(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Share_Permition_Info zInfo = new Share_Permition_Info();
            zInfo.AutoKey = AutoKey;
            zInfo.Delete();

            zResult.Message = zInfo.Message;
            return zResult;
        }
        [WebMethod]
        public static ItemReturn DeleteProject(int Key)
        {
            Project_Info zInfo = new Project_Info();
            ItemReturn zResult = new ItemReturn();
            zInfo.Key = Key;
            zInfo.Delete();
            zInfo.DeleteRelate();
            return zResult;
        }
        [WebMethod]
        public static ItemReturn InitProject(
            int Key, string ProjectName, string Investor, string BasicPrice,
            string Street, int Province, int District, int IsPrimary, string MainTable, string AddressFull, string Content)
        {
            ItemReturn zResult = new ItemReturn();
            if (MainTable == string.Empty || MainTable == "0")
            {
                zResult.Message = "Liên hệ admin để xử lỗi này !. mã [AssetType]";
                return zResult;
            }

            Project_Info zInfo = new Project_Info(Key);

            zInfo.ProjectName = ProjectName.Trim();
            zInfo.Investor = Investor.Trim();
            zInfo.BasicPrice = double.Parse(BasicPrice.Replace(",", ""));
            zInfo.IsPrimary = IsPrimary;
            zInfo.MainTable = MainTable.Trim();
            zInfo.ShortTable = Helper.ShortTable(MainTable.Trim());
            zInfo.Address = AddressFull.Trim();
            zInfo.Street = Street.Trim();
            zInfo.DistrictKey = District;
            zInfo.ProvinceKey = Province;
            zInfo.Contents = Content.Trim();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemReturn SaveProject(
         int Key, string ProjectName, string Investor, string BasicPrice,
         string Street, int Province, int District, string AddressFull, string Content)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Info zInfo = new Project_Info(Key);

            zInfo.ProjectName = ProjectName.Trim();
            zInfo.Investor = Investor.Trim();
            zInfo.BasicPrice = double.Parse(BasicPrice.Replace(",", ""));
            zInfo.Address = AddressFull.Trim();
            zInfo.Street = Street.Trim();
            zInfo.DistrictKey = District;
            zInfo.ProvinceKey = Province;
            zInfo.Contents = Content.Trim();
            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.Save();
            zResult.Message = zInfo.Message;
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }
        [WebMethod]
        public static ItemReturn UpdateLatLng(int Key, string LatLng)
        {
            ItemReturn zResult = new ItemReturn();
            Project_Info zInfo = new Project_Info(Key);
            zInfo.Lat = LatLng.Split(',')[0];
            zInfo.Lng = LatLng.Split(',')[1];
            zInfo.LatLng = LatLng;
            zInfo.Update();
            return zResult;
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            UploadFile();
        }
        protected void btnUploadImg_Click(object sender, EventArgs e)
        {
            UploadImg();
        }
    }
}