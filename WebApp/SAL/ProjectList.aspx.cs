﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ProjectList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
                Tools.DropDown_DDL(DDL_Table, "SELECT TableName, CategoryName FROM SYS_Table WHERE TYPE = 2 ORDER BY Rank", false);
                Tools.DropDown_DDL(DDL_Province, "SELECT ProvinceKey, Type + ' ' + Name FROM SYS_Province ORDER BY TYPE", false);
                Tools.DropDown_DDL(DDL_District, "SELECT DistrictKey, Type + ' ' + Name FROM SYS_District WHERE ProvinceKey = 79 ORDER BY TYPE", false);
                Tools.DropDown_DDL(DDL_SearchProvince, "SELECT ProvinceKey, Type + ' ' + Name FROM SYS_Province ORDER BY TYPE", false);
                Tools.DropDown_DDL(DDL_SearchDistrict, "SELECT DistrictKey, Type + ' ' + Name FROM SYS_District WHERE ProvinceKey = 79 ORDER BY TYPE", false);
                Tools.DropDown_DDL(DDL_Category, "SELECT CategoryKey, CategoryName FROM PUL_Category ORDER BY CategoryName", false);
                Tools.DropDown_DDL(DDL_Price, "SELECT Value, Product FROM SYS_Categories WHERE Type = 17 ORDER BY [RANK]", false);
                Tools.DropDown_DDL(DDL_Price2, "SELECT Value, Product FROM SYS_Categories WHERE Type = 18 ORDER BY [RANK]", false);
                DDL_Province.SelectedValue = "79";
                DDL_SearchProvince.SelectedValue = "79";                
                LoadData();
            }
        }
        void LoadData()
        {
            DataTable zTable = Project_Data.List();
            ViewHtml(zTable);
        }
        void ViewHtml(DataTable Table)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table  table-bordered table-hover' id='tblData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Tên dự án</th>");
            zSb.AppendLine("        <th>Địa chỉ</th>");
            zSb.AppendLine("        <th>Chủ đầu tư</th>");
            zSb.AppendLine("        <th>Giá trung bình (VNĐ)</th>");
            zSb.AppendLine("        <th>Xem chi tiết</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");
            if (Table.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in Table.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["ProjectKey"].ToString() + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Investor"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["BasicPrice"].ToDoubleString() + "</td>");
                    zSb.AppendLine("                <td><div class='action-buttons'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>");
                    zSb.AppendLine("            </tr>");

                    zSb.AppendLine("<tr class='detail-row'>");
                    zSb.AppendLine("    <td colspan=6>");
                    zSb.AppendLine("        <div class='table-detail' project='" + r["ProjectKey"].ToString() + "'></div>");
                    zSb.AppendLine("    </td>");
                    zSb.AppendLine("</tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }
            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Literal_Table.Text = zSb.ToString();
        }
        [WebMethod]
        public static string GetDetail(int ProjectKey, int Category, string Room, string Price)
        {
            double FromPrice = 0;
            double ToPrice = 0;
            if (Price != string.Empty)
            {
                if (Price.Contains("-"))
                {
                    FromPrice = float.Parse(Price.Split('-')[0]);
                    ToPrice = float.Parse(Price.Split('-')[1]);
                }

                if (Price.Contains("<"))
                {
                    FromPrice = float.Parse(Price.Replace("<", ""));
                }

                if (Price.Contains(">"))
                {
                    ToPrice = float.Parse(Price.Replace(">", ""));
                }
            }

            DataTable zList = Project_Assets_Data.List(ProjectKey, Category, Room.ToInt(), FromPrice, ToPrice);

            #region view detail
            string html = "";
            html += "<table class='table table-bordered'>";
            html += "<thead><tr><th class=td1>Loại</th><th class=td2>Số phòng</th><th class=td3>Giá bán (VNĐ)</th><th class=td4>Giá thuê (VNĐ)</th><th>Diễn giải</th></tr></thead>";
            html += "<tbody>";
            for (var j = 0; j < zList.Rows.Count; j++)
            {            
                html += "<tr><td>" + zList.Rows[j]["CategoryName"].ToString() + "</td><td>" + zList.Rows[j]["Room"].ToString() + "</td><td class='giatien'>" + zList.Rows[j]["Money"].ToDoubleString() + "</td><td class='giatien'>" + zList.Rows[j]["Money2"].ToDoubleString() + "</td><td>" + zList.Rows[j]["Description"].ToString() + "</td></tr>";
            }
            html += "</tbody>";
            html += "</table>";
            #endregion
            return html;
        }
        [WebMethod]
        public static string SearchDetail(int Province, int District, int Category, int Room, string Price, int LoaiGia)
        {
            DataTable zTable = new DataTable();
            StringBuilder zSb = new StringBuilder();
            if (Category != 0 || Room != 0 || Price != string.Empty)
            {
                #region [tim phuc tap]
                double FromPrice = 0;
                double ToPrice = 0;
                if (Price != string.Empty)
                {
                    if (Price.Contains("-"))
                    {
                        FromPrice = float.Parse(Price.Split('-')[0]);
                        ToPrice = float.Parse(Price.Split('-')[1]);
                    }

                    if (Price.Contains("<"))
                    {
                        FromPrice = float.Parse(Price.Replace("<", ""));
                    }

                    if (Price.Contains(">"))
                    {
                        ToPrice = float.Parse(Price.Replace(">", ""));
                    }
                }

                DataTable zList = Project_Assets_Data.List(Category, Room, FromPrice, ToPrice, LoaiGia);
                string Project = "";
                foreach (DataRow r in zList.Rows)
                {
                    Project += r["ProjectKey"].ToString() + ",";
                }
                zTable = Project_Data.List(Project.Remove(Project.LastIndexOf(","), 1), Province, District);
                #endregion
            }
            else
            {
                #region [tìm don gian]
                zTable = Project_Data.List(Province, District);
                #endregion
            }

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='" + r["ProjectKey"].ToString() + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r["ProjectName"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Address"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["Investor"].ToString() + "</td>");
                    zSb.AppendLine("                <td>" + r["BasicPrice"].ToDoubleString() + "</td>");
                    zSb.AppendLine("                <td><div class='action-buttons'><a href='#' class='green bigger-140 show-details-btn' title='Show Details'><i class='ace-icon fa fa-angle-double-up'></i><span class='sr-only'>Details</span></a></div></td>");
                    zSb.AppendLine("            </tr>");

                    zSb.AppendLine("<tr class='detail-row'>");
                    zSb.AppendLine("    <td colspan=6>");
                    zSb.AppendLine("        <div class='table-detail'><div class='row' project='" + r["ProjectKey"].ToString() + "'></div></div>");
                    zSb.AppendLine("    </td>");
                    zSb.AppendLine("</tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }

        #region [MyRegion]
        static string[] _Permitsion; //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        void CheckRole()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL01";
            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[1].ToInt() == 1)
                LitButton.Text = @"
                <span class='tools pull-right'>
                    <a href='#mCreateNew' data-toggle='modal' class='btn btn-white btn-info btn-bold'>
                        <i class='ace-icon fa fa-plus'></i>
                        Tạo mới
                    </a>
                </span>";
            else
                LitButton.Text = "";
        }
        #endregion
    }
}