﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ProjectView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ProjectKey.Value = Request["ID"];

                CheckRoles();
                LoadData();
                LoadAsset();
                LoadResale();
                LoadPicture();
                LoadFolder();
                LoadFee();
            }
        }

        void LoadData()
        {
            int Key = HID_ProjectKey.Value.ToInt();
            if (Key != 0)
            {
                Project_Info zInfo = new Project_Info(Key);
                txt_ProjectName.Text = zInfo.ProjectName + "<small>" + zInfo.Address + "</small>";
                txt_Content.Text = zInfo.Contents;

                //txt_ProjectName2.Text = zInfo.ProjectName;
                //txt_Investor.Text = zInfo.Investor;
                //txt_BasicPrice.Text = zInfo.BasicPrice.ToString("n0");
                //txt_Address.Text = zInfo.Address;
                //

                StringBuilder zSb = new StringBuilder();
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
                zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy HH:mm") + "</td></tr>");

                DataTable zTable = Share_Permition_Data.List_ShareDepartment(HID_ProjectKey.Value.ToInt(), "Project");
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Đã phân cho:</td><td>" + zTable.Rows[i]["EmployeeName"].ToString() + "</td></tr>");
                }
                zSb.AppendLine("</table>");
                Lit_Info.Text = zSb.ToString();
                string LatLng = zInfo.LatLng == string.Empty ? "10.8231, 106.6297" : zInfo.LatLng;
                Lit_Script.Text = "<script> var myLatLng= new google.maps.LatLng(" + LatLng + "), myCenter = new google.maps.LatLng(" + LatLng + ");</script>";
            }
        }
        void LoadAsset()
        {
            StringBuilder zSb = new StringBuilder();
            DataTable zTable = Project_Assets_Data.List(HID_ProjectKey.Value.ToInt());
            zSb.AppendLine("    <table class='table  table-bordered table-hover' id='tblAsset'>");
            zSb.AppendLine("         <thead>");
            zSb.AppendLine("               <tr>");
            zSb.AppendLine("                   <th>STT</th>");
            zSb.AppendLine("                   <th>Loại sản phẩm</th>");
            zSb.AppendLine("                   <th>Số phòng ngủ</th>");
            zSb.AppendLine("                   <th>Diện tích (m<sup>2</sup>)</th>");
            zSb.AppendLine("                   <th>Giá bán (VNĐ)</th>");
            zSb.AppendLine("                   <th>Giá thuê (VNĐ)</th>");
            zSb.AppendLine("                   <th>Diễn giải</th>");
            zSb.AppendLine("               </tr>");
            zSb.AppendLine("          </thead>");
            zSb.AppendLine("          <tbody>");
            if (zTable.Rows.Count > 0)
            {
                int no = 1;
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    zSb.AppendLine("<tr class='' id=" + zTable.Rows[i]["AssetKey"].ToString() + " fid=" + zTable.Rows[i]["CategoryKey"].ToString() + ">");
                    zSb.AppendLine("    <td>" + no++ + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["Bed"].ToString() + "</td>");
                    zSb.AppendLine("    <td>" + float.Parse(zTable.Rows[i]["Area"].ToString()) + "</td>");
                    zSb.AppendLine("    <td class='giatien'>" + zTable.Rows[i]["Money"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td class='giatien'>" + zTable.Rows[i]["Money2"].ToDoubleString() + "</td>");
                    zSb.AppendLine("    <td>" + zTable.Rows[i]["Name"].ToString() + " " + zTable.Rows[i]["Description"].ToString() + "</td>");
                    zSb.AppendLine("</tr>");
                }
            }
            else
            {
                zSb.AppendLine("         <tr id='-1'>");
                zSb.AppendLine("                   <td></td><td colspan='5'>Chưa có dữ liệu</td>");
                zSb.AppendLine("         </tr>");
            }
            zSb.AppendLine("          </tbody>");
            zSb.AppendLine("      </table>");

            Lit_Product.Text = zSb.ToString();
        }
        void LoadResale()
        {
            string DDL_Category = Tools.Html_Select("DDL_Category", "Loại SP", "form-control input-sm", false, "SELECT A.CategoryKey, C.CategoryName FROM PUL_Project_Category A LEFT JOIN PUL_Category C ON C.CategoryKey = A.CategoryKey WHERE A.ProjectKey=" + HID_ProjectKey.Value, false);
            string DDL_Purpose = @"
<select name='DDL_Purpose' id='DDL_Purpose' class='form-control input-sm'>
	<option selected='selected' value='0' disabled='disabled'>Nhu cầu</option>
	<option value='CN'>Chuyển nhượng</option>
	<option value='CT'>Cho thuê</option>
	<option value='KT'>Kinh doanh</option>
</select>";
            string txt_Name = "<input type='text' id='txt_Name' class='form-control input-sm' placeholder='Mã căn' />";
            string txt_Bed = @"
<select name='DDL_Room' id='DDL_Room' class='form-control input-sm'>
	<option selected='selected' value='0' disabled='disabled'>PN</option>
	<option value='1'>1 PN</option>
	<option value='2'>2 PN</option>
	<option value='3'>3 PN</option>
    <option value='3'>4 PN</option>
    <option value='3'>5 PN</option>
</select>";
            string btn_Search = "<a href='#' class='btn-block btn btn-sm btn-purple' id='btnSearch'><i class='ace-icon fa fa-search'></i>Lọc</a>";

            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            List<ItemAsset> zList = new List<ItemAsset>();

            zList = Product_Data.Search(HID_ProjectKey.Value.ToInt(), 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0);

            zSb.AppendLine("<table class='table table-hover table-bordered' id='tblExtraData'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th colspan=2>" + DDL_Category + "</th>");
            zSb.AppendLine("        <th>" + txt_Name + "</th>");
            zSb.AppendLine("        <th>" + txt_Bed + "</th>");
            zSb.AppendLine("        <th>Diện tích m<sup>2</sup></th>");
            zSb.AppendLine("        <th>" + DDL_Purpose + "</th>");
            zSb.AppendLine("        <th>" + btn_Search + "</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zList.Count > 0)
            {
                int i = 1;
                foreach (ItemAsset r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td>" + r.Area + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            zList = Project_Data.List_Asset(HID_ProjectKey.Value.ToInt());
            txt_AmountProduct.Text = zList.Count.ToString();
            Lit_Resale.Text = zSb.ToString();


        }
        void LoadPicture()
        {
            int no = 1;
            DataTable zTable = Categories_Data.ListFolder("309,310,311");
            StringBuilder zMaster = new StringBuilder();
            foreach (DataRow r in zTable.Rows)
            {
                int FolderKey = r["AutoKey"].ToInt();
                string FolderName = r["Product"].ToString();
                List<ItemDocument> zList = Document_Data.List(FolderKey, HID_ProjectKey.Value.ToInt());

                zMaster.AppendLine("<div class='panel panel-default'>");
                zMaster.AppendLine("    <div class='panel-heading'>");
                zMaster.AppendLine("        <h4 class='panel-title'>");
                zMaster.AppendLine("            <a class='accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion2' href='#collapse2" + no + "' aria-expanded='false'>");
                zMaster.AppendLine("                <i class='bigger-110 ace-icon fa fa-angle-right' data-icon-hide='ace-icon fa fa-angle-down' data-icon-show='ace-icon fa fa-angle-right'></i>");
                zMaster.AppendLine("                    &nbsp;#" + no + " " + FolderName.Split('.')[1] + "<span class='pull-right'> Có " + zList.Count() + " tập tin</span>");
                zMaster.AppendLine("            </a>");
                zMaster.AppendLine("        </h4>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("    <div class='panel-collapse collapse' id='collapse2" + no + "' aria-expanded='false' style='height: 0px;'>");
                zMaster.AppendLine("        <div class='panel-body' id='hinhanh" + no + "'>");

                #region [Child]
                StringBuilder zChild = new StringBuilder();


                foreach (ItemDocument item in zList)
                {
                    zChild.AppendLine(@"<a class='' href='" + item.ImageUrl + "' title=''><img width=100 height=70 src='" + item.ImageThumb.ToThumb() + "'></a>");
                }
                zMaster.AppendLine(zChild.ToString());
                #endregion

                zMaster.AppendLine("         </div>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("</div>");
                no++;
            }
            Lit_ListPicture.Text = zMaster.ToString();
        }
        void LoadFolder()
        {
            DataTable zTable = Categories_Data.ListFolder("129,131,132,133,134,135");
            int no = 1;
            foreach (DataRow r in zTable.Rows)
            {
                int FolderKey = r["AutoKey"].ToInt();
                string FolderName = r["Product"].ToString();
                List<ItemDocument> zList = Document_Data.List(FolderKey, HID_ProjectKey.Value.ToInt());
                StringBuilder zMaster = new StringBuilder();

                zMaster.AppendLine("<div class='panel panel-default'>");
                zMaster.AppendLine("    <div class='panel-heading'>");
                zMaster.AppendLine("        <h4 class='panel-title'>");
                zMaster.AppendLine("            <a class='accordion-toggle collapsed' data-toggle='collapse' data-parent='#accordion' href='#collapseOne" + no + "' aria-expanded='false'>");
                zMaster.AppendLine("                <i class='bigger-110 ace-icon fa fa-angle-right' data-icon-hide='ace-icon fa fa-angle-down' data-icon-show='ace-icon fa fa-angle-right'></i>");
                zMaster.AppendLine("                    &nbsp;#" + no + " " + FolderName.Split('.')[1] + "<span class='pull-right'> Có " + zList.Count() + " tập tin</span>");
                zMaster.AppendLine("            </a>");
                zMaster.AppendLine("        </h4>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("    <div class='panel-collapse collapse' id='collapseOne" + no + "' aria-expanded='false' style='height: 0px;'>");
                zMaster.AppendLine("        <div class='panel-body'>");

                #region [Child]
                StringBuilder zChild = new StringBuilder();
                zChild.AppendLine("<table class='table table-hover table-bordered' id='tblFile'>");
                zChild.AppendLine("  <thead>");
                zChild.AppendLine("     <tr>");
                zChild.AppendLine("         <td>STT</td>");
                zChild.AppendLine("         <td>Tập tin</td>");
                zChild.AppendLine("         <td>Diễn giải</td>");
                zChild.AppendLine("         <td>Ngày cập nhật</td>");
                zChild.AppendLine("         <td>Người cập nhật</td>");
                zChild.AppendLine("     </tr>");
                zChild.AppendLine(" </thead>");
                zChild.AppendLine(" <tbody>");
                int i = 1;
                foreach (ItemDocument item in zList)
                {
                    zChild.AppendLine("<tr id=" + item.FileKey + "><td>" + (i++) + "</td><td><a class='iframe' href='" + item.ImageUrl.ToFullLink() + "'>" + item.ImageName + "</a></td><td>" + item.Description + "</td><td>" + item.ModifiedDate.ToDateString() + "</td><td>" + item.ModifiedName + "</td></tr>");
                }
                zChild.AppendLine(" </tbody>");
                zChild.AppendLine(" </table>");
                zMaster.AppendLine(zChild.ToString());
                #endregion

                zMaster.AppendLine("         </div>");
                zMaster.AppendLine("    </div>");
                zMaster.AppendLine("</div>");

                Lit_Folder.Text += zMaster.ToString();
                no++;
            }
        }
        void LoadFee()
        {
            DataTable zTable = Project_Fee_Data.List(HID_ProjectKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table table-hover table-bordered'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <tr><th>STT</th><th>Tên phí</th><th>Số tiền (VNĐ)</th><th>Diễn giải</th></tr>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("    <tbody>");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                zSb.AppendLine("<tr><td>" + (i + 1) + "</td><td><i class='ace-icon fa fa-usd'></i>" + zTable.Rows[i]["Name"].ToString() + "</td><td class='giatien'>" + zTable.Rows[i]["Money"].ToDoubleString() + " (VNĐ)</td><td>" + zTable.Rows[i]["Description"].ToString() + "</td></tr>");
            }
            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            Lit_Fee.Text = zSb.ToString();
        }
        static string GetPrice(ItemAsset Item)
        {
            string PurposeShow = "";
            if (Item.Purpose.Contains(","))
            {
                string[] temp = Item.Purpose.Split(',');
                foreach (string s in temp)
                {
                    if (s != string.Empty)
                    {
                        switch (s.Trim())
                        {
                            case "Chuyển nhượng":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                                break;

                            case "Cho thuê":
                                PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                                break;
                        }
                    }
                }
            }
            else
            {
                string s = Item.Purpose;
                switch (s.Trim())
                {
                    case "Chuyển nhượng":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Chuyển nhượng</div><div class='col-xs-6 giatien'>" + Item.Price_VND.ToDoubleString() + "</div></div>";
                        break;

                    case "Cho thuê":
                        PurposeShow += "<div class=row><div class='col-xs-6'>Cho thuê</div><div class='col-xs-6 giatien'>" + Item.PriceRent_VND.ToDoubleString() + "</div></div>";
                        break;
                }
            }

            return PurposeShow;
        }

        //search
        [WebMethod]
        public static string SearchAsset(int Project, string AssetID, string Room, string Category, string Purpose)
        {
            List<ItemAsset> zList = new List<ItemAsset>();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            if (UnitLevel < 2)
                Department = 0;

            StringBuilder zSb = new StringBuilder();
            zList = Product_Data.Search(Project, Category.ToInt(), string.Empty, string.Empty, string.Empty, Room, AssetID, string.Empty, Purpose, 0, 0);
            if (zList.Count > 0)
            {
                int i = 1;
                foreach (ItemAsset r in zList)
                {
                    zSb.AppendLine("            <tr id='" + r.AssetKey + "' fid='" + r.AssetType + "'>");
                    zSb.AppendLine("                <td>" + i++ + "</td>");
                    zSb.AppendLine("                <td>" + r.CategoryName + "</td>");
                    zSb.AppendLine("                <td>" + r.AssetID + "</td>");
                    zSb.AppendLine("                <td>" + r.Room + "</td>");
                    zSb.AppendLine("                <td>" + r.AreaWall + "</td>");
                    zSb.AppendLine("                <td>" + GetPrice(r) + "</td>");
                    zSb.AppendLine("                <td>" + r.Status + "</td>");
                    zSb.AppendLine("            </tr>");
                }
            }
            else
            {
                zSb.AppendLine("            <tr>");
                zSb.AppendLine("                <td>1</td><td colspan='6'>Chưa có dữ liệu</td>");
                zSb.AppendLine("            </tr>");
            }

            return zSb.ToString();
        }

        [WebMethod]
        public static ItemReturn GetAssetInfo(int AssetKey)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            Product_Info zAsset = new Product_Info(AssetKey, "ResaleApartment");

            #region [html view usd]
            string ViewMoney = "";
            ViewMoney += " <div class='col-sm-12'>";
            ViewMoney += "      <div class='onoffswitch'>";
            ViewMoney += "          <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='Usdswitch' />";
            ViewMoney += "          <label class='onoffswitch-label' for='Usdswitch'><span class='onoffswitch-inner'></span><span class='onoffswitch-switch'></span></label>";
            ViewMoney += "      </div>";
            ViewMoney += "  </div>";
            #endregion

            #region [Asset Info]
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='profile-user-info profile-user-info-striped'>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Mã căn</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='macan'>" + zAsset.ItemAsset.AssetID + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Tình trạng</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='tinhtrang'>" + zAsset.ItemAsset.Status + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Ngày liên hệ lại</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='ngaylienhe'>" + zAsset.ItemAsset.DateContractEnd + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá bán <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giaban1' style='display:none'>" + zAsset.ItemAsset.Price_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giaban2'>" + zAsset.ItemAsset.Price_VND + " (VND)</div><div class='pull-right'>" + ViewMoney + "</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá thuê <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><div view=usd class='pull-left giatien' id='giathue1' style='display:none'>" + zAsset.ItemAsset.PriceRent_USD + " (USD)</div><div view=vnd class='pull-left giatien' id='giathue2'>" + zAsset.ItemAsset.PriceRent_VND + " (VND)</div></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Giá mua <i class='ace-icon fa fa-usd blue'></i></div>");
            zSb.AppendLine("        <div class='profile-info-value'><span class=giatien id='giamua'>" + zAsset.ItemAsset.PricePurchase + " (VND)</span></div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Diện tích tim tường</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='dientich'>" + zAsset.ItemAsset.AreaWall + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nội thất</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='noithat'>" + zAsset.ItemAsset.Furniture + "</div>");
            zSb.AppendLine("    </div>");
            zSb.AppendLine("    <div class='profile-info-row'>");
            zSb.AppendLine("        <div class='profile-info-name'>Nhu cầu</div>");
            zSb.AppendLine("        <div class='profile-info-value' id='nhucau'>" + zAsset.ItemAsset.Purpose + "</div>");
            zSb.AppendLine("    </div>");

            if (UnitLevel <= 1)
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
                zSb.AppendLine("    </div>");
            }
            else if (zAsset.ItemAsset.EmployeeKey == HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"])
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'> Ghi chú</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='ghichu' value>" + zAsset.ItemAsset.Description + "</div>");
                zSb.AppendLine("    </div>");
            }
            if (!CheckViewPhone(
                zAsset.ItemAsset.EmployeeKey.ToInt(),
                zAsset.ItemAsset.AssetKey.ToInt(),
                zAsset.ItemAsset.AssetType))
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Sales quản lý</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.EmployeeName + "</div>");
                zSb.AppendLine("    </div>");
            }
            else
            {
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Chủ nhà</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.CustomerName + "</div>");
                zSb.AppendLine("    </div>");
                zSb.AppendLine("    <div class='profile-info-row'>");
                zSb.AppendLine("        <div class='profile-info-name'>Số điện thoại</div>");
                zSb.AppendLine("        <div class='profile-info-value' id='sales'>" + zAsset.ItemAsset.Phone1.HtmlPhone() + "</div>");
                zSb.AppendLine("    </div>");
            }

            zSb.AppendLine("</div>");
            #endregion
            ItemReturn zResult = new ItemReturn();
            zResult.Result = zSb.ToString();
            zResult.Result2 = @"<div class='widget-toolbar'><a href='/SAL/ProductView.aspx?ID=" + AssetKey + "&Type=ResaleApartment' onclick='$('.se-pre-con').fadeIn('slow');'><i class='ace-icon fa fa-external-link blue'></i>&nbsp;Xem chi tiết</a></div>";
            return zResult;
        }

        static bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            if (UnitLevel == 0 || UnitLevel == 1 || CurrentAgent == OwnerAgent)
            {
                return true;
            }

            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(AssetKey, Type);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }

        #region [Check Roles]
        //vi trí 0 read, 1 add, 2 edit, 3 delete; giá trị mỗi vị trí 0 và 1
        static string[] _Permitsion;
        void CheckRoles()
        {
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "SAL01";

            string[] result = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            _Permitsion = result;
            if (_Permitsion[2].ToInt() == 1)
            {
                Lit_Button.Text = @"
                        <button type='button' class='btn btn-white btn-info btn-bold' id='btnEdit'>
                            <i class='ace-icon fa fa-pencil blue'></i>
                            Chỉnh sửa
                        </button>";
            }
        }
        #endregion
    }
}