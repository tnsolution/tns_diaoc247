﻿using Lib.Config;
using Lib.HRM;
using Lib.KPI;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StringBuilder zSb = new StringBuilder();
                DataTable zTable = Categories_Data.ListDefine_Target_KPI();
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("<div class='checkbox'><label><input type='checkbox' class='ace' value='" + r["AutoKey"] + "' name='chkPurpose' checked='checked' /><span class='lbl'>" + r["ProductGroup"] + "% " + r["Product"] + "</span></label></div>");
                    lblMuctieu.Text += r["AutoKey"] + ",";
                }
                lblMuctieu.Text = lblMuctieu.Text.Remove(lblMuctieu.Text.LastIndexOf(","));
                Literal1.Text = zSb.ToString();

                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                HIDFromDate.Value = FromDate.ToString("dd/MM/yyyy HH:mm:ss");
                HIDToDate.Value = ToDate.ToString("dd/MM/yyyy HH:mm:ss");

                CheckRole();
                if (Request["Start"] != null && Request["End"] != null)
                {
                    FetchData(Request["Start"], Request["End"]);
                }
                else
                {
                    //chưa có báo cáo
                    string Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                    Report_Info zInfo = new Report_Info(Employee.ToInt(), DateTime.Now);
                    if (zInfo.Key == 0)
                    {
                        FetchData();
                        //LiteralTitle.Text = "Bạn chưa lập báo cáo";
                        //LiteralButtonTop.Text += "<button type=button onclick='taobaocao()' class='pull-right btn btn-primary btn-white'><i class='ace-icon fa fa-filter'></i>Lập báo cáo</button>";
                    }
                    else
                    {
                        //LiteralTitle.Text = "Bạn đã lập báo cáo rồi";
                        Literal_Button.Text = "<h4 id='ghichu'>Ghi chú</h4>" + zInfo.Description;
                        ItemReturn zResult = new ItemReturn();
                        string UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"];
                        string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                        string DepartmentRole = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];

                        DateTime zFromDate = new DateTime();
                        DateTime zToDate = new DateTime();
                        DateTime zQueryDate = new DateTime();

                        zQueryDate = DateTime.Now;
                        zFromDate = new DateTime(zQueryDate.Year, zQueryDate.Month, zQueryDate.Day, 0, 0, 0);
                        zToDate = new DateTime(zQueryDate.Year, zQueryDate.Month, zQueryDate.Day, 23, 59, 59);

                        switch (UnitLevel.ToInt())
                        {
                            //GD và Admin
                            case 0:
                            case 1:
                                zResult = SearchData(zFromDate, zToDate, DepartmentRole, Employee, lblMuctieu.Text);
                                break;
                            //Trưởng Phòng
                            case 2:
                                zResult = SearchData(zFromDate, zToDate, Department, Employee, lblMuctieu.Text);
                                break;
                            //Nhân viên
                            default:
                                zResult = SearchData(zFromDate, zToDate, string.Empty, Employee, lblMuctieu.Text);
                                break;
                        }
                        Literal_Table.Text = zResult.Result;
                        Literal_Table_Sub.Text = zResult.Result2;
                    }
                }
            }
        }
        void FetchData(string DateStart, string DateEnd)
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
            {
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            }
            else
            {
                ViewDepartment = Department;
            }

            DateTime zFromDate = Convert.ToDateTime(DateStart);
            DateTime zToDate = Convert.ToDateTime(DateEnd);
            ItemReturn zResult = new ItemReturn();

            if (UnitLevel >= 7)
            {
                zResult = SearchData(zFromDate, zToDate, string.Empty, Employee, lblMuctieu.Text);
            }
            else
            {
                zResult = SearchData(zFromDate, zToDate, Department, string.Empty, lblMuctieu.Text);
            }

            Literal_Table.Text = zResult.Result;
            Literal_Table_Sub.Text = zResult.Result2;

            Report_Info zInfo = new Report_Info(Employee.ToInt(), DateTime.Now);
            if (zInfo.Key != 0)
            {
                Literal_Button.Text = "<h4 id='ghichu'>Ghi chú</h4>" + zInfo.Description;
            }

            LiteralTitle.Text = "Báo cáo trong ngày " + zFromDate.ToString("dd/MM/yyyy");
        }
        void FetchData()
        {
            string Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
            string UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"];
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string DepartmentRole = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];

            ItemReturn zResult = new ItemReturn();
            if (UnitLevel.ToInt() <= 3)
            { zResult = SearchData(DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"), DepartmentRole, Employee, lblMuctieu.Text); }
            if (UnitLevel.ToInt() > 3 &&
               UnitLevel.ToInt() <= 6)
            { zResult = SearchData(DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"), Department, Employee, lblMuctieu.Text); }
            if (UnitLevel.ToInt() >= 7)
            { zResult = SearchData(DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"), string.Empty, Employee, lblMuctieu.Text); }

            Literal_Table.Text = zResult.Result;
            Literal_Table_Sub.Text = zResult.Result2;

            Report_Info zInfo = new Report_Info(Employee.ToInt(), DateTime.Now);
            if (zInfo.Key != 0)
            {
                Literal_Button.Text = "<h4 id='ghichu'>Ghi chú</h4>" + zInfo.Description;
            }
            else
            {
                string ReportKey = HttpContext.Current.Request.Cookies["UserLog"]["ReportKey"];
                if (ReportKey.ToInt() > 0)
                {
                    // Là nhân viên, kiểm tra cấp bật nhân viên
                    string html = "<h4 id='ghichu'>Ghi chú</h4>";
                    html += "<textarea id='txtGhichut' class='form-control'></textarea>";
                    html += "";
                    Literal_Button.Text = html;
                    //LiteralButtonTop.Text = "<a href='#mProduct' id='createNew' class='pull-right btn btn-primary btn-white' data-toggle='modal'><i class='ace-icon fa fa-eyes'></i>Lịch biểu</a>";
                    LiteralButtonTop.Text += "<a href='#mProduct' id='btnSave' class='pull-right btn btn-primary btn-white'><i class='ace-icon fa fa-send'></i>Gửi BC</a>";
                }
            }

            LiteralTitle.Text = "Báo cáo ngày " + DateTime.Now.ToString("dd/MM/yyyy");
        }

        ItemReturn SearchData(DateTime FromDate, DateTime ToDate, string Department, string Employee, string MucTieu)
        {
            string strResult = "";
            string strResult2 = "";
            ItemReturn zResult = new ItemReturn();

            //FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            //ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            if (Employee != string.Empty)
            {
                strResult = Process_Employee_Time(MucTieu, Department, Employee, FromDate, ToDate);
                strResult2 = ListItem(Employee, FromDate.ToString(), ToDate.ToString(), lblMuctieu.Text);
            }
            else if (Department != string.Empty)
            {
                strResult = Process_Department_Time(MucTieu, Department, FromDate, ToDate);
                strResult2 = ListItem(Employee, FromDate.ToString(), ToDate.ToString(), lblMuctieu.Text);
            }

            zResult.Result = strResult;
            zResult.Result2 = strResult2;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn SearchData(string ThoiGianTu, string ThoiGIanDen, string Department, string Employee, string MucTieu)
        {
            string strResult = "";
            string strResult2 = "";
            ItemReturn zResult = new ItemReturn();
            DateTime zFromDate = Tools.ConvertToDate(ThoiGianTu);
            DateTime zToDate = Tools.ConvertToDate(ThoiGIanDen);
            zFromDate = new DateTime(zFromDate.Year, zFromDate.Month, zFromDate.Day, 0, 0, 0);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            if (Department == "0")
            {
                Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            }

            if (Employee != string.Empty && Employee != "0")
            {
                strResult = Process_Employee_Time(MucTieu, Department, Employee, zFromDate, zToDate);
                //strResult2 = ListItem(Employee, zFromDate.ToString(), zToDate.ToString(), MucTieu);
            }
            else if (Department != string.Empty)
            {
                strResult = Process_Department_Time(MucTieu, Department, zFromDate, zToDate);
                //strResult2 = ListItem(Employee, zFromDate.ToString(), zToDate.ToString(), MucTieu);
            }


            zResult.Result = strResult;
            zResult.Result2 = strResult2;
            return zResult;
        }
        //phong
        protected static string Process_Department_Time(string MucTieu, string Department, DateTime FromDate, DateTime ToDate)
        {
            SqlContext Sql = new SqlContext();
            StringBuilder zSb = new StringBuilder();
            DataTable zData = Employees_Data.Staff_Working(Department);
            DataTable zHeader = Helper_Data.List(MucTieu);

            zSb.AppendLine("<table class='table table-bordered' id='tbl'>");

            #region [Tiêu đề]           
            #region [Th row 1]
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th rowspan=2 class=phong>Phòng</td>");
            zSb.AppendLine("            <th rowspan=2 class=nhanvien>Nhân viên</td>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("        <th>" + r["Name"].ToString() + "</th>");
            }
            zSb.AppendLine("        </tr>");
            #endregion
            #region [Th row 2]
            zSb.AppendLine("        <tr>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Thực hiện");
                zSb.AppendLine("            </th>");
            }
            zSb.AppendLine("        </tr>");

            #endregion
            #endregion

            #region [Data]
            if (zData.Rows.Count > 0)
            {
                double[] Array_Doing_Depart = new double[50];
                double[] Array_Doing_Employee = new double[50];
                foreach (DataRow r in zData.Rows)
                {
                    int Employee = r["EmployeeKey"].ToInt();
                    string DepartmentName = r["DepartmentName"].ToString();
                    string EmployeeName = r["EmployeeName"].ToString();

                    zSb.AppendLine("        <tr key=" + Employee + ">");
                    zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
                    zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");
                    int Exists = Get_Report(Employee.ToString(), FromDate, ToDate).ToInt();
                    if (Exists == 0 && Employee != HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt())
                    {
                        zSb.AppendLine("        <td colspan=" + zHeader.Rows.Count + ">Chưa gửi báo cáo !.</td>");
                    }
                    else
                    {
                        int n = 0;
                        foreach (DataRow rValue in zHeader.Rows)
                        {

                            #region Go SQL GetData
                            Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                            Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                            DataTable zRow = Sql.GetData("SELECT dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate)");
                            #endregion

                            #region Calculator                        
                            double TH = zRow.Rows[0][0].ToDouble();
                            Array_Doing_Depart[n] = Array_Doing_Depart[n] + TH;
                            Array_Doing_Employee[n] = TH;
                            n++;
                            #endregion
                        }

                        #region Show
                        zSb.AppendLine(Show_Data_Employee(Array_Doing_Employee, zHeader.Rows.Count));
                        #endregion
                    }
                    zSb.AppendLine("        </tr>");
                }
                zSb.AppendLine(Show_Sum_Row("Tổng phòng", Array_Doing_Depart, zHeader.Rows.Count, "tongphong"));
            }
            #endregion

            zSb.AppendLine("</table>");
            Sql.CloseConnect();
            return zSb.ToString();
        }
        //nhan vien
        protected static string Process_Employee_Time(string MucTieu, string Department, string Employee, DateTime FromDate, DateTime ToDate)
        {
            SqlContext Sql = new SqlContext();
            StringBuilder zSb = new StringBuilder();

            Employees_Info zEmployee = new Employees_Info(Employee.ToInt());
            string EmployeeName = zEmployee.LastName + " " + zEmployee.FirstName;
            string DepartmentName = zEmployee.DepartmentName;
            double[] Array_Doing_Employee = new double[50];
            DataTable zHeader = Helper_Data.List(MucTieu);
            zSb.AppendLine("<table class='table table-bordered' id='tbl'>");
            #region [Tiêu đề]
            #region [Th row 1]
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <th rowspan=2 class=phong>Phòng</td>");
            zSb.AppendLine("            <th rowspan=2 class=nhanvien>Nhân viên</td>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("        <th>" + r["Name"].ToString() + "</th>");
            }
            zSb.AppendLine("        </tr>");
            #endregion
            #region [Th row 2]
            zSb.AppendLine("        <tr>");
            foreach (DataRow r in zHeader.Rows)
            {
                zSb.AppendLine("            <th>");
                zSb.AppendLine("                Thực hiện");
                zSb.AppendLine("            </th>");
            }
            zSb.AppendLine("        </tr>");
            #endregion
            #endregion

            zSb.AppendLine("        <tr key=" + Employee + ">");
            zSb.AppendLine("            <td class=phong>" + DepartmentName + "</td>");
            zSb.AppendLine("            <td class=nhanvien>" + EmployeeName + "</td>");
            int Exists = Get_Report(Employee.ToString(), FromDate, ToDate).ToInt();
            if (Exists == 0 && Employee != HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"])
            {
                zSb.AppendLine("        <td colspan=" + zHeader.Rows.Count + ">Chưa gửi báo cáo !.</td>");
            }
            else
            {
                foreach (DataRow rValue in zHeader.Rows)
                {
                    #region Go SQL GetData
                    Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                    float Value = Sql.GetObject("SELECT dbo.KPI_DoingV3(" + Employee + ", " + rValue["AutoKey"].ToString() + ", @FromDate, @ToDate)").ToFLoat();
                    #endregion        

                    #region Show
                    zSb.AppendLine("        <td class=number>" + Value.ToString("n2") + "</td>");
                    #endregion
                }
            }
            zSb.AppendLine("        </tr>");
            zSb.AppendLine("</table>");
            return zSb.ToString();
        }

        protected static string Show_Data_Employee(double[] Doing, int Length)
        {
            StringBuilder zSb = new StringBuilder();
            for (int n = 0; n < Length; n++)
            {
                zSb.AppendLine("        <td class=number>" + Doing[n].ToString("n2") + "</td>");
            }
            return zSb.ToString();
        }
        protected static string Show_Sum_Row(string Name, double[] Doing, int Length, string CssClass)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <td class='" + CssClass + "' colspan=2>" + Name + "</td>");
            for (int n = 0; n < Length; n++)
            {
                double TH = Doing[n].ToDouble();
                zSb.AppendLine("        <td class='number " + CssClass + "'>" + Doing[n].ToString("n2") + "</td>");
            }
            zSb.AppendLine("        </tr>");
            return zSb.ToString();
        }

        //kiem tra có report hoac ko
        static string Get_Report(string Employee, DateTime FromDate, DateTime ToDate)
        {
            SqlContext Sql = new SqlContext();
            Sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            Sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            int Key = Sql.GetObject("SELECT ReportKey FROM TASK_Report WHERE EmployeeKey = " + Employee + " AND ReportDate BETWEEN @FromDate AND @ToDate").ToInt();
            Sql.CloseConnect();
            if (Key != 0)
            {
                return Key.ToString();
            }
            else
            {
                return "0";
            }
        }
        //tinh tong
        static string Sum_Day(string Name, double[] ItemDoing, int Length, string CssClass)
        {
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("        <tr>");
            zSb.AppendLine("            <td colspan='2' class=" + CssClass + ">" + Name + "</td>");
            for (int i = 0; i < Length; i++)
            {
                zSb.AppendLine("        <td class='" + CssClass + " number'>" + ItemDoing[i] + "</td>");
            }
            zSb.AppendLine("        </tr>");
            return zSb.ToString();
        }
        //get datalist
        [WebMethod]
        public static string ListItem(string Employee, string FromDate, string ToDate, string Muctieu)
        {
            try
            {
                DateTime zFromDate; //= DateTime.Parse(FromDate);
                DateTime.TryParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zFromDate);
                DateTime zToDate;// = DateTime.Parse(ToDate);
                DateTime.TryParseExact(ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zToDate);
                StringBuilder zSb = new StringBuilder();
                int EmployeeKey = Employee.ToInt();
                DataTable zHeader = Helper_Data.List(Muctieu);
                foreach (DataRow r in zHeader.Rows)
                {
                    string temp = string.Empty;

                    #region [SubTable]
                    StringBuilder zSub = new StringBuilder();
                    DataTable ztbSub = new DataTable();
                    int Category = r["AutoKey"].ToInt();
                    switch (Category)
                    {
                        case 290:
                        case 291:
                        case 306:
                            //khách hàng
                            ztbSub = Helper.GetKPI_Customer(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Thực hiện</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Khách hàng</td>");
                                zSub.AppendLine("                   <td>Điện thoại</td>");
                                zSub.AppendLine("                   <td class='noidung'>Nội dung</td>");
                                zSub.AppendLine("                   <td>Tiếp xúc</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("       <td>" + i + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["CustomerName"].ToString() + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["Phone1"].ToString().Replace(" ", "") + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["Description"].ToString() + "</td>");
                                    zSub.AppendLine("       <td>" + rSub["Status"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;

                        case 295:
                        case 298:
                            //giao dich 
                            ztbSub = Helper.GetKPI_Trade(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class=nhanvien>Thực hiện</td>");
                                zSub.AppendLine("                   <td>Mã</td>");
                                zSub.AppendLine("                   <td>Dự án</td>");
                                zSub.AppendLine("                   <td>Doanh thu</td>");
                                //zSub.AppendLine("                   <td>Tiếp xúc</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("    <td>" + i + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["AssetID"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["ProjectName"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["Income"].ToDoubleString() + "</td>");
                                    //zSub.AppendLine("    <td>" + rSub["Status"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;

                        case 307:
                            //Cap nhat san pham
                            ztbSub = Helper.GetKPI_Asset(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Thực hiện</td>");
                                zSub.AppendLine("                   <td>Mã</td>");
                                zSub.AppendLine("                   <td>Dự án</td>");
                                zSub.AppendLine("                   <td class='noidung'>Nội dung</td>");
                                //zSub.AppendLine("                   <td>Tiếp xúc</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("    <td>" + i + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["AssetID"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["ProjectName"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["Description"].ToString() + "</td>");
                                    //zSub.AppendLine("    <td>" + rSub["Status"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;

                        case 329:
                            //sale phone
                            ztbSub = Helper.GetKPI_Phone(EmployeeKey, 0, Category, zFromDate, zToDate);
                            if (ztbSub.Rows.Count > 0)
                            {
                                zSub.AppendLine("    <div class='table-detail'>");
                                zSub.AppendLine("            <table class='table table-striped'>");
                                zSub.AppendLine("                <thead>");
                                zSub.AppendLine("                <tr>");
                                zSub.AppendLine("                   <td class='no'>STT</td>");
                                zSub.AppendLine("                   <td class='nhanvien'>Thực hiện</td>");
                                zSub.AppendLine("                   <td>Thông tin</td>");
                                zSub.AppendLine("                   <td>Sản phẩm</td>");
                                zSub.AppendLine("                   <td class='noidung'>Tình trạng</td>");
                                zSub.AppendLine("                </tr>");
                                zSub.AppendLine("                </thead>");
                                zSub.AppendLine("                <tbody>");
                                int i = 1;
                                foreach (DataRow rSub in ztbSub.Rows)
                                {
                                    string phone1 = rSub["Phone1"].ToString() == string.Empty ? string.Empty : rSub["Phone1"].ToString() + "<br />";
                                    string phone2 = rSub["Phone2"].ToString() == string.Empty ? string.Empty : rSub["Phone2"].ToString() + "<br />";
                                    string mail1 = rSub["Email1"].ToString() == string.Empty ? string.Empty : rSub["Email1"].ToString() + "<br />";
                                    string mail2 = rSub["Email2"].ToString() == string.Empty ? string.Empty : rSub["Email2"].ToString();

                                    zSub.AppendLine("    <tr>");
                                    zSub.AppendLine("    <td>" + i + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["EmployeeDo"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>Họ tên: " + rSub["FullName"].ToString() + "<br/>Điện thoại: " + phone1 + phone2 + mail1 + mail2 + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["Product"].ToString() + "<br/>" + rSub["Asset"].ToString() + "<br/>PN:" + rSub["Room"].ToString() + "</td>");
                                    zSub.AppendLine("    <td>" + rSub["StatusName"].ToString() + "</td>");
                                    zSub.AppendLine("    </tr>");
                                    i++;
                                }
                                zSub.AppendLine("                </tbody>");
                                zSub.AppendLine("            </table>");
                                zSub.AppendLine("    </div>");
                            }
                            break;
                    }
                    #endregion

                    zSb.AppendLine("<tr>");
                    zSb.AppendLine("    <td class=no>#</td>");
                    zSb.AppendLine("    <td><span style='line-height: 2;'>" + r["Name"].ToString() + "</span></td>");
                    zSb.AppendLine("</tr>");

                    zSb.AppendLine("<tr class='detail-row'>");
                    zSb.AppendLine("    <td colspan=2>");
                    zSb.AppendLine(zSub.ToString());
                    zSb.AppendLine("    </td>");
                    zSb.AppendLine("</tr>");
                }
                return zSb.ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        [WebMethod]
        public static string GetDescription(string Employee, string Date)
        {
            DateTime zDate = Convert.ToDateTime(Date);
            Report_Info zInfo = new Report_Info(Employee.ToInt(), zDate);
            return zInfo.Description;
        }
        [WebMethod]
        public static string SendReport(string Employee, string FromDate, string ToDate, string Text)
        {
            DateTime nDate = DateTime.Now;
            DateTime zFromDate = nDate;
            DateTime zTodate = nDate;
            string Result = "";
            Report_Info zInfo = new Report_Info();
            zInfo.EmployeeKey = Employee.ToInt();
            zInfo.ReportDate = zFromDate;
            zInfo.Start = zFromDate.ToString("yyyy-MM-dd");
            zInfo.End = zFromDate.AddDays(1).ToString("yyyy-MM-dd");
            zInfo.Description = Text;
            zInfo.EmployeeName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.Title = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Create();
            int Key = zInfo.Key;
            if (Key != 0)
            {
                string query = @"
UPDATE KPI_Assets SET ParentKey = @KEY WHERE CreatedDate BETWEEN @FromDate AND @ToDate AND EmployeeKey = @EmployeeKey
UPDATE KPI_Customers SET ParentKey = @KEY WHERE CreatedDate BETWEEN @FromDate AND @ToDate AND EmployeeKey = @EmployeeKey
UPDATE KPI_Phone SET ParentKey = @KEY WHERE CreatedDate BETWEEN @FromDate AND @ToDate AND EmployeeKey = @EmployeeKey
UPDATE KPI_Trades SET ParentKey = @KEY WHERE CreatedDate BETWEEN @FromDate AND @ToDate AND EmployeeKey = @EmployeeKey";
                SqlContext sql = new SqlContext();
                sql.CMD.Parameters.Add("@KEY", SqlDbType.Int).Value = Key;
                sql.CMD.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = Employee;
                sql.CMD.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                sql.CMD.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zTodate;
                sql.GetObject(query);
            }
            else
            {
                Result = "ERROR";
            }

            return Result;
        }

        //
        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];
            string ViewDepartment = "";
            if (HttpContext.Current.Request.Cookies["ViewDepart"] != null)
            {
                ViewDepartment = HttpContext.Current.Request.Cookies["ViewDepart"].Value.ToUrlDecode();
            }
            else
            {
                ViewDepartment = Department;
            }

            if (UnitLevel <= 3)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName ", false);

                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;

                //Literal_Table.Text = SearchData("choncongty", "chonthang", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.Month.ToString(), "", DateTime.Now.Year.ToString(), lblMuctieu.Text, string.Empty, ViewDepartment).Result;
            }
            if (UnitLevel > 3 &&
                UnitLevel <= 6)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey IN (" + ViewDepartment + ") ORDER BY LastName", false);
                Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName ", false);

                DDL_Employee.Visible = true;
                DDL_Department.Visible = true;

                //Literal_Table.Text = SearchData("chonphong", "chonthang", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.Month.ToString(), "", DateTime.Now.Year.ToString(), lblMuctieu.Text, string.Empty, ViewDepartment).Result;
            }
            if (UnitLevel >= 7)
            {
                Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);

                DDL_Employee.SelectedValue = Employee.ToString();

                DDL_Department.Visible = false;
                DDL_Employee.Visible = false;

                //Literal_Table.Text = SearchData("chonnhanvien", "chonthang", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.Month.ToString(), "", DateTime.Now.Year.ToString(), lblMuctieu.Text, Employee.ToString(), ViewDepartment).Result;
            }

            //Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey IN (" + ViewDepartment + ") ORDER BY DepartmentName ", false);
        }

        protected void LoadBaoCao_Click(object sender, EventArgs e)
        {
            //FetchData();
        }
    }
}