﻿using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebApp.SAL
{
    public partial class ReportList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRole();
                LoadData();
            }
        }

        void LoadData()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            StringBuilder zSb = new StringBuilder();
            DateTime zDate = DateTime.Now;
            Report_Info zInfo = new Report_Info(Employee, zDate);
            zSb.AppendLine("<table id='tbldata' class='table table-bordered table-hover'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Chỉ tiêu</th>");
            zSb.AppendLine("        <th>Kết quả ngày</th>");
            zSb.AppendLine("        <th>Yêu cầu/ tháng</th>");
            zSb.AppendLine("        <th>Đã thực hiện/ tháng</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("        <tbody>");

            if (zInfo.Key > 0)
            {
                //get data đã có
                int i = 1;
                DataTable zChild = Report_Rec_Data.List(zInfo.Key, zInfo.ReportDate.Month, zInfo.ReportDate.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["Result"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Require"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Doing"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }
            }
            else
            {
                //get data mới
                int i = 1;
                DataTable zChild = Plan_Rec_Data.List(Employee, DateTime.Now.Month, DateTime.Now.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>0</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Require"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Doing"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            zSb.ToString();
            LitTitle.Text = "Báo cáo " + DateTime.Now.ToString("dd/MM/yyyy");
            LitData.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemReturn GetRecord(int ReportKey, string ReportDate, int EmployeeKey)
        {
            ItemReturn zResult = new ItemReturn();
            StringBuilder zSb = new StringBuilder();

            int Employee = EmployeeKey;
            string Name = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            DateTime zDate = DateTime.Now;
            Report_Info zInfo = new Report_Info();
            if (ReportKey != 0)
            {
                zInfo = new Report_Info(ReportKey);
                zDate = zInfo.ReportDate;
                Name = zInfo.EmployeeName;
            }
            else
            {
                zDate = Convert.ToDateTime(ReportDate);
                zInfo = new Report_Info(Employee, zDate);
                Name = zInfo.EmployeeName;
            }

            zSb.AppendLine("<table id='tbldata' class='table table-bordered table-hover'>");
            zSb.AppendLine("    <thead>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Chỉ tiêu</th>");
            zSb.AppendLine("        <th>Kết quả ngày</th>");
            zSb.AppendLine("        <th>Yêu cầu/ tháng</th>");
            zSb.AppendLine("        <th>Đã thực hiện/ tháng</th>");
            zSb.AppendLine("    </thead>");
            zSb.AppendLine("        <tbody>");

            if (zInfo.Key > 0)
            {
                //get data đã có
                int i = 1;
                DataTable zChild = Report_Rec_Data.List(zInfo.Key, zInfo.ReportDate.Month, zInfo.ReportDate.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["Result"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Require"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Doing"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }

                zResult.Result2 = zInfo.ReportDate.ToString("dd/MM/yyyy");
                zResult.Result = "Báo cáo ngày " + zInfo.ReportDate.ToString("dd/MM/yyyy") + " " + Name;

                if (zDate.Day < DateTime.Now.Day)
                {
                    zResult.Result3 = "0";
                }
                else
                {
                    zResult.Result3 = "1";
                }
            }
            else
            {
                if (zDate.Day < DateTime.Now.Day)
                {
                    zResult.Result = "Báo cáo ngày" + zDate.ToString("dd/MM/yyyy");
                    zResult.Result3 = "0";
                    zResult.Message = "<h1 class='center'><img src='/Upload/worry.gif' style='width:64px;' /> &nbsp; Bánh mì cháy !!!</h1>";
                    return zResult;
                }
                else
                {
                    zResult.Result3 = "1";
                }

                //get data mới
                int i = 1;
                DataTable zChild = Plan_Rec_Data.List(EmployeeKey, DateTime.Now.Month, DateTime.Now.Year);
                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled' Category='" + r["CategoryKey"].ToString() + "'>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>0</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Require"].ToString() + "</td>");
                    zSb.AppendLine("        <td class='edit-disabled'>" + r["Doing"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }

                zResult.Result2 = zDate.ToString("dd/MM/yyyy");
                zResult.Result = "Lập mới báo cáo";
            }

            zSb.AppendLine("    </tbody>");
            zSb.AppendLine("</table>");
            zSb.ToString();

            // return paramater

            zResult.Result5 = zInfo.Key.ToString();
            zResult.Result4 = zInfo.Description;
            zResult.Message = zSb.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemReturn SaveReport(int Key, string StartDate, string EndDate, string Description, string Table)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            string Name = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            ItemReturn zResult = new ItemReturn();
           
            Report_Info zInfo;
            if (Key == 0)
            {
                DateTime ReportDate = Convert.ToDateTime(StartDate);
                zInfo = new Report_Info(Employee, ReportDate);
                zInfo.ReportDate = ReportDate;
                zInfo.Start = StartDate;
                zInfo.End = EndDate;
            }
            else
                zInfo = new Report_Info(Key);

            zInfo.Title = Name;
            zInfo.Description = Description;

            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = Employee.ToString();
            zInfo.ModifiedBy = Employee.ToString();
            zInfo.CreatedName = Name;
            zInfo.ModifiedName = Name;
            zInfo.Save();

            if (zInfo.Message == string.Empty)
            {
                string Message = new Report_Rec_Info().Delete(zInfo.Key);

                List<ItemWeb> zList = new List<ItemWeb>();
                string[] row = Table.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in row)
                {
                    string[] col = item.Split(',');
                    ItemWeb itemWeb = new ItemWeb();
                    itemWeb.Value = col[0];
                    itemWeb.Text = col[1];
                    zList.Add(itemWeb);
                }

                foreach (ItemWeb item in zList)
                {
                    Report_Rec_Info zRecord = new Report_Rec_Info();
                    zRecord.ReportKey = zInfo.Key;
                    zRecord.CategoryKey = item.Value.ToInt();
                    zRecord.Result = item.Text.ToInt();
                    zRecord.Create();
                }
            }

            zResult.Message = zInfo.Message;
            return zResult;
        }

        void CheckRole()
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            switch (UnitLevel)
            {
                case 0:
                case 1:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments ORDER BY DepartmentName", false);

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = true;
                    break;

                case 2:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE IsWorking=2 AND DepartmentKey = " + Department + " ORDER BY LastName", false);
                    Tools.DropDown_DDL(DDL_Department, "SELECT DepartmentKey, DepartmentName FROM HRM_Departments WHERE DepartmentKey = " + Department + " ORDER BY DepartmentName", false);
                    DDL_Department.SelectedValue = Department.ToString();
                    DDL_Employee.SelectedValue = Employee.ToString();

                    DDL_Employee.Visible = true;
                    DDL_Department.Visible = false;
                    break;

                default:
                    Tools.DropDown_DDL(DDL_Employee, "SELECT EmployeeKey, LastName + ' ' + FirstName FROM HRM_Employees WHERE EmployeeKey = " + Employee + " AND IsWorking=2 ORDER BY LastName", false);
                    DDL_Employee.SelectedValue = Employee.ToString();
                    DDL_Department.Visible = false;
                    DDL_Employee.Visible = false;
                    break;
            }
        }
    }
}