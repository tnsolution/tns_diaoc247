﻿var Employee;
var Department;
var unit;
$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();
});

jQuery(function ($) {
    Employee = Page.getEmployee();
    Department = Page.getDepartment();
    unit = Page.getUnitLevel();
    if (unit <= 2) {
        $(".modal.aside").ace_aside();
    }

    $(".select2").select2({ width: "100%" });
    $("#btnSearch").click(function () {
        Employee = $('[id$=DDL_Employee]').val();
        Department = $('[id$=DDL_Department]').val();
        if (Employee == null) {
            alert("Bạn phải chọn nhân viên");
        }
        if (Department == null) {
            Department = Page.getDepartment();
        }
        $('#calendar').fullCalendar('destroy');
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: '',// 'month,agendaWeek,agendaDay'//
            },
            defaultView: 'month',
            eventClick: updateEvent,
            selectable: true,
            selectHelper: true,
            select: selectDate,
            editable: true,
            events: '/JS_Report.ashx?Employee=' + Employee + '&Department=' + Department,
            eventRender: function (event, element) {
                //alert(event.title);
                element.qtip({
                    content: {
                        text: qTipText(event.start, event.end, event.description),
                        title: '<strong>' + event.title + '</strong>'
                    },
                    position: {
                        my: 'top center',
                        at: 'bottom center'
                    },
                    style: { classes: 'qtip-shadow qtip-rounded tooltipfont' }
                });
            }
        });
    });

    $('[id$=DDL_Department]').change(function () {
        var Key = $(this).val();
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetEmployees",
            data: JSON.stringify({
                DepartmentKey: $(this).val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                var District = $("[id$=DDL_Employee]");
                District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                $.each(msg.d, function () {
                    District.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                
            }
        });
    });
});
jQuery(function ($) {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var options = {
        weekday: "long", year: "numeric", month: "short",
        day: "numeric", hour: "2-digit", minute: "2-digit"
    };

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: '',//;'month,agendaWeek,agendaDay'//
        },
        defaultView: 'month',
        eventClick: updateEvent,
        selectable: true,
        selectHelper: true,
        select: selectDate,
        editable: true,
        events: '/JS_Report.ashx?Employee=' + Employee + '&Department=' + Department,
        eventRender: function (event, element) {
            //alert(event.title);
            element.qtip({
                content: {
                    text: qTipText(event.start, event.end, event.description),
                    title: '<strong>' + event.title + '</strong>'
                },
                position: {
                    my: 'top center',
                    at: 'bottom center'
                },
                style: { classes: 'qtip-shadow qtip-rounded tooltipfont' }
            });
        }
    });
    $("#btnSave").click(function () {
        var total = $('#tbldata tbody tr').length;
        var row = "";
        var num = 0;
        $('#tbldata tbody tr').each(function () {
            num++;
            if (num < total)
                row += "" + $(this).find('td:eq(1)').attr('Category') + "," + $(this).find('td:eq(2)').text() + ";";
            else
                row += "" + $(this).find('td:eq(1)').attr('Category') + "," + $(this).find('td:eq(2)').text();
        });       
        console.log($("#eventStartDate").val());
        console.log($("#eventId").val());
        if ($("#eventStartDate").val() == '' &&
            $("#eventId").val() == 0) {
            alert("Bạn phải chọn ngày lập báo cáo");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: '/SAL/ReportList.aspx/SaveReport',
            data: JSON.stringify({
                'Key': $("#eventId").val(),
                'StartDate': $("#eventStartDate").val(),
                'EndDate': $("#eventEndDate").val(),
                'Description': $("[id$=txt_Description]").val(),
                'Table': row,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                if (msg.d.Message != '') {
                    Page.showPopupMessage("Lỗi lưu dữ liệu", msg.d.Message);
                }
                else
                    location.reload();
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

    });
});

var currentUpdateEvent;
var addStartDate;
var addEndDate;
var globalAllDay;

function updateEvent(event, element) {
    if ($("[id$=DDL_Employee]").val() != null)
        Employee = $("[id$=DDL_Employee]").val();
    console.log('Employee' + Employee);
    $("#eventId").val(event.id);
    $.ajax({
        type: 'POST',
        url: '/SAL/ReportList.aspx/GetRecord',
        data: JSON.stringify({
            ReportKey: event.id,
            ReportDate: '',
            EmployeeKey: Employee
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {

            $("#ghichu").show();
            $("#noidungbentrai").empty();
            $("#noidungbentrai").append($(r.d.Message));

            $("#txt_Description").val(r.d.Result4);
            $("#tieudetrang").text(r.d.Result);
            console.log(r.d.Result3);
            if (r.d.Result3 == 0) {
                $("#tieudetrang").text(r.d.Result);
                $("#noidungbentrai").empty();
                $("#noidungbentrai").append(r.d.Message);
                $("#btnSave").hide();

                $("#txt_Description").attr("readonly", true);
            }
            else {
                $("#btnSave").show()
                $("#txt_Description").attr("readonly", false);
                $("#noidungbentrai").empty();
                $("#noidungbentrai").append($(r.d.Message));
                $("#tieudetrang").text(r.d.Result);
                $("#tbldata").editableTableWidget().
                    numericInputExample().
                    find('td:nth-child(2)').
                    focus(function () {
                        $(this).select();
                    });

                //if (r.d.Result5 != 0)
                //    $("#btnSave").hide();
                //else {
                //    $("#txt_Description").attr("readonly", false);
                //    $("#btnSave").show();
                //}
            }
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Lỗi: " + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        },
        complete: function () {
        }
    });
    return false;
}
function selectDate(start, end, allDay) {
    if ($("[id$=DDL_Employee]").val() != null)
        Employee = $("[id$=DDL_Employee]").val();

    console.log('Employee' + Employee);
    console.log(addStartDate);

    addStartDate = start;
    addEndDate = end;
    globalAllDay = allDay;

    $("#eventStartDate").val(addStartDate.toJSON());
    $("#eventEndDate").val(addEndDate.toJSON());

    $("#eventId").val(0);
    $("#txt_Description").val('');
    $.ajax({
        type: 'POST',
        url: '/SAL/ReportList.aspx/GetRecord',
        data: JSON.stringify({
            ReportKey: 0,
            ReportDate: start,
            EmployeeKey: Employee
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            $("#eventId").val(r.d.Result5);
            $("#ghichu").show();
            $("#noidungbentrai").empty();
            $("#noidungbentrai").append($(r.d.Message));

            $("#txt_Description").val(r.d.Result4);
            $("#tieudetrang").text(r.d.Result);
            console.log(r.d.Result3);
            if (r.d.Result3 == 0) {
                $("#tieudetrang").text(r.d.Result);
                $("#noidungbentrai").empty();
                $("#noidungbentrai").append(r.d.Message);
                $("#btnSave").hide();

                $("#txt_Description").attr("readonly", true);
            }
            else {
                $("#btnSave").show()
                $("#txt_Description").attr("readonly", false);
                $("#noidungbentrai").empty();
                $("#noidungbentrai").append($(r.d.Message));
                $("#tieudetrang").text(r.d.Result);
                $("#tbldata").editableTableWidget().
                    numericInputExample().
                    find('td:nth-child(2)').
                    focus(function () {
                        $(this).select();
                    });
            }
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Lỗi: " + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        },
        complete: function () {
        }
    });
}

function isAllDay(startDate, endDate) {
    var allDay;

    if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
        allDay = true;
        globalAllDay = true;
    }
    else {
        allDay = false;
        globalAllDay = false;
    }

    return allDay;
}
function qTipText(start, end, description) {
    var text;

    //if (end !== null)
    //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") + "<br/><br/>" + description;
    //else
    //    text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") + "<br/><strong>End:</strong><br/><br/>" + description;

    return description;
}
//---------------