﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp.SAL
{
    public partial class ScheduleEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_ScheduleKey.Value = Request["ID"];

                LoadInfo();
                LoadTable();
            }
        }

        void LoadInfo()
        {
            Lit_TitlePage.Text = "Lập kế hoạch.";
            Schedule_Info zInfo = new Schedule_Info(HID_ScheduleKey.Value.ToInt());
            txt_Date.Value = zInfo.ScheduleDate.ToString("dd/MM/yyyy");
            txt_Description.Value = zInfo.Description;

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Người cập nhật:</td><td>" + zInfo.ModifiedName + "</td></tr>");
            zSb.AppendLine("    <tr><td><i class='ace-icon fa fa-angle-right bigger-110'></i>&nbsp Ngày cập nhật:</td><td>" + zInfo.ModifiedDate.ToString("dd/MM/yyyy") + "</td></tr>");
            zSb.AppendLine("</table>");
            Lit_Info.Text = zSb.ToString();
        }
        void LoadTable()
        {
            DataTable zTable = Schedule_Rec_Data.List(HID_ScheduleKey.Value.ToInt());
            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<table class='table  table-bordered table-hover' id='tbldata'>");
            zSb.AppendLine("   <thead>");
            zSb.AppendLine("    <tr>");
            zSb.AppendLine("        <th>STT</th>");
            zSb.AppendLine("        <th>Thời gian</th>");
            zSb.AppendLine("        <th>Nội dung</th>");
            zSb.AppendLine("    </tr>");
            zSb.AppendLine("   </thead>");
            zSb.AppendLine("        <tbody>");

            if (zTable.Rows.Count > 0)
            {
                int i = 1;
                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("            <tr id='"+r["ID"].ToString()+"'>");
                    zSb.AppendLine("               <td>" + (i++) + "</td>");            
                    zSb.AppendLine("               <td>" + r["WorkTime"].ToString() + "</td>");
                    zSb.AppendLine("               <td>" + r["WorkContent"].ToString() + "</td>");              
                    zSb.AppendLine("            </tr>");
                }
            }

            zSb.AppendLine("        </tbody>");
            zSb.AppendLine("</table>");

            Lit_Table.Text = zSb.ToString();
        }

        [WebMethod]
        public static ItemResult DeleteSchedule(int Key)
        {
            ItemResult zResult = new ItemResult();
            Schedule_Info zInfo = new Schedule_Info();
            zInfo.Key = Key;
            zInfo.Delete();
            zResult.Message = zInfo.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemResult SaveSchedule(int Key, string Date, string Description)
        {
            ItemResult zResult = new ItemResult();
            Schedule_Info zInfo = new Schedule_Info(Key);
            zInfo.ScheduleDate = Tools.ConvertToDate(Date);
            zInfo.Description = Description;

            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;            
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        public static ItemResult InitInfo(string Date, string Description)
        {
            ItemResult zResult = new ItemResult();
            Schedule_Info zInfo = new Schedule_Info();
            zInfo.ScheduleDate = Tools.ConvertToDate(Date);
            zInfo.Description = Description;

            zInfo.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            zInfo.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            else
                zResult.Message = "OK";
            zResult.Result = zInfo.Key.ToString();
            return zResult;
        }

        [WebMethod]
        //public static ItemSchedule GetDetailRecord(int AutoKey)
        //{
        //    Schedule_Rec_Info zInfo = new Schedule_Rec_Info(AutoKey);
        //    ItemSchedule zItem = new ItemSchedule();
        //    zItem.ID = zInfo.ID.ToString();
        //    zInfo.WorkTime = zInfo.WorkTime;
        //    zItem.WorkTime = zInfo.WorkTime;
        //    zItem.WorkContent = zInfo.WorkContent;
        //    zItem.Message = zInfo.Message;
        //    return zItem;
        //}

        [WebMethod]
        public static ItemSchedule[] GetListRecord(int Key)
        {
            DataTable zTable = Schedule_Rec_Data.List(Key);
            List<ItemSchedule> ListAsset = zTable.DataTableToList<ItemSchedule>();
            return ListAsset.ToArray();
        }

        [WebMethod]
        public static ItemResult SaveDetailRecord(int AutoKey, int Key, string Time, string Content)
        {
            Schedule_Rec_Info zInfo = new Schedule_Rec_Info(AutoKey);
            ItemResult zResult = new ItemResult();

            zInfo.TimeKey = Key;
            zInfo.WorkTime = Time;
            zInfo.WorkContent = Content;
            zInfo.Save();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            zResult.Result = zInfo.ID.ToString();
            return zResult;
        }
    }
}