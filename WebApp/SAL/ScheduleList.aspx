﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ScheduleList.aspx.cs" Inherits="WebApp.SAL.ScheduleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/fullcalendar.min.css" />
    <link rel="stylesheet" href="/template/jquery.timepicker.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" />
    <style>
        .fc-content {           
            white-space: inherit !important;
        }

        .tooltipfont {
            font-size: 14px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div class="page-content">
        <div class="page-header">
            <h1>Kế hoạch
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Chọn phòng--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Employee" runat="server" CssClass="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True" disabled="disabled"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade" id="mView" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" id="title">Kế hoạch</h4>
                </div>
                <div class="modal-body">
                    <div id="input">
                        <div class="form-group">
                            <label class="control-label">Thời gian </label>
                            <div class="input-group bootstrap-timepicker" id="datepairExample2">
                                <input type="text" id="txt_AddFrom" class="form-control col-sm-6 time start" placeholder="Chọn giờ phút" required="" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                                <input type="text" id="txt_AddTo" class="form-control col-sm-6 time end" placeholder="Chọn giờ phút" required="" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Chi tiết nội dung trong khung giờ</label>
                            <div class="input-group">
                                <input name="txt_Content" type="text" id="txt_AddContent" class="form-control" placeholder="..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button" id="addRecord">
                                        <i class="ace-icon fa fa-plus bigger-110"></i>
                                        Thêm !
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="bang">
                        <table id="tbldata" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th>...</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="noidung">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSave">OK</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="childId" value="0" />
    <input type="hidden" id="eventId" value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="/template/ace-master/assets/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="/template/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/template/datepair.js"></script>
    <script type="text/javascript" src="/template/jquery.datepair.js"></script>
    <script src="ScheduleList.js"></script>
</asp:Content>
