﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProgressBoard_SaleNew_Edit.aspx.cs" Inherits="WebApp.ADM.ProgressBoard_SaleNew_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .thAction {
            width: 50px;
        }

        .profile-info-name {
            width: 200px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Cài đặt chi tiết dự án
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="profile-user-info profile-user-info-striped">
                <div class="profile-info-row">
                    <div class="profile-info-name">Dự án</div>
                    <div class="profile-info-value">
                        <span>
                            <asp:Literal ID="LitProject" runat="server"></asp:Literal></span>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">Doanh thu hoặc Giá bán</div>
                    <div class="profile-info-value">
                        <span>
                            <asp:Literal ID="LitMethod1" runat="server"></asp:Literal></span>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">Phần trăm hoặc Lũy tiến</div>
                    <div class="profile-info-value">
                        <span>
                            <asp:Literal ID="LitMethod2" runat="server"></asp:Literal></span>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">Kích hoạt sử dụng</div>
                    <div class="profile-info-value">
                        <span>
                            <asp:Literal ID="LitActive" runat="server"></asp:Literal></span>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name">Ghi chú</div>
                    <div class="profile-info-value">
                        <span>
                            <asp:Literal ID="LitDescription" runat="server"></asp:Literal></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tools pull-right">
                    <a href="#" class="btn btn-white btn-info btn-bold" id="addnew">
                        <i class="ace-icon fa fa-plus"></i>
                        Thêm
                    </a>
                </div>
                <table class='table table-hover table-bordered' id='tableProgressive'>
                    <thead>
                        <tr>
                            <th>Từ</th>
                            <th>Đến</th>
                            <th>Tỷ lệ</th>
                            <th>Ghi chú</th>
                            <th class='thAction'>...</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID='Literal_Table' runat='server'></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey_Detail" runat="server" Value="0" />
    <div class='modal' id='modal'>
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>×</span></button>
                    <h4 class='modal-title'>Tiêu đề</h4>
                </div>
                <div class='modal-body'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='form-horizontal'>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Từ</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="txt_FromMoney" placeholder="Nhập số" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Đến</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="txt_ToMoney" placeholder="Nhập số" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Tỷ lệ</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="txt_Rate" placeholder="Nhập số" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ghi chú</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="txt_Description" placeholder="Nhập text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default pull-left' data-dismiss='modal'>
                        Đóng</button>
                    <button type='button' class='btn btn-primary' id='btnSaveModal' data-dismiss='modal'>
                        Lưu</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/table-edits.min.js"></script>
    <script type="text/javascript" src="/template/jquery.number.min.js"></script>
    <script>
        $("[id$=txt_FromMoney]").number(true, 0);
        $("[id$=txt_ToMoney]").number(true, 0);
        $("[id$=txt_Rate]").number(true, 2);

        $("#addnew").click(function () {
            $("[id$=HID_AutoKey_Detail]").val(0);
            $("#modal").modal("show");
        });
        $("#btnSave").click(function () {
            $(location).attr('href', '/ADM/ProgressBoard_SaleNew.aspx');
        });

        $("#tableProgressive tr").on("click", "td:not(:last-child)", function () {
            var id = $(this).closest('tr').attr('id');
            console.log(id);
            $("[id$=HID_AutoKey_Detail]").val(id);
            $("#modal").modal("show");

            $.ajax({
                type: 'POST',
                url: 'ProgressBoard_SaleNew_Edit.aspx/LoadDetail',
                data: JSON.stringify({
                    'AutoKey': id
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {

                },
                success: function (msg) {
                    var obj = JSON.parse(msg.d);
                    $("[id$=txt_FromMoney]").val(obj.FromMoney);
                    $("[id$=txt_ToMoney]").val(obj.ToMoney);
                    $("[id$=txt_Rate]").val(obj.Rate);
                    $("[id$=txt_Description]").val(obj.Description);
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
        $('#tableProgressive tr').on('click', 'a[btn=delete]', function () {
            if (confirm('Bạn có chắc xóa')) {
                var AutoKey = $(this).closest('tr').attr('id');
                $.ajax({
                    type: 'POST',
                    url: 'ProgressBoard_SaleNew_Edit.aspx/DeleteDetail',
                    data: JSON.stringify({
                        'AutoKey': AutoKey,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d == 'OK') {
                            $('$tableProgressive tr[id=' + AutoKey + ']').remove();
                        }
                        else {
                            alert(msg.d);
                        }
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        });

        $("#btnSaveModal").click(function () {
            $.ajax({
                type: 'POST',
                url: 'ProgressBoard_SaleNew_Edit.aspx/SaveDetail',
                data: JSON.stringify({
                    'AutoKey': $("[id$=HID_AutoKey_Detail]").val(),
                    'ParentKey': $("[id$=HID_AutoKey]").val(),
                    'ProjectKey': $("[id$=HID_ProjectKey]").val(),
                    'FromMoney': $("[id$=txt_FromMoney]").val(),
                    'ToMoney': $("[id$=txt_ToMoney]").val(),
                    'Rate': $("[id$=txt_Rate]").val(),
                    'Description': $("[id$=txt_Description]").val()
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (!isNaN(msg.d)) {
                        location.reload();
                    }
                    else {
                        alert(msg.d);
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });

        $("#btnDelete").click(function () {
            if (confirm('Bạn có chắc xóa. Các thông tin kèm theo cũng sẽ mất !.')) {
                $.ajax({
                    type: 'POST',
                    url: 'ProgressBoard_SaleNew.aspx/DeleteParent',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    success: function (msg) {
                       $(location).attr('href', '/ADM/ProgressBoard_SaleNew.aspx');
                    },
                    complete: function () {

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        });
    </script>
</asp:Content>
