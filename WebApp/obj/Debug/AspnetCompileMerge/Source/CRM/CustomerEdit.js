﻿var CustomerKey = $("[id$=HID_CustomerKey]").val();
var Type = $("[id$=HID_CustomerType]").val();
$(function () {
    $("[custype='0']").hide();
    $("[custype='1']").hide();
    $("[custype=" + Type + "]").show();
    $(".select2").select2({ width: "100%" });

    //xoa khach
    $("#btnDelete").click(function () {
        if (CustomerKey > 0) {
            if (confirm("Bạn có chắc xóa thông tin")) {
                $.ajax({
                    type: "POST",
                    url: "/CRM/CustomerEdit.aspx/DeleteCustomer",
                    data: JSON.stringify({
                        "CustomerKey": CustomerKey,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $("#btnDelete").attr("disabled", "disabled");
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            window.location = "/CRM/CustomerList.aspx";
                        }
                    },
                    complete: function () {
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        else {
            Page.showNotiMessageInfo("Thông báo !", "Không có thông tin để xóa.");
        }
    });
    //luu khach
    $("#btnSaveCustomer").click(function () {
        var needKey = "";
        var need = "";
        var source = "";
        var status = "";
        var wantkey = "";
        var want = "";
        if ($("[id$=DDL_Status]").val() != null)
            status = $("[id$=DDL_Status]").val();
        if ($("[id$=DDL_Source]").val() != null)
            source = $("[id$=DDL_Source]").val();
        $("[id$=LB_Need] :selected").map(function (i, el) {
            wantkey += $(el).val() + ",";
            want += $(el).text() + ",";
        });

        $.ajax({
            type: "POST",
            url: "/CRM/CustomerEdit.aspx/SaveCustomer",
            data: JSON.stringify({
                "CustomerKey": CustomerKey,
                "CustomerName": $('[id$=txt_CustomerName]').val(),
                "Address3": $("[id$=txt_Address3]").val(),
                "Tax": $("[id$=txt_Tax]").val(),
                "Birthday": $('[id$=txt_Birthday]').val(),
                "CardID": $("[id$=txt_CardID]").val(),
                "CardDate": $("[id$=txt_CardDate]").val(),
                "CardPlace": $("[id$=CardPlace]").val(),
                "Source": source,
                "SourceNote": $('[id$=txt_SourceNote]').val(),
                "WantShow": want,
                "WantSearch": wantkey,
                "Status": status,
                "Description": $('[id$=txt_Description]').val(),
                "Phone1": $('[id$=txt_Phone1]').val(),
                "Phone2": $('[id$=txt_Phone2]').val(),
                "CompanyName": $('[id$=txt_CompanyName]').val(),
                "Address1": $('[id$=txt_Address1]').val(),
                "Address2": $('[id$=txt_Address2]').val(),
                "Email1": $('[id$=txt_Email1]').val(),
                "Email2": $('[id$=txt_Email2]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $("#btnSave").attr("disabled", "disabled");
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                }
                else {
                    window.location = "CustomerList.aspx";
                }
            },
            complete: function () {
                $("#btnSave").removeAttr("disabled");
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //Page.showPopupMessage("Lỗi thực hiện", "...")
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

    });
    //save allow
    $("#btnSaveShare").click(function () {
        $.ajax({
            type: "POST",
            url: "/CRM/CustomerEdit.aspx/SaveShare",
            data: JSON.stringify({
                "CustomerKey": CustomerKey,
                "Type": 'Customer',
                "EmployeeKey": $('[id$=DDL_Employee]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getShare();
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //del allow
    $("#tblShare").on("click", "a[btn='btnDelShare']", function (e) {
        var Key = $(this).closest('div').parent().parent().attr('id');
        delShare(Key);
    });
    //--------------------------
    //get want
    $("#tblWant tbody").on("click", "tr td:not(:last-child)", function () {
        var Key = $(this).closest('tr').attr('id');
        $("[id$=HID_RecordKey]").val(Key)
        getRecord();
    });
    //del want
    $("#tblWant").on("click", "a[btn='btnDelWant']", function (e) {
        if (confirm("Bạn có chắc xóa thông tin")) {
            var Key = $(this).closest('tr').attr('id');
            $.ajax({
                type: "POST",
                url: "/CRM/CustomerEdit.aspx/DeleteRecord",
                data: JSON.stringify({
                    "RecordKey": Key,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        Page.showPopupMessage("Lỗi !", msg.d.Message);
                    }
                    else {
                        getRecordList();
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //save want
    $("#btnSaveRecord").click(function () {
        var room = $("[id$=DDL_Bed]").val();
        var price = $("[id$=txt_Price]").val();
        var area = $("[id$=txt_Area]").val();
        var note = $("[id$=txt_Note]").val();

        var projectKey = "", project = "";
        var categoryKey = "", category = "";
        var furnitureKey = "", furniture = "";

        $("[id$=LB_Project] :selected").map(function (i, el) {
            projectKey += $(el).val() + ",";
            project += $(el).text() + ",";
        });
        $("[id$=LB_Category] :selected").map(function (i, el) {
            categoryKey += $(el).val() + ",";
            category += $(el).text() + ",";
        });
        $("[id$=LB_Furniture] :selected").map(function (i, el) {
            furnitureKey += $(el).val() + ",";
            furniture += $(el).text() + ",";
        });

        $.ajax({
            type: "POST",
            url: "/CRM/CustomerEdit.aspx/SaveRecord",
            data: JSON.stringify({
                "CustomerKey": CustomerKey,
                "RecordKey": $("[id$=HID_RecordKey]").val(),
                "Note": $("[id$=txt_Note]").val(),
                "ProjectKey": projectKey,
                "Project": project,
                "ProjectOther": $("[id$=txt_Other]").val(),
                "CategoryKey": categoryKey,
                "Category": category,
                "FurnitureKey": furnitureKey,
                "Furniture": furniture,
                "Room": $("[id$=DDL_Bed]").val(),
                "Price": $("[id$=txt_Price]").val(),
                "Area": $("[id$=txt_Area]").val(),
                "WantKey": $("[id$=DDL_Want]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                } else {
                    getRecordList();
                }
                $("[id$=HID_RecordKey]").val(0);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        $("[id$=HID_RecordKey]").val(0),
            Page.resetValue("mRecord");
    });
    //--------------------------
    //get care
    $("#tblCare tbody").on("click", "tr td:not(:last-child)", function () {
        var Key = $(this).closest('tr').attr('id');
        $("[id$=HID_CareKey]").val(Key)
        getCare();
    });
    //del care
    $("#tblCare").on("click", "a[btn='btnDelCare']", function (e) {
        if (confirm("Bạn có chắc xóa thông tin")) {
            var Key = $(this).closest('tr').attr('id');
            $.ajax({
                type: "POST",
                url: "/CRM/CustomerEdit.aspx/DeleteCare",
                data: JSON.stringify({
                    "AutoKey": Key,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        Page.showPopupMessage("Lỗi !", msg.d.Message);
                    }
                    else {
                        getCareList();
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //save care
    $("#btnSaveCare").click(function () {
        var Category = $("[id$=DDL_Status2]").val();
        if (Category == null || Category == undefined)
            Category = 328;

        $.ajax({
            type: "POST",
            url: "/CRM/CustomerEdit.aspx/SaveCare",
            data: JSON.stringify({
                "AutoKey": $("[id$=HID_CareKey]").val(),
                "CustomerKey": $("[id$=HID_CustomerKey]").val(),
                "CategoryKey": Category,
                "Content": $("[id$=txt_Care]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    Page.showPopupMessage("Lỗi !", msg.d.Message);
                } else {
                    getCareList();
                }
                $("[id$=HID_CareKey]").val(0);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        Page.resetValue("mRecord");
    });
});
function getCare() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerEdit.aspx/GetCare",
        data: JSON.stringify({
            'AutoKey': $("[id$=HID_CareKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $("[id$=txt_Care]").val(msg.d.Result);
            $('#mCare').modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getCareList() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerEdit.aspx/GetCareList",
        data: JSON.stringify({
            'CustomerKey': $("[id$=HID_CustomerKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $('#tblCare tbody').empty();
            $('#tblCare tbody').append($(msg.d));
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//----------------------------
function getRecord() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerEdit.aspx/GetRecord",
        data: JSON.stringify({
            'RecordKey': $("[id$=HID_RecordKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.RecordKey <= 0) {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
            }
            else {
                $('#mRecord').modal({
                    backdrop: true,
                    show: true
                });
                //$("[id$=DDL_Bed] option[value='" + msg.d.Room + "']").val()
                $("[id$=HID_RecordKey]").val(msg.d.RecordKey);
                $("[id$=DDL_Bed]").val(msg.d.Room).change();
                $("[id$=txt_Price]").val(msg.d.Price);
                $("[id$=txt_Note]").val(msg.d.Note);
                $("[id$=txt_Area]").val(msg.d.Aera);
                $("[id$=DDL_Want]").val(msg.d.Want).trigger("change");

                var projectkey = msg.d.ProjectKey;
                var project = msg.d.Project;
                var furniturekey = msg.d.FurnitureKey;
                var furniture = msg.d.Furniture;
                var categorykey = msg.d.CategoryKey;
                var category = msg.d.Category;

                $.each(projectkey.split(","), function (i, e) {
                    $("[id$=LB_Project] option[value='" + e + "']").prop("selected", true).change();
                });
                $.each(furniturekey.split(","), function (i, e) {
                    $("[id$=LB_Furniture] option[value='" + e + "']").prop("selected", true).change();
                });
                $.each(categorykey.split(","), function (i, e) {
                    $("[id$=LB_Category] option[value='" + e + "']").prop("selected", true).change();
                });
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getRecordList() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerEdit.aspx/GetRecordList",
        data: JSON.stringify({
            'CustomerKey': CustomerKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $('#tblWant tbody').empty();
            $('#tblWant tbody').append($(msg.d));
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getShare() {
    $('#tblShare').empty();
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerEdit.aspx/GetShare",
        data: JSON.stringify({
            'CustomerKey': CustomerKey,
            'Type': 'Customer',
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            for (var i = 0; i < msg.d.length; i++) {
                var html = "<div class='profile-info-row' id='" + msg.d[i].Value + "'>";
                html += "<div class='profile-info-name'>Chia thông tin cho </div><div class='profile-info-value'>" + msg.d[i].Text;
                html += "<div class='hidden-sm hidden-xs action-buttons pull-right'><a href='#' btn='btnDelShare' class='red'><i class='ace-icon fa fa-trash-o bigger-130'></i> Xóa</a></div>";
                html += "</div>";
                $('#tblShare').append(html);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function delShare(id) {
    $.ajax({
        type: 'POST',
        url: '/CRM/CustomerEdit.aspx/DeleteShare',
        data: JSON.stringify({
            'AutoKey': id
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != '') {
                Page.showPopupMessage("Lỗi xóa dữ liệu !", msg.d.Message);
            } else {
                getShare();
            }
        },
        complete: function () {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function validate() {
    $('input[required]').each(function (idx, item) {
        Page.checkError($(item));
    });
    $('select[required]').each(function (idx, item) {
        Page.checkSelect($(item));
    });
    if ($('div.error').length > 0) {
        return false;
    }
    return true;
}
function CheckPhone() {
    var val = $('[id$=txt_Phone1]').val();
    if (val.trim().length <= 0 && CustomerKey == 0) {
        alert("Bạn phải nhập SĐT");
    }
    else {
        $.ajax({
            type: "POST",
            url: "/CRM/CustomerEdit.aspx/CheckPhone",
            data: JSON.stringify({
                "Phone": $('[id$=txt_Phone1]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (msg) {
                if (!isNaN(msg.d.Result)) {
                    if (confirm("Số phone này đã có khách, bạn có muốn xem")) {
                        window.location = "/CRM/CustomerEdit.aspx?ID=" + msg.d.Result + "Type=" + msg.d.Result2;
                    }
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}