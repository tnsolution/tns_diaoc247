﻿$(function () {
    var Type = $("[id$=HID_CustomerType]").val();
    var Key = $("[id$=HID_CustomerKey]").val();

    $("[custype='0']").hide();
    $("[custype='1']").hide();
    $("[custype=" + Type + "]").show();
    var x = $('#leftbox').height();
    var x = x - 75;
    $('#scroll').ace_scroll({
        size: x,
    });
    $("#tblExtraData").tableHeadFixer();
    $(".select2").select2({ width: "100%" });
    //-----------------------------------------------------------------------------------End Lay Out

    $('#tblTrade tbody').on('click', 'tr', function () {
        var trid = $(this).closest('tr').attr('id');
        window.location = '/FNC/TradeView.aspx?ID=' + trid;
    });
    $('#tblMain tr.action td:not(:last-child)').on('click', function (e) {
        e.preventDefault();

        if ($(this).hasClass('indam')) {
            $(this).removeClass('indam');
            $(this).closest('tr').next().toggleClass('open')
        }
        else {
            $('#tblMain tbody tr').removeClass('open indam');

            $(this).closest('tr').next().toggleClass('open');
            if ($(this).closest('tr').hasClass('indam'))
                $(this).closest('tr').removeClass('indam');
            else
                $(this).closest('tr').addClass('indam');
        }
    });
    $('#tblExtraData tbody').on('click', 'tr', function () {
        var trid = $(this).closest('tr').attr('id');

        $.ajax({
            type: "POST",
            url: "/CRM/CustomerView.aspx/GetView",
            data: JSON.stringify({
                "CustomerKey": trid,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $("#tieude").text(msg.d.CustomerName);
                $("#hoten").text(msg.d.CustomerName);
                var text = "<i class='ace-icon fa fa-mobile bigger-180 blue'></i>&nbsp;" + msg.d.Phone1 + " <i class='ace-icon fa fa-mobile bigger-180 blue'></i>&nbsp;" + msg.d.Phone2;
                $("#sdt").empty().append($(text));
                $("#email").text(msg.d.Email1);
                $("#nguon").text(msg.d.Consent);
                $("#ghichu").text(msg.d.Description);
                $("#ngaysinh").text(msg.d.Birthday);
                $("#cmnd").text(msg.d.CardID);
                $("#ngaycap").text(msg.d.CardDate);
                $("#noicap").text(msg.d.CardPlace);
                $("#diachi1").text(msg.d.Address1);
                $("#diachi2").text(msg.d.Address2);
                if (msg.d.CategoryKey == 1) {
                    $("#tencongty").text(msg.d.CompanyName);
                    $("#diachi3").text(msg.d.Address3);
                    $("#masothue").text(msg.d.Tax);
                }

                $("[id$=HID_CustomerType]").val(msg.d.CategoryKey);
                $("[id$=HID_CustomerKey]").val(msg.d.CustomerKey);

                console.log(msg.d.CustomerKey);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showNotiMessageError("Lỗi,", "Vui lòng liên hệ Admin");
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        setTimeout(function () {
            //load quan tam
            $.ajax({
                type: "POST",
                url: "/CRM/CustomerView.aspx/GetConsent",
                data: JSON.stringify({
                    "Key": trid,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (msg) {
                    $("#quantam").empty();
                    $("#quantam").append($(msg.d));
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }, 1000);
        setTimeout(function () {
            //load giao dich
            $.ajax({
                type: "POST",
                url: "/CRM/CustomerView.aspx/GetTrade",
                data: JSON.stringify({
                    "Key": trid,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (msg) {
                    $("#giaodich").empty();
                    $("#giaodich").append($(msg.d.Result));

                    $("#tonggiaodich").empty();
                    $("#tonggiaodich").append(msg.d.Result2);
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }, 1000);
        setTimeout(function () { CheckRemind(trid); }, 1000)
    });
    //-----------------------------------------------------------------------------------End Table Action

    $("#btnExpand").on("click", function () {
        $(this).toggleClass("active");
        $(this).text($(this).text() == 'Mở rộng' ? 'Thu gọn' : 'Mở rộng');
        if ($(this).hasClass("active"))
            $('#tblMain tbody tr').removeClass('open indam');
        else {
            $('#tblMain tbody tr.action').addClass('indam');
            $('#tblMain tbody tr').nextUntil('.action').addClass('open');
        }
    });
    $("#btnEdit").click(function () {
        window.location = "CustomerEdit.aspx?ID=" + Page.getUrlParameter("ID") + "&Type=" + Page.getUrlParameter("Type");
    });
    $("#btnSearch").click(function () {
        var project = "", category = "";
        if ($("[id$=DDL_Project]").val() != null)
            project = $("[id$=DDL_Project] option:selected").text();
        if ($("[id$=DDL_Category]").val() != null)
            category = $("[id$=DDL_Category] option:selected").text();

        $.ajax({
            type: "POST",
            url: "/CRM/CustomerView.aspx/Search",
            data: JSON.stringify({
                "Name": $("[id$=txt_Name]").val(),
                "Phone": $("[id$=txt_Phone]").val(),
                "Project": project,
                "Category": category,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $("#tblExtraData tbody").empty();
                $("#tblExtraData tbody").append($(msg.d));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //-----------------------------------------------------------------------------------End Action button
    $("#btnExeReminder").hide();
    $("#btnExeReminder").click(function () {
        $.ajax({
            type: "POST",
            url: "/CRM/CustomerView.aspx/ExeReminder",
            data: JSON.stringify({
                "RemindKey": $("[id$=HID_RemindKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Result == 1) {
                    Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                    $("#btnExeReminder").hide();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#GetInfoCustomer").click(function () {
        var id = $("[id$=HID_CustomerKey]").val();
        console.log(id);
        GetInfoCustomer();
        $('#mInfo').modal({
            backdrop: true,
            show: true
        });
    });
    $("#GetInfoAddress").click(function () {
        var id = $("[id$=HID_CustomerKey]").val();
        console.log(id);
        GetInfoAddress();
        $('#mAddress').modal({
            backdrop: true,
            show: true
        });
    });
    $("#GetInfoShare").click(function () {
        $('#mShare').modal({
            backdrop: true,
            show: true
        });
    });
    $("#btnSaveInfo").click(function () {
        SaveInfo();
    });
    $("#btnSaveShare").click(function () {
        SaveShare();
    });
    $("#btnSaveAddress").click(function () {
        SaveAddress();
    });
    $("#btnSaveWant").click(function () { SaveWant(); });
    $("#btnSaveCare").click(function () { SaveCare(); });
    //----------------------------------------------------------------------------------End Main Action

    CheckRemind(Key);
});

function AddNewInfo(RecordKey) {
    $("#mCare").modal("show");
    $("[id$=HID_ConsentKey]").val(RecordKey);
}
//-----------------------------------
function SaveCare() {
    var Category = $("[id$=DDL_Status2]").val();
    if (Category == null || Category == undefined)
        Category = 328;
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/SaveCare",
        data: JSON.stringify({
            "CustomerKey": $("[id$=HID_CustomerKey]").val(),
            "ConsentKey": $("[id$=HID_ConsentKey]").val(),
            "CategoryKey": Category,
            "Content": $("[id$=txt_Care]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
            } else {
                var url = '/CRM/CustomerView.aspx?ID=' + $("[id$=HID_CustomerKey]").val() + '&Type=0';
                console.log(url);
                window.location.replace(url);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveWant() {
    var room = $("[id$=DDL_Bed]").val();
    var price = $("[id$=txt_Price]").val();
    var area = $("[id$=txt_Area]").val();
    var note = $("[id$=txt_Note]").val();

    var wantkey = $("[id$=DDL_Want]").val();
    var wantshow = $("[id$=DDL_Want] :selected").text();

    var projectKey = "", project = "";
    var categoryKey = "", category = "";
    var furnitureKey = "", furniture = "";

    $("[id$=LB_Project] :selected").map(function (i, el) {
        projectKey += $(el).val() + ",";
        project += $(el).text() + ",";
    });
    $("[id$=LB_Category] :selected").map(function (i, el) {
        categoryKey += $(el).val() + ",";
        category += $(el).text() + ",";
    });
    $("[id$=LB_Furniture] :selected").map(function (i, el) {
        furnitureKey += $(el).val() + ",";
        furniture += $(el).text() + ",";
    });

    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/SaveWant",
        data: JSON.stringify({
            "CustomerKey": $("[id$=HID_CustomerKey]").val(),
            "Note": $("[id$=txt_Note]").val(),
            "ProjectKey": projectKey,
            "Project": project,
            "ProjectOther": $("[id$=txt_Other]").val(),
            "CategoryKey": categoryKey,
            "Category": category,
            "FurnitureKey": furnitureKey,
            "Furniture": furniture,
            "Room": $("[id$=DDL_Bed]").val(),
            "Price": $("[id$=txt_Price]").val(),
            "Area": $("[id$=txt_Area]").val(),
            "WantKey": wantkey
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
            } else {
                location.reload();
            }

        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//-----------------------------------
function SaveInfo() {
    var source = "";
    var status = "";
    if ($("[id$=DDL_Status]").val() != null)
        status = $("[id$=DDL_Status]").val();
    if ($("[id$=DDL_Source]").val() != null)
        source = $("[id$=DDL_Source]").val();

    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/SaveCustomer",
        data: JSON.stringify({
            "CustomerKey": $('[id$=HID_CustomerKey]').val(),
            "CustomerName": $('[id$=txt_CustomerName]').val(),
            "Source": source,
            "SourceNote": $('[id$=txt_SourceNote]').val(),
            "Phone1": $('[id$=txt_Phone1]').val(),
            "Phone2": $('[id$=txt_Phone2]').val(),
            "Status": status,
            "Description": $('[id$=txt_Description]').val(),
            "Email1": $('[id$=txt_Email1]').val(),
            "Email2": $('[id$=txt_Email2]').val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message.length <= 0) {
                Page.showNotiMessageInfo("Đã cập nhật thành công !.");
                location.reload();
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo !.", xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function GetInfoCustomer() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/GetInfoCustomer",
        data: JSON.stringify({
            "CustomerKey": $("[id$=HID_CustomerKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $("[id$=txt_CustomerName]").val(msg.d.CustomerName);
            $("[id$=txt_Phone1]").val(msg.d.Phone1);
            $("[id$=txt_Phone2]").val(msg.d.Phone2);
            $("[id$=DDL_Source]").val(msg.d.SourceKey).trigger("change");
            $("[id$=txt_SourceNote]").val(msg.d.SourceNote);
            $("[id$=DDL_Status]").val(msg.d.StatusKey).trigger("change");
            $("[id$=txt_Description]").val(msg.d.Description);
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//-----------------------------------
function GetInfoAddress() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/GetInfoAddress",
        data: JSON.stringify({
            "CustomerKey": $("[id$=HID_CustomerKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $("[id$=txt_Birthday]").val(msg.d.Birthday);
            $("[id$=txt_CardID]").val(msg.d.CardID);
            $("[id$=txt_CardDate]").val(msg.d.CardDate);
            $("[id$=txt_CardPlace]").val(msg.d.CardPlace);
            $("[id$=txt_CompanyName]").val(msg.d.CompanyName);
            $("[id$=txt_Tax]").val(msg.d.Tax);
            $("[id$=txt_Address1]").val(msg.d.Address1);
            $("[id$=txt_Address2]").val(msg.d.Address2);
            $("[id$=txt_Address3]").val(msg.d.Address3);
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveAddress() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/SaveAddress",
        data: JSON.stringify({
            "CustomerKey": $('[id$=HID_CustomerKey]').val(),
            "Birthday": $('[id$=txt_Birthday]').val(),
            "CardID": $("[id$=txt_CardID]").val(),
            "CardDate": $("[id$=txt_CardDate]").val(),
            "CardPlace": $("[id$=CardPlace]").val(),
            "Address1": $('[id$=txt_Address1]').val(),
            "Address2": $('[id$=txt_Address2]').val(),
            "Address3": $("[id$=txt_Address3]").val(),
            "CompanyName": $('[id$=txt_CompanyName]').val(),
            "Tax": $("[id$=txt_Tax]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message.length <= 0) {
                Page.showNotiMessageInfo("Đã cập nhật thành công !.");
                location.reload();
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo !.", xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//-----------------------------------
function SaveShare() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/SaveShare",
        data: JSON.stringify({
            "CustomerKey": $("[id$=HID_CustomerKey]").val(),
            "TableName": 'Customer',
            "EmployeeKey": $('[id$=DDL_Employee]').val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message.length <= 0) {
                GetShare();
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Thông báo !.", xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function GetShare() {
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/GetShare",
        data: JSON.stringify({
            'CustomerKey': $("[id$=HID_CustomerKey]").val(),
            'TableName': 'Customer',
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#tblShare').empty();
        },
        success: function (msg) {
            for (var i = 0; i < msg.d.length; i++) {
                var html = "<div class='profile-info-row' id='" + msg.d[i].Value + "'>";
                html += "<div class='profile-info-name'>Chia sản phẩm cho </div><div class='profile-info-value'>" + msg.d[i].Text + "</div></div>";

                $('#tblShare').append(html);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//
function CheckRemind(CustomerKey) {
    //kiem tra khi load
    $.ajax({
        type: "POST",
        url: "/CRM/CustomerView.aspx/CheckReminder",
        data: JSON.stringify({
            "CustomerKey": CustomerKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                $("[id$=HID_RemindKey]").val(msg.d.Result);
                Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                $("#btnExeReminder").show();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}