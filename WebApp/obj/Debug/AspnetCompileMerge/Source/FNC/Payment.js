﻿var picker = new Pikaday({
    field: document.getElementById('txtTuNgay'),
    format: 'DD/MM/YYYY',
    onSelect: function (date) {
        $("[id$=HID_FromDate]").val(picker.toString());
        console.log(picker.toString());
    }
});
var picker2 = new Pikaday({
    field: document.getElementById('txtDenNgay'),
    format: 'DD/MM/YYYY',
    onSelect: function (date) {
        $("[id$=HID_ToDate]").val(picker2.toString());
        console.log(picker2.toString());
    }
});

$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(document).ready(function () {
    $("#ViewPrevious").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=HID_NextPrev]").val(-1);
        $("[id$=btnView]").trigger("click");
    });
    $("#ViewNext").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=HID_NextPrev]").val(1);
        $("[id$=btnView]").trigger("click");
    });
    $(".modal.aside").ace_aside();
    $(".select2").select2({ width: "100%" });
    $('input[moneyinput]').number(true, 0);
    $("[id$=txt_PercentVAT]").blur(function () {
        var vat = $('[id$=txt_PercentVAT]').val().replace(/,/g, '');
        var money = $('[id$=txt_PaymentMoney]').val().replace(/,/g, '');
        var amount = parseFloat(money) * parseFloat(vat) / 100 + parseFloat(money);
        $('[id$=txt_PaymentAmount]').val(Page.FormatMoney(amount));
    });
    $("[id$=txt_PaymentMoney]").blur(function () {
        var vat = $('[id$=txt_PercentVAT]').val().replace(/,/g, '');
        var money = $('[id$=txt_PaymentMoney]').val().replace(/,/g, '');
        var amount = parseFloat(money) * parseFloat(vat) / 100 + parseFloat(money);
        $('[id$=txt_PaymentAmount]').val(Page.FormatMoney(amount));
    });
    $("#txt_PaymentDate").val(Page.getCurrentDate());
    //----
    $("[id$=DDL_Department]").on('change', function (e) {
        $("[id$=HID_Department]").val(this.value);
    });
    $("#btnSearch").click(function () {
        var status = $("[id$=DDL_Status]").val();
        if (status == null)
            status = 0;

        var department = $("[id$=DDL_Department]").val();
        if (department == null)
            department = Page.getDepartment();
        var text = "Từ ngày: " + $("[id$=txtTuNgay]").val() + " đến ngày: " + $("[id$=txtDenNgay]").val();
        $("[id$=lbltime]").text(text);
        $.ajax({
            type: 'POST',
            url: '/FNC/Payment.aspx/Search',
            data: JSON.stringify({
                'FromDate': $("[id$=txtTuNgay]").val(),
                'ToDate': $("[id$=txtDenNgay]").val(),
                'Department': department,
                'Category': $("[id$=HID_Category]").val(),
                'Status':status
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $("#tblPayment tbody").empty();
                $("#tblPayment tbody").append($(msg.d.Result2));
                //$("#title").text("Thực chi tháng " + $("[id$=DDL_Month]").val());
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#tblPayment td")
        .not(".noclick")
        .on("click", function () {
            var trid = $(this).closest('tr').attr('id');
            $.ajax({
                type: 'POST',
                url: '/FNC/Payment.aspx/GetPayment',
                data: JSON.stringify({
                    'AutoKey': trid,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        alert(msg.d.Message);
                        return false;
                    }
                    $('#mPayment').modal('show');
                    $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                    $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                    $('[id$=DDL_Employee1]').val(msg.d.PaymentTo).trigger("change");
                    $('[id$=txt_PaymentDate]').val(msg.d.PaymentDate);
                    $('[id$=txt_PaymentContents]').val(msg.d.Contents);
                    $('[id$=txt_PaymentMoney]').val(msg.d.Money);
                    $('[id$=txt_PercentVAT]').val(msg.d.VAT);
                    $('[id$=txt_PaymentAmount]').val(msg.d.Amount);
                    $('[id$=txt_PaymentDescription]').val(msg.d.Description);
                    $('input[name="chkStatus"][value=' + msg.d.IsHasVAT + ']').prop('checked', true);
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                  
                }
            });
        });
    $("#tblPayment").on("click", ".show-details-btn", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('tr').next().toggleClass('open');
        $(this).find(ace.vars['.icon'])
            .toggleClass('fa-angle-double-down')
            .toggleClass('fa-angle-double-up');
    });
    //-----
    $("#btnSavePayment").on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/FNC/Payment.aspx/SavePayment',
            data: JSON.stringify({
                'AutoKey': $("[id$=HID_AutoKey]").val(),
                'Department': $('[id$=DDL_Department1]').val(),
                'Employee': $('[id$=DDL_Employee1]').val(),
                'Category': $("[id$=HID_Category]").val(),
                'Date': $('[id$=txt_PaymentDate]').val(),
                'Contents': $('[id$=txt_PaymentContents]').val(),
                'Description': $('[id$=txt_PaymentDescription]').val(),
                'Money': $('[id$=txt_PaymentMoney]').val(),
                'VAT': $('[id$=txt_PercentVAT]').val(),
                'Amount': $('[id$=txt_PaymentAmount]').val(),
                'Check': $('input[name="chkStatus"]:checked').val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d == 'OK') {
                    $("[id$=HID_AutoKey]").val(0);
                    location.href = '/FNC/Payment.aspx?Category=' + $("[id$=HID_Category]").val();
                }
                else {
                    alert(msg.d);
                }
            },
            complete: function () {
              
            },
            error: function (xhr, ajaxOptions, thrownError) {
              
            }
        });

    });
    $("#btnOK").click(function () {
        ApproveCheck(1)
    });
    $("#btnNotOK").click(function () {
        ApproveCheck(-1)
    });
    $("#btnDownload").click(function () {
        $("[id$=btnExport]").trigger("click");
    });
});