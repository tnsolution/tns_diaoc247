﻿var isFormValid = true;
var isUSD = 1;
jQuery(function ($) {
    $("[id$=txt_Message]").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#buttonSend").trigger("click");
        }
    });
    $('.iframe').colorbox({ iframe: true, width: "100%", height: "100%" });
});
$(document).ready(function () {
    $("[id$=DDLTruongPhong]").on('change', function (e) {
        var valueSelected = $(this).val();
        $("[id$=DDLDaiDienC]").val(valueSelected).trigger('change');
    });
    $("[id$=DDLPhong]").on('change', function (e) {
        var valueSelected = $(this).val();
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetEmployees",
            data: JSON.stringify({
                "DepartmentKey": valueSelected,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                var District = $("[id$=DDLNhanVien]");
                var ddl = $("[id$=DDLDaiDienC]");

                ddl.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                $.each(msg.d, function () {
                    var object = this;
                    if (object !== '') {
                        ddl.append($("<option></option>").val(object.Value).html(object.Text));
                    }
                });
                District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                $.each(msg.d, function () {
                    var object = this;
                    if (object !== '') {
                        District.append($("<option></option>").val(object.Value).html(object.Text));
                    }
                });
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetManager",
            data: JSON.stringify({
                "Department": valueSelected,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                var District = $("[id$=DDLTruongPhong]");
                District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                $.each(msg.d, function () {
                    var object = this;
                    if (object !== '') {
                        District.append($("<option></option>").val(object.Value).html(object.Text));
                    }
                });
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("[id$=DDLDonVi]").change(function () {
        isUSD = this.value;
        if (isUSD == 1)
            $("[loaitien]").text("VNĐ");
        if (isUSD == 2)
            $("[loaitien]").text("USD");
    });
    $("[id$=DDLLoaiCanHo]").on('change', function (e) {
        $("[id$=HID_LoaiCan]").val($("[id$=DDLLoaiCanHo] option:selected").text());
    });
    $("[id$=DDLDuAn]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetCategoryAsset",
                data: JSON.stringify({
                    "ProjectKey": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (msg) {
                    var District = $("[id$=DDLLoaiCanHo]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //--action--
    //$("#btnCopy").click(function () {

    //});
    $("#btnSave").click(function () {
        //if (!checkSave()) {
        //    return false;
        //}

        $('.se-pre-con').fadeIn('slow');
        var total = $('#tblBangThanhToan tbody tr').length;
        var row = "";
        var num = 0;
        $('#tblBangThanhToan tbody tr').each(function () {
            num++;
            if (num < total)
                row += ""
                    + $(this).find('td:eq(0)').text() + ":"
                    + $(this).find('td:eq(1)').text() + ":"
                    + $(this).find('td:eq(2)').text() + ":"
                    + $(this).find('td:eq(3)').text() + ":"
                    + $(this).find('td:eq(4)').text() + ":"
                    + $(this).find('td:eq(5)').text() + ";";
            else
                row += ""
                    + $(this).find('td:eq(0)').text() + ":"
                    + $(this).find('td:eq(1)').text() + ":"
                    + $(this).find('td:eq(2)').text() + ":"
                    + $(this).find('td:eq(3)').text() + ":"
                    + $(this).find('td:eq(4)').text() + ":"
                    + $(this).find('td:eq(5)').text();

        });
        $("[id$=HID_BangThanhToan]").val(row);

        $("[id$=btnTriggerSave]").trigger("click");
    });
    $("#btnIn").click(function () {        
        printDiv('printableArea');        
    });
    $("#btnApprove").click(function () {
        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit1.aspx/Approve",
            data: JSON.stringify({
                "TicketKey": $("[id$=HID_TicketKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                location.reload();
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnNoCheck").click(function () {
        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit1.aspx/NotApprove",
            data: JSON.stringify({
                "TicketKey": $("[id$=HID_TicketKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                location.reload();
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnSendTrade").click(function () {
        if (!checkSave()) {
            return false;
        }

        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit1.aspx/SendTrade",
            data: JSON.stringify({
                "TicketKey": $("[id$=HID_TicketKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                window.location = "/FNC/TradeEdit.aspx?ID=" + msg.d.Result;
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btndownload").click(function () {
        if ($("[id$=DDLFile]").val() == null) {
            alert("Bạn phải chọn tập tin cần xử lý");
            return false;
        }
        else {
            if (!checkSave()) {
                return false;
            }

            $("[id$=btnWord]").trigger("click");
        }
    });
    $("#btnSendApprove").click(function () {
        if (!checkSave()) {
            return false;
        }

        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit1.aspx/SendApprove",
            data: JSON.stringify({
                "TicketKey": $("[id$=HID_TicketKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                location.reload();
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("[id$=btnWord]").click(function () {
        if (!checkSave()) {
            return false;
        }

        var total = $('#tblBangThanhToan tbody tr').length;
        var row = "";
        var num = 0;
        $('#tblBangThanhToan tbody tr').each(function () {
            num++;
            if (num < total)
                row += ""
                    + $(this).find('td:eq(0)').text() + ":"
                    + $(this).find('td:eq(1)').text() + ":"
                    + $(this).find('td:eq(2)').text() + ":"
                    + $(this).find('td:eq(3)').text() + ":"
                    + $(this).find('td:eq(4)').text() + ":"
                    + $(this).find('td:eq(5)').text() + ";";
            else
                row += ""
                    + $(this).find('td:eq(0)').text() + ":"
                    + $(this).find('td:eq(1)').text() + ":"
                    + $(this).find('td:eq(2)').text() + ":"
                    + $(this).find('td:eq(3)').text() + ":"
                    + $(this).find('td:eq(4)').text() + ":"
                    + $(this).find('td:eq(5)').text();
        });
        $("[id$=HID_BangThanhToan]").val(row);
    });
    //----------

    $("input[moneyinput]").number(true, 0);
    $(".select2").select2({ width: "100%" });
    $("[role='datepicker']").datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        startDate: '01/01/1900',
        todayHighlight: true
    });

    $("[id$=txtGiaChuyenNhuong]").on("blur", function () {
        var value = parseFloat($(this).val().replace(/,/g, ''));
        console.log(value);
        if (isUSD == 1)
            $("[id$=giachuyennhuongbangchu]").text(capitalize(DOCSO.doc(value)));
        else
            $("[id$=giachuyennhuongbangchu]").text(capitalize(toWords(value)) + "USD");
    });
    $("[id$=txtSoTienThanhToan]").on("blur", function () {
        var value = parseFloat($(this).val().replace(/,/g, ''));
        if (isUSD == 1)
            $("[id$=sotienthanhtoanbangchu]").text("Bằng chữ:" + capitalize(DOCSO.doc(value)));
        else
            $("[id$=sotienthanhtoanbangchu]").text("Bằng chữ:" + capitalize(toWords(value)) + "USD");
    });
    $("[id$=txtSoTienNhanSo]").on("blur", function () {
        var value = parseFloat($(this).val().replace(/,/g, ''));
        if (isUSD == 1)
            $("[id$=sotiennhansobangchu]").text("Bằng chữ:" + capitalize(DOCSO.doc(value)));
        else
            $("[id$=sotiennhansobangchu]").text("Bằng chữ:" + capitalize(toWords(value)) + "USD");
    });
    $("[id$=txtPhiC]").on("blur", function () {
        var value = parseFloat($(this).val().replace(/,/g, ''));
        if (isUSD == 1)
            $("[id$=phicbangchu]").text(capitalize(DOCSO.doc(value)));
        else
            $("[id$=phicbangchu]").text(capitalize(toWords(value)) + "USD");
    });

    $("#AddRow").click(function () {
        var num = $('#tblBangThanhToan tbody tr').length;
        num = num + 1;
        var row = '<tr id=' + num + '><td tabindex=1>Đặt cọc đợt ' + (num) + '</td><td tabindex=1>&nbsp;</td><td tabindex=1 onclick="addInput(this)">&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1>&nbsp;</td><td tabindex=1></td><td tabindex=1 class=edit-disabled><span btn="btnDelete" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i></span></td></tr>';
        $('#tblBangThanhToan tbody').append(row);
    });
    $("#tblFile tr").on("click", "[btn='btnDeleteFile']", function (e) {
        var Key = $(this).closest('tr').attr('id');
        $("tr[id=" + Key + "]").remove();
        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit1.aspx/DeleteFile",
            data: JSON.stringify({
                "FileKey": Key,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                //location.reload();
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#tblBangThanhToan").on("click", "[btn='btnDelete']", function (e) {
        var Key = $(this).closest('tr').attr('id');
        $("tr[id=" + Key + "]").remove();
    });
    $("#tblBangThanhToan").editableTableWidget()
        .focus(function () {
            $(this).select();
        });
});

function checkSave() {
    var isFormValid = true;
    $("[require]").not(".noneed").each(function () {
        isFormValid = true;
        if ($.trim($(this).val()).length == 0) {
            $(this).addClass("highlight");
            isFormValid = false;
            $(this).focus();
            return false;
        }
        else {
            $(this).removeClass("highlight");
            isFormValid = true;
        }
    });
    if (!isFormValid) {
        alert("Bạn phải nhập đủ các thông tin !.");
        return false;
    }
    return true;
}
function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25 + o.scrollHeight) + "px";
}
function capitalize(s) {
    return s[0].toUpperCase() + s.slice(1);
}
function closeInput(elm) {
    var td = elm.parentNode;
    var value = elm.value;
    td.removeChild(elm);
    td.innerHTML = value;

    value = parseFloat(value.replace(/,/g, ''));
    if (isUSD == 1) {
        $(td).next('td').text('VNĐ');
        $(td).next('td').next('td').
            text(capitalize(DOCSO.doc(value)));
    }
    else {
        $(td).next('td').text('USD');
        $(td).next('td').next('td').
            text(capitalize(toWords(value)) + "USD");
    }
}

function addInput(elm) {
    if (elm.getElementsByTagName('input').length > 0) return;

    var value = elm.innerHTML;
    elm.innerHTML = '';

    var input = document.createElement('input');
    input.style.setProperty("width", "100%");
    input.setAttribute('type', 'text');
    input.setAttribute('value', value);
    input.setAttribute('onBlur', 'closeInput(this)');
    elm.appendChild(input);
    $(input).number(true, 0);
    input.focus();
}
// American Numbering System
var th = ['', 'thousand', 'million', 'billion', 'trillion'];
var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
function toWords(s) {
    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');

}