﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="DepartmentList.aspx.cs" Inherits="WebApp.HRM.DepartmentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap-editable.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Các phòng ban
                <span class="tools pull-right">
                    <a href="DepartmentEdit.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <asp:HiddenField ID="HID_Type" runat="server" Value="1" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/assets/js/bootstrap-editable.min.js"></script>
    <script src="/template/ace-master/assets/js/ace-editable.min.js"></script>
    <script>
        $(document).ready(function () {

            $.fn.editable.defaults.mode = 'inline';
            $.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
            $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>' +
                '<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';

            $("#tblData tbody tr td:not(:last-child)").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "DepartmentEdit.aspx?ID=" + trid;
            });

            $('.editable').editable({
                type: 'spinner',
                name: 'rank',
                spinner: {
                    min: 0,
                    max: 99,
                    step: 1,
                    on_sides: true,
                    nativeUI: true//if true and browser support input[type=number], native browser control will be used
                },
            });
            $('.editable').on('save', function (e, params) {
                var no = params.newValue;
                var id = $(this).closest('tr').attr('id');
                $.ajax({
                    type: "POST",
                    url: "DepartmentList.aspx/ChangeRank",
                    data: JSON.stringify({
                        "DepartmentKey": id,
                        "Rank": no
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (msg) {
                        if (msg.d.Message != "") {
                            Page.showPopupMessage("Lỗi !", msg.d.Message);
                        }
                        else {
                            window.location = "DepartmentList.aspx";
                            //Page.showNotiMessageInfo("Thông báo !", "Đã xóa thông tin thành công.");
                            //$("input, select").val("");
                        }
                    },
                    complete: function () {
                        //$(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
    </script>
</asp:Content>
