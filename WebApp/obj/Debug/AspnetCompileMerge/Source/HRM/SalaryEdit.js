﻿var _Thucnhan = 0;
var _Lan1 = 0;
var _Lan2 = 0;
var tran_isDevined = 0;

$(function () {
    $("[id$=chkDevined]").change(function () {
        if ($(this).is(':checked')) {
            tran_isDevined = 1;

        }
        else {
            tran_isDevined = 0;

        }
        console.log(tran_isDevined);
    });
    $("[id$=DDL_Employee]").change(function () {
        var d = new Date(),
            n = d.getMonth() + 1,
            y = d.getFullYear();
        var key = this.value;
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/CheckEmployee',
            data: JSON.stringify({
                "sEmployee": Page.getEmployee(),
                "Month": n,
                "Year": y,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (r) {
                if (r.d != "0") {
                    if (confirm("Nhân viên này đã có bảng lương rồi !."))
                        window.location.href = "/HRM/SalaryEdit.aspx?id=" + r.d;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            },
            complete: function () {

            }
        });
    });
    $("img").click(function () {
        var id = $(this).attr('id');
        var status = $(this).attr('action');
        if (status != 1 || status != 2) {
            if (id == Page.getEmployee()) {
                Approve(id, status);
            }
        }
    });
    $("#btnOK").click(function () {
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/Accepted',
            data: JSON.stringify({
                "sEmployee": Page.getEmployee(),
                "sAutoKey": $("[id$=HID_AutoKey]").val()
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d == "") {
                    window.location.href = "/HRM/SalaryList.aspx";
                }
                else {
                    Page.showNotiMessageError("Thông báo", r.d);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });
    $("#btnProcess").click(function () {
        var employee = $("[id$=DDL_Employee]").val();
        var fromdate = $("[id$=txtFromDate]").val();
        var todate = $("[id$=txtToDate]").val();
        var thuong = $("[id$=txt13]").val();
        var lan = $("[id$=DDLTag]").val();
        var thue = $("[id$=txt9]").val();
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/Process',
            data: JSON.stringify({
                "sEmployee": employee,
                "sFromDate": fromdate,
                "sToDate": todate,
                "sthuethunhap": thue,
                "sthuong": thuong,
                "slan": lan,
                "chia": tran_isDevined
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d.Message != "") {
                    Page.showNotiMessageInfo("Thông báo", r.d.Message);
                }
                if (r.d.AutoKey != 0) {
                    Page.showNotiMessageInfo("Thông báo", "Đã tính lương cho thông tin này rồi !.");
                }
                $("#bangtinh").empty();
                $("#bangtinh").append($(r.d.TableDetail));
                $("[id$=txt1]").val(r.d.Param1);
                $("[id$=txt2]").val(r.d.Param2);
                $("[id$=txt3]").val(r.d.Param3);
                $("[id$=txt4]").val(r.d.Param4);
                $("[id$=txt51]").val(r.d.Param51);
                $("[id$=txt6]").val(r.d.Param6);
                $("[id$=txt7]").val(r.d.Param7);
                $("[id$=txt8]").val(r.d.Param8);
                $("[id$=txt9]").val(r.d.Param9);
                $("[id$=txt10]").val(r.d.Param10);
                $("[id$=txt11]").val(r.d.Param11);
                $("[id$=txt12]").val(r.d.Param12);
                $("[id$=txt14]").val(r.d.Param14);
                $("[id$=HID_Tong]").val(r.d.Note1 + r.d.Note2 + r.d.Note3);

                _Thucnhan = r.d.Param10;
                _Lan1 = r.d.Param11;
                _Lan2 = r.d.Param12;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            }
        });
    });
    $("#btnSave").click(function () {
        var employee = $("[id$=DDL_Employee]").val();
        var fromdate = $("[id$=txtFromDate]").val();
        var todate = $("[id$=txtToDate]").val();
        var description = $("[id$=txtdescription]").val();
        var param1 = $("[id$=txt1]").val();
        var param2 = $("[id$=txt2]").val();
        var param3 = $("[id$=txt3]").val();
        var param4 = $("[id$=txt4]").val();
        var param5 = $("[id$=txt5]").val();
        var param51 = $("[id$=txt51]").val();
        var param6 = $("[id$=txt6]").val();
        var param7 = $("[id$=txt7]").val();
        var param8 = $("[id$=txt8]").val();
        var param9 = $("[id$=txt9]").val();
        var param10 = $("[id$=txt10]").val();
        var param11 = $("[id$=txt11]").val();
        var param12 = $("[id$=txt12]").val();
        var param13 = $("[id$=txt13]").val();
        var param14 = $("[id$=txt14]").val();
        var note = $("[id$=txtNote1]").val();

        var total = $('#tblCommission tbody tr:not(.tong)').length;
        var row = "";
        var rownew = "";
        var num = 0;
        if (total != undefined && total > 0) {
            $('#tblCommission tbody tr:not(.tong)').each(function () {
                num++;
                if (num < total)
                    row +=
                        $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text() + ":"
                        + $(this).find('td:eq(7)').text() + ":"
                        + $(this).attr("tradekey") + ":"
                        + $(this).attr("type") + ":"
                        + $(this).attr("source") + ";";
                else
                    row +=
                        $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text() + ":"
                        + $(this).find('td:eq(7)').text() + ":"
                        + $(this).attr("tradekey") + ":"
                        + $(this).attr("type") + ":"
                        + $(this).attr("source");
            });
        }
        num = 0;
        total = $('#tblCommissionNew tbody tr:not(.tong)').length;

        if (total != undefined && total > 0) {
            $('#tblCommissionNew tbody tr:not(.tong)').each(function () {
                num++;
                if (num < total) {                    
                    rownew +=
                        $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text() + ":"
                        + $(this).find('td:eq(7)').text() + ":"
                        + $(this).attr("tradekey") + ":"
                        + $(this).attr("type") + ":"
                        + $(this).attr("source") + ";";
                }
                if (num == total) {
                    rownew +=
                        $(this).find('td:eq(0)').text() + ":"
                        + $(this).find('td:eq(1)').text() + ":"
                        + $(this).find('td:eq(2)').text() + ":"
                        + $(this).find('td:eq(3)').text() + ":"
                        + $(this).find('td:eq(4)').text() + ":"
                        + $(this).find('td:eq(5)').text() + ":"
                        + $(this).find('td:eq(6)').text() + ":"
                        + $(this).find('td:eq(7)').text() + ":"
                        + $(this).attr("tradekey") + ":"
                        + $(this).attr("type") + ":"
                        + $(this).attr("source");
                }               
            });
        }        
        $.ajax({
            type: 'POST',
            url: '/HRM/SalaryEdit.aspx/Save',
            data: JSON.stringify({
                "sEmployee": employee,
                "sFromDate": fromdate,
                "sToDate": todate,
                "sDescription": description,
                "sTable": row,
                "Param1": param1,
                "Param2": param2,
                "Param3": param3,
                "Param4": param4,
                "Param5": param5,
                "Param51": param51,
                "Param6": param6,
                "Param7": param7,
                "Param8": param8,
                "Param9": param9,
                "Param10": param10,
                "Param11": param11,
                "Param12": param12,
                "Param13": param13,
                "Param14": param14,
                "Tag": $("[id$=DDLTag]").val(),
                "NoteBonus": note,
                "sTableNew": rownew,
                "chia": tran_isDevined
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (isNaN(r.d)) {
                    Page.showNotiMessageError("Lỗi", r.d);
                } else {
                    location.href = "/HRM/SalaryEdit.aspx?ID=" + r.d;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.se-pre-con').fadeOut('slow');
            },
            complete: function () {
            }
        });
    });
    $("#btnSend").click(function () {
        var key = $("[id$=HID_AutoKey]").val();
        if (key != 0) {
            $.ajax({
                type: 'POST',
                url: '/HRM/SalaryEdit.aspx/SendMessage',
                data: JSON.stringify({
                    "sAutoKey": $("[id$=HID_AutoKey]").val()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (r) {
                    if (r.d == "") {
                        window.location.href = "/HRM/SalaryList.aspx";
                    }
                    else {
                        Page.showNotiMessageError("Thông báo", r.d);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                }
            });
        }
        else {
            Page.showNotiMessageInfo("Không có thông tin để gửi !.");
        }
    });
    $("input[moneyinput]").number(true, 0);
    $(".select2").select2({ width: "100%" });

    $("[id$=DDLTag]").change(function () {
        Cal();
    });
    $("input[isCal]").on('blur', function (e) {
        Cal();
    });
    //$("[id$=txt8]").val($("#tblCommission #hieuqua").text());
});
function Cal() {
    var thue = parseFloat(Page.RemoveComma($("[id$=txt9]").val()));
    var thuong = parseFloat(Page.RemoveComma($("[id$=txt13]").val()));
    var lan = $("[id$=DDLTag]").val();
    var thucnhan = parseFloat(Page.RemoveComma(_Thucnhan)) + thuong - thue;

    $("[id$=txt10]").val(Page.FormatMoney(thucnhan));

    if (lan === "1") {
        var textlan1 = parseFloat(Page.RemoveComma(_Lan1));
        textlan1 = textlan1 + thuong;
        var textlan2 = thucnhan - textlan1;

        $("[id$=txt11]").val(Page.FormatMoney(textlan1));
        $("[id$=txt12]").val(Page.FormatMoney(textlan2));
    }
    else {
        textlan1 = parseFloat(Page.RemoveComma(_Lan1));
        textlan2 = thucnhan - textlan1;

        $("[id$=txt11]").val(Page.FormatMoney(textlan1));
        $("[id$=txt12]").val(Page.FormatMoney(textlan2));
    }
}
function Approve(id, status) {
    $.ajax({
        type: "POST",
        url: "/HRM/SalaryEdit.aspx/Approve",
        data: JSON.stringify({
            "AutoKey": $('[id$=HID_AutoKey]').val(),
            "Employee": id,
            "Action": status,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (msg) {
            if (msg.d.Result == "ERROR") {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
                $(".se-pre-con").fadeOut("slow");
            }
            else {
                location.reload();
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
            $(".se-pre-con").fadeOut("slow");
        }
    });
}