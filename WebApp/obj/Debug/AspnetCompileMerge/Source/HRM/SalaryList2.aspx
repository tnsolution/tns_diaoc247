﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="SalaryList2.aspx.cs" Inherits="WebApp.HRM.SalaryList2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Bảng lương
                <small>
                    <asp:Label ID="lbltime" runat="server" Text=".."></asp:Label>
                </small>
                <asp:Literal ID="LitButton" runat="server"></asp:Literal>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12" id="divtable">
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ngày tháng năm</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtFromDate" placeholder="Từ " />
                        </div>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtToDate" placeholder="Đến " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Phòng, nhân viên</label>
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Employee2" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>

                    <a href="#" id="btnDownload" class="btn btn-primary pull-right">
                        <i class="ace-icon fa fa-file-excel-o"></i>
                        Excel
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_Employee" runat="server" />
    <asp:HiddenField ID="HID_Department" runat="server" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script src="../table2excel.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txtFromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txtToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            Date.prototype.ddmmyyyy = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
            };

            $("button[btnSend]").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                var employee = $(this).closest('tr').attr('employee');

                $.ajax({
                    type: 'POST',
                    url: '/HRM/SalaryList2.aspx/SendMessage',
                    data: JSON.stringify({
                        "sAutoKey": trid,
                        "sEmployee": employee
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {

                    },
                    success: function (r) {
                        if (r.d == "OK") {
                            Page.showNotiMessageInfo("Thông báo", "Đã gửi");
                            location.reload(true);
                        }
                        else {
                            Page.showNotiMessageError("Thông báo", r.d);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    },
                    complete: function () {

                    }
                });
            });

            $("[id$=txtFromDate]").val(firstDay.ddmmyyyy());
            $("[id$=txtToDate]").val(lastDay.ddmmyyyy());
            $("[id$=DDL_Department]").on('change', function (e) {
                var valueSelected = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        "DepartmentKey": valueSelected,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee2]");
                        District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                        $.each(msg.d, function () {
                            var object = this;
                            if (object !== '') {
                                District.append($("<option></option>").val(object.Value).html(object.Text));
                            }
                        });
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $('.modal.aside').ace_aside();
            $(".select2").select2({ width: "100%" });
            $("#ViewPrevious").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $('.se-pre-con').fadeIn('slow');
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $("#tblData").on("click", "tr", function () {
                var trid = $(this).closest('tr').attr('id');
                location.href = '/HRM/SalaryEdit.aspx?ID=' + trid;
            });
            $("#tblData").on("click", "td:last-child", function (e) {
                e.stopPropagation();
            });
            $("#btnSearch").click(function () {

                var employee = $("[id$=DDL_Employee2]").val();
                var department = $("[id$=DDL_Department]").val();
                var fromdate = $("[id$=txtFromDate]").val();
                var todate = $("[id$=txtToDate]").val();

                $.ajax({
                    type: "POST",
                    url: "/HRM/SalaryList2.aspx/Search",
                    data: JSON.stringify({
                        "Department": department,
                        "Employee": employee,
                        "FromDate": fromdate,
                        "ToDate": todate,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#tblData').empty();
                        $('#tblData').append($(msg.d.Result));
                        $("[id$=lbltime]").text("Từ ngày " + $("[id$=txtFromDate]").val() + " đến ngày " + $("[id$=txtToDate]").val() + " tổng " + msg.d.Result2);

                        $("[id$=HID_Department]").val(department);
                        $("[id$=HID_Employee]").val(employee);

                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $("#btnDownload").click(function () {
                $("#tblData").table2excel({
                    exclude: ".btnclass",
                    filename: "luong.xls"
                });
            });
        });
    </script>
</asp:Content>
