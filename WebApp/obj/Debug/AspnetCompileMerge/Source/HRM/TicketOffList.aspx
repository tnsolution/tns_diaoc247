﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="TicketOffList.aspx.cs" Inherits="WebApp.HRM.TicketOffList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .td5 {
            width: 5%;
        }

        .td15 {
            width: 15%;
        }

        .td20 {
            width: 20%;
        }

        .gachngang {
            text-decoration: line-through !important;
        }
    </style>
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Danh sách đơn xin phép
                <span class="tools pull-right">
                    <a href="TicketOffEdit.aspx" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
                <span class="pull-right action-buttons">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table2" runat="server"></asp:Literal>
        </div>
    </div>
    <div id="left-menu" class="modal aside" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm kiếm
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ngày tháng năm</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtFromDate" placeholder="Từ " />
                        </div>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right"
                                id="txtToDate" placeholder="Đến " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Phòng, nhân viên</label>
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Employee" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                        <label>Tình trạng</label>
                        <asp:DropDownList ID="DDL_Status" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="-1" Text="--Tất cả--" Selected="False">                                
                            </asp:ListItem>
                            <asp:ListItem Value="0">Chưa xử lý</asp:ListItem>
                            <asp:ListItem Value="1">Duyệt</asp:ListItem>
                            <asp:ListItem Value="2">Không duyệt</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DDL_Confirm" runat="server" class="select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="-1" Text="--Tất cả--" Selected="False"></asp:ListItem>
                            <asp:ListItem Value="0">Chưa xác nhận</asp:ListItem>
                            <asp:ListItem Value="1">Đã xác nhận</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm
                    </a>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script>
        var picker = new Pikaday({
            field: document.getElementById('txtFromDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        var picker2 = new Pikaday({
            field: document.getElementById('txtToDate'),
            format: 'DD/MM/YYYY',
            onSelect: function (date) {
            }
        });
        $(document).on('click', '.modal-backdrop.in', function (e) {
            //in ajax mode, remove before leaving page
            $('#closeLeft').trigger("click");
            //$(".modal-backdrop.in").remove();           
        });
        $(document).ready(function () {
            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            Date.prototype.ddmmyyyy = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
            };

            $("[id$=txtFromDate]").val(firstDay.ddmmyyyy());
            $("[id$=txtToDate]").val(lastDay.ddmmyyyy());

            $("#ViewPrevious").click(function () {
                $("[id$=HID_NextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=HID_NextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });

            $(".modal.aside").ace_aside();
            $(".select2").select2({ width: "100%" });

            $("#tblData tbody").on("click", "tr td:not(:last-child)", function () {
                $('.se-pre-con').fadeIn('slow');
                var trid = $(this).closest('tr').attr('id');
                window.location = "TicketOffEdit.aspx?ID=" + trid;
            });
            $("#tblData1 tbody").on("click", "tr td:not(:last-child)", function () {
                $('.se-pre-con').fadeIn('slow');
                var trid = $(this).closest('tr').attr('id');
                window.location = "TicketOffEdit.aspx?ID=" + trid;
            });

            $("[id$=DDL_Department]").on('change', function (e) {
                var valueSelected = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/Ajax.aspx/GetEmployees",
                    data: JSON.stringify({
                        "DepartmentKey": valueSelected,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        var District = $("[id$=DDL_Employee]");
                        District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Chọn nhân viên--</option>');
                        $.each(msg.d, function () {
                            var object = this;
                            if (object !== '') {
                                District.append($("<option></option>").val(object.Value).html(object.Text));
                            }
                        });
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });

            $("#btnSearch").click(function () {

                var employee = $("[id$=DDL_Employee]").val();
                var department = $("[id$=DDL_Department]").val();
                var fromdate = $("[id$=txtFromDate]").val();
                var todate = $("[id$=txtToDate]").val();
                var status = $("[id$=DDL_Status]").val();
                var confirm = $("[id$=DDL_Confirm]").val();
                $.ajax({
                    type: "POST",
                    url: "/HRM/TicketOffList.aspx/Search",
                    data: JSON.stringify({
                        "Department": department,
                        "Employee": employee,
                        "FromDate": fromdate,
                        "ToDate": todate,
                        "Status": status,
                        "Confirm": confirm
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        $('#tblData tbody').empty();
                        $('#tblData tbody').append($(msg.d));

                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
        });
        function ApproveCheck(ticketkey, employeekey, object) {
            $.ajax({
                type: 'POST',
                url: '/HRM/TicketOffList.aspx/Approve',
                data: JSON.stringify({
                    'TicketKey': ticketkey,
                    'EmployeeKey': employeekey
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    if (msg.d.Result == "ERROR") {
                        Page.showNotiMessageError("Thông báo !", msg.d.Message);
                    }
                    else {
                        Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                        $(object).attr('src', '../template/custom-image/true.png');
                    }
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    </script>
</asp:Content>
