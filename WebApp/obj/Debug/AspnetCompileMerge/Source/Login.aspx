﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApp.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/ace-rtl.min.css" />
    <style>
        .login-layout {
            background-color: white !important;
        }

            .login-layout .widget-box {
                padding: 1px !important;
            }
    </style>
</head>
<body class="login-layout">
    <div class="se-pre-con"></div>
    <form id="form1" runat="server">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="space-6"></div>
                            <div class="center">
                                <img src="http://crm.diaoc247.vn/_Resource/Img/Logo.png" style="width: 70%" alt="">
                            </div>
                            <div class="space-6"></div>
                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Đăng nhập
                                            </h4>
                                            <div class="space-6"></div>
                                            <div>
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="Username" id="txt_UserName" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Password" id="txt_Password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>
                                                    <div class="space"></div>
                                                    <div class="clearfix">
                                                        <label class="inline">
                                                            <input type="checkbox" class="ace" id="chkRemember" value="1" />
                                                            <span class="lbl">Nhớ mật khẩu</span>
                                                        </label>
                                                        <button type="button" class="width-35 pull-right btn btn-sm btn-primary" id="btnSubmit">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Login</span>
                                                        </button>
                                                    </div>
                                                    <div class="space-4"></div>
                                                    <h5 class="header center bigger">iControl Co., Ltd</h5>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HID_User" runat="server" />
        <asp:HiddenField ID="HID_Pass" runat="server" />
        <script src="/template/ace-master/assets/js/jquery-2.1.4.min.js"></script>
        <script src="/Cookie.js"></script>
        <script>
            window.mobilecheck = function () {
                var check = false;
                (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
                return check;
            };

            var error_obj = null;
            var error_prefix = 'Phải nhập ';
            var error_message = '<div class="text-danger error">{msg}</div>';
            $(function () {
                if (check) {
                    window.location.replace("http://m.crm.diaoc247.vn");
                }

                Cookies.remove('ViewSpec');
                Cookies.remove('ViewDepart');

                var user = $("[id$=HID_User]").val();
                var pass = $("[id$=HID_Pass]").val();
                var check = 0;
                $("#chkRemember").change(function () {
                    if ($(this).prop('checked'))
                        check = 1;
                    else
                        check = 0;
                });
                if (user != undefined && user != "" &&
                    pass != undefined && pass != "") {
                    $('[id$=txt_UserName]').val(user);
                    $('[id$=txt_Password]').val(pass);
                    $('#chkRemember').prop('checked', true);
                }

                $("input").on("keypress", function (e) {
                    if (e.keyCode == 13) {
                        $("#btnSubmit").trigger("click")
                    }
                });
                $('#btnSubmit').on('click', function () {
                    var UserName = $('[id$=txt_UserName]').val();
                    var Password = $('[id$=txt_Password]').val();
                    if (UserName == '' ||
                        Password == '') {
                        alert('Bạn phải nhập tên tài khoản và mật khẩu');
                        return false;
                    }

                    $.ajax({
                        type: "POST",
                        url: "Login.aspx/CheckLogin",
                        data: JSON.stringify({
                            "UserName": UserName,
                            "Password": Password,
                            "Remember": check,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            $(".se-pre-con").fadeIn("slow");
                        },
                        success: function (msg) {
                            if (msg.d.Result == "1")
                                window.location = "/Default.aspx";
                            else
                                alert(msg.d.Message);
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
                });
            });
    </script>
    </form>
</body>
</html>
