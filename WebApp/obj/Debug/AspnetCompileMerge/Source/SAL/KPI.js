﻿var currentdate = "";
var picker3 = new Pikaday(
    {
        field: document.getElementById('txtCurrentDate'),
        firstDay: 1,
        bound: false,
        container: document.getElementById('container'),
        onSelect: function (date) {
            currentdate = date;
        }
    });

function MergeCommonRows(table) {
    var topMatchTd;
    var previousValue = "";
    var rowSpan = 1;

    $('.phong').each(function () {

        if ($(this).text() == previousValue) {
            rowSpan++;
            $(topMatchTd).attr('rowspan', rowSpan);
            $(this).remove();
        }
        else {
            topMatchTd = $(this);
            rowSpan = 1;
        }

        previousValue = $(this).text();
    });
}

$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();           
});
$(document).ready(function () {
    $('#noidung2').on('click', 'tr', function (e) {
        $(this).closest('tr').next().toggle();
    });
    $('#noidung').on('click', 'tr', function (e) {
        var selected = $(this).hasClass("highlight");
        $("#noidung tr").removeClass("highlight");
        if (!selected)
            $(this).addClass("highlight");

        var ten = $(this).find("td").eq(0).html();
        ten = "Thông tin chi tiết " + ten;
        $("#text").html(ten);
    });

    var unit = Page.getUnitLevel();
    if (unit <= 3) {
        $("#level1").show();
        $("#level2").show();
        $("#level3").show();
        var $radios = $('input:radio[name=rdo1]');
        $radios.filter('[value=choncongty]').prop('checked', true);
    }
    if (unit >= 7) {
        $("#level1").hide();
        $("#level2").hide();
        $("#level3").hide();
        $("#tochuc").hide();
        var $radios = $('input:radio[name=rdo1]');
        $radios.filter('[value=chonnhanvien]').prop('checked', true);
    }
    if (unit > 3 && unit <= 6) {
        $("#level1").hide();
        $("#level2").show();
        $("#level3").show();
        var $radios = $('input:radio[name=rdo1]');
        $radios.filter('[value=chonphong]').prop('checked', true);
    }

    MergeCommonRows($('#tbl'));
    Date.prototype.ddmmyyyy = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = this.getDate().toString();
        return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy; // padding
    };

    $("div[type='an']").hide();
    $("div[type='an2']").hide();
    $(".modal.aside").ace_aside();
    $(".select2").select2({ width: "100%" });
    $("#chonthang").show();
    $("input[name=rdo1]").change(function () {
        $("div[type='an']").hide();
        $("#" + this.value).show();
    });
    $("input[name=rdo2]").change(function () {
        $("div[type='an2']").hide();
        $("#" + this.value).show();
        if (this.value !== 'chonngay') {
            $("#chonnam").show();
        }
    });
    $("#btnSearch").click(function () {

        var employee = $("[id$=DDL_Employee]").val();
        if (employee == null || employee == undefined)
            employee = $("[id$=HIDEmployee]").val();
        var department = $("[id$=DDL_Department]").val();
        if (department == null || department == undefined)
            department = $("[id$=HIDDepartment]").val();

        var chkarray = "";
        var ngay = "";
        var doituong = $('input[name=rdo1]:checked').val();
        var thoigian = $('input[name=rdo2]:checked').val();
        var nam = $("[id$=DDLYear]").val();
        var thang = $("[id$=DDLMonth]").val();
        var quy = $("[id$=DDLQuarter]").val();

        if (currentdate != "")
            ngay = currentdate.ddmmyyyy();

        $("input:checkbox[name=chkPurpose]:checked").each(function () {
            chkarray += $(this).val() + ",";
        });

        $.ajax({
            type: "POST",
            url: "/SAL/KPI.aspx/SearchData",
            data: JSON.stringify({
                "DoiTuong": doituong,
                "ThoiGian": thoigian,
                "Ngay": ngay,
                "Thang": thang,
                "Quy": quy,
                "Nam": nam,
                "MucTieu": chkarray,
                "Employee": employee,
                "Department": department,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $("#noidung").empty();
                $("#noidung2").empty();
                $("#noidung").append(msg.d.Result);
                $("#noidung2").append(msg.d.Result5);
                $("#title").text(msg.d.Result2);
                MergeCommonRows($('#tbl'));

                $("[id$=HIDFromDate]").val(msg.d.Result3);
                $("[id$=HIDToDate]").val(msg.d.Result4);

                $("#noidung2").empty();
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError);
                $("#noidung").empty();
            }
        });
    });
    $('#noidung').on("click", "tr", function () {
        var key = $(this).closest('tr').attr('key');
        var chkarray = "";
        var ngay = "";
        var doituong = $('input[name=rdo1]:checked').val();
        var thoigian = $('input[name=rdo2]:checked').val();
        var nam = $("[id$=DDLYear]").val();
        var thang = $("[id$=DDLMonth]").val();
        var quy = $("[id$=DDLQuarter]").val();
        var employee = $("[id$=DDL_Employee]").val();
        if (employee == null || employee == undefined)
            employee = 0;
        var department = $("[id$=DDL_Department]").val();
        if (department == null || department == undefined)
            department = 0;
        $("input:checkbox[name=chkPurpose]:checked").each(function () {
            chkarray += $(this).val() + ",";
        });

        if (key != undefined || key != '') {
            $.ajax({
                type: "POST",
                url: "/SAL/KPI.aspx/ListItem",
                data: JSON.stringify({
                    "MucTieu": chkarray,
                    "Department": department,
                    "Employee": key,
                    "FromDate": $("[id$=HIDFromDate]").val(),
                    "ToDate": $("[id$=HIDToDate]").val(),
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    $("#noidung2").empty();
                    $("#noidung2").append(msg.d);
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                }
            });
        }
    });
});