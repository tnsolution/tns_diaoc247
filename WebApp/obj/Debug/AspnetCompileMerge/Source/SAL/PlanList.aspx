﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="PlanList.aspx.cs" Inherits="WebApp.SAL.PlanList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Mục tiêu nhân viên
                <asp:Label ID="lblDateView" runat="server" Text="..."></asp:Label>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnPrev">
                        <i class="ace-icon fa fa-arrow-left"></i>Trước
                    </button>
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnNext">
                        Sau<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                    </button>
                    <a href="#mSearch" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-search"></i>
                        Tìm lọc
                    </a>
                    <a href="#mPlanBox" data-toggle="modal" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                </span>
            </h1>
        </div>
        <div class="row">
            <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="modal fade" id="mSearch" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Chọn điều kiện</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_DepartmentSearch" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_EmployeeSearch" runat="server" class="form-control select2" AppendDataBoundItems="true">
                            <asp:ListItem Value="0" Text="--Tất cả nhân viên--" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnSearch">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mPlanBox" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title">Cài đặt mục tiêu mới</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Tháng</label>
                        <div>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" role="monthpicker" class="form-control pull-right"
                                    id="txt_Month" placeholder="Chọn tháng năm" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Chọn phòng--" Selected="True" disabled="disabled"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <asp:DropDownList ID="DDL_Employee" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Chọn nhân viên--" Selected="True" disabled="disabled"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ghi chú</label>
                        <div>
                            <input name="txt_Description" type="text" id="txt_Description" class="form-control" placeholder="..." />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-right" data-dismiss="modal" id="btnPlanNext">Tiếp tục</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_FromDate" runat="server" Value="" />
    <asp:HiddenField ID="HID_ToDate" runat="server" Value="" />
    <asp:HiddenField ID="HID_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnView" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnView_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/bootbox.js"></script>
    <script>
        $(document).ready(function () {
            $("#btnPrev").click(function () {
                $("[id$=HID_ViewNextPrev]").val(-1);
                $("[id$=btnView]").trigger("click");
            });
            $("#btnNext").click(function () {
                $("[id$=HID_ViewNextPrev]").val(1);
                $("[id$=btnView]").trigger("click");
            });
            $("[role='monthpicker']").datepicker({
                autoclose: true,
                format: 'mm/yyyy',
                startDate: '01/01/1900',
                todayHighlight: true
            });
            $(".select2").select2({ width: "100%" });
            $("#btnPlanNext").click(function () {
                var Department = $('[id$=DDL_Department]').val();
                var Employee = $('[id$=DDL_Employee]').val();
                var Description = $('#txt_Description').val();
                var Date = $('#txt_Month').val();
                $.ajax({
                    type: 'POST',
                    url: '/SAL/PlanEdit.aspx/SavePlan',
                    data: JSON.stringify({
                        'Key': 0,
                        'Department': Department,
                        'Employee': Employee,
                        'Description': Description,
                        'Date': Date
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    success: function (msg) {
                        if (msg.d.Message == "") {
                            location.href = '/SAL/PlanEdit.aspx?ID=' + msg.d.Result;
                        }
                        else {
                            Page.showPopupMessage("Lỗi liên hệ Admin", msg.d.Message);
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            });
            $("#btnSearch").click(function () {
                $("[id$=btnView]").trigger("click");
            });
            $(".show-details-btn").on('click', function (e) {
                e.preventDefault();
                $(this).closest('tr').next().toggleClass('open');
                $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
            });
            $("#tblPlanList tbody tr td:not(:last-child)").on("click", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "PlanView.aspx?ID=" + trid;
            });
            $("[btn='btnCopy']").on(ace.click_event, function () {
                var now = new Date();
                var nextmonth = now.getMonth() + 2;
                var lastmonth = $(this).closest('tr').find("td:eq(1)").text();
                var lastname = $(this).closest('tr').find("td:eq(2)").text();
                var employeekey = $(this).closest('tr').attr("fid");
                var key = $(this).closest('tr').attr("id");
                bootbox.confirm({
                    message: "Bạn muốn copy nội dung tháng [" + lastmonth + "] của [" + lastname + "] này cho tháng " + nextmonth + " ?",
                    buttons: {
                        confirm: {
                            label: "OK",
                            className: "btn-primary btn-sm",
                        },
                        cancel: {
                            label: "Cancel",
                            className: "btn-sm",
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                type: 'POST',
                                url: '/SAL/PlanList.aspx/CopyPlan',
                                data: JSON.stringify({
                                    'PlanKey': key,
                                    'EmployeeKey': employeekey,
                                }),
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                beforeSend: function () {

                                },
                                success: function (msg) {
                                    if (msg.d.Message == "") {
                                        Page.showNotiMessageInfo("Thông báo", "Thông tin tháng [" + lastmonth + "] của [" + lastname + "] đã được copy");
                                        //location.href = '/SAL/PlanEdit.aspx?ID=' + msg.d.Result;
                                    }
                                    else {
                                        Page.showPopupMessage("Thông báo", msg.d.Message);
                                    }
                                },
                                complete: function () {
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.status);
                                    console.log(xhr.responseText);
                                    console.log(thrownError);
                                }
                            });
                        }
                    }
                });
            });
        });
    </script>
</asp:Content>
