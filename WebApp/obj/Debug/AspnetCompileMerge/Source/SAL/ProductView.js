﻿var _Usdban = 0, _Usdthue = 0, _Usdmua = 0
_assetkey = 0, _guestkey = 0;       //tinh gia usd
var _No1Image = 0, _Cookies = 0, _UnitLevel = 0, _EmployeeKey;
// Page.getUrlParameter("ID")
tinymce.init({
    selector: '[id$=txt_WebNoiDung]',
    height: 250,
    theme: 'modern',
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
    ],
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
    ]
});
$(function () {
    var x = $('#leftbox').height();
    var x = x - 75;
    $('#chunha').ace_scroll({
        size: 200,
    });
    $('#hinhanh').ace_scroll({
        size: 200,
    });
    $("#bangphai").ace_scroll({
        size: x,
    });
    $("#tblExtraData").tableHeadFixer();

    var AssetKey = Page.getUrlParameter("ID");
    var AssetType = Page.getUrlParameter("Type");
    var Edit = $("[id$=HID_Edit]").val();
    if (Edit == 0) { $("[isEdit]").hide(); } else { $("[isEdit]").show(); }

    $('input[moneyinput]').number(true, 0);
    $(".select2").select2({ width: "100%" });
    $("div[view='usd']").hide();
    $("div[view='vnd']").show();
    $('#id-input-file-2').ace_file_input({
        style: 'well',
        no_file: 'No File ...',
        no_icon: 'ace-icon fa fa-cloud-upload',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        droppable: true,
        thumbnail: true, //| true | large
        whitelist: 'gif|png|jpg|jpeg',
        blacklist: 'exe|php'
    }).on("change", function () {
        if ($('#id-input-file-2').val().length > 0) {
            var ext2 = $('#id-input-file-2').val().split('.').pop().toLowerCase();
            if ($.inArray(ext2, ['jpg', 'png', 'gif']) == -1) {
                $("#UploadImg").addClass("disabled");
                alert('Tập tin không hợp lệ !');
            }
            else {
                $("#UploadImg").removeClass("disabled");
            }
        }
    });
    $("#chkPriceType").change(function () {
        if ($(this).is(':checked')) {
            $("div[view='usd']").show();
            $("div[view='vnd']").hide();
        }
        else {
            $("div[view='vnd']").show();
            $("div[view='usd']").hide();
        }
    });
    $("#DDL_Category").change(function () { myFunction(this, 3); });
    $("#DDL_Status").change(function () { myFunction(this, 5); });
    //-----------------------------------------------------------------------------------Layout Action
    $("#tblExtraData tbody").on("click", "tr", function () {
        var trid = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('fid');
        LoadAsset(trid, type);
    });
    $('#tblMain tr.action td:not(:last-child)').on('click', function (e) {
        e.preventDefault();

        if ($(this).hasClass('indam')) {
            $(this).removeClass('indam');
            $(this).closest('tr').next().toggleClass('open')
        }
        else {
            $('#tblMain tbody tr').removeClass('open indam');

            $(this).closest('tr').next().toggleClass('open');
            if ($(this).closest('tr').hasClass('indam'))
                $(this).closest('tr').removeClass('indam');
            else
                $(this).closest('tr').addClass('indam');
        }
    });
    //-----------------------------------------------------------------------------------Table Action
    $("[id$=txt_updatePriceRentUSD]").hide();
    $("[id$=txt_updatePriceUSD]").hide();
    $("[id$=txt_updatePricePurchaseUSD]").hide();

    $("[id$=Usdswitch1]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_updatePriceUSD]").show();
            $("[id$=txt_updatePriceVND]").hide();
            $("#updategiaban").text("Giá bán USD");
            _Usdban = 1;
        }
        else {
            $("[id$=txt_updatePriceUSD]").hide();
            $("[id$=txt_updatePriceVND]").show();
            $("#updategiaban").text("Giá bán VNĐ");
            _Usdban = 2;
        }
    });
    $("[id$=Usdswitch2]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_updatePriceRentUSD]").show();
            $("[id$=txt_updatePriceRentVND]").hide();
            $("#updategiathue").text("Giá thuê USD");
            _Usdthue = 1;
        }
        else {
            $("[id$=txt_updatePriceRentUSD]").hide();
            $("[id$=txt_updatePriceRentVND]").show();
            $("#updategiathue").text("Giá thuê VNĐ");
            _Usdthue = 2;
        }
    });
    $("[id$=Usdswitch3]").change(function () {
        if ($(this).is(':checked')) {
            $("[id$=txt_updatePricePurchaseUSD]").show();
            $("[id$=txt_updatePricePurchaseVND]").hide();
            $("#updategiamua").text("Giá mua USD");
            _Usdmua = 1;
        }
        else {
            $("[id$=txt_updatePricePurchaseUSD]").hide();
            $("[id$=txt_updatePricePurchaseVND]").show();
            $("#updategiamua").text("Giá mua VNĐ");
            _Usdmua = 2;
        }
    });

    //$("[id$=txt_updatePricePurchaseUSD]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_updatePricePurchaseVND]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_updatePriceUSD]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_updatePriceRentUSD]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_updatePriceVND]").blur(function () { ConvertMoney(); });
    //$("[id$=txt_updatePriceRentVND]").blur(function () { ConvertMoney(); });

    //------------------------------------------------------------------------------------End Action USD
    $("#UpdateAsset").click(function () {
        var id = $("[id$=HID_AssetKey]").val();
        console.log(id);
        UpdateAsset(id);
    });
    $("#UploadImg").click(function () {
        $("[id$=btnUploadImg]").trigger("click");
    });
    $("#btnExeTransfer").hide();
    $("#btnExeReminder").hide();
    $("#btnExeTransfer").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/ExeTransfer",
            data: JSON.stringify({
                "AssetKey": $("[id$=HID_TransferKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Result == 1) {
                    Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                    $("#btnExeTransfer").hide();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnExeReminder").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/ExeReminder",
            data: JSON.stringify({
                "RemindKey": $("[id$=HID_RemindKey]").val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg.d.Result == 1) {
                    Page.showNotiMessageInfo("Thông báo !", msg.d.Message);
                    $("#btnExeReminder").hide();
                }
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnExpand").on("click", function () {
        $(this).toggleClass("active");
        $(this).text($(this).text() == 'Mở rộng' ? 'Thu gọn' : 'Mở rộng');
        if ($(this).hasClass("active"))
            $('#tblMain tbody tr').removeClass('open indam');
        else {
            $('#tblMain tbody tr.action').addClass('indam');
            $('#tblMain tbody tr').nextUntil('.action').addClass('open');
        }
    });
    $("#btnSearch").click(function () {
        var Category = $("[id$=DDL_Category]").val();
        var Purpose = $("[id$=DDL_Purpose]").val();
        var Room = $("[id$=DDL_Room]").val();
        if (Category == null)
            Category = 0;
        if (Purpose == null)
            Purpose = "";
        if (Room == null)
            Room = "";

        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/SearchAsset",
            data: JSON.stringify({
                "Project": $("[id$=HID_ProjectKey]").val(),
                "AssetID": $("#txt_Name").val(),
                "Room": Room,
                "Category": Category,
                "Purpose": Purpose,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $('#tblExtraData tbody').empty();
                $('#tblExtraData tbody').append($(msg.d));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnEdit").click(function () {
        $('.se-pre-con').fadeIn('slow');
        var link = '/SAL/ProductEdit.aspx?ID=' + $("[id$=HID_AssetKey]").val() + '&Type=' + $("[id$=HID_AssetType]").val();
        window.location = link;
    });
    $("#btnProcess").click(function () {
        if (!checkSave())
            return false;

        var Purpose = "";
        $("input[name='chkPurpose']:checked").each(function (i) {
            Purpose += $(this).val() + "/ ";
        });

        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/SaveProduct",
            data: JSON.stringify({
                "AssetKey": $("[id$=HID_AssetKey]").val(),
                "AssetType": $("[id$=HID_AssetType]").val(),
                "AssetStatus": $("[id$=DDL_StatusAsset2]").val(),
                "AssetRemind": $("[id$=txt_updateDateContractEnd]").val(),
                "AssetPrice_VND": $("[id$=txt_updatePriceVND]").val(),
                "AssetPriceRent_VND": $("[id$=txt_updatePriceRentVND]").val(),
                "AssetPrice_USD": $("[id$=txt_updatePriceUSD]").val(),
                "AssetPriceRent_USD": $("[id$=txt_updatePriceRentUSD]").val(),
                "AssetPricePurchase_VND": $("[id$=txt_updatePricePurchaseVND]").val(),
                "AssetPricePurchase_USD": $("[id$=txt_updatePricePurchaseUSD]").val(),
                "AssetFurnitur": $("[id$=DDL_Furnitur2]").val(),
                "AssetPurpose": Purpose,
                "AssetDescription": $("[id$=txt_updateAssetDescription]").val(),
                "AssetLegal": $("[id$=DDL_Legal]").val(),
                "UsdMua": _Usdmua,
                "UsdBan": _Usdban,
                "UsdThue": _Usdthue,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message == "OK") {
                    Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('.se-pre-con').fadeOut('slow');
                Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
        $("#mProcess").modal("hide");
    });
    $("#AddGuest").click(function () {
        var id = 0;
        var type = 'ResaleApartment';
        $("[id$=HID_GuestKey]").val(0);
        $('#mGuest').modal({
            backdrop: true,
            show: true
        });
        $('#mGuest').find('input:text').val('');
    });
    $("#AddShare").click(function () {
        $('#mShare').modal({
            backdrop: true,
            show: true
        });
    });
    $("#AddPost").click(function () {
        $("#mPost").modal({
            backdrop: true,
            show: true
        });
    });
    $("#btnSavePost").click(function () {
        tinymce.triggerSave();
        var noidung = $("[id$=txt_WebNoiDung]").val();
        var tieude = $("[id$=txt_WebTieuDe]").val();
        var hienthi = $("[id$=DDL_WebChon]").val();

        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/SavePost",
            data: JSON.stringify({
                "AssetKey": $('[id$=HID_AssetKey]').val(),
                "AssetType": $("[id$=HID_AssetType]").val(),
                "NoiDung": noidung,
                "TieuDe": tieude,
                "HienThi": hienthi,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Result == 'OK') {
                    var url = '/SAL/ProductView.aspx?ID=' + $('[id$=HID_AssetKey]').val() + "&Type=" + $("[id$=HID_AssetType]").val();
                    console.log(url);
                    window.location.replace(url);
                }
                else { console.log(msg.d.Result); }
                //location.reload();
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnSaveShare").click(function () {
        $.ajax({
            type: "POST",
            url: "/SAL/ProductView.aspx/SaveShare",
            data: JSON.stringify({
                "AssetKey": $('[id$=HID_AssetKey]').val(),
                "AssetType": $("[id$=HID_AssetType]").val(),
                "EmployeeKey": $('[id$=DDL_Employee]').val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message.length <= 0) {
                    getShare();
                } else {
                    Page.showPopupMessage("Thông báo !.", msg.d.Message);
                }
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnSaveGuest").click(function () {
        SaveGuest();
    });
    //------------------------------------------------------------------------------------End Action button

    CheckRemind(AssetKey);
    CheckTransfer(AssetKey);
});
function CheckTransfer(AssetKey) {
    //kiem tra khi load
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/CheckTransfer",
        data: JSON.stringify({
            "AssetKey": AssetKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                $("[id$=HID_TransferKey]").val(msg.d.Result);
                Page.showNotiMessageInfo("Thông báo sản phẩm mới dc giao !", msg.d.Message);
                $("#btnExeTransfer").show();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function CheckRemind(AssetKey) {
    //kiem tra khi load
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/CheckReminder",
        data: JSON.stringify({
            "AssetKey": AssetKey,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                $("[id$=HID_RemindKey]").val(msg.d.Result);
                Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                $("#btnExeReminder").show();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function AssetCheckEmployee(AssetKey, AssetType) {
    //kiem tra khi load
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/AssetCheckEmployee",
        data: JSON.stringify({
            "AssetKey": AssetKey,
            "AssetType": AssetType,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {

            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function LoadAsset(trid, type) {
    //load data on click
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/GetAsset",
        data: JSON.stringify({
            "AssetKey": trid,
            "AssetType": type
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            $("#leftbox div[value]").html("");

            var giaban1 = msg.d.Price_USD + " (USD) ";
            var giaban2 = msg.d.Price_VND + " (VNĐ)"
            var giathue1 = msg.d.PriceRent_USD + " (USD)";
            var giathue2 = msg.d.PriceRent_VND + " (VNĐ)"
            var giamua1 = msg.d.PricePurchase_USD + " (USD)";
            var giamua2 = msg.d.PricePurchase_VND + " (VNĐ)";

            $("#tieudetrang").text(msg.d.AssetID);
            $("#tinhtrang").text(msg.d.Status);
            $("#phaply").text(msg.d.Legal);
            $("#giaban1").text(giaban1);
            $("#giathue1").text(giathue1);
            $("#giaban2").text(giaban2);
            $("#giathue2").text(giathue2);
            $("#giamua1").text(giamua1);
            $("#giamua2").text(giamua2);

            $("#noithat").text(msg.d.Furniture);
            $("#nhucau").text(msg.d.Purpose);
            $("#ghichu").text(msg.d.Description);

            if (msg.d.Edit == 1) {
                $("#sdtchunha").text(msg.d.Phone1);
                $("#tenchunha").text(msg.d.CustomerName);
            }
            else {
                $("#sales").text("Liên hệ " + msg.d.EmployeeName + " để xử lý thông tin !.");
            }

            $("#duan").text(msg.d.ProjectName);
            $("#masp").text(msg.d.AssetID);
            $("#loaisp").text(msg.d.CategoryName);
            $("#sophong").text(msg.d.Room);

            $("#ngaylienhe").text(msg.d.DateContractEnd);
            $("#dttimtuong").text(msg.d.AreaWall);
            $("#dtthongthuy").text(msg.d.AreaWater);
            $("#huongview").text(msg.d.View);
            $("#huongcua").text(msg.d.Door);

            $("#tieude").text(msg.d.WebTitle);
            $("#xuatban").text(msg.d.WebPublished);
            $("#noidung").text(msg.d.WebContent);

            $("#nguoikhoitao").text(msg.d.CreatedName);
            $("#ngaykhoitao").text(msg.d.CreatedDate);
            $("#nguoicapnhat").text(msg.d.ModifiedName);
            $("#ngaycapnhat").text(msg.d.ModifiedDate);
            $("#nguoiquanly").text(msg.d.EmployeeName);

            var edit = msg.d.Edit;
            var employeename = msg.d.EmployeeName;
            if (employeename == null)
                employeename = "";
            var employeekey = msg.d.EmployeeKey;
            if (employeekey == null)
                employeekey = 0;

            console.log(edit);
            console.log(msg.d.EmployeeName)
            setTimeout(function () {
                //load chu nha on click
                $.ajax({
                    type: "POST",
                    url: "/SAL/ProductView.aspx/GetOwner",
                    data: JSON.stringify({
                        "AssetKey": trid,
                        "AssetType": type,
                        "Edit": edit,
                        "EmployeeKey": employeekey,
                        "EmployeeName": employeename,
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        $("#chunha").empty();
                        $("#chunha").append($(msg.d.Result));
                    },
                    complete: function () {
                        $('.se-pre-con').fadeOut('slow');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }, 1000);

            if (edit == 0) {
                $("[isEdit]").hide();
            }
            else {
                $("[isEdit]").show();
            }
        },
        complete: function () {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.se-pre-con').fadeOut('slow');
            Page.showNotiMessageError("Lỗi,", "Vui lòng liên hệ Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
    //load file on click
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/GetFile",
        data: JSON.stringify({
            "AssetKey": trid,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            $("#hinhanh").empty();
            $("#hinhanh").append($(msg.d));
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
    //kiem tra ngay lien he on click
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/CheckReminder",
        data: JSON.stringify({
            "AssetKey": trid,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                $("[id$=HID_RemindKey]").val(msg.d.Result);
                Page.showNotiMessageInfo("Thông báo đến hẹn !", msg.d.Message);
                $("#btnExeReminder").show();
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });

    $("[id$=HID_AssetKey]").val(trid);
    $("[id$=HID_AssetType]").val(type);
}
function UpdateAsset(id) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/GetAssetEdit",
        data: JSON.stringify({
            "AssetKey": id,
            "AssetType": $("[id$=HID_AssetType]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            _assetkey = msg.d.AssetKey;
            $("[id$=txt_updateDateContractEnd]").val(msg.d.DateContractEnd);
            $("[id$=txt_updatePriceRentVND]").val(msg.d.PriceRent_VND);
            $("[id$=txt_updatePriceRentUSD]").val(msg.d.PriceRent_USD);
            $("[id$=txt_updatePriceVND]").val(msg.d.Price_VND);
            $("[id$=txt_updatePriceUSD]").val(msg.d.Price_USD);
            $("[id$=txt_updatePricePurchaseVND]").val(msg.d.PricePurchaseVND);
            $("[id$=txt_updatePricePurchaseUSD]").val(msg.d.PricePurchaseUSD);
            $("[id$=txt_updateAssetDescription]").val(msg.d.Description);
            $("[id$=DDL_Furnitur2]").val(msg.d.FurnitureKey).trigger("change");
            $("[id$=DDL_StatusAsset2]").val(msg.d.StatusKey).trigger("change");
            $("[id$=DDL_Legal]").val(msg.d.LegalKey).trigger("change");
            var SetPurpose = msg.d.PurposeKey.split("/");
            for (var i in SetPurpose) {
                var val = $.trim(SetPurpose[i]);
                $('input[type=checkbox][value="' + val + '"]').prop('checked', true);
            }

            $("#mProcess").modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showNotiMessageError("Lỗi thực hiện !.", "Vui lòng thông báo cho Admin");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function ConvertMoney() {
    if (_Usd == 1) {
        var priceUsd = $("[id$=txt_updatePriceUSD]").val();
        var priceRentUsd = $("[id$=txt_updatePriceRentUSD]").val();
        var pricePurchaseUSD = $("[id$=txt_updatePricePurchaseUSD]").val();

        var priceVnd = Page.RemoveComma(priceUsd) * usdRate;
        var priceRentVnd = Page.RemoveComma(priceRentUsd) * usdRate;
        var pricePurchaseVnd = Page.RemoveComma(pricePurchaseUSD) * usdRate;

        $("[id$=txt_updatePricePurchaseVND]").val(Page.FormatMoney(pricePurchaseVnd));
        $("[id$=txt_updatePriceRentVND]").val(Page.FormatMoney(priceRentVnd));
        $("[id$=txt_updatePriceVND]").val(Page.FormatMoney(priceVnd));
    }
    else {
        var priceVnd = $("[id$=txt_updatePriceVND]").val();
        var priceRentVnd = $("[id$=txt_updatePriceRentVND]").val();
        var pricePurchaseVnd = $("[id$=txt_updatePricePurchaseVND]").val();

        var priceUsd = Page.RemoveComma(priceVnd) / usdRate;
        var priceRentUsd = Page.RemoveComma(priceRentVnd) / usdRate;
        var pricePurchaseUsd = Page.RemoveComma(pricePurchaseVnd) / usdRate;

        $("[id$=txt_updatePricePurchaseUSD]").val(Page.FormatMoney(pricePurchaseUsd));
        $("[id$=txt_updatePriceRentUSD]").val(Page.FormatMoney(priceRentUsd));
        $("[id$=txt_updatePriceUSD]").val(Page.FormatMoney(priceUsd));
    }
}
function UpdateGuest(id, type) {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/GetGuest",
        data: JSON.stringify({
            'Type': type,
            'AutoKey': id,
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('.se-pre-con').fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message != "") {
                Page.showPopupMessage("Lỗi !", msg.d.Message);
            }
            else {
                $('#mGuest').modal({
                    backdrop: true,
                    show: true
                });
                $("[id$=HID_AssetType]").val(msg.d.Type);
                $("[id$=HID_GuestKey]").val(msg.d.OwnerKey);
                $("[id$=HID_AssetKey]").val(msg.d.AssetKey);
                $("[id$=txt_CustomerName]").val(msg.d.CustomerName);
                $("[id$=txt_Phone]").val(msg.d.Phone1);
                $("[id$=txt_Phone2]").val(msg.d.Phone2);
                $("[id$=txt_Email]").val(msg.d.Email1);
                $("[id$=txt_Address]").val(msg.d.Address1);
                $("[id$=txt_Address2]").val(msg.d.Address2);
            }
        },
        complete: function () {
            $('.se-pre-con').fadeOut('slow');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveGuest() {
    $.ajax({
        type: 'POST',
        url: '/SAL/ProductView.aspx/SaveGuest',
        data: JSON.stringify({
            'AutoKey': $("[id$=HID_GuestKey]").val(),
            'AssetType': $("[id$=HID_AssetType]").val(),
            'AssetKey': $("[id$=HID_AssetKey]").val(),
            'CustomerName': $("[id$=txt_CustomerName]").val(),
            'Phone1': $("[id$=txt_Phone]").val(),
            'Phone2': $("[id$=txt_Phone2]").val(),
            'Email': $("[id$=txt_Email]").val(),
            'Address1': $("[id$=txt_Address]").val(),
            'Address2': $("[id$=txt_Address2]").val(),
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn('slow');
        },
        success: function (msg) {
            if (msg.d.Message == "OK") {
                Page.showNotiMessageInfo("Thông báo !", "Đã lưu thành công.");
                window.location = "/SAL/ProductView.aspx?ID=" + $("[id$=HID_AssetKey]").val() + "&Type=ResaleApartment";
            } else {
                Page.showPopupMessage("Thông báo !.", msg.d.Message);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $(".se-pre-con").fadeOut('slow');
            Page.showPopupMessage("Thông báo cho Admin", "Lỗi thực hiện lệnh" + xhr.responseText);
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function getShare() {
    $.ajax({
        type: "POST",
        url: "/SAL/ProductView.aspx/GetShare",
        data: JSON.stringify({
            'AssetKey': $("[id$=HID_AssetKey]").val(),
            'AssetType': $('[id$=HID_AssetType]').val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#tblShare').empty();
        },
        success: function (msg) {
            for (var i = 0; i < msg.d.length; i++) {
                var html = "<div class='profile-info-row' id='" + msg.d[i].Value + "'>";
                html += "<div class='profile-info-name'>Chia sản phẩm cho </div><div class='profile-info-value'>" + msg.d[i].Text + "</div></div>";

                $('#tblShare').append(html);
            }
        },
        complete: function () {

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//------
function myFunction(arg, no) {
    var input, filter, table, tr, td, i;
    input = $("#" + arg.getAttribute("id")).val();
    filter = input.toUpperCase();
    table = $("#tblExtraData");
    tr = table.find("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[no];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
//------
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function getCookieValue(cname, cval) {
    var ca = cname.split('&');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cval) == 0) {
            return c.substring(cval.length, c.length).replace("=", "");
        }
    }
}
//------
function checkSave() {
    var status = $("[id$=DDL_StatusAsset2]").val();
    var noithat = $("[id$=DDL_Furnitur2]").val();
    if (status == 313 ||
        status == 29 ||
        status == 30) {
        return true;
    }
    if (status == null ||
        status == undefined ||
        status == 0) {
        Page.showNotiMessageError("Bạn phải chọn tình trạng !.");
        return false;
    }

    //314 lien hệ lại
    if (status == 314) {
        if ($("#txt_updateDateContractEnd").val().length <= 0) {
            Page.showNotiMessageError("Bạn phải nhập ngày liên hệ lại !.");
            return false;
        }
    }
    else {
        if (!$('#chkCT').prop('checked') &&
            !$('#chkCN').prop('checked')) {
            Page.showNotiMessageError("Bạn phải nhập chọn nhu cầu");
            return false;
        }
        if ($('#chkCT').prop('checked') && _Usdthue == 0) {
            if ($('#txt_updatePriceRentVND').val() == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá thuê VNĐ");
                return false;
            }
        }
        if ($('#chkCT').prop('checked') && _Usdthue == 1) {
            if ($('#txt_updatePriceRentUSD').val() == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá thuê USD");
                return false;
            }
        }
        if ($('#chkCN').prop('checked')) {
            if ($('#txt_updatePriceVND').val() == 0 && _Usdmua == 0) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá chuyển nhượng VNĐ");
                return false;
            }
        }
        if ($('#chkCN').prop('checked')) {
            if ($('#txt_updatePriceVND').val() == 0 && _Usdmua == 1) {
                Page.showNotiMessageError("Bạn phải nhập thông tin giá chuyển nhượng USD");
                return false;
            }
        }
        if (noithat == null || noithat == undefined || noithat == 0) {
            Page.showNotiMessageError("Bạn phải chọn nội thất !.");
            return false;
        }
    }

    return true;
}