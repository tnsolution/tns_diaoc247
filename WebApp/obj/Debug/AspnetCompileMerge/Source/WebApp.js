﻿var error_obj = null;
var error_prefix = "Phải nhập ";
var error_message = "<div class='text-danger error'>{msg}</div>";
var usdRate = 22760;
var interval = null;
var ReadV1 = "";//doc cookies
var ReadV2 = "";//doc cookies
var Page = {
    initialize: function () {
        $("a[href$='.doc']").not("[target]").each(function () {
            $(this).attr('href', 'http://docs.google.com/viewer?embedded=true&url=' + $(this).attr('href'));
        });
        $("a[href$='.docx']").not("[target]").each(function () {
            $(this).attr('href', 'http://docs.google.com/viewer?embedded=true&url=' + $(this).attr('href'));
        });
        $("a[href$='.pdf']").each(function () {
            $(this).attr('href', $(this).attr('href'));
        });
        $("a[href$='.xls']").each(function () {
            $(this).attr('href', 'http://docs.google.com/viewer?embedded=true&url=' + $(this).attr('href'));
        });
        $("a[href$='.xlsx']").each(function () {
            $(this).attr('href', 'http://docs.google.com/viewer?embedded=true&url=' + $(this).attr('href'));
        });

        ReadV1 = Cookies.get('ViewSpec');
        ReadV2 = Cookies.get('ViewDepart');

        Page.checkAlert();
        Page.interval = setInterval(function () {
            /// call your function here
            checkAlert();
        }, 900000);

        $("[role='datepicker']").datepicker({
            orientation: "bottom",
            autoclose: true,
            format: 'dd/mm/yyyy',
            startDate: '01/01/1900',
            todayHighlight: true
        });

        var url = window.location.pathname;
        var current = $('ul.nav-list a[href="' + url + '"]').parent();
        current.addClass('active');
        current.parents('li').addClass('active open');

        $("ul.nav li a").click(function (e) {
            if (this.getAttribute("href").charAt(0) !== "#")
                $('.se-pre-con').fadeIn('slow');
        });

        $("li").on("click", ".san", function () {
            var val = $(this).attr('value');
            var text = $(this).text();

            $("#san").html(" " + text + ' <i class="ace-icon fa fa-angle-down bigger-110">');

            $("[id$=HID_Spec]").val(val);
            Cookies.set('ViewSpec', val);

            Page.FilterGroup(val);
            $('.se-pre-con').fadeIn('slow');
            setTimeout(function () {
                if (location.port.length > 0) {
                    var url = document.location.protocol + "//" + document.location.hostname + ":" + location.port + document.location.pathname;
                    window.location.replace(url);
                }
                else {
                    var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname;
                    window.location.replace(url);
                }
            }, 3000);           
        });
        $("li").on("click", ".khuvuc", function () {
            var val2 = $(this).attr('value');
            var text = $(this).text();

            $("#khuvuc").html(" " + text + ' <i class="ace-icon fa fa-angle-down bigger-110">');

            $("[id$=HID_Department]").val(val2);
            Cookies.set('ViewDepart', val2);

            $('.se-pre-con').fadeIn('slow');
            if (location.port.length > 0) {
                var search = $(location).attr('search');
                var url = document.location.protocol + "//" + document.location.hostname + ":" + location.port + document.location.pathname + search;
                
                window.location.replace(url);
            }
            else {
                var search = $(location).attr('search');
                var url = document.location.protocol + "//" + document.location.hostname + document.location.pathname + search;
                alert($(location).attr('search'));
                window.location.replace(url);
            }
        });

        //get the state ul li click on post       
        if (ReadV1 != undefined && ReadV1 != "") {
            var text = $('li .san[value=' + ReadV1 + ']').html();
            $("[id$=HID_Spec]").val(ReadV1);
            $("#san").html(text);
            Page.FilterGroup(ReadV1);
        }
        if (ReadV2 != undefined && ReadV2 != "") {
            $("[id$=HID_Department]").val(ReadV2);

            if (ReadV2.length > 3) {
                $("#khuvuc").html("--Tất cả--");
                return;
            }
            var text = $('li .khuvuc[value=\'' + ReadV2 + '\']').html();
            $("#khuvuc").html(text);
        }

        $("[id$=HID_Unit]").val(Page.getUnitLevel());
        $("[id$=HID_Spec]").val(Page.getSpec());
        $("[id$=HID_Department]").val(Page.getDepartment());
        $("[id$=HID_DepartmentRole]").val(Page.getDepartmentRole());
        $("[id$=HID_Employee]").val(Page.getEmployee());
    },
    myAlert: function (a) {
        $("#snackbar").empty();
        $("#snackbar").append(a);
        var x = document.getElementById("snackbar")
        x.className = "show";
        setTimeout(function () { x.className = x.className.replace("show", ""); }, 10000);
    },
    checkAlert: function () {
        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        if (dt.getHours() >= 17) {
            var Employee = Page.getEmployee();
            $.ajax({
                url: '/Ajax.aspx/Auto_Alert',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    'Employee': Employee,
                }),
                beforeSend: function (xhr) {
                },
                success: function (response) {
                    if (response.d == "ERROR") {
                        alert("Lỗi tự động");
                    }
                    else {
                        if (response.d != "") {
                            Page.myAlert(response.d);
                        }
                        else {
                            clearInterval(interval);
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                complete: function (response) {
                }
            });
        }
    },

    //---------------------------------    
    getUnitLevel: function () {
        cookieList = document.cookie.split('; ');
        cookies = {};
        for (i = cookieList.length - 1; i >= 0; i--) {
            var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
            if (cName == 'UserLog') {
                var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                cookies = val.split('&')[0].split('=');
                return cookies[1];
            }
        }
    },
    getSpec: function () {
        cookieList = document.cookie.split('; ');
        cookies = {};
        for (i = cookieList.length - 1; i >= 0; i--) {
            var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
            if (cName == 'UserLog') {
                var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                cookies = val.split('&')[1].split('=');
                return cookies[1];
            }
        }
    },
    getDepartment: function () {
        cookieList = document.cookie.split('; ');
        cookies = {};
        for (i = cookieList.length - 1; i >= 0; i--) {
            var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
            if (cName == 'UserLog') {
                var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                cookies = val.split('&')[2].split('=');
                return cookies[1];
            }
        }
    },
    getDepartmentRole: function () {
        cookieList = document.cookie.split('; ');
        cookies = {};
        for (i = cookieList.length - 1; i >= 0; i--) {
            var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
            if (cName == 'UserLog') {
                var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                cookies = val.split('&')[3].split('=');
                return cookies[1];
            }
        }
    },
    getEmployee: function () {
        cookieList = document.cookie.split('; ');
        cookies = {};
        for (i = cookieList.length - 1; i >= 0; i--) {
            var cName = cookieList[i].substr(0, cookieList[i].indexOf('='));
            if (cName == 'UserLog') {
                var val = cookieList[i].substr(cookieList[i].indexOf('=') + 1);
                cookies = val.split('&')[5].split('=');
                return cookies[1];
            }
        }
    },
    //---------------------------------
    goBackWithRefresh: function () {
        var last_url = document.referrer;
        if (last_url) {
            window.location.href = last_url;
        } else {
            window.history.back();
        }
    },
    //---------------------------------
    FilterGroup: function (Key) {
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetDepartment",
            data: JSON.stringify({
                "Spec": Key,
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (msg) {
                var District = $("[id$=OptionKhuvuc]");
                District.find('li').remove();
                $.each(msg.d, function () {
                    var object = this;
                    if (object !== '') {
                        District.append($("<li class='option khuvuc' value='" + object.Value + "'>" + object.Text + "</li>"));
                    }
                });

                $("[id$=HID_Department]").val(msg.d[0].Value);
                Cookies.set('ViewDepart', msg.d[0].Value);
                ReadV2 = msg.d[0].Value;
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    },
    //---------------------------------
    showNotiMessagesticky: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-error"
        });
    },
    showNotiMessageInfo: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-info gritter-light"
        });
    },
    showNotiMessageError: function (title, content) {
        $.gritter.add({
            title: title,
            text: content,
            class_name: "gritter-error gritter-light"
        });
    },
    showNotiMessageInfoButton: function (title, content) {
        $.gritter.add({
            title: title,
            text: content + '<br/><a href="#" class="red">Đồng ý</a>',
            class_name: "gritter-error gritter-light"
        });
    },
    showPopupMessage: function (title, content) {
        $("#titContent").text(content);
        $("#titMessage").text(title);
        $("#mMessageBox").modal({
            backdrop: true,
            show: true
        });
    },
    getUrlParameter: function (sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split("&"),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split("=");

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    getCurrentDate: function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        return output;
    },
    getNextWorkDate: function () {
        var output = "";
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate() + 1;

        if (d.getDay() == 6) {
            day = d.getDate() + 2;
            output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        }
        else {
            output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        }

        return output;
    },
    getLastDayMonth: function () {
        var date = new Date();
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var month = ((lastDay.getMonth() + 1) > 9) ? (lastDay.getMonth() + 1) : '0' + (lastDay.getMonth() + 1);
        var day = lastDay.getDate() + "/" + month + "/" + lastDay.getFullYear();
        return day;
    },
    getFirstDayMonth: function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var month = ((firstDay.getMonth() + 1) > 9) ? (firstDay.getMonth() + 1) : '0' + (firstDay.getMonth() + 1);

        var day = firstDay.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + "/" + month + "/" + firstDay.getFullYear();
        return output;
    },
    //---------------------------------
    FloatInput: function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    },
    CheckExt: function (file) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'png':
            case 'gif':
                return false;
            case 'zip':
            case 'rar':
                return false;
            case 'pdf':
                return false;
            case 'xlsx':
            case 'xls':
            case 'docx':
            case 'doc':
                return true;

            default:
                return false;
        }
    },
    RemoveUnicode: function (str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "");
        str = str.replace(/-+-/g, ""); //thay thế 2- thành 1- 
        str = str.replace(/^\-+|\-+$/g, "");

        return str;
    },
    FormatMoney: function (num) {
        var str = num.toString().replace("$", "").replace(/ /g, ''),
            parts = false,
            output = [],
            i = 1,
            formatted = null;

        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");

        return (formatted);
    },
    RemoveComma: function (value) {
        return value.replace(/,/g, '');
    },
    //----------------------
    validate: function () {
        $('input[required]').each(function (idx, item) {
            Page.checkError($(item));
        });
        $('select[required]').each(function (idx, item) {
            Page.checkSelect($(item));
        });

        if ($('div.error').length > 0) {
            return false;
        }
        return true;
    },
    checkError: function (txt) {
        if (txt.attr("role") === "datepicker") {
            txt.parent().next().remove();
        } else {
            txt.next().remove();
        }
        var val = $.trim(txt.val());
        if (val === "") {
            var title = error_prefix + $.trim(txt.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            if (txt.attr("role") === "datepicker") {
                txt.parent().parent().append($(error));
            } else {
                txt.parent().append($(error));
            }
            if (error_obj === null) {
                error_obj = txt;
            }
        }
    },
    checkSelect: function (select) {
        select.next().next().remove();
        var val = select.val();
        if (val <= 0) {
            var title = error_prefix + $.trim(select.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            select.parent().append($(error));
            if (error_obj === null) {
                error_obj = select;
            }
        }
    },
    checkSelectUseZero: function (select) {
        select.next().next().remove();
        var val = select.val();
        if (val < 0) {
            var title = error_prefix + $.trim(select.parents(".form-group").find("label").text());
            var error = error_message.replace("{msg}", title);
            select.parent().append($(error));
            if (error_obj === null) {
                error_obj = select;
            }
        }
    },
    //-----------------------
    resetValue: function (id) {
        jQuery("#" + id).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'text':
                case 'textarea':
                case 'file':
                case 'select-one':
                case 'select-multiple':
                case 'date':
                case 'number':
                case 'tel':
                case 'email':
                    jQuery(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
                    break;
            }
        });
    },
};
$(document).ready(Page.initialize);