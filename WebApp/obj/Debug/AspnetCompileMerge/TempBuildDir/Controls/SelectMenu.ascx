﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectMenu.ascx.cs" Inherits="WebApp.Controls.SelectMenu" %>
<ul class="nav navbar-nav">
    <li>
        <asp:Literal ID="Literal_Group" runat="server"></asp:Literal>        
    </li>
    <li>
        <asp:Literal ID="Literal_Branch" runat="server"></asp:Literal>
    </li>
</ul>
