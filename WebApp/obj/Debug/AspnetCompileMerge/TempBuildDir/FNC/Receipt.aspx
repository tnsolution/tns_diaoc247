﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Receipt.aspx.cs" Inherits="WebApp.FNC.Receipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .td10 {
            width: 10%;
        }

        .td5 {
            width: 5%;
        }
    </style>
    <link rel="stylesheet" href="/template/Pikaday-master/css/pikaday.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1 id="title">Thu<small><asp:Label ID="lbltime" runat="server" Text="..."></asp:Label></small>
                <span class="tools pull-right noprint">
                    <a href="#mReceipt" class="btn btn-white btn-info btn-bold" data-toggle="modal">
                        <i class="ace-icon fa fa-plus"></i>
                        Tạo mới
                    </a>
                    <a href="#" id="btnDownload" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-file-excel-o"></i>
                        Excel
                    </a>
                </span>
                <span class="pull-right action-buttons">
                    <a href="#" id="ViewPrevious">← Tháng trước</a> |
                      <a href="#" id="ViewNext">Tháng sau →</a>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12" id="tbl">
                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div id="left-menu" class="modal aside noprint" data-placement="left" data-fixed="true">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header no-padding">
                    <div class="table-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeLeft">
                            <span class="white">×</span>
                        </button>
                        Tìm thông tin
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true" Style="width: 100%">
                            <asp:ListItem Value="0" Text="-- Tất cả phòng --" Selected="True"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control"
                                id="txtTuNgay" placeholder="Từ ngày" />
                        </div>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control"
                                id="txtDenNgay" placeholder="Đến ngày" />
                        </div>
                    </div>
                    <div class="center">
                        <a class="btn btn-primary" id="btnSearch" data-dismiss="modal">
                            <i class="ace-icon fa fa-search"></i>
                            Tìm
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <button class="aside-trigger btn btn-info btn-app btn-xs ace-settings-btn" data-target="#left-menu" data-toggle="modal" type="button">
            <i data-icon1="fa-search-plus" data-icon2="fa-search-minus" class="ace-icon fa bigger-110 icon-only fa-search-plus"></i>
        </button>
    </div>
    <div class="modal fade noprint" id="mReceipt" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title blue">Nhập thông tin thu</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group" id="divphong">
                                <label class="control-label">Phòng</label>
                                <asp:DropDownList ID="DDL_Department1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0" Text="-- Chọn phòng --" disabled="disabled" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label class="control-label">Người giao tiền</label>
                                        <asp:DropDownList ID="DDL_Employee2" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="control-label">Người nhận tiền</label>
                                        <asp:DropDownList ID="DDL_Employee1" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="-- Chọn nhân viên --" disabled="disabled" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label">Ngày thu/ nhận tiền</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="txt_ReceiptDate" role='datepicker' placeholder="Chọn" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung</label>
                                <input type="text" class="form-control" id="txt_ReceiptContents" placeholder="Nhập text" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <input type="text" class="form-control" id="txt_ReceiptAmount" placeholder="Nhập số" moneyinput="true" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Ghi chú</label>
                                <textarea class="form-control" id="txt_ReceiptDescription" placeholder="Nhập text" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveReceipt" data-dismiss="modal">
                    <i class="ace-icon fa fa-floppy-o"></i>
                    Lưu
                </button>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Category" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_FromDate" runat="server" />
    <asp:HiddenField ID="HID_ToDate" runat="server" />
    <asp:HiddenField ID="HID_NextPrev" runat="server" />
    <asp:HiddenField ID="HID_Department" runat="server" Value="0" />
    <asp:Button ID="btnView" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnView_Click" />
    <asp:Button ID="btnExport" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnExport_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/moment.min.js"></script>
    <script src="/template/Pikaday-master/pikaday.js"></script>
    <script src="Receipt.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <asp:Literal ID="LitScript" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $("#txtTuNgay").val(Page.getFirstDayMonth());
            $("#txtDenNgay").val(Page.getLastDayMonth());
        });
        //function SendMail(id) {
        //    $.ajax({
        //        type: 'POST',
        //        url: '/FNC/Receipt.aspx/SendMail',
        //        data: JSON.stringify({
        //            "noidung": id,
        //        }),
        //        contentType: 'application/json; charset=utf-8',
        //        dataType: 'json',
        //        beforeSend: function () {
        //        },
        //        success: function (msg) {
        //        },
        //        complete: function () {
        //        },
        //        error: function (xhr, ajaxOptions, thrownError) {
        //            console.log(xhr.status);
        //            console.log(xhr.responseText);
        //            console.log(thrownError);
        //        }
        //    });
        //}
    </script>
</asp:Content>
