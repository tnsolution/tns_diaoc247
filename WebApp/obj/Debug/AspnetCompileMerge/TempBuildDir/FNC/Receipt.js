﻿var picker = new Pikaday({
    field: document.getElementById('txtTuNgay'),
    format: 'DD/MM/YYYY',
    onSelect: function (date) {
        $("[id$=HID_FromDate]").val(picker.toString());
        console.log(picker.toString());
    }
});
var picker2 = new Pikaday({
    field: document.getElementById('txtDenNgay'),
    format: 'DD/MM/YYYY',
    onSelect: function (date) {
        $("[id$=HID_ToDate]").val(picker2.toString());
        console.log(picker2.toString());
    }
});
$(document).on('click', '.modal-backdrop.in', function (e) {
    //in ajax mode, remove before leaving page
    $('#closeLeft').trigger("click");
    //$(".modal-backdrop.in").remove();
});
$(document).ready(function () {
    $("#ViewPrevious").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=HID_NextPrev]").val(-1);
        $("[id$=btnView]").trigger("click");
    });
    $("#ViewNext").click(function () {
        $('.se-pre-con').fadeIn('slow');
        $("[id$=HID_NextPrev]").val(1);
        $("[id$=btnView]").trigger("click");
    });
    $("#txt_ReceiptDate").val(Page.getCurrentDate());
    $('[id$=DDL_Department1]').change(function () {
        var Key = $(this).val();
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetEmployees",
            data: JSON.stringify({
                DepartmentKey: $(this).val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                var District = $("[id$=DDL_Employee1]");
                District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                $.each(msg.d, function () {
                    District.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            },
            complete: function () {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("[id$=DDL_Department]").on('change', function (e) {
        $("[id$=HID_Department]").val(this.value);
    });
    $("input[moneyinput]").number(true, 0);
    $(".modal.aside").ace_aside();
    $(".select2").select2({ width: "100%" });
    //----
    $("#btnSearch").click(function () {
        var department = $("[id$=DDL_Department]").val();
        if (department == null)
            department = Page.getDepartment();
        var text = "Từ ngày: " + $("[id$=txtTuNgay]").val() + " đến ngày: " + $("[id$=txtDenNgay]").val();
        $("[id$=lbltime]").text(text);
        $.ajax({
            type: 'POST',
            url: '/FNC/Receipt.aspx/Search',
            data: JSON.stringify({
                'FromDate': $("[id$=txtTuNgay]").val(),
                'ToDate': $("[id$=txtDenNgay]").val(),
                'Department': department,
                'Category': $("[id$=HID_Category]").val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                $("#tbl").empty();
                $("#tbl").append($(msg.d.Result));
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#tblReceipt td").not(".noclick").on("click", function () {
        var trid = $(this).closest('tr').attr('id');
        $.ajax({
            type: 'POST',
            url: '/FNC/Receipt.aspx/GetReceipt',
            data: JSON.stringify({
                'AutoKey': trid,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d.Message != "") {
                    alert(msg.d.Message);
                    return false;
                }
                $('#mReceipt').modal('show');

                $("[id$=HID_AutoKey]").val(msg.d.AutoKey);
                $('[id$=DDL_Department1]').val(msg.d.DepartmentKey).trigger("change");
                $('[id$=DDL_Employee1]').val(msg.d.ReceiptFrom).trigger("change");
                $('[id$=DDL_Employee2]').val(msg.d.ReceiptBy).trigger("change");
                $('[id$=txt_ReceiptDate]').val(msg.d.ReceiptDate);
                $('[id$=txt_ReceiptContents]').val(msg.d.Contents);
                $('[id$=txt_ReceiptAmount]').val(msg.d.Amount);
                $('[id$=txt_ReceiptDescription]').val(msg.d.Description);
            },
            complete: function () {
                $('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnSaveReceipt").on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/FNC/Receipt.aspx/SaveReceipt',
            data: JSON.stringify({
                'AutoKey': $("[id$=HID_AutoKey]").val(),
                'Department': $('[id$=DDL_Department1]').val(),
                'Employee1': $('[id$=DDL_Employee1]').val(),
                'Employee2': $('[id$=DDL_Employee2]').val(),
                'Category': $("[id$=HID_Category]").val(),
                'Date': $('[id$=txt_ReceiptDate]').val(),
                'Contents': $('[id$=txt_ReceiptContents]').val(),
                'Description': $('[id$=txt_ReceiptDescription]').val(),
                'Amount': $('[id$=txt_ReceiptAmount]').val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (msg) {
                if (msg.d == 'OK') {
                    $("[id$=HID_AutoKey]").val(0);
                    location.href = '/FNC/Receipt.aspx?Category=' + $("[id$=HID_Category]").val();
                }
                else {
                    alert(msg.d);
                }
            },
            complete: function () {
                //$('.se-pre-con').fadeOut('slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnDownload").click(function () {
        $("[id$=btnExport]").trigger("click");
    });
});