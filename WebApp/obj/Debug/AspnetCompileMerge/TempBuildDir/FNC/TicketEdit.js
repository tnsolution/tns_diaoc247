﻿$(function () {
    $("#btnCollectInfo").click(function () {
        $("[id$=btnProcessFile]").trigger("click");
        //$.ajax({
        //    type: 'POST',
        //    url: '/FNC/TicketEdit.aspx/GenerateAll',
        //    data: JSON.stringify({
        //        'Key': $("[id$=HID_TicketKey]").val(),
        //    }),
        //    contentType: 'application/json; charset=utf-8',
        //    dataType: 'json',
        //    beforeSend: function () {
        //        $(".se-pre-con").fadeIn("slow");
        //    },
        //    success: function (r) {
        //        $("#allinfo").empty();
        //        $("#allinfo").append(r.d.Result);
        //    },
        //    complete: function () {
        //        $(".se-pre-con").fadeOut("slow");
        //    },
        //    error: function (xhr, ajaxOptions, thrownError) {
        //        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
        //        console.log(xhr.status);
        //        console.log(xhr.responseText);
        //        console.log(thrownError);
        //    }
        //});
    });
    $("input[moneyinput]").number(true, 0);
    $(".select2").select2({ width: "100%" });
    var category = $("[id$=HID_TicketCategory]").val();
    if (category == 228) {
        $("li[type='chuyennhuong']").show();
        $("li[type='chothue']").hide();
    }
    else {
        $("li[type='chothue']").show();
        $("li[type='chuyennhuong']").hide();
    }
    $("#btnSavePayment").click(function () {
        SavePayment();
    });
    $("#btnEdit").click(function () {
        SaveTicket();
    });
    $("[id$=DDL_AssetProject]").on('change', function (e) {
        var valueSelected = this.value;
        if (valueSelected != 0) {
            $.ajax({
                type: "POST",
                url: "/Ajax.aspx/GetCategoryAsset",
                data: JSON.stringify({
                    "ProjectKey": valueSelected,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    var District = $("[id$=DDL_AssetCategory]");
                    District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                    $.each(msg.d, function () {
                        var object = this;
                        if (object !== '') {
                            District.append($("<option></option>").val(object.Value).html(object.Text));
                        }
                    });
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    $("[id$=DDL_Department]").change(function () {
        var Key = $(this).val();
        $.ajax({
            type: "POST",
            url: "/Ajax.aspx/GetEmployees",
            data: JSON.stringify({
                DepartmentKey: $(this).val(),
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
            },
            success: function (msg) {
                var District = $("[id$=DDL_Employee]");
                District.empty().append('<option selected="selected" value="0" disabled="disabled">--Chọn--</option>');
                $.each(msg.d, function () {
                    District.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            },
            complete: function () {
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    $("#btnAddGuest").click(function () {
        $("#mOwner").modal('show');
        $("[id$=HID_CustomerEdit]").val(2);
    });
    //tim sp bang tblAssetSearch chọn bảng tblAssetSearch
    $("#btnAssetSearch").click(function () {
        var project = $("[id$=DDL_AssetProject]").val();
        var category = $("[id$=DDL_AssetCategory]").val();
        var assetID = $("#txt_AssetID").val();
        if (project == null)
            project = "0";
        if (category == null)
            category = "0";
        if (assetID == null)
            assetID = "";
        $('#tblAssetSearch > tbody > tr').remove();
        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit.aspx/AssetSearch",
            data: JSON.stringify({
                Project: project,
                Category: category,
                AssetID: assetID
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                for (var i = 0; i < msg.d.length; i++) {
                    var id = msg.d[i].AssetID;
                    var category = msg.d[i].CategoryName;
                    var area = msg.d[i].Area;
                    $('#tblAssetSearch > tbody:last').append('<tr class="" id="' + msg.d[i].AssetKey + '" type="' + msg.d[i].AssetType + '" style="cursor:pointer">'
                            + '<td>' + i++ + '</td>'
                            + '<td>' + id + '</td>'
                            + '<td>' + category + '</td>'
                            + '<td>' + area + '</td></tr>');
                }
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(".se-pre-con").fadeOut("slow");
                Page.showPopupMessage("...", "Lỗi thực hiện !." + xhr.responseText)
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //chon sp bang tblAssetSearch chon ra bang tblAsset
    $("#tblAssetSearch > tbody").on("click", "tr", function () {
        var id = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('type');
        var key = $("[id$=HID_TicketKey]").val();
        $.ajax({
            type: 'POST',
            url: '/FNC/TicketEdit.aspx/AssetSelect',
            data: JSON.stringify({
                'Key': key,
                'AssetKey': id,
                'Type': type
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                $('#mAssetFilter').modal('hide');
                $('#tblAsset > tbody > tr').remove();
                $('#tblAsset > tbody:last').append('<tr class="" id="' + msg.d.AssetKey + '" type="' + msg.d.AssetType + '">'
                        + '<td>#</td>'
                        + '<td>' + msg.d.ProjectName + '</td>'
                        + '<td>' + msg.d.AssetID + '</td>'
                        + '<td>' + msg.d.CategoryName + '</td>'
                        + '<td>' + msg.d.AreaWall + '</td>'
                        + '<td>' + msg.d.Address + '</td><td><div class="hidden-sm hidden-xs action-buttons pull-right"><a btn="btnDelAsset" href="#" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i> Xóa</a></div></td></tr>');
                Page.showNotiMessageInfo("..", "Đã cập nhật thông tin sản phẩm vào phiếu bán hàng.");

                $("[id$=HID_AssetType]").val(msg.d.AssetType);
                //get list owner
                ListOwner(id, msg.d.AssetID, type);

                $("[id$=txt_Amount]").val(msg.d.Price_VND);
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Lỗi phát sinh !');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //xóa sp bang data trong tblAsset
    $("#tblAsset > tbody").on("click", "a[btn='btnDelAsset']", function () {
        if (confirm("Bạn có muốn xóa sp ra khỏi phiếu bán hàng")) {
            var key = $("[id$=HID_TicketKey]").val();
            $.ajax({
                type: 'POST',
                url: '/FNC/TicketEdit.aspx/AssetDelete',
                data: JSON.stringify({
                    'Key': key,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Message != "") {
                        Page.showPopupMessage("Lỗi xóa thông tin.", msg.d.Message);
                    }
                    else {
                        $('#tblAsset> tbody > tr').remove();
                        Page.showNotiMessageInfo("..", "Đã xóa sản phẩm trong thông tin giao dịch.");
                    }
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //select trên tblAsset
    $("#tblAsset > tbody").on("click", "tr td:not(:last-child)", function () {
        var id = $(this).closest('tr').attr('id');
        var type = $(this).closest('tr').attr('type');
        var name = $(this).find('td').eq(2).text();
        ListOwner(id, name, type)

        $("[id$=HID_AssetKey]").val(id);
    });
    //chon khach từ bang tblOwnerList ra modal
    $("#tblOwnerList > tbody").on("click", "tr", function () {
        var id = $(this).closest('tr').attr('id');
        var fid = $(this).closest('tr').attr('fid');
        var type = $(this).closest('tr').attr('type');

        $.ajax({
            type: 'POST',
            url: '/FNC/TicketEdit.aspx/OneOwner',
            data: JSON.stringify({
                'OwnerKey': fid,
                'Type': type,
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (r) {
                var object = r.d;
                $("[id$=HID_CustomerKey]").val(object.CustomerKey);
                $("#txt_CustomerName").val(object.CustomerName);
                $("#txt_CardID").val(object.CardID);
                $("#txt_CardPlace").val(object.CardPlace);
                $("#txt_Phone1").val(object.Phone1);
                $("#txt_Phone2").val(object.Phone2);
                $("#txt_Address1").val(object.Address1);
                $("#txt_Address2").val(object.Address2);
                $("#txt_CardDate").val(object.CardDate);
                $("#txt_Birthday").val(object.Birthday);
                $("#chkIsOwner1").prop('checked', true);
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

        $('#mOwnerList').modal('hide');
        $('#mOwner').modal({
            backdrop: true,
            show: true
        });
    });
    //luu khach
    $("#btnSaveCustomer").click(function () {
        var edit = $("[id$=HID_CustomerEdit]").val();
        console.log('set truong hop luu khach ' + edit);
        switch (edit) {
            case 1:
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TicketEdit.aspx/SaveCustomer',
                    data: JSON.stringify({
                        'CustomerKey': $("[id$=HID_CustomerKey]").val(),
                        'AssetKey': $("[id$=HID_AssetKey]").val(),
                        'AssetType': $("[id$=HID_AssetType]").val(),
                        'CustomerName': $("#txt_CustomerName").val(),
                        'Phone': $("#txt_Phone1").val(),
                        'Phone2': $("#txt_Phone2").val(),
                        'Address': $("#txt_Address1").val(),
                        'Address2': $("#txt_Address2").val(),
                        'CardDate': $("#txt_CardDate").val(),
                        'CardID': $("#txt_CardID").val(),
                        'CardPlace': $("#txt_CardPlace").val(),
                        'Birthday': $("#txt_Birthday").val(),
                        'IsOwner': $('input[name=chkPurpose]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            Page.showNotiMessageInfo("...", "Lưu thông tin khách thành công");

                        ListGuest();
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                break;

            case 2:
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TicketEdit.aspx/SaveNewCustomer',
                    data: JSON.stringify({
                        'TicketKey': $("[id$=HID_TicketKey]").val(),
                        'CustomerName': $("#txt_CustomerName").val(),
                        'AssetKey': $("[id$=HID_AssetKey]").val(),
                        'AssetType': $("[id$=HID_AssetType]").val(),
                        'Phone': $("#txt_Phone1").val(),
                        'Phone2': $("#txt_Phone2").val(),
                        'Address': $("#txt_Address1").val(),
                        'Address2': $("#txt_Address2").val(),
                        'CardDate': $("#txt_CardDate").val(),
                        'CardID': $("#txt_CardID").val(),
                        'CardPlace': $("#txt_CardPlace").val(),
                        'Birthday': $("#txt_Birthday").val(),
                        'EmployeeKey': $("[id$=DDL_Employee]").val(),
                        'IsOwner': $('input[name=chkPurpose]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            Page.showNotiMessageInfo("...", "Lưu thông tin khách thành công");

                        ListGuest();
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                break;

            default:
                $.ajax({
                    type: 'POST',
                    url: '/FNC/TicketEdit.aspx/SaveGuest',
                    data: JSON.stringify({
                        'TicketKey': $("[id$=HID_TicketKey]").val(),
                        'OwnerKey': $("[id$=HID_OwnerKey]").val(),
                        'CustomerKey': $("[id$=HID_CustomerKey]").val(),
                        'AssetKey': $("[id$=HID_AssetKey]").val(),
                        'AssetType': $("[id$=HID_AssetType]").val(),
                        'CustomerName': $("#txt_CustomerName").val(),
                        'Phone': $("#txt_Phone1").val(),
                        'Phone2': $("#txt_Phone2").val(),
                        'Address': $("#txt_Address1").val(),
                        'Address2': $("#txt_Address2").val(),
                        'CardDate': $("#txt_CardDate").val(),
                        'CardID': $("#txt_CardID").val(),
                        'CardPlace': $("#txt_CardPlace").val(),
                        'Birthday': $("#txt_Birthday").val(),
                        'EmployeeKey': $("[id$=DDL_Employee]").val(),
                        'IsOwner': $('input[name=chkPurpose]:checked').val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $(".se-pre-con").fadeIn("slow");
                    },
                    success: function (r) {
                        if (r.d.Result > 0)
                            Page.showNotiMessageInfo("...", "Lưu thông tin khách thành công");

                        ListGuest();
                    },
                    complete: function () {
                        $(".se-pre-con").fadeOut("slow");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
                break;
        }

        $('#mOwner').modal('hide');
        $('#mGuestFilter').modal('hide');
        $("[id$=HID_CustomerEdit]").val(0);
        Page.resetValue("mOwner");
    });
    //xoa khach
    $("#tblCustomer > tbody").on("click", "a[btn='btnDelCustomer']", function () {
        if (confirm("Bạn có muốn xóa khách ra khỏi giao dịch")) {
            var key = $(this).closest('tr').attr('id');
            $.ajax({
                type: 'POST',
                url: '/FNC/TicketEdit.aspx/GuestDelete',
                data: JSON.stringify({
                    'TicketKey': $("[id$=HID_TicketKey]").val(),
                    'CustomerKey': key,
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                beforeSend: function () {
                    $(".se-pre-con").fadeIn("slow");
                },
                success: function (msg) {
                    if (msg.d.Message == "") {
                        $('#tblCustomer > tbody > tr[id=' + key + ']').remove();
                    }
                    else {
                        Page.showPopupMessage("Lỗi xóa thông tin !.", msg.d.Message);
                    }
                },
                complete: function () {
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    });
    //edit khach
    $("#tblCustomer > tbody").on("click", "tr td:not(:last-child)", function () {
        var id = $(this).closest('tr').attr('id');
        $("[id$=HID_CustomerEdit]").val(1);

        OpenGuest(id);
    });
    //tim khach vao bang tblGuestSearch
    $("#btnGuestSearch").click(function () {
        var name = $("#txt_Search_CustomerName").val();
        var phone = $("#txt_Search_CustomerID").val();

        $.ajax({
            type: "POST",
            url: "/FNC/TicketEdit.aspx/GuestSearch",
            data: JSON.stringify({
                Phone: phone,
                Name: name
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                $('#tblGuestSearch> tbody > tr').remove();
                var no = 1;
                for (var i = 0; i < msg.d.length; i++) {
                    $('#tblGuestSearch > tbody:last').append('<tr class="" ID="' + msg.d[i].CustomerKey + '">'
                            + '<td>' + (no++) + '</td>'
                            + '<td>' + msg.d[i].CustomerName + '</td>'
                            + '<td>' + msg.d[i].Phone1 + '<br/>' + msg.d[i].Phone2 + '</td></tr>');
                }
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Lỗi phát sinh !');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    });
    //chon khach da tim dc
    $("#tblGuestSearch > tbody").on("click", "tr", function () {
        var id = $(this).closest('tr').attr('id');
        OpenGuest(id);
    });
    $("#tblPayment > tbody").on("click", "tr td:not(:last-child)", function () {
        var id = $(this).closest('tr').attr('id');
        $("[id$=HID_PaymentKey]").val(id);

        OpenPayment(id);
    });
});
function ListGuest() {
    $("#tblCustomer > tbody").empty();
    $.ajax({
        type: "POST",
        url: "/FNC/TicketEdit.aspx/GetGuest",
        data: JSON.stringify({
            'TicketKey': $("[id$=HID_TicketKey]").val(),
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (msg) {
            var no = 1;
            for (var i = 0; i < msg.d.length; i++) {
                $('#tblCustomer > tbody:last').append('<tr class="" ID="' + msg.d[i].CustomerKey + '">'
                    + '<td>' + (no++) + '</td>'
                    + '<td>' + msg.d[i].CustomerName + '<br/>' + msg.d[i].Owner + '</td>'
                    + '<td>' + msg.d[i].Phone1 + '<br/>' + msg.d[i].Phone2 + '</td>'
                    + '<td>' + msg.d[i].Birthday + '</td>'
                    + '<td>' + msg.d[i].CardID + '<br/>Nơi cấp:' + msg.d[i].CardPlace + '</td>'
                    + '<td> Liên hệ: ' + msg.d[i].Address1 + '<br/>Thường trú:' + msg.d[i].Address2 + '</td>'
                    + '<td><div class="hidden-sm hidden-xs action-buttons pull-right"><a btn="btnDelCustomer" href="#" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i> Xóa</a></div></td></tr>');
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Lỗi phát sinh !');
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function ListOwner(id, name, type) {
    $.ajax({
        type: 'POST',
        url: '/FNC/TicketEdit.aspx/GetOwner',
        data: JSON.stringify({
            'AssetKey': id,
            'Type': type,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            if (r.d.length > 1) {
                var textmsg = "Lưu ý mã căn " + name + " có nhiều thông tin chủ nhà !. Vui lòng tự chọn thông tin.";
                textmsg += "(Thông tin mới gần nhất đầu tiên !.)";
                $("#note").text(textmsg);
                $("#tblOwnerList > tbody > tr").remove();
                for (var i = 0; i < r.d.length; i++) {
                    $("#tblOwnerList > tbody:last").append('<tr type=' + type + ' fid=' + r.d[i].OwnerKey + ' id=' + r.d[i].CustomerKey + '><td>' + i + '</td><td>' + r.d[i].CustomerName + '</td><td>' + r.d[i].Phone1 + '</td><td>' + r.d[i].CreatedDate + '</td></tr>');
                }

                $('#mOwnerList').modal({
                    backdrop: true,
                    show: true
                });
            }
            else {
                var object = r.d[0];
                if (object == undefined) {
                    alert("Lỗi thực hiện");
                    return false;
                }

                $("[id$=HID_OwnerKey]").val(object.OwnerKey);
                $("[id$=HID_CustomerKey]").val(object.CustomerKey);
                $("#txt_CustomerName").val(object.CustomerName);
                $("#txt_CardID").val(object.CardID);
                $("#txt_CardPlace").val(object.CardPlace);
                $("#txt_Phone1").val(object.Phone1);
                $("#txt_Phone2").val(object.Phone2);
                $("#txt_Address1").val(object.Address1);
                $("#txt_Address2").val(object.Address2);
                $("#txt_CardDate").val(object.CardDate);
                $("#txt_Birthday").val(object.Birthday);
                $("#chkIsOwner1").prop('checked', true);

                $('#mOwner').modal({
                    backdrop: true,
                    show: true
                });
            }
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function OpenGuest(id) {
    $.ajax({
        type: 'POST',
        url: '/FNC/TicketEdit.aspx/GetCustomer',
        data: JSON.stringify({
            'TicketKey': $("[id$=HID_TicketKey]").val(),
            'CustomerKey': id,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            var object = r.d;
            if (object == undefined) {
                alert("Lỗi thực hiện");
                return false;
            }

            $("[id$=HID_CustomerKey]").val(object.CustomerKey);
            $("#txt_CustomerName").val(object.CustomerName);
            $("#txt_CardID").val(object.CardID);
            $("#txt_CardPlace").val(object.CardPlace);
            $("#txt_Phone1").val(object.Phone1);
            $("#txt_Phone2").val(object.Phone2);
            $("#txt_Address1").val(object.Address1);
            $("#txt_Address2").val(object.Address2);
            $("#txt_CardDate").val(object.CardDate);
            $("#txt_Birthday").val(object.Birthday);
            if (object.IsOwner == 3)
                $("#chkIsOwner3").prop('checked', true);
            if (object.IsOwner == 2)
                $("#chkIsOwner2").prop('checked', true);
            if (object.IsOwner == 1)
                $("#chkIsOwner1").prop('checked', true);

            $('#mOwner').modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
function SaveTicket() {
    var TicketKey = $('[id$=HID_TicketKey]').val();
    var TicketDate = $('[id$=txt_DateCreate]').val();

    var TicketCategory = $('[id$=DDL_Category]').val();
    var EmployeeKey = $('[id$=DDL_Employee]').val();
    var DepartmentKey = $('[id$=DDL_Department]').val();

    var Amount = $('[id$=txt_Amount]').val();
    var PreOrderAmount = $('[id$=txt_PreOrderAmount]').val();
    var DateOrder = $('[id$=txt_DatePreOrder]').val();
    var ReferenceName = $('[id$=txt_ReferenceName]').val();
    var ReferencePercent = $('[id$=txt_ReferencePercent]').val();

    var TongGiaTriChuyenNhuong = $('[id$=txt_TongGiaTriChuyenNhuong]').val();
    var ThanhToanNhanSoHong = $('[id$=txt_ThanhToanNhanSo]').val();
    var BenBThanhToanBenA = $('[id$=txt_BthanhtoanA]').val();
    var GiaCongChung = $('[id$=txt_giacongchung]').val();
    var NgayCongChung = $('[id$=txt_ngaycongchung]').val();
    var PhiMoiGioiBenC = $('[id$=txt_phidichvuC]').val();
    var ThoaThuanChuyenNhuong = $('[id$=txt_ThoaThuanChuyenNhuong]').val();
    //    
    var SoTienThue = $('[id$=txt_SoTienThue]').val();
    var SoTienThueBangChu = $('[id$=txt_SoTienThueBangChu]').val();
    var SoTienThueGom = $('[id$=txt_SoTienThueGom]').val();
    var NgayCocThue = $('[id$=txt_NgayCocThue]').val();
    var TienCocThue = $('[id$=txt_TienCocThue]').val();
    var NgayThanhToanThue = $('[id$=txt_NgayThanhToanThue]').val();
    var SoTienThanhToanThue = $('[id$=txt_SoTienThanhToanThue]').val();
    var ThueTheo = $('[id$=DDLThueTheo]').val();
    var ChuyenTienTuNgay = $('[id$=txt_ChuyenTienTuNgay]').val();
    var ChuyenTienDenNgay = $('[id$=txt_ChuyenTienDenNgay]').val();
    var ThoiGianThue = $('[id$=txt_ThoiGianThue]').val();
    var ThueTuNgay = $('[id$=txt_ThueTuNgay]').val();
    var ThueDenNgay = $('[id$=txt_ThueDenNgay]').val();
    var NgayKyHopDong = $('[id$=txt_NgayKyHopDong]').val();
    var NgayBanGiao = $('[id$=txt_NgayBanGiao]').val();
    var NgayTinhTien = $('[id$=txt_NgayTinhTien]').val();
    var ThoaThuanChoThue = $('[id$=txt_ThoaThuanChoThue]').val();// thoa thuan khac
    ////
    console.log(TicketDate);
    console.log(DateOrder);
    console.log(TicketCategory);
    if (TicketCategory == 228) {
        $.ajax({
            type: 'POST',
            url: '/FNC/TicketEdit.aspx/Save_ChuyenNhuong',
            data: JSON.stringify({
                "TicketKey": TicketKey,
                "Employee": EmployeeKey,
                "Department": DepartmentKey,
                "TicketDate": TicketDate,
                "Amount": Amount,
                "PreOrderAmount": PreOrderAmount,
                "DateOrder": DateOrder,
                "ReferenceName": ReferenceName,
                "ReferencePercent": ReferencePercent,
                //
                "TongGiaTriChuyenNhuong": TongGiaTriChuyenNhuong,
                "ThanhToanNhanSoHong": ThanhToanNhanSoHong,
                "BenBThanhToanBenA": BenBThanhToanBenA,
                "GiaCongChung": GiaCongChung,
                "NgayCongChung": NgayCongChung,
                "PhiMoiGioiBenC": PhiMoiGioiBenC,
                'ThoaThuanChuyenNhuong': ThoaThuanChuyenNhuong,

            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d.Result > 0)
                    window.location = "/FNC/TicketEdit.aspx?ID=" + r.d.Result;
                else
                    Page.showPopupMessage("Lỗi khởi tạo thông tin", r.d.Message);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
                $('.se-pre-con').fadeOut('slow');
            },
            complete: function () {
            }
        });
    }
    if (TicketCategory == 227) {
        $.ajax({
            type: 'POST',
            url: '/FNC/TicketEdit.aspx/Save_ChoThue',
            data: JSON.stringify({
                "TicketKey": TicketKey,
                "Employee": EmployeeKey,
                "Department": DepartmentKey,
                "TicketDate": TicketDate,
                "Amount": Amount,
                "PreOrderAmount": PreOrderAmount,
                "DateOrder": DateOrder,
                "ReferenceName": ReferenceName,
                "ReferencePercent": ReferencePercent,
                //
                'SoTienThue': SoTienThue,
                'SoTienThueBangChu': SoTienThueBangChu,
                'SoTienThueGom': SoTienThueGom,
                'NgayCocThue': NgayCocThue,
                'TienCocThue': TienCocThue,
                'NgayThanhToanThue': NgayThanhToanThue,
                'SoTienThanhToanThue': SoTienThanhToanThue,
                'ThueTheo': ThueTheo,
                'ChuyenTienTuNgay': ChuyenTienTuNgay,
                'ChuyenTienDenNgay': ChuyenTienDenNgay,
                'ThoiGianThue': ThoiGianThue,
                'ThueTuNgay': ThueTuNgay,
                'ThueDenNgay': ThueDenNgay,
                'NgayKyHopDong': NgayKyHopDong,
                'NgayBanGiao': NgayBanGiao,
                'NgayTinhTien': NgayTinhTien,
                'ThoaThuanChoThue': ThoaThuanChoThue,

            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $('.se-pre-con').fadeIn('slow');
            },
            success: function (r) {
                if (r.d.Result > 0)
                    window.location = "/FNC/TicketEdit.aspx?ID=" + r.d.Result;
                else
                    Page.showPopupMessage("Lỗi khởi tạo thông tin", r.d.Message);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
                $('.se-pre-con').fadeOut('slow');
            },
            complete: function () {
            }
        });
    }
}
function SavePayment() {

    $.ajax({
        type: 'POST',
        url: '/FNC/TicketEdit.aspx/SavePayment',
        data: JSON.stringify({
            'AutoKey': $("[id$=HID_PaymentKey]").val(),
            'TicketKey': $("[id$=HID_TicketKey]").val(),
            'Title': $("[id$=txt_TieuDe]").val(),
            'TimePayment': $("[id$=txt_Ngay]").val(),
            'Amount': $("[id$=txt_Sotien]").val(),
            'Description': $("[id$=txt_noidungDot]").val(),
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            $("#bangPayment").empty();
            $("#bangPayment").append($(r.d.Result));
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });

    $("[id$=HID_PaymentKey]").val(0);
}
function DeletePayment() {
    if (confirm("Bạn có muốn xóa !.")) {
        var AutoKey = $(this).closest('tr').attr('id');
        $.ajax({
            type: 'POST',
            url: '/FNC/TicketEdit.aspx/DeletePayment',
            data: JSON.stringify({
                'AutoKey': AutoKey,
                'TicketKey': $("[id$=HID_TicketKey]").val(),
            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function () {
                $(".se-pre-con").fadeIn("slow");
            },
            success: function (msg) {
                if (msg.d.Message == "") {
                    $('#tblPayment > tbody > tr[id=' + key + ']').remove();
                }
                else {
                    Page.showPopupMessage("Lỗi xóa thông tin !.", msg.d.Message);
                }
            },
            complete: function () {
                $(".se-pre-con").fadeOut("slow");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Lỗi phát sinh !');
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}
function OpenGuest(id) {
    $.ajax({
        type: 'POST',
        url: '/FNC/TicketEdit.aspx/GetPayment',
        data: JSON.stringify({
            'TicketKey': $("[id$=HID_TicketKey]").val(),
            'AutoKey': id,
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function () {
            $(".se-pre-con").fadeIn("slow");
        },
        success: function (r) {
            var object = r.d;
            if (object == undefined) {
                alert("Lỗi thực hiện");
                return false;
            }

            $("#txt_Ngay").val(object.TimePayment);
            $("#txt_TieuDe").val(object.Title);
            $("#txt_Sotien").val(object.Amount);
            $("#txt_noidungDot").val(object.Description);

            $('#mPayment').modal({
                backdrop: true,
                show: true
            });
        },
        complete: function () {
            $(".se-pre-con").fadeOut("slow");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            Page.showPopupMessage("Lỗi thực hiện", "Thực thi lệnh lấy khách hàng không thành công");
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });
}
//