﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ViewChart.aspx.cs" Inherits="WebApp.FNC.ViewChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <style>
        .widget-main {
            padding: 5px !important;
        }

        .col-xs-2 {
            padding: 0px !important;
        }

        .scroll-content {
            overflow: hidden !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="col-xs-2">
            <div class="widget-box ui-sortable-handle">
                <div class="widget-header widget-header-flat widget-header-small">
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="form-group">
                            <label>Ngày tháng năm</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                    id="txtFromDate" placeholder="Từ " />
                            </div>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" runat="server" role="datepicker" class="form-control pull-right"
                                    id="txtToDate" placeholder="Đến " />
                            </div>
                        </div>
                        <div class="form-group" id="quanly">
                            <label>Phòng, nhân viên</label>
                            <asp:DropDownList ID="DDL_Department" runat="server" class="select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Tất cả phòng--" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="DDL_Employee" runat="server" class="select2" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Tất cả chuyên viên--" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="align-center">
                            <button type="button" class="btn btn-primary" id="btnSearch" runat="server" onserverclick="btnSearch_ServerClick" onclick="$('.se-pre-con').fadeIn('slow');">
                                <i class="ace-icon fa fa-search"></i>
                                Tìm
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-md-6">
                    <div class="widget-box widget-color-orange">
                        <div class="widget-header widget-header-flat widget-header-small">
                            <h3 class="widget-title" style="color: white;">Số giao dịch:
                                <asp:Label ID="lblNumber" runat="server" Text="0"></asp:Label></h3>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="chart">
                                    <canvas id="chart-area1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="widget-box widget-color-orange">
                        <div class="widget-header widget-header-flat widget-header-small">
                            <h3 class="widget-title" style="color: white;">Doanh thu (VNĐ):
                                <asp:Label ID="lblFee" runat="server" Text="0"></asp:Label></h3>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="chart">
                                    <canvas id="chart-area2" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-box ui-sortable-handle">
                        <div class="widget-header widget-header-flat widget-header-small">
                            <h3 class="widget-title">Chi tiết</h3>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main" id="parent" style="padding: 0px;">
                                <asp:Literal ID="Literal_Table" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script type="text/javascript" src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript" src="/template/chartjs/Chart.js"></script>
    <script type="text/javascript" src="/template/tableHeadFixer.js"></script>
    <asp:Literal ID="LitDataChart" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $('#parent').ace_scroll({
                size: 250
            });
            $('#tableData').tableHeadFixer();
            $("#tableData tbody").on("click", "tr", function () {
                var trid = $(this).closest('tr').attr('id');
                window.location = "/FNC/TradeView.aspx?ID=" + trid;
            });
            $(".select2").select2({ width: "100%" });
            $("[id$=DDL_Department]").on('change', function (e) {
                var valueSelected = this.value;
                if (valueSelected != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/Ajax.aspx/GetEmployees",
                        data: JSON.stringify({
                            "DepartmentKey": valueSelected,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {

                        },
                        success: function (msg) {
                            var District = $("[id$=DDL_Employee]");
                            District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Nhân viên--</option>');
                            $.each(msg.d, function () {
                                var object = this;
                                District.append($("<option></option>").val(this['Value']).html(this['Text']));
                            });
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });

            window.onload = function () {
                var ctxB = document.getElementById("chart-area1").getContext("2d");
                //ctxB.height = 300;
                window.myBar = new Chart(ctxB).Bar(areaChartData1, {
                    responsive: true,
                    animation: true,
                    maintainAspectRatio: true,
                });

                var ctxB1 = document.getElementById("chart-area2").getContext("2d");
                //ctxB1.height = 300;
                window.myBar = new Chart(ctxB1).Bar(areaChartData2, {
                    responsive: true,
                    animation: true,
                    maintainAspectRatio: true,
                    scaleLabel: function (obj) { return '(VNĐ) ' + obj.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); },
                    tooltipTemplate: function (obj) { return Page.FormatMoney(obj.value); },
                });
            }
        });
    </script>
</asp:Content>
