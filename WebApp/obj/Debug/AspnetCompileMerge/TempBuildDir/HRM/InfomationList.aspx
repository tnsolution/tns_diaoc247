﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="InfomationList.aspx.cs" Inherits="WebApp.HRM.InfomationList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Thông tin nội bộ
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#faq-tab-1" class="center">
                                <i class="blue ace-icon fa fa-question-circle bigger-120"></i>
                                Giới thiệu
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#human" class="center">
                                <i class="green ace-icon fa fa-folder bigger-120"></i>
                                Nhân sự
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#rule" class="center">
                                <i class="orange ace-icon fa fa-archive bigger-120"></i>
                                Nội quy
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" class="center" href="#policy">
                                <i class="purple ace-icon fa fa-file bigger-120"></i>
                                Chính sách
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" class="center" href="#web">
                                <i class="purple ace-icon fa fa-globe bigger-120"></i>
                                Hệ thống web
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="faq-tab-1" class="tab-pane in active">
                            <div id="faq-list-1" class="panel-group accordion-style1 accordion-style2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a href="#faq-1-1" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle" aria-expanded="true">
                                            <i class="pull-right ace-icon fa fa-chevron-down" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>
                                            <i class="ace-icon fa fa-info bigger-130"></i>
                                            &nbsp; Giới thiệu về ĐỊA ỐC 247
                                        </a>
                                    </div>
                                    <div class="panel-collapse collapse in" id="faq-1-1" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <dl>
                                                <dd>Được thành lập vào ngày 11 tháng 11 năm 2014, với phương châm hướng tới sự chuyên nghiệp và minh bạch trong thông tin chuyên ngành của từng Dự án Bất Động Sản, nhằm góp phần phát triển thị trường bất động sản tại Thành Phố Hồ Chí Minh nói riêng và tại Việt Nam nói chung</dd>
                                                <dd>Công Ty Cổ Phần Đầu Tư <b>ĐỊA ỐC 247</b> hoạt động chuyên sâu trong lĩnh vực <b>Vận hành – Khai thác cho thuê & Quản Lý: “Căn hộ – Căn hộ dịch vụ”</b> tại Thành phố Hồ Chí Minh. Bên cạnh đó dịch vụ Chuyển nhượng & Phân phối dự án cho các Chủ đầu tư luôn là thế mạnh của Công Ty</dd>
                                                <center style="margin: 10px"><img src="/upload/jpg.jpg" alt="" /></center>
                                                <dd>Lấy Khách Hàng làm trọng tâm qua đó những dự án Công Ty lựa chọn phân phối luôn đảm bảo tính <b>An Toàn</b> về Pháp Lý; mức độ <b>Uy Tín</b> của <b>Chủ Đầu Tư và Giá Trị Gia Tăng</b> cho từng sản phẩm sao cho phù hợp và đáp ứng đúng nhu cầu đa dạng của từng Khách Hàng</dd>
                                                <dd><b>“Chính Trực – Kỷ Luật – Phát Triển”</b> là 3 yếu tố nhân sự cần có mà Công Ty luôn đặt quan tâm hàng đầu cho việc lựa chọn và đào tạo nhân sự có chất lượng, có cùng tâm huyết và mục tiêu chung</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="policy" class="tab-pane">
                            <div>
                                <!--chinhsach---><iframe src="http://docs.google.com/gview?url=http://crm.diaoc247.vn/Upload/Company/Rule/Chính sách 1.2018.pdf&embedded=true" 
                                    style="width: 100%; height: 450px;" frameborder="0"></iframe>
                            </div>
                        </div>
                        <div id="rule" class="tab-pane">
                            <div>
                                <!--noiquy---><iframe src="http://docs.google.com/gview?url=http://crm.diaoc247.vn/Upload/Company/Rule/Nội quy vào quy tắc ứng xử nội bộ.pdf&embedded=true" 
                                    style="width: 100%; height: 450px;" frameborder="0"></iframe>
                            </div>
                        </div>
                        <div id="web" class="tab-pane">
                            <div id="faq-list-2" class="panel-group accordion-style1 accordion-style2">
                                <asp:Literal ID="Lit_Web" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div id="human" class="tab-pane">
                            <div id="faq-list-3" class="panel-group accordion-style1 accordion-style2">
                                <asp:Literal ID="Lit_Staff" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
