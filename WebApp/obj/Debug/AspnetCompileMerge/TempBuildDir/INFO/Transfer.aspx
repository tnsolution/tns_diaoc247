﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="Transfer.aspx.cs" Inherits="WebApp.INFO.Transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/CheckCSS.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }

        .form-group {
            margin-bottom: 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>Giao nhận sản phẩm...
                <span class="pull-right">
                    <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <asp:Literal ID="LitMessage" runat="server"></asp:Literal>
                <table class='table table-hover table-bordered' id='tblData'>
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã sản phẩm</th>
                            <th>Loại sản phẩm</th>
                            <th>Quản lý trước đó</th>
                            <th>Tình trạng thông tin</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID="LitTable" runat="server"></asp:Literal>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <asp:Button ID="btnConfirm" runat="server" Text="..." Style="display: none; visibility: hidden" OnClick="btnConfirm_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script>
        $(function () {
            $(".select2").select2({ width: "100%" });
            $("#confirm").click(function () {
                $("[id$=btnConfirm]").trigger("click");
            });
        });
    </script>
</asp:Content>
