﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebApp.Master" AutoEventWireup="true" CodeBehind="ProjectEdit.aspx.cs" Inherits="WebApp.SAL.ProjectEdit" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/template/ace-master/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="/template/ace-master/assets/css/colorbox.min.css" />
    <style>
        .profile-info-name {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPage" runat="server">
    <div class="page-content">
        <div class="page-header">
            <h1>
                <asp:Literal ID="Lit_TitlePage" runat="server"></asp:Literal>
                <span class="tools pull-right">
                    <button type="button" class="btn btn-white btn-info btn-bold" id="btnSave">
                        <i class="ace-icon fa fa-floppy-o blue"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-white btn-warning btn-bold" id="btnDelete">
                        <i class="ace-icon fa fa-trash-o orange2"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-white btn-default btn-bold" onclick="javascript:history.back(); return false;">
                        <i class="ace-icon fa fa-reply blue"></i>
                        Trở về
                    </button>
                </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-9">
                <div class="tabbable tabs-left" id="Tabs">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#info">
                                <i class="pink ace-icon fa fa-briefcase bigger-110"></i>
                                Thông tin cơ bản
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#info2">
                                <i class="blue ace-icon fa fa-circle bigger-110"></i>
                                Ghi chú
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#fee">
                                <i class="blue ace-icon fa fa-circle bigger-110"></i>
                                Các loại phí
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#categroyAsset">
                                <i class="blue ace-icon fa fa-list-alt bigger-110"></i>
                                Các loại sản phẩm
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#asset">
                                <i class="blue ace-icon fa fa-list bigger-110"></i>
                                Sản phẩm cơ bản
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#file">
                                <i class="blue ace-icon fa fa-folder bigger-110"></i>
                                Tập tin
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#picture">
                                <i class="blue ace-icon fa fa-picture-o bigger-110"></i>
                                Hình ảnh
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#map" id="showMap">
                                <i class="blue ace-icon fa fa-map bigger-110"></i>
                                Vị trí bản đồ
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#allow">
                                <i class="blue ace-icon fa fa-circle bigger-110"></i>
                                Phân cho phòng ban
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="info" class="tab-pane in active">
                            <div class="col-xs-12">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tên dư án</label>
                                        <div class="col-sm-4">
                                            <input id="txt_ProjectName" runat="server" class="form-control" placeholder="Nhập text" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Chủ đầu tư</label>
                                        <div class="col-sm-4">
                                            <input id="txt_Investor" runat="server" class="form-control" placeholder="Nhập text" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Đơn giá trung bình</label>
                                        <div class="col-sm-4">
                                            <input id="txt_BasicPrice" runat="server" class="form-control" placeholder="Nhập số $$" type="text" moneyonly='true' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tỉnh/ Thành phố</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_Province" runat="server" CssClass="select2"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Quận/ Huyện</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DDL_District" runat="server" CssClass="select2"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Số, đường</label>
                                        <div class="col-sm-4">
                                            <input type="text" runat="server" class="form-control" id="txt_Address" placeholder="Nhập text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Địa chỉ đầy đủ</label>
                                        <div class="col-sm-10">
                                            <input type="text" runat="server" class="form-control" id="txt_AddressFull" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="info2" class="tab-pane">
                            <textarea id="txt_Content" cols="20" rows="10" runat="server"></textarea>
                        </div>
                        <div id="categroyAsset" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Chọn các loại sản phẩm thuộc dự án này (căn hộ, office-tel, ...) </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <asp:DropDownList ID="DDL_Category" runat="server" CssClass="select2">
                                        <asp:ListItem Value="0" Text="--Chọn--" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-8">
                                    <button type="button" class="btn btn-white btn-sm btn-default btn-round pull-right" id="btnSaveCategory">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Add
                                    </button>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_Category" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="asset" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Các sản phẩm cơ bản thuộc dự án
                                            <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#mAsset" data-toggle="modal">
                                                <i class="ace-icon fa fa-plus"></i>
                                                Add
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_Product" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="file" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-danger list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Các tập tin của dự án, khi tải tập tin vui lòng chọn mục tập tin (giới thiệu, pháp lý, ...) cần tải lên, chỉ cho phép các tập tin văn bản</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <input type="file" id="id-input-file-1" name="id-input-file-1" multiple="" />
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="">Thu mục</label>
                                        <asp:DropDownList ID="DDL_Folder" runat="server" CssClass="select2">
                                            <asp:ListItem Value="0" Text="--Chọn thư mục tập tin--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label class="">Diễn giải tập tin</label>
                                        <asp:TextBox ID="txt_Description" runat="server" CssClass="form-control" placeholder="Diễn giải tập tin, hình ảnh, ..."></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadFile">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Upload
                                    </a>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="accordion" class="accordion-style1 panel-group">
                                        <asp:Literal ID="Lit_Folder" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="picture" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Các hình ảnh của dự án, khi tải hình vui lòng chọn mục hình ảnh cần tải lên</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <input type="file" id="id-input-file-2" name="id-input-file-2" multiple="" />
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="">Thu mục</label>
                                        <asp:DropDownList ID="DDL_Folder2" runat="server" CssClass="select2">
                                            <asp:ListItem Value="0" Text="--Chọn thư mục tập tin--" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <label class="">Diễn giải tập tin</label>
                                        <asp:TextBox ID="txt_Description2" runat="server" CssClass="form-control" placeholder="Diễn giải tập tin, hình ảnh, ..."></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" data-toggle="modal" id="UploadImg">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Upload
                                    </a>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="accordion2" class="accordion-style1 panel-group">
                                        <asp:Literal ID="Lit_ListPicture" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="map" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-green list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Chỉ vị trí dự án trên bản đồ google map
                                            <span class="pull-right">
                                                <a class="btn btn-white btn-sm btn-default btn-round pull-right" href="#" id="UpdateLocation"><i class="ace-icon fa fa-pencil"></i>Cập nhật vị trí</a></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="googleMap" style="width: 100%; height: 400px;"></div>
                                </div>
                            </div>
                        </div>
                        <div id="allow" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Cho phép phòng ban chia sẽ được tìm kiếm sản phẩm của dự án</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="DDL_Department" runat="server" class="form-control select2" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="--Chọn--" Selected="True" Disable="disabled"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-8">
                                            <button type='button' class='btn btn-white btn-default btn-round btn-sm' id='btnSaveShare'>
                                                <i class="ace-icon fa fa-plus"></i>Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_AllowSearch" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div id="fee" class="tab-pane">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="text-warning list-unstyled spaced">
                                        <li><i class="ace-icon fa fa-exclamation-triangle"></i>Nhập các loại chi phí</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <input id="txt_Name" class="form-control" placeholder="Chi phí" type="text" />
                                </div>
                                <div class="col-xs-3">
                                    <input id="txt_Money" class="form-control" placeholder="Số tiền $$" type="text" moneyonly='true' />
                                </div>
                                <div class="col-xs-4">
                                    <input id="txt_FeeDescription" class="form-control" placeholder="Diễn giải" type="text" />
                                </div>
                                <div class="col-xs-1">
                                    <button type="button" class="btn btn-white btn-sm btn-default btn-round pull-right" id="btnSaveFee">
                                        <i class="ace-icon fa fa-plus"></i>
                                        Add
                                    </button>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:Literal ID="Lit_Fee" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title">Thông tin</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-sm-12" id="tableRoles">
                                    <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mAsset">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Cơ cấu sản phẩm</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Loại sản phẩm</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="DDL_AssetCategory" runat="server" CssClass="select2"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Số phòng ngủ</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_BedRoom" role="numeric" placeholder="Chỉ nhập số" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Diện tích (m2)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_AssetArea" role="float" placeholder="Chỉ nhập số" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Giá bán (VNĐ)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_AssetAmount" placeholder="Chỉ nhập số" moneyonly='true' />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Giá thuê (VNĐ)</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_AssetAmount2" placeholder="Chỉ nhập số" moneyonly='true' />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Diễn giải</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="txt_AssetDescription" placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveAsset" data-dismiss="modal">
                        Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_LatLng" runat="server" Value="0" />
    <asp:Button ID="btnUploadFile" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadFile_Click" />
    <asp:Button ID="btnUploadImg" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnUploadImg_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeFsZQuxAyCGnlcXLu5CyOfxwXfKtVxD0"></script>
    <script src="/template/ace-master/plugins/select2/select2.full.min.js"></script>
    <script src="/template/ace-master/assets/js/jquery.colorbox.min.js"></script>
    <script src="/template/jquery.number.min.js"></script>
    <script src="/template/tinymce/tinymce.min.js"></script>
    <asp:Literal ID="Lit_Script" runat="server"></asp:Literal>
    <script src="ProjectEdit.js"></script>
    <!--tiny mce-->
</asp:Content>
