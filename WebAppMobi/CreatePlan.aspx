﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CreatePlan.aspx.cs" Inherits="WebAppMobi.CreatePlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/themes/plugins/datepicker/datepicker3.css" />
    <link rel="stylesheet" href="/themes/jquery.timepicker.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content-header">
            <h1 id="tieude">Kế hoạch cho ngày
            </h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="box box-success flat">
                    <div class="box-header with-border" style="padding: 0px !important;">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="datepicker" placeholder="Chọn ngày tháng năm" />
                        </div>
                        <div id="datepairExample">
                            <input type="text" class="form-control flat time start" placeholder="-Từ giờ-" id="txt_Fromhours" style="float: left; width: 50%" />
                            <input type="text" class="form-control flat time end" placeholder="-Đến giờ-" id="txt_Tohours" style="float: right; width: 50%" />
                        </div>
                        <div class="input-group flat">
                            <input type="text" class="form-control flat" placeholder="Nội dung" id="txt_Content" />
                            <span class="input-group-btn flat">
                                <button type="button" class="btn btn-info btn-flat" id="addrow">Thêm !</button>
                            </span>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="divdata">
                            <asp:Literal ID="LiteralMeesage" runat="server"></asp:Literal>
                            <asp:Literal ID="LiteralEditTable" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="box-footer">
                        <asp:Button ID="btnSave" runat="server" Text="Lưu" OnClientClick="savePlan()" OnClick="btnSave_Click" />
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HFTimeKey" runat="server" Value="0" />
    <asp:HiddenField ID="HFDatePicker" runat="server" Value="0" />
    <asp:HiddenField ID="HFData" runat="server" Value="0" />
    <script type="text/javascript" src="/themes/mindmup-editabletable.js"></script>
    <script type="text/javascript" src="/themes/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="/themes/datepair.js"></script>
    <script type="text/javascript" src="/themes/jquery.datepair.js"></script>
    <script type="text/javascript" src="/themes/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        function savePlan() {
            var total = $('#tableEdit tbody tr').length;
            var num = 0;
            var tabledata = '';
            var id = $('[id$=HFTimeKey]').val();

            $('#tableEdit tbody tr').each(function () {
                num++;
                if (num == total) {
                    $(this).find('td').each(function () {
                        tabledata += $(this).html() + ',';
                    });
                }
                else {
                    $(this).find('td').each(function () {
                        tabledata += $(this).html() + ',';
                    });
                    tabledata += ';';
                }
            });

            $('[id$=HFDatePicker]').val($("#datepicker").val());
            $('[id$=HFData]').val(tabledata);
        }
        function getNextWorkDate() {
            var output = "";
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate() + 1;

            if (d.getDay() == 6) {
                day = d.getDate() + 2;
                output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
            }
            else {
                output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
            }

            return output;
        }

        $(document).ready(function () {
            $('#datepicker').val(getNextWorkDate());
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                startDate: '01/01/1900',
            }).change(function () {
                $("#tieude").text("Kế hoạch ngày " + $('#datepicker').val());
            });
            $("#tieude").text("Kế hoạch ngày " + $('#datepicker').val());
            $('#txt_Fromhours').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });
            $('#txt_Tohours').timepicker({
                'showDuration': true,
                'timeFormat': 'H:i',
                'show2400': true
            });
            $('#datepairExample').datepair({ 'defaultTimeDelta': 1800000 });
            $('#tableEdit').editableTableWidget();
            $('#addrow').click(function () {
                var num = $('#tableEdit tr').length;

                $('#tableEdit tbody').append('<tr><td tabindex=1>' + (num++) + '</td><td tabindex=1>' + $('#txt_Fromhours').val() + '-' + $('#txt_Tohours').val() + '</td><td tabindex=1>' + $('#txt_Content').val() + '</td></tr>');

                var tu = $('#txt_Fromhours').val();
                var den = $('#txt_Tohours').val();
                $('#txt_Fromhours').val(den);
                $('#txt_Content').val("");
            });
        });
    </script>
</asp:Content>
