﻿using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class CreateReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Init_TableReport();
            }
        }
        void Init_TableReport()
        {
            StringBuilder zSb = new StringBuilder();

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Report = Report_Data.Exits(Employee);
            if (Report > 0)
            {
                #region [update]
                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <td class='clsContent'>Ngày</td>");
                zSb.AppendLine("        <td>" + DateTime.Now.ToString("dd/MM/yyyy") + "</td>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <td>Nhân viên</td>");
                zSb.AppendLine("        <td>" + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "</td>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("</table>");

                zSb = new StringBuilder();

                Report_Info zInfo = new Report_Info(Report);
                DataTable zChild = Report_Rec_Data.List(zInfo.Key, zInfo.ReportDate.Month, zInfo.ReportDate.Year);
                int i = 1;
                zSb.AppendLine("<table id='reportData' class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th class='clsNo'>STT</th>");
                zSb.AppendLine("        <th style='display:none'></th>");
                zSb.AppendLine("        <th>Chỉ tiêu</th>");
                zSb.AppendLine("        <th>Kết quả</th>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("        <tbody>");

                foreach (DataRow r in zChild.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td style='display:none'>" + r["CategoryKey"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["Result"].ToString() + "</td>");
                    zSb.AppendLine("    </tr>");
                }

                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                #endregion                

                txt_ReportNote.Value = zInfo.Description;
            }
            else
            {
                #region [new]
                zSb.AppendLine("<table class='table table-border'>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <td class='clsContent'>Ngày</td>");
                zSb.AppendLine("        <td>" + DateTime.Now.ToString("dd/MM/yyyy") + "</td>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <td>Nhân viên</td>");
                zSb.AppendLine("        <td>" + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "</td>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("</table>");

                LitTitleReport.Text = zSb.ToString();

                zSb = new StringBuilder();
                DataTable zTable = Plan_Rec_Data.List(Employee, DateTime.Now.Month, DateTime.Now.Year);
                int i = 1;
                zSb.AppendLine("<table id='reportData' class='table table-border'>");
                zSb.AppendLine("    <thead>");
                zSb.AppendLine("        <th class='clsNo'>STT</th>");
                zSb.AppendLine("        <th style='display:none'></th>");
                zSb.AppendLine("        <th>Chỉ tiêu</th>");
                zSb.AppendLine("        <th>Kết quả</th>");
                zSb.AppendLine("    </thead>");
                zSb.AppendLine("        <tbody>");

                foreach (DataRow r in zTable.Rows)
                {
                    zSb.AppendLine("    <tr>");
                    zSb.AppendLine("        <td>" + (i++).ToString() + "</td>");
                    zSb.AppendLine("        <td style='display:none'>" + r["CategoryKey"].ToString() + "</td>");
                    zSb.AppendLine("        <td>" + r["CategoryName"].ToString() + "</td>");
                    zSb.AppendLine("        <td>0</td>");
                    zSb.AppendLine("    </tr>");
                }

                zSb.AppendLine("    </tbody>");
                zSb.AppendLine("</table>");
                #endregion                
            }
          
            HFReportKey.Value = Report.ToString();
            LitDataReport.Text = zSb.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int Report = HFReportKey.Value.ToInt();
            string tabledata = HFData.Value;
            string note = txt_ReportNote.Value;
            string Message = "";
            if (Report > 0)
            {
                int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
                string Name = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();

                Report_Info zInfo = new Report_Info(Report);
                zInfo.EmployeeKey = Employee;
                zInfo.DepartmentKey = Department;
                zInfo.CreatedBy = Employee.ToString();
                zInfo.ModifiedBy = Employee.ToString();
                zInfo.CreatedName = Name;
                zInfo.ModifiedName = Name;
                zInfo.Description = note;
                zInfo.Save();

                Message = new Report_Rec_Info().Delete(Report);
                string SQL = "";
                string[] row = tabledata.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in row)
                {
                    string[] col = item.Split(',');

                    SQL += " INSERT INTO TASK_Report_Detail (ReportKey, CategoryKey, CategoryName, Result) VALUES ( " + Report + " ,N'" + col[1] + "',N'" + col[2] + "',N'" + col[3] + "') \r\n";
                }

                if (SQL != string.Empty)
                {
                    Message = CustomInsert.Exe(SQL).Message;
                    if (Message != string.Empty)
                        LitMessage.Text = "Lỗi khi update data !" + Message;
                }

                Response.Redirect("Target.aspx");
            }
            else
            {
                #region Luu mới          
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
                string DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"];
                string Name = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
                string SQL = "";
                string Start = DateTime.Now.ToString("yyyy/MM/dd");
                string End = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");

                SQL += @"
Declare @ID INT;
INSERT INTO TASK_Report (EmployeeKey, DepartmentKey, ReportDate, Title, [Start], [End], Allday, Description, CreatedDate, CreatedBy, CreatedName, ModifiedDate, ModifiedBy, ModifiedName) 
VALUES (" + EmployeeKey + "," + DepartmentKey + ", GETDATE(),N'" + Name + "','" + Start + "','" + End + "',1, N'" + note + "' ,GETDATE()," + EmployeeKey + ",N'" + Name + "',GETDATE()," + EmployeeKey + ",N'" + Name + "')";

                SQL += " SET @ID = (SELECT ReportKey FROM TASK_Report WHERE ReportKey = SCOPE_IDENTITY())";

                //Task_Report_Info zInfo = new Task_Report_Info();
                string[] row = tabledata.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in row)
                {
                    string[] col = item.Split(',');

                    SQL += " INSERT INTO TASK_Report_Detail (ReportKey, CategoryKey, CategoryName, Result) VALUES ( @ID ,N'" + col[1] + "',N'" + col[2] + "',N'" + col[3] + "') \r\n";
                }
                if (SQL != string.Empty)
                {
                    Message = CustomInsert.Exe(SQL).Message;
                    if (Message != string.Empty)
                        LitMessage.Text = "Lỗi khi update data !" + Message;
                }

                Response.Redirect("Target.aspx");
                #endregion
            }
        }
    }
}