﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="WebAppMobi.Customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="box box-success flat">
                    <div class="box-header with-border" style="padding: 0px !important;">
                        <div style="width: 50%; float: left">
                            <asp:DropDownList ID="DDL_Project" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="" Text="--Chọn dự án--" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div style="width: 50%; float: right">
                            <asp:DropDownList ID="DDL_Number" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="" Text="--Chọn phòng ngủ--" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="--1--"></asp:ListItem>
                                <asp:ListItem Value="2" Text="--2--"></asp:ListItem>
                                <asp:ListItem Value="3" Text="--3--"></asp:ListItem>
                                <asp:ListItem Value="4" Text="--4--"></asp:ListItem>
                                <asp:ListItem Value="5" Text="--5--"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="input-group flat" style="width: 50%; float: right">
                            <input type="text" class="form-control flat" placeholder="Tên khách có dấu" id="txt_Search" runat="server" />
                            <span class="input-group-btn flat">
                                <button type="button" class="btn btn-info btn-flat" id="btnSearch" loading="yes">Tìm !</button>
                            </span>
                        </div>

                        <div style="width: 50%; float: right">
                            <asp:DropDownList ID="DDL_Category" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="--Chọn loại sản phẩm--" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="divdata">
                            <asp:Literal ID="LiteralData" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>

    <script>
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        $(function () {
            $("[id$=DDL_Project]").on('change', function (e) {
                if (this.value != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/Ajax.aspx/GetCategoryAsset",
                        data: JSON.stringify({
                            "ProjectKey": this.value,
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () { },
                        success: function (msg) {
                            var District = $("[id$=DDL_Category]");
                            District.find('option').remove().end().append('<option selected="selected" value="0" disabled="disabled">--Loại sản phẩm--</option>');
                            $.each(msg.d, function () {
                                var object = this;
                                if (object !== '') {
                                    District.append($("<option></option>").val(object.Value).html(object.Text));
                                }
                            });
                        },
                        complete: function () {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.status);
                            console.log(xhr.responseText);
                            console.log(thrownError);
                        }
                    });
                }
            });

            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('#back-to-top').tooltip('show');
            $('#btnSearch').click(function () {
                Search();
            });
            Search();
        });

        function Search() {
            var assetID = $("[id$=txt_Search]").val();
            if (assetID == null)
                assetID = "";
            var project = "";
            var category = "";
            if ($("[id$=DDL_Project]").val() != 0)
                project = $("[id$=DDL_Project] option:selected").text();
            if ($("[id$=DDL_Category]").val() != null &&
                $("[id$=DDL_Category]").val() != 0)
                category = $("[id$=DDL_Category] option:selected").text();
            var bed = $("[id$=DDL_Number]").val();
            if (bed == null)
                bed = "";

            console.log($("[id$=DDL_Category]").val());
            $.ajax({
                type: "POST",
                url: "Customer.aspx/Search",
                data: JSON.stringify({
                    CustomerName: assetID,
                    Room: bed,
                    Project: project,
                    Category: category,
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $('.se-pre-con').fadeIn('slow');
                },
                success: function (msg) {
                    $('#divdata').empty();
                    $('#divdata').append($(msg.d.Result));
                },
                complete: function () {
                    $('.se-pre-con').fadeOut('slow');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    </script>
</asp:Content>
