﻿using Lib.CRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class CustomerDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["ID"] != null)
                    HID_CustomerKey.Value = Request["ID"];

                LoadInfo();
            }
        }

        // WebAppMobi.CustomerDetail
        void LoadInfo()
        {
            StringBuilder stringBuilder = new StringBuilder();
            Customer_Info customer_Info = new Customer_Info(HID_CustomerKey.Value.ToInt());
            stringBuilder.AppendLine("          <div class='table-responsive'>");
            stringBuilder.AppendLine("            <table id=tbltable class='table'>");

            #region [thong tin co ban & lien lac]
            stringBuilder.AppendLine("              <tbody><tr><th colspan='2' class='trheader coban'>Thông tin cơ bản<span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th><tr><tr>");
            stringBuilder.AppendLine("                <th style='width:30%'>Họ tên:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.CustomerName + "</td>");
            stringBuilder.AppendLine("              </tr>");

            string phone = "";
            if (customer_Info.Phone1 != string.Empty)
            {
                phone += @"<a href='tel:" + customer_Info.Phone1 + "'>" + customer_Info.Phone1 + "</a>&nbsp;&nbsp;&nbsp;<a style='float:right; margin-left: 5px;' class='btn btn-social-icon btn-dropbox' href='tel:" + customer_Info.Phone1 + "'><i class='fa fa-phone'></i></a>&nbsp;<a style='float:right' class='btn btn-social-icon btn-dropbox' href='sms:" + customer_Info.Phone1 + @"'><i class='fa fa-send'></i></a>";
            }
            if (customer_Info.Phone2 != string.Empty)
            {
                phone += "<br /><a href='tel:" + customer_Info.Phone2 + "'>" + customer_Info.Phone2 + "</a>&nbsp;&nbsp;&nbsp;<a style='float:right; margin-left: 5px;' class='btn btn-social-icon btn-dropbox' href='tel:" + customer_Info.Phone2 + "'><i class='fa fa-phone'></i></a>&nbsp;<a style='float:right' class='btn btn-social-icon btn-dropbox' href='sms:" + customer_Info.Phone2 + @"'><i class='fa fa-send'></i></a>";
            }

            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("                <th>Số điện thoại :</th>");
            stringBuilder.AppendLine("                <td>" + phone + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("                <th>Email:</th>");
            stringBuilder.AppendLine("                 <td>" + customer_Info.Email1 + "<br />" + customer_Info.Email2 + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("                <th>Nguồn khách:</th>");
            stringBuilder.AppendLine("                <td>" + customer_Info.CustomerSource + ":" + customer_Info.SourceNote + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("                <th>Mức độ:</th>");
            if (customer_Info.StatusName!=string.Empty)
                stringBuilder.AppendLine("                <td>" + customer_Info.StatusName + "</td>");
            else
                stringBuilder.AppendLine("                <td><a href='#myEdit' data-toggle=modal>Chưa cập nhật</a></td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("              <th>Nhu cầu</th><td>" + customer_Info.WantShow + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("                <th>Ghi chú</th>");
            stringBuilder.AppendLine("                <td style='word-wrap: break-word'></td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban>");
            stringBuilder.AppendLine("                <td colspan='2' style='word-wrap: break-word'>" + customer_Info.Description + "</td>");
            stringBuilder.AppendLine("              </tr>");
            stringBuilder.AppendLine("              <tr coban><th colspan='2' class='trheader lienlac'>Thông tin liên lạc<span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th><tr><tr>");
            bool flag = customer_Info.HasTrade == 1;
            if (flag)
            {
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Ngày sinh:</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.Birthday.ToString("dd/MM/yyyy") + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Số CMND/Passport</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CardID + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Ngày cấp</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CardDate.ToString("dd/MM/yyyy") + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Nơi cấp</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CardPlace + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Địa chỉ thường trú</th>");
                stringBuilder.AppendLine("                <td style='word-wrap: break-word'>" + customer_Info.Address1 + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Địa chỉ liên lạc</th>");
                stringBuilder.AppendLine("                <td style='word-wrap: break-word'>" + customer_Info.Address2 + "</td>");
                stringBuilder.AppendLine("              </tr>");
            }
            else
            {
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th></th>");
                stringBuilder.AppendLine("                <td></td>");
                stringBuilder.AppendLine("              </tr>");
            }
            bool flag2 = customer_Info.CategoryKey == 1;
            if (flag2)
            {
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Tên công ty</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.CompanyName + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Mã số thuế</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.TaxCode + "</td>");
                stringBuilder.AppendLine("              </tr>");
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th>Địa chỉ liên lạc</th>");
                stringBuilder.AppendLine("                <td>" + customer_Info.Address3 + "</td>");
                stringBuilder.AppendLine("              </tr>");
            }
            else
            {
                stringBuilder.AppendLine("              <tr lienlac>");
                stringBuilder.AppendLine("                <th></th>");
                stringBuilder.AppendLine("                <td></td>");
                stringBuilder.AppendLine("              </tr>");
            }
            #endregion

            #region [thong tin quan tam]
            List<ItemConsent> list = Consents_Data.List(HID_CustomerKey.Value.ToInt());
            int num;
            int total = list.Count;
            stringBuilder.AppendLine("              <tr><th colspan='2' class='trheader quantam'>Quan tâm: " + total + "<span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th><tr><tr>");
            if (total > 0)
            {
                for (int i = 0; i < list.Count; i = num + 1)
                {
                    stringBuilder.AppendLine("<tr quantam>");
                    stringBuilder.AppendLine("    <td style='background:#0ff3'><u>Dự án</u></td><td style='background:#0ff3'>" + list[i].Project + "<a href='#' onclick='OpenModal(" + list[i].RecordKey + "); return false;' style='float:right'>Chăm sóc <span class='fa fa-plus-square-o'></span></a></td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr quantam>");
                    stringBuilder.AppendLine("    <td>Loại</td><td>" + list[i].Category + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr quantam>");
                    stringBuilder.AppendLine("    <td>Phòng</td><td>" + list[i].Room + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr quantam>");
                    stringBuilder.AppendLine("    <td>Nội thất</td><td>" + list[i].Furniture + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr quantam>");
                    stringBuilder.AppendLine("    <td>Giá</td><td>" + list[i].Price.ToDoubleString() + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    DataTable dataTable = Care_Data.List(HID_CustomerKey.Value.ToInt(), list[i].RecordKey.ToInt());
                    bool flag3 = dataTable.Rows.Count > 0;
                    if (flag3)
                    {
                        stringBuilder.AppendLine("<tr quantam>");
                        stringBuilder.AppendLine("  <td colspan=2><u>Nội dung trao đổi</u></td>");
                        stringBuilder.AppendLine("</tr>");
                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            stringBuilder.AppendLine("<tr quantam>");
                            stringBuilder.AppendLine("  <td colspan=2 style='word-wrap: break-word'>" + dataRow["CreatedDate"].ToDateString() + " | " + dataRow["CreatedBy"].ToString() + " | " + dataRow["ContentDetail"].ToString() + "</td>");
                            stringBuilder.AppendLine("</tr>");
                        }
                    }
                    num = i;
                }
            }
            else
            {
                stringBuilder.AppendLine("              <tr quantam>");
                stringBuilder.AppendLine("                <th></th>");
                stringBuilder.AppendLine("                <td></td>");
                stringBuilder.AppendLine("              </tr>");
            }
            #endregion

            #region [thong tin giao dich]
            List<ItemTrade> zTable = Customer_Data.Get_Asset(HID_CustomerKey.Value.ToInt());
            total = zTable.Count;
            stringBuilder.AppendLine("              <tr><th colspan='2' class='trheader giaodich'>Giao dịch: " + total + "<span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th><tr><tr>");
            if (total > 0)
            {
                foreach (ItemTrade r in zTable)
                {
                    stringBuilder.AppendLine("<tr giaodich>");
                    stringBuilder.AppendLine("    <td>Ngày ký HĐ</td><td>" + r.ContractDate.ToDateString() + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr giaodich>");
                    stringBuilder.AppendLine("    <td>Loại giao dịch</td><td>" + r.Category + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr giaodich>");
                    stringBuilder.AppendLine("    <td>Mã căn</td><td>" + r.AssetID + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr giaodich>");
                    stringBuilder.AppendLine("    <td>Khách là</td><td>" + r.Owner + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr giaodich>");
                    stringBuilder.AppendLine("    <td>Dự án</td><td>" + r.ProjectName + "</td>");
                    stringBuilder.AppendLine("</tr>");
                    stringBuilder.AppendLine("<tr giaodich>");
                    stringBuilder.AppendLine("    <td>Doanh thu</td><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + r.InCome.ToDoubleString() + "</span></td>");
                    stringBuilder.AppendLine("</tr>");
                }
            }
            else
            {
                stringBuilder.AppendLine("              <tr giaodich>");
                stringBuilder.AppendLine("                <th></th>");
                stringBuilder.AppendLine("                <td></td>");
                stringBuilder.AppendLine("              </tr>");
            }
            stringBuilder.AppendLine("            </tbody></table>");
            #endregion

            stringBuilder.AppendLine("          </div>");
            this.LiteralData.Text = stringBuilder.ToString();

            Tools.DropDown_DDL(DDLStatus, "SELECT AutoKey, Product FROM SYS_Categories WHERE Type = 12", false);
            DDLStatus.SelectedValue = customer_Info.Status.ToString();
            txtDescription.Value = customer_Info.Description;
        }

        [WebMethod]
        public static ItemReturn SaveCare(int CustomerKey, int ConsentKey, string Content)
        {
            Care_Info care_Info = new Care_Info();
            care_Info.ConsentKey = ConsentKey;
            care_Info.CustomerKey = CustomerKey;
            care_Info.ContentDetail = Content.Trim();
            care_Info.CreatedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            care_Info.ModifiedBy = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]);
            care_Info.Create();
            return new ItemReturn
            {
                Message = care_Info.Message
            };
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            Customer_Info zInfo = new Customer_Info();
            zInfo.Key = int.Parse(HID_CustomerKey.Value);
            zInfo.Status = int.Parse(DDLStatus.SelectedValue);
            zInfo.Description = txtDescription.Value.Trim();
            zInfo.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zInfo.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zInfo.UpdateMobile();
            Response.Redirect(Request.RawUrl);
        }
    }
}