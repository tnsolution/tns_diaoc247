﻿using Lib.CRM;
using Lib.FNC;
using Lib.SAL;
using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class DashBroad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                ViewCurrentMonth();
                LoadMessage();
            }
        }
        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }
        void ViewActivity(DateTime FromDate, DateTime ToDate)
        {
            DataTable Table = new DataTable();

            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            string DepartmentRole = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentRole"];

            int TradeNeedApprove = 0;
            int TradeNeedCollect = 0;
            double InCome = 0;
            double Sum = 0;

            TradeNeedApprove = HomePage.Count_Trade_Need_Approved(FromDate, ToDate, DepartmentRole, Employee);
            TradeNeedCollect = HomePage.Count_Trade_Need_Collect(FromDate, ToDate, DepartmentRole, Employee);
            InCome = HomePage.Sum_Trade(FromDate, ToDate, DepartmentRole, Employee);
            Sum = HomePage.Sum_Fee(FromDate, ToDate, DepartmentRole);

            if (UnitLevel <= 1)
                LitCapital.Text = @"
            <div class='col-md-3 col-sm-6 col-xs-12'>
                    <div class='info-box'>
                        <a loading='yes' href='Capital.aspx'>
                            <span class='info-box-icon bg-aqua-active'><i class='ion-social-usd-outline'></i></span>
                            <div class='info-box-content'>
                                <span class='info-box-text'>Quỹ công ty</span>
                                <span class='info-box-number'>" + HomePage.Sum_Capital(FromDate, ToDate).ToString("n0") + @"
                                 </span>
                            </div>
                        </a>
                    </div>
                </div>";

            LitFee.Text = Sum.ToString("n0");
            LitFeePlay.Text = HomePage.Sum_FeePlay(FromDate, ToDate, DepartmentRole).ToString("n0");
            Lit_TotalIncome.Text = InCome.ToString("n0");
            Lit_TradeNotFinish.Text = TradeNeedCollect.ToString();
            Lit_TradeNotApprove.Text = TradeNeedApprove.ToString();            
        }
        void ViewCurrentMonth()
        {
            Lit_TimeText.Text = "Tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "2";
        }
        void ViewCurrentYear()
        {
            Lit_TimeText.Text = "Năm " + DateTime.Now.Year;

            DateTime FromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "1";
        }
        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                return;
            }
        }
        void ViewYear(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = ToDate.AddYears(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate.AddYears(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
        }
        //xem năm, hoặc tháng
        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewCurrentYear();
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }
        //xem nam, tháng trước, hoặc sau,
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewYear(ViewTimeNextPrev);
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }

        void LoadMessage()
        {
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            DataTable zTable = new DataTable();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = HomePage.Get_Annou(0);
                    break;
                default:
                    zTable = HomePage.Get_Annou(Department);
                    break;
            }

            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("<table class='table'>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <td style='witdh:50%'><b>" + Convert.ToDateTime(r["CreatedDate"]).ToString("dd/MM hh:mm") + "</b></td>");
                zSb.AppendLine("        <td style='witdh:50%'><b>" + r["CreatedBy"].ToString() + "</b></td>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("    <tr>");
                zSb.AppendLine("        <td colspan='2'>" + r["Contents"].ToString() + "</td>");
                zSb.AppendLine("    </tr>");
                zSb.AppendLine("</table>");
            }

            LitDataAnnounce.Text = zSb.ToString();
        }
    }
}