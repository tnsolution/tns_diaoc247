﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebAppMobi.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link rel="stylesheet" href="/themes/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
    <link rel="stylesheet" href="/themes/dist/css/AdminLTE.min.css" />
    <link rel="stylesheet" href="/themes/dist/css/skins/_all-skins.min.css" />
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <script type="text/javascript" src="/themes/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="/themes/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/themes/dist/js/app.min.js"></script>
    <script type="text/javascript" src="/themes/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
</head>
<body class="layout-top-nav skin-purple-light fixed">
    <form id="form1" runat="server" enctype="multipart/form-data">
      <%--  <header class="main-header">
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="../../index2.html" class="navbar-brand"><b>ĐỊA ỐC </b>247</a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="navbar-collapse pull-left collapse" id="navbar-collapse" aria-expanded="false" style="height: 1px;">
                        <ul class="nav navbar-nav">
                            <li><a href="#">Sản phẩm <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">Khách hàng</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li><a><i class="glyphicon glyphicon-chevron-left"></i></a></li>
                            <li><a><i class="glyphicon glyphicon-chevron-right"></i></a></li>
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- The user image in the navbar-->
                                    <img src="/themes/dist/img/avatar5.png" class="user-image" alt="User Image">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs">Alexander Pierce</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="/themes/dist/img/avatar5.png" class="user-image" alt="User Image">

                                        <p>
                                            Alexander Pierce - Web Developer
                    <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Followers</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-custom-menu -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </header>
        <div class="content-wrapper" style="padding-top: 50px;">
            <div class="content-header">
                <h1>Hoạt động   
                    <small>Tháng 7</small>
                    <span class="box-tools pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            Action
                    <span class="fa fa-caret-down"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Năm</a></li>
                            <li><a href="#">Tháng</a></li>
                            <li><a href="#">Tuần</a></li>
                        </ul>
                    </span>
                </h1>

            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow-gradient"><i class="ion ion-ios-folder-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Giao dịch chưa duyệt</span>
                                <span class="info-box-number">760</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red-gradient"><i class="ion ion-ios-checkmark-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Giao dịch chưa thanh toán</span>
                                <span class="info-box-number">760</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-light-blue-gradient"><i class="ion ion-ios-people-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Khách đã nhập</span>
                                <span class="info-box-number">760</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green-gradient"><i class="ion-social-usd-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Doanh thu</span>
                                <span class="info-box-number">760,000,000</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="ion-information"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Thông tin khách cần khai thác</span>
                                <span class="info-box-number">760</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Bảng xếp hạng doanh số</h3>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="chart">
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.9
            </div>
            <strong>Copyright &copy; 2017 <a href="hht.vn">HHT</a>.</strong>
            All rights reserved.           
        </div>
        <asp:Literal ID="LitDataChart" runat="server"></asp:Literal>
        <script>
            $(document).ready(function () {

            });
            $(window).scroll(function () {
                //var scrollPos = $(window).scrollTop();
                //if (scrollPos > 0) {
                //    $("#navtop").fadeOut();
                //}
                //else {
                //    $("#navtop").fadeIn();
                //}
            });
            function FormatMoney(num) {
                var str = num.toString().replace("$", "").replace(/ /g, ''),
                parts = false,
                output = [],
                i = 1,
                formatted = null;

                if (str.indexOf(".") > 0) {
                    parts = str.split(".");
                    str = parts[0];
                }
                str = str.split("").reverse();
                for (var j = 0, len = str.length; j < len; j++) {
                    if (str[j] != ",") {
                        output.push(str[j]);
                        if (i % 3 == 0 && j < (len - 1)) {
                            output.push(",");
                        }
                        i++;
                    }
                }
                formatted = output.reverse().join("");

                return (formatted);
            };
        </script>
        <script>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: areaChartData,
                options: {

                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    }
                }
            });
        </script>--%>
    </form>
</body>
</html>
