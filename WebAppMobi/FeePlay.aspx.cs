﻿using Lib.FNC;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppMobi
{
    public partial class FeePlay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewCurrentMonth();
            }
        }
        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }
        void ViewCurrentMonth()
        {
            Lit_TimeText.Text = "Xập xình tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "2";
        }
        void ViewCurrentYear()
        {
            Lit_TimeText.Text = "Năm " + DateTime.Now.Year;

            DateTime FromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "1";
        }
        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Xập xình tháng " + FromDate.Month;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Xập xình tháng " + FromDate.Month;
                return;
            }
        }
        void ViewYear(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = ToDate.AddYears(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate.AddYears(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
        }
        void ViewActivity(DateTime FromDate, DateTime ToDate)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            double TongThu = 0, TongChi = 0;

            DataTable zTable = new DataTable();
            StringBuilder zSb1 = new StringBuilder();
            StringBuilder zSb2 = new StringBuilder();

            zTable = Receipt_Detail_Data.List(FromDate, ToDate, 0, 2);
            foreach (DataRow r in zTable.Rows)
            {
                string Status = "(Chưa thu)";
                int IsApprove = r["IsApproved"].ToInt();
                if (IsApprove == 1)
                {
                    Status = "<img width='20' class='noprint' alt='' src='../template/custom-image/true.png'>&nbsp;";
                    TongThu += r["Amount"].ToDouble();
                }

                zSb1.AppendLine("<table class='table'>");
                zSb1.AppendLine("    <tr>");
                zSb1.AppendLine("            <td style='border-top: 3px solid #00c0ef;'><b>" + r["ReceiptDate"].ToDateString() + "</b> " + Status + ": " + r["ReceiptFromName"].ToString() + "<span style='float:right; font-weight: bold; color: #c90927; font-size: 16px'>" + r["Amount"].ToDoubleString() + "</span></td>");
                zSb1.AppendLine("    </tr>");
                zSb1.AppendLine("     <tr><td><b>Nội dung: </b>" + r["Contents"].ToString() + "</td></tr>");
                zSb1.AppendLine("</table>");


            }
            zSb2.AppendLine("<table class='table'>");
            zSb2.AppendLine("    <tr><th class='trheader'>Tổng thu</th><th class='trheader'><span style='float:right; font-weight: bold; color: #c90927; font-size: 16px'>" + TongThu.ToString("n0") + "</span></th></tr>");
            zSb2.AppendLine("</table>");

            LiteralData1.Text = zSb2.ToString() + zSb1.ToString();

            zTable = new DataTable();
            zSb1 = new StringBuilder();
            zSb2 = new StringBuilder();

            zTable = Payment_Detail_Data.List(FromDate, ToDate, Department, 2);
            foreach (DataRow r in zTable.Rows)
            {
                zSb1.AppendLine("<table class='table'>");
                zSb1.AppendLine("    <tr>");
                zSb1.AppendLine("            <td style='border-top: 3px solid #00c0ef;'><b>" + r["PaymentDate"].ToDateString() + "</b> Người gửi: " + r["PaymentByName"].ToString() + "<span style='float:right; font-weight: bold; color: #c90927; font-size: 16px'>" + r["Amount"].ToDoubleString() + "</span></td>");
                zSb1.AppendLine("    </tr>");
                zSb1.AppendLine("     <tr><td><b>Nội dung: </b>" + r["Contents"].ToString() + "</td></tr>");
                zSb1.AppendLine("</table>");

                TongChi += r["Amount"].ToDouble();
            }
            zSb2.AppendLine("<table class='table'>");
            zSb2.AppendLine("    <tr><th class='trheader'>Tổng chi</th><th class='trheader'><span style='float:right; font-weight: bold; color: #c90927; font-size: 16px'>" + TongChi.ToString("n0") + "</span></th></tr>");
            zSb2.AppendLine("</table>");

            LiteralData2.Text = zSb2.ToString() + zSb1.ToString();
        }

        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewCurrentYear();
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }

        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewYear(ViewTimeNextPrev);
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }

        [WebMethod]
        public static ItemReturn GetOutCapitalStatus(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Capital_Output_Info zInfo = new Capital_Output_Info(AutoKey);
            Receipt_Detail_Info zReceipt = new Receipt_Detail_Info(zInfo.AutoKey, true);

            if (zReceipt.IsApproved == 1)
            {
                zResult.Message = "Phiếu chi quỹ này đã tự động lập phiếu thu, và đã duyệt không thể gỡ được. Bạn phải gỡ hoặc xóa phiếu thu đã phát sinh";
                return zResult;
            }

            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();

            #region [auto create phieu thu]           
            zReceipt.ReceiptDate = zInfo.CapitalDate;
            zReceipt.ReceiptFrom = zInfo.ToEmployee;
            zReceipt.ReceiptBy = zInfo.FromEmployee;
            zReceipt.Contents = zInfo.Contents.Trim();
            zReceipt.Description = zInfo.Description.Trim();
            zReceipt.Amount = zInfo.Amount;
            zReceipt.CategoryKey = 1;
            zReceipt.DepartmentKey = zInfo.DepartmentKey;
            zReceipt.EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zReceipt.CreatedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zReceipt.CreatedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zReceipt.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zReceipt.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            if (zReceipt.AutoKey != 0)
                zReceipt.Update();
            else
            {
                zReceipt.FromCapital = zInfo.AutoKey;
                zReceipt.Create();
            }
            #endregion

            if (zInfo.Message != string.Empty)
                zResult.Message = "Lỗi duyệt chi quỹ: " + zInfo.Message;
            if (zReceipt.Message != string.Empty)
                zReceipt.Message = "Lỗi tạo phiếu thu" + zReceipt.Message;
            return zResult;
        }

        [WebMethod]
        public static ItemReturn GetInCapitalStatus(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            Capital_Input_Info zInfo = new Capital_Input_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}