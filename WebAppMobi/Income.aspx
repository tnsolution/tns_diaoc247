﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Income.aspx.cs" Inherits="WebAppMobi.Income" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content-header">
            <h1>Thực thu
                    <small>
                        <asp:Literal ID="Lit_TimeText" runat="server"></asp:Literal></small>
                <span class="box-tools btn-group pull-right">
                    <a class="btn btn-default btn-sm" id="ViewPrevious"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="true">
                        Chọn xem
                    <span class="fa fa-caret-down"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" id="ViewYear">Năm</a></li>
                        <li><a href="#" id="ViewMonth">Tháng</a></li>
                    </ul>
                    <a class="btn btn-default btn-sm" id="ViewNext"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </span>
            </h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thực thu dự án</h3>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="myChart1"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thực thu Phòng</h3>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="myChart2"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thực thu nhân viên</h3>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="myChart3"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HiddenFromDate" runat="server" Value="" />
    <asp:HiddenField ID="HiddenToDate" runat="server" Value="" />
    <asp:HiddenField ID="Hidden_ViewTime" runat="server" Value="0" />
    <asp:HiddenField ID="Hidden_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnActionViewTime" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewTime_Click" />
    <asp:Button ID="btnActionViewNextPrev" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewNextPrev_Click" />
    <asp:Literal ID="LitDataChart" runat="server"></asp:Literal>
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(-1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewYear").click(function () {
                $("[id$=Hidden_ViewTime]").val(1);
                $("[id$=btnActionViewTime]").trigger("click");
            });
            $("#ViewMonth").click(function () {
                $("[id$=Hidden_ViewTime]").val(2);
                $("[id$=btnActionViewTime]").trigger("click");
            });
        });  
      
        function FormatMoney(num) {
            var str = num.toString().replace("$", "").replace(/ /g, ''),
            parts = false,
            output = [],
            i = 1,
            formatted = null;

            if (str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] != ",") {
                    output.push(str[j]);
                    if (i % 3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");

            return (formatted);
        };
    </script>
    <script>
        var ctx1 = document.getElementById("myChart1");
        var ctx2 = document.getElementById("myChart2");
        var ctx3 = document.getElementById("myChart3");

        var myChart1 = new Chart(ctx1, {
            type: 'horizontalBar',
            data: areaChartData1,
            options: {
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            //console.log(tooltipItem.yLabel)
                            return FormatMoney(tooltipItem.xLabel);
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            callback: function (value) {
                                return FormatMoney(value / 1000000) + "triệu";
                            },
                        },
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });
        var myChart2 = new Chart(ctx2, {
            type: 'horizontalBar',
            data: areaChartData2,
            options: {
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            //console.log(tooltipItem.yLabel)
                            return FormatMoney(tooltipItem.xLabel);
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            callback: function (value) {
                                return FormatMoney(value/1000000)+"triệu";
                            },
                        },
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });       
        var myChart3 = new Chart(ctx3, {
            type: 'horizontalBar',
            data: areaChartData3,
            options: {
                responsive: true,
                maintainAspectRatio: false,                
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            //console.log(tooltipItem.yLabel)
                            return FormatMoney(tooltipItem.xLabel);
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            callback: function (value) {
                                return FormatMoney(value / 1000000) + "triệu";
                            },
                        },
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });
    </script>
</asp:Content>
