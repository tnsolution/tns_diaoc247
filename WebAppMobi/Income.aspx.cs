﻿using Lib.SYS;
using Lib.TASK;
using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class Income : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                ViewCurrentMonth();
            }
        }

        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }

        void ViewCurrentMonth()
        {
            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            string zScript = "";
            Lit_TimeText.Text = "Tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            if (UnitLevel >= 3)
            {
                zScript += ViewChart1(DepartmentKey, EmployeeKey, FromDate, ToDate);
                zScript += ViewChart2(DepartmentKey, EmployeeKey, FromDate, ToDate);
                zScript += ViewChart3(DepartmentKey, EmployeeKey, FromDate, ToDate);
            }
            if (UnitLevel == 2)
            {
                zScript += ViewChart1(DepartmentKey, 0, FromDate, ToDate);
                zScript += ViewChart2(DepartmentKey, 0, FromDate, ToDate);
                zScript += ViewChart3(DepartmentKey, 0, FromDate, ToDate);
            }
            if (UnitLevel <= 1)
            {
                zScript += ViewChart1(0, 0, FromDate, ToDate);
                zScript += ViewChart2(0, 0, FromDate, ToDate);
                zScript += ViewChart3(0, 0, FromDate, ToDate);
            }

            LitDataChart.Text = zScript;

            Hidden_ViewTime.Value = "2";
        }
        void ViewCurrentYear()
        {
            string zScript = "";

            int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            Lit_TimeText.Text = "Năm " + DateTime.Now.Year;

            DateTime FromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            if (UnitLevel >= 3)
            {
                zScript += ViewChart1(DepartmentKey, EmployeeKey, FromDate, ToDate);
                zScript += ViewChart2(DepartmentKey, EmployeeKey, FromDate, ToDate);
                zScript += ViewChart3(DepartmentKey, EmployeeKey, FromDate, ToDate);
            }
            if (UnitLevel == 2)
            {
                zScript += ViewChart1(DepartmentKey, 0, FromDate, ToDate);
                zScript += ViewChart2(DepartmentKey, 0, FromDate, ToDate);
                zScript += ViewChart3(DepartmentKey, 0, FromDate, ToDate);
            }
            if (UnitLevel <= 1)
            {
                zScript += ViewChart1(0, 0, FromDate, ToDate);
                zScript += ViewChart2(0, 0, FromDate, ToDate);
                zScript += ViewChart3(0, 0, FromDate, ToDate);
            }
            LitDataChart.Text = zScript;
            Hidden_ViewTime.Value = "1";
        }

        void ViewMonth(int TimeNextPrev)
        {
            string zScript = "";

            if (TimeNextPrev == 1)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                if (UnitLevel >= 3)
                {
                    zScript += ViewChart1(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, EmployeeKey, FromDate, ToDate);
                }
                if (UnitLevel == 2)
                {
                    zScript += ViewChart1(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, 0, FromDate, ToDate);
                }
                if (UnitLevel <= 1)
                {
                    zScript += ViewChart1(0, 0, FromDate, ToDate);
                    zScript += ViewChart2(0, 0, FromDate, ToDate);
                    zScript += ViewChart3(0, 0, FromDate, ToDate);
                }

                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                LitDataChart.Text = zScript;
                return;
            }
            if (TimeNextPrev == -1)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);


                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                if (UnitLevel >= 3)
                {
                    zScript += ViewChart1(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, EmployeeKey, FromDate, ToDate);
                }
                if (UnitLevel == 2)
                {
                    zScript += ViewChart1(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, 0, FromDate, ToDate);
                }
                if (UnitLevel <= 1)
                {
                    zScript += ViewChart1(0, 0, FromDate, ToDate);
                    zScript += ViewChart2(0, 0, FromDate, ToDate);
                    zScript += ViewChart3(0, 0, FromDate, ToDate);
                }

                Lit_TimeText.Text = "Tháng " + FromDate.Month;
                LitDataChart.Text = zScript;
                return;
            }
        }
        void ViewYear(int TimeNextPrev)
        {
            string zScript = "";
            if (TimeNextPrev == 1)
            {
                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = ToDate.AddYears(1).AddDays(-1);
               

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                if (UnitLevel >= 3)
                {
                    zScript += ViewChart1(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, EmployeeKey, FromDate, ToDate);
                }
                if (UnitLevel == 2)
                {
                    zScript += ViewChart1(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, 0, FromDate, ToDate);
                }
                if (UnitLevel <= 1)
                {
                    zScript += ViewChart1(0, 0, FromDate, ToDate);
                    zScript += ViewChart2(0, 0, FromDate, ToDate);
                    zScript += ViewChart3(0, 0, FromDate, ToDate);
                }

                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate.AddYears(1).AddDays(-1);

                int EmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                if (UnitLevel >= 3)
                {
                    zScript += ViewChart1(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, EmployeeKey, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, EmployeeKey, FromDate, ToDate);
                }
                if (UnitLevel == 2)
                {
                    zScript += ViewChart1(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart2(DepartmentKey, 0, FromDate, ToDate);
                    zScript += ViewChart3(DepartmentKey, 0, FromDate, ToDate);
                }
                if (UnitLevel <= 1)
                {
                    zScript += ViewChart1(0, 0, FromDate, ToDate);
                    zScript += ViewChart2(0, 0, FromDate, ToDate);
                    zScript += ViewChart3(0, 0, FromDate, ToDate);
                }

                LitDataChart.Text = zScript;
                Lit_TimeText.Text = "Năm " + FromDate.Year;
                return;
            }
        }

        string ViewChart1(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable Table = Report_Data.ChartReport_Project(DepartmentKey, EmployeeKey, FromDate, ToDate);
            StringBuilder zSb = new StringBuilder();
            string zSblbl = "";
            string zSbdataInCome = "";
            if (Table.Rows.Count > 0)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    zSblbl += "'" + r["ProjectName"].ToString() + "',";
                    zSbdataInCome += "'" + r["INCOME"].ToString() + "',";
                }

                zSb.AppendLine(" var areaChartData1 = {");
                zSb.AppendLine("    labels: [" + zSblbl + "],");
                zSb.AppendLine("    datasets: [{");
                zSb.AppendLine("         label: '# VNĐ',");
                zSb.AppendLine("         backgroundColor: 'rgba(60, 141, 188, 0.5)',");
                zSb.AppendLine("         borderColor: 'rgba(60, 141, 188, 0.9)',");
                zSb.AppendLine("         borderWidth: '1',");
                zSb.AppendLine("         data: [" + zSbdataInCome.Remove(zSbdataInCome.LastIndexOf(",")) + "]");
                zSb.AppendLine("    }]");
                zSb.AppendLine(" };");
            }
            return "<script>" + zSb.ToString() + "</script>";
        }
        string ViewChart2(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable Table = Report_Data.ChartReport_Department(DepartmentKey, EmployeeKey, FromDate, ToDate);
            StringBuilder zSb = new StringBuilder();
            string zSblbl = "";
            string zSbdataInCome = "";
            if (Table.Rows.Count > 0)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    zSblbl += "'" + r["DepartmentName"].ToString() + "',";
                    zSbdataInCome += "'" + r["INCOME"].ToString() + "',";
                }

                zSb.AppendLine(" var areaChartData2 = {");
                zSb.AppendLine("    labels: [" + zSblbl + "],");
                zSb.AppendLine("    datasets: [{");
                zSb.AppendLine("         label: '# VNĐ',");
                zSb.AppendLine("         backgroundColor: 'rgba(60, 141, 188, 0.5)',");
                zSb.AppendLine("         borderColor: 'rgba(60, 141, 188, 0.9)',");
                zSb.AppendLine("         borderWidth: '1',");
                zSb.AppendLine("         data: [" + zSbdataInCome.Remove(zSbdataInCome.LastIndexOf(",")) + "]");
                zSb.AppendLine("    }]");
                zSb.AppendLine(" };");
            }
            return "<script>" + zSb.ToString() + "</script>";
        }
        string ViewChart3(int DepartmentKey, int EmployeeKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable Table = Report_Data.ChartReport_Employee(DepartmentKey, EmployeeKey, FromDate, ToDate);
            StringBuilder zSb = new StringBuilder();
            string zSblbl = "";
            string zSbdataInCome = "";
            if (Table.Rows.Count > 0)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    zSblbl += "'" + r["EmployeeName"].ToString() + "',";
                    zSbdataInCome += "'" + r["INCOME"].ToString() + "',";
                }

                zSb.AppendLine(" var areaChartData3 = {");
                zSb.AppendLine("    labels: [" + zSblbl + "],");
                zSb.AppendLine("    datasets: [{");
                zSb.AppendLine("         label: '# VNĐ',");
                zSb.AppendLine("         backgroundColor: 'rgba(60, 141, 188, 0.5)',");
                zSb.AppendLine("         borderColor: 'rgba(60, 141, 188, 0.9)',");
                zSb.AppendLine("         borderWidth: '1',");
                zSb.AppendLine("         data: [" + zSbdataInCome.Remove(zSbdataInCome.LastIndexOf(",")) + "]");
                zSb.AppendLine("    }]");
                zSb.AppendLine(" };");
            }
            return "<script>" + zSb.ToString() + "</script>";
        }
        //xem năm, hoặc tháng
        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewCurrentYear();
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }

        //xem nam, tháng trước, hoặc sau,
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewYear(ViewTimeNextPrev);
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }
    }
}