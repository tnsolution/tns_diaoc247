﻿using Lib.SYS;
using System;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadUser();
        }

        void LoadUser()
        {
            StringBuilder zSbMenu = new StringBuilder();
            #region [UserName]
            zSbMenu.AppendLine("<li class='dropdown user user-menu'>");
            zSbMenu.AppendLine("    <a href = '#' class='dropdown-toggle' data-toggle='dropdown'>");
            zSbMenu.AppendLine("          <img src = '/themes/dist/img/avatar5.png' class='user-image' alt='User Image' />");
            zSbMenu.AppendLine("          <span class='hidden-xs'>" + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "</span>");
            zSbMenu.AppendLine("     </a>");

            zSbMenu.AppendLine("    <ul class='dropdown-menu'>");
            zSbMenu.AppendLine("        <li class='user-header'>");
            zSbMenu.AppendLine("            <img src='/themes/dist/img/avatar5.png' class='user-image' class='img-circle' alt='User Image'>");
            zSbMenu.AppendLine("            <p>");
            zSbMenu.AppendLine(HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + " </br> " + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["Position"]));
            zSbMenu.AppendLine("                <small>Thành viên DIAOC 247</small>");
            zSbMenu.AppendLine("            </p>");
            zSbMenu.AppendLine("        </li>");

            zSbMenu.AppendLine("       <li class='user-footer'>");
            zSbMenu.AppendLine("           <div class='pull-left'>");
            //zSbMenu.AppendLine("           <a data-toggle='modal'  href='#mProfile' class='btn btn-default btn-flat'>Profile</a>");
            zSbMenu.AppendLine("           </div>");
            zSbMenu.AppendLine("           <div class='pull-right'>");
            zSbMenu.AppendLine("           <a href='/Login.aspx' class='btn btn-default btn-flat'>Sign out</a>");
            zSbMenu.AppendLine("          </div>");
            zSbMenu.AppendLine("        </li>");
            zSbMenu.AppendLine("    </ul>");
            zSbMenu.AppendLine("</li>");
            #endregion
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            int DepartmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            string RolePage = "FNC";
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (Permitsion[1].ToInt() == 1)
            {
                int NumReceipt = 0;
                int NumPayment = 0;
                double SumReceipt = 0;
                double SumPayment = 0;

                switch (UnitLevel)
                {
                    case 0:
                    case 1:
                        //SumReceipt = Notification_Data.Sum_Receipt(DateTime.Now.Year, DateTime.Now.Month, 0, 1);
                        //SumPayment = Notification_Data.Sum_Payment(DateTime.Now.Year, DateTime.Now.Month, 0, 1);
                        NumReceipt = Notification_Data.Count_Receipt(0, 1);
                        NumPayment = Notification_Data.Count_Payment(0, 1);
                        break;

                    default:
                        //SumReceipt = Notification_Data.Sum_Receipt(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        //SumPayment = Notification_Data.Sum_Payment(DateTime.Now.Year, DateTime.Now.Month, DepartmentKey, 1);
                        NumReceipt = Notification_Data.Count_Receipt(DepartmentKey, 1);
                        NumPayment = Notification_Data.Count_Payment(DepartmentKey, 1);
                        break;
                }

                LitFNC.Text = @"
<li loading='yes' style='border-bottom: 1px solid #FFF;'><a href='Receipt.aspx' style='font-size: 17px'>Thu <span style='float:right'>Chờ duyệt: " + NumReceipt + @"</span></a></li>
<li loading='yes' style='border-bottom: 1px solid #FFF;'><a href='Payment.aspx' style='font-size: 17px'>Chi <span style='float:right'>Chờ duyệt: " + NumPayment + " </span></a></li>";
            }
            LitMenu.Text = zSbMenu.ToString();
        }
    }
}