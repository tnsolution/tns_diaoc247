﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="WebAppMobi.Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .trheader {
            background-color: #DDDDDD;
        }

        .gachngang {
            text-decoration: line-through !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success flat">
                        <div class="box-header with-border">
                            <h4>
                                <asp:Literal ID="Lit_TimeText" runat="server"></asp:Literal>
                                <span class="box-tools btn-group pull-right">
                                    <a loading="yes" class="btn btn-default btn-sm" id="ViewPrevious"><i class="glyphicon glyphicon-chevron-left"></i></a>                                 
                                    <a loading="yes" class="btn btn-default btn-sm" id="ViewNext"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                </span>
                            </h4>
                        </div>
                        <div class="box-body">
                            <asp:Literal ID="LiteralData1" runat="server"></asp:Literal>
                            <asp:Literal ID="LiteralData2" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mApprove" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" id="pophead">Bạn có đồng ý duyệt thông tin</h4>
                </div>
                <div class="modal-body">
                    <textarea id="txt_Note" rows="5" cols="20" class="form-control" placeholder="Nội dung, lý do, duyệt/ không duyệt"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnNotOK" data-dismiss="modal">
                        <i class="ace-icon fa fa-crosshairs"></i>
                        Không duyệt
                    </button>
                    <button type="button" class="btn btn-info" id="btnOK" data-dismiss="modal">
                        <i class="ace-icon fa fa-check"></i>
                        Duyệt
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_Category" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AutoKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_Level" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenFromDate" runat="server" Value="" />
    <asp:HiddenField ID="HiddenToDate" runat="server" Value="" />
    <asp:HiddenField ID="Hidden_ViewTime" runat="server" Value="0" />
    <asp:HiddenField ID="Hidden_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnActionViewTime" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewTime_Click" />
    <asp:Button ID="btnActionViewNextPrev" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewNextPrev_Click" />
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(-1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewYear").click(function () {
                $("[id$=Hidden_ViewTime]").val(1);
                $("[id$=btnActionViewTime]").trigger("click");
            });
            $("#ViewMonth").click(function () {
                $("[id$=Hidden_ViewTime]").val(2);
                $("[id$=btnActionViewTime]").trigger("click");
            });
            $("#btnOK").click(function () {
                ApproveCheck(1)
            });
            $("#btnNotOK").click(function () {
                ApproveCheck(-1)
            });
            $("#tbltable tr.action").click(function () {
                $("tr[id]").hide();
                var obj = $(this);
                var id = obj.attr("key");
                obj.toggleClass("open");
                if (obj.hasClass("open")) {
                    $("tr[id=" + id + "]").show();
                }
            });
            $("tr[head]").click(function () {
                var head = $(this).attr("head");
                var obj = $(this);
                obj.toggleClass("open");
                $("tr[xem]").hide();
                if (obj.hasClass("open")) {
                    $("tr[xem=" + head + "]").show();
                }
            });
            $("tr[xem]").hide();
            $("tr[id]").hide();
        });
        function PopupCheck(id, lvl) {
            $("[id$=HID_AutoKey]").val(id);
            $("[id$=HID_Level]").val(lvl);
            if (lvl == 2) {
                $("#pophead").text("Bạn có muốn xác nhận thông tin !.");
            }
            $("#mApprove").modal("show");
        }
        function ApproveCheck(approve) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được")) {
                $.ajax({
                    type: 'POST',
                    url: '/Payment.aspx/PaymentStatus',
                    data: JSON.stringify({
                        'AutoKey': $("[id$=HID_AutoKey]").val(),
                        'Approve': approve,
                        'Note': $("#txt_Note").val(),
                        'Level': $("[id$=HID_Level]").val(),
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showNotiMessageError("Thông báo !", msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function UnApproveCheck(key, lvl) {
            if (confirm("Bạn có chắc gỡ duyệt thông tin !.")) {
                $.ajax({
                    type: 'POST',
                    url: '/Payment.aspx/PaymentStatus',
                    data: JSON.stringify({
                        'AutoKey': key,
                        'Approve': 0,
                        'Note': $("#txt_Note").val(),
                        'Level': lvl,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            Page.showNotiMessageError("Thông báo !", msg.d.Message);
                            $('.se-pre-con').fadeOut('slow');
                        }
                        else {
                            location.reload();
                            //$("#" + key).css("background-color", "#c5d0dc");
                            //$(this).attr('id');
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
