﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppMobi
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewCurrentMonth();
            }
        }
        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }
        void ViewCurrentMonth()
        {
            //Lit_TimeText.Text = "Phí chi tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "2";
        }
        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                //Lit_TimeText.Text = "Phí chi tháng " + FromDate.Month;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                //Lit_TimeText.Text = "Phí chi tháng " + FromDate.Month;
                return;
            }
        }
        void ViewActivity(DateTime FromDate, DateTime ToDate)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            DataTable zTable = new DataTable();
            StringBuilder zSb1 = new StringBuilder();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = Departments_Data.List(0);
                    break;

                default:
                    zTable = Departments_Data.List(Department);
                    break;
            }
            double tong = 0;

            foreach (DataRow r in zTable.Rows)
            {
                DataTable zFee = Payment_Detail_Data.List(FromDate.Month, FromDate.Year, r["DepartmentKey"].ToInt(), 1);
                int sochuaduyet = zFee.Select("IsApproved2 = 0").Length;
                double tongtien = zFee.AsEnumerable().Where(
                    o => o.Field<int>("IsApproved") == 1 && o.Field<int>("IsApproved2") == 1).Sum(
                    o => o.Field<decimal>("Amount")).ToDouble();
                //zFee.Compute("SUM(Amount)", "IsApproved = 1 AND IsApproved2 = 1").ToDouble();
                tong += tongtien;

                zSb1.AppendLine("<table class='table' id=tbltable>");
                zSb1.AppendLine("    <tr class='action' style='background: #DDDD' key=" + r["DepartmentKey"].ToInt() + ">");
                zSb1.AppendLine("            <td><b>" + r["DepartmentName"].ToString()
                    + "</b><span class='pull-right'> | Chờ duyệt: "
                    + sochuaduyet + "</span><span class='pull-right' style='font-weight: bold; color: #c90927;'>"
                    + tongtien.ToString("n0") + "</span></td>");
                zSb1.AppendLine("    </tr>");
                zSb1.AppendLine("     <tr id=" + r["DepartmentKey"].ToInt() + ">");
                zSb1.AppendLine("       <td>");

                foreach (DataRow rf in zFee.Rows)
                {
                    double MoneyVat = (rf["Money"].ToDouble() * rf["VAT"].ToInt()) / 100;
                    string ClassGachNgang = "";
                    string BackColor = "";

                    #region [Kiểm tra tình trạng]
                    int Duyet1 = rf["IsApproved"].ToInt();
                    int Duyet2 = rf["IsApproved2"].ToInt();

                    string Jquery1 = "style='float:right' float:right' onclick='UnApproveCheck(" + rf["AutoKey"].ToString() + ",1)'";
                    string Jquery2 = "style='float:right' float:right' onclick='UnApproveCheck(" + rf["AutoKey"].ToString() + ",2)'";

                    string Status1 = "<img width=20 " + Jquery1 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved"].ToInt() + "' alt='' src='../themes/custom-image/false.png' />";
                    string Status2 = "<img width=20 " + Jquery2 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved2"].ToInt() + "' alt='' src='../themes/custom-image/false.png' />";
                    string Status3 = "";

                    if (Duyet1 == 0)
                        Jquery1 = "style='cursor:pointer; float:right' onclick='PopupCheck(" + rf["AutoKey"].ToString() + ",1)'";
                    else
                    {
                        if (Duyet1 == 1 && Duyet2 == 0)
                        {
                            Jquery2 = "style='cursor:pointer; float:right' onclick='PopupCheck(" + rf["AutoKey"].ToString() + ",2)'";
                        }
                    }

                    switch (Duyet1)
                    {
                        case -1:
                            Status1 = "<img width=20 " + Jquery1 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved"].ToInt() + "' alt='' src='../themes/custom-image/cross.png' />" + rf["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status1 = "<img width=20 " + Jquery1 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved"].ToInt() + "' alt='' src='../themes/custom-image/false.png' />";
                            break;
                        case 1:
                            Status1 = "<img width=20 " + Jquery1 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved"].ToInt() + "' alt='' src='../themes/custom-image/true.png' />" + rf["ApprovedName"].ToString();
                            break;
                    }

                    switch (Duyet2)
                    {
                        case -1:
                            Status2 = "<img width=20 " + Jquery2 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved2"].ToInt() + "' alt='' src='../themes/custom-image/cross.png' />" + rf["ApprovedName"].ToString();
                            break;
                        case 0:
                            Status2 = "<img width=20 " + Jquery2 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved2"].ToInt() + "' alt='' src='../themes/custom-image/false.png' />";
                            break;
                        case 1:
                            Status2 = "<img width=20 " + Jquery2 + " id='" + rf["AutoKey"].ToString() + "' check='" + rf["IsApproved2"].ToInt() + "' alt='' src='../themes/custom-image/true.png' />" + rf["ApprovedName"].ToString();
                            break;
                    }

                    if (Duyet1 == -1 || Duyet2 == -1)
                        ClassGachNgang = "gachngang";
                    if (Duyet1 == 0 || Duyet2 == 0)
                        BackColor = "style=background-color:#c5d0dc";
                    if (Duyet1 == 1 && Duyet2 == 1)
                        Status3 = "<img width=20 alt='' src='../themes/custom-image/true.png' />";
                    else
                        Status3 = "<img width=20 alt='' src='../themes/custom-image/false.png' />";
                    #endregion

                    zSb1.AppendLine("<table class='table' id='tblTable' " + BackColor + ">");
                    zSb1.AppendLine("     <tr><th style='border-top: 3px solid #00c0ef;'>Ngày chi: " + rf["PaymentDate"].ToDateString() + "</th></tr>");
                    zSb1.AppendLine("     <tr><td style='font-weight: bold; color: #c90927; font-size: 16px' " + ClassGachNgang + " ><div style='width:50%;float:left'>Số tiền: " + rf["Money"].ToDoubleString() + "</div><div style='width:50%;float:right'>VAT: " + MoneyVat.ToDoubleString() + "</div></td></tr>");
                    zSb1.AppendLine("     <tr><td style='font-weight: bold; color: #c90927; font-size: 16px' " + ClassGachNgang + " >Thành tiền: " + rf["Amount"].ToDoubleString() + "<span class='pull-right'>" + Status3 + "</span></td></tr>");
                    zSb1.AppendLine("     <tr><td><b>Nội dung: </b>" + rf["Contents"].ToString() + "</td></tr>");
                    zSb1.AppendLine("     <tr head='" + rf["AutoKey"].ToString() + "'><td><span><i class='fa fa-chevron-right'></i></span><b> Ghi chú: </b>" + rf["Description"].ToString() + "</td></tr>");
                    zSb1.AppendLine("     <tr xem='" + rf["AutoKey"].ToString() + "'><td><b>Người chi: </b>" + rf["PaymentToName"].ToString() + "</td></tr>");
                    zSb1.AppendLine("     <tr xem='" + rf["AutoKey"].ToString() + "'><td><b>Xác nhận: </b>" + Status1 + "</td></tr>");
                    zSb1.AppendLine("     <tr xem='" + rf["AutoKey"].ToString() + "'><td><b>Duyệt: </b>" + Status2 + "</td></tr>");
                    zSb1.AppendLine("</table>");
                }

                zSb1.AppendLine("       </td>");
                zSb1.AppendLine("     </tr>");
                zSb1.AppendLine("</table>");
            }

            LiteralData1.Text = zSb1.ToString();
            Lit_TimeText.Text = "Phí chi tháng " + FromDate.Month + " | <span style='font-weight: bold; color: #c90927;'>" + tong.ToString("n0") + "</span>";
        }
        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }
        [WebMethod]
        public static ItemReturn PaymentStatus(int AutoKey, int Approve, string Note, int Level)
        {
            /*
             duyệt và gỡ duyệt theo lần 1 xác nhan, 2 duyet
             gỡ duyệt thi không luu ghi chú
             */

            ItemReturn zResult = new ItemReturn();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (Permitsion[2].ToInt() == 0 && UnitLevel > 2)
            {
                zResult.Message = "Bạn chưa có quyền duyệt thông tin này xin vui lòng liên hệ quản lý !.";
                return zResult;
            }

            Payment_Detail_Info zInfo = new Payment_Detail_Info(AutoKey);
            if (Level == 1)
            {
                zInfo.IsApproved = Approve;
                if (Approve != 0)
                    zInfo.ApproveNote = Note.Trim() + "\r\n";
                zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SetStatus();
            }
            else
            {
                zInfo.IsApproved2 = Approve;
                if (Approve != 0)
                    zInfo.ApproveNote2 = Note.Trim() + "\r\n";
                zInfo.Approved2By = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                zInfo.SetStatus2();
            }
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}