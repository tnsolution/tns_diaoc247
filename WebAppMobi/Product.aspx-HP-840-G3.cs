﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class Product : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                int Unit = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
                int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
                int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
                Tools.DropDown_DDL(DDL_Project, @"SELECT ProjectKey, ProjectName FROM PUL_Project A", false);
            }
        }

        static bool CheckViewPhone(int Agent, int Asset, string Type)
        {
            int Unit = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            #region [Check View Phone]
            if (Unit == 0 || Unit == 1 || Employee == Agent)
            {
                return true;
            }
            else
            {
                DataTable zPermition = Share_Permition_Data.List_ShareEmployee(Asset, Type);
                {
                    for (int i = 0; i < zPermition.Rows.Count; i++)
                    {
                        if (Employee == zPermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }

        // WebAppMobi.Product
        [WebMethod]
        public static ItemReturn Search(int Project, int Category, string Room, string Asset, string Purpose, int Index, int Show)
        {
            ItemReturn itemResult = new ItemReturn();
            int num = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            List<Product_ItemMobile> list = Product_Data.SearchMobile(Project, Category, string.Empty, string.Empty, string.Empty, Room, Asset, string.Empty, Purpose, 0, 0, Index, Show);
            StringBuilder stringBuilder = new StringBuilder();
            bool flag = list.Count > 0;
            if (flag)
            {
                foreach (Product_ItemMobile current in list)
                {
                    string Phone = "";
                    string text = "<b>"+ current.CategoryName + ":</b> " + current.AssetID.Trim() +" | <b>PN:</b> "+ current.Room;
                    //text += "<br /><b>Loại căn: </b>" + current.CategoryName;
                    //text += "<br /><b>Phòng ngủ: </b>" + current.Room;
                    text += "<br /><b>Diện tích: </b>" + current.Area;
                    text += "<br /><b>Tình trạng: </b>" + current.Status;
                    text += "<br /><b>Bán (VNĐ): </b><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + current.Price_VND.ToString("n0") + "</span>";
                    text += "<br /><b>Thuê (VNĐ): </b><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + current.PriceRent_VND.ToString("n0") + "</span>";
                    bool flag2 = Product.CheckViewPhone(current.EmployeeKey, current.AssetKey, current.AssetType);
                    if (flag2)
                        Phone = "<a href='tel:" + current.CustomerPhone + "' class='btn btn-primary btn-block btn-flat' style='margin-top:2px'><i class='ace-icon fa fa-phone'></i>Chủ nhà:" + current.CustomerName + "</a>";
                    else
                        Phone = "<a href='tel:" + current.EmployeePhone + "' class='btn btn-primary btn-block btn-flat' style='margin-top:2px'><i class='ace-icon fa fa-phone'></i>Chuyên viên:" + current.EmployeeName + "</a>";

                    stringBuilder.AppendLine("<a href='ProductDetail.aspx?ID=" + current.AssetKey + "&Type=" + current.AssetType + "'>");
                    stringBuilder.AppendLine("    <table class='table table-hover table-bordered'>");
                    stringBuilder.AppendLine("        <tr>");
                    stringBuilder.AppendLine("            <td style='width: 20%'><img src='/_Resource/Img/logo.jpg' style='width: 100px; height: 100px' /></td>");
                    stringBuilder.AppendLine("            <td>" + text + "</td>");
                    stringBuilder.AppendLine("        </tr>");
                    stringBuilder.AppendLine("        <tr>");
                    stringBuilder.AppendLine("            <td colspan=2>" + Phone + "</td>");
                    stringBuilder.AppendLine("        </tr>");
                    stringBuilder.AppendLine("    </table>");
                    stringBuilder.AppendLine("</a>");
                }
                itemResult.Result = stringBuilder.ToString();
            }
            else
            {
                itemResult.Result = "0";
            }
            return itemResult;
        }
    }
}