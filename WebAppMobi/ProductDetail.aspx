﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="WebAppMobi.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/themes/plugins/Gallery-master/css/blueimp-gallery.min.css" />
    <link rel="stylesheet" href="/themes/CheckCSS.css" />
    <style>
        #details {
            height: 250px;
            overflow-y: scroll;
        }

        .td40 {
            width: 40%;
        }

        .sup {
            vertical-align: top;
            font-size: smaller;
        }

        .trheader {
            background-color: #DDDDDD;
        }
    </style>
    <script>
        function fileSelected() {
            var count = document.getElementById('fileToUpload').files.length;
            document.getElementById('details').innerHTML = "";
            for (var index = 0; index < count; index++) {
                var file = document.getElementById('fileToUpload').files[index];
                var fileSize = 0;
                if (file.size > 1024 * 1024)
                    fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
                else
                    fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
                document.getElementById('details').innerHTML += 'Name: ' + file.name + '<br>Size: ' + fileSize + '<br>Type: ' + file.type;
                document.getElementById('details').innerHTML += '<p>';
            }
        }
        function uploadFile() {
            var fd = new FormData();
            var count = document.getElementById('fileToUpload').files.length;
            for (var index = 0; index < count; index++) {
                var file = document.getElementById('fileToUpload').files[index];
                fd.append(file.name, file);
            }

            var key = $("[id$=HID_AssetKey]").val();
            var type = $("[id$=HID_AssetType]").val();
            var project = $("[id$=HID_ProjectKey]").val();

            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener("progress", uploadProgress, false);
            xhr.addEventListener("load", uploadComplete, false);
            xhr.addEventListener("error", uploadFailed, false);
            xhr.addEventListener("abort", uploadCanceled, false);
            xhr.open("POST", "Upload/SaveFile.aspx?key=" + key + "&type=" + type + "&project" + project);
            xhr.send(fd);
        }
        function uploadProgress(evt) {
            if (evt.lengthComputable) {
                var percentComplete = Math.round(evt.loaded * 100 / evt.total);
                document.getElementById('progress').innerHTML = percentComplete.toString() + '%';
            } else {
                document.getElementById('progress').innerHTML = 'unable to compute';
            }
        }
        function uploadComplete(evt) {
            /* This event is raised when the server send back a response */
            console.log(evt.target.responseText);
            $('.se-pre-con').fadeIn('slow');
            //location.reload();
            $(location).attr('href',window.location.href);
        }
        function uploadFailed(evt) {
            alert("There was an error attempting to upload the file.");
        }
        function uploadCanceled(evt) {
            alert("The upload has been canceled by the user or the browser dropped the connection.");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Carousel</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group-sm pull-left">
                        <a href="javascript: history.go(-1);" class="btn btn-default"><i class="ion-arrow-left-b"></i>&nbsp Quay về</a>
                    </div>
                    <asp:Literal ID="LitButton" runat="server"></asp:Literal>
                </div>
            </div>
            <div id="blueimp-gallery-carousel" class="blueimp-gallery blueimp-gallery-carousel">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
            <asp:Literal ID="Lit_Info" runat="server"></asp:Literal>
        </div>
        <div class="box-footer">
        </div>
        <!-- /.box-body -->
    </div>

    <div class="modal fade" id="myModal" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Chọn ảnh</h4>
                </div>
                <div class="modal-body">
                    <input type="file" name="fileToUpload" id="fileToUpload" multiple="multiple" onchange="fileSelected();" accept="image/*" />
                    <div id="details"></div>
                    <div id="progress"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnAddImage">
                        <i class="ace-icon fa fa-plus"></i>
                        Thêm
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myEdit" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #4da44d;">
                    <button type="button" class="close" data-dismiss="modal">
                        ×</button>
                    <h4 class="modal-title" style="color: White">Tình trang/ Ghi chú</h4>
                </div>
                <div class="modal-body">
                    <asp:DropDownList ID="DDLStatus" runat="server" class="form-control"></asp:DropDownList>
                    <textarea id="txtDescription" rows="5" cols="20" class="form-control" runat="server"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="btnSave" runat="server" onserverclick="btnSave_ServerClick">
                        <i class="ace-icon fa fa-plus"></i>
                        Cập nhật
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
    <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <script type="text/javascript" src="/themes/plugins/Gallery-master/js/blueimp-gallery.min.js"></script>
    <script>
        $(function () {
            $("tr[head]").click(function () {
                var head = $(this).attr("head");
                var obj = $(this);
                obj.toggleClass("open");
                $("tr[xem]").hide();
                if (obj.hasClass("open")) {
                    $("tr[xem=" + head + "]").show();
                }
            });
            $('.slide').click(function () {
                $('#links').trigger('click');
            });
            $("#btnAddImage").click(function () {
                uploadFile();
            });
            $("tr[view=usd]").hide();
            $("[id$=Usdswitch3]").change(function () {
                if ($(this).is(':checked')) {
                    $("tr[view=usd]").show();
                    $("tr[view=vnd]").hide();
                }
                else {
                    $("tr[view=usd]").hide();
                    $("tr[view=vnd]").show();
                }
            });
        });

        blueimp.Gallery(document.getElementById('links').getElementsByTagName('a'), {
            container: '#blueimp-gallery-carousel',
            carousel: true
        });
        document.getElementById('links').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = { index: link, event: event },
                links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    </script>
</asp:Content>
