﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;

namespace WebAppMobi
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                HID_AssetKey.Value = Request["ID"];
                HID_AssetType.Value = Request["Type"];

                LoadAsset();
            }
        }

        void LoadAsset()
        {
            int AssetKey = HID_AssetKey.Value.ToInt();
            string Type = HID_AssetType.Value;
            Product_Info zAsset = new Product_Info(AssetKey, Type);
            List<ItemDocument> zTable = Document_Data.List(135, AssetKey, Type);
            ItemAsset Item = zAsset.ItemAsset;
            LoadInfo(Item.AssetID, Item.CustomerName,
                Item.Price_VND, Item.PriceRent_VND,
                Item.Price_USD, Item.PriceRent_USD,
                Item.Status, Item.Purpose, Item.ProjectName,
                Item.Address, Item.AreaWater, Item.AreaWall, Item.View, Item.Door,
                Item.Furniture, Item.Description, Item.Phone1, Item.Room, Item.EmployeePhone,
                Item.EmployeeKey, Item.AssetKey, Item.AssetType, Item.EmployeeName,
                zTable);

            HID_ProjectKey.Value = Item.ProjectKey;

            Tools.DropDown_DDL(DDLStatus, "SELECT AutoKey, Product FROM dbo.SYS_Categories WHERE Type = 4", false);
            DDLStatus.SelectedValue = Item.StatusKey;
            txtDescription.Value = Item.Description;
        }

        void LoadInfo(string MaSP, string Tenchunha,
            string GiaBanVND, string GiaThueVND, string GiaBanUSD, string GiaThueUSD,
            string TinhTrang, string NhuCau, string DuAn, string DiaChi,
            string DienTichthongthuy, string DienTichtimtuong, string View,
            string Cua, string NoiThat, string GhiChu,
            string PhoneKhach, string PhongNgu, string PhoneNhanVien,
            string EmployeeKey, string AssetKey, string AssetType, string TenSales,
            List<ItemDocument> ListImg)
        {
            string view = @"<div class='onoffswitch'>
                                            <input type='checkbox' name='onoffswitch' class='onoffswitch-checkbox' id='Usdswitch3' />
                                            <label class='onoffswitch-label' for='Usdswitch3'>
                                                <span class='onoffswitch-inner'></span>
                                                <span class='onoffswitch-switch'></span>
                                            </label>
                                        </div>";

            StringBuilder zSb = new StringBuilder();
            zSb.AppendLine("<div class='form-group'>");

            zSb.AppendLine("<table class='table'>");
            zSb.AppendLine("        <tr head=thongtinlamviec><th colspan='2' class=trheader>Thông tin làm việc <span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec><th>Mã căn:</th><td>" + MaSP + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec><th class=td40>Tình trang:</th><td><a href='#myEdit' data-toggle=modal>" + TinhTrang + "</a></td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec><th>Nhu cầu:</th><td>" + NhuCau + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec><th>Xem giá:</th><td>" + view + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec view=vnd><th>Giá bán (VNĐ):</th><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + GiaBanVND + "</span></td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec view=vnd><th>Giá thuê (VNĐ):</th><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + GiaThueVND + "</span></td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec view=usd><th>Giá bán (USD):</th><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + GiaBanUSD + "</span></td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec view=usd><th>Giá thuê (USD):</th><td><span style='font-weight: bold; color: #c90927; font-size: 16px'>" + GiaThueUSD + "</span></td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlamviec><th>Ghi chú:</th><td>" + GhiChu + "</td></tr>");
            zSb.AppendLine("        <tr head=thongtincoban><th colspan='2' class=trheader>Thông tin cơ bản <span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Dự án:</td><td>" + DuAn + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Diện tích thông thủy:</th><td>" + DienTichthongthuy + " m<span class=sup>2</span></td></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Diện tích tim tường:</th><td>" + DienTichtimtuong + " m<span class=sup>2</span></td></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Hướng cửa:</th><td>" + Cua + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Hướng View:</th><td>" + View + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Nội thất:</th><td>" + NoiThat + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtincoban><th>Phòng ngủ:</th><td>" + PhongNgu + "</td></tr>");
            zSb.AppendLine("        <tr head=thongtinlienhe><th colspan='2' class=trheader>Thông tin liên hệ <span class='pull-right'><a href='#' class='text-muted'><i class='fa fa-chevron-right'></i></a></span></th></tr>");
            zSb.AppendLine("        <tr xem=thongtinlienhe><th>Chủ nhà:</th><td>" + Tenchunha + "</td></tr>");
            zSb.AppendLine("        <tr xem=thongtinlienhe><th>Sale quản lý:</th><td>" + TenSales + "</td></tr>");
            zSb.AppendLine("</table>");
            zSb.AppendLine("</div>");
            zSb.AppendLine("<div class='footer'>");

            bool flag = CheckViewPhone(EmployeeKey.ToInt(), AssetKey.ToInt(), AssetType);
            if (flag)
            {
                zSb.AppendLine("                <table class='table table-bordered' style='margin-bottom:0px !important;'>");
                zSb.AppendLine("                    <tr>");
                zSb.AppendLine("                        <td style='width: 50%'>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='tel:" + PhoneKhach + "'>");
                zSb.AppendLine("                                <i class='fa fa-phone'></i>&nbsp Gọi chủ nhà.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                        <td>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='sms:" + PhoneKhach + "'>");
                zSb.AppendLine("                                <i class='fa fa-send'></i>&nbsp Nhắn chủ nhà.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                    </tr>");
                zSb.AppendLine("                </table>");

                LitButton.Text = @" <div class='form-group-sm pull-right'><a href='#myModal' data-toggle='modal' class='btn btn-default'>&nbsp Tải ảnh</a></div>";
            }
            else
            {
                zSb.AppendLine("                <table class='table table-bordered' style='margin-bottom:0px !important;'>");
                zSb.AppendLine("                    <tr>");
                zSb.AppendLine("                        <td style='width: 50%'>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='tel:" + PhoneNhanVien + "'>");
                zSb.AppendLine("                                <i class='fa fa-phone'></i>&nbsp Gọi chuyên viên.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                        <td>");
                zSb.AppendLine("                            <a class='btn btn-primary btn-block btn-flat' href='sms:" + PhoneNhanVien + "'>");
                zSb.AppendLine("                                <i class='fa fa-send'></i>&nbsp Nhắn chuyên viên.");
                zSb.AppendLine("                            </a>");
                zSb.AppendLine("                        </td>");
                zSb.AppendLine("                    </tr>");
                zSb.AppendLine("                </table>");
            }

            zSb.AppendLine("            </div>");
            zSb.AppendLine("<div id='links' style='display: none'>");

            if (ListImg.Count > 0)
            {
                foreach (ItemDocument r in ListImg)
                {
                    string url = "";// "http://crm.diaoc247.vn/" + r.ImageUrl.ToString();
                    zSb.AppendLine("<a href='" + url + "' title=''><img width='96' height='72' src='" + url + "'></a>");
                }
            }
            else
            {
                string url = @"/_Resource/Img/noimage.png";
                zSb.AppendLine("<a href='" + url + "' title='Chưa có hình'><img width='96' height='72' src='" + url + "'></a>");
            }
            zSb.AppendLine("            </div>");
            Lit_Info.Text = zSb.ToString();
        }

        bool CheckViewPhone(int EmployeeKey, int AssetKey, string Type)
        {
            #region [Check View Phone]
            int OwnerAgent = EmployeeKey;
            int CurrentAgent = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Position = HttpContext.Current.Request.Cookies["UserLog"]["PositionKey"].ToInt();

            if (UnitLevel == 0 ||
                UnitLevel == 1 ||
                Position == 2)
            {
                return true;
            }

            if (CurrentAgent == OwnerAgent)
            {
                return true;
            }
            else
            {
                DataTable zTableSharePermition = Share_Permition_Data.List_ShareDepartment(AssetKey, Type);
                {
                    for (int i = 0; i < zTableSharePermition.Rows.Count; i++)
                    {
                        if (CurrentAgent == zTableSharePermition.Rows[i]["EmployeeKey"].ToInt())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
            #endregion
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            int AssetKey = HID_AssetKey.Value.ToInt();
            string Type = HID_AssetType.Value;
            Product_Info zAsset = new Product_Info(AssetKey, Type);
            zAsset.ItemAsset.StatusKey = DDLStatus.SelectedValue;
            zAsset.ItemAsset.Description = txtDescription.Value.Trim();
            zAsset.ItemAsset.ModifiedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"];
            zAsset.ItemAsset.ModifiedName = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"].ToUrlDecode();
            zAsset.UpdateMobile(Type);
        }
    }
}