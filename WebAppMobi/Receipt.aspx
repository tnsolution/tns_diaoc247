﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Receipt.aspx.cs" Inherits="WebAppMobi.Receipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .trheader {
            background-color: #DDDDDD;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success flat">
                        <div class="box-header with-border">
                            <h4>
                                <asp:Literal ID="Lit_TimeText" runat="server"></asp:Literal>
                                <span class="box-tools btn-group pull-right">
                                    <a loading="yes" class="btn btn-default btn-sm" id="ViewPrevious"><i class="glyphicon glyphicon-chevron-left"></i></a>                               
                                    <a loading="yes" class="btn btn-default btn-sm" id="ViewNext"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                </span>
                            </h4>
                        </div>
                        <div class="box-body">
                            <asp:Literal ID="LiteralData1" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HiddenFromDate" runat="server" Value="" />
    <asp:HiddenField ID="HiddenToDate" runat="server" Value="" />
    <asp:HiddenField ID="Hidden_ViewTime" runat="server" Value="0" />
    <asp:HiddenField ID="Hidden_ViewNextPrev" runat="server" Value="0" />
    <asp:Button ID="btnActionViewTime" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewTime_Click" />
    <asp:Button ID="btnActionViewNextPrev" runat="server" Text="Button" Style="display: none; visibility: hidden" OnClick="btnActionViewNextPrev_Click" />
    <script>
        $(document).ready(function () {
            $("#ViewPrevious").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(-1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewNext").click(function () {
                $("[id$=Hidden_ViewNextPrev]").val(1);
                $("[id$=btnActionViewNextPrev]").trigger("click");
            });
            $("#ViewYear").click(function () {
                $("[id$=Hidden_ViewTime]").val(1);
                $("[id$=btnActionViewTime]").trigger("click");
            });
            $("#ViewMonth").click(function () {
                $("[id$=Hidden_ViewTime]").val(2);
                $("[id$=btnActionViewTime]").trigger("click");
            });
            $("#tbltable tr.action").click(function () {
                $("tr[id]").hide();
                var obj = $(this);
                var id = obj.attr("key");
                obj.toggleClass("open");
                if (obj.hasClass("open")) {
                    $("tr[id=" + id + "]").show();
                }
            });
            $("tr[head]").click(function () {
                var head = $(this).attr("head");
                var obj = $(this);
                obj.toggleClass("open");
                $("tr[xem]").hide();
                if (obj.hasClass("open")) {
                    $("tr[xem=" + head + "]").show();
                }
            });
            $("a[inuncheck]").click(function () {
                var id = $(this).attr('inuncheck');
                if (id != undefined) {
                    UnApproveCheck(id);
                }
            });
            $("a[incheck]").click(function () {
                var id = $(this).attr('incheck');
                if (id != undefined) {
                    ApproveCheck(id);
                }
            });
            $("tr[xem]").hide();
            $("tr[id]").hide();
        });
        function ApproveCheck(trid) {
            if (confirm("Bạn có chắc duyệt thông tin !. Sau khi duyệt thông tin sẽ không thay đổi được")) {
                $.ajax({
                    type: 'POST',
                    url: '/Receipt.aspx/ReceiptStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            alert("Lỗi !" + msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
        function UnApproveCheck(trid) {
            if (confirm("Bạn có chắc gỡ duyệt thông tin !.")) {
                $.ajax({
                    type: 'POST',
                    url: '/Receipt.aspx/ReceiptStatus',
                    data: JSON.stringify({
                        'AutoKey': trid,
                    }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function () {
                        $('.se-pre-con').fadeIn('slow');
                    },
                    success: function (msg) {
                        if (msg.d.Message != '') {
                            alert("Lỗi !" + msg.d.Message);
                        }
                        else {
                            location.reload();
                        }
                    },
                    complete: function () {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }
        }
    </script>
</asp:Content>
