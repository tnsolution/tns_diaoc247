﻿using Lib.FNC;
using Lib.HRM;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class Receipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewCurrentMonth();
            }
        }
        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }
        void ViewCurrentMonth()
        {
            //Lit_TimeText.Text = "Phí thu tháng " + DateTime.Now.Month;

            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            ViewActivity(FromDate, ToDate);
            Hidden_ViewTime.Value = "2";
        }
        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                //Lit_TimeText.Text = "Phí thu tháng " + FromDate.Month;
                return;
            }
            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();

                ViewActivity(FromDate, ToDate);
                //Lit_TimeText.Text = "Phí thu tháng " + FromDate.Month;
                return;
            }
        }
        void ViewActivity(DateTime FromDate, DateTime ToDate)
        {
            int Employee = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            int Department = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();

            DataTable zTable = new DataTable();
            StringBuilder zSb1 = new StringBuilder();
            switch (UnitLevel)
            {
                case 0:
                case 1:
                    zTable = Departments_Data.List(0);
                    break;

                default:
                    zTable = Departments_Data.List(Department);
                    break;
            }
            double tong = 0;
            foreach (DataRow r in zTable.Rows)
            {
                DataTable zFee = Receipt_Detail_Data.List(FromDate.Month, FromDate.Year, r["DepartmentKey"].ToInt(), 1);
                int sochuaduyet = zFee.Select("IsApproved = 0").Length;
                double tongtien = zFee.Compute("SUM(Amount)", "IsApproved = 1 ").ToDouble();
                tong += tongtien;

                zSb1.AppendLine("<table class='table' id=tbltable>");
                zSb1.AppendLine("    <tr class='action' style='background: #DDDD' key=" + r["DepartmentKey"].ToInt() + ">");
                zSb1.AppendLine("            <td><b>" + r["DepartmentName"].ToString()
                  + "</b><span class='pull-right'> | Chờ duyệt: "
                  + sochuaduyet + "</span><span class='pull-right' style='font-weight: bold; color: #c90927;'>"
                  + tongtien.ToString("n0") + "</span></td>");
                zSb1.AppendLine("    </tr>");
                zSb1.AppendLine("     <tr id=" + r["DepartmentKey"].ToInt() + ">");
                zSb1.AppendLine("           <td>");

                #region [Chi tiết trong phòng]
                foreach (DataRow rf in zFee.Rows)
                {
                    string BackColor = "";
                    string Status = "";
                    int Duyet = rf["IsApproved"].ToInt();
                    if (Duyet == 1)
                    {
                        Status = "<a href='#' inUncheck=" + rf["AutoKey"].ToString() + "><img width=20 style='cursor:pointer;float:right' id='" + rf["AutoKey"].ToString()
                            + "' check='" + rf["IsApproved"].ToInt() + "' alt='' src='../themes/custom-image/true.png' /></a>";
                    }
                    else
                    {
                        BackColor = "style='background:#3c8dbc70'";
                        Status = "<a href='#' inCheck=" + rf["AutoKey"].ToString() + "><img width=20 style='cursor:pointer;float:right' id='" + rf["AutoKey"].ToString()
                            + "' check='" + rf["IsApproved"].ToInt() + "' alt='' src='../themes/custom-image/false.png' /></a>";
                    }

                    zSb1.AppendLine("<table class='table' id='tblTable' " + BackColor + ">");
                    zSb1.AppendLine("    <tr>");
                    zSb1.AppendLine("            <td style='border-top: 3px solid #00c0ef;'><b>Ngày thu: </b> " + rf["ReceiptDate"].ToDateString()
                        + "<span style='float:right; font-weight: bold; color: #c90927; font-size: 16px'>" + rf["Amount"].ToDoubleString() + "</span></td>");
                    zSb1.AppendLine("    </tr>");
                    zSb1.AppendLine("     <tr><td><b>Nội dung: </b>" + rf["Contents"].ToString() + "</td></tr>");
                    zSb1.AppendLine("     <tr head='"+ rf["AutoKey"].ToString() + "'><td><span><i class='fa fa-chevron-right'></i></span><b> Ghi chú: </b>" + rf["Description"].ToString() + "</td></tr>");
                    zSb1.AppendLine("     <tr xem='" + rf["AutoKey"].ToString() + "'><td><b>Người giao: </b>" + rf["ReceiptByName"].ToString() + "</td></tr>");
                    zSb1.AppendLine("     <tr xem='" + rf["AutoKey"].ToString() + "'><td><b>Người nhận: </b>" + rf["ReceiptFromName"].ToString() + Status + "</td></tr>");
                    zSb1.AppendLine("</table>");
                }
                #endregion

                zSb1.AppendLine("           </td>");
                zSb1.AppendLine("     </tr>");
                zSb1.AppendLine("</table>");
            }
            Lit_TimeText.Text = "Phí chi tháng " + FromDate.Month + " | <span style='font-weight: bold; color: #c90927;'>" + tong.ToString("n0") + "</span>";
            LiteralData1.Text = zSb1.ToString();
        }
        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }
        [WebMethod]
        public static ItemReturn ReceiptStatus(int AutoKey)
        {
            ItemReturn zResult = new ItemReturn();
            int UnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();
            string UserKey = HttpContext.Current.Request.Cookies["UserLog"]["UserKey"];
            string RolePage = "FNC";
            string[] Permitsion = User_Data.RolesCheck(UserKey, RolePage).Split(',');
            if (Permitsion[2].ToInt() == 0 && UnitLevel > 2)
            {
                zResult.Message = "Bạn chưa có quyền duyệt thông tin này xin vui lòng liên hệ quản lý !.";
                return zResult;
            }

            Receipt_Detail_Info zInfo = new Receipt_Detail_Info(AutoKey);
            if (zInfo.IsApproved == 0)
                zInfo.IsApproved = 1;
            else
                zInfo.IsApproved = 0;
            zInfo.ApprovedBy = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            zInfo.SetStatus();
            if (zInfo.Message != string.Empty)
                zResult.Message = zInfo.Message;
            return zResult;
        }
    }
}