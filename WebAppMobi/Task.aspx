﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Task.aspx.cs" Inherits="WebAppMobi.Task" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
            <style>
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="content-wrapper" style="padding-top: 50px;">
        <div class="content">
            <div class="row">
                <div class="box box-success flat">
                    <div class="box-header with-border" style="padding: 0px !important;">
                                               
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="divdata">
                            <asp:Literal ID="LiteralData" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="box-footer">
                        <ul id="pagination-demo" class="pagination-sm"></ul>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <script src="jquery.twbsPagination.min.js"></script>
    <asp:Literal ID="LiteralPage" runat="server"></asp:Literal>
    <script>
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        $(function () {
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('#back-to-top').tooltip('show');
        });

        function searchCustomer(Page) {        
           
            $('#divdata').empty();
            $.ajax({
                type: "POST",
                url: "Task.aspx/Customer_Search",
                data: JSON.stringify({                  
                    Page: Page
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                success: function (msg) {
                    for (var i = 0; i < msg.d.length; i++) {                     
                        var tenkhach = msg.d[i].CustomerName;                       
                        var duan = msg.d[i].Product;
                        var phone = msg.d[i].Phone1;
                        var canho = msg.d[i].Asset;
                        var address1 = msg.d[i].Address1;
                        var address2 = msg.d[i].Address2;
                        var html = '';
                     
                        html += '   <table class="table table-hover table-bordered">'
                        html += '       <tr>'
                        html += '           <td>Tên khách hàng: ' + tenkhach + '<br />'
                        html += '               <b>Dự án: </b>' + duan + '<br />'
                        html += '               <b>Căn hộ: </b>' + canho + '<br />'
                        html += '               <b>Địa chỉ thường trú: </b>' + address1 + '<br />'
                        html += '               <b>Địa chỉ tạm trú: </b>' + address2 + '<br />'
                        html += '               <div><a href="tel:' + phone + '" class="btn btn-primary pull-right btn-flat">Gọi khách</a></div>'
                        html += '           </td>'
                        html += '       </tr>'
                        html += '   </table>'
                     
                        $('#divdata').append(html);
                    }
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
        function countProduct() {            
            var assetID = $("[id$=txt_Search]").val();        
            if (assetID == null)
                assetID = "";
            $.ajax({
                type: "POST",
                url: "Customer.aspx/Customer_Total",
                data: JSON.stringify({
                    ProjectKey: project,
                    Category: category,
                    AssetName: assetID
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                success: function (msg) {
                    alert(msg.d);
                    return msg.d;
                },
                complete: function () {

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Lỗi phát sinh !');
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    </script>
</asp:Content>
