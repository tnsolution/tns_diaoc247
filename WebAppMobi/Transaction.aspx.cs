﻿using Lib.SAL;
using Lib.SYS;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebAppMobi
{
    public partial class Transaction : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.Cookies["UserLog"] == null)
                    Response.Redirect("/Login.aspx");

                ViewCurrentMonth();
            }
        }

        private void StartEndOfWeek(DateTime DateCurrent, out DateTime WeekStart, out DateTime WeekEnd)
        {
            DayOfWeek zStartOfWeek = DayOfWeek.Sunday;
            DayOfWeek zEndOfWeek = DayOfWeek.Saturday;
            int zStartDiff = zStartOfWeek - DateCurrent.DayOfWeek;
            int zEndDiff = zEndOfWeek - DateCurrent.DayOfWeek;
            WeekStart = DateCurrent.AddDays(zStartDiff);
            WeekEnd = DateCurrent.AddDays(zEndDiff);
            WeekEnd = new DateTime(WeekEnd.Year, WeekEnd.Month, WeekEnd.Day, 23, 59, 59);
        }
        private void StartEndOfMonth(DateTime DateCurrent, out DateTime MonthStart, out DateTime MonthEnd)
        {
            MonthStart = new DateTime(DateCurrent.Year, DateCurrent.Month, 1, 0, 0, 0);
            MonthEnd = MonthStart.AddMonths(1);
            MonthEnd = MonthEnd.AddDays(-1);
            MonthEnd = new DateTime(MonthEnd.Year, MonthEnd.Month, MonthEnd.Day, 23, 59, 59);
        }

        void ViewCurrentYear()
        {
            DateTime FromDate = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            int ID = int.Parse(Request["Type"]);
            if (ID == 1)
                Lit_TimeText.Text = "<h3 class='box-title'>GD cần duyệt năm " + FromDate.Year + "</h3>";
            if (ID == 2)
                Lit_TimeText.Text = "<h3 class='box-title'>GD cần thu năm " + FromDate.Year + "</h3>";

            ViewHtml(FromDate, ToDate, ID);
        }
        void ViewCurrentMonth()
        {
            DateTime FromDate;
            DateTime ToDate;
            StartEndOfMonth(DateTime.Now, out FromDate, out ToDate);

            HiddenFromDate.Value = FromDate.ToString();
            HiddenToDate.Value = ToDate.ToString();

            int ID = int.Parse(Request["Type"]);
            if (ID == 1)
                Lit_TimeText.Text = "<h3 class='box-title'>GD cần duyệt tháng " + FromDate.Month + "</h3>";
            if (ID == 2)
                Lit_TimeText.Text = "<h3 class='box-title'>GD cần thu tháng " + FromDate.Month + "</h3>";

            ViewHtml(FromDate, ToDate, ID);
        }

        void ViewMonth(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(1);
                ToDate = ToDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();
                int ID = int.Parse(Request["Type"]);
                if (ID == 1)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần duyệt tháng " + FromDate.Month + "</h3>";
                if (ID == 2)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần thu tháng " + FromDate.Month + "</h3>";
                ViewHtml(FromDate, ToDate, ID);
                return;
            }

            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddMonths(-1);
                ToDate = FromDate.AddMonths(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();
                int ID = int.Parse(Request["Type"]);
                if (ID == 1)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần duyệt tháng " + FromDate.Month + "</h3>";
                if (ID == 2)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần thu tháng " + FromDate.Month + "</h3>";
                ViewHtml(FromDate, ToDate, ID);
                return;
            }
        }
        void ViewYear(int TimeNextPrev)
        {
            if (TimeNextPrev == 1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(1);
                ToDate = ToDate.AddYears(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();
                int ID = int.Parse(Request["Type"]);
                if (ID == 1)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần duyệt năm " + FromDate.Year + "</h3>";
                if (ID == 2)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần thu năm " + FromDate.Year + "</h3>";
                ViewHtml(FromDate, ToDate, ID);
                return;
            }

            if (TimeNextPrev == -1)
            {
                DateTime FromDate = Convert.ToDateTime(HiddenFromDate.Value);
                DateTime ToDate = Convert.ToDateTime(HiddenToDate.Value);

                FromDate = FromDate.AddYears(-1);
                ToDate = FromDate.AddYears(1).AddDays(-1);

                HiddenFromDate.Value = FromDate.ToString();
                HiddenToDate.Value = ToDate.ToString();
                int ID = int.Parse(Request["Type"]);
                if (ID == 1)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD chưa duyệt năm " + FromDate.Year + "</h3>";
                if (ID == 2)
                    Lit_TimeText.Text = "<h3 class='box-title'>GD cần thu năm " + FromDate.Year + "</h3>";
                ViewHtml(FromDate, ToDate, ID);
                return;
            }
        }

        void ViewHtml(DateTime FromDate, DateTime ToDate, int ID)
        {
            int zEmployeeKey = HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"].ToInt();
            int zDeparmentKey = HttpContext.Current.Request.Cookies["UserLog"]["DepartmentKey"].ToInt();
            int zUnitLevel = HttpContext.Current.Request.Cookies["UserLog"]["UnitLevel"].ToInt();

            DataTable zTable = new DataTable();
            switch (zUnitLevel)
            {
                case 0:
                case 1:
                    zTable = Trade_Data.Get(0, 0, ID, FromDate, ToDate);
                    break;

                case 2:
                    zTable = Trade_Data.Get(zDeparmentKey, 0, ID, FromDate, ToDate);
                    break;

                default:
                    zTable = Trade_Data.Get(zDeparmentKey, zEmployeeKey, ID, FromDate, ToDate);
                    break;
            }

            StringBuilder zSb = new StringBuilder();
            foreach (DataRow r in zTable.Rows)
            {
                zSb.AppendLine("          <div class='table-responsive'>");
                zSb.AppendLine("            <table class='table'>");
                zSb.AppendLine("              <tbody><tr>");
                zSb.AppendLine("                <th style='width:30%'>Ngày lập:</th>");
                zSb.AppendLine("                <td>" + Convert.ToDateTime(r["TransactionDate"]).ToString("dd/MM/yyyy") + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Ngày ký</th>");
                zSb.AppendLine("                <td>" + Convert.ToDateTime(r["DateContract"]).ToString("dd/MM/yyyy") + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Dự án:</th>");
                zSb.AppendLine("                <td>" + r["ProjectName"].ToString() + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Căn hộ:</th>");
                zSb.AppendLine("                <td>" + r["AssetID"].ToString() + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Loại căn hộ:</th>");
                zSb.AppendLine("                <td>" + r["AssetCategory"].ToString() + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Địa chỉ:</th>");
                zSb.AppendLine("                <td style='word-wrap: break-word'>" + r["Address"].ToString() + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Giao dịch:</th>");
                zSb.AppendLine("                <td>" + r["TradeCategory"].ToString() + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Doanh thu:</th>");
                zSb.AppendLine("                <td class='money'>" + Convert.ToDouble(r["Income"]).ToString("n0") + "</td>");
                zSb.AppendLine("              </tr>");
                zSb.AppendLine("              <tr>");
                zSb.AppendLine("                <th>Nhân viên:</th>");
                zSb.AppendLine("                <td>" + r["EmployeeName"].ToString() + "</td>");
                zSb.AppendLine("              </tr>");
              
                if (zUnitLevel == 0 || zUnitLevel == 1)
                {
                    string button = "<button type='button' class='btn btn-info pull-right'><i class='fa fa-check-o'></i> Duyệt GD</ button>";
                    if (ID == 1)
                        button = "<button btn='DuyetGD' type='button' class='btn btn-info pull-right' key='" + r["TransactionKey"].ToString() + "'><i class='fa fa-check-o'></i> Duyệt GD</ button>";
                    else
                        button = "<button btn='DuyetThu' type='button' class='btn btn-success pull-right' key='" + r["TransactionKey"].ToString() + "'><i class='fa fa-check-o'></i> Duyệt thu</ button>";

                    zSb.AppendLine("              </tr><td colspan='2'>" + button + "</td><tr>");
                }
                zSb.AppendLine("            </tbody></table>");
                zSb.AppendLine("          </div>");
            }

            LiteralData.Text = zSb.ToString();
        }

        protected void btnActionViewTime_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewCurrentYear();
                    break;

                //view month
                case 2:
                    ViewCurrentMonth();
                    break;

                //view month
                default:
                    ViewCurrentMonth();
                    break;
            }
        }
        protected void btnActionViewNextPrev_Click(object sender, EventArgs e)
        {
            int ViewType = Hidden_ViewTime.Value.ToInt();
            int ViewTimeNextPrev = Hidden_ViewNextPrev.Value.ToInt();
            switch (ViewType)
            {
                //view year
                case 1:
                    ViewYear(ViewTimeNextPrev);
                    break;

                //view month
                case 2:
                    ViewMonth(ViewTimeNextPrev);
                    break;

                //view month
                default:
                    ViewMonth(ViewTimeNextPrev);
                    break;
            }
        }

        [WebMethod]
        public static string SaveApprove(int MasterKey)
        {
            int IsApproved;
            Trade_Info zInfo = new Trade_Info(MasterKey);
            if (zInfo.IsApproved == 0)
                IsApproved = 1;
            else
                IsApproved = 0;

            zInfo.IsApproved = IsApproved;
            zInfo.DateApprove = DateTime.Now;
            zInfo.ApprovedBy = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);
            zInfo.Approve();

            if (zInfo.Message == string.Empty)
                return zInfo.Key.ToString();
            else
                return "Lỗi không lưu được ! " + zInfo.Message;
        }
        [WebMethod]
        public static string SaveFinish(int MasterKey)
        {
            int IsFinish;
            Trade_Info zInfo = new Trade_Info(MasterKey);
            if (zInfo.IsFinish == 0)
                IsFinish = 1;
            else
                IsFinish = 0;

            zInfo.IsFinish = IsFinish;
            zInfo.FinishDate = DateTime.Now;
            zInfo.FinishBy = int.Parse(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"]);
            zInfo.Finish();

            if (zInfo.Message == string.Empty)
                return zInfo.Key.ToString();
            else
                return "Lỗi không lưu được ! " + zInfo.Message;
        }
    }
}