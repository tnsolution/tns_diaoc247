﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaveFile.aspx.cs" Inherits="WebAppMobi.Upload.SaveFile" %>

<%
    HID_AssetType.Value = Request["type"];
    HID_AssetKey.Value = Request["key"];
    HID_ProjectKey.Value = Request["project"];
    string zSQL = "";
    try
    {
        string directory = @"C:\inetpub\web\crm.247.v3\Upload\File\" + HID_AssetType.Value + "\\" + HID_AssetKey.Value + "\\";
        string zPath = "~/Upload/File/" + HID_AssetType.Value + "/" + HID_AssetKey.Value + "/";
        HttpFileCollection files = HttpContext.Current.Request.Files;
        for (int index = 0; index < files.Count; index++)
        {

            HttpPostedFile uploadfile = files[index];
            string Name = uploadfile.FileName;
            System.IO.DirectoryInfo nDir = new System.IO.DirectoryInfo(directory);
            if (!nDir.Exists)
            {
                nDir.Create();
            }
            uploadfile.SaveAs(directory + Name);

            string thumb = System.IO.Path.GetFileNameWithoutExtension(Name) + "_thumb" + System.IO.Path.GetExtension(Name);

            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(uploadfile.InputStream);
            System.Drawing.Image objImage = resizeImage(bitmap, new System.Drawing.Size(150, 150));
            objImage.Save(directory + thumb, System.Drawing.Imaging.ImageFormat.Jpeg);

            string zFileName = System.IO.Path.GetFileNameWithoutExtension(uploadfile.FileName);
            string zFileThumb = zFileName + "_thumb" + System.IO.Path.GetExtension(uploadfile.FileName);
            string zFileLarge = System.IO.Path.GetFileName(uploadfile.FileName);

            string zFilePathThumb = Server.MapPath(System.IO.Path.Combine(zPath, zFileThumb));
            string zFilePathLarge = Server.MapPath(System.IO.Path.Combine(zPath, zFileLarge));

            string zFileUrlThumb = (zPath + zFileThumb).Substring(1, zPath.Length + zFileThumb.Length - 1);
            string zFileUrlLarge = (zPath + zFileLarge).Substring(1, zPath.Length + zFileLarge.Length - 1);

            zSQL += @"INSERT INTO dbo.PUL_Document (
TableName, ProjectKey, CategoryKey, ImagePath, ImageThumb, ImageUrl, ImageName ,CreatedDate, ModifiedDate, CreatedBy, CreatedName, ModifiedBy, ModifiedName) 
VALUES ('" + HID_AssetType.Value + "','" + HID_AssetKey.Value + "','" + 135 + "',N'" + zFileLarge + "',N'" + zFileUrlThumb + "',N'" + zFileUrlLarge + "',N'" + zFileName + "',GETDATE(), GETDATE(),'"
                    + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                    + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "','"
                    + HttpContext.Current.Request.Cookies["UserLog"]["EmployeeKey"] + "',N'"
                    + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["UserLog"]["EmployeeName"]) + "')";
        }

        HttpContext.Current.Response.Write(directory + "Upload successfully!");
        if (zSQL != string.Empty)
        {
            string Message = Lib.SYS.CustomInsert.Exe(zSQL).Message;
            if (Message != string.Empty)
            {
                HttpContext.Current.Response.Write("Lỗi" + Message);
            }
        }


    }
    catch (Exception ex)
    {
        HttpContext.Current.Response.Write(ex.ToString());
    }
%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HID_ProjectKey" runat="server" Value="0" />
        <asp:HiddenField ID="HID_AssetType" runat="server" Value="0" />
        <asp:HiddenField ID="HID_AssetKey" runat="server" Value="0" />
        <div>
        </div>
    </form>
</body>
</html>
